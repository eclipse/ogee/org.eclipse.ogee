/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.nodes;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ogee.exploration.tree.Activator;
import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.exploration.tree.api.TreeNode;
import org.eclipse.ogee.exploration.tree.background.BackgroundTreeContentProvider;
import org.eclipse.ogee.exploration.tree.nls.messages.Messages;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.swt.graphics.Image;

/**
 * Represents a service node in the Service Exploration tree
 */
public class ServiceNode extends TreeNode {
	private EDMXSet edmxSet;
	public static Image image;

	// initialize images
	static {
		ImageRegistry registry = Activator.getDefault().getImageRegistry();
		ServiceNode.image = registry.get(Activator.SERVICE);
	}

	public ServiceNode(String title, EDMXSet edmxSet) {
		this(title, edmxSet, null);
	}

	public ServiceNode(String title, EDMXSet edmxSet, ITreeNode parent) {
		super(title, parent);

		this.edmxSet = edmxSet;
	}

	@Override
	public boolean hasChildren() {
		if (this.edmxSet == null) {
			return false;
		}

		return true;
	}

	@Override
	public Image getImage() {
		return ServiceNode.image;
	}

	protected void createChildren() {
		this.children = new Object[0];

		if (this.edmxSet == null) {
			return;
		}

		EDMX mainEDMX = this.edmxSet.getMainEDMX();
		if (mainEDMX == null) {
			return;
		}

		DataService dataService = mainEDMX.getDataService();
		if (dataService == null) {
			return;
		}

		EList<Schema> schemata = dataService.getSchemata();
		if (schemata == null || schemata.isEmpty()) {
			return;
		}

		this.children = new Object[schemata.size()];
		for (int i = 0; i < schemata.size(); i++) {
			this.children[i] = schemata.get(i);
		}
	}

	public void setEdmxSet(EDMXSet edmxSet) {
		this.edmxSet = edmxSet;
	}

	public EDMXSet getEdmxSet() {
		return this.edmxSet;
	}

	@Override
	public void clearChildren() {
		try {
			if (this.edmxSet != null) {
				for (TreeIterator<EObject> iter = this.edmxSet.eAllContents(); iter
						.hasNext();) {
					iter.next().eAdapters().clear();
				}

				EcoreUtil.delete(this.edmxSet, true);
			}

			this.edmxSet = null;
		}
		// CHECKSTYLE:OFF
		catch (Throwable cause) {
			Logger.getLogger(BackgroundTreeContentProvider.class).logWarning(
					Messages.ErrorFreeingResource, cause);
		}
		// CHECKSTYLE:ON
		super.clearChildren();
	}
}
