/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.background;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

import org.eclipse.ogee.exploration.tree.api.ITreeNode;

public class BackgroundJobManager {
	private static final int MAX_THREADS = 10;

	private ConcurrentHashMap<ITreeNode, ExecutorService> pools;

	public BackgroundJobManager() {
		this.pools = new ConcurrentHashMap<ITreeNode, ExecutorService>();
	}

	public void scheduleJob(Runnable backgroundJob, ITreeNode topTreeNode) {
		ExecutorService pool = this.pools.get(topTreeNode);
		if (pool == null) {
			pool = java.util.concurrent.Executors
					.newFixedThreadPool(MAX_THREADS);
			this.pools.put(topTreeNode, pool);
		}

		pool.execute(backgroundJob);
	}
}
