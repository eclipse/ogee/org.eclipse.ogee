/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.background;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ogee.exploration.tree.nls.messages.Messages;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.swt.widgets.Display;

final class BackgroundjobHasChildren implements Runnable {
	private final Display display;
	private final BackgroundTreeContentProvider backgroundTreeContentProvider;
	private final Object element;

	BackgroundjobHasChildren(
			BackgroundTreeContentProvider backgroundTreeContentProvider,
			Display display, Object element) {
		this.backgroundTreeContentProvider = backgroundTreeContentProvider;
		this.display = display;
		this.element = element;
	}

	@Override
	public void run() {
		boolean hasChildren = false;

		try {
			hasChildren = this.backgroundTreeContentProvider.delegateContentPrivider
					.hasChildren(this.element);
		} catch (Exception e) // NOPMD
		{
			Logger.getFrameworkLogger().logError(
					Messages.BackgroundjobHasChildren_0 + this.element, e);
		}

		this.backgroundTreeContentProvider.hasChildrenCache.put(this.element,
				hasChildren);

		final TreeViewer viewer = this.backgroundTreeContentProvider.viewer;

		if (viewer != null && this.display != null
				&& !viewer.getControl().isDisposed()) {
			this.display.asyncExec(new Runnable() {
				@Override
				public void run() {
					if (viewer != null && display != null
							&& !viewer.getControl().isDisposed()) {
						viewer.refresh(element, true);
					}
				}
			});
		}
	}
}