/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.background;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Display;

final class BackgroundJobRefresh implements Runnable {
	private final BackgroundTreeContentProvider backgroundTreeContentProvider;
	private final Display display;
	private final Object element;

	BackgroundJobRefresh(
			BackgroundTreeContentProvider backgroundTreeContentProvider,
			Display display, Object element) {
		this.backgroundTreeContentProvider = backgroundTreeContentProvider;
		this.display = display;
		this.element = element;
	}

	@Override
	public void run() {
		final TreeViewer viewer = this.backgroundTreeContentProvider.viewer;

		if (viewer != null && display != null
				&& !viewer.getControl().isDisposed())
			display.asyncExec(new Runnable() {
				@Override
				public void run() {
					if (viewer != null && display != null
							&& !viewer.getControl().isDisposed())
						viewer.refresh(element, true);
				}
			});
	}
}