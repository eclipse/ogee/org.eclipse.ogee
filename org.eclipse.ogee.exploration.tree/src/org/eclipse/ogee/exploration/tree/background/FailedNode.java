/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.background;

import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.exploration.tree.api.TreeNode;

class FailedNode extends TreeNode {
	public FailedNode(ITreeNode parent) {
		super("Failed to Load", parent); //$NON-NLS-1$
	}

	@Override
	protected void createChildren() {

	}

	@Override
	public boolean hasChildren() {
		return false;
	}

	static final FailedNode INSTANCE = new FailedNode(null);
}