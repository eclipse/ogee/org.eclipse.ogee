/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.background;

import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.exploration.tree.api.TreeNode;

public class LoadingNode extends TreeNode {
	public LoadingNode(ITreeNode parent) {
		super("Loading...", parent); //$NON-NLS-1$
	}

	@Override
	protected void createChildren() {

	}

	@Override
	public boolean hasChildren() {
		return false;
	}

	static final LoadingNode INSTANCE = new LoadingNode(null); // NOPMD
}