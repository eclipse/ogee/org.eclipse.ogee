/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.background;

import java.util.Observable;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.exploration.tree.nls.messages.Messages;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.swt.widgets.Display;

public class BackgroundTreeContentProvider extends Observable implements
		ITreeContentProvider {
	ITreeContentProvider delegateContentPrivider;
	TreeViewer viewer;
	ConcurrentHashMap<Object, Object[]> cache = new ConcurrentHashMap<Object, Object[]>();
	ConcurrentHashMap<Object, Boolean> hasChildrenCache = new ConcurrentHashMap<Object, Boolean>();

	private BackgroundJobManager backgroudJobManager = new BackgroundJobManager();
	private Display display = Display.getCurrent();

	public static final Object[] LOADING_ITEM = new Object[] { LoadingNode.INSTANCE };

	public static final Object[] FAILED_ITEM = new Object[] { FailedNode.INSTANCE };

	public BackgroundTreeContentProvider(
			ITreeContentProvider delegateContentPrivider) {
		super();
		this.delegateContentPrivider = delegateContentPrivider;
	}

	@Override
	public Object[] getChildren(final Object parentElement) {
		if (parentElement instanceof LoadingNode
				|| parentElement instanceof FailedNode) {
			return new Object[0];
		}

		if (parentElement instanceof ITreeNode) {
			Object[] children = this.cache.get(parentElement);
			if (children == null) {
				ITreeNode treeNode = (ITreeNode) parentElement;
				asyncGetChildren(treeNode);

				children = LOADING_ITEM;
			}

			return children; // NOPMD
		} else {
			return this.delegateContentPrivider.getChildren(parentElement);
		}
	}

	/**
	 * Executes the async GetChildren operation in a background. When the
	 * execution is finished, the vi
	 * 
	 * @param parentElement
	 */
	private void asyncGetChildren(final ITreeNode treeNode) {
		this.cache.put(treeNode, LOADING_ITEM);

		ITreeNode topParent = getTopTreeNode(treeNode);
		this.backgroudJobManager.scheduleJob(new BackgroundJobGetChildren(this,
				display, treeNode), topParent);
	}

	@Override
	public void dispose() {
		this.delegateContentPrivider.dispose();
	}

	@Override
	public void inputChanged(Viewer viewer, Object obj, Object obj1) {
		this.delegateContentPrivider.inputChanged(viewer, obj, obj1);
		this.viewer = (TreeViewer) viewer;
		this.cache.clear();
		this.hasChildrenCache.clear();
	}

	@Override
	public Object[] getElements(Object obj) {
		return delegateContentPrivider.getElements(obj);
	}

	@Override
	public Object getParent(Object obj) {
		return delegateContentPrivider.getParent(obj);
	}

	@Override
	public boolean hasChildren(final Object element) {
		if (element instanceof LoadingNode || element instanceof FailedNode) {
			return false;
		}

		if (element instanceof ITreeNode) {
			ITreeNode treeNode = (ITreeNode) element;

			Boolean hasChildrenFromCache = this.hasChildrenCache.get(treeNode);
			if (hasChildrenFromCache == null) {
				asyncHasChildren(treeNode);
				hasChildrenFromCache = true;
			}

			return hasChildrenFromCache;
		} else {
			return this.delegateContentPrivider.hasChildren(element);
		}
	}

	private void asyncHasChildren(final ITreeNode treeNode) {
		this.hasChildrenCache.put(treeNode, true);

		ITreeNode topParent = getTopTreeNode(treeNode);
		this.backgroudJobManager.scheduleJob(new BackgroundjobHasChildren(this,
				display, treeNode), topParent);
	}

	private ITreeNode getTopTreeNode(ITreeNode treeNode) {
		if (treeNode.getParent() == null) {
			return treeNode;
		}

		return treeNode.getParent();
	}

	public void reload(final Object element) {
		Job job = new Job("refreshing " + element) { //$NON-NLS-1$
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					if (!(element instanceof ITreeNode)) {
						// reload supported only for ITreeNode
						return Status.OK_STATUS;
					}

					ITreeNode treeNode = (ITreeNode) element;
					treeNode.clearChildren();

					Object[] services = cache.get(element);
					if (services != null) {
						for (Object service : services) {
							hasChildrenCache.remove(service);
						}
					}

					hasChildrenCache.remove(element);
					cache.remove(element);

					BackgroundJobGetChildren backgroundJobGetChildren = new BackgroundJobGetChildren(
							BackgroundTreeContentProvider.this, display,
							element);
					backgroundJobGetChildren.run();

					BackgroundjobHasChildren backgroundjobHasChildren = new BackgroundjobHasChildren(
							BackgroundTreeContentProvider.this, display,
							element);
					backgroundjobHasChildren.run();

					BackgroundJobRefresh backgroundJobRefresh = new BackgroundJobRefresh(
							BackgroundTreeContentProvider.this, display,
							element);
					backgroundJobRefresh.run();
				}
				// CHECKSTYLE:OFF
				catch (Throwable cause) {
					notifyTreeObservers();

					Logger.getLogger(BackgroundTreeContentProvider.class)
							.logWarning(Messages.ErrorFreeingResource, cause);
				}
				// CHECKSTYLE:ON

				notifyTreeObservers();

				return Status.OK_STATUS;
			}

			/**
			 * 
			 */
			private void notifyTreeObservers() {
				setChanged();
				notifyObservers();
				clearChanged();
			}
		};

		job.schedule();
	}
}
