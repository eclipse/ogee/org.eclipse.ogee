/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.viewer;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.navigation.tree.MNViewLabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * Maps an element of the viewer's model to an image and text string used to
 * display the element in the viewer's control. Uses MNViewLabelProvider class
 * to map an EDMXSet object children.
 */
public class ServiceTreeLabelProvider extends LabelProvider {
	private MNViewLabelProvider mnViewLabelProvider;

	public ServiceTreeLabelProvider() {
		this.mnViewLabelProvider = new MNViewLabelProvider();
	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof ITreeNode) {
			return ((ITreeNode) element).getImage();
		} else {
			return this.mnViewLabelProvider.getImage(element);
		}
	}

	@Override
	public String getText(Object element) {
		if (element instanceof ITreeNode) {
			return ((ITreeNode) element).getTitle();
		} else {
			return this.mnViewLabelProvider.getText(element);
		}
	}
}
