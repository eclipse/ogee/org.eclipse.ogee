/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.viewer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.exploration.tree.sorter.TreeSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;

/**
 * Represents a service tree viewer
 */
public class ServiceTreeViewer extends TreeViewer {
	/**
	 * Constructor.
	 * 
	 * @param parent
	 *            parent composite containing the tree view
	 */
	public ServiceTreeViewer(Composite parent) {
		super(parent, SWT.V_SCROLL | SWT.BORDER);

		createPartControl(parent);
	}

	/**
	 * Sets root service element.
	 * 
	 * @param treeNode
	 *            root service element.
	 */
	public void setService(ITreeNode treeNode) {
		List<ITreeNode> list = new ArrayList<ITreeNode>();
		list.add(treeNode);
		setInput(list);
	}

	/**
	 * Creates the viewpart ui in the provided parent container according to the
	 * layout data.
	 * 
	 * @param parent
	 *            - parent container
	 * @param layoutData
	 *            - layout data
	 */
	public void setLayoutData(Object layoutData) {
		// set tree viewer layout data
		Tree tree = getTree();
		tree.setLayoutData(layoutData);
	}

	/**
	 * Creates the viewpart ui in the provided parent container according to the
	 * layout data.
	 * 
	 * @param parent
	 *            - parent container
	 */
	private void createPartControl(Composite parent) {
		setAutoExpandLevel(3);

		final ServiceTreeContentProvider treeContentProvider = new ServiceTreeContentProvider();
		setContentProvider(treeContentProvider);
		setLabelProvider(new ServiceTreeLabelProvider());

		setSorter(new TreeSorter());
	}

	/**
	 * Clears all tree data from the viewpart.
	 */
	public void clear() {
		setInput(null);
		refresh();
	}
}
