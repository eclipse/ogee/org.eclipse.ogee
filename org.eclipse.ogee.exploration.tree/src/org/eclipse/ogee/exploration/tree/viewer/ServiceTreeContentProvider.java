/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.viewer;

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.navigation.tree.MNViewContentProvider;

/**
 * Tree content provider for resource objects that can be adapted to the
 * interface ITreeNode. Uses MNViewContentProvider class to map an EDMXSet
 * object children.
 */
public class ServiceTreeContentProvider implements ITreeContentProvider {
	private MNViewContentProvider mnViewContentProvider;

	public ServiceTreeContentProvider() {
		this.mnViewContentProvider = new MNViewContentProvider();
	}

	@Override
	public Object[] getChildren(Object element) {
		if (element instanceof ITreeNode) {
			return ((ITreeNode) element).getChildren();
		}

		return this.mnViewContentProvider.getChildren(element);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object[] getElements(Object element) {
		if (element instanceof List) {
			return ((List<ITreeNode>) element).toArray();
		}

		return new Object[0];
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof ITreeNode) {
			return ((ITreeNode) element).getParent();
		}

		return mnViewContentProvider.getParent(element);
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof ITreeNode) {
			return ((ITreeNode) element).hasChildren();
		}

		return mnViewContentProvider.hasChildren(element);
	}

	@Override
	public void dispose() {
		mnViewContentProvider.dispose();
	}

	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
		// Do nothing
	}
}
