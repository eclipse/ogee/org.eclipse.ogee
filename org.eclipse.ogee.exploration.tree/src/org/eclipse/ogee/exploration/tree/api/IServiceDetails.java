/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.api;

import java.util.Date;

import org.eclipse.emf.edit.provider.IItemLabelProvider;

/**
 * Represents a service details object.
 */
public interface IServiceDetails extends IItemLabelProvider {
	/**
	 * Returns the service description.
	 * 
	 * @return the service description.
	 */
	public String getDescription();

	/**
	 * Returns the service title.
	 * 
	 * @return the service title.
	 */
	public String getTitle();

	/**
	 * Returns the service author.
	 * 
	 * @return the service author.
	 */
	public String getAuthor();

	/**
	 * Returns the service technical version.
	 * 
	 * @return the service technical version.
	 */
	public Integer getTechnicalServiceVersion();

	/**
	 * Returns the service id.
	 * 
	 * @return the service id.
	 */
	public String getID();

	/**
	 * Returns the service metadata url.
	 * 
	 * @return the service metadata url.
	 */
	public String getMetadataUrl();

	/**
	 * Returns the service technical name.
	 * 
	 * @return the service technical name.
	 */
	public String getTechnicalServiceName();

	/**
	 * Returns the service url.
	 * 
	 * @return the service url.
	 */
	public String getServiceUrl();

	/**
	 * Returns the service update date.
	 * 
	 * @return the service update date.
	 */
	public Date getUpdatedDate();
}
