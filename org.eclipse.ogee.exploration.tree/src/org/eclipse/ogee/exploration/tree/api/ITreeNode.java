/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.api;

import org.eclipse.swt.graphics.Image;

/**
 * Representing a node in the Service Exploration tree. This interface does not
 * represent any node that is derived from an input EDMXSet model object.
 */
public interface ITreeNode {
	/**
	 * Checks if the node's children exist.
	 * 
	 * @return true if exist, otherwise false.
	 */
	public boolean hasChildren();

	/**
	 * Returns node's parent node.
	 * 
	 * @return parent node.
	 */
	public ITreeNode getParent();

	/**
	 * Returns node's title.
	 * 
	 * @return title.
	 */
	public String getTitle();

	/**
	 * Returns node's children.
	 * 
	 * @return If there are no children, returns an empty array.
	 */
	public Object[] getChildren();

	/**
	 * Returns the node's element image.
	 * 
	 * @return image.
	 */
	public Image getImage();

	/**
	 * Clears the children elements.
	 */
	public void clearChildren();

	/**
	 * Reloadable sub interface
	 */
	public interface Reloadable {
		public void markReloadRequired();
	}

}
