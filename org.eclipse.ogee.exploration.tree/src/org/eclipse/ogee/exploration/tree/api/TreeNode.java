/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.exploration.tree.api;

import org.eclipse.swt.graphics.Image;

/* Default or adapter implementation for ITreeNode */
public abstract class TreeNode implements ITreeNode {
	protected ITreeNode parent;
	protected Object[] children;
	protected String title;

	public TreeNode(String title, ITreeNode parent) {
		this.title = title;
		this.parent = parent;
	}

	public Image getImage() {
		return null;
	}

	public String getTitle() {
		return title;
	}

	public boolean hasChildren() {
		return true;
	}

	public ITreeNode getParent() {
		return parent;
	}

	public Object[] getChildren() {
		if (children == null) {
			createChildren();
		}

		// safety if createChildren was not implemented correctly in subclass
		if (children == null) {
			children = new Object[0];
		}

		// returns internal array for better performance
		return children; // NOPMD
	}

	public void clearChildren() {
		this.children = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getTitle();
	}

	/* subclasses should override this method and add the child nodes */
	protected abstract void createChildren();
}
