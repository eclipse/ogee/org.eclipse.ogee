/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.odatav2.converter;

import org.eclipse.ogee.export.util.converters.api.IAssociationConverter;
import org.eclipse.ogee.export.util.converters.api.IAssociationSetConverter;
import org.eclipse.ogee.export.util.converters.api.IAssociationSetEndConverter;
import org.eclipse.ogee.export.util.converters.api.IComplexTypeConverter;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IEndConverter;
import org.eclipse.ogee.export.util.converters.api.IEntityContainerConverter;
import org.eclipse.ogee.export.util.converters.api.IEntitySetConverter;
import org.eclipse.ogee.export.util.converters.api.IEntityTypeConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IFunctionImportConverter;
import org.eclipse.ogee.export.util.converters.api.IFunctionImportParameterConverter;
import org.eclipse.ogee.export.util.converters.api.IKeyConverter;
import org.eclipse.ogee.export.util.converters.api.INavigationPropertyConverter;
import org.eclipse.ogee.export.util.converters.api.IPropertyConverter;
import org.eclipse.ogee.export.util.converters.api.IReferentialConstraintConverter;
import org.eclipse.ogee.export.util.converters.api.ISchemaConverter;
import org.eclipse.ogee.export.util.converters.api.ITransformer;
import org.eclipse.ogee.export.util.converters.api.IUsingConverter;

public class FactoryOdataV2 implements IFormatConverterFactory {

	@Override
	public ISchemaConverter getSchemaConverter() {
		ISchemaConverter schemaConverter = new SchemaConverterOdataV2();
		return schemaConverter;

	}

	@Override
	public IAssociationConverter getAssociationConverter() {
		IAssociationConverter associationConverter = new AssociationConverterOdataV2();
		return associationConverter;
	}

	@Override
	public IEndConverter getEndConverter() {
		IEndConverter endConverter = new EndConverterOdataV2();
		return endConverter;
	}

	@Override
	public IReferentialConstraintConverter getReferentialConstraintConverter() {
		IReferentialConstraintConverter referentialConstraintConverter = new ReferentialConstraintConverterOdataV2();
		return referentialConstraintConverter;
	}

	@Override
	public IDocumentationConverter getDocumatationConverter() {
		IDocumentationConverter documentationConverter = new DocumentationConverterOdataV2();
		return documentationConverter;
	}

	@Override
	public IAssociationSetConverter getAssociationSetConverter() {
		IAssociationSetConverter associationSetConverter = new AssociationSetConverterOdataV2();
		return associationSetConverter;
	}

	@Override
	public IAssociationSetEndConverter getAssociationSetEndConverter() {
		IAssociationSetEndConverter associationSetEndConverter = new AssociationSetEndConverterOdataV2();
		return associationSetEndConverter;
	}

	@Override
	public IComplexTypeConverter getComplexType() {
		IComplexTypeConverter complexTypeConverter = new ComplexTypeConverterOdataV2();
		return complexTypeConverter;
	}

	@Override
	public IEndConverter getDependentConverter() {
		IEndConverter endConverter = new DependentConverterOdataV2();
		return endConverter;
	}

	@Override
	public IEntityContainerConverter getEntityContainerConverter() {
		IEntityContainerConverter entityContainerConverter = new EntityContainerConverterOdataV2();
		return entityContainerConverter;
	}

	@Override
	public IEntitySetConverter getEntitySetConverter() {
		IEntitySetConverter entitySetConverter = new EntitySetConverterOdataV2();
		return entitySetConverter;
	}

	@Override
	public IEntityTypeConverter getEntityTypeConverter() {
		IEntityTypeConverter entityTypeConverter = new EntityTypeConverterOdataV2();
		return entityTypeConverter;
	}

	@Override
	public IFunctionImportConverter getFunctionImportConverter() {
		IFunctionImportConverter functionImportConverter = new FunctionImportConverterOdataV2();
		return functionImportConverter;
	}

	@Override
	public IFunctionImportParameterConverter getFunctionImportParameterConverter() {
		IFunctionImportParameterConverter functionImportParameterConverter = new FunctionImportParameterConverterOdataV2();
		return functionImportParameterConverter;
	}

	@Override
	public IKeyConverter getKeyConverter() {
		IKeyConverter keyConverter = new KeyConverterOdataV2();
		return keyConverter;
	}

	@Override
	public INavigationPropertyConverter getNavigationPropertyConverter() {
		INavigationPropertyConverter navigationPropertyConverter = new NavigationPropertyConverterOdataV2();
		return navigationPropertyConverter;
	}

	@Override
	public IEndConverter getPrincipalConverter() {
		IEndConverter endConverter = new PrincipalConverterOdataV2();
		return endConverter;
	}

	@Override
	public IPropertyConverter getPropertyConverter() {
		IPropertyConverter propertyConverter = new PropertyConverterOdataV2();
		return propertyConverter;
	}

	@Override
	public IPropertyConverter getPropertyRefConverter() {
		IPropertyConverter propertyRefConverter = new PropertyRefConverterOdataV2();
		return propertyRefConverter;
	}

	@Override
	public ITransformer getTransformer() {
		ITransformer transformer = new TransformationOdataV2();
		return transformer;
	}

	@Override
	public IUsingConverter getUsingConverter() {
		IUsingConverter usingConverter = new UsingConverterOdataV2();
		return usingConverter;
	}

}
