/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.odatav2.converter;

import java.io.File;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ogee.export.messages.ModelExportMessages;
import org.eclipse.ogee.export.odatav2.Activator;
import org.eclipse.ogee.export.util.ModelExportException;
import org.eclipse.ogee.export.util.ui.ModelExportAbstractUiProvider;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;

public class ODataV2EdmxHandler extends ModelExportAbstractUiProvider {
	private static final Logger LOGGER = Activator.LOGGER;

	public ODataV2EdmxHandler() {
		super(Activator.PLUGIN_ID);
	}

	@Override
	public final void performFinish(final EDMXSet edmxSet)
			throws ModelExportException {
		File destinationFile = new File(this.destinationText.getText());
		if (destinationFile.exists() && !destinationFile.canWrite()) {
			// Destination file {0} is not writable
			throw new ModelExportException(NLS.bind(
					ModelExportMessages.exportValidationsDestinationError,
					destinationFile.getName()));
		}

		ODataV2EdmxConvert edmConvert = new ODataV2EdmxConvert();

		// Create EDmx document file from EDMXset
		final Document document = edmConvert.createModel(edmxSet);

		// Save the document to local file
		try {
			printFile(document, destinationFile);
		} catch (TransformerException e) {
			throw new ModelExportException(e);
		}

	}

	private final void printFile(final Document document, File destinationFile)
			throws TransformerException {
		// Create transformer
		final Transformer transformer = new TransformationOdataV2()
				.createTransformer();
		final DOMSource source = new DOMSource(document);

		// Save fileO
		final StreamResult result = new StreamResult(destinationFile);

		// to set the indentation
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(
				"{http://xml.apache.org/xslt}indent-amount", "4");
		transformer.setOutputProperty(
				"{http://xml.apache.org/xalan}indent-amount", "4");

		// Transform source to the file
		transformer.transform(source, result);

		LOGGER.log(NLS.bind(ModelExportMessages.exportFileSaved,
				destinationFile.getPath()));
	}

	@Override
	public List<IWizardPage> getPages() {
		// No additional pages are needed for this extension
		return null;
	}

	@Override
	protected IStatus validateDestination(String destinationFilePath) {
		IStatus status = super.validateDestination(destinationFilePath);
		// Fix for CSN 0001291605 2014
		/*
		 * if (status == null) { status = new Status(IStatus.INFO, pluginId,
		 * Messages.odataV2Warning); }
		 */
		return status;
	}
}
