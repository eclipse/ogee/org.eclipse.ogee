/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.odatav2.converter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.ogee.export.messages.ModelExportMessages;
import org.eclipse.ogee.export.odatav2.Activator;
import org.eclipse.ogee.export.odatav2.api.IOdataV2EdmxConvert;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ODataV2EdmxConvert implements IOdataV2EdmxConvert {

	private static final String HTTP_WWW_W3_ORG_2000_XMLNS = "http://www.w3.org/2000/xmlns/"; //$NON-NLS-1$
	private static final String HTTP_SCHEMAS_MICROSOFT_METADATA = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"; //$NON-NLS-1$
	private static final String HTTP_SCHEMAS_MICROSOFT_COM = "http://schemas.microsoft.com/ado/2007/06/edmx"; //$NON-NLS-1$
	private static final String EDMX_SUF = ":Edmx"; //$NON-NLS-1$
	private static final Logger LOGGER = Activator.LOGGER;

	@Override
	public final Document createModel(final EDMXSet edmxSet) {
		// Create document
		final Document document = this.buildDocFactory();

		// Set EDMX header
		final Element dataService = this.setEdmxHeader(document);

		// Set schema
		SchemaConverterOdataV2 converterOdataV2 = new SchemaConverterOdataV2();
		// Create OData V2 factory
		final FactoryOdataV2 factoryOdataV2 = new FactoryOdataV2();
		converterOdataV2.createElement(factoryOdataV2, document, dataService,
				edmxSet);

		return document;

	}

	private Element setEdmxHeader(final Document document) {
		// Create root namespace element
		final Element rootTreeNode = createEdmxNameSpace(document);

		// Set root version
		setRootVersion(rootTreeNode);

		// Set root namespace
		setNS(rootTreeNode);

		document.appendChild(rootTreeNode);

		// Set data service node
		final Element dataService = setDataService(document, rootTreeNode);

		rootTreeNode.appendChild(dataService);

		return dataService;
	}

	private Document buildDocFactory() {

		Document document = null;
		DocumentBuilder documentBuilder;

		try {
			// Build document factory
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
					.newInstance();
			documentBuilderFactory.setFeature(
					"http://xml.org/sax/features/external-general-entities",
					false);
			documentBuilderFactory.setFeature(
					"http://xml.org/sax/features/external-parameter-entities",
					false);
			documentBuilderFactory.setFeature(
					"http://apache.org/xml/features/disallow-doctype-decl",
					true);
			documentBuilderFactory.setValidating(true);
			documentBuilderFactory.setNamespaceAware(true);
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
			document = documentBuilder.newDocument();
			document.setXmlStandalone(true);
		} catch (ParserConfigurationException e) {
			LOGGER.log(ModelExportMessages.errorCreatingDomElement);
		}
		return document;
	}

	private Element createEdmxNameSpace(final Document document) {
		return document.createElementNS(HTTP_SCHEMAS_MICROSOFT_COM,
				IAttribute.EDMX + EDMX_SUF);
	}

	private void setRootVersion(final Element rootTreeNode) {
		rootTreeNode.setAttribute(IAttribute.VERSION,
				IAttribute.VERSION_NUM_1_0);
	}

	private void setNS(final Element rootTreeNode) {
		rootTreeNode.setAttributeNS(HTTP_WWW_W3_ORG_2000_XMLNS,
				"xmlns:edmx", HTTP_SCHEMAS_MICROSOFT_COM); //$NON-NLS-1$
		rootTreeNode.setAttributeNS(HTTP_WWW_W3_ORG_2000_XMLNS,
				"xmlns:m", HTTP_SCHEMAS_MICROSOFT_METADATA); //$NON-NLS-1$
	}

	private static Element setDataService(final Document document,
			final Element rootTreeNode) {
		Element dataServices = document.createElement(IAttribute.EDMX
				+ ":DataServices"); //$NON-NLS-1$
		dataServices.setAttribute(
				"m:DataServiceVersion", IAttribute.VERSION_NUM_2_0); //$NON-NLS-1$
		rootTreeNode.appendChild(dataServices);
		return dataServices;
	}

}
