/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.patterns.exploration;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ogee.component.exploration.ExplorationComponent;
import org.eclipse.ogee.component.exploration.service.model.extensions.IServiceModel;
import org.eclipse.ogee.component.exploration.service.model.extensions.ServiceModelConstants;
import org.eclipse.ogee.core.extensions.components.Component;
import org.eclipse.ogee.core.extensions.patterns.Pattern;

/**
 * Represents an exploration pattern, made of Exploration Component.
 */
public class ExplorationPattern extends Pattern implements IServiceModel {
	protected String EXPLORATION_COMPONENT_ID = ExplorationComponent.COMPONENT_ID;

	@Override
	public List<IWizardPage> getWizardPages() {
		// get the Exploration component
		Component explorationComponent = this.components
				.get(this.EXPLORATION_COMPONENT_ID);

		// get the wizard pages from the Exploration component
		List<IWizardPage> explorationComponentWizardPages = explorationComponent
				.getWizardPages();

		return explorationComponentWizardPages;
	}

	/**
	 * Returns the model of this pattern by retrieving the model of its
	 * component.
	 */
	public EnumMap<ServiceModelConstants, Object> getServiceModel() {
		ExplorationComponent explorationComponent = (ExplorationComponent) this.components
				.get(this.EXPLORATION_COMPONENT_ID);

		return explorationComponent.getServiceModel();
	}

	@Override
	public void initialize(Map<String, Object> data) {
		ExplorationComponent explorationComponent = (ExplorationComponent) this.components
				.get(this.EXPLORATION_COMPONENT_ID);

		explorationComponent.initialize(data);
	}
}
