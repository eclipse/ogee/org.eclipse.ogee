/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.wizard.page;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ogee.export.Activator;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

public class ModelExportSourceSelectionValidator implements
		ISelectionStatusValidator {
	private ModelExportMainPage modelExportPage;

	public ModelExportSourceSelectionValidator(
			ModelExportMainPage modelExportPage) {
		this.modelExportPage = modelExportPage;
	}

	@Override
	public IStatus validate(Object[] selection) {
		IStatus status = null;
		// Validate the selected source in the browse window dialog
		String sourceFilePath = null;
		if (selection.length > 0) {
			IResource selectedResource = (IResource) selection[0];
			sourceFilePath = selectedResource.getFullPath().toOSString();
		}
		status = modelExportPage.getPageLogic().validateSource(sourceFilePath);
		if (status == null) {
			status = new Status(IStatus.OK, Activator.PLUGIN_ID, null);
		}
		return status;
	}
}
