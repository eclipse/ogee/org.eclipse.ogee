/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.wizard.page;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.ogee.export.messages.ModelExportMessages;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;


public class ModelExportSourceButtonListener implements SelectionListener {
	private ModelExportMainPage modelExportPage;

	public ModelExportSourceButtonListener(ModelExportMainPage modelExportPage) {
		this.modelExportPage = modelExportPage;
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		// Open the brows file dialog to select a source file
		ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
				this.modelExportPage.getShell(), new WorkbenchLabelProvider(),
				new BaseWorkbenchContentProvider());
		dialog.setTitle(ModelExportMessages.sourcePopUpTitle);
		dialog.setMessage(ModelExportMessages.sourcePopUpHeader);
		dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
		dialog.setHelpAvailable(false);

		ModelExportSourceSelectionValidator validator = new ModelExportSourceSelectionValidator(
				this.modelExportPage);
		dialog.setValidator(validator);
		int returnCode = dialog.open();

		if (returnCode == ElementTreeSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result != null && result.length >= 1) {
				IResource odataFile = (IResource) result[0];
				this.modelExportPage.setSourceFilePath(odataFile.getFullPath()
						.toOSString());
			}
		}
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// do nothing
	}
}
