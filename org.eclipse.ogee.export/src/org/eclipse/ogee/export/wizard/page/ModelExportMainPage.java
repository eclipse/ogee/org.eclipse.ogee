/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.wizard.page;

import java.net.URL;
import java.util.SortedSet;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.ogee.export.Activator;
import org.eclipse.ogee.export.messages.ModelExportMessages;
import org.eclipse.ogee.export.util.ModelExportException;
import org.eclipse.ogee.export.util.ui.IModelExportFirstPage;
import org.eclipse.ogee.export.wizard.IUiConstant;
import org.eclipse.ogee.export.wizard.ModelExportWizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ogee.help.IHelpConstants;

/**
 * ModelExportPage UI page
 */
public class ModelExportMainPage extends WizardPage implements
		IModelExportFirstPage {

	final private ModelExportPageLogic pageLogic;

	private Text sourceText;
	private Label sourceNameLabel;
	private Button sourceBrowseButton;

	private Combo destinationFormatCombo;
	private Text destinationFormatLabel;
	private Group destinationGroup;

	private IStatus pageStatus; // Status based on page UI elements
	private IStatus extensionStatus; // Status based on extension UI elements

	private int selectedDestinationFormatIndex;
	private static ImageDescriptor imageDescriptor = null;
	private Composite mainContainer;
	private Composite extensionCompositeUI;

	// Default destination format should be set to V2 - save V2 index for
	// default combo selection
	private int v2ExtensionIndex = 0;

	static {
		final URL imageUrl = Activator.getDefault().getBundle()
				.getEntry(IUiConstant.WIZARD_PAGE_BANNER_ICON);
		imageDescriptor = ImageDescriptor.createFromURL(imageUrl);
	}

	/**
	 * constructor - set page
	 */
	public ModelExportMainPage() {
		super(IUiConstant.PAGE_NAME, ModelExportMessages.exportPageHeader,
				imageDescriptor);
		setDescription(ModelExportMessages.exportPageDescription);
		this.selectedDestinationFormatIndex = -1;
		this.pageStatus = null;
		this.extensionStatus = null;
		this.pageLogic = new ModelExportPageLogic();
	}

	/**
	 * Create Page UI
	 */
	@Override
	public void createControl(final Composite parent) {
		this.mainContainer = new Composite(parent, SWT.NULL);
		createUi(this.mainContainer);
		setControl(this.mainContainer);
		validatePageOnCreation();
	}

	/**
	 * Set selected file in workspace
	 * 
	 * @param originSelection
	 *            selected file in the workspace
	 */
	public void setOriginSelection(final IStructuredSelection originSelection) {
		this.pageLogic.setOriginSelection(originSelection);
	}

	/**
	 * Convert the Source path in the workspace to the raw path in the file
	 * system
	 * 
	 * @return - raw source path
	 */
	public IPath getSourceRawFilePath() {
		final String sourceFilePath = getSourceFilePath();
		final IFile sourceFile = ResourcesPlugin.getWorkspace().getRoot()
				.getFile(new Path(sourceFilePath));

		return sourceFile.getFullPath();
	}

	@Override
	public void performHelp() {
		getShell().setData(IHelpConstants.ORG_ECLIPSE_UI_HELP, IHelpConstants.MODEL_EXPORT); //$NON-NLS-1$ //$NON-NLS-2$
		/*getShell().setData(
				IUiConstant.ORG_ECLIPSE_UI_HELP,
				IUiConstant.MODEL_EXPORT_PACKAGE
						+ IUiConstant.MODEL_EXPORT_HELP);*/
	}

	@Override
	public void setVisible(final boolean visible) {
		if (visible) {
			// Set the matching help context to the page when it's visible
						PlatformUI
								.getWorkbench()
								.getHelpSystem()
								.setHelp(
										getShell(),IHelpConstants.MODEL_EXPORT);
			
			
			/*PlatformUI
					.getWorkbench()
					.getHelpSystem()
					.setHelp(
							getShell(),
							IUiConstant.MODEL_EXPORT_PACKAGE
									+ IUiConstant.MODEL_EXPORT_HELP);*/
		}

		super.setVisible(visible);
	}

	@Override
	public ModelExportWizard getWizard() {
		return (ModelExportWizard) super.getWizard();
	}

	/**
	 * Update the page status based on validation performed in the extension UI
	 */
	public void updateExtensionValidations(final IStatus status) {
		this.extensionStatus = status;
		updatePage();
	}

	/**
	 * Update page status based on user inputs
	 */
	protected void updatePageValidations() {
		this.pageStatus = this.pageLogic.validatePage(getSourceFilePath(),
				selectedDestinationFormatIndex);
		updatePage();
	}

	protected ModelExportPageLogic getPageLogic() {
		return pageLogic;
	}

	protected void setSourceFilePath(final String sourceFilePath) {
		this.sourceText.setText(sourceFilePath);
	}

	/**
	 * User has selected a destination format from the combo
	 */
	private void destinationFormatSelected() {
		// Check if the destination format was changed
		if (selectedDestinationFormatIndex == this.destinationFormatCombo
				.getSelectionIndex()) {
			return;
		}
		// Update selected destination format index
		this.selectedDestinationFormatIndex = this.destinationFormatCombo
				.getSelectionIndex();
		// Update wizard with the new selected extension
		final String selectedDestinationFormatName = this.destinationFormatCombo
				.getItem(this.selectedDestinationFormatIndex);
		final IVisible visibleExtension = (IVisible) this.destinationFormatCombo
				.getData(selectedDestinationFormatName);
		getWizard().setSelectedDestinationFormat(visibleExtension.getId());
		// Clear error / warning messages if came from previous selected
		// extension
		this.extensionStatus = null;
		// Clear all UI elements from previous selected extension
		Control[] children = this.extensionCompositeUI.getChildren();
		for (Control control : children) {
			control.dispose();
		}
		// Create new UI elements from the new selected extension
		try {
			getWizard().createFirstPageUI(this.extensionCompositeUI,
					(IModelExportFirstPage) this);
		} catch (ModelExportException e) {
			// Do nothing
		}
		// Update description of the selected extension (Label under the Combo)
		this.destinationFormatLabel.setText(visibleExtension.getDescription());
		// Refresh UI to display the new components of the page
		if (extensionCompositeUI != null
				&& !this.extensionCompositeUI.isDisposed()) {
			this.extensionCompositeUI.layout();
		}
		this.destinationGroup.layout();
		this.mainContainer.layout();
		// Update page description from the extension
		String extensionPageDescription = getWizard().getPageDescription();
		if (extensionPageDescription == null) {
			// Set default page description if no description was defined by the
			// extension
			setDescription(ModelExportMessages.exportPageDescription);
		} else {
			setDescription(getWizard().getPageDescription());
		}
		// Update page status
		updatePageValidations();
	}

	/**
	 * Create UI for the page
	 * 
	 * @param parent
	 *            the main page composite
	 */
	private void createUi(Composite parent) {
		GridLayout gridLayout = new GridLayout(1, false);
		parent.setLayout(gridLayout);

		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1);
		gridData.grabExcessHorizontalSpace = true;
		parent.setLayoutData(gridData);

		createSource(parent);
		createDestinationGroup(parent);
	}

	/**
	 * Create UI for the destination input part - destination group, combo,
	 * label, extension part.
	 * 
	 * @param parent
	 *            the parent composite
	 */
	private void createDestinationGroup(Composite parent) {
		this.destinationGroup = new Group(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		this.destinationGroup.setLayout(layout);
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		this.destinationGroup.setLayoutData(gridData);
		this.destinationGroup
				.setText(ModelExportMessages.exportPageLableDestinationFormat);
		createDestinationFormat(this.destinationGroup);
		this.extensionCompositeUI = new Composite(this.destinationGroup,
				SWT.NONE);
		// Set default layout
		this.extensionCompositeUI.setLayout(new GridLayout());
		// Default destination format should be set to V2 - save V2 index for
		// default combo selection
		this.destinationFormatCombo.select(this.v2ExtensionIndex);
		this.destinationFormatCombo.notifyListeners(SWT.Selection, new Event());
	}

	/**
	 * Validate the page upon its creation
	 */
	private void validatePageOnCreation() {
		this.pageStatus = this.pageLogic.validatePage(getSourceFilePath(),
				this.selectedDestinationFormatIndex);
		// Update messages and "Finish" button availability
		updatePage();
		// To prevent initiate the wizard in an error state
		setMessage(null);
	}

	/**
	 * Create UI for the source input text
	 * 
	 * @param parent
	 *            the parent composite
	 */
	private void createSource(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);

		GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.marginWidth = 0;
		container.setLayout(gridLayout);

		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1);
		gridData.grabExcessHorizontalSpace = true;
		container.setLayoutData(gridData);

		// create Source label
		this.sourceNameLabel = new Label(container, SWT.NONE);
		this.sourceNameLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				false, false, 1, 1));
		this.sourceNameLabel
				.setText(ModelExportMessages.exportPageLableSourceFile);

		// create Source text
		this.sourceText = new Text(container, SWT.BORDER);
		this.sourceText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		// Set the default source path if exist
		String defaultSourceFilePath = pageLogic.getDefaultSourceFilePath();
		if (defaultSourceFilePath != null) {
			this.sourceText.setText(defaultSourceFilePath);
		}
		this.sourceText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				updatePageValidations();

			}
		});

		// Source file name browse button
		this.sourceBrowseButton = new Button(container, SWT.PUSH);
		this.sourceBrowseButton.setLayoutData(new GridData(SWT.RIGHT,
				SWT.CENTER, false, false, 1, 1));
		this.sourceBrowseButton
				.setText(ModelExportMessages.browseDestinationButtonText);
		ModelExportSourceButtonListener sourceBrowseListener = new ModelExportSourceButtonListener(
				this);
		this.sourceBrowseButton.addSelectionListener(sourceBrowseListener);
	}

	/**
	 * Create UI for the destination format combo and a text label under it + an
	 * empty line
	 * 
	 * @param parent
	 *            the parent composite
	 */
	private void createDestinationFormat(Composite parent) {
		// Create destination format combo
		this.destinationFormatCombo = new Combo(parent, SWT.DROP_DOWN
				| SWT.BORDER | SWT.SINGLE | SWT.READ_ONLY);
		this.destinationFormatCombo.setLayoutData(new GridData(
				GridData.FILL_HORIZONTAL));
		this.destinationFormatCombo
				.setToolTipText(ModelExportMessages.exportPageDestinationFormatToolTip);

		// Set destination Format
		SortedSet<? extends IVisible> visibleExtensions = getWizard()
				.getVisibleExtensions();
		int loopIndex = 0;
		for (IVisible visibleExtension : visibleExtensions) {
			String extensionId = visibleExtension.getId();
			if (extensionId.equals(IUiConstant.V2_EXTENSION_ID)) {
				// Default destination format should be set to V2 - save V2
				// index for default combo selection
				this.v2ExtensionIndex = loopIndex;
			}
			String formatDisplayName = visibleExtension.getDisplayName();
			this.destinationFormatCombo.add(formatDisplayName);
			this.destinationFormatCombo.setData(formatDisplayName,
					visibleExtension);
			loopIndex++;
		}

		this.destinationFormatCombo
				.addSelectionListener(new SelectionListener() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						destinationFormatSelected();
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent arg0) {
						// Do nothing
					}
				});

		this.destinationFormatLabel = new Text(parent, SWT.READ_ONLY
				| SWT.MULTI | SWT.WRAP);
		this.destinationFormatLabel.setLayoutData(new GridData(
				GridData.FILL_HORIZONTAL));
		// Add an empty line under the combo text
		new Label(parent, SWT.NONE);
	}

	/**
	 * Update messages and Finish button availability
	 */
	private void updatePage() {
		boolean isPageValid = true;

		// Get message to display
		IStatus status = this.pageLogic.getMessageToDisplay(this.pageStatus,
				this.extensionStatus);
		if (status == null) {
			setMessage(null);
		} else {
			if (status.getSeverity() == IStatus.ERROR) {
				isPageValid = false;
			}
			setMessage(status.getMessage(),
					convertMessageSeverity(status.getSeverity()));
		}
		setPageComplete(isPageValid);
	}

	/**
	 * Convert severity status of type org.eclipse.core.runtime.IStatus to type
	 * org.eclipse.jface.dialogs.IMessageProvider
	 * 
	 * @param statusSeverity
	 *            of type org.eclipse.core.runtime.IStatus
	 * 
	 * @return severity status of type
	 *         org.eclipse.jface.dialogs.IMessageProvider
	 */
	private int convertMessageSeverity(int statusSeverity) {
		int messageProviderSeverity;
		switch (statusSeverity) {
		case IStatus.ERROR:
			messageProviderSeverity = ERROR;
			break;
		case IStatus.WARNING:
			messageProviderSeverity = WARNING;
			break;
		case IStatus.INFO:
			messageProviderSeverity = INFORMATION;
			break;
		default:
			messageProviderSeverity = NONE;
			break;
		}
		return messageProviderSeverity;
	}

	/**
	 * Get source file path from the source Text UI element
	 */
	public String getSourceFilePath() {
		return this.sourceText.getText().trim();
	}
}
