/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.wizard.page;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ogee.export.Activator;
import org.eclipse.ogee.export.messages.ModelExportMessages;
import org.eclipse.osgi.util.NLS;

/**
 * Process the page logic( validation)
 */
public class ModelExportPageLogic {
	private IStructuredSelection originSelection;
	private String defaultSourceFilePath = null;

	/**
	 * Set the selected file in the workspace
	 * 
	 * @param originSelection
	 */
	public void setOriginSelection(IStructuredSelection originSelection) {
		this.originSelection = originSelection;
	}

	/**
	 * Validate source and destination format from the model export page based
	 * on user inputs
	 */
	public IStatus validatePage(String sourceFilePath,
			int selectedDestinationFormatIndex) {
		IStatus status = null;

		if (selectedDestinationFormatIndex == -1) {
			// Select export format
			status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					ModelExportMessages.exportValidationsFormatError01);
		} else {
			status = validateSource(sourceFilePath);
		}
		return status;
	}

	/**
	 * Validate source path based on user input
	 */
	public IStatus validateSource(String sourceFilePath) {
		IStatus status = null;

		if (sourceFilePath == null || sourceFilePath.isEmpty()) {
			// Select model file of type *.odata
			status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					ModelExportMessages.exportValidationsSourceError01);
		} else if (!sourceFilePath.endsWith(".odata")) //$NON-NLS-1$
		{
			// Model file must be of type *.odata
			status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					ModelExportMessages.exportValidationsSourceError02);
		} else {
			// Check that the source file exists
			if (!ResourcesPlugin.getWorkspace().getRoot()
					.exists(new Path(sourceFilePath))) {
				// Model file {0} does not exist
				status = new Status(
						IStatus.ERROR,
						Activator.PLUGIN_ID,
						NLS.bind(
								ModelExportMessages.exportValidationsSourceError03,
								sourceFilePath));
			}
		}
		return status;
	}

	/**
	 * Get the message to display based on messages from the page validations
	 * and the extension UI validations.
	 * 
	 * @param pageValidationStatus
	 *            - status based on the page validations
	 * @param extensionValidationStatus
	 *            - status based on the extension validations
	 * @return - status to display in the wizard page
	 */
	public IStatus getMessageToDisplay(IStatus pageValidationStatus,
			IStatus extensionValidationStatus) {
		IStatus status = null;
		// ModelExportMessage message = new ModelExportMessage();
		if (pageValidationStatus != null && extensionValidationStatus != null) {
			if (pageValidationStatus.getSeverity() == IStatus.ERROR)
				status = pageValidationStatus;
			else {
				status = extensionValidationStatus;
			}
		} else {
			if (pageValidationStatus != null) {
				status = pageValidationStatus;
			} else if (extensionValidationStatus != null) {
				status = extensionValidationStatus;
			}
		}
		return status;
	}

	/**
	 * Build the default source path based on the selected model file. If there
	 * is no selected model file - no default is set
	 * 
	 * @return
	 */
	public String getDefaultSourceFilePath() {
		// Build the source default path
		IFile projectFile = extractProjectFileFromSelection(this.originSelection);

		if (projectFile != null) {
			// Build the source default path
			this.defaultSourceFilePath = projectFile.getFullPath().toOSString();

		}
		return this.defaultSourceFilePath;
	}

	private IFile extractProjectFileFromSelection(
			IStructuredSelection originSelection) {
		IFile projectFile = null;
		if (originSelection == null)
			return projectFile;
		if (originSelection instanceof TreeSelection) {
			TreeSelection treeSelection = (TreeSelection) originSelection;
			Object firstElement = treeSelection.getFirstElement();
			if (firstElement == null)
				return projectFile;
			projectFile = (IFile) Platform.getAdapterManager().getAdapter(
					firstElement, IFile.class);
			if (projectFile == null) {
				if (firstElement instanceof IAdaptable)
					projectFile = (IFile) ((IAdaptable) firstElement)
							.getAdapter(IFile.class);
			}
		}
		return projectFile;
	}

}
