/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.wizard;

import java.util.List;
import java.util.SortedSet;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.ogee.core.wizard.FrwkAbstractWizard;
import org.eclipse.ogee.export.Activator;
import org.eclipse.ogee.export.extensionpoint.ModelExportExtensionsManager;
import org.eclipse.ogee.export.messages.ModelExportMessages;
import org.eclipse.ogee.export.util.ModelExportException;
import org.eclipse.ogee.export.util.ui.IModelExportFirstPage;
import org.eclipse.ogee.export.wizard.page.ModelExportMainPage;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IValidator;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.ResourceUtil;

/**
 * Export wizard class which control the UI logic and call the extension point.
 * at the end
 */
public class ModelExportWizard extends FrwkAbstractWizard implements
		IExportWizard {
	/**
	 * Selected folder or file
	 */
	private IStructuredSelection folderSelection;
	private ModelExportExtensionsManager extensionsManager;

	/**
	 * Constructor
	 */
	public ModelExportWizard() {
		super();
		setWindowTitle(ModelExportMessages.exportWizardTitle);
		setNeedsProgressMonitor(false);
	}

	@Override
	public void init(final IWorkbench workbench,
			final IStructuredSelection originSelection) {
		// Initiate the wizard main properties - this method is called from the
		// Wizard
		this.extensionsManager = ModelExportExtensionsManager.getInstance();
		this.folderSelection = originSelection;
	}

	/**
	 * Return the Wizard ID
	 */
	public static String getWizardId() {
		return IUiConstant.MODEL_EXPORT_PACKAGE + IUiConstant.WIZARD_ID;
	}

	@Override
	public void addPages() {
		final ModelExportMainPage modelExportPage = new ModelExportMainPage();
		modelExportPage.setOriginSelection(this.folderSelection);
		addPage(modelExportPage);
	}

	/**
	 * When user clicks the finish button the method gets the EDMX set of the
	 * selected file from the domain model, checks if the model is modified or
	 * not. If checks pass the method calls the selected extension based on the
	 * selected Destination format with the EDMX set. A model validity check is
	 * also performed and a warning is displayed in case the model is not valid.
	 */
	@Override
	public boolean performFinish() {
		boolean closeWizard = true; // Indicates if wizard should be closed
		boolean actionCanceled = false;
		EDMXSet edmxSet = null;
		// Get wizard main page
		final ModelExportMainPage page = (ModelExportMainPage) getPage(IUiConstant.PAGE_NAME);
		// Get selected OData model path, file and URI
		final IPath sourceFilePath = page.getSourceRawFilePath();
		final IFile file = ResourcesPlugin.getWorkspace().getRoot()
				.getFile(sourceFilePath);
		final URI uri = URI.createPlatformResourceURI(file.getFullPath()
				.toOSString(), true);
		// Get workbench data
		final IWorkbenchPage workbenchPage = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		final IEditorPart part = ResourceUtil.findEditor(workbenchPage, file);

		// Check if the model was modified
		if (part != null && part.isDirty()) {
			// In case the model was modified - ask user what to do - save
			// the model before export or export the last saved model or
			// cancel the operation.
			final int result = callSaveDialog(file.getName(), getShell());
			switch (result) {
			case 0: // Yes - save the file and export the saved file
				part.doSave(null);
				break;
			case 1: // No - don't save and export the last saved model
				break;
			default: // Cancel action - go back to the wizard
				closeWizard = false;
				actionCanceled = true;
			}
		}
		if (!actionCanceled) {
			// Get the EDMX set
			edmxSet = getEDMXSetFromUri(uri);
			final String fileName = file.getName();
			// Check if the model is valid
			final boolean validModel = checkModelValidity(edmxSet, fileName);
			if (validModel) {
				try {
					// Call the extension point handler to perform the export
					this.extensionsManager.performFinish(edmxSet);
					// Free all members of the wizard extension class
					this.extensionsManager.clear();
				} catch (ModelExportException e) {
					// Export failed - display an error message
					showError(e);
					closeWizard = false;
				}
			} else {
				final WizardPage currentPage = (WizardPage) getContainer()
						.getCurrentPage();
				// Set the error message to the wizard header
				if (currentPage != null) {
					final String message = NLS.bind(
							ModelExportMessages.exportWizardError01, fileName);
					currentPage.setMessage(message, IMessageProvider.ERROR);
				}
				closeWizard = false;
			}
		}

		return closeWizard;
	}

	public SortedSet<? extends IVisible> getVisibleExtensions() {
		return extensionsManager.getVisibleExtensions();
	}

	public void setSelectedDestinationFormat(final String selectedExtensionId) {
		List<IWizardPage> extensionPages;
		this.extensionsManager.setSelectedExtension(selectedExtensionId);
		try {
			extensionPages = this.extensionsManager.getExtensionPages();
			// remove old pages from the specified page index
			removePages(1);
			// Add wizard pages from the extension
			if (extensionPages != null) {
				for (IWizardPage page : extensionPages) {
					addPage(page);
				}
			}
		} catch (ModelExportException e) {
			showError(e);
		}
	}

	public void createFirstPageUI(final Composite parent,
			final IModelExportFirstPage wizardPage) throws ModelExportException {
		this.extensionsManager.createFirstPageUI(parent, wizardPage);
	}

	/*
	 * Display the errors on the wizard
	 * 
	 * @param exception - the exception which hold the text we would like to
	 * display on the wizard header
	 */
	private void showError(final Throwable exception) {
		// Add the exception to the error log
		Logger.getFrameworkLogger().logError(exception.getMessage(), exception);

		final WizardPage currentPage = (WizardPage) getContainer()
				.getCurrentPage();
		// Set the error message to the wizard header
		if (currentPage != null) {
			currentPage.setMessage(ModelExportMessages.exportWizardError01,
					IMessageProvider.ERROR);
		}
	}

	/**
	 * Check if the model is valid. If not - add a message to the error log
	 * 
	 * @param edmxSet
	 *            - model EDMX set
	 * @param fileName
	 *            - model file name
	 */
	private static boolean checkModelValidity(final EDMXSet edmxSet,
			final String fileName) {

		boolean validModel = true;

		final IValidator validator = IModelContext.INSTANCE.createValidator();
		final Diagnostic diagnostic = validator.validate(edmxSet);
		if (diagnostic.getSeverity() == Diagnostic.ERROR) {
			validModel = false;
			final String message = NLS.bind(
					ModelExportMessages.notValidWarningMessage, fileName);
			Logger.getLogger(Activator.PLUGIN_ID).logError(message);
		}

		return validModel;
	}

	private static EDMXSet getEDMXSetFromUri(final URI uri) {
		EDMXSet edmxSet = null;
		final ResourceSet currResourceSet = new ResourceSetImpl();
		final Resource resource = currResourceSet.getResource(uri, true);
		final EList<EObject> resourceContents = resource.getContents();
		for (EObject resourceContent : resourceContents) {
			if (resourceContent instanceof EDMXSet) {
				edmxSet = (EDMXSet) resourceContent;
				break;
			}
		}
		return edmxSet;
	}

	private static int callSaveDialog(final String fileName, final Shell shell) {
		final String[] buttonTexts = new String[] {
				ModelExportMessages.saveDialogYesButton,
				ModelExportMessages.saveDialogNoButton,
				ModelExportMessages.saveDialogCancelButton };
		final MessageDialog dialog = new MessageDialog(shell,
				ModelExportMessages.saveDialogTitle, null, NLS.bind(
						ModelExportMessages.saveDialogMessage, fileName),
				MessageDialog.CONFIRM, buttonTexts, 0);
		return dialog.open();
	}

	/**
	 * Get extension description to be displayed under the page title
	 * 
	 * @return - page description
	 */
	public String getPageDescription() {
		return this.extensionsManager.getPageDescription();
	}

	@Override
	public boolean performCancel() {
		this.extensionsManager.clear();
		return super.performCancel();
	}

}
