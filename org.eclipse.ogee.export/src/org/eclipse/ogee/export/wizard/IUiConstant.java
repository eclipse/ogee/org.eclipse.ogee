/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * contant for the model export UI 
 */
package org.eclipse.ogee.export.wizard;

/**
 * Constants for Wizard UI
 */
public interface IUiConstant {
	public static final String WIZARD_ID = ".wizard"; //$NON-NLS-1$
	public static final String ORG_ECLIPSE_UI_HELP = "org.eclipse.ui.help"; //$NON-NLS-1$
	public static final String MODEL_EXPORT_PACKAGE = "org.eclipse.ogee.export"; //$NON-NLS-1$
	public static final String MODEL_EXPORT_HELP = ".d7397d6fe51b47b0925c37da30564aa1"; //$NON-NLS-1$
	public static final String PAGE_NAME = "ExportModelProjectPage"; //$NON-NLS-1$
	public static final String WIZARD_PAGE_BANNER_ICON = "res/images/WIZARD_BANNER_ICON.gif"; //$NON-NLS-1$	
	public static final String V2_EXTENSION_ID = "org.eclipse.ogee.export.odatav2"; //$NON-NLS-1$
	public static final String V3_EXTENSION_ID = "org.eclipse.ogee.export.odatav3"; //$NON-NLS-1$

}
