/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.messages;

import org.eclipse.osgi.util.NLS;

public final class ModelExportMessages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.ogee.export.messages.ModelExport"; //$NON-NLS-1$
	public static String browseButtonText;
	public static String browseDestinationButtonText;
	public static String destinationPopUpTitle;
	public static String exportExtensionError01;
	public static String exportExtensionError02;
	public static String exportPageDescription;
	public static String exportPageDestinationFormatToolTip;
	public static String exportPageHeader;
	public static String exportPageLableDestinationFile;
	public static String exportPageLableDestinationFormat;
	public static String exportPageLableSourceFile;
	public static String exportWizardError01;
	public static String exportWizardTitle;
	public static String exportValidationsDestinationError02;
	public static String exportValidationsDestinationError03;
	public static String exportValidationsDestinationError04;
	public static String exportValidationsDestinationError05;
	public static String exportValidationsDestinationWarning01;
	public static String exportValidationsSourceError01;
	public static String exportValidationsSourceError02;
	public static String exportValidationsSourceError03;
	public static String exportValidationsFormatError01;
	public static String sourcePopUpHeader;
	public static String sourcePopUpTitle;
	public static String saveDialogYesButton;
	public static String saveDialogNoButton;
	public static String saveDialogCancelButton;
	public static String saveDialogTitle;
	public static String saveDialogMessage;
	public static String notValidWarningMessage;
	public static String exportFileSaved;
	public static String errorCreatingDomElement;
	public static String exportValidationsDestinationError;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, ModelExportMessages.class);
	}

	private ModelExportMessages() {
	}
}
