/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.extensionpoint;

import java.net.URL;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.ogee.core.extensions.Extension;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.ogee.core.extensions.visible.VisibleExtension;
import org.eclipse.ogee.export.messages.ModelExportMessages;
import org.eclipse.ogee.export.util.ModelExportException;
import org.eclipse.ogee.export.util.extensionpoint.IModelExportProvider;
import org.eclipse.osgi.util.NLS;

public class ModelExportExtension extends Extension implements IVisible {
	private IVisible visibleExtension;
	private IModelExportProvider modelExportProvider;
	private static final String EXTENSTION_ATTRIBUTE = "ModelExportProvider"; //$NON-NLS-1$

	public ModelExportExtension(IConfigurationElement configElement)
			throws ExtensionException {
		super(configElement);
		this.visibleExtension = new VisibleExtension(configElement, this.id);
	}

	/**
	 * Get the Model Export Implementation.
	 * 
	 * @return ModelExportCreator
	 * @throws ExtensionException
	 */
	public IModelExportProvider getModelExportProvider()
			throws ModelExportException {
		// only creates it once
		if (this.modelExportProvider == null) {
			try {
				// creates and returns a new instance of the executable
				// extension
				// identified by the odata model name
				Object obj = this.configElement
						.createExecutableExtension(EXTENSTION_ATTRIBUTE);
				if (obj == null) {
					throw new ModelExportException(NLS.bind(
							ModelExportMessages.exportExtensionError01,
							this.visibleExtension.getDisplayName()));

				}

				if (obj instanceof IModelExportProvider) {
					this.modelExportProvider = (IModelExportProvider) obj;
					return this.modelExportProvider;
				}

				throw new ModelExportException(NLS.bind(
						ModelExportMessages.exportExtensionError01,
						this.visibleExtension.getDisplayName()));
			} catch (CoreException e) {
				throw new ModelExportException(e);

			}
		}

		return this.modelExportProvider;

	}

	@Override
	public int compareTo(IVisible o) {
		return this.visibleExtension.compareTo(o);
	}

	@Override
	public String getDisplayName() {
		return this.visibleExtension.getDisplayName();
	}

	@Override
	public URL getIcon() {
		return this.visibleExtension.getIcon();
	}

	@Override
	public String getDescription() {
		return this.visibleExtension.getDescription();
	}

	@Override
	public String toString() {
		return super.toString() + this.visibleExtension.toString();
	}
}
