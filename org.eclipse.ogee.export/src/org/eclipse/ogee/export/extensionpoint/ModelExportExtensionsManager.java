/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.extensionpoint;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.export.Activator;
import org.eclipse.ogee.export.messages.ModelExportMessages;
import org.eclipse.ogee.export.util.ModelExportException;
import org.eclipse.ogee.export.util.extensionpoint.IModelExportProvider;
import org.eclipse.ogee.export.util.ui.IModelExportFirstPage;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Composite;

/**
 * The Extension point handler class responsible to handle the calls to the
 * specific extension.
 */
public final class ModelExportExtensionsManager {

	private static final String NEW_LINE = "\n"; //$NON-NLS-1$
	private static ModelExportExtensionsManager instance = null;
	private static final String MODEL_EXPORT_EXTENSION_POINT_ID = ".modelExport"; //$NON-NLS-1$ 
	private IConfigurationElement[] configElements;
	private Set<ModelExportExtension> modelExportExtensions;
	private ModelExportExtension selectedModelExportExtension;
	private IModelExportProvider selectedModelExportProvider;

	private ModelExportExtensionsManager() throws ModelExportException {
		this.modelExportExtensions = new HashSet<ModelExportExtension>();
		// Retrieves the extensions from the plugin.xml
		this.configElements = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(
						Activator.PLUGIN_ID + MODEL_EXPORT_EXTENSION_POINT_ID);

		for (IConfigurationElement configElement : this.configElements) {
			try {
				ModelExportExtension modelExportExtension = new ModelExportExtension(
						configElement);
				this.modelExportExtensions.add(modelExportExtension);
			} catch (ExtensionException e) {
				String id = configElement.getAttribute(ExtensionAttributes.id
						.name());
				Logger.getFrameworkLogger()
						.logWarning(
								e.getMessage()
										+ NEW_LINE
										+ NLS.bind(
												FrameworkMessages.ServiceImplementationExtensionsManager_1,
												id));
			}
		}
	}

	/**
	 * ModelExportExtensionPointHandler - Get singleton instance
	 */
	public static ModelExportExtensionsManager getInstance() {
		if (instance == null) {
			try {
				instance = new ModelExportExtensionsManager();
			} catch (ModelExportException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instance;
	}

	/**
	 * Destroy the singleton instance
	 */
	public static void destroyInstance() {
		instance = null;
	}

	public void setSelectedExtension(String selectedExtensionId) {
		this.selectedModelExportExtension = null;
		this.selectedModelExportProvider = null;
		for (ModelExportExtension modelExportExtension : this.modelExportExtensions) {
			if (modelExportExtension.getId().equals(selectedExtensionId)) {
				this.selectedModelExportExtension = modelExportExtension;
				try {
					this.selectedModelExportProvider = this.selectedModelExportExtension
							.getModelExportProvider();
					break;
				} catch (ModelExportException e) {
					// Do nothing
				}
			}
		}
	}

	/**
	 * Execute - run the export model for the specific extension
	 * 
	 * @param edmxSet
	 *            - model root ( domain model)
	 * @param destinationFile
	 *            - destination file
	 * @throws ModelExportException
	 */
	public void performFinish(EDMXSet edmxSet) throws ModelExportException {
		if (this.selectedModelExportExtension == null
				|| this.selectedModelExportProvider == null) {
			throw new ModelExportException(
					ModelExportMessages.exportExtensionError02);
		}
		this.selectedModelExportProvider.performFinish(edmxSet);
	}

	/**
	 * Clear data after the wizard is closed
	 */
	public void clear() {
		destroyInstance();
		this.selectedModelExportProvider.clear();
	}

	public SortedSet<? extends IVisible> getVisibleExtensions() {
		// we sort the set only when the user requests it from the UI
		SortedSet<? extends IVisible> sortedModelExportExtensions = new TreeSet<ModelExportExtension>(
				this.modelExportExtensions);
		return sortedModelExportExtensions;
	}

	public void createFirstPageUI(final Composite parent,
			final IModelExportFirstPage wizardPage) throws ModelExportException {
		if (this.selectedModelExportProvider == null) {
			throw new ModelExportException(NLS.bind(
					ModelExportMessages.exportExtensionError01, "")); //$NON-NLS-1$
		}

		this.selectedModelExportProvider.createFirstPageUI(parent, wizardPage);
	}

	public List<IWizardPage> getExtensionPages() throws ModelExportException {
		List<IWizardPage> wizardPages = new ArrayList<IWizardPage>();
		if (this.selectedModelExportProvider == null) {
			throw new ModelExportException(NLS.bind(
					ModelExportMessages.exportExtensionError01, "")); //$NON-NLS-1$
		}
		wizardPages = this.selectedModelExportProvider.getPages();

		return wizardPages;
	}

	/**
	 * Get extension description to be displayed under the page title
	 * 
	 * @return - page description
	 */
	public String getPageDescription() {
		return this.selectedModelExportProvider.getPageDescription();
	}
}