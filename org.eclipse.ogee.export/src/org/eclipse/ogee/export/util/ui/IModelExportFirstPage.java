/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.widgets.Shell;

/**
 * Expose specific parts of the wizard page to be used in interface
 * org.eclipse.ogee.core
 * .model.export.util.extensionpoint.IModelExportProvider, when implementing
 * extension point modelExport. Create the UI elements and validations for the
 * first page of the export wizard.
 * 
 */
public interface IModelExportFirstPage {
	/**
	 * This method sets the message and the isPageComplete flag of the first
	 * page of the wizard based on the status that is sent. This method should
	 * be used after performing validations on user input on the UI objects
	 * added to the first page of the wizard.
	 * 
	 * @param status
	 *            of the user input
	 */
	public void updateExtensionValidations(IStatus status);

	/**
	 * Returns the shell of the container of the wizard page.
	 * 
	 * @return the shell
	 */
	public Shell getShell();

	/**
	 * Returns the source file path as modified by the user.
	 * 
	 * @return the source file path
	 */
	public String getSourceFilePath();
}
