/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.ui;

import java.io.File;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ogee.export.messages.ModelExportMessages;
import org.eclipse.ogee.export.util.extensionpoint.IModelExportProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * This class implements a specific UI and validations for the first page of the
 * wizard under the export format combo. The UI is a label "Export As:", a Text
 * field to insert a destination XML file and a brows button.
 * 
 * 
 */
public abstract class ModelExportAbstractUiProvider implements
		IModelExportProvider {
	protected Text destinationText;
	protected String pluginId;
	private Label destinationNameLabel;
	private Button destinationBrowseButton;
	private IModelExportFirstPage wizardPage;
	static String defaultDestinationFilePath;

	/**
	 * Constructor
	 * 
	 * @param pluginId
	 *            The ID of the specific plugin. Needed for the Status returned
	 *            in the validations
	 */
	public ModelExportAbstractUiProvider(final String pluginId) {
		this.pluginId = pluginId;
	}

	@Override
	public void createFirstPageUI(final Composite extensionCompositeUI,
			final IModelExportFirstPage wizardPage) {
		this.wizardPage = wizardPage;
		final GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.marginWidth = 0;
		extensionCompositeUI.setLayout(gridLayout);

		final GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		gridData.grabExcessHorizontalSpace = true;
		extensionCompositeUI.setLayoutData(gridData);

		// create Destination label
		this.destinationNameLabel = new Label(extensionCompositeUI, SWT.NONE);
		this.destinationNameLabel.setLayoutData(new GridData(SWT.FILL,
				SWT.CENTER, false, false, 1, 1));
		this.destinationNameLabel
				.setText(ModelExportMessages.exportPageLableDestinationFile);

		// create Destination text
		this.destinationText = new Text(extensionCompositeUI, SWT.BORDER);
		this.destinationText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));
		if (ModelExportAbstractUiProvider.defaultDestinationFilePath != null) {
			this.destinationText
					.setText(ModelExportAbstractUiProvider.defaultDestinationFilePath);
		}
		this.destinationText.setEditable(true);
		// Set last destination file path if it was set
		if (ModelExportAbstractUiProvider.defaultDestinationFilePath != null) {
			this.destinationText
					.setText(ModelExportAbstractUiProvider.defaultDestinationFilePath);
		}
		this.destinationText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent exception) {
				modifyDestinationText();
			}
		});

		// Destination file name browse button
		this.destinationBrowseButton = new Button(extensionCompositeUI,
				SWT.PUSH);
		this.destinationBrowseButton.setLayoutData(new GridData(SWT.RIGHT,
				SWT.CENTER, false, false, 1, 1));
		this.destinationBrowseButton
				.setText(ModelExportMessages.browseButtonText);
		this.destinationBrowseButton
				.addSelectionListener(new SelectionListener() {
					@Override
					public void widgetDefaultSelected(final SelectionEvent arg0) {
						// do nothing
					}

					@Override
					public void widgetSelected(final SelectionEvent arg0) {
						browseDestination(getDestinationFilePath());
					}
				});
		// Validate destination on creation
		this.wizardPage
				.updateExtensionValidations(validateDestination(getDestinationFilePath()));
	}

	public abstract List<IWizardPage> getPages();

	/**
	 * Validate the destination text.
	 * 
	 * @param destinationFilePath
	 *            destination file path from the destination text element
	 * @return the result of the validations
	 */
	protected IStatus validateDestination(final String destinationFilePath) {
		IStatus status = null;
		// Validations for destination file
		if (destinationFilePath == null || destinationFilePath.isEmpty()) {
			// Select destination file of type *.xml
			status = new Status(IStatus.ERROR, pluginId,
					ModelExportMessages.exportValidationsDestinationError04);
		} else if (!destinationFilePath.endsWith(".xml")) //$NON-NLS-1$
		{
			// Destination file must be of type *.xml
			status = new Status(IStatus.ERROR, pluginId,
					ModelExportMessages.exportValidationsDestinationError03);
		} else {
			final File destFile = new File(destinationFilePath);
			if (destFile.exists()) {
				if (destFile.canWrite()) {
					// Destination file {0} already exists. File will be
					// overwritten
					status = new Status(
							IStatus.WARNING,
							pluginId,
							NLS.bind(
									ModelExportMessages.exportValidationsDestinationWarning01,
									destFile.getName()));
				} else {
					// Destination file {0} is not writable
					status = new Status(
							IStatus.ERROR,
							pluginId,
							NLS.bind(
									ModelExportMessages.exportValidationsDestinationError05,
									destFile.getName()));
				}
			} else {
				// In case the destination file does not exist - validations for
				// the folder of the file
				final String destinationFolderPath = destFile.getParent();
				if (destinationFolderPath == null
						|| destinationFolderPath.isEmpty()
						|| destinationFolderPath.equals(File.separator)) {
					// Destination folder {0} does not exist
					status = new Status(
							IStatus.ERROR,
							pluginId,
							NLS.bind(
									ModelExportMessages.exportValidationsDestinationError02,
									destinationFilePath));
				} else {
					final File destinationFolder = new File(
							destinationFolderPath);
					if (!destinationFolder.isDirectory()) {
						// Destination folder {0} does not exist
						status = new Status(
								IStatus.ERROR,
								pluginId,
								NLS.bind(
										ModelExportMessages.exportValidationsDestinationError02,
										destinationFolder));
					}
				}
			}
		}
		return status;
	}

	private void browseDestination(final String destinationFilePath) {
		final FileDialog dialog = new FileDialog(this.wizardPage.getShell(),
				SWT.SAVE);
		dialog.setText(ModelExportMessages.destinationPopUpTitle);
		dialog.setFileName(destinationFilePath);
		dialog.setFilterPath(destinationFilePath);
		final String[] filterExt = { "*.xml" }; //$NON-NLS-1$
		dialog.setFilterExtensions(filterExt);

		// Set the default destination file name as the .odata source file name
		final String fileName = getDefaultDestinationFileName();
		if (fileName != null) {
			dialog.setFileName(fileName);
		}
		final String folderpath = dialog.open();
		if (folderpath != null) {
			this.destinationText.setText(folderpath);
		}
	}

	private String getDefaultDestinationFileName() {
		final String currentDestinationFilePath = this.destinationText
				.getText();
		final String sourceFilePath = this.wizardPage.getSourceFilePath();
		final IFile sourceFile = ResourcesPlugin.getWorkspace().getRoot()
				.getFile(new Path(sourceFilePath));
		String fileName = null;
		// If currentFilePath exists - don't give any default proposal.
		// The default destination file name is taken from the source odata file
		// name
		if ((currentDestinationFilePath == null || currentDestinationFilePath
				.isEmpty())
				&& sourceFile != null
				&& sourceFilePath.endsWith(".odata")) { //$NON-NLS-1$
			final String fileNames[] = sourceFile.getName().split(".odata"); //$NON-NLS-1$
			fileName = fileNames[0];
		}
		return fileName;
	}

	private String getDestinationFilePath() {
		return this.destinationText.getText();
	}

	private void modifyDestinationText() {
		final String destinationFilePath = getDestinationFilePath();
		// Save the path for next call of the wizard
		ModelExportAbstractUiProvider.defaultDestinationFilePath = destinationFilePath;
		// Call validations of the path
		this.wizardPage
				.updateExtensionValidations(validateDestination(destinationFilePath));
	}

	@Override
	public void clear() {
		ModelExportAbstractUiProvider.clearStatic();

	}

	private static void clearStatic() {
		ModelExportAbstractUiProvider.defaultDestinationFilePath = null;
	}

	@Override
	public String getPageDescription() {
		return ModelExportMessages.exportPageDescription;
	}
}
