/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util;

/**
 * Model Export Exception
 */
public class ModelExportException extends Exception {
	private static final long serialVersionUID = 7495760549265637103L;

	/**
	 * Empty constructor.
	 */
	public ModelExportException() {
	}

	/**
	 * Constructs a new Model Export Exception.
	 * 
	 * @param message
	 *            - the message of this exception.
	 */
	public ModelExportException(final String message) {
		super(message);
	}

	/**
	 * Constructs a new Model Export Exception.
	 * 
	 * @param cause
	 *            - the cause of the exception.
	 */
	public ModelExportException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Constructs a new Model Export Exception.
	 * 
	 * @param message
	 *            - the message of this exception.
	 * @param cause
	 *            - the cause of the exception.
	 */
	public ModelExportException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
