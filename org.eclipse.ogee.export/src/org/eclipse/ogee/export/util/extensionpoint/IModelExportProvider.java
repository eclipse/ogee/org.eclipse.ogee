/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.export.util.extensionpoint;

import java.util.List;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ogee.export.util.ModelExportException;
import org.eclipse.ogee.export.util.ui.IModelExportFirstPage;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.swt.widgets.Composite;

/**
 * Plug-ins that want to extend the service model export format must implement
 * this interface. This interface supplies methods to implement the following:
 * 1. Add UI objects to the first page of the wizard under the export format
 * combo. 2. add pages to the wizard after the main page. 3. Implement the
 * converter to the specific format, after user presses the finish button of the
 * wizard.
 */
public interface IModelExportProvider {
	/**
	 * Export the model to specific extension. This method is called when the
	 * user presses the finish button of the wizard.
	 * 
	 * @param edmxSet
	 *            setMessage and setPageComplete domain model root
	 */
	public void performFinish(EDMXSet edmxSet) throws ModelExportException;

	/**
	 * Create the UI elements and validations for the first page of the export
	 * wizard.
	 * 
	 * @param parent
	 *            composite of the UI elements
	 * @param wizardPage
	 *            supplies access to attributes of the wizard page as defined in
	 *            interface org.eclipse.ogee.export.provider. For example
	 *            to get the shell or update the page status.
	 */
	public void createFirstPageUI(Composite parent,
			IModelExportFirstPage wizardPage);

	/**
	 * Get additional pages to be displayed in the wizard after the main page
	 * 
	 * @return list of additional pages
	 */
	public List<IWizardPage> getPages();

	/**
	 * Get extension description to be displayed in the first page under the
	 * page title. In case the description in null a default description will be
	 * displayed.
	 * 
	 * @return page description
	 */
	public String getPageDescription();

	/**
	 * Clear data after the wizard is closed
	 */
	public void clear();

}
