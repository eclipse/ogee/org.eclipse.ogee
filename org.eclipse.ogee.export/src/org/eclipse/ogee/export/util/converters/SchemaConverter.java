/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAssociationConverter;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IComplexTypeConverter;
import org.eclipse.ogee.export.util.converters.api.IEntityContainerConverter;
import org.eclipse.ogee.export.util.converters.api.IEntityTypeConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.ISchemaConverter;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.export.util.converters.api.IUsingConverter;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SchemaConverter implements ISchemaConverter {

	private List<Element> edmxSchemaList = new ArrayList<Element>();

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parent, final EDMXSet edmxSet) {

		EList<Schema> schemaes = edmxSet.getMainEDMX().getDataService()
				.getSchemata();
		for (Schema currSchema : schemaes) {
			// Create schema element
			Element edmxSchema = document.createElement(ITag.SCHEMA);

			// Set schema name space name
			edmxSchema.setAttribute(IAttribute.NAMESPACE,
					currSchema.getNamespace());

			// Set schema alias
			String alias = currSchema.getAlias();
			if (alias != null && !alias.isEmpty()) {
				edmxSchema.setAttribute(IAttribute.ALIAS, alias);
			}

			// Set schema reference
			edmxSchema.setAttribute(IAttribute.XMLNS, IAttribute.EDM_NS);

			// Using
			IUsingConverter usingConverter = currentFactory.getUsingConverter();
			EList<Using> usings = currSchema.getUsings();
			for (Using using : usings) {
				usingConverter.createElement(currentFactory, document,
						edmxSchema, using);
			}

			// Entity Type
			IEntityTypeConverter entityTypeConverter = currentFactory
					.getEntityTypeConverter();
			EList<EntityType> entityTypes = currSchema.getEntityTypes();
			for (EntityType currEntityType : entityTypes) {
				entityTypeConverter.createElement(currentFactory, document,
						edmxSchema, currEntityType);
			}

			// Complex Type
			IComplexTypeConverter complexTypeConverter = currentFactory
					.getComplexType();
			EList<ComplexType> complexTypes = currSchema.getComplexTypes();
			for (ComplexType currComplexType : complexTypes) {
				complexTypeConverter.createElement(currentFactory, document,
						edmxSchema, currComplexType);
			}

			// Association
			IAssociationConverter associationConverter = currentFactory
					.getAssociationConverter();
			EList<Association> associations = currSchema.getAssociations();
			for (Association currAssociation : associations) {
				associationConverter.createElement(currentFactory, document,
						edmxSchema, currAssociation);
			}

			// Entity Container
			IEntityContainerConverter entityContainerConverter = currentFactory
					.getEntityContainerConverter();
			EList<EntityContainer> entityContainers = currSchema
					.getContainers();
			for (EntityContainer currEntityContainer : entityContainers) {
				entityContainerConverter.createElement(currentFactory,
						document, edmxSchema, currEntityContainer);
			}

			parent.appendChild(edmxSchema);

			this.edmxSchemaList.add(edmxSchema);

		}

	}

	@Override
	public List<Element> getElement() {

		return this.edmxSchemaList;
	}

}
