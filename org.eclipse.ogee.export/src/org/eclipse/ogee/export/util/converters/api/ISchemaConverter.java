/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import java.util.List;

import org.eclipse.ogee.model.odata.EDMXSet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * 
 *  This API is responsible to handle schema convertion from EdmxSet 
 *  to the document tree element
 */


public interface ISchemaConverter {
	/**
	 * Create DOM property element and add it to his DOM parent - Edmx
	 * 
	 * @param document
	 *            Insert Dom document
	 * @param parent
	 *            Insert property parent - Edmx
	 * @param property
	 *            Insert Edmx set
	 * @param currentFactory
	 *            Insert current factory instance
	 */
	public void createElement(IFormatConverterFactory currentFactory,
			Document document, Element parent, EDMXSet EdmxSet);

	/**
	 * Return property element
	 */
	public List<Element> getElement();

}
