/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

/**
 * Enables creation of Converter instance based on factory format (V2,
 * OData4SAP, V3)
 * 
 */
public interface IFormatConverterFactory {
	/**
	 * @returns instance of AssociationConverter
	 */
	public IAssociationConverter getAssociationConverter();

	/**
	 * @returns instance of AssociationSetConverter
	 */
	public IAssociationSetConverter getAssociationSetConverter();

	/**
	 * @returns instance of AssociationSetEndConverter
	 */
	public IAssociationSetEndConverter getAssociationSetEndConverter();

	/**
	 * @returns instance of ComplexTypeConverter
	 */
	public IComplexTypeConverter getComplexType();

	/**
	 * @returns instance of DependentConverter
	 */
	public IEndConverter getDependentConverter();

	/**
	 * @returns instance of DocumatationConverter
	 */
	public IDocumentationConverter getDocumatationConverter();

	/**
	 * @returns instance of EndConverter
	 */
	public IEndConverter getEndConverter();

	/**
	 * @returns instance of EntityContainerConverter
	 */
	public IEntityContainerConverter getEntityContainerConverter();

	/**
	 * @returns instance of EntitySetConverter
	 */
	public IEntitySetConverter getEntitySetConverter();

	/**
	 * @returns instance of EntityTypeConverter
	 */
	public IEntityTypeConverter getEntityTypeConverter();

	/**
	 * @returns instance of FunctionImportConverter
	 */
	public IFunctionImportConverter getFunctionImportConverter();

	/**
	 * @returns instance of FunctionImportParameterConverter
	 */
	public IFunctionImportParameterConverter getFunctionImportParameterConverter();

	/**
	 * @returns instance of KeyConverter
	 */
	public IKeyConverter getKeyConverter();

	/**
	 * @returns instance of NavigationPropertyConverter
	 */
	public INavigationPropertyConverter getNavigationPropertyConverter();

	/**
	 * @returns instance of PrincipalConverter
	 */
	public IEndConverter getPrincipalConverter();

	/**
	 * @returns instance of PropertyConverter
	 */
	public IPropertyConverter getPropertyConverter();

	/**
	 * @returns instance of PropertyRefConverter
	 */
	public IPropertyConverter getPropertyRefConverter();

	/**
	 * @returns instance of ReferentialConstraintConverter
	 */
	public IReferentialConstraintConverter getReferentialConstraintConverter();

	/**
	 * @returns instance of SchemaConverter
	 */
	public ISchemaConverter getSchemaConverter();

	/**
	 * @returns instance of Transformation
	 */
	public ITransformer getTransformer();

	/**
	 * @returns instance of UsingConverter
	 */
	public IUsingConverter getUsingConverter();

}
