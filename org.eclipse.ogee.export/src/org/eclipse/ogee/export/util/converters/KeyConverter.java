/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IKeyConverter;
import org.eclipse.ogee.export.util.converters.api.IPropertyConverter;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Property;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class KeyConverter implements IKeyConverter {
	private Element keyElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parent,
			final EntityType entityType) {
		this.keyElement = document.createElement(ITag.KEY);

		// Get propertyRef
		EList<Property> propertyRefKey = entityType.getKeys();
		for (Property propertyRefKeys : propertyRefKey) {
			IPropertyConverter propRefKey = currentFactory
					.getPropertyRefConverter();
			propRefKey.createElement(currentFactory, document, this.keyElement,
					propertyRefKeys);
		}

		parent.appendChild(keyElement);
	}

	@Override
	public Element getElement() {
		return this.keyElement;
	}

}
