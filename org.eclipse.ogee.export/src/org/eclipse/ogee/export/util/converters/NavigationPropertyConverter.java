/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.INavigationPropertyConverter;
import org.eclipse.ogee.export.util.converters.api.ISymbol;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class NavigationPropertyConverter implements
		INavigationPropertyConverter {

	private Element navigationPropertyElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parent,
			final NavigationProperty navigationProperty) {

		// Create document property element
		this.navigationPropertyElement = document
				.createElement(ITag.NAVIGATIONPROPERTY);

		// Set name
		this.navigationPropertyElement.setAttribute(IAttribute.NAME,
				navigationProperty.getName());

		// Set Relationship
		final Association relationship = navigationProperty.getRelationship();
		if (relationship != null) {
			String typeSchemaIdentifier = getTypeSchemaIdentifier(relationship,
					navigationProperty);
			this.navigationPropertyElement.setAttribute(
					IAttribute.RELATIONSHIP, typeSchemaIdentifier + ISymbol.DOT
							+ relationship.getName());

		}

		// Set ToRole
		final Role toRole = navigationProperty.getToRole();
		if (toRole != null) {
			this.navigationPropertyElement.setAttribute(IAttribute.TOROLE,
					toRole.getName());
		}

		// Set FromRole
		final Role fromRole = navigationProperty.getFromRole();
		if (fromRole != null) {
			this.navigationPropertyElement.setAttribute(IAttribute.FROMROLE,
					fromRole.getName());
		}
		// Create Documentation sub-element
		Documentation documentation = navigationProperty.getDocumentation();
		if (documentation != null) {
			IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					this.navigationPropertyElement, documentation);
		}

		// Add to EntityType
		parent.appendChild(this.navigationPropertyElement);
	}

	@Override
	public Element getElement() {

		return this.navigationPropertyElement;
	}

	private String getTypeSchemaIdentifier(Association relationship,
			NavigationProperty navigationProperty) {
		String schemaIdentifier;
		// Get the used type namespace
		schemaIdentifier = ((Schema) relationship.eContainer()).getNamespace();
		// Get the current model schema namespace
		Schema currentSchema = ((Schema) ((EntityType) navigationProperty
				.eContainer()).eContainer());
		if (currentSchema.getNamespace().equals(schemaIdentifier)) {
			// It means that the used type is not a reference type
			// Check if current model schema has alias
			String currentSchemaAlias = currentSchema.getAlias();
			if (currentSchemaAlias != null && !currentSchemaAlias.isEmpty()) {
				// Schema identifier will be an alias name
				schemaIdentifier = currentSchemaAlias;
			}
		} else {
			// It means that the used type is from reference type
			// Check if current model schema has using list
			EList<Using> usingsList = currentSchema.getUsings();
			if (usingsList != null) {
				for (Using usingEntry : usingsList) {
					// Check in using list the respective alias name that can be
					// used instead the namespace name
					if (usingEntry.getUsedNamespace().getNamespace()
							.equals(schemaIdentifier)) {
						schemaIdentifier = usingEntry.getAlias();
						break;
					}
				}
			}
		}
		return schemaIdentifier;
	}
}
