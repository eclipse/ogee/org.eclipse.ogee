/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IEntityTypeConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IKeyConverter;
import org.eclipse.ogee.export.util.converters.api.INavigationPropertyConverter;
import org.eclipse.ogee.export.util.converters.api.IPropertyConverter;
import org.eclipse.ogee.export.util.converters.api.ISymbol;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class EntityTypeConverter implements IEntityTypeConverter {

	private Element entityTypeElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parent,
			final EntityType entityType) {

		// Create document property element
		this.entityTypeElement = document.createElement(ITag.ENTITYTYPE);

		parent.appendChild(entityTypeElement);
		// Set Name
		this.entityTypeElement.setAttribute(IAttribute.NAME,
				entityType.getName());

		// Set Base type - Optional
		EntityType baseType = entityType.getBaseType();
		if (baseType != null) {
			String typeSchemaIdentifier = getTypeSchemaIdentifier(baseType,
					entityType);
			this.entityTypeElement.setAttribute(IAttribute.BASETYPE,
					typeSchemaIdentifier + ISymbol.DOT + baseType.getName());
		}

		// Is Abstract - optional
		if (entityType.isAbstract() == true) {
			this.entityTypeElement.setAttribute(IAttribute.ABSTRACT,
					IAttribute.TRUE);
		}

		// Create Documentation sub-element
		Documentation documentation = entityType.getDocumentation();
		if (documentation != null) {
			IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					entityTypeElement, documentation);
		}

		if (baseType == null) {

			// Get Keys and properties reference
			IKeyConverter keyConverter = currentFactory.getKeyConverter();
			keyConverter.createElement(currentFactory, document,
					entityTypeElement, entityType);

			// Get Keys properties
			EList<Property> keyProperties = entityType.getKeys();
			for (Property property : keyProperties) {
				IPropertyConverter propertyConverter = currentFactory
						.getPropertyConverter();
				propertyConverter.createElement(currentFactory, document,
						entityTypeElement, property);
			}
		}

		// Get properties
		EList<Property> properties = entityType.getProperties();
		for (Property property : properties) {
			IPropertyConverter propertyConverter = currentFactory
					.getPropertyConverter();
			propertyConverter.createElement(currentFactory, document,
					entityTypeElement, property);
		}

		// Get navigation property
		EList<NavigationProperty> navigationProperties = entityType
				.getNavigationProperties();
		for (NavigationProperty navigationProperty : navigationProperties) {
			INavigationPropertyConverter navigationPropertyConverter = currentFactory
					.getNavigationPropertyConverter();
			navigationPropertyConverter.createElement(currentFactory, document,
					entityTypeElement, navigationProperty);
		}

	}

	@Override
	public Element getElement() {

		return this.entityTypeElement;
	}

	private String getTypeSchemaIdentifier(EntityType baseEntityType,
			EntityType entityType) {
		String schemaIdentifier;
		// Get the used type namespace
		schemaIdentifier = ((Schema) baseEntityType.eContainer())
				.getNamespace();
		// Get the current model schema namespace
		Schema currentSchema = (Schema) entityType.eContainer();
		if (currentSchema.getNamespace().equals(schemaIdentifier)) {
			// It means that the used type is not a reference type
			// Check if current model schema has alias
			String currentSchemaAlias = currentSchema.getAlias();
			if (currentSchemaAlias != null && !currentSchemaAlias.isEmpty()) {
				// Schema identifier will be an alias name
				schemaIdentifier = currentSchemaAlias;
			}
		} else {
			// It means that the used type is from reference type
			// Check if current model schema has using list
			EList<Using> usingsList = currentSchema.getUsings();
			if (usingsList != null) {
				for (Using usingEntry : usingsList) {
					// Check in using list the respective alias name that can be
					// used instead the namespace name
					if (usingEntry.getUsedNamespace().getNamespace()
							.equals(schemaIdentifier)) {
						schemaIdentifier = usingEntry.getAlias();
						break;
					}
				}
			}
		}
		return schemaIdentifier;
	}

}
