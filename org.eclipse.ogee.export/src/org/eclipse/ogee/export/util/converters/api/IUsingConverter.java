/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * 
 *         This API is responsible to handle using convertion from EdmxSet -
 *         schema to the tree document element
 */

public interface IUsingConverter {
	/**
	 * Create Dom Using element and add it to his Dom parent - Schema
	 * 
	 * @param document
	 *            Insert Dom document
	 * @param parent
	 *            Insert navigation property parent - Schema
	 * @param using
	 *            Insert using tag
	 * @param currentFactory
	 *            Insert current factory instance
	 */
	public void createElement(IFormatConverterFactory currentFactory,
			Document document, Element parent, Using using);

	/**
	 * Return Schema
	 */
	public Element getElement();

}
