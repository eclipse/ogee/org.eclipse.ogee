/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import org.eclipse.ogee.model.odata.AssociationSet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * The interface "IAssociationSetConverter" is responsible for converting
 * AssociationSet from the Domain model Object into DOM Association Set element
 * 
 */
public interface IAssociationSetConverter {

	/**
	 * @return returns AssociationSet element
	 */
	public Element getElement();

	/**
	 * Creates AssociationSet DOM element. AssociationSetEnd sub elements are
	 * created as well.
	 * 
	 * @param document
	 *            - DOM document
	 * @param parentElement
	 *            - parent element in DOM
	 * @param associationSet
	 *            - object from Domain Model
	 * @param currentFactory
	 *            - Insert current factory instance
	 */
	public void createElement(IFormatConverterFactory currentFactory,
			Document document, Element parentElement,
			AssociationSet associationSet);

}
