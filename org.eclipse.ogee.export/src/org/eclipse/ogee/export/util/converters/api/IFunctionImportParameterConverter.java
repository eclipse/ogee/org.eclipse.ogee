/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import org.eclipse.ogee.model.odata.Parameter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *  The class is responsible to convert Function Import
 *         Parameter Domain model EObject into DOM Function Import Parameter
 *         element
 */
public interface IFunctionImportParameterConverter {
	/**
	 * Returns FunctionImportParameter DOM element
	 */
	public Element getElement();

	/**
	 * Creates FunctionImportParameter DOM element
	 * 
	 * @param document
	 *            DOM object
	 * @param parentElement
	 *            FunctionImport element
	 * @param Domain
	 *            model functionImportParameter object
	 * @param currentFactory
	 *            Insert current factory instance
	 */
	public void createElement(IFormatConverterFactory currentFactory,
			Document document, Element parentElement,
			Parameter functionImportParameter);
}
