/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import org.eclipse.ogee.model.odata.EntityType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * 
 *         This API is responsible to handle entity type convertion from EdmxSet
 *         - schema to the tree document element
 */
public interface IEntityTypeConverter {
	/**
	 * Create Dom enityType element and add it to his Dom parent - Schema
	 * 
	 * @param document
	 *            Insert Dom document
	 * @param parent
	 *            Insert entity type parent - Schema
	 * @param property
	 *            Insert EntityType object
	 * @param currentFactory
	 *            Insert current factory instance
	 */
	void createElement(IFormatConverterFactory currentFactory,
			Document document, Element parent, EntityType entityType);

	/**
	 * Return entityType Element
	 */
	public Element getElement();

}
