/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.export.util.converters.api.IUsingConverter;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class UsingConverter implements IUsingConverter {

	private Element usingElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parent, final Using using) {

		// Create element Using
		this.usingElement = document.createElement(ITag.USING);

		// Create nameSpace tag
		Schema usedNamespace = using.getUsedNamespace();

		if (usedNamespace != null) {
			String namespace = usedNamespace.getNamespace();
			this.usingElement.setAttribute(IAttribute.NAMESPACE, namespace);
		}

		// Get alias

		this.usingElement.setAttribute(IAttribute.ALIAS, using.getAlias());

		// Add to Schema
		parent.appendChild(usingElement);

	}

	@Override
	public Element getElement() {

		return usingElement;
	}

}
