/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

public interface ITag {
	public static final String SCHEMA = "Schema"; //$NON-NLS-1$
	public static final String ENTITYSET = "EntitySet"; //$NON-NLS-1$
	public static final String PROPERTY = "Property"; //$NON-NLS-1$
	public static final String FUNCTIONIMPORT = "FunctionImport"; //$NON-NLS-1$
	public static final String ASSOCIATION = "Association"; //$NON-NLS-1$
	public static final String ASSOCIATIONSET = "AssociationSet"; //$NON-NLS-1$
	public static final String ENTITYTYPE = "EntityType"; //$NON-NLS-1$
	public static final String KEY = "Key"; //$NON-NLS-1$
	public static final String PROPERTYREF = "PropertyRef"; //$NON-NLS-1$
	public static final String NAVIGATIONPROPERTY = "NavigationProperty"; //$NON-NLS-1$
	public static final String COMPLEXTYPE = "ComplexType"; //$NON-NLS-1$
	public static final String END = "End"; //$NON-NLS-1$
	public static final String REFERENTIALCONSTRAINT = "ReferentialConstraint"; //$NON-NLS-1$
	public static final String PRINCIPAL = "Principal"; //$NON-NLS-1$
	public static final String DEPENDENT = "Dependent"; //$NON-NLS-1$
	public static final String ENTITYCONTAINER = "EntityContainer"; //$NON-NLS-1$
	public static final String DOCUMENTATION = "Documentation"; //$NON-NLS-1$
	public static final String PARAMETER = "Parameter"; //$NON-NLS-1$
	public static final String USING = "Using"; //$NON-NLS-1$
	public static final String SUMMARY = "Summary"; //$NON-NLS-1$
	public static final String LONGDESCRIPTION = "LongDescription"; //$NON-NLS-1$

}
