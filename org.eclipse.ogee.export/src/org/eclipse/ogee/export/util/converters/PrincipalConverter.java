/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IEndConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.Role;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class PrincipalConverter implements IEndConverter {

	private Element principalElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement,
			final Role principal) {

		this.principalElement = document.createElement(ITag.PRINCIPAL);

		// Set Role
		this.principalElement
				.setAttribute(IAttribute.ROLE, principal.getName());

		// append the principalElement to the Referential Constraint
		parentElement.appendChild(this.principalElement);
	}

	@Override
	public Element getElement() {
		// TODO Auto-generated method stub
		return this.principalElement;
	}

}
