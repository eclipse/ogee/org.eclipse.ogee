/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IEndConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.ISymbol;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class EndConverter implements IEndConverter {

	private Element endElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement, final Role end) {

		endElement = document.createElement(ITag.END);

		// Set Role
		final String roleName = end.getName();
		if (roleName != null) {
			this.endElement.setAttribute(IAttribute.ROLE, roleName);
		}

		// Set Type
		final EntityType endType = end.getType();
		String typeSchemaIdentifier = getTypeSchemaIdentifier(endType, end);
		this.endElement.setAttribute(IAttribute.TYPE, typeSchemaIdentifier
				+ ISymbol.DOT + endType.getName());

		// Set Multiplicity
		this.endElement.setAttribute(IAttribute.MULTIPLICITY, end
				.getMultiplicity().getLiteral());

		// Set Documentation
		final Documentation documentation = end.getDocumentation();
		if (documentation != null) {
			// Get the Documentation converter
			final IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					this.endElement, documentation);
		}

		// append the End to the Association
		parentElement.appendChild(this.endElement);
	}

	@Override
	public Element getElement() {
		return this.endElement;
	}

	private String getTypeSchemaIdentifier(EntityType entityType, Role end) {
		String schemaIdentifier;
		// Get the used type namespace
		schemaIdentifier = ((Schema) entityType.eContainer()).getNamespace();
		// Get the current model schema namespace
		Schema currentSchema = ((Schema) ((Association) end.eContainer())
				.eContainer());
		if (currentSchema.getNamespace().equals(schemaIdentifier)) {
			// It means that the used type is not a reference type
			// Check if current model schema has alias
			String currentSchemaAlias = currentSchema.getAlias();
			if (currentSchemaAlias != null && !currentSchemaAlias.isEmpty()) {
				// Schema identifier will be an alias name
				schemaIdentifier = currentSchemaAlias;
			}
		} else {
			// It means that the used type is from reference type
			// Check if current model schema has using list
			EList<Using> usingsList = currentSchema.getUsings();
			if (usingsList != null) {
				for (Using usingEntry : usingsList) {
					// Check in using list the respective alias name that can be
					// used instead the namespace name
					if (usingEntry.getUsedNamespace().getNamespace()
							.equals(schemaIdentifier)) {
						schemaIdentifier = usingEntry.getAlias();
						break;
					}
				}
			}
		}
		return schemaIdentifier;
	}

}
