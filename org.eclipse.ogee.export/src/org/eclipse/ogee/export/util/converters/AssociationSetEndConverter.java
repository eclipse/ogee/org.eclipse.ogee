/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.ogee.export.util.converters.api.IAssociationSetEndConverter;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.Role;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class AssociationSetEndConverter implements IAssociationSetEndConverter {

	private Element associationSetEndElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement,
			final AssociationSetEnd associationSetEnd) {

		// Create associationSetEndElement XML Element
		this.associationSetEndElement = document.createElement(ITag.END);
		Role associationSetRole = associationSetEnd.getRole();
		if (associationSetRole != null) {
			this.associationSetEndElement.setAttribute(IAttribute.ROLE,
					associationSetRole.getName());
		}

		EntitySet associationEntitySet = associationSetEnd.getEntitySet();
		if (associationEntitySet != null) {
			this.associationSetEndElement.setAttribute(IAttribute.ENTITYSET,
					associationEntitySet.getName());
		}

		// Add Documentation sub_element
		Documentation documentation = associationSetEnd.getDocumentation();
		if (documentation != null) {
			IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					this.associationSetEndElement, documentation);
		}

		parentElement.appendChild(this.associationSetEndElement);
	}

	@Override
	public Element getElement() {
		return this.associationSetEndElement;
	}

}
