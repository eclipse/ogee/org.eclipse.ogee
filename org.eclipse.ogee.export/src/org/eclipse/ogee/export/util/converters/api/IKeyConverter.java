/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import org.eclipse.ogee.model.odata.EntityType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * 
 *  This API is responsible to handle key property convertion from EdmxSet - entity type
 *  to the Document tree element
 */


public interface IKeyConverter {
	/**
	 * Create DOM key element and add it to his DOM parent - entityType
	 * 
	 * @param document
	 *            Insert Dom document
	 * @param parent
	 *            Insert key parent - entityType
	 * @param entityType
	 *            Insert entityType object
	 * @param currentFactory
	 *            Insert current factory instance
	 */
	public void createElement(IFormatConverterFactory currentFactory,
			Document document, Element parent, EntityType entityType);

	/**
	 * Return key element
	 * 
	 * @return
	 */
	public Element getElement();

}
