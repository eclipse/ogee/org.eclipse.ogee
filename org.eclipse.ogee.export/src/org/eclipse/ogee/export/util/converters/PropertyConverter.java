/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IPropertyConverter;
import org.eclipse.ogee.export.util.converters.api.ISymbol;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class PropertyConverter implements IPropertyConverter {

	private Element propertyElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parent,
			final Property property) {

		// Create document property element
		this.propertyElement = document.createElement(ITag.PROPERTY);
		// Set property
		setProperty(property);
		// Create Documentation sub-element
		final Documentation documentation = property.getDocumentation();
		if (documentation != null) {
			final IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					propertyElement, documentation);
		}

		// Get simple element - property
		final IPropertyTypeUsage propertyTypeUsage = property.getType();

		if (propertyTypeUsage != null) {

			if (propertyTypeUsage instanceof SimpleTypeUsage) {
				final SimpleType simpletype = ((SimpleTypeUsage) propertyTypeUsage)
						.getSimpleType();
				// Set simple type
				if (simpletype != null) {
					setSimpleType(simpletype);
				}
			} else if (propertyTypeUsage instanceof ComplexTypeUsage) {

				// Get complex element - property
				{
					final ComplexType complexType = ((ComplexTypeUsage) propertyTypeUsage)
							.getComplexType();
					if (complexType != null) {
						String typeSchemaIdentifier = getTypeSchemaIdentifier(
								complexType, property);
						this.propertyElement.setAttribute(IAttribute.TYPE,
								typeSchemaIdentifier + ISymbol.DOT
										+ complexType.getName());
					}
				}
			} else if (propertyTypeUsage instanceof EnumTypeUsage) {
				// Get enum type and covert it to basic type
				final EnumType enumType = ((EnumTypeUsage) propertyTypeUsage)
						.getEnumType();
				if (enumType != null) {
					this.propertyElement.setAttribute(IAttribute.TYPE, enumType
							.getUnderlyingType().getLiteral());
				}

			}
		}

		parent.appendChild(propertyElement);
	}

	@Override
	public Element getElement() {

		return this.propertyElement;
	}

	private void setSimpleType(final SimpleType simpletype) {

		// set type - required
		this.propertyElement.setAttribute(IAttribute.TYPE, simpletype.getType()
				.getLiteral());
		// Default value
		if (simpletype.getDefaultValue() != null) {
			this.propertyElement.setAttribute(IAttribute.DEFAULTVALUE,
					((SimpleValue) (simpletype.getDefaultValue()))
							.getValueObject().toString());
		}

		// Max length - optional
		if (simpletype.getMaxLength() > 0) {
			this.propertyElement.setAttribute(IAttribute.MAXLENGTH,
					String.valueOf(simpletype.getMaxLength()));
		}

		// FixedLength - optional
		if (simpletype.getFixedLength() > 0) {
			this.propertyElement.setAttribute(IAttribute.FIXEDLENGTH,
					String.valueOf(simpletype.getFixedLength()));
		}

		// Precision - optional
		if (simpletype.getPrecision() > 0) {
			this.propertyElement.setAttribute(IAttribute.PRECISION,
					String.valueOf(simpletype.getPrecision()));
		}

		// Scale - optional
		if (simpletype.getScale() > 0) {
			this.propertyElement.setAttribute(IAttribute.SCALE,
					String.valueOf(simpletype.getScale()));
		}

	}

	private void setProperty(final Property property) {

		// set Name - required
		this.propertyElement.setAttribute(IAttribute.NAME, property.getName());

		// Is Nullable - optional
		if (!property.isNullable()) {
			this.propertyElement.setAttribute(IAttribute.NULLABLE,
					IAttribute.FALSE);
		}

		// ConcurrencyMode - optional
		if (property.isForEtag()) {
			this.propertyElement.setAttribute(IAttribute.CONCURRENCY_MODE,
					IAttribute.CUNNC_FIXED);
		}

	}

	public String getTypeSchemaIdentifier(Object typeObject, Property property) {
		String schemaIdentifier = null;
		// Get the used type namespace
		// Get the used type namespace
		if (typeObject instanceof ComplexType) {
			ComplexType complexType = (ComplexType) typeObject;
			schemaIdentifier = ((Schema) complexType.eContainer())
					.getNamespace();

		} else if (typeObject instanceof EnumType) {
			EnumType enumType = (EnumType) typeObject;
			schemaIdentifier = ((Schema) enumType.eContainer()).getNamespace();
		}

		// Get the current model schema namespace
		Schema currentSchema = null;

		if (property.eContainer() instanceof EntityType) {
			currentSchema = (Schema) ((EntityType) property.eContainer())
					.eContainer();
		} else if (property.eContainer() instanceof ComplexType) {
			currentSchema = (Schema) ((ComplexType) property.eContainer())
					.eContainer();
		}

		if (currentSchema != null) {
			if (currentSchema.getNamespace().equals(schemaIdentifier)) {
				// It means that the used type is not a reference type
				// Check if current model schema has alias
				String currentSchemaAlias = currentSchema.getAlias();
				if (currentSchemaAlias != null && !currentSchemaAlias.isEmpty()) {
					// Schema identifier will be an alias name
					schemaIdentifier = currentSchemaAlias;
				}
			} else {
				// It means that the used type is from reference type
				// Check if current model schema has using list
				EList<Using> usingsList = currentSchema.getUsings();
				if (usingsList != null) {
					for (Using usingEntry : usingsList) {
						// Check in using list the respective alias name that
						// can be
						// used instead the namespace name
						if (usingEntry.getUsedNamespace().getNamespace()
								.equals(schemaIdentifier)) {
							schemaIdentifier = usingEntry.getAlias();
							break;
						}
					}
				}
			}
		}
		return schemaIdentifier;
	}

}
