/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IEntitySetConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.ISymbol;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class EntitySetConverter implements IEntitySetConverter {

	private Element entitySetElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement,
			final EntitySet entitySet) {

		// Create EntitySet XML Element
		this.entitySetElement = document.createElement(ITag.ENTITYSET);
		// Set Attributes to EntitySet element
		this.entitySetElement
				.setAttribute(IAttribute.NAME, entitySet.getName());
		EntityType entityType = entitySet.getType();
		if (entityType != null) {
			String typeSchemaIdentifier = getTypeSchemaIdentifier(entityType,
					entitySet);
			this.entitySetElement.setAttribute(IAttribute.ENTITYTYPE,
					typeSchemaIdentifier + ISymbol.DOT + entityType.getName());
		}

		// Add Documentation sub_element
		Documentation documentation = entitySet.getDocumentation();
		if (documentation != null) {
			IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					this.entitySetElement, documentation);
		}

		// Add EntitySet to EntityContainer
		parentElement.appendChild(this.entitySetElement);
	}

	private String getTypeSchemaIdentifier(EntityType entityType,
			EntitySet entitySet) {
		String schemaIdentifier;
		// Get the used type namespace
		schemaIdentifier = ((Schema) entityType.eContainer()).getNamespace();
		// Get the current model schema namespace
		Schema currentSchema = ((Schema) ((EntityContainer) entitySet
				.eContainer()).eContainer());
		if (currentSchema.getNamespace().equals(schemaIdentifier)) {
			// It means that the used type is not a reference type
			// Check if current model schema has alias
			String currentSchemaAlias = currentSchema.getAlias();
			if (currentSchemaAlias != null && !currentSchemaAlias.isEmpty()) {
				// Schema identifier will be an alias name
				schemaIdentifier = currentSchemaAlias;
			}
		} else {
			// It means that the used type is from reference type
			// Check if current model schema has using list
			EList<Using> usingsList = currentSchema.getUsings();
			if (usingsList != null) {
				for (Using usingEntry : usingsList) {
					// Check in using list the respective alias name that can be
					// used instead the namespace name
					if (usingEntry.getUsedNamespace().getNamespace()
							.equals(schemaIdentifier)) {
						schemaIdentifier = usingEntry.getAlias();
						break;
					}
				}
			}
		}
		return schemaIdentifier;
	}

	@Override
	public Element getElement() {
		return this.entitySetElement;
	}

}
