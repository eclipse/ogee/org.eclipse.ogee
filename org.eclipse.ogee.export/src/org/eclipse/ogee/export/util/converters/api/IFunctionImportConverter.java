/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import org.eclipse.ogee.model.odata.FunctionImport;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *  The class is responsible to convert Function Import
 *         Domain model EObject into DOM Function Import element
 */
public interface IFunctionImportConverter {

	/**
	 * Returns FunctionImport DOM element
	 */
	public Element getElement();

	/**
	 * Creates FunctionImport DOM element. Parameter sub-elements are created as
	 * well.
	 * 
	 * @param document
	 *            DOM object
	 * @param parentElement
	 *            EntityContainer element
	 * @param functionImport
	 *            Domain model functionImport object
	 * @param currentFactory
	 *            Insert current factory instance
	 */
	public void createElement(IFormatConverterFactory currentFactory,
			Document document, Element parentElement,
			FunctionImport functionImport);

}
