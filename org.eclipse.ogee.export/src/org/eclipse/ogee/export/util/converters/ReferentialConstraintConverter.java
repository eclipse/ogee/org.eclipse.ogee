/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import java.util.Map.Entry;

import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IEndConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IPropertyConverter;
import org.eclipse.ogee.export.util.converters.api.IReferentialConstraintConverter;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Role;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ReferentialConstraintConverter implements
		IReferentialConstraintConverter {

	private Element referentialConstraintElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement,
			final ReferentialConstraint referentialConstraint) {

		this.referentialConstraintElement = document
				.createElement(ITag.REFERENTIALCONSTRAINT);

		// Set Documentation
		final Documentation documentation = referentialConstraint
				.getDocumentation();
		if (documentation != null) {
			final IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					this.referentialConstraintElement, documentation);
		}

		// Set the Principal
		final Role principal = referentialConstraint.getPrincipal();
		IEndConverter principalConverter = currentFactory
				.getPrincipalConverter();
		principalConverter.createElement(currentFactory, document,
				this.referentialConstraintElement, principal);

		// Set the Dependent
		final Role dependent = referentialConstraint.getDependent();
		final IEndConverter dependentConverter = currentFactory
				.getDependentConverter();
		dependentConverter.createElement(currentFactory, document,
				this.referentialConstraintElement, dependent);

		// add the property ref for the principal(key) and dependent(value)
		final Element principalElement = principalConverter.getElement();
		final Element dependentElement = dependentConverter.getElement();
		final IPropertyConverter propertyRefConverter = currentFactory
				.getPropertyRefConverter();
		for (Entry<Property, Property> keyMapping : referentialConstraint
				.getKeyMappings()) {
			propertyRefConverter.createElement(currentFactory, document,
					principalElement, keyMapping.getKey());
			propertyRefConverter.createElement(currentFactory, document,
					dependentElement, keyMapping.getValue());
		}

		// append the Referential Constraint to the Association
		parentElement.appendChild(this.referentialConstraintElement);
	}

	@Override
	public Element getElement() {
		return referentialConstraintElement;
	}

}
