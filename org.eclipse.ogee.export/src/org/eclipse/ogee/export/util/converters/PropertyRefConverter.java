/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IPropertyConverter;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.Property;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class PropertyRefConverter implements IPropertyConverter {

	private Element propertyRefElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parent,
			final Property property) {
		if (property != null) {
			this.propertyRefElement = document.createElement(ITag.PROPERTYREF);
			this.propertyRefElement.setAttribute(IAttribute.NAME,
					property.getName());
			parent.appendChild(propertyRefElement);
		}
	}

	@Override
	public Element getElement() {
		return this.propertyRefElement;
	}

}
