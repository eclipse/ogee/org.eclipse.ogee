/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import org.eclipse.ogee.model.odata.ComplexType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *  Responsible to convert Complex Type Set Domain model EObject
 *         into DOM Complex Type element
 */
public interface IComplexTypeConverter {

	/**
	 * Create Dom Complex Type element and add it to his Dom parent
	 * 
	 * @param document
	 *            Insert Dom document
	 * @param parentElement
	 *            Insert Complex Type parent - Schema
	 * @param complexType
	 *            Insert Complex Type
	 * @param currentFactory
	 *            Insert current factory instance
	 * @param currentFactory
	 *            Insert current factory instance
	 */
	public void createElement(IFormatConverterFactory currentFactory,
			Document document, Element parentElement, ComplexType complexType);

	/**
	 * @return Complex Type
	 */
	public Element getElement();

}
