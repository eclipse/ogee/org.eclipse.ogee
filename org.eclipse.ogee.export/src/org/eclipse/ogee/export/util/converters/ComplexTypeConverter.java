/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IComplexTypeConverter;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IPropertyConverter;
import org.eclipse.ogee.export.util.converters.api.ISymbol;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ComplexTypeConverter implements IComplexTypeConverter {
	private Element complexTypeElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement,
			final ComplexType complexType) {

		this.complexTypeElement = document.createElement(ITag.COMPLEXTYPE);

		// Set Name
		this.complexTypeElement.setAttribute(IAttribute.NAME,
				complexType.getName());

		// Set Base type - Optional
		final ComplexType baseType = complexType.getBaseType();
		if (baseType != null) {
			String typeSchemaIdentifier = getTypeSchemaIdentifier(baseType,
					complexType);
			complexTypeElement.setAttribute(IAttribute.BASETYPE,
					typeSchemaIdentifier + ISymbol.DOT + baseType.getName());
		}

		// Is Abstract - optional
		if (complexType.isAbstract()) {
			complexTypeElement.setAttribute(IAttribute.ABSTRACT,
					IAttribute.TRUE);
		}

		// Set Documentation
		final Documentation documentation = complexType.getDocumentation();
		if (documentation != null) {
			final IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					this.complexTypeElement, documentation);
		}

		// Add properties
		final IPropertyConverter propertyConverter = currentFactory
				.getPropertyConverter();
		final EList<Property> properties = complexType.getProperties();
		for (Property currProperty : properties) {
			propertyConverter.createElement(currentFactory, document,
					this.complexTypeElement, currProperty);
		}

		// append the Complex Type to the Document
		parentElement.appendChild(this.complexTypeElement);

	}

	@Override
	public Element getElement() {

		return this.complexTypeElement;
	}

	private String getTypeSchemaIdentifier(ComplexType baseType,
			ComplexType complexType) {
		String schemaIdentifier;
		// Get the used type namespace
		schemaIdentifier = ((Schema) baseType.eContainer()).getNamespace();
		// Get the current model schema namespace
		Schema currentSchema = (Schema) complexType.eContainer();
		if (currentSchema.getNamespace().equals(schemaIdentifier)) {
			// It means that the used type is not a reference type
			// Check if current model schema has alias
			String currentSchemaAlias = currentSchema.getAlias();
			if (currentSchemaAlias != null && !currentSchemaAlias.isEmpty()) {
				// Schema identifier will be an alias name
				schemaIdentifier = currentSchemaAlias;
			}
		} else {
			// It means that the used type is from reference type
			// Check if current model schema has using list
			EList<Using> usingsList = currentSchema.getUsings();
			if (usingsList != null) {
				for (Using usingEntry : usingsList) {
					// Check in using list the respective alias name that can be
					// used instead the namespace name
					if (usingEntry.getUsedNamespace().getNamespace()
							.equals(schemaIdentifier)) {
						schemaIdentifier = usingEntry.getAlias();
						break;
					}
				}
			}
		}
		return schemaIdentifier;
	}

}
