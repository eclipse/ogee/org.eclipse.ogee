/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import org.eclipse.ogee.model.odata.Role;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *  Responsible to convert End(Role) Set Domain model EObject
 *         into DOM End element
 */

public interface IEndConverter {

	/**
	 * Create Dom Documentation element and add it to his Dom parent
	 * 
	 * @param document
	 *            Insert Dom document
	 * @param parentElement
	 *            Insert Association End parent - Association
	 * @param end
	 *            Insert Association End(Role)
	 * @param currentFactory
	 *            Insert current factory instance
	 */
	public void createElement(IFormatConverterFactory currentFactory,
			Document document, Element parentElement, Role end);

	/**
	 * @return Association End
	 */
	public Element getElement();

}
