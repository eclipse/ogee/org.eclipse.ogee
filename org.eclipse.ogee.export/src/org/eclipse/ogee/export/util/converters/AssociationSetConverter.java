/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAssociationSetConverter;
import org.eclipse.ogee.export.util.converters.api.IAssociationSetEndConverter;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.ISymbol;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class AssociationSetConverter implements IAssociationSetConverter {

	private Element associationSetElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement,
			final AssociationSet associationSet) {

		// Create AssociationSet XML Element
		this.associationSetElement = document
				.createElement(ITag.ASSOCIATIONSET);
		// Set Attributes to AssociationSet element
		this.associationSetElement.setAttribute(IAttribute.NAME,
				associationSet.getName());
		Association association = associationSet.getAssociation();
		if (association != null) {
			String typeSchemaIdentifier = getTypeSchemaIdentifier(association,
					associationSet);
			this.associationSetElement.setAttribute(IAttribute.ASSOCIATION,
					typeSchemaIdentifier + ISymbol.DOT + association.getName());
		}

		// Create Documentation sub-element
		Documentation documentation = associationSet.getDocumentation();
		if (documentation != null) {
			IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					this.associationSetElement, documentation);
		}

		// Create End sub-elements
		EList<AssociationSetEnd> endList = associationSet.getEnds();
		for (AssociationSetEnd currEndList : endList) {
			// Create AssociationSet role nodes (end elements)
			IAssociationSetEndConverter associationSetEndConverter = currentFactory
					.getAssociationSetEndConverter();
			associationSetEndConverter.createElement(currentFactory, document,
					this.associationSetElement, currEndList);
		}

		// Add AssociationSet to EntityContainer
		parentElement.appendChild(this.associationSetElement);
	}

	@Override
	public Element getElement() {
		return this.associationSetElement;
	}

	private String getTypeSchemaIdentifier(Association association,
			AssociationSet associationSet) {
		String schemaIdentifier;
		// Get the used type namespace
		schemaIdentifier = ((Schema) association.eContainer()).getNamespace();
		// Get the current model schema namespace
		Schema currentSchema = ((Schema) ((EntityContainer) associationSet
				.eContainer()).eContainer());
		if (currentSchema.getNamespace().equals(schemaIdentifier)) {
			// It means that the used type is not a reference type
			// Check if current model schema has alias
			String currentSchemaAlias = currentSchema.getAlias();
			if (currentSchemaAlias != null && !currentSchemaAlias.isEmpty()) {
				// Schema identifier will be an alias name
				schemaIdentifier = currentSchemaAlias;
			}
		} else {
			// It means that the used type is from reference type
			// Check if current model schema has using list
			EList<Using> usingsList = currentSchema.getUsings();
			if (usingsList != null) {
				for (Using usingEntry : usingsList) {
					// Check in using list the respective alias name that can be
					// used instead the namespace name
					if (usingEntry.getUsedNamespace().getNamespace()
							.equals(schemaIdentifier)) {
						schemaIdentifier = usingEntry.getAlias();
						break;
					}
				}
			}
		}
		return schemaIdentifier;
	}
}
