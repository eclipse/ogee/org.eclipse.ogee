/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAssociationSetConverter;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IEntityContainerConverter;
import org.eclipse.ogee.export.util.converters.api.IEntitySetConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IFunctionImportConverter;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class EntityContainerConverter implements IEntityContainerConverter {

	private Element entityContainerElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement,
			final EntityContainer entityContainer) {

		// Create EntityContainer XML Element
		this.entityContainerElement = document
				.createElement(ITag.ENTITYCONTAINER);

		// Set Attributes to EntityContainer element
		this.entityContainerElement.setAttribute(IAttribute.NAME,
				entityContainer.getName());

		// Set IsDefaultEntityContainer attribute to entityContainerElement
		if (entityContainer.isDefault() == true) {
			// element
			this.entityContainerElement.setAttribute(
					IAttribute.ISDEFAULTENTITYCONTAINER, IAttribute.TRUE);
		}

		// Add EntityContainer to Schema
		parentElement.appendChild(this.entityContainerElement);

		// Create EntityContainer Sub-Elements

		// Add Documentation sub element
		Documentation documentation = entityContainer.getDocumentation();
		if (documentation != null) {
			IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					this.entityContainerElement, documentation);
		}
		// Create EntitySet elements
		EList<EntitySet> entitySets = entityContainer.getEntitySets();
		for (EntitySet currEntitySet : entitySets) {
			IEntitySetConverter entitySetConverter = currentFactory
					.getEntitySetConverter();
			entitySetConverter.createElement(currentFactory, document,
					this.entityContainerElement, currEntitySet);
		}

		// Create AssociationSet elements
		EList<AssociationSet> associationSet = entityContainer
				.getAssociationSets();
		for (AssociationSet currAssociationSet : associationSet) {
			IAssociationSetConverter associationSetConverter = currentFactory
					.getAssociationSetConverter();
			associationSetConverter.createElement(currentFactory, document,
					this.entityContainerElement, currAssociationSet);
		}

		// Create FunctionImport elements
		EList<FunctionImport> functionImportSet = entityContainer
				.getFunctionImports();
		for (FunctionImport currFunctionImportSet : functionImportSet) {
			IFunctionImportConverter functionImportConverter = currentFactory
					.getFunctionImportConverter();
			functionImportConverter.createElement(currentFactory, document,
					this.entityContainerElement, currFunctionImportSet);
		}

	}

	/**
	 * 
	 */
	@Override
	public Element getElement() {
		return this.entityContainerElement;
	}

}
