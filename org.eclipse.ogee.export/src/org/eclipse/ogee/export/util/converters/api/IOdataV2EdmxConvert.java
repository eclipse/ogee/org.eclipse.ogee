/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import org.eclipse.ogee.model.odata.EDMXSet;
import org.w3c.dom.Document;

/**
 * 
 *         This API is responsible the convertion from EdmxSet to OData V2 file
 *         format
 * 
 */
public interface IOdataV2EdmxConvert {
	/**
	 * Create DOM Model document from Edmxset
	 * 
	 * @param EdmxSet
	 *            - Domain model
	 */
	public Document createModel(EDMXSet edmxSet);

}
