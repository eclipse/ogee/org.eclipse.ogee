/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IFunctionImportParameterConverter;
import org.eclipse.ogee.export.util.converters.api.ISymbol;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IParameterTypeUsage;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class FunctionImportParameterConverter implements IFunctionImportParameterConverter {

	private Element parameterElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory, Document document, Element parentElement,
			Parameter functionImportParameter) {

		// Create FunctionImportParameter XML Element
		this.parameterElement = document.createElement(ITag.PARAMETER);
		this.parameterElement.setAttribute(IAttribute.NAME, functionImportParameter.getName());
		// Add Type attribute.
		IParameterTypeUsage parameterTypeUsage = functionImportParameter.getType();
		if (parameterTypeUsage != null) {
			if (parameterTypeUsage instanceof SimpleTypeUsage) {
				// FunctionImport parameter from Simple type
				SimpleType simpleType = ((SimpleTypeUsage) parameterTypeUsage).getSimpleType();
				if (simpleType != null) {
					EDMTypes simpleTypeEdmType = simpleType.getType();
					if (simpleTypeEdmType != null) {
						this.parameterElement.setAttribute(IAttribute.TYPE, simpleTypeEdmType.getLiteral());
					}

					int maxLength = simpleType.getMaxLength();
					if (maxLength >= 0) {
						this.parameterElement.setAttribute(IAttribute.MAXLENGTH, String.valueOf(maxLength));
					}

					if (!(simpleTypeEdmType.equals(EDMTypes.BOOLEAN) || simpleTypeEdmType.equals(EDMTypes.STRING))) {
						int precision = simpleType.getPrecision();
						if (precision >= 0) {
							this.parameterElement.setAttribute(IAttribute.PRECISION, String.valueOf(precision));
						}

						int scale = simpleType.getScale();
						if (scale >= 0) {
							this.parameterElement.setAttribute(IAttribute.SCALE, String.valueOf(scale));
						}
					}

				}

			}
			if (parameterTypeUsage instanceof ComplexTypeUsage) {
				// FunctionImport parameter from Complex type
				ComplexType complexType = ((ComplexTypeUsage) parameterTypeUsage).getComplexType();
				if (complexType != null) {
					String typeSchemaIdentifier = getTypeSchemaIdentifier(complexType, functionImportParameter);
					this.parameterElement.setAttribute(IAttribute.TYPE,
							typeSchemaIdentifier + ISymbol.DOT + complexType.getName());
				}

			}

			if (parameterTypeUsage instanceof EnumTypeUsage) {
				// FunctionImport parameter from Enum type
				EnumType enumType = ((EnumTypeUsage) parameterTypeUsage).getEnumType();
				if (enumType != null) {
					this.parameterElement.setAttribute(IAttribute.TYPE, enumType.getUnderlyingType().getLiteral());
				}

			}
			// based on the assumption that if complex type is supported as a
			// type in odata V2, then entity type should be supported as well.
			if (parameterTypeUsage instanceof EntityTypeUsage) {
				// FunctionImport parameter from EntityType
				EntityType entityType = ((EntityTypeUsage) parameterTypeUsage).getEntityType();
				if (entityType != null) {
					String typeSchemaIdentifier = getTypeSchemaIdentifier(entityType, functionImportParameter);
					this.parameterElement.setAttribute(IAttribute.TYPE,
							typeSchemaIdentifier + ISymbol.DOT + entityType.getName());
				}

			}
		}

		// Add Mode Attribute
		this.parameterElement.setAttribute(IAttribute.MODE, IAttribute.IN);

		parentElement.appendChild(this.parameterElement);

		// Create Documentation sub-element
		Documentation documentation = functionImportParameter.getDocumentation();
		if (documentation != null) {
			IDocumentationConverter documentationConverter = currentFactory.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document, this.parameterElement, documentation);
		}
	}

	@Override
	public Element getElement() {
		return this.parameterElement;
	}

	public String getTypeSchemaIdentifier(Object typeObject, Parameter functionImportParameter) {
		String schemaIdentifier = null;
		// Get the used type namespace
		if (typeObject instanceof ComplexType) {
			ComplexType complexType = (ComplexType) typeObject;
			schemaIdentifier = ((Schema) complexType.eContainer()).getNamespace();

		} else if (typeObject instanceof EntityType) {
			EntityType entityType = (EntityType) typeObject;
			schemaIdentifier = ((Schema) entityType.eContainer()).getNamespace();

		} else if (typeObject instanceof EnumType) {
			EnumType enumType = (EnumType) typeObject;
			schemaIdentifier = ((Schema) enumType.eContainer()).getNamespace();
		}

		// Get the current model schema namespace
		Schema currentSchema = ((Schema) ((EntityContainer) ((FunctionImport) (functionImportParameter.eContainer()))
				.eContainer()).eContainer());
		if (currentSchema.getNamespace().equals(schemaIdentifier)) {
			// It means that the used type is not a reference type
			// Check if current model schema has alias
			String currentSchemaAlias = currentSchema.getAlias();
			if (currentSchemaAlias != null && !currentSchemaAlias.isEmpty()) {
				// Schema identifier will be an alias name
				schemaIdentifier = currentSchemaAlias;
			}
		} else {
			// It means that the used type is from reference type
			// Check if current model schema has using list
			EList<Using> usingsList = currentSchema.getUsings();
			if (usingsList != null) {
				for (Using usingEntry : usingsList) {
					// Check in using list the respective alias name that can be
					// used instead the namespace name
					if (usingEntry.getUsedNamespace().getNamespace().equals(schemaIdentifier)) {
						schemaIdentifier = usingEntry.getAlias();
						break;
					}
				}
			}
		}
		return schemaIdentifier;
	}

}
