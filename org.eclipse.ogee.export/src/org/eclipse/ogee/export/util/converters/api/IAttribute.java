/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

public interface IAttribute {

	public static final String NAMESPACE = "Namespace"; //$NON-NLS-1$
	public static final String ALIAS = "Alias"; //$NON-NLS-1$
	public static final String NAME = "Name"; //$NON-NLS-1$
	public static final String BASETYPE = "BaseType"; //$NON-NLS-1$
	public static final String ABSTRACT = "Abstract"; //$NON-NLS-1$
	public static final String ANNOTATIONATTRIBUTE = "AnnotationAttribute"; //$NON-NLS-1$
	public static final String TYPE = "Type"; //$NON-NLS-1$
	public static final String NULLABLE = "Nullable"; //$NON-NLS-1$
	public static final String DEFAULTVALUE = "DefaultValue"; //$NON-NLS-1$
	public static final String MAXLENGTH = "MaxLength"; //$NON-NLS-1$
	public static final String FIELDLENGTH = "FieldLength"; //$NON-NLS-1$
	public static final String PRECISION = "Precision"; //$NON-NLS-1$
	public static final String RELATIONSHIP = "Relationship"; //$NON-NLS-1$
	public static final String TOROLE = "ToRole"; //$NON-NLS-1$
	public static final String FROMROLE = "FromRole"; //$NON-NLS-1$
	public static final String ROLE = "Role"; //$NON-NLS-1$
	public static final String MULTIPLICITY = "Multiplicity"; //$NON-NLS-1$
	public static final String EXTENDS = "Extends"; //$NON-NLS-1$
	public static final String RETURNTYPE = "ReturnType"; //$NON-NLS-1$
	public static final String METHODACCESS = "MethodAccess"; //$NON-NLS-1$
	public static final String MODE = "Mode"; //$NON-NLS-1$
	public static final String DOCUMENTATION = "Documentation"; //$NON-NLS-1$
	public static final String ASSOCIATION = "Association"; //$NON-NLS-1$
	public static final String ENTITYTYPE = "EntityType"; //$NON-NLS-1$
	public static final String ENTITYSET = "EntitySet"; //$NON-NLS-1$
	public static final String SCALE = "Scale"; //$NON-NLS-1$
	public static final String ISDEFAULTENTITYCONTAINER = "m:IsDefaultEntityContainer"; //$NON-NLS-1$
	public static final String IN = "In"; //$NON-NLS-1$
	public static final String FALSE = "false"; //$NON-NLS-1$
	public static final String TRUE = "true"; //$NON-NLS-1$
	public static final String FIXEDLENGTH = "FixedLength"; //$NON-NLS-1$
	public static final String VERSION = "Version"; //$NON-NLS-1$
	public static final String VERSION_NUM_1_0 = "1.0"; //$NON-NLS-1$
	public static final String EDMX = "edmx"; //$NON-NLS-1$
	public static final String VERSION_NUM_2_0 = "2.0"; //$NON-NLS-1$
	public static final String EDM_NS = "http://schemas.microsoft.com/ado/2008/09/edm"; //$NON-NLS-1$
	public static final String XMLNS = "xmlns"; //$NON-NLS-1$
	public static final String CONCURRENCY_MODE = "ConcurrencyMode"; //$NON-NLS-1$
	public static final String CUNNC_FIXED = "Fixed"; //$NON-NLS-1$
	public static final String HTTPMETHOD = "m:HttpMethod"; //$NON-NLS-1$
	public static final String POST = "POST"; //$NON-NLS-1$
	public static final String GET = "GET"; //$NON-NLS-1$
	public static final String EDM_NS_V3 = "http://schemas.microsoft.com/ado/2009/11/edm"; //$NON-NLS-1$
	public static final String VERSION_NUM_3_0 = "3.0"; //$NON-NLS-1$
	public static final String URL = "Url"; //$NON-NLS-1$

}
