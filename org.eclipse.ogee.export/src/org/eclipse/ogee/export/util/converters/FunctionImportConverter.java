/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IFunctionImportConverter;
import org.eclipse.ogee.export.util.converters.api.IFunctionImportParameterConverter;
import org.eclipse.ogee.export.util.converters.api.ISymbol;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.Using;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class FunctionImportConverter implements IFunctionImportConverter {

	private Element functionImportElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement,
			final FunctionImport functionImport) {

		// Create FunctionImport XML Element
		this.functionImportElement = document
				.createElement(ITag.FUNCTIONIMPORT);
		// Set Attributes to FunctionImport element
		this.functionImportElement.setAttribute(IAttribute.NAME,
				functionImport.getName());

		if (functionImport.isSideEffecting() == true) {
			this.functionImportElement.setAttribute(IAttribute.HTTPMETHOD,
					IAttribute.POST);
		} else {
			this.functionImportElement.setAttribute(IAttribute.HTTPMETHOD,
					IAttribute.GET);
		}

		IFunctionReturnTypeUsage propertyTypeUsage = functionImport
				.getReturnType();

		if (propertyTypeUsage != null) {

			// entity return type
			if (propertyTypeUsage instanceof ReturnEntityTypeUsage) {
				ReturnEntityTypeUsage entityTypeUsageImpl = (ReturnEntityTypeUsage) (propertyTypeUsage);
				EntityType entityType = entityTypeUsageImpl.getEntityType();
				if (entityType != null) {
					// Set Entity Set Attribute
					EntitySet entitySet = entityTypeUsageImpl.getEntitySet();
					if (entitySet != null) {
						this.functionImportElement.setAttribute(
								IAttribute.ENTITYSET, entitySet.getName());
					}

					String typeSchemaIdentifier = getTypeSchemaIdentifier(
							entityType, functionImport);
					if (entityTypeUsageImpl.isCollection() == true) {
						// Set Return Type Attribute as a collection of
						// entityTypes
						this.functionImportElement
								.setAttribute(
										IAttribute.RETURNTYPE,
										ISymbol.COLLECTION
												+ ISymbol.LEFT_BRACKET
												+ typeSchemaIdentifier
												+ ISymbol.DOT
												+ (entityType.getName() + ISymbol.RIGHT_BRACKET));

					} else {
						this.functionImportElement.setAttribute(
								IAttribute.RETURNTYPE, typeSchemaIdentifier
										+ ISymbol.DOT + entityType.getName());
					}
				}
			}

			// simple return type
			if (propertyTypeUsage instanceof SimpleTypeUsage) {
				SimpleTypeUsage simpleTypeUsage = (SimpleTypeUsage) (propertyTypeUsage);
				SimpleType simpleType = simpleTypeUsage.getSimpleType();
				if (simpleType != null) {
					EDMTypes simpleTypeEdmType = simpleType.getType();
					if (simpleTypeEdmType != null) {
						String simpleTypeName = simpleTypeEdmType.getLiteral();
						if (simpleTypeUsage.isCollection() == true) {
							this.functionImportElement.setAttribute(
									IAttribute.RETURNTYPE, ISymbol.COLLECTION
											+ ISymbol.LEFT_BRACKET
											+ simpleTypeName
											+ ISymbol.RIGHT_BRACKET);
						} else {
							this.functionImportElement.setAttribute(
									IAttribute.RETURNTYPE, simpleTypeName);
						}
					}
				}
			}

			// complex return type
			if (propertyTypeUsage instanceof ComplexTypeUsage) {
				ComplexTypeUsage complexTypeUsage = (ComplexTypeUsage) (propertyTypeUsage);
				ComplexType complexType = complexTypeUsage.getComplexType();
				if (complexType != null) {
					String typeSchemaIdentifier = getTypeSchemaIdentifier(
							complexType, functionImport);
					if (complexTypeUsage.isCollection() == true) {

						this.functionImportElement.setAttribute(
								IAttribute.RETURNTYPE, ISymbol.COLLECTION
										+ ISymbol.LEFT_BRACKET
										+ typeSchemaIdentifier + ISymbol.DOT
										+ complexType.getName()
										+ ISymbol.RIGHT_BRACKET);
					} else {
						this.functionImportElement.setAttribute(
								IAttribute.RETURNTYPE, typeSchemaIdentifier
										+ ISymbol.DOT + complexType.getName());
					}
				}
			}

			// Enum return type (since enum exists only in V3, in V2 and V24Sap
			// Underlying type of enum will be returned.)
			if (propertyTypeUsage instanceof EnumTypeUsage) {
				EnumTypeUsage enumTypeUsage = (EnumTypeUsage) (propertyTypeUsage);
				EnumType enumType = enumTypeUsage.getEnumType();
				if (enumType != null) {
					EDMTypes enumTypeEdmType = enumType.getUnderlyingType();
					if (enumTypeEdmType != null) {
						String enumTypeName = enumTypeEdmType.getLiteral();
						if (enumTypeUsage.isCollection() == true) {
							this.functionImportElement.setAttribute(
									IAttribute.RETURNTYPE, ISymbol.COLLECTION
											+ ISymbol.LEFT_BRACKET
											+ enumTypeName
											+ ISymbol.RIGHT_BRACKET);
						} else {
							this.functionImportElement.setAttribute(
									IAttribute.RETURNTYPE, enumTypeName);
						}
					}
				}
			}

		}

		// Create Documentation sub-element
		Documentation documentation = functionImport.getDocumentation();
		if (documentation != null) {
			IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					this.functionImportElement, documentation);
		}

		// Create Parameter sub-elements
		EList<Parameter> parameterList = functionImport.getParameters();
		for (Parameter currParameter : parameterList) {
			// Create Function Import parameter elements
			IFunctionImportParameterConverter functionImportParameterConverter = currentFactory
					.getFunctionImportParameterConverter();
			functionImportParameterConverter.createElement(currentFactory,
					document, this.functionImportElement, currParameter);
		}

		// Add Function Import to EntityContainer
		parentElement.appendChild(this.functionImportElement);

	}

	@Override
	public Element getElement() {
		return this.functionImportElement;
	}

	public String getTypeSchemaIdentifier(Object typeObject,
			FunctionImport functionImport) {
		String schemaIdentifier = null;
		// Get the used type namespace
		if (typeObject instanceof ComplexType) {
			ComplexType complexType = (ComplexType) typeObject;
			schemaIdentifier = ((Schema) complexType.eContainer())
					.getNamespace();
		} else if (typeObject instanceof EntityType) {
			EntityType entityType = (EntityType) typeObject;
			schemaIdentifier = ((Schema) entityType.eContainer())
					.getNamespace();
		} else if (typeObject instanceof EnumType) {
			EnumType enumType = (EnumType) typeObject;
			schemaIdentifier = ((Schema) enumType.eContainer()).getNamespace();
		}

		// Get the current model schema namespace
		Schema currentSchema = ((Schema) ((EntityContainer) functionImport
				.eContainer()).eContainer());
		if (currentSchema.getNamespace().equals(schemaIdentifier)) {
			// It means that the used type is not a reference type
			// Check if current model schema has alias
			String currentSchemaAlias = currentSchema.getAlias();
			if (currentSchemaAlias != null && !currentSchemaAlias.isEmpty()) {
				// Schema identifier will be an alias name
				schemaIdentifier = currentSchemaAlias;
			}
		} else {
			// It means that the used type is from reference type
			// Check if current model schema has using list
			EList<Using> usingsList = currentSchema.getUsings();
			if (usingsList != null) {
				for (Using usingEntry : usingsList) {
					// Check in using list the respective alias name that can be
					// used instead the namespace name
					if (usingEntry.getUsedNamespace().getNamespace()
							.equals(schemaIdentifier)) {
						schemaIdentifier = usingEntry.getAlias();
						break;
					}
				}
			}
		}
		return schemaIdentifier;
	}
}
