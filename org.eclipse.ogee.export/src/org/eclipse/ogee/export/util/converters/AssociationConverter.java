/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.export.util.converters.api.IAssociationConverter;
import org.eclipse.ogee.export.util.converters.api.IAttribute;
import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IEndConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.IReferentialConstraintConverter;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Role;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class AssociationConverter implements IAssociationConverter {

	private Element associationElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement,
			final Association association) {

		this.associationElement = document.createElement(ITag.ASSOCIATION);

		// Set Name
		this.associationElement.setAttribute(IAttribute.NAME,
				association.getName());

		// Set Documentation
		final Documentation documentation = association.getDocumentation();
		if (documentation != null) {
			final IDocumentationConverter documentationConverter = currentFactory
					.getDocumatationConverter();
			documentationConverter.createElement(currentFactory, document,
					this.associationElement, documentation);
		}

		// Set Ends
		final EList<Role> ends = association.getEnds();
		if (ends != null) {
			final IEndConverter endConverter = currentFactory.getEndConverter();
			for (Role currEnd : ends) {
				endConverter.createElement(currentFactory, document,
						this.associationElement, currEnd);
			}

		}
		// set Referential Constraint
		final ReferentialConstraint referentialConstraint = association
				.getReferentialConstraint();
		if (referentialConstraint != null) {
			final IReferentialConstraintConverter referentialConstraintConverter = currentFactory
					.getReferentialConstraintConverter();
			referentialConstraintConverter.createElement(currentFactory,
					document, this.associationElement, referentialConstraint);
		}

		parentElement.appendChild(this.associationElement);
	}

	@Override
	public Element getElement() {
		return this.associationElement;
	}

}
