/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

public interface ISymbol {
	public static final String DOT = "."; //$NON-NLS-1$
	public static final String COLLECTION = "Collection"; //$NON-NLS-1$
	public static final String LEFT_BRACKET = "("; //$NON-NLS-1$
	public static final String RIGHT_BRACKET = ")"; //$NON-NLS-1$
}
