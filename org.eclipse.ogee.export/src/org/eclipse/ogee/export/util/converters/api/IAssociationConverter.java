/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters.api;

import org.eclipse.ogee.model.odata.Association;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * 
 * The interface "IAssociationConverter" is responsible for converting
 * Association from domain model Object into DOM Association element
 */
public interface IAssociationConverter {

	/**
	 * Create DOM Association element and add it to its DOM parent
	 * 
	 * @param document
	 *            - DOM document
	 * @param parentElement
	 *            - Association parent (Schema)
	 * @param association
	 *            - Association from domain model
	 * @param currentFactory
	 *            - Current conversion factory instance
	 */
	public void createElement(IFormatConverterFactory currentFactory,
			Document document, Element parentElement, Association association);

	/**
	 * @return Association DOM element
	 */
	public Element getElement();

}
