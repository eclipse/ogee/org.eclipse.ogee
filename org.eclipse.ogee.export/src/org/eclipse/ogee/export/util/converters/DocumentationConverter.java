/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.export.util.converters;

import org.eclipse.ogee.export.util.converters.api.IDocumentationConverter;
import org.eclipse.ogee.export.util.converters.api.IFormatConverterFactory;
import org.eclipse.ogee.export.util.converters.api.ITag;
import org.eclipse.ogee.model.odata.Documentation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DocumentationConverter implements IDocumentationConverter {

	private Element documentationElement;

	@Override
	public void createElement(final IFormatConverterFactory currentFactory,
			final Document document, final Element parentElement,
			final Documentation documentation) {

		this.documentationElement = document.createElement(ITag.DOCUMENTATION);

		// Set Summary
		Element summaryElement = document.createElement(ITag.SUMMARY);
		summaryElement.setTextContent(documentation.getSummary());
		this.documentationElement.appendChild(summaryElement);

		// set Long Description
		Element longDescElement = document.createElement(ITag.LONGDESCRIPTION);
		longDescElement.setTextContent(documentation.getLongDescription());
		this.documentationElement.appendChild(longDescElement);

		// append the Documentation to the Document
		parentElement.appendChild(this.documentationElement);

	}

	@Override
	public Element getElement() {
		// TODO Auto-generated method stub
		return this.documentationElement;
	}

}
