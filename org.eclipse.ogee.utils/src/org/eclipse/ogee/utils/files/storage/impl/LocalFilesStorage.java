/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.files.storage.impl;

import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.eclipse.ogee.client.util.Util;
import org.eclipse.ogee.utils.files.storage.api.FilesStorageException;
import org.eclipse.ogee.utils.files.storage.api.IFilesStorage;
import org.eclipse.ogee.utils.nls.messages.FrameworkUtilsMessages;
import org.eclipse.osgi.util.NLS;

/**
 * Represents a local files storage.
 */
public class LocalFilesStorage implements IFilesStorage {
	/**
	 * the main storage folder
	 */
	private static final String mainStorageFolder = System
			.getProperty("user.home") + File.separator + ".data-storage" + File.separator; //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * UTF 8 encoding
	 */
	private static final String UTF_8 = "UTF-8"; //$NON-NLS-1$
	/**
	 * file locks container. One lock object for each file.
	 */
	private ConcurrentHashMap<String, ReentrantReadWriteLock> connParamsLocks;

	/**
	 * SingletonHolder is loaded on the first execution of
	 * LocalFilesStorage.getInstance() or the first access to
	 * SingletonHolder.INSTANCE, not before.
	 */
	private static class SingletonHolder {
		public static final IFilesStorage INSTANCE = new LocalFilesStorage();
	}

	/**
	 * Private constructor. Loads previously stored files and prepares file
	 * locks container.
	 */
	private LocalFilesStorage() {
		this.connParamsLocks = new ConcurrentHashMap<String, ReentrantReadWriteLock>();

		// create main storage folder
		File mainFolder = new File(mainStorageFolder);
		boolean created = mainFolder.mkdir();
		// if the main folder already exists
		if (!created) { // get all previously created folders
			File[] storedFolders = mainFolder.listFiles();
			if (storedFolders == null) {
				return;
			}

			for (File folder : storedFolders) {
				if (folder.isDirectory()) { // get all previously created folder
											// files
					File[] storedFiles = folder.listFiles();
					if (storedFiles == null) {
						return;
					}

					for (File storedFile : storedFiles) {
						if (storedFile.isFile()) { // create lock for each
													// stored file
							this.connParamsLocks.put(
									storedFile.getAbsolutePath(),
									new ReentrantReadWriteLock());
						}
					}
				}
			}
		}
	}

	/**
	 * Returns the LocalFilesStorage instance.
	 * 
	 * @return the LocalFilesStorage instance.
	 */
	public static IFilesStorage getInstance() {
		return SingletonHolder.INSTANCE;
	}

	@Override
	public void store(String folderName, String fileName, String content)
			throws FilesStorageException {
		validateInput("store", "folderName", folderName); //$NON-NLS-1$ //$NON-NLS-2$
		validateInput("store", "fileName", fileName); //$NON-NLS-1$ //$NON-NLS-2$
		validateInput("store", "content", content); //$NON-NLS-1$ //$NON-NLS-2$

		// get folder path
		StringBuilder sbFolderPath = getFolderPath(folderName);
		// store file only if folder exists.
		// It should be created only by "this.create" method.
		File folder = new File(sbFolderPath.toString());

		if (folder.exists()) { // get file path
			String filePath = createFilePath(sbFolderPath, fileName);
			// get file lock object
			ReentrantReadWriteLock connParamsLock = this.connParamsLocks
					.get(filePath);
			if (connParamsLock == null) {
				connParamsLock = new ReentrantReadWriteLock();
			}

			try {
				connParamsLock.writeLock().lock();
				this.connParamsLocks.put(filePath, connParamsLock);
				// store the content in the specified location
				storeFileContent(filePath, content);
			} finally {
				connParamsLock.writeLock().unlock();
			}
		}
	}

	@Override
	public String get(String folderName, String fileName)
			throws FilesStorageException {
		validateInput("get", "folderName", folderName); //$NON-NLS-1$ //$NON-NLS-2$
		validateInput("get", "fileName", fileName); //$NON-NLS-1$ //$NON-NLS-2$

		String content = null;

		// get folder path
		StringBuilder sbFolderPath = getFolderPath(folderName);
		// get file path
		String filePath = createFilePath(sbFolderPath, fileName);
		// get file lock object
		ReentrantReadWriteLock connParamsLock = this.connParamsLocks
				.get(filePath);
		if (connParamsLock == null) { // should not read a file without the file
										// lock object
			return null;
		}

		// should not read the file if it does not exist
		File file = new File(filePath);
		if (file.exists()) {
			try {
				connParamsLock.readLock().lock();
				// get file content
				content = getFileContent(filePath);
			} finally {
				connParamsLock.readLock().unlock();
			}
		}

		return content;
	}

	/**
	 * Returns the specified folder absolute path.
	 * 
	 * @param folderName
	 *            - the folder name
	 * @return the specified folder absolute path.
	 */
	private StringBuilder getFolderPath(String folderName) {
		StringBuilder sb = new StringBuilder();

		sb.append(mainStorageFolder).append(normalize(folderName));

		return sb;
	}

	/**
	 * Returns the specified file absolute path.
	 * 
	 * @param sbFolderPath
	 *            - file's folder absolute path
	 * @param fileName
	 *            - the file name
	 * @return the specified file absolute path.
	 */
	private String createFilePath(StringBuilder sbFolderPath, String fileName) {
		StringBuilder sb = new StringBuilder(sbFolderPath);

		sb.append(File.separator).append(normalize(fileName));

		return sb.toString();
	}

	@Override
	public synchronized void delete(String folderName)
			throws FilesStorageException {
		validateInput("delete", "folderName", folderName); //$NON-NLS-1$ //$NON-NLS-2$

		String folderPath = getFolderPath(folderName).toString();
		File folder = new File(folderPath);
		if (folder.isDirectory()) {
			delete(folder);

			Set<String> keys = this.connParamsLocks.keySet();
			for (String key : keys) {
				if (key.startsWith(folderPath)) {
					this.connParamsLocks.remove(key);
				}
			}
		}
	}

	/**
	 * Deletes the direcory from the file system
	 * 
	 * @param file
	 *            directory to delete
	 */
	private void delete(File file) {
		if (file.isDirectory()) {
			// directory is empty, then delete it
			if (file.list().length == 0) {
				file.delete();
			} else {
				// list all the directory contents
				String files[] = file.list();
				for (String fileName : files) {
					// construct the file structure
					File fileDelete = new File(file, fileName);

					// recursive delete
					delete(fileDelete);
				}

				// check the directory again, if empty then delete it
				if (file.list().length == 0) {
					file.delete();
				}
			}

		} else // is file
		{
			file.delete();
		}
	}

	/**
	 * Stores string as to the specified file.
	 * 
	 * @param filePath
	 *            - file path.
	 * @param fileAsString
	 *            - string value.
	 * @throws FilesStorageException
	 */
	private void storeFileContent(String filePath, String fileAsString)
			throws FilesStorageException {
		ByteArrayInputStream bain = null;
		FileOutputStream fos = null;

		try {
			bain = new ByteArrayInputStream(fileAsString.getBytes(UTF_8));

			File file = new File(filePath);
			fos = new FileOutputStream(file);
			byte buf[] = new byte[1024];

			int len;
			while ((len = bain.read(buf)) > 0) {
				fos.write(buf, 0, len);
			}

			fos.flush();
		} catch (IOException e) {
			throw new FilesStorageException(e);
		} finally {
			closeStream(fos);
			closeStream(bain);
		}
	}

	/**
	 * Closes stream.
	 * 
	 * @param stream
	 *            - the stream
	 * @throws FilesStorageException
	 */
	private void closeStream(Closeable stream) throws FilesStorageException {
		try {
			if (stream != null) {
				stream.close();
			}
		} catch (IOException e) {
			throw new FilesStorageException(e);
		}
	}

	/**
	 * Replaces special characters in the specified string input value. '/' ,
	 * '?', ':', '.' will be replaced to '#'.
	 * 
	 * @param inputValue
	 *            - string input value
	 * @return normalized input value as StringBuilder.
	 */
	public static StringBuilder normalize(String inputValue) {
		StringBuilder sb = new StringBuilder();

		char[] charArr = inputValue.toLowerCase().toCharArray();
		for (int i = 0; i < charArr.length; i++) {
			// Single character case
			if (charArr[i] == '/' || charArr[i] == '?' || charArr[i] == '*'
					|| charArr[i] == '<' || charArr[i] == '>'
					|| charArr[i] == ':' || charArr[i] == '.'
					|| charArr[i] == '\\' || charArr[i] == '|'
					|| charArr[i] == '"') {
				sb.append('#');
			} else {
				sb.append(inputValue.charAt(i));
			}
		}

		return sb;
	}

	/**
	 * Reads file content to string.
	 * 
	 * @param fileAbsolutePath
	 *            - file absolute path.
	 * @return file content as string
	 * @throws FilesStorageException
	 */
	private String getFileContent(String fileAbsolutePath)
			throws FilesStorageException {
		String dataStr = null;

		FileInputStream fis = null;

		try {
			fis = new FileInputStream(fileAbsolutePath);

			dataStr = Util.convertStreamToString(fis);

			// byte buffer[] = new byte[8192];
			// int read = 0;
			//
			// StringBuilder responseBody = new StringBuilder();
			// while ((read = fis.read(buffer)) != -1)
			// {
			// responseBody.append(new String(buffer, 0, read, UTF_8));
			// }
			//
			// dataStr = responseBody.toString();
		} catch (IOException e) {
			throw new FilesStorageException(e);
		}
		// finally
		// {
		// closeStream(fis);
		// }
		//
		return dataStr;
	}

	@Override
	public synchronized void create(String folderName)
			throws FilesStorageException {
		validateInput("create", "folderName", folderName); //$NON-NLS-1$ //$NON-NLS-2$

		File mainFolder = new File(mainStorageFolder);
		mainFolder.mkdir();

		StringBuilder sbFolderPath = getFolderPath(folderName);
		File newFolder = new File(sbFolderPath.toString());

		newFolder.mkdir();
	}

	/**
	 * Validates input parameter. An input parameter value cannot be null. If an
	 * input parameter is of type string, it cannot be empty.
	 * 
	 * @param methodName
	 *            - method name
	 * @param paramName
	 *            - parameter name
	 * @param paramValue
	 *            - parameter value
	 * @throws FilesStorageException
	 *             if the input parameter is not valid.
	 */
	private void validateInput(String methodName, String paramName,
			String paramValue) throws FilesStorageException {
		if (paramValue == null) {
			throw new FilesStorageException(NLS.bind(
					FrameworkUtilsMessages.LocalFilesStorage_1, paramName,
					methodName));
		}

		if (paramValue.trim().isEmpty()) {
			throw new FilesStorageException(NLS.bind(
					FrameworkUtilsMessages.LocalFilesStorage_2, paramName,
					methodName));
		}
	}

	@Override
	public synchronized void clean() throws FilesStorageException {
		File mainDir = new File(mainStorageFolder);
		delete(mainDir);
	}

	@Override
	public synchronized void delete(String folderName, String fileName)
			throws FilesStorageException {
		validateInput("exists", "folderName", folderName); //$NON-NLS-1$ //$NON-NLS-2$
		validateInput("exists", "fileName", fileName); //$NON-NLS-1$ //$NON-NLS-2$

		StringBuilder sbFolderPath = getFolderPath(folderName);
		File folder = new File(sbFolderPath.toString());
		if (folder.exists()) {
			String filePath = createFilePath(sbFolderPath, fileName);
			File file = new File(filePath);
			file.delete();

			this.connParamsLocks.remove(file.getAbsolutePath());
		}
	}

	@Override
	public boolean exists(String folderName, String fileName)
			throws FilesStorageException {
		validateInput("exists", "folderName", folderName); //$NON-NLS-1$ //$NON-NLS-2$
		validateInput("exists", "fileName", fileName); //$NON-NLS-1$ //$NON-NLS-2$

		StringBuilder sbFolderPath = getFolderPath(folderName);
		File folder = new File(sbFolderPath.toString());
		if (folder.exists()) {
			String filePath = createFilePath(sbFolderPath, fileName);

			File file = new File(filePath);
			boolean fileExists = file.exists();

			return fileExists;
		}

		return false;
	}
}
