/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.files.storage.api;

/**
 * Represents an off line files storage.
 */
public interface IFilesStorage {
	/**
	 * Stores the given data as a file in the specified folder. The data is not
	 * stored if the folder does not exists. Overrides the file if it already
	 * exists in the folder.
	 * 
	 * @param folderName
	 *            - the folder name.
	 * @param fileName
	 *            - the file name.
	 * @param fileContent
	 *            - file content as string.
	 * @throws FilesStorageException
	 */
	public void store(String folderName, String fileName, String fileContent)
			throws FilesStorageException;

	/**
	 * Returns the specified file content from the folder as a String.
	 * 
	 * @param folderName
	 *            - the folder name.
	 * @param fileName
	 *            - the file name.
	 * @return the specified file content as String from the folder, or null if
	 *         the file was not found.
	 * @throws FilesStorageException
	 */
	public String get(String folderName, String fileName)
			throws FilesStorageException;

	/**
	 * Deletes folder and all its files if the specified folder exists.
	 * 
	 * @param folderName
	 *            - the folder name.
	 * @throws FilesStorageException
	 */
	public void delete(String folderName) throws FilesStorageException;

	/**
	 * Creates a new empty folder if the specified folder does not exist.
	 * 
	 * @param folderName
	 *            - the folder name.
	 * @throws FilesStorageException
	 */
	public void create(String folderName) throws FilesStorageException;

	/**
	 * Deletes all stored files.
	 * 
	 * @throws FilesStorageException
	 */
	public void clean() throws FilesStorageException;

	/**
	 * Deletes the specified file from the folder.
	 * 
	 * @param folderName
	 *            folder to delete from
	 * @param fileName
	 *            file to delete
	 */
	public void delete(String folderName, String fileName)
			throws FilesStorageException;

	public boolean exists(String folderName, String fileName)
			throws FilesStorageException;
}
