/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.nls.messages;

import org.eclipse.osgi.util.NLS;

public class FrameworkUtilsMessages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.ogee.utils.nls.messages.framework"; //$NON-NLS-1$

	public static String FilesOverwriteDialog_0;

	public static String FileUtils_0;

	public static String FileUtils_1;

	public static String FileUtils_12;

	public static String FileUtils_14;

	public static String FileUtils_15;

	public static String FileUtils_16;

	public static String FileUtils_18;

	public static String FileUtils_19;

	public static String GenericServiceValidator_1;

	public static String GenericServiceValidator_2;

	public static String GenericServiceValidator_3;

	public static String GenericServiceValidator_4;

	public static String GenericServiceValidator_5;

	public static String GenericServiceValidator_6;

	public static String GenericServiceValidator_8;

	public static String GenericServiceValidator_9;

	public static String GenericServiceValidator_10;

	public static String LocalFilesStorage_1;

	public static String LocalFilesStorage_2;

	public static String Logger_0;

	public static String Logger_1;

	public static String ODATA_CATEGORY_PREFERENCE_TEXT0;

	public static String ProjectUtils_0;

	public static String ProjectUtils_1;

	public static String ProjectUtils_2;

	public static String Result_0;

	public static String ServiceDocumentFileValidator_0;

	public static String ServiceDocumentFileValidator_1;

	public static String ServiceDocumentFileValidator_2;

	public static String ServiceDocumentFileValidator_3;

	public static String ServiceMetadataFileValidator_0;

	public static String ServiceMetadataFileValidator_1;

	public static String ServiceMetadataFileValidator_2;

	public static String InvalidXMLResponse;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, FrameworkUtilsMessages.class);
	}

	private FrameworkUtilsMessages() {
	}
}
