/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.perspective;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class Perspective implements IPerspectiveFactory {

	private static final String MINIATURE_VIEW_ID = "org.eclipse.graphiti.ui.internal.editor.thumbnailview"; //$NON-NLS-1$	
	private static final String BOTTOM_ID = "bottom"; //$NON-NLS-1$
	private static final float BOTTOM_RATIO = 0.70f;
	private static final float LEFT_RATIO = 0.20f;
	private static final float BOTTOM_LEFT_RATIO = 0.5f;

	@Override
	public void createInitialLayout(IPageLayout layout) {
		// Get the editor area.
		String editorArea = layout.getEditorArea();

		// Top left: project explorer view
		layout.addView(IPageLayout.ID_PROJECT_EXPLORER, IPageLayout.LEFT,
				LEFT_RATIO, editorArea);

		// Bottom left: Miniature view
		layout.addView(MINIATURE_VIEW_ID, IPageLayout.BOTTOM,
				BOTTOM_LEFT_RATIO, IPageLayout.ID_PROJECT_EXPLORER);
		// layout.addView(SERVICE_CATALOG_VIEW_ID, IPageLayout.BOTTOM,
		// BOTTOM_LEFT_RATIO, IPageLayout.ID_PROJECT_EXPLORER);

		// Bottom: Property Sheet view and problem view
		IFolderLayout bottom = layout.createFolder(BOTTOM_ID,
				IPageLayout.BOTTOM, BOTTOM_RATIO, editorArea);
		bottom.addView(IPageLayout.ID_PROP_SHEET);
		bottom.addView(IPageLayout.ID_PROBLEM_VIEW);
		bottom.addView("org.eclipse.pde.runtime.LogView"); //Error log. //A constant doesn't seem to exist for this ID //$NON-NLS-1$

		// Add OData Model to File -> new
		layout.addNewWizardShortcut("org.eclipse.ogee.core.wizard.ODataServiceWizard"); //$NON-NLS-1$
		// Add Service Implementation to File -> new
		layout.addNewWizardShortcut("com.sap.odata.dte.sap.smp.ui.ServiceImplementationWizard"); //$NON-NLS-1$

		// Add Project Explorer to Window -> show view
		layout.addShowViewShortcut(IPageLayout.ID_PROJECT_EXPLORER);
		// Add Properties Editor to Window -> show view
		layout.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);
		// Add Problem View to Window -> show view
		layout.addShowViewShortcut(IPageLayout.ID_PROBLEM_VIEW);
		layout.addShowViewShortcut("org.eclipse.pde.runtime.LogView"); //Error log. //A constant doesn't seem to exist for this ID //$NON-NLS-1$
		// Add Miniature view to Window -> show view
		layout.addShowViewShortcut(MINIATURE_VIEW_ID);

	}

}