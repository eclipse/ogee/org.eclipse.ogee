/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils;

import java.io.File;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.actions.OrganizeImportsAction;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.nls.messages.FrameworkUtilsMessages;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveRegistry;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.FileEditorInput;

/**
 * A utility class for a project.
 * 
 */
public class ProjectUtils {

	public static final String DEFAULT_TEXT_ID = "org.eclipse.ui.DefaultTextEditor";

	/**
	 * Refreshes the given project. Mainly used after proxy is generated in that
	 * project.
	 * 
	 * @param projectName
	 * @throws CoreException
	 */
	public static void refreshProject(String projectName) throws CoreException {
		// get app project
		IProject project = getProject(projectName);
		if (project == null) {
			return;
		}
		// refresh the project
		project.refreshLocal(IProject.DEPTH_INFINITE, null);
	}

	/**
	 * Rebuilds the given project. Mainly used after proxy is generated in that
	 * project.
	 * 
	 * @param projectName
	 * @throws CoreException
	 * @throws TemplateException
	 */
	public static void cleanProject(String projectName, IProgressMonitor monitor)
			throws CoreException {
		// get app project
		IProject project = getProject(projectName);
		if (project == null) {
			return;
		}
		// clean the project
		project.build(IncrementalProjectBuilder.CLEAN_BUILD, monitor);
		// rebuild the project
		project.build(IncrementalProjectBuilder.FULL_BUILD, monitor);
	}

	/**
	 * Open the relevant files after their generation.
	 * 
	 * @param projectName
	 * @param generatedFilePath
	 * @throws PartInitException
	 */
	public static void openFiles(String projectName,
			List<String> generatedFilePath) throws PartInitException {
		for (String filePath : generatedFilePath) {
			openFile(projectName, filePath);
		}
	}

	/**
	 * Removes unused java imports and saves all java files after the removal.
	 * 
	 * @param codeGenResult
	 *            resources to remove unused imports from
	 * @throws EnvironmentExtensionException
	 * @throws CoreException
	 */
	public static void organizeJavaImportsAndSave(String projectName,
			List<String> generatedFilesPathes, IProgressMonitor monitor)
			throws CoreException {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		try {
			monitor.beginTask(FrameworkUtilsMessages.ProjectUtils_0,
					generatedFilesPathes.size());

			// run organize imports only for Java project
			if (PlatformUI.isWorkbenchRunning() && isJavaProject(projectName)) {
				IWorkbenchWindow activeWorkbenchWindow = PlatformUI
						.getWorkbench().getActiveWorkbenchWindow();
				IWorkbenchPage workbenchPage = activeWorkbenchWindow
						.getActivePage();
				IWorkbenchPartSite site = workbenchPage.getActivePart()
						.getSite();

				IWorkspaceRoot wsRoot = ResourcesPlugin.getWorkspace()
						.getRoot();
				OrganizeImportsAction organizeAction = new OrganizeImportsAction(
						site);

				ICompilationUnit[] cUnits = new ICompilationUnit[generatedFilesPathes
						.size()];
				for (int i = 0; i < cUnits.length; i++) {
					String generatedFilePath = generatedFilesPathes.get(i);
					IPath location = new Path(generatedFilePath);
					IFile file = wsRoot.getFileForLocation(location);
					ICompilationUnit cUnit = JavaCore
							.createCompilationUnitFrom(file);
					cUnits[i] = cUnit;
				}

				organizeAction.runOnMultiple(cUnits);
			}
		} finally {
			monitor.done();
		}
	}

	/**
	 * Checks if the given project is a java project.
	 * 
	 * @param selection
	 *            selection to check
	 * @return true if a java project, otherwise - false
	 * @throws CoreException
	 */
	public static boolean isJavaProject(String projectName)
			throws CoreException {
		// get app project
		IProject project = getProject(projectName);
		if (project == null) {
			return false;
		}
		// return true if the project has java nature
		return project.hasNature(JavaCore.NATURE_ID);
	}

	/**
	 * Opens the generated file in the default editor.
	 * 
	 * @param projectName
	 * @param filePath
	 * @throws PartInitException
	 */
	public static void openFile(String projectName, String filePath)
			throws PartInitException {
		if (!PlatformUI.isWorkbenchRunning()) {
			return;
		}

		// get app project
		IProject project = getProject(projectName);
		if (project == null) {
			return;
		}

		int srcIndex = filePath.indexOf(File.separator + "src"); //$NON-NLS-1$

		IFile file = project.getFile(filePath.substring(srcIndex));
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow window = workbench.getWorkbenchWindows()[0];
		if (window != null) {
			IWorkbenchPage page = window.getActivePage();
			if (page != null) {
				IEditorDescriptor descriptor = workbench.getEditorRegistry()
						.getDefaultEditor(file.getName());
				if (descriptor != null) {
					page.openEditor(new FileEditorInput(file),
							descriptor.getId());
				} else {
					page.openEditor(new FileEditorInput(file), DEFAULT_TEXT_ID);
				}
			}
		}
	}

	/**
	 * Returns project
	 * 
	 * @param projectName
	 *            project name
	 * @return project
	 */
	public static IProject getProject(String projectName) {
		if (projectName != null) {
			projectName = projectName.trim();
		}
		if (projectName == null || projectName.isEmpty()) {
			return null;
		}

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		projectName = (projectName.startsWith(File.separator) ? projectName
				.substring(1) : projectName);
		IStatus validateStatus = workspace.validateName(projectName,
				IResource.PROJECT);
		if (IStatus.OK == validateStatus.getSeverity()) {
			IWorkspaceRoot root = workspace.getRoot();

			IProject project = root.getProject(projectName);

			return project;
		}

		return null;
	}

	/**
	 * @param projectName
	 *            project name
	 * @return true if a project with similar name already exists, otherwise
	 *         false.
	 */
	public static boolean projectExists(String projectName) {
		// get app project
		IProject project = getProject(projectName);
		if (project == null) {
			return false;
		}

		if (project.exists()) {
			return true;
		}

		IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
				.getProjects();
		for (IProject iProject : projects) {
			if (iProject.getName().equalsIgnoreCase(project.getName())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 * @param projectName
	 * @return handle to java project
	 */
	public static IJavaProject getJavaProject(String projectName) {
		IProject project = getProject(projectName);
		if (project == null) {
			return null;
		}

		IJavaProject javaProject = JavaCore.create(project);

		return javaProject;
	}

	/**
	 * Returns project folder full path in OS format
	 * 
	 * @param projectName
	 *            project name
	 * @return project path
	 */
	public static String getProjectPath(String projectName) {
		IProject project = getProject(projectName);
		if (project == null) {
			return null;
		}

		IPath projectLocation = project.getLocation();
		if (projectLocation == null) {
			return null;
		}

		String projectPath = projectLocation.toOSString();

		if (!projectPath.endsWith(File.separator)) {
			projectPath = projectPath + File.separator;
		}

		return projectPath;
	}

	/**
	 * Open perspective by perspective id.
	 * 
	 * @param perspId
	 *            The ID of the Eclipse perspective to open.
	 */
	public static void openPerspective(String perspId) {
		if (perspId != null) {
			IPreferenceStore store = PlatformUI.getPreferenceStore();
			String pref = store
					.getString(IWorkbenchPreferenceConstants.OPEN_NEW_PERSPECTIVE);

			IWorkbench workbench = PlatformUI.getWorkbench();
			// Implement open behavior.
			try {
				if (pref.equals(IWorkbenchPreferenceConstants.OPEN_PERSPECTIVE_WINDOW)) {
					workbench.openWorkbenchWindow(perspId,
							ResourcesPlugin.getWorkspace());
				} else if (pref
						.equals(IWorkbenchPreferenceConstants.OPEN_PERSPECTIVE_REPLACE)) {
					IPerspectiveRegistry reg = workbench
							.getPerspectiveRegistry();
					IPerspectiveDescriptor desc = reg
							.findPerspectiveWithId(perspId);
					if (desc != null) {
						// workbench.getActiveWorkbenchWindow().getActivePage().setPerspective(desc);
						workbench.showPerspective(perspId,
								workbench.getActiveWorkbenchWindow());
					} else {
						Logger.getUtilsLogger().logWarning(
								NLS.bind(FrameworkUtilsMessages.ProjectUtils_1,
										perspId));
					}
				}
			} catch (WorkbenchException e) {
				Logger.getUtilsLogger()
						.logWarning(
								NLS.bind(FrameworkUtilsMessages.ProjectUtils_2,
										perspId), e);
			}
		}
	}

}
