/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.eclipse.ogee.client.connectivity.api.IServerConnectionParameters;
import org.eclipse.ogee.client.connectivity.impl.ServerConnectionParameters;
import org.eclipse.ogee.client.connectivity.security.UsernamePasswordCredentials;

public class ServerConnectionParametersUtil {
	public static final String HTTP = "http://"; //$NON-NLS-1$
	public static final String HTTPS = "https://"; //$NON-NLS-1$
	public static final String _443 = "443"; //$NON-NLS-1$
	public static final String _80 = "80"; //$NON-NLS-1$

	public static String createConnectionParamsKey(
			IServerConnectionParameters connParams) {
		if (connParams == null) {
			return null;
		}

		StringBuilder sb = getServerUrl(connParams);

		UsernamePasswordCredentials credentials = getCredentials(connParams);
		if (credentials == null) {
			return sb.toString();
		}

		String userName = getUsername(connParams);
		if (userName == null) {
			return sb.toString();
		}

		// String client = getClient(connParams);
		//sb = sb.append("_").append(client); //$NON-NLS-1$

		sb.append("_").append(userName); //$NON-NLS-1$ 

		return sb.toString();
	}

	/**
	 * Returns an url representation of the specified gateway.
	 * 
	 * @param connParams
	 * @return an url representation of the specified gateway.
	 */
	public static StringBuilder getServerUrl(
			IServerConnectionParameters connParams) {
		if (connParams == null) {
			return null;
		}

		String host = getHost(connParams);
		if (host == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();

		boolean useSsl = useSsl(connParams);
		String protocol = (useSsl ? HTTPS : HTTP);

		String port = getPort(connParams);

		sb.append(protocol).append(host).append(":").append(port); //$NON-NLS-1$

		return sb;
	}

	public static String getHost(IServerConnectionParameters connParams) {
		if (connParams == null) {
			return null;
		}

		String host = connParams.getHost();
		if (host != null) {
			host = host.trim();
		}

		if (host == null || host.isEmpty()) {
			return null;
		}

		return host.toLowerCase(Locale.ENGLISH);
	}

	public static String getPort(IServerConnectionParameters connParams) {
		if (connParams == null) {
			return null;
		}

		String port = connParams.getPort();
		if (port != null) {
			port = port.trim();
		}

		if (port == null || port.isEmpty()) {
			boolean useSsl = useSsl(connParams);
			String defaultPort = (useSsl ? _443 : _80);

			return defaultPort;
		}

		return port;
	}

	public static boolean useSsl(IServerConnectionParameters connParams) {
		if (connParams == null) {
			return false;
		}

		Object useSslObj = connParams.get(IServerConnectionParameters.USE_SSL);
		if (useSslObj == null) {
			return false;
		}

		Boolean useSsl = Boolean.valueOf(useSslObj.toString());

		return useSsl;
	}

	/**
	 * Creates the specified gateway off line storage folder name.
	 * 
	 * @param connParams
	 *            gateway connection parameters.
	 * @return
	 */
	public static String createGatewayName(
			IServerConnectionParameters connParams) {
		StringBuilder sb = getServerUrl(connParams);
		if (sb == null) {
			return null;
		}

		/*
		 * String client = getClient(connParams); sb =
		 * sb.append("_").append(client); //$NON-NLS-1$
		 */
		UsernamePasswordCredentials credentials = getCredentials(connParams);
		if (credentials == null) {
			return sb.toString();
		}

		String userName = getUsername(connParams);
		if (userName != null) {
			sb.append("_").append(userName); //$NON-NLS-1$ 
		}

		return sb.toString();
	}

	public static UsernamePasswordCredentials getCredentials(
			IServerConnectionParameters connParams) {
		if (connParams == null) {
			return null;
		}

		UsernamePasswordCredentials credentials = (UsernamePasswordCredentials) connParams
				.get(IServerConnectionParameters.AUTHENTICATION);
		if (credentials == null) {
			return null;
		}

		return credentials;
	}

	public static String getUsername(IServerConnectionParameters connParams) {
		UsernamePasswordCredentials credentials = getCredentials(connParams);
		if (credentials == null) {
			return null;
		}

		String userName = credentials.getUserName();
		if (userName != null) {
			userName = userName.trim();
		}

		if (userName == null || userName.isEmpty()) {
			return null;
		}

		return userName.toLowerCase(Locale.ENGLISH);
	}

	public static String getPassword(IServerConnectionParameters connParams) {
		UsernamePasswordCredentials credentials = getCredentials(connParams);
		if (credentials == null) {
			return null;
		}

		String password = credentials.getPassword();
		if (password != null) {
			password = password.trim();
		}

		if (password == null || password.isEmpty()) {
			return null;
		}

		return password;
	}

	/**
	 * Creates a gateway configuration from the service's URL.
	 * 
	 * @param serviceUrl
	 *            a service's URL.
	 * @return IGatewayConfiguration gateway configuration.
	 * @throws MalformedURLException
	 */
	public static IServerConnectionParameters createServerConnectionParameters(
			String serviceUrl) throws MalformedURLException {
		if (serviceUrl != null) {
			serviceUrl = serviceUrl.trim();
		}

		if (serviceUrl == null || serviceUrl.isEmpty()) {
			return null;
		}

		serviceUrl = serviceUrl.toLowerCase(Locale.ENGLISH);

		URL url = new URL(serviceUrl);
		// get if https is in use
		boolean useHttps = serviceUrl.startsWith(HTTPS);
		// get default port
		String defaultPort = (useHttps ? _443 : _80);
		// get host
		String host = url.getHost();
		// get port
		int intPort = url.getPort();
		String port = (intPort == -1) ? defaultPort : String.valueOf(intPort);

		IServerConnectionParameters conParams = new ServerConnectionParameters(
				host, port);
		conParams.put(IServerConnectionParameters.USE_SSL, useHttps);

		// get url parameters
		Map<String, String> queryMap = getQueryMap(url.getQuery());
		Set<String> keySet = queryMap.keySet();
		for (String key : keySet) {
			conParams.put(key, queryMap.get(key));
		}

		return conParams;
	}

	/**
	 * @param query
	 * @return - query map
	 */
	private static Map<String, String> getQueryMap(String query) {
		Map<String, String> map = new HashMap<String, String>();

		if (query != null) {
			query = query.trim();
		}

		if (query == null || query.isEmpty()) {
			return map;
		}

		String[] params = query.split("&"); //$NON-NLS-1$
		for (String param : params) {
			String[] split = param.split("="); //$NON-NLS-1$
			if (split.length == 2) {
				String name = split[0];
				String value = split[1];
				map.put(name, value);
			}
		}

		return map;
	}
}
