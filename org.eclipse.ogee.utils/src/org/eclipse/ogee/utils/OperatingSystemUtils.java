/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils;

import java.util.Locale;

/**
 * A utility class for an operating system.
 * 
 */
public class OperatingSystemUtils {
	/**
	 * Check whether the operating system is of Mac.
	 * 
	 * @return - true whether the operating system is of Mac, false otherwise.
	 */
	public static boolean isMacOs() {
		// get the operating system
		String os = System.getProperty("os.name").toLowerCase(Locale.ENGLISH); //$NON-NLS-1$
		boolean isMac = (os.indexOf("mac") >= 0); //Mac OS of any version //$NON-NLS-1$

		return isMac;
	}

	/**
	 * Check whether the operating system is of Windows.
	 * 
	 * @return - true whether the operating system is of Windows, false
	 *         otherwise.
	 */
	public static boolean isWinOs() {
		// get the operating system
		String os = System.getProperty("os.name").toLowerCase(Locale.ENGLISH); //$NON-NLS-1$
		boolean isWin = (os.indexOf("win") >= 0); //Win OS of any version //$NON-NLS-1$

		return isWin;
	}
}
