/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils;

import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorSupportProvider;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IconAndMessageDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.Policy;
import org.eclipse.ogee.utils.activator.Activator;
import org.eclipse.ogee.utils.nls.messages.FrameworkUtilsMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

public class FilesOverwriteDialog extends IconAndMessageDialog {
	/**
	 * Static to prevent opening of overwrite dialogs for automated testing.
	 */
	public static boolean AUTOMATED_MODE = false;

	/**
	 * Reserve room for this many list items.
	 */
	private static final int LIST_ITEM_COUNT = 7;

	/**
	 * The Details button.
	 */
	private Button detailsButton;

	/**
	 * The title of the dialog.
	 */
	private String title;

	/**
	 * The SWT list control that displays the overwrite details.
	 */
	private List list;

	/**
	 * Indicates whether the overwrite details viewer is currently created.
	 */
	private boolean listCreated = false;

	/**
	 * The main status object.
	 */
	private IStatus status;

	/**
	 * The current clipboard. To be disposed when closing the dialog.
	 */
	private Clipboard clipboard;

	/**
	 * the return code
	 */
	private int returnCode;

	/**
	 * files to overwrite
	 */
	private java.util.List<String> files;

	/**
	 * Creates a files overwrite dialog.
	 * 
	 * @param parentShell
	 *            the shell under which to create this dialog
	 * @param dialogTitle
	 *            the title to use for this dialog, or <code>null</code> to
	 *            indicate that the default title should be used
	 * @param message
	 *            the message to show in this dialog
	 * @param files
	 *            files to overwrite
	 * @see org.eclipse.core.runtime.IStatus#matches(int)
	 */
	public FilesOverwriteDialog(Shell parentShell, String dialogTitle,
			String message, java.util.List<String> files) {
		super(parentShell);
		this.title = dialogTitle == null ? "Title is null" : dialogTitle; //$NON-NLS-1$

		this.status = new Status(IStatus.WARNING, Activator.PLUGIN_ID, null);

		this.message = message == null ? "Message is null" : message; //$NON-NLS-1$

		this.files = files;
		if (files == null) {
			this.files = new ArrayList<String>();
		}
	}

	@Override
	protected void buttonPressed(int id) {
		if (id == IDialogConstants.DETAILS_ID) {
			// was the details button pressed?
			toggleDetailsArea();
		} else {
			this.returnCode = id;
			setReturnCode(id);
			close();
		}
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(this.title);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create Yes, No and Details buttons
		createButton(parent, IDialogConstants.YES_ID,
				IDialogConstants.YES_LABEL, true);

		createButton(parent, IDialogConstants.NO_ID, IDialogConstants.NO_LABEL,
				true);

		createDetailsButton(parent);
	}

	/**
	 * Create the area for extra overwrite support information.
	 * 
	 * @param parent
	 */
	private void createSupportArea(Composite parent) {
		ErrorSupportProvider provider = Policy.getErrorSupportProvider();

		if (provider == null) {
			return;
		}

		Composite supportArea = new Composite(parent, SWT.NONE);
		provider.createSupportArea(supportArea, this.status);

		GridData supportData = new GridData(SWT.FILL, SWT.FILL, true, true);
		supportData.verticalSpan = 3;
		supportArea.setLayoutData(supportData);
		if (supportArea.getLayout() == null) {
			GridLayout layout = new GridLayout();
			layout.marginWidth = 0;
			layout.marginHeight = 0;
			supportArea.setLayout(layout); // Give it a default layout if one
											// isn't set
		}
	}

	/**
	 * Create the details button if it should be included.
	 * 
	 * @param parent
	 *            the parent composite
	 */
	private void createDetailsButton(Composite parent) {
		this.detailsButton = createButton(parent, IDialogConstants.DETAILS_ID,
				IDialogConstants.SHOW_DETAILS_LABEL, false);
	}

	/**
	 * This implementation of the <code>Dialog</code> framework method creates
	 * and lays out a composite. Subclasses that require a different dialog area
	 * may either override this method, or call the <code>super</code>
	 * implementation and add controls to the created composite.
	 * 
	 * Note: Since 3.4, the created composite no longer grabs excess vertical
	 * space. See https://bugs.eclipse.org/bugs/show_bug.cgi?id=72489. If the
	 * old behavior is desired by subclasses, get the returned composite's
	 * layout data and set grabExcessVerticalSpace to true.
	 */
	protected Control createDialogArea(Composite parent) {
		// Create a composite with standard margins and spacing
		// Add the messageArea to this composite so that as subclasses add
		// widgets to the messageArea
		// and dialogArea, the number of children of parent remains fixed and
		// with consistent layout.
		// Fixes bug #240135
		Composite composite = new Composite(parent, SWT.NONE);
		createMessageArea(composite);
		createSupportArea(parent);
		GridLayout layout = new GridLayout();
		layout.marginHeight = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_MARGIN);
		layout.marginWidth = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
		layout.verticalSpacing = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_SPACING);
		layout.horizontalSpacing = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
		layout.numColumns = 2;
		composite.setLayout(layout);
		GridData childData = new GridData(GridData.FILL_BOTH);
		childData.horizontalSpan = 2;
		childData.grabExcessVerticalSpace = false;
		composite.setLayoutData(childData);
		composite.setFont(parent.getFont());

		return composite;
	}

	@Override
	protected void createDialogAndButtonArea(Composite parent) {
		super.createDialogAndButtonArea(parent);
		if (this.dialogArea instanceof Composite) {
			// Create a label if there are no children to force a smaller layout
			Composite dialogComposite = (Composite) this.dialogArea;
			if (dialogComposite.getChildren().length == 0) {
				new Label(dialogComposite, SWT.NULL);
			}
		}
	}

	@Override
	protected Image getImage() {
		return getWarningImage();
	}

	/**
	 * Create this dialog's drop-down list component.
	 * 
	 * @param parent
	 *            the parent composite
	 * @return the drop-down list component
	 */
	private List createDropDownList(Composite parent) {
		// create the list
		this.list = new List(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.MULTI);
		// fill the list
		populateList(this.list);
		GridData data = new GridData(GridData.HORIZONTAL_ALIGN_FILL
				| GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL
				| GridData.GRAB_VERTICAL);
		data.heightHint = this.list.getItemHeight() * LIST_ITEM_COUNT;
		data.horizontalSpan = 2;
		this.list.setLayoutData(data);
		this.list.setFont(parent.getFont());
		Menu copyMenu = new Menu(this.list);
		MenuItem copyItem = new MenuItem(copyMenu, SWT.NONE);
		copyItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				copyToClipboard();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				copyToClipboard();
			}
		});
		copyItem.setText(JFaceResources.getString("copy")); //$NON-NLS-1$
		this.list.setMenu(copyMenu);
		this.listCreated = true;

		return this.list;
	}

	@Override
	public int open() {
		if (!AUTOMATED_MODE) {
			return super.open();
		}

		return this.returnCode;
	}

	/**
	 * Populate the list with the messages.
	 * 
	 * @param listToPopulate
	 *            the list to populate
	 */
	private void populateList(List listToPopulate) {
		listToPopulate.add(FrameworkUtilsMessages.FilesOverwriteDialog_0);
		listToPopulate.add("\n"); //$NON-NLS-1$

		for (String file : this.files) {
			listToPopulate.add(file);
		}
	}

	/**
	 * Toggles the unfolding of the details area. This is triggered by the user
	 * pressing the details button.
	 */
	private void toggleDetailsArea() {
		Point windowSize = getShell().getSize();
		Point oldSize = getShell().computeSize(SWT.DEFAULT, SWT.DEFAULT);
		if (this.listCreated) {
			this.list.dispose();
			this.listCreated = false;
			this.detailsButton.setText(IDialogConstants.SHOW_DETAILS_LABEL);
		} else {
			this.list = createDropDownList((Composite) getContents());
			this.detailsButton.setText(IDialogConstants.HIDE_DETAILS_LABEL);
			getContents().getShell().layout();
		}
		Point newSize = getShell().computeSize(SWT.DEFAULT, SWT.DEFAULT);
		getShell()
				.setSize(
						new Point(windowSize.x, windowSize.y
								+ (newSize.y - oldSize.y)));
	}

	/**
	 * Copy the contents of the statuses to the clipboard.
	 */
	private void copyToClipboard() {
		if (this.clipboard != null) {
			this.clipboard.dispose();
		}
		StringBuffer statusBuffer = new StringBuffer();
		for (String file : this.files) {
			statusBuffer.append(file).append("\n "); //$NON-NLS-1$
		}

		this.clipboard = new Clipboard(this.list.getDisplay());
		this.clipboard.setContents(new Object[] { statusBuffer.toString() },
				new Transfer[] { TextTransfer.getInstance() });
	}

	@Override
	public boolean close() {
		if (this.clipboard != null) {
			this.clipboard.dispose();
		}

		return super.close();
	}

	@Override
	protected boolean isResizable() {
		return true;
	}
}
