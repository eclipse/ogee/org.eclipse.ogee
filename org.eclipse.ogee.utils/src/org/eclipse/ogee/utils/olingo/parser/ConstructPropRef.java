/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.PropertyRef;

public class ConstructPropRef {

	private PropertyRef[] propRefs = null;
	private PropertyRef propRef;

	public ConstructPropRef() {

	}

	public PropertyRef[] getPropRef(
			List<org.apache.olingo.odata2.api.edm.provider.PropertyRef> propRefOlingo) {
		propRefs = new PropertyRef[propRefOlingo.size()];
		for (int i = 0; i < propRefOlingo.size(); i++) {
			propRef = new PropertyRef();
			propRef.setName(propRefOlingo.get(i).getName());
			propRefs[i] = propRef;
		}
		return propRefs;
	}

}
