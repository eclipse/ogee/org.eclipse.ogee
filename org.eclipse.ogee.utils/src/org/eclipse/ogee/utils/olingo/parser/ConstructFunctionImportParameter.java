/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.Parameter;

public class ConstructFunctionImportParameter {
	private org.apache.olingo.odata2.api.edm.provider.FunctionImportParameter functionImportParameterOlingo = null;
	private Parameter paramter;
	private ConstructDocumentation constructDocumentation;

	public ConstructFunctionImportParameter() {

	}

	public Parameter[] constructParameter(
			List<org.apache.olingo.odata2.api.edm.provider.FunctionImportParameter> functionImportParameterListOlingo) {

		Parameter[] parameters = new Parameter[functionImportParameterListOlingo
				.size()];

		for (int i = 0; i < functionImportParameterListOlingo.size(); i++) {

			paramter = new Parameter();
			functionImportParameterOlingo = functionImportParameterListOlingo
					.get(i);

			if (null != functionImportParameterOlingo.getName()) {
				paramter.setName(functionImportParameterOlingo.getName());
			}

			if (null != functionImportParameterOlingo.getType()) {
				paramter.setType(functionImportParameterOlingo.getType()
						.getFullQualifiedName().toString());
			}

			if (null != functionImportParameterOlingo.getMode()) {
				paramter.setMode(functionImportParameterOlingo.getMode());
			}
			if (null != functionImportParameterOlingo.getFacets()
					&& null != functionImportParameterOlingo.getFacets()
							.getMaxLength()) {
				paramter.setMaxLength(functionImportParameterOlingo.getFacets()
						.getMaxLength().toString());
			}
			constructDocumentation = new ConstructDocumentation();
			if (null != functionImportParameterOlingo.getDocumentation()) {
				Documentation documentation = constructDocumentation
						.setDocumentation(functionImportParameterOlingo
								.getDocumentation());
				paramter.setDocumentation(documentation);

			}

			parameters[i] = paramter;
		}
		return parameters;
	}

}
