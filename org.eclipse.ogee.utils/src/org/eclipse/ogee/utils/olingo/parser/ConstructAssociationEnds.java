/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.End;

public class ConstructAssociationEnds {
	private org.apache.olingo.odata2.api.edm.provider.AssociationEnd associationEndOlingo = null;
	private End end;
	private ConstructDocumentation constructDocumentation;

	public ConstructAssociationEnds() {

	}

	public End[] setAssociationEnd(
			List<org.apache.olingo.odata2.api.edm.provider.AssociationEnd> associationEndListOlingo) {

		End[] ends = new End[associationEndListOlingo.size()];

		for (int i = 0; i < associationEndListOlingo.size(); i++) {
			end = new End();
			associationEndOlingo = associationEndListOlingo.get(i);

			if (null != associationEndOlingo.getRole()) {
				end.setRole(associationEndOlingo.getRole());
			}

			if (null != associationEndOlingo.getType()) {
				end.setType(associationEndOlingo.getType().toString());
			}
			if (null != associationEndOlingo.getMultiplicity()) {
				end.setMultiplicity(associationEndOlingo.getMultiplicity()
						.toString());
			}

			// Construct Documentation
			constructDocumentation = new ConstructDocumentation();
			if (null != associationEndOlingo.getDocumentation()) {
				Documentation documentation = constructDocumentation
						.setDocumentation(associationEndOlingo
								.getDocumentation());
				end.setDocumentation(documentation);

			}

			ends[i] = end;
		}
		return ends;
	}

}
