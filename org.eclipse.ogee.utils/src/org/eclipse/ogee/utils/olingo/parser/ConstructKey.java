/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.Key;
import org.eclipse.ogee.client.model.edmx.PropertyRef;

public class ConstructKey {

	private Key key = null;
	private List<org.apache.olingo.odata2.api.edm.provider.PropertyRef> propRefOlingoList = null;
	private ConstructPropRef constructPropRef;
	private PropertyRef[] propRefs;

	public ConstructKey() {

	}

	public Key setKey(org.apache.olingo.odata2.api.edm.provider.Key keyOlingo) {
		propRefOlingoList = keyOlingo.getKeys();
		key = new Key();
		constructPropRef = new ConstructPropRef();
		if (null != propRefOlingoList) {
			propRefs = constructPropRef.getPropRef(propRefOlingoList);
			key.setPropertyRefs(propRefs);
		}
		return key;
	}

}
