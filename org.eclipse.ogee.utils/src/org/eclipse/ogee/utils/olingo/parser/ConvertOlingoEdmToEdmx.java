/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.apache.olingo.odata2.api.edm.provider.EdmProvider;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.eclipse.ogee.client.model.edmx.Association;
import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.EdmxDataServices;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.model.edmx.Using;
import org.eclipse.ogee.client.model.edmx.v3.EdmxV3;

public class ConvertOlingoEdmToEdmx {

	public static final String DEFAULT_VERSION = "2.0"; //$NON-NLS-1$

	private EntityType[] entityTypes;
	private ComplexType[] complexTypes;
	private EntityContainer[] entityContainers;
	private Schema schema;
	private Schema[] schemas;
	private Association[] associations;
	private Using[] usings;

	private ConstructEntityType constructEntityType;
	private ConstructEntityContainer constructEntityContainer;
	private ConstructComplexType constructComplexType;
	private ConstructAssociations constructAssociation;
	private ConstructUsing constructUsing;

	private EdmProvider edmProvider;
	private EdmxV3 edmx;
	private EdmxDataServices edmxDataServices;
	private List<org.apache.olingo.odata2.api.edm.provider.Schema> schemaListOlingo = null;
	private org.apache.olingo.odata2.api.edm.provider.Schema schemaOlingo;

	public ConvertOlingoEdmToEdmx(EdmProvider edmProvider) {
		this.edmProvider = edmProvider;
	}

	public Edmx convertToEdmx() throws ODataException, Exception {
		edmx = new EdmxV3();
		edmxDataServices = new EdmxDataServices();

		schemaListOlingo = edmProvider.getSchemas();
		schemas = new Schema[schemaListOlingo.size()];
		for (int i = 0; i < schemaListOlingo.size(); i++) {
			schemaOlingo = schemaListOlingo.get(i);

			schema = new Schema();

			// construct entity type
			if (null != schemaOlingo.getEntityTypes()) {
				constructEntityType = new ConstructEntityType();
				entityTypes = constructEntityType
						.constructEntityType(schemaOlingo.getEntityTypes());
				schema.setEntityTypes(entityTypes);
			}
			// construct associations
			if (null != schemaOlingo.getAssociations()) {
				constructAssociation = new ConstructAssociations();
				associations = constructAssociation
						.constructAssociation(schemaOlingo.getAssociations());
				schema.setAssociations(associations);
			}

			// construct entity container
			if (null != schemaOlingo.getEntityContainers()) {
				constructEntityContainer = new ConstructEntityContainer();
				entityContainers = constructEntityContainer
						.construct(schemaOlingo.getEntityContainers());
				schema.setEntityContainers(entityContainers);
			}
			// construct complex types
			if (null != schemaOlingo.getComplexTypes()) {
				constructComplexType = new ConstructComplexType();
				complexTypes = constructComplexType
						.constructComplexType(schemaOlingo.getComplexTypes());
				schema.setComplexTypes(complexTypes);
			}
			// construct using
			if (null != schemaOlingo.getUsings()) {
				constructUsing = new ConstructUsing();
				usings = constructUsing
						.constructUsing(schemaOlingo.getUsings());
				schema.setUsings(usings);
			}
			schema.setNamespace(schemaOlingo.getNamespace());
			schemas[i] = schema;

		}
		// Version is not coming properly from olingo API . Setting default
		// version.
		// edmxDataServices.setmDataServiceVersion(edm.getServiceMetadata().getDataServiceVersion());
		edmxDataServices.setmDataServiceVersion(DEFAULT_VERSION);

		edmxDataServices.setSchemas(schemas);
		edmx.setEdmxDataServices(edmxDataServices);

		return edmx;
	}
}
