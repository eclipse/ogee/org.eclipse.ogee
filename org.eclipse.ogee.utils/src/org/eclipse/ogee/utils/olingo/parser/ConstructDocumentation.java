/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.LongDescription;
import org.eclipse.ogee.client.model.edmx.Summary;

public class ConstructDocumentation {

	private Documentation documentation;
	private Summary summary;
	private LongDescription longDescription;

	public ConstructDocumentation() {

	}

	public Documentation setDocumentation(
			org.apache.olingo.odata2.api.edm.provider.Documentation documentationOlingo) {
		documentation = new Documentation();
		summary = new Summary();
		longDescription = new LongDescription();
		longDescription.setValue(documentationOlingo.getLongDescription());
		summary.setValue(documentationOlingo.getSummary());
		documentation.setLongDescription(longDescription);
		documentation.setSummary(summary);
		return documentation;
	}

}
