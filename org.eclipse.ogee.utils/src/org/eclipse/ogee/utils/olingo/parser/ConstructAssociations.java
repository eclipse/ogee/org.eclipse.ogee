/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.ArrayList;
import java.util.List;

import org.apache.olingo.odata2.api.edm.provider.AssociationEnd;
import org.eclipse.ogee.client.model.edmx.Association;
import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.End;
import org.eclipse.ogee.client.model.edmx.ReferentialConstraint;

public class ConstructAssociations {

	private Association association;
	private Association[] associations;
	private End[] ends = null;
	private ArrayList<AssociationEnd> assoactionEndArray;
	private org.apache.olingo.odata2.api.edm.provider.Association associationOlingo = null;
	private ConstructDocumentation constructDocumentation;
	private ConstructAssociationEnds constructAssociationEnds;
	private ConstructReferentialConstraint constructRfConstraint;

	public Association[] constructAssociation(
			List<org.apache.olingo.odata2.api.edm.provider.Association> associationListOlingo) {

		associations = new Association[associationListOlingo.size()];
		for (int j = 0; j < associationListOlingo.size(); j++) {
			association = new Association();
			associationOlingo = associationListOlingo.get(j);

			// Set name
			association.setName(associationOlingo.getName());

			// Construct Documentation
			if (null != associationOlingo.getDocumentation()) {
				constructDocumentation = new ConstructDocumentation();
				Documentation documentation = constructDocumentation
						.setDocumentation(associationOlingo.getDocumentation());
				association.setDocumentation(documentation);
			}

			// Construct Association Ends
			assoactionEndArray = new ArrayList<AssociationEnd>();
			assoactionEndArray.add(associationOlingo.getEnd1());
			assoactionEndArray.add(associationOlingo.getEnd2());

			constructAssociationEnds = new ConstructAssociationEnds();
			ends = constructAssociationEnds
					.setAssociationEnd(assoactionEndArray);
			association.setEnds(ends);

			// construct referential constraint
			if (null != associationOlingo.getReferentialConstraint()) {
				constructRfConstraint = new ConstructReferentialConstraint();
				ReferentialConstraint refconstraint = constructRfConstraint
						.setRefConstraint(associationOlingo
								.getReferentialConstraint());
				association.setReferentialConstraint(refconstraint);
			}

			// set association to array
			associations[j] = association;
		}

		return associations;
	}
}