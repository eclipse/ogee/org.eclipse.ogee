/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.ArrayList;
import java.util.List;

import org.apache.olingo.odata2.api.edm.provider.AssociationSetEnd;
import org.eclipse.ogee.client.model.edmx.AssociationSet;
import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.End;

public class ConstructAssociationSets {

	private org.apache.olingo.odata2.api.edm.provider.AssociationSet associationSetOlingo = null;
	private ArrayList<AssociationSetEnd> assoactionSetEndArray;
	private End[] ends = null;
	private AssociationSet associationSet;
	private ConstructDocumentation constructDocumentation;
	private ConstructAssociationSetEnds constructAssociationSetEnds;

	public ConstructAssociationSets() {

	}

	public AssociationSet[] constructAssociationSet(
			List<org.apache.olingo.odata2.api.edm.provider.AssociationSet> associationSetListOlingo) {

		AssociationSet[] associationSetList = new AssociationSet[associationSetListOlingo
				.size()];

		for (int i = 0; i < associationSetListOlingo.size(); i++) {

			associationSet = new AssociationSet();
			associationSetOlingo = associationSetListOlingo.get(i);

			if (null != associationSetOlingo.getName()) {
				associationSet.setName(associationSetOlingo.getName());
			}

			if (null != associationSetOlingo.getAssociation()) {
				associationSet.setAssociation(associationSetOlingo
						.getAssociation().toString());
			}
			constructDocumentation = new ConstructDocumentation();
			if (null != associationSetOlingo.getDocumentation()) {
				Documentation documentation = constructDocumentation
						.setDocumentation(associationSetOlingo
								.getDocumentation());
				associationSet.setDocumentation(documentation);

			}

			assoactionSetEndArray = new ArrayList<AssociationSetEnd>();
			assoactionSetEndArray.add(associationSetOlingo.getEnd1());
			assoactionSetEndArray.add(associationSetOlingo.getEnd2());

			constructAssociationSetEnds = new ConstructAssociationSetEnds();
			ends = constructAssociationSetEnds
					.setAssociatioSetEnds(assoactionSetEndArray);

			associationSet.setEnds(ends);

			associationSetList[i] = associationSet;
		}

		return associationSetList;
	}

}
