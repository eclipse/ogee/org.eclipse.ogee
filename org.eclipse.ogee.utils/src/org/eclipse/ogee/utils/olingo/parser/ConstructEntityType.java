/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.Key;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.client.model.edmx.Property;

public class ConstructEntityType {

	private org.apache.olingo.odata2.api.edm.provider.EntityType entityTypeOlingo = null;
	private Property[] properties = null;
	private NavigationProperty[] navigationProperties = null;
	private EntityType entityType;
	private ConstructKey constructKey;
	private ConstructDocumentation constructDocumentation;
	private ConstructProperty constructProperty;
	private ConstructNavigationProperty constructNavigationProperty;

	public EntityType[] constructEntityType(
			List<org.apache.olingo.odata2.api.edm.provider.EntityType> entityTypes) {

		EntityType[] entityArray = new EntityType[entityTypes.size()];

		for (int j = 0; j < entityTypes.size(); j++) {
			entityType = new EntityType();
			entityTypeOlingo = entityTypes.get(j);
			entityType.setName(entityTypeOlingo.getName());
			entityType.setMHasStream(entityTypeOlingo.isHasStream());
			entityType.setAbstract(entityTypeOlingo.isAbstract());

			// Construct Key
			if (null != entityTypeOlingo.getKey()) {
				constructKey = new ConstructKey();
				Key key = constructKey.setKey(entityTypeOlingo.getKey());
				entityType.setKey(key);
			}

			if (null != entityTypeOlingo.getBaseType()) {
				entityType
						.setBaseType(entityTypeOlingo.getBaseType().getName());
			}

			// Construct Documentation
			if (null != entityTypeOlingo.getDocumentation()) {
				constructDocumentation = new ConstructDocumentation();
				Documentation documentation = constructDocumentation
						.setDocumentation(entityTypeOlingo.getDocumentation());
				entityType.setDocumentation(documentation);
			}

			// Construct Properties
			if (null != entityTypeOlingo.getProperties()) {
				constructProperty = new ConstructProperty();
				properties = constructProperty.setProperty(entityTypeOlingo
						.getProperties());
				entityType.setProperties(properties);
			}

			// Construct Navigation Properties
			if (null != entityTypeOlingo.getNavigationProperties()) {
				constructNavigationProperty = new ConstructNavigationProperty();
				navigationProperties = constructNavigationProperty
						.setNavigationProperty(entityTypeOlingo
								.getNavigationProperties());
				entityType.setNavigationProperties(navigationProperties);
			}
			entityArray[j] = entityType;
		}

		return entityArray;
	}
}
