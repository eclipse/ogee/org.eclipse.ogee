/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.Parameter;

public class ConstructFunctionImport {
	private org.apache.olingo.odata2.api.edm.provider.FunctionImport functionImportOlingo = null;
	private Parameter[] parameters = null;

	private FunctionImport[] functionImports;
	private FunctionImport functionImport;
	private ConstructDocumentation constructDocumentation;
	private ConstructFunctionImportParameter constructFunctionImportParamter;

	public ConstructFunctionImport() {

	}

	public FunctionImport[] constructFunctionImport(
			List<org.apache.olingo.odata2.api.edm.provider.FunctionImport> functionImportListOlingo) {

		functionImports = new FunctionImport[functionImportListOlingo.size()];

		for (int i = 0; i < functionImportListOlingo.size(); i++) {

			functionImport = new FunctionImport();
			functionImportOlingo = functionImportListOlingo.get(i);

			if (null != functionImportOlingo.getName()) {
				functionImport.setName(functionImportOlingo.getName());
			}

			if (null != functionImportOlingo.getReturnType()) {
				functionImport.setReturnType(functionImportOlingo
						.getReturnType().getTypeName().toString());
			}

			if (null != functionImportOlingo.getHttpMethod()) {
				functionImport.setmHttpMethod(functionImportOlingo
						.getHttpMethod());
			}
			if (null != functionImportOlingo.getEntitySet()) {
				functionImport
						.setEntitySet(functionImportOlingo.getEntitySet());
			}

			// Construct Documentation
			if (null != functionImportOlingo.getDocumentation()) {
				constructDocumentation = new ConstructDocumentation();
				Documentation documentation = constructDocumentation
						.setDocumentation(functionImportOlingo
								.getDocumentation());
				functionImport.setDocumentation(documentation);

			}

			// Construct Parameter

			if (null != functionImportOlingo.getParameters()) {
				constructFunctionImportParamter = new ConstructFunctionImportParameter();
				parameters = constructFunctionImportParamter
						.constructParameter(functionImportOlingo
								.getParameters());
				functionImport.setParameters(parameters);
			}

			functionImports[i] = functionImport;
		}

		return functionImports;
	}
}
