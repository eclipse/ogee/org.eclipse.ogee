/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.EntitySet;

public class ConstructEntitySets {
	private org.apache.olingo.odata2.api.edm.provider.EntitySet entitySetOlingo = null;
	private EntitySet entitySet;
	private ConstructDocumentation constructDocumentation;

	public ConstructEntitySets() {

	}

	public EntitySet[] constructEntitySet(
			List<org.apache.olingo.odata2.api.edm.provider.EntitySet> entitySetListOlingo) {

		EntitySet[] entitySetList = new EntitySet[entitySetListOlingo.size()];

		for (int i = 0; i < entitySetListOlingo.size(); i++) {

			entitySet = new EntitySet();
			entitySetOlingo = entitySetListOlingo.get(i);

			if (null != entitySetOlingo.getName()) {
				entitySet.setName(entitySetOlingo.getName());
			}

			if (null != entitySetOlingo.getEntityType()) {
				entitySet.setEntityType(entitySetOlingo.getEntityType()
						.toString());
			}
			constructDocumentation = new ConstructDocumentation();
			if (null != entitySetOlingo.getDocumentation()) {
				Documentation documentation = constructDocumentation
						.setDocumentation(entitySetOlingo.getDocumentation());
				entitySet.setDocumentation(documentation);

			}

			entitySetList[i] = entitySet;
		}

		return entitySetList;
	}

}
