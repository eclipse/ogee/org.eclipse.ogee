/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.End;

public class ConstructAssociationSetEnds {
	private org.apache.olingo.odata2.api.edm.provider.AssociationSetEnd associationSetEndOlingo = null;
	private End end;
	private ConstructDocumentation constructDocumentation;

	public ConstructAssociationSetEnds() {

	}

	public End[] setAssociatioSetEnds(
			List<org.apache.olingo.odata2.api.edm.provider.AssociationSetEnd> associationSetEndListOlingo) {

		End[] ends = new End[associationSetEndListOlingo.size()];

		for (int i = 0; i < associationSetEndListOlingo.size(); i++) {
			end = new End();
			associationSetEndOlingo = associationSetEndListOlingo.get(i);

			if (null != associationSetEndOlingo.getRole()) {
				end.setRole(associationSetEndOlingo.getRole());
			}

			if (null != associationSetEndOlingo.getEntitySet()) {
				end.setEntitySet(associationSetEndOlingo.getEntitySet());
			}

			constructDocumentation = new ConstructDocumentation();
			if (null != associationSetEndOlingo.getDocumentation()) {
				Documentation documentation = constructDocumentation
						.setDocumentation(associationSetEndOlingo
								.getDocumentation());
				end.setDocumentation(documentation);

			}

			ends[i] = end;
		}
		return ends;
	}
}
