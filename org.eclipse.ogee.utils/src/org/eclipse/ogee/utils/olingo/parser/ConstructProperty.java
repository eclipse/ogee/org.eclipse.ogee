/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.apache.olingo.odata2.api.edm.EdmSimpleTypeKind;
import org.apache.olingo.odata2.api.edm.FullQualifiedName;
import org.apache.olingo.odata2.api.edm.provider.Facets;
import org.apache.olingo.odata2.api.edm.provider.SimpleProperty;
import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.Property;

public class ConstructProperty {
	private org.apache.olingo.odata2.api.edm.provider.Property propertyOlingo = null;
	private Property property;
	private ConstructDocumentation constructDocumentation;

	public ConstructProperty() {

	}

	public Property[] setProperty(
			List<org.apache.olingo.odata2.api.edm.provider.Property> propertyListOlingo) {
		Property[] properties = new Property[propertyListOlingo.size()];
		for (int i = 0; i < propertyListOlingo.size(); i++) {
			property = new Property();

			propertyOlingo = propertyListOlingo.get(i);

			if (propertyOlingo instanceof SimpleProperty) {
				EdmSimpleTypeKind type = ((org.apache.olingo.odata2.api.edm.provider.SimpleProperty) propertyOlingo)
						.getType();
				property.setType(type.name());
			} else {
				FullQualifiedName type1 = ((org.apache.olingo.odata2.api.edm.provider.ComplexProperty) propertyOlingo)
						.getType();
				if (type1 != null) {
					property.setType(type1.toString());
				}
			}

			property.setName(propertyOlingo.getName());
			if (null != propertyOlingo.getCustomizableFeedMappings()) {

				Boolean fcKeepInContent = propertyOlingo.getCustomizableFeedMappings().getFcKeepInContent();
				if (fcKeepInContent != null) property.setmFC_KeepInContent(fcKeepInContent);
				if (null != propertyOlingo.getCustomizableFeedMappings()
						.getFcTargetPath()) {
					property.setmFC_TargetPath(propertyOlingo
							.getCustomizableFeedMappings().getFcTargetPath());
				}
			}
			constructDocumentation = new ConstructDocumentation();
			if (null != propertyOlingo.getDocumentation()) {
				Documentation documentation = constructDocumentation
						.setDocumentation(propertyOlingo.getDocumentation());
				property.setDocumentation(documentation);

			}
			org.apache.olingo.odata2.api.edm.provider.Facets facetsOlingo = (Facets) propertyOlingo
					.getFacets();

			if (null != facetsOlingo) {
				if (null != facetsOlingo.isNullable()) {
					property.setNullable(facetsOlingo.isNullable());
				}
				if (null != facetsOlingo.getMaxLength()) {

					property.setMaxLength(facetsOlingo.getMaxLength()
							.toString());

				}
				if (null != facetsOlingo.getConcurrencyMode()) {
					property.setAttribute("ConcurrencyMode", facetsOlingo
							.getConcurrencyMode().toString());
				}

				if (null != facetsOlingo.getScale()) {
					property.setAttribute("Scale", facetsOlingo.getScale()
							.toString());
				}

				if (null != facetsOlingo.getPrecision()) {
					property.setAttribute("Precision", facetsOlingo
							.getPrecision().toString());
				}

				if (null != facetsOlingo.isFixedLength()) {
					property.setFixedLength(facetsOlingo.isFixedLength()
							.toString());
				}

				if (null != facetsOlingo.getDefaultValue()) {
					property.setAttribute("DefaultValue",
							facetsOlingo.getDefaultValue());
				}

				if (null != facetsOlingo.isUnicode()) {
					property.setUnicode(facetsOlingo.isUnicode().toString());
				}

			}
			properties[i] = property;
		}
		return properties;
	}

}
