/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.Property;

public class ConstructComplexType {

	private org.apache.olingo.odata2.api.edm.provider.ComplexType complexTypeOlingo = null;
	private ComplexType complexType;
	private ComplexType[] complexTypes;
	private Property[] properties = null;
	private ConstructDocumentation constructDocumentation;
	private ConstructProperty constructProperty;

	public ComplexType[] constructComplexType(
			List<org.apache.olingo.odata2.api.edm.provider.ComplexType> complexTypeListOlingo) {

		complexTypes = new ComplexType[complexTypeListOlingo.size()];
		for (int j = 0; j < complexTypeListOlingo.size(); j++) {

			complexType = new ComplexType();
			complexTypeOlingo = complexTypeListOlingo.get(j);

			complexType.setName(complexTypeOlingo.getName());
			complexType.setAbstract(complexTypeOlingo.isAbstract());

			if (null != complexTypeOlingo.getBaseType()) {
				complexType.setBaseType(complexTypeOlingo.getBaseType()
						.getName());
			}

			// Construct Documentation
			if (null != complexTypeOlingo.getDocumentation()) {
				constructDocumentation = new ConstructDocumentation();
				Documentation documentation = constructDocumentation
						.setDocumentation(complexTypeOlingo.getDocumentation());
				complexType.setDocumentation(documentation);
			}

			// Construct Properties
			if (null != complexTypeOlingo.getProperties()) {
				constructProperty = new ConstructProperty();
				properties = constructProperty.setProperty(complexTypeOlingo
						.getProperties());
				complexType.setProperties(properties);
				complexTypes[j] = complexType;
			}
		}

		return complexTypes;
	}
}
