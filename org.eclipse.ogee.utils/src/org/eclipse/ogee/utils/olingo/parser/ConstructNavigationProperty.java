/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;

public class ConstructNavigationProperty {
	private org.apache.olingo.odata2.api.edm.provider.NavigationProperty navigationPropertyOlingo = null;
	private NavigationProperty navigationProperty;
	private ConstructDocumentation constructDocumentation;

	public ConstructNavigationProperty() {

	}

	public NavigationProperty[] setNavigationProperty(
			List<org.apache.olingo.odata2.api.edm.provider.NavigationProperty> navigationPropertyListOlingo) {
		NavigationProperty[] navigationProperties = new NavigationProperty[navigationPropertyListOlingo
				.size()];
		for (int i = 0; i < navigationPropertyListOlingo.size(); i++) {
			navigationProperty = new NavigationProperty();
			navigationPropertyOlingo = navigationPropertyListOlingo.get(i);
			navigationProperty.setName(navigationPropertyOlingo.getName());
			constructDocumentation = new ConstructDocumentation();
			if (null != navigationPropertyOlingo.getDocumentation()) {
				Documentation documentation = constructDocumentation
						.setDocumentation(navigationPropertyOlingo
								.getDocumentation());
				navigationProperty.setDocumentation(documentation);

			}
			if (null != navigationPropertyOlingo.getFromRole()) {
				navigationProperty.setFromRole(navigationPropertyOlingo
						.getFromRole());
			}
			if (null != navigationPropertyOlingo.getToRole()) {
				navigationProperty.setToRole(navigationPropertyOlingo
						.getToRole());
			}
			if (null != navigationPropertyOlingo.getRelationship()) {
				navigationProperty.setRelationship(navigationPropertyOlingo
						.getRelationship().getNamespace()
						+ "."
						+ navigationPropertyOlingo.getRelationship().getName());
			}
			navigationProperties[i] = navigationProperty;
		}
		return navigationProperties;
	}

}
