/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.AssociationSet;
import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.FunctionImport;

public class ConstructEntityContainer {

	private org.apache.olingo.odata2.api.edm.provider.EntityContainer entityContainerOlingo = null;

	private EntityContainer entityContainer;
	private EntityContainer[] entityContainers;
	private EntitySet[] entitySets;
	private AssociationSet[] associationSets;
	private FunctionImport[] functionImports;
	private ConstructDocumentation constructDocumentation;
	private ConstructEntitySets constructEntitySets;
	private ConstructAssociationSets constructAssociationSets;
	private ConstructFunctionImport constructFunctionImports;

	public EntityContainer[] construct(
			List<org.apache.olingo.odata2.api.edm.provider.EntityContainer> entityContainerListOlingo) {

		entityContainers = new EntityContainer[entityContainerListOlingo.size()];
		for (int j = 0; j < entityContainerListOlingo.size(); j++) {
			entityContainer = new EntityContainer();
			entityContainerOlingo = entityContainerListOlingo.get(j);

			entityContainer.setName(entityContainerOlingo.getName());

			entityContainer.setDefault(entityContainerOlingo
					.isDefaultEntityContainer());

			constructDocumentation = new ConstructDocumentation();
			if (null != entityContainerOlingo.getDocumentation()) {
				Documentation documentation = constructDocumentation
						.setDocumentation(entityContainerOlingo
								.getDocumentation());
				entityContainer.setDocumentation(documentation);
			}

			// Construct EntitySet
			constructEntitySets = new ConstructEntitySets();
			if (entityContainerOlingo.getEntitySets().size() > 0) {
				entitySets = constructEntitySets
						.constructEntitySet(entityContainerOlingo
								.getEntitySets());
				entityContainer.setEntitySets(entitySets);

			}

			// Construct Association Set
			constructAssociationSets = new ConstructAssociationSets();
			if (entityContainerOlingo.getAssociationSets().size() > 0) {
				associationSets = constructAssociationSets
						.constructAssociationSet(entityContainerOlingo
								.getAssociationSets());
				entityContainer.setAssociationSets(associationSets);
			}

			// Construct Function Import
			constructFunctionImports = new ConstructFunctionImport();
			if (entityContainerOlingo.getFunctionImports().size() > 0) {
				functionImports = constructFunctionImports
						.constructFunctionImport(entityContainerOlingo
								.getFunctionImports());
				entityContainer.setFunctionImports(functionImports);
			}

			entityContainers[j] = entityContainer;
		}

		return entityContainers;
	}
}
