/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import org.eclipse.ogee.client.model.edmx.Dependent;
import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.Principal;
import org.eclipse.ogee.client.model.edmx.PropertyRef;
import org.eclipse.ogee.client.model.edmx.ReferentialConstraint;

public class ConstructReferentialConstraint {
	private ReferentialConstraint referentialConstraint;
	private Principal principal;
	private ConstructPropRef constructPropRefPrincipal;
	private Dependent dependent;
	private ConstructPropRef constructPropRefDependent;
	private ConstructDocumentation constructDocumentation;

	public ConstructReferentialConstraint() {

	}

	public ReferentialConstraint setRefConstraint(
			org.apache.olingo.odata2.api.edm.provider.ReferentialConstraint referentialConstraintOlingo) {
		referentialConstraint = new ReferentialConstraint();
		principal = new Principal();
		principal.setRole(referentialConstraintOlingo.getPrincipal().getRole());

		constructPropRefPrincipal = new ConstructPropRef();
		if (null != referentialConstraintOlingo.getPrincipal()
				.getPropertyRefs()) {
			PropertyRef[] propref = constructPropRefPrincipal
					.getPropRef(referentialConstraintOlingo.getPrincipal()
							.getPropertyRefs());
			principal.setPropertyRefs(propref);
		}

		dependent = new Dependent();
		dependent.setRole(referentialConstraintOlingo.getDependent().getRole());
		constructPropRefDependent = new ConstructPropRef();
		if (null != referentialConstraintOlingo.getDependent()
				.getPropertyRefs()) {
			PropertyRef[] propref = constructPropRefDependent
					.getPropRef(referentialConstraintOlingo.getDependent()
							.getPropertyRefs());
			dependent.setPropertyRefs(propref);
		}

		constructDocumentation = new ConstructDocumentation();
		if (null != referentialConstraintOlingo.getDocumentation()) {
			Documentation documentation = constructDocumentation
					.setDocumentation(referentialConstraintOlingo
							.getDocumentation());
			referentialConstraint.setDocumentation(documentation);

		}

		referentialConstraint.setPrincipal(principal);
		referentialConstraint.setDependent(dependent);
		return referentialConstraint;
	}
}
