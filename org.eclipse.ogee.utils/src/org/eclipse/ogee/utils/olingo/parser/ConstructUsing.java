/*******************************************************************************
 * Copyright (c) 2015 SAP SE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     
 *******************************************************************************/
package org.eclipse.ogee.utils.olingo.parser;

import java.util.List;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.Using;

public class ConstructUsing {
	private org.apache.olingo.odata2.api.edm.provider.Using usingOlingo = null;
	private Using using;
	private ConstructDocumentation constructDocumentation;

	public ConstructUsing() {

	}

	public Using[] constructUsing(
			List<org.apache.olingo.odata2.api.edm.provider.Using> usingListOlingo) {

		Using[] usings = new Using[usingListOlingo.size()];

		for (int i = 0; i < usingListOlingo.size(); i++) {

			using = new Using();
			usingOlingo = usingListOlingo.get(i);

			if (null != usingOlingo.getNamespace()) {
				using.setNamespace(usingOlingo.getNamespace());
			}

			if (null != usingOlingo.getAlias()) {
				using.setAlias(usingOlingo.getAlias());
			}
			constructDocumentation = new ConstructDocumentation();
			if (null != usingOlingo.getDocumentation()) {
				Documentation documentation = constructDocumentation
						.setDocumentation(usingOlingo.getDocumentation());
				using.setDocumentation(documentation);

			}

			usings[i] = using;
		}
		return usings;
	}
}
