/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class StringParsingUtils {
	private static final String QUESTION_MARK_SIGN = "?"; //$NON-NLS-1$
	private static final String DOLLAR_SIGN = "$"; //$NON-NLS-1$
	private static final String SLASH_SUFFIX = "/"; //$NON-NLS-1$

	/**
	 * Extracts Service name from URL.
	 * 
	 * @param currentSelectedUrl
	 *            - metadata or service URL with optional parameters the method
	 *            will not work in case URL includes collection
	 * @return- service name
	 */
	public static String extractServiceName(String currentSelectedUrl) {
		if (currentSelectedUrl.contains(DOLLAR_SIGN)) {
			currentSelectedUrl = currentSelectedUrl.substring(0,
					currentSelectedUrl.indexOf(DOLLAR_SIGN) - 1);
		}
		if (currentSelectedUrl.contains(QUESTION_MARK_SIGN)) {
			currentSelectedUrl = currentSelectedUrl.substring(0,
					currentSelectedUrl.indexOf(QUESTION_MARK_SIGN) - 1);
		}

		URL url;

		try {
			url = new URL(currentSelectedUrl);
		} catch (MalformedURLException e) {
			return null;
		}

		String path = url.getPath();
		List<String> list = Arrays.asList(path.split("/")); //$NON-NLS-1$
		int lsize = list.size();
		return list.get(lsize - 1);
	}

	/**
	 * Removes the parameters from the given url
	 * 
	 * @param inputUri
	 * @return - the url without the parameters.
	 */
	public static String removeURLParameters(String inputUri) {
		if (inputUri.contains(DOLLAR_SIGN)) {
			inputUri = inputUri.substring(0, inputUri.indexOf(DOLLAR_SIGN) - 1);
		}

		if (inputUri.contains(QUESTION_MARK_SIGN)) {
			inputUri = inputUri.substring(0,
					inputUri.indexOf(QUESTION_MARK_SIGN));
		}

		if (!inputUri.endsWith(SLASH_SUFFIX)) {
			inputUri = inputUri + SLASH_SUFFIX;
		}

		return inputUri;
	}

	/**
	 * Extracts URL parameters from the given URL string
	 * 
	 * @param serviceUrl
	 * @return URL parameters from the given URL string including the '?' sign
	 */
	public static String extractURLParametersString(String serviceUrl) {
		String urlParameters = ""; //$NON-NLS-1$
		if (serviceUrl.contains(QUESTION_MARK_SIGN)) {
			urlParameters = serviceUrl
					.substring(serviceUrl.indexOf(QUESTION_MARK_SIGN),
							serviceUrl.length());
		}

		return urlParameters;
	}
}
