/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ogee.utils.activator.Activator;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.nls.messages.FrameworkUtilsMessages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

public class ODataDevelopmentCategoryPreferencePage extends
		FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	private Logger logger = Logger
			.getLogger(ODataDevelopmentCategoryPreferencePage.class);

	/**
	 * Create a category for Ogee in Preferences.
	 */
	public ODataDevelopmentCategoryPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription(FrameworkUtilsMessages.ODATA_CATEGORY_PREFERENCE_TEXT0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {

		// Nothing to implement
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors
	 * ()
	 */
	@Override
	protected void createFieldEditors() {

		// Nothing to implement
	}

	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			// set the matching help context to the page when it's visible
			PlatformUI
					.getWorkbench()
					.getHelpSystem()
					.setHelp(getShell(),
							"org.eclipse.ogee.utils.09c77aa71c2d4783aa84aa625ef148efODataDevelopment"); //$NON-NLS-1$
		}

		super.setVisible(visible);
	}

	/**
	 * Set the right help key for the context sensitive help
	 */
	@Override
	public void performHelp() {
		try {
			getShell()
					.setData(
							"org.eclipse.ui.help", "org.eclipse.ogee.utils.09c77aa71c2d4783aa84aa625ef148efODataDevelopment"); //$NON-NLS-1$ //$NON-NLS-2$
		} catch (NullPointerException e) {
			logger.logError(e.getMessage(), e);
		}
	}
}
