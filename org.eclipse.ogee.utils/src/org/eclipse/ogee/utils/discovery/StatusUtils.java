/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.discovery;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.wizard.WizardPage;

/**
 * Convenience methods to manage status objects.
 * 
 */
public final class StatusUtils {
	private StatusUtils() {
		// no instantiation
	}

	/**
	 * To apply the specified status to the status line of a wizard page.
	 * 
	 * @param status
	 *            status to be applied
	 * @param wizardPage
	 *            wizard page the status will be displayed on
	 */
	public static void applyToStatusLine(final IStatus status,
			final WizardPage wizardPage) {
		ContractChecker.nullCheckParam(status, "status"); //$NON-NLS-1$
		ContractChecker.nullCheckParam(wizardPage, "wizardPage"); //$NON-NLS-1$

		String message = status.getMessage();
		switch (status.getSeverity()) {
		case IStatus.OK:
			wizardPage.setMessage(message, IMessageProvider.NONE);
			wizardPage.setPageComplete(true);
			wizardPage.setErrorMessage(null);
			break;
		case IStatus.WARNING:
			wizardPage.setMessage(message, IMessageProvider.WARNING);
			wizardPage.setPageComplete(true);
			wizardPage.setErrorMessage(null);
			break;
		case IStatus.INFO:
			wizardPage.setMessage(message, IMessageProvider.INFORMATION);
			wizardPage.setPageComplete(false);
			wizardPage.setErrorMessage(null);
			break;
		default:
			if (message.length() == 0) {
				message = null;
			}
			wizardPage.setMessage(null);
			wizardPage.setErrorMessage(message);
			wizardPage.setPageComplete(false);
			break;
		}
	}

	/**
	 * Finds the most severe status from a array of stati. An error is more
	 * severe than a warning, and a warning is more severe than ok.
	 */
	public static IStatus getMostSevere(IStatus[] status) {
		IStatus max = null;
		for (int i = 0; i < status.length; i++) {
			IStatus curr = status[i];
			if (curr.matches(IStatus.ERROR)) {
				return curr;
			}
			if (max == null || curr.getSeverity() > max.getSeverity()) {
				max = curr;
			}
		}
		return max;
	}

	/**
	 * Provides a new status object with severity IStatus.CANCEL.
	 * 
	 * @param message
	 *            status message
	 * @return the cancel status
	 */
	public static IStatus statusCancel(final String message) {
		return createStatus(IStatus.CANCEL, message);
	}

	/**
	 * Provides a new status object with severity IStatus.ERROR.
	 * 
	 * @param message
	 *            status message
	 * @return the error status
	 */
	public static IStatus statusError(final String message) {
		return createStatus(IStatus.ERROR, message);
	}

	/**
	 * Provides a new status object with severity IStatus.ERROR.
	 * 
	 * @param message
	 *            status message
	 * @param ex
	 *            exception that corresponds to the status
	 * @return the error status
	 */
	public static IStatus statusError(final String message, final Throwable ex) {
		return createStatus(IStatus.ERROR, message, ex);
	}

	/**
	 * Provides a new status object with severity IStatus.WARNING.
	 * 
	 * @param message
	 *            status message
	 * @return the warning status
	 */
	public static IStatus statusWarning(final String message) {
		return createStatus(IStatus.WARNING, message);
	}

	/**
	 * Provides a new status object with severity IStatus.WARNING.
	 * 
	 * @param message
	 *            status message
	 * @param ex
	 *            exception that corresponds to the status
	 * @return the warning status
	 */
	public static IStatus statusWarning(final String message, final Throwable ex) {
		return createStatus(IStatus.WARNING, message, ex);
	}

	/**
	 * Provides a new status object with severity IStatus.INFO.
	 * 
	 * @param message
	 *            status message
	 * @return the info status
	 */
	public static IStatus statusInfo(final String message) {
		return createStatus(IStatus.INFO, message);
	}

	/**
	 * Provides a new status object with severity IStatus.OK.
	 * 
	 * @param message
	 *            status message
	 * @return the ok status
	 */
	public static IStatus statusOk(final String message) {
		return createStatus(IStatus.OK, message);
	}

	private static IStatus createStatus(final int severity, final String message) {
		return new Status(severity, "id", 0, message, null); //$NON-NLS-1$
	}

	private static IStatus createStatus(final int severity,
			final String message, final Throwable ex) {
		return new Status(severity, "id", 0, message, ex); //$NON-NLS-1$
	}

	/**
	 * Provides a new multistatus object
	 * 
	 * @param message
	 *            status message
	 * @return multistatus
	 */
	public static MultiStatus createMultiStatus(String message) {
		return new MultiStatus("id", 0, message, null); //$NON-NLS-1$
	}
}
