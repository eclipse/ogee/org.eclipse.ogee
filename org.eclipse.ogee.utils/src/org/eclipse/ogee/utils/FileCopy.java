/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.eclipse.ogee.utils.logger.Logger;

/**
 * Represents a FileCopy.
 * 
 */
public class FileCopy {
	private static Logger logger = Logger.getUtilsLogger();
	public static final String ORG_ECLIPSE_OGEE_CLIENT = "org.eclipse.ogee.client"; //$NON-NLS-1$
	public static final String LIB_FOLDER = "/lib"; //$NON-NLS-1$

	/**
	 * Copies the jar to the given destination folder.
	 * 
	 * @param destinationFolder
	 * @return - the new path of the jar.
	 */
	public static String copyLibJar(String destinationFolder, String jarPath) {
		try {

			if (jarPath == null || jarPath.equals("")) //$NON-NLS-1$
			{
				return null;
			}

			String file = copyJarFile(jarPath, new File(destinationFolder));

			return file;
		} catch (IOException e) {
			logger.logError(e.getMessage(), e);
		}

		return null;
	}

	private static String copyJarFile(String srcFile, File destinationFolder)
			throws IOException {
		File inputFile = new File(File.separator + srcFile);

		String fileName = inputFile.getName();

		File destFile = new File(destinationFolder, fileName);

		if (destFile.exists()) {
			return destFile.getAbsolutePath();
		}

		FileInputStream fin = null;
		FileOutputStream fout = null;

		try {
			fin = new FileInputStream(inputFile);
			fout = new FileOutputStream(destFile);

			byte[] b = new byte[1024];
			int noOfBytes = 0;

			// read bytes from source file and write to destination file
			while ((noOfBytes = fin.read(b)) != -1) {
				fout.write(b, 0, noOfBytes);
			}

			return destFile.getAbsolutePath();
		} catch (IOException e) {
			throw e;
		} finally {
			if (fin != null) {
				fin.close();
			}

			if (fout != null) {
				fout.close();
			}
		}
	}

	public static void writeToFile(String string, String path)
			throws IOException {
		FileOutputStream fos = null;
		OutputStreamWriter osw = null;
		BufferedWriter bw = null;

		try {
			fos = new FileOutputStream(path);
			osw = new OutputStreamWriter(fos, "UTF-8"); //$NON-NLS-1$
			bw = new BufferedWriter(osw);

			bw.write(string);
		} catch (IOException e) {
			throw e;
		} finally {
			if (bw != null) {
				bw.close();
			}

			if (osw != null) {
				osw.close();
			}

			if (fos != null) {
				fos.close();
			}
		}
	}
}
