/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.service.validation;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.olingo.odata2.api.edm.provider.EdmProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderException;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.core.edm.provider.EdmxProvider;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.utils.FileUtils;
import org.eclipse.ogee.utils.nls.messages.FrameworkUtilsMessages;
import org.eclipse.ogee.utils.olingo.parser.ConvertOlingoEdmToEdmx;
import org.eclipse.ogee.utils.service.validation.Result.Status;
import org.eclipse.osgi.util.NLS;

public class ServiceMetadataFileValidator implements
		IServiceMetadataFileValidator {

	// Olingo Parser
	private EdmProvider edmProvider;
	private ConvertOlingoEdmToEdmx convertOlingoEdmToEdmx;
	private Edmx edmx = null;
	private static final String CHARACTER_ENCODING = "UTF-8"; //$NON-NLS-1$

	public ServiceMetadataFileValidator() {

	}

	public Result validate(String serviceMetadataUri, IProgressMonitor monitor) {
		Result result = new Result();
		result.setServiceUrlValidation(false);

		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}
		InputStream inputStream = null;
		try {
			// check if the uri is a file
			if (FileUtils.isFileUriValid(serviceMetadataUri)) {
				// try to create a file from the service metadata uri
				File metadataFile = FileUtils.getFile(serviceMetadataUri);
				// get service metadata xml
				String serviceMetadataXml = FileUtils
						.readFileAsString(metadataFile);
				// get service edmx object

				inputStream = new ByteArrayInputStream(
						serviceMetadataXml.getBytes(CHARACTER_ENCODING));

				this.edmProvider = new EdmxProvider().parse(inputStream, false);
				this.convertOlingoEdmToEdmx = new ConvertOlingoEdmToEdmx(
						edmProvider);
				this.edmx = convertOlingoEdmToEdmx.convertToEdmx();

				if (this.edmx == null) {
					result.setStatus(Status.ERROR);
					result.setMessage(FrameworkUtilsMessages.ServiceMetadataFileValidator_0);
				} else {
					// set service metadata uri
					result.setServiceMetadataUri(metadataFile.toURI()
							.toString());
					// set metadata xml
					result.setServiceMetadataXml(serviceMetadataXml);
					// set edmx
					result.setEdmx(this.edmx);
					// get service name
					String serviceName = EdmxUtilities
							.extractServiceName(this.edmx);
					// set service name
					result.setServiceName(serviceName);
				}
			} else {
				result.setStatus(Status.ERROR);
				result.setMessage(NLS.bind(
						FrameworkUtilsMessages.ServiceMetadataFileValidator_1,
						serviceMetadataUri));
			}
		}
		// Error handling

		catch (EntityProviderException e) {
			result.setException(e);
			result.setStatus(Status.ERROR);
			result.setMessage(FrameworkUtilsMessages.ServiceMetadataFileValidator_2);
		} catch (IOException e) {
			result.setException(e);
			result.setStatus(Status.ERROR);
			result.setMessage(e.getLocalizedMessage());
		} catch (ODataException e) {
			result.setException(e);
			result.setStatus(Status.ERROR);
			result.setMessage(e.getLocalizedMessage());
		} catch (Exception e) {
			result.setException(e);
			result.setStatus(Status.ERROR);
			result.setMessage(e.getLocalizedMessage());
		} finally {
			monitor.done();
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					result.setException(e);
					result.setStatus(Status.ERROR);
					result.setMessage(e.getLocalizedMessage());
				}
			}
		}
		return result;
	}
}
