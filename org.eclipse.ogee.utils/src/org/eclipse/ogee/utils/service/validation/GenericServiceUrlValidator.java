/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.service.validation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;

import org.apache.olingo.odata2.api.edm.provider.EdmProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderException;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.core.edm.provider.EdmxProvider;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestResponse;
import org.eclipse.ogee.client.connectivity.api.IServerConnectionParameters;
import org.eclipse.ogee.client.connectivity.api.RestClientFactory;
import org.eclipse.ogee.client.connectivity.impl.RestRequest;
import org.eclipse.ogee.client.exceptions.RestClientException;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.utils.ServerConnectionParametersUtil;
import org.eclipse.ogee.utils.StringParsingUtils;
import org.eclipse.ogee.utils.nls.messages.FrameworkUtilsMessages;
import org.eclipse.ogee.utils.olingo.parser.ConvertOlingoEdmToEdmx;
import org.eclipse.ogee.utils.service.validation.Result.Status;

public class GenericServiceUrlValidator implements IServiceUrlValidator {
	private static final String HTTPS = "https"; //$NON-NLS-1$

	private static final String HTTP = "http"; //$NON-NLS-1$

	protected static final String SLASH_SUFFIX = "/"; //$NON-NLS-1$

	protected static final String METADATA_SUFFIX = "$metadata"; //$NON-NLS-1$

	// Olingo Parser
	private EdmProvider edmProvider;
	private ConvertOlingoEdmToEdmx convertOlingoEdmToEdmx;
	private Edmx edmx = null;
	private static final String CHARACTER_ENCODING = "UTF-8"; //$NON-NLS-1$

	public GenericServiceUrlValidator() {
	}

	/**
	 * Validate syntax of a service URL
	 * 
	 * @param serviceUrl
	 * @return true if the URL is valid
	 */
	public static boolean isServiceUrlSyntaxValid(String serviceUrl) {

		if (serviceUrl != null) {
			serviceUrl = serviceUrl.trim();
		}

		if (serviceUrl == null || serviceUrl.isEmpty()) {
			return false;
		}

		try {
			URI uri = new URI(serviceUrl);
			String scheme = uri.getScheme();
			if (scheme == null) {
				uri = new URI(HTTP + "://" + serviceUrl); //$NON-NLS-1$
				scheme = uri.getScheme();
			}

			URL url = uri.toURL();
			String host = url.getHost();
			if (host == null || host.isEmpty())
				return false;
			return scheme.toLowerCase(Locale.ENGLISH).equals(HTTP)
					|| scheme.toLowerCase(Locale.ENGLISH).equals(HTTPS);
		} catch (URISyntaxException e) {
			return false;
		} catch (MalformedURLException e) {
			return false;
		}
	}

	@Override
	public Result validate(String serviceUrl, IProgressMonitor monitor) {
		Throwable exception = null;
		Status status = Result.Status.OK;
		String message = Result.EMPTY;
		Result result = new Result();
		result.setStatus(status);
		result.setMessage(FrameworkUtilsMessages.GenericServiceValidator_5);

		if (serviceUrl != null) {
			serviceUrl = serviceUrl.trim();
		}

		if (serviceUrl == null || serviceUrl.isEmpty()) {
			Result error_result = new Result();
			error_result.setStatus(Result.Status.ERROR);
			error_result
					.setMessage(FrameworkUtilsMessages.GenericServiceValidator_1);

			return error_result;
		}

		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}
		InputStream inputStream = null;
		try {
			// empty input check is not needed here since this
			// button will be enabled only on a non empty input.
			monitor.beginTask(FrameworkUtilsMessages.GenericServiceValidator_2,
					8);
			// remove all slashes at the end of the url
			serviceUrl = removeSlashSuffix(serviceUrl);

			IServerConnectionParameters serverConnectionParams = ServerConnectionParametersUtil
					.createServerConnectionParameters(serviceUrl);
			if (serverConnectionParams == null) {
				message = FrameworkUtilsMessages.GenericServiceValidator_3;
				status = Status.ERROR;
			} else if (serverConnectionParams.getHost() == null) {
				result.setStatus(Status.CONN_PARAMS_VALIDATION_CANCELED);
				result.setMessage(FrameworkUtilsMessages.GenericServiceValidator_4);
			} else {
				result.setServerConnection(serverConnectionParams);

				monitor.worked(1);

				// create client
				IRestClient client = RestClientFactory
						.createInstance(serverConnectionParams);
				result.setClient(client);

				monitor.worked(1);

				// set original service url
				result.setOriginalServiceUrl(serviceUrl);

				result.setBaseUrl(removeURLParameters(serviceUrl));
				result.setUrlParameters(extractURLParameters(serviceUrl));

				monitor.worked(1);

				// get the metadata and parse it
				String serviceMetadataXml = getServiceMetadataXml(result);
				if (serviceMetadataXml.startsWith("<html")) //$NON-NLS-1$
				{
					Result error_result = new Result();
					error_result.setStatus(Result.Status.ERROR);
					error_result
							.setMessage(FrameworkUtilsMessages.InvalidXMLResponse);

					return error_result;
				}

				result.setServiceMetadataXml(serviceMetadataXml);

				monitor.worked(1);

				// get service document
				String serviceDocumentXml = getServiceDocumentXml(result);
				result.setServiceDocumentXml(serviceDocumentXml);

				monitor.worked(1);

				inputStream = new ByteArrayInputStream(
						serviceMetadataXml.getBytes(CHARACTER_ENCODING));

				// get service edmx object

				this.edmProvider = new EdmxProvider().parse(inputStream, false);
				this.convertOlingoEdmToEdmx = new ConvertOlingoEdmToEdmx(
						edmProvider);
				this.edmx = convertOlingoEdmToEdmx.convertToEdmx();

				if (!isEntitySetsFound(edmx)) {
					result.setStatus(Status.CANNOT_BE_CONSUMED);
					result.setMessage(FrameworkUtilsMessages.GenericServiceValidator_10);
				}
				result.setEdmx(edmx);

				monitor.worked(1);

				// get service name
				String serviceName = EdmxUtilities
						.extractServiceName(this.edmx);
				result.setServiceName(serviceName);
				result.setServiceUrl(serviceUrl);
				monitor.worked(1);
			}
		}
		// Error handling
		catch (EntityProviderException e) {
			result.setException(e);
			result.setStatus(Status.ERROR);
			result.setMessage(FrameworkUtilsMessages.GenericServiceValidator_6);
		} catch (ODataException e) {
			result.setException(e);
			result.setStatus(Status.ERROR);
			result.setMessage(e.getLocalizedMessage());
		} catch (RestClientException e) {
			exception = e;
			status = Status.ERROR;
			message = restClientErrorMessage(e);
		} catch (MalformedURLException e) {
			exception = e;
			status = Status.ERROR;
			message = FrameworkUtilsMessages.GenericServiceValidator_3;
		}// CHECKSTYLE:OFF
		catch (Throwable e) {// CHECKSTYLE:ON
			exception = e;
			status = Status.ERROR;
			message = FrameworkUtilsMessages.GenericServiceValidator_3;
		} finally {
			monitor.done();

			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					result.setException(e);
					result.setStatus(Status.ERROR);
					result.setMessage(e.getLocalizedMessage());
				}
			}

		}

		if (status == Status.ERROR) {
			Result error_result = new Result();
			error_result.setException(exception);
			error_result.setStatus(status);
			error_result.setMessage(message);

			return error_result;
		}

		// the result is valid
		return result;
	}

	protected String extractURLParameters(String serviceUrl) {
		return StringParsingUtils.extractURLParametersString(serviceUrl);
	}

	protected boolean isEntitySetsFound(Edmx edmx) {
		boolean isEntitySetsFound = false;

		Schema[] schemas = edmx.getEdmxDataServices().getSchemas();
		for (Schema schema : schemas) {
			EntityContainer[] entityContainers = schema.getEntityContainers();
			for (EntityContainer entityContainer : entityContainers) {
				if (entityContainer.getEntitySets().length > 0) // no entity
																// sets
				{
					isEntitySetsFound = true;

				}
			}
		}

		return isEntitySetsFound;
	}

	protected String getServiceDocumentXml(Result result)
			throws RestClientException {
		if (result == null) {
			return null;
		}

		String serviceUrl = result.getServiceUrl();
		if (serviceUrl == null) {
			return null;
		}

		IRestClient restClient = result.getRestClient();
		if (restClient == null) {
			return null;
		}

		String serviceDocumentUrl = buildServiceDocumentUrl(result);

		String serviceDocumentXml = executeRequest(serviceDocumentUrl,
				restClient);

		return serviceDocumentXml;
	}

	protected String buildServiceDocumentUrl(Result result) {
		String serviceDocumentUrl = null;
		String baseUrl = result.getBaseUrl();
		String urlParameters = result.getUrlParameters();

		// add "/" if needed
		baseUrl = addSlashSuffix(baseUrl);
		serviceDocumentUrl = baseUrl + urlParameters;
		return serviceDocumentUrl;
	}

	/**
	 * Receives a rest client exception and returns a message.
	 * 
	 * @param rce
	 *            - Rest client exception
	 * @return
	 */
	protected String restClientErrorMessage(RestClientException rce) {
		String message = FrameworkUtilsMessages.GenericServiceValidator_8;

		if (rce.getStatusCode() == 401) {
			message = FrameworkUtilsMessages.GenericServiceValidator_9;
		}

		return message;
	}

	/**
	 * Extracts the metadata from the given uri
	 * 
	 * @param result
	 * @return - the metadata
	 * @throws RestClientException
	 */
	protected String getServiceMetadataXml(Result result)
			throws RestClientException {
		if (result == null) {
			return null;
		}

		String originalServiceUrl = result.getOriginalServiceUrl();
		if (originalServiceUrl == null) {
			return null;
		}

		IRestClient restClient = result.getRestClient();
		if (restClient == null) {
			return null;
		}

		String serviceMetadataUrl = buildMetadataUrl(result);

		return executeRequest(serviceMetadataUrl, restClient);
	}

	protected String buildMetadataUrl(Result result) {
		String metadataUrl = null;
		String baseUrl = result.getBaseUrl();
		String urlParameters = result.getUrlParameters();

		if (baseUrl == null) {
			return null;
		}

		if (urlParameters == null) {
			urlParameters = ""; //$NON-NLS-1$
		}

		if (!baseUrl.contains(METADATA_SUFFIX)) {
			// add "/" if needed
			baseUrl = addSlashSuffix(baseUrl);
			metadataUrl = baseUrl + METADATA_SUFFIX + urlParameters;
		} else {
			metadataUrl = baseUrl + urlParameters;
		}
		return metadataUrl;

	}

	protected String validateMetadataSuffix(String serviceUrl) {
		if (!serviceUrl.contains(METADATA_SUFFIX)) {
			int index = serviceUrl.indexOf('?');
			if (index == -1) {
				// add "/" if needed
				serviceUrl = addSlashSuffix(serviceUrl);
				serviceUrl = serviceUrl + METADATA_SUFFIX;
			} else {
				String cleanUrl = serviceUrl.substring(0, index);
				// add "/" if needed
				cleanUrl = addSlashSuffix(cleanUrl);
				String urlParameters = serviceUrl.substring(index);
				serviceUrl = cleanUrl + METADATA_SUFFIX + urlParameters;
			}
		}
		return serviceUrl;
	}

	private String addSlashSuffix(String serviceUrl) {
		if (!serviceUrl.endsWith(SLASH_SUFFIX)) {
			serviceUrl = serviceUrl + SLASH_SUFFIX;
		}

		return serviceUrl;
	}

	/**
	 * Executes a Rest client request with the given url.
	 * 
	 * @param url
	 *            request url
	 * @param clinet
	 *            rest clinet
	 * @return response body
	 * @throws RestClientException
	 */
	protected String executeRequest(String url, IRestClient client)
			throws RestClientException {
		RestRequest request = new RestRequest(url);
		IRestResponse response = client.execute(request);
		String result = response.getBody();

		return result;
	}

	/**
	 * Removes the slash from the end of the given url.
	 * 
	 * @param url
	 * @return - the url without the last slash.
	 */
	protected String removeSlashSuffix(String url) {
		if (url != null) {
			while (url.endsWith(SLASH_SUFFIX)) {
				url = url.substring(0, url.lastIndexOf(SLASH_SUFFIX));
			}
		}

		return url;
	}

	/**
	 * Removes the parameters from the given url.
	 * 
	 * @param inputUri
	 * @return - the url without the parameters.
	 */
	protected String removeURLParameters(String inputUri) {
		return StringParsingUtils.removeURLParameters(inputUri);
	}

	@Override
	public Result validateSimple(String serviceUrl, IProgressMonitor monitor) {
		final class SimpleGenericServiceUrlValidator extends
				GenericServiceUrlValidator {
			@Override
			protected String validateMetadataSuffix(String serviceUrl) {
				return serviceUrl;
			}

			@Override
			protected String removeURLParameters(String inputUri) {
				return inputUri;
			}

			@Override
			protected boolean isEntitySetsFound(Edmx edmx) {
				return true;
			}

			@Override
			protected String getServiceDocumentXml(Result result)
					throws RestClientException {
				return null;
			}
		}

		SimpleGenericServiceUrlValidator simpleValidator = new SimpleGenericServiceUrlValidator();

		return simpleValidator.validate(serviceUrl, monitor);
	}
}
