/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.service.validation;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ogee.utils.activator.Activator;
import org.eclipse.ogee.utils.logger.Logger;

public class ServiceValidator {
	/**
	 * returns service url validator
	 * 
	 * @return service url validator
	 */
	public static IServiceUrlValidator getServiceUrlValidator() {
		// set default service url validator
		IServiceUrlValidator serviceUrlValidator = new GenericServiceUrlValidator();

		try {
			// get service url validator
			IConfigurationElement[] svConfigElems = Platform
					.getExtensionRegistry().getConfigurationElementsFor(
							Activator.PLUGIN_ID + ".service_url_validator"); //$NON-NLS-1$

			for (IConfigurationElement svConfigElem : svConfigElems) {
				// load first service url validator and exit
				serviceUrlValidator = (IServiceUrlValidator) svConfigElem
						.createExecutableExtension("ServiceUrlValidator"); //$NON-NLS-1$

				return serviceUrlValidator;
			}
		} catch (CoreException e) {
			Logger.getUtilsLogger().logError(e);
		}

		return serviceUrlValidator;
	}

	/**
	 * returns service metadata file validator
	 * 
	 * @return service metadata file validator
	 */
	public static IServiceMetadataFileValidator getServiceMetadataValidator() {
		IServiceMetadataFileValidator serviceMetadataFileValidator = new ServiceMetadataFileValidator();

		return serviceMetadataFileValidator;
	}

	/**
	 * returns service document file validator
	 * 
	 * @return service document file validator
	 */
	public static IServiceDocumentFileValidator getServiceDocumentValidator() {
		IServiceDocumentFileValidator serviceDocumentFileValidator = new ServiceDocumentFileValidator();

		return serviceDocumentFileValidator;
	}
}
