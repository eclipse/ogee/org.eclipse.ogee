/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.service.validation;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ogee.client.exceptions.ParserException;
import org.eclipse.ogee.client.model.atom.AppService;
import org.eclipse.ogee.client.parser.IODataParser;
import org.eclipse.ogee.client.parser.ODataParserFactory;
import org.eclipse.ogee.client.parser.Representation;
import org.eclipse.ogee.utils.FileUtils;
import org.eclipse.ogee.utils.nls.messages.FrameworkUtilsMessages;
import org.eclipse.ogee.utils.service.validation.Result.Status;

public class ServiceDocumentFileValidator implements
		IServiceDocumentFileValidator {
	// odata atom parser
	private IODataParser atomParser;

	public ServiceDocumentFileValidator() {
		this.atomParser = ODataParserFactory
				.createInstance(Representation.ATOM);
	}

	@Override
	public Result validate(String serviceDocumentUri, IProgressMonitor monitor) {
		Result result = new Result();
		result.setServiceUrlValidation(false);

		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		try {
			// check if the input is a file
			if (FileUtils.isFileUriValid(serviceDocumentUri)) {
				// try to create a file from the service document uri
				File serviceDocumentFile = FileUtils
						.getFile(serviceDocumentUri);
				// get service document xml
				String serviceDocumentXml = FileUtils
						.readFileAsString(serviceDocumentFile);
				// get service document object
				AppService appService = (AppService) this.atomParser
						.parseServiceDocument(serviceDocumentXml);
				if (appService == null) {
					result.setStatus(Status.ERROR);
					result.setMessage(FrameworkUtilsMessages.ServiceDocumentFileValidator_0);
				} else {
					// set service document uri
					result.setServiceDocumentUri(serviceDocumentFile.toURI()
							.toString());
					// set service document xml
					result.setServiceDocumentXml(serviceDocumentXml);
					// set app service
					result.setAppService(appService);
					// get service url
					String serviceUrl = appService.getXmlBase();
					// set service url
					result.setServiceUrl(serviceUrl);
				}
			} else {
				result.setStatus(Status.ERROR);
				result.setMessage(FrameworkUtilsMessages.ServiceDocumentFileValidator_1);
			}
		}
		// Error handling
		catch (ParserException e) {
			result.setException(e);
			result.setStatus(Status.ERROR);
			result.setMessage(FrameworkUtilsMessages.ServiceDocumentFileValidator_2);
		} catch (IOException e) {
			result.setException(e);
			result.setStatus(Status.ERROR);
			result.setMessage(FrameworkUtilsMessages.ServiceDocumentFileValidator_3);
		} finally {
			monitor.done();
		}

		return result;
	}
}
