/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.service.validation;

import org.eclipse.core.runtime.IProgressMonitor;

public interface IServiceUrlValidator {
	/**
	 * validation of service url
	 * 
	 * @param serviceUrl
	 * @param monitor
	 * @return
	 */
	public Result validate(String serviceUrl, IProgressMonitor monitor);

	/**
	 * simple validation service url
	 * 
	 * @param serviceUrl
	 * @param monitor
	 * @return
	 */
	public Result validateSimple(String serviceUrl, IProgressMonitor monitor);
}
