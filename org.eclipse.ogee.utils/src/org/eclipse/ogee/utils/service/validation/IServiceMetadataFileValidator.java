/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.service.validation;

import org.eclipse.core.runtime.IProgressMonitor;

public interface IServiceMetadataFileValidator {
	/**
	 * Validates the service metadata file and updates the progress monitor.
	 * 
	 * @param monitor
	 *            - progress monitor
	 * @param metadataUri
	 *            - the service's metadata uri
	 * 
	 * @return validation result
	 */
	public Result validate(String serviceMetadataUri, IProgressMonitor monitor);
}
