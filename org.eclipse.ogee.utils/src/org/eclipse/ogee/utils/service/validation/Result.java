/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils.service.validation;

import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IServerConnectionParameters;
import org.eclipse.ogee.client.connectivity.impl.ServerConnectionParameters;
import org.eclipse.ogee.client.model.atom.AppService;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.v3.EdmxV3;
import org.eclipse.ogee.client.model.generic.IMediaResource;
import org.eclipse.ogee.client.model.generic.ODataEntry;
import org.eclipse.ogee.client.model.generic.ODataLink;
import org.eclipse.ogee.client.model.generic.ODataProperty;
import org.eclipse.ogee.utils.nls.messages.FrameworkUtilsMessages;

public class Result {
	private Throwable exception;
	private Status status;
	private String message;
	private String serviceUrl;

	private String baseUrl;
	private String urlParameters;

	private String originalServiceUrl;
	private String serviceMetadataXml;
	private Edmx edmx;
	private String serviceName;
	private String serviceDocumentXml;
	private IServerConnectionParameters serverConParams;
	private IRestClient client;
	private AppService appService;
	private String serviceMetadataUri;
	private String serviceDocumentUri;
	private ODataEntry serviceEntry;
	private boolean isServiceUrlValidation;
	private String catalogServicePath;
	private String serverType;

	public static final String EMPTY = ""; //$NON-NLS-1$
	public static final IServerConnectionParameters EMPTY_GW = new ServerConnectionParameters(
			null, null);

	public static enum Status {
		OK, ERROR, CONN_PARAMS_VALIDATION_CANCELED, CUSTOM_ERROR, CANNOT_BE_CONSUMED
	}

	public static final ODataEntry EMPTY_SERVICE_ENTRY = new ODataEntry() {
		@Override
		public void putProperty(String key, String value) {
			// empty method
		}

		@Override
		public void putProperty(ODataProperty property) {
			// empty method
		}

		@Override
		public boolean isMediaLinkEntry() {
			return false;
		}

		@Override
		public ODataProperty getProperty(String key) {
			return null;
		}

		@Override
		public ODataProperty[] getProperties() {
			return new ODataProperty[0];
		}

		@Override
		public IMediaResource getMediaResource() {
			return null;
		}

		@Override
		public ODataLink[] getLinks() {
			return new ODataLink[0];
		}

		@Override
		public String getId() {
			return ""; //$NON-NLS-1$
		}

		@Override
		public String getType() {
			return null;
		}
	};

	public Result() {
		this.status = Status.OK;
		this.appService = null;
		this.exception = null;
		this.client = null;
		this.edmx = null;
		this.serverConParams = EMPTY_GW;
		this.message = FrameworkUtilsMessages.Result_0;
		this.serviceDocumentUri = EMPTY;
		this.serviceDocumentXml = EMPTY;
		this.serviceMetadataUri = EMPTY;
		this.serviceMetadataXml = EMPTY;
		this.serviceName = EMPTY;
		this.serviceUrl = EMPTY;
		this.originalServiceUrl = EMPTY;
		this.serviceEntry = EMPTY_SERVICE_ENTRY;
		this.isServiceUrlValidation = true;
	}

	public Result(Result result) {
		this();

		if (result != null) {
			this.status = result.getStatus();
			this.appService = result.getAppService();
			this.exception = result.getException();
			this.client = result.getRestClient();
			this.edmx = result.getEdmx();			
			this.serverConParams = result.getServerConnectionParameters();
			this.message = result.getMessage();
			this.serviceDocumentUri = result.getServiceDocumentUri();
			this.serviceDocumentXml = result.getServiceDocumentXml();
			this.serviceMetadataUri = result.getServiceMetadataUri();
			this.serviceMetadataXml = result.getServiceMetadataXml();
			this.serviceName = result.getServiceName();
			this.serviceUrl = result.getServiceUrl();
			this.originalServiceUrl = result.getOriginalServiceUrl();
			this.serviceEntry = result.getServiceEntry();
			this.isServiceUrlValidation = result.isServiceUrlValidation();
		}
	}

	public boolean isServiceUrlValidation() {
		return isServiceUrlValidation;
	}

	public void setServiceUrlValidation(boolean isServiceUrlValidation) {
		this.isServiceUrlValidation = isServiceUrlValidation;
	}

	public ODataEntry getServiceEntry() {
		return this.serviceEntry;
	}

	public String getServiceMetadataUri() {
		return this.serviceMetadataUri;
	}

	public String getServiceDocumentUri() {
		return this.serviceDocumentUri;
	}

	public AppService getAppService() {
		return this.appService;
	}

	public IRestClient getRestClient() {
		return this.client;
	}

	public IServerConnectionParameters getServerConnectionParameters() {
		return this.serverConParams;
	}

	public Throwable getException() {
		return this.exception;
	}

	public Status getStatus() {
		return this.status;
	}

	public String getMessage() {
		return this.message;
	}

	public String getServiceUrl() {
		return this.serviceUrl;
	}

	public String getOriginalServiceUrl() {
		return this.originalServiceUrl;
	}

	public String getServiceDocumentXml() {
		return this.serviceDocumentXml;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	public String getServiceMetadataXml() {
		return this.serviceMetadataXml;
	}

	public EdmxV3 getEdmx() {
		return (EdmxV3) this.edmx;
	}

	public Edmx getEdmx1() {
		return this.edmx;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	public void setOriginalServiceUrl(String originalServiceUrl) {
		this.originalServiceUrl = originalServiceUrl;
	}

	public void setServiceMetadataXml(String serviceMetadataXml) {
		this.serviceMetadataXml = serviceMetadataXml;
	}

	public void setEdmx(Edmx edmx) {
		this.edmx = edmx;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public void setServiceDocumentXml(String serviceDocumentXml) {
		this.serviceDocumentXml = serviceDocumentXml;
	}

	public void setServerConnection(IServerConnectionParameters connectionParams) {
		this.serverConParams = connectionParams;
	}

	public void setClient(IRestClient client) {
		this.client = client;
	}

	public void setAppService(AppService appService) {
		this.appService = appService;
	}

	public void setServiceMetadataUri(String serviceMetadataUri) {
		this.serviceMetadataUri = serviceMetadataUri;
	}

	public void setServiceDocumentUri(String serviceDocumentUri) {
		this.serviceDocumentUri = serviceDocumentUri;
	}

	public void setServiceEntry(ODataEntry serviceEntry) {
		this.serviceEntry = serviceEntry;
	}

	public String getCatalogServicePath() {
		return this.catalogServicePath;
	}

	public void setCatalogServicePath(String catalogServicePath) {
		this.catalogServicePath = catalogServicePath;
	}

	public String getServerType() {
		return this.serverType;
	}

	public void setServerType(String serverType) {
		this.serverType = serverType;
	}

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * @param baseUrl
	 *            the baseUrl to set
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * @return the urlParameters
	 */
	public String getUrlParameters() {
		return urlParameters;
	}

	/**
	 * @param urlParameters
	 *            the urlParameters to set
	 */
	public void setUrlParameters(String urlParameters) {
		this.urlParameters = urlParameters;
	}
}
