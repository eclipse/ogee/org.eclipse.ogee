/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.Locale;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.nls.messages.FrameworkUtilsMessages;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Bundle;

public class FileUtils {
	private static final String UTF_8 = "UTF-8"; //$NON-NLS-1$
	private static final String FILE_SCHEMA = "file:"; //$NON-NLS-1$

	public static String readFileAsString(File file)
			throws FileNotFoundException, java.io.IOException {
		byte[] buffer = new byte[(int) file.length()];
		BufferedInputStream f = null;
		try {
			f = new BufferedInputStream(new FileInputStream(file));
			f.read(buffer);
		} finally {
			if (f != null) {
				f.close();
			}
		}

		return new String(buffer, UTF_8);
	}

	/**
	 * Validate syntax of a service file uri
	 * 
	 * @param fileUri
	 * @return
	 */
	public static boolean isFileUriValid(String fileUri) {
		if (fileUri != null) {
			fileUri = fileUri.trim();
		}

		if (fileUri == null || fileUri.isEmpty()) {
			return false;
		}

		// check if is a file path
		File file = getFile(fileUri);
		if (file.isFile()) {
			return true;
		}

		return false;
	}

	/**
	 * returns a file located by the specified path.
	 * 
	 * @param filePath
	 * @return a file located by the specified path
	 */
	public static File getFile(String filePath) {
		try {
			if (filePath != null) {
				filePath = filePath.trim();
			}

			if (filePath == null || filePath.isEmpty()) {
				return null;
			}

			if (filePath.toLowerCase(Locale.ENGLISH).startsWith(FILE_SCHEMA)) {
				filePath = filePath.substring(FILE_SCHEMA.length());
			}

			filePath = URLDecoder.decode(filePath, UTF_8);
			File file = new File(filePath);
			URI uri = file.toURI();

			file = new File(uri);

			return file;
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	/**
	 * Method that create new directory
	 * 
	 * @param dirPath
	 *            The path where the new directory should be created.
	 * @return True if the operation has finished successfully and false
	 *         otherwise.
	 */
	public static boolean makeDir(String dirPath) {
		File f = new File(dirPath);
		return f.mkdirs();
	}

	/**
	 * Method that create a directory, considering overwrite parameter value. If
	 * overwrite is true, the method will delete (recursively) the folder, if
	 * exist, and then create a new folder in this path. If overwrite is false,
	 * the method will create a new folder in this path, only if the folder is
	 * not already exist.
	 * 
	 * @param dirPath
	 *            The path where the new directory should be created.
	 * @param overwrite
	 *            Indicates if exists folder with the same path should be
	 *            overwritten.
	 * @return True if the operation has finished successfully and false
	 *         otherwise.
	 */
	public static boolean makeDir(String dirPath, boolean overwrite) {
		boolean success = true;
		if (overwrite) {
			// overwrite folder if already exist
			File dir = new File(dirPath);
			if (dir.exists()) {
				success = deleteDir(dir);
			}
			success = success && makeDir(dirPath);
		} else {
			// make folder only if not already exist
			if (!(new File(dirPath)).exists()) {
				success = makeDir(dirPath);
			}
		}
		return success;
	}

	/**
	 * Method that deletes a directory (given by its string path) and all its
	 * content recursively.
	 * 
	 * @param directoryPath
	 *            The path to the directory to delete.
	 * @return True if the deletion succeeded, or false otherwise.
	 */
	public static boolean deleteDir(String directoryPath) {
		File dir = new File(directoryPath);
		return FileUtils.deleteDir(dir);
	}

	/**
	 * Method that deletes a given directory and all its content recursively.
	 * 
	 * @param dir
	 *            The file object represents the directory to delete.
	 * @return True if the deletion succeeded, or false otherwise.
	 */
	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		// Delete file and if directory is empty delete it
		return dir.delete();
	}

	/**
	 * Method that copies the content of a directory located in a specified
	 * path, into a specified destination
	 * 
	 * @param dirSourceFullPath
	 *            The source directory absolute path.
	 * @param dirTargetFullPath
	 *            The output directory absolute path.
	 * @return Directory The directory object represents the copied output
	 *         directory.
	 * @throws IOException
	 */
	public static File copyDir(String dirSourceFullPath,
			String dirTargetFullPath) throws IOException {
		File sourceDir = new File(dirSourceFullPath);
		File destinationDir = new File(dirTargetFullPath);
		copyDir(sourceDir, destinationDir);
		return destinationDir;
	}

	private static void copyDir(File sourceDir, File destinationDir)
			throws IOException {
		if (sourceDir.isDirectory()) {
			// if directory not exists, create it
			if (!destinationDir.exists()) {
				destinationDir.mkdirs();
			}

			// list all the directory contents and copy the children recursively
			String children[] = sourceDir.list();

			for (String child : children) {
				File srcChildFile = new File(sourceDir, child);
				File destChildFile = new File(destinationDir, child);
				copyDir(srcChildFile, destChildFile);
			}
		} else {
			// if file, then copy it
			String srcFilePath = sourceDir.getAbsolutePath();
			String destFilePath = destinationDir.getAbsolutePath();
			copyFile(srcFilePath, destFilePath);
		}
	}

	/**
	 * Creates a new file (does not overwrite existing files)
	 * 
	 * @param filePath
	 *            The path of the file to create.
	 * @throws IOException
	 */
	public static void createNewFile(String filePath) throws IOException {
		File f = new File(filePath);
		f.createNewFile();
	}

	/**
	 * Creates a new file with option to overwrite it if its already exist
	 * 
	 * @param filePath
	 *            The path of the file to create.
	 * @param overwrite
	 *            Should the new file overwrite an existing file with the same
	 *            path
	 * @throws IOException
	 */
	public static void createNewFile(String filePath, boolean overwrite)
			throws IOException {
		File f = new File(filePath);
		if (overwrite) {
			if (f.exists()) {
				f.delete();
			}
		}
		f.createNewFile();
	}

	/**
	 * Method that deletes a given file.
	 * 
	 * @param file
	 *            The path of the file to delete.
	 * @return True if the deletion succeeded, or false otherwise.
	 */
	public static boolean deleteFile(String file) {
		File f = new File(file);
		return f.delete();
	}

	/**
	 * Save string to existing file
	 * 
	 * @param filePath
	 *            The path of the output file.
	 * @param text
	 *            The content to save in the output file.
	 * @throws IOException
	 */
	public static void saveStringToFile(String filePath, String text)
			throws IOException {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(filePath));
			writer.write(text);
		} catch (IOException e) {
			Logger.getUtilsLogger().logError(
					NLS.bind(FrameworkUtilsMessages.FileUtils_0, filePath), e);
			throw e;
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				Logger.getUtilsLogger().logWarning(
						FrameworkUtilsMessages.FileUtils_1, e);
			}
		}
	}

	/**
	 * Copy the shared static resources to a given destination
	 * 
	 * @param bundle
	 *            The bundle object in it the directory is located.
	 * @param projectDestination
	 *            A path to the destination directory where the shared static
	 *            resources files will be copied to
	 * @param sharedStaticResourceDirectory
	 *            The path in the bundle that contains all shared static
	 *            resources.
	 * @param sharedStaticResources
	 *            The array that contains the names of all shared static
	 *            resources to be copied to the destination
	 * @throws IOException
	 *             , URISyntaxException
	 */
	public static void copyResourcesFromBundle(Bundle bundle,
			String projectDestination, String sharedStaticResourceDirectory,
			String[] sharedStaticResources) throws IOException,
			URISyntaxException {
		if (sharedStaticResources != null) {
			for (int i = 0; i < sharedStaticResources.length; i++) {
				FileUtils.copyDirOrFileFromBundle(bundle,
						sharedStaticResourceDirectory
								+ sharedStaticResources[i], projectDestination,
						sharedStaticResources[i]);
			}
		}
	}

	/**
	 * Copy the File or the Directory with its content from a bundle into a
	 * destination directory
	 * 
	 * @param bundle
	 *            The bundle object in it the directory is located.
	 * @param relativeDirPathInBundle
	 *            Directory path relative to bundle.
	 * @param destinationDirPath
	 *            A path to the destination directory where the content of the
	 *            bundle directory will be copied into. Note that this folder
	 *            must be already exist.
	 * @throws IOException
	 *             , URISyntaxException
	 */
	public static void copyDirOrFileFromBundle(Bundle bundle,
			String relativePathInBundle, String destinationDirPath,
			String resourceName) throws IOException, URISyntaxException {

		destinationDirPath = normalizeFolderPath(destinationDirPath);

		// get the bundle file as File object
		File bundleFile;
		bundleFile = convertBundleFilePathToFile(bundle, relativePathInBundle);

		// check if this bundle entry represents a file or folder, and handle
		// accordingly:
		if (bundleFile.isDirectory()) {

			// This entry represents a folder:
			// create this folder in the destination, and continue to the next
			// entry element
			FileUtils.makeDir(destinationDirPath + resourceName);

			for (File file : bundleFile.listFiles()) {
				copyDirOrFileFromBundle(
						bundle,
						relativePathInBundle + "/" + file.getName(), destinationDirPath + resourceName, file.getName()); //$NON-NLS-1$
			}
		} else {
			// handle as static/resource file (copy file as it is)
			String fileTargetFullPath = destinationDirPath + resourceName;

			// check that in case of a file, all the dirs in its path already
			// exist, and if not create them
			String fileName = bundleFile.getName();

			// remove the folder path (in bundle) from file relative path, and
			// create the target path
			int dirPathIndex = fileTargetFullPath.lastIndexOf(fileName);
			String pathToFileInTarget = fileTargetFullPath.substring(0,
					dirPathIndex);
			if (!((new File(pathToFileInTarget)).exists())) {
				FileUtils.makeDir(pathToFileInTarget);
			}

			try {
				// after we are sure all dirs in path to file exists, just copy
				// the file
				copyBundleFile(bundle, relativePathInBundle, fileTargetFullPath);
			} catch (IOException e) {
				throw e;
			}
		}
	}

	/**
	 * Copy the content of a bundle directory, into a destination directory
	 * 
	 * @param bundle
	 *            The bundle object in it the directory is located.
	 * @param relativeDirPathInBundle
	 *            Directory path relative to bundle.
	 * @param destinationDirPath
	 *            A path to the destination directory where the content of the
	 *            bundle directory will be copied into. Note that this folder
	 *            must be already exist.
	 * @throws IOException
	 *             , URISyntaxException
	 */
	public static void copyDirFromBundle(Bundle bundle,
			String relativeDirPathInBundle, String destinationDirPath)
			throws IOException, URISyntaxException {

		destinationDirPath = normalizeFolderPath(destinationDirPath);
		relativeDirPathInBundle = normalizeFolderPath(relativeDirPathInBundle);

		Enumeration<URL> filesEnum = bundle.findEntries(new Path(
				relativeDirPathInBundle).toString(), "*", true); //$NON-NLS-1$
		// enumerate all entries in bundle folder, recursively (also in
		// subfolders)
		while (filesEnum.hasMoreElements()) {
			// get relative path of file in the bundle
			String filePathInBundle = ((URL) filesEnum.nextElement()).getFile();

			// remove the folder path (in bundle) from file relative path, and
			// create the target path
			int dirPathIndex = filePathInBundle
					.indexOf(relativeDirPathInBundle);
			String filePathInTarget = filePathInBundle.substring(dirPathIndex
					+ relativeDirPathInBundle.length());

			// get the bundle file as File object
			File bundleFile;
			bundleFile = convertBundleFilePathToFile(bundle, filePathInBundle);

			// check if this bundle entry represents a file or folder, and
			// handle accordingly:
			if (bundleFile.isDirectory()) {
				// This entry represents a folder:
				// create this folder in the destination, and continue to the
				// next entry element
				(new File(destinationDirPath + filePathInTarget)).mkdir();
			} else {
				// handle as static/resource file (copy file as it is)
				try {
					String fileTargetFullPath = destinationDirPath
							+ filePathInTarget;
					copyBundleFile(bundle, filePathInBundle, fileTargetFullPath);
				} catch (IOException e) {
					throw e;
				}
			}
		}
	}

	/**
	 * Copy the content of a bundle directory, into a destination directory
	 * 
	 * @param bundle
	 *            The bundle object in it the directory is located.
	 * @param relativeDirPathInBundle
	 *            Directory path relative to bundle.
	 * @param destinationDirPath
	 *            A path to the destination directory where the content of the
	 *            bundle directory will be copied into. Note that this folder
	 *            must be already exist.
	 * @throws IOException
	 *             , URISyntaxException
	 */
	public static void copyRuntimeLibFromBundle(Bundle bundle,
			String relativeDirPathInBundle, String destinationDirPath)
			throws IOException, URISyntaxException {

		destinationDirPath = normalizeFolderPath(destinationDirPath);
		relativeDirPathInBundle = normalizeFolderPath(relativeDirPathInBundle);

		Enumeration<URL> filesEnum = bundle.findEntries(new Path(
				relativeDirPathInBundle).toString(), "*", true); //$NON-NLS-1$

		// enumerate all entries in bundle folder, recursively (also in
		// subfolders)
		while (filesEnum.hasMoreElements()) {
			// get relative path of file in the bundle
			String filePathInBundle = ((URL) filesEnum.nextElement()).getFile();

			if (filePathInBundle.contains("org.eclipse.ogee.rt.android.framework")) //$NON-NLS-1$
			{
				// remove the folder path (in bundle) from file relative path,
				// and create the target path
				int dirPathIndex = filePathInBundle
						.indexOf(relativeDirPathInBundle);
				String filePathInTarget = filePathInBundle
						.substring(dirPathIndex
								+ relativeDirPathInBundle.length());

				// get the bundle file as File object
				File bundleFile;
				bundleFile = convertBundleFilePathToFile(bundle,
						filePathInBundle);

				// check if this bundle entry represents a file or folder, and
				// handle accordingly:
				if (bundleFile.isDirectory()) {
					// This entry represents a folder:
					// create this folder in the destination, and continue to
					// the next entry element
					(new File(destinationDirPath + filePathInTarget)).mkdir();
				} else {
					// handle as static/resource file (copy file as it is)
					try {
						String fileTargetFullPath = destinationDirPath
								+ filePathInTarget;
						copyBundleFile(bundle, filePathInBundle,
								fileTargetFullPath);
					} catch (IOException e) {
						throw e;
					}
				}
				return;
			}
		}
	}

	/**
	 * Method that converts a file path relative to the bundle, to a File
	 * object.
	 * 
	 * @param filePathAtBundle
	 *            The file path relative to bundle
	 * @param bundle
	 *            The bundle object in it the file is located.
	 * @return File The file object represents the given file.
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public static File convertBundleFilePathToFile(Bundle bundle,
			String filePathAtBundle) throws IOException, URISyntaxException {
		URL bundleEntryUrl = bundle.getEntry(filePathAtBundle);
		URL absoluteUrl = FileLocator.toFileURL(bundleEntryUrl);
		String encodedUrl = absoluteUrl.toString().replace(" ", "%20"); //$NON-NLS-1$ //$NON-NLS-2$
		URI uri = new URI(encodedUrl);
		return new File(uri);
	}

	/**
	 * Method that copies a file from a bundle to a specified destination.
	 * 
	 * @param bundle
	 *            The bundle object in it the file is located.
	 * @param filePathAtBundle
	 *            The source file path relative to bundle.
	 * @param fileTargetFullPath
	 *            The output file absolute path.
	 * @return File The file object represents the copied output file.
	 * @throws IOException
	 */
	public static File copyBundleFile(Bundle bundle, String filePathAtBundle,
			String fileTargetFullPath) throws IOException {
		InputStream inputStream;
		try {
			IPath pathInBundle = new Path(filePathAtBundle);
			inputStream = FileLocator.openStream(bundle, pathInBundle, false);
		} catch (IOException e) {
			Logger.getUtilsLogger().logError(
					NLS.bind(FrameworkUtilsMessages.FileUtils_12,
							filePathAtBundle), e);
			throw e;
		}
		return copyFile(fileTargetFullPath, inputStream);
	}

	/**
	 * Method that copies a file from an input stream to a specified destination
	 * 
	 * @param fileTargetFullPath
	 *            The output file absolute path.
	 * @param in
	 *            The input stream with the source file content.
	 * @return File The file object represents the copied output file.
	 * @throws IOException
	 */
	public static File copyFile(String fileTargetFullPath, InputStream in)
			throws IOException {
		File file = null;
		OutputStream out = null;
		try {
			file = new File(fileTargetFullPath);
			out = new FileOutputStream(file);
			byte buf[] = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.flush();
		} catch (FileNotFoundException e) {
			Logger.getUtilsLogger().logError(
					NLS.bind(FrameworkUtilsMessages.FileUtils_14,
							fileTargetFullPath), e);
			throw e;
		} catch (IOException e) {
			Logger.getUtilsLogger().logError(
					NLS.bind(FrameworkUtilsMessages.FileUtils_15,
							fileTargetFullPath), e);
			throw e;
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					Logger.getUtilsLogger().logWarning(
							NLS.bind(FrameworkUtilsMessages.FileUtils_16,
									fileTargetFullPath), e);
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					Logger.getUtilsLogger().logWarning(
							FrameworkUtilsMessages.FileUtils_18, e);
				}
			}
		}
		return file;
	}

	/**
	 * Method that copies a file located in a specified path to a specified
	 * destination
	 * 
	 * @param fileSourceFullPath
	 *            The source file absolute path.
	 * @param fileTargetFullPath
	 *            The output file absolute path.
	 * @return File The file object represents the copied output file.
	 * @throws IOException
	 */
	public static File copyFile(String fileSourceFullPath,
			String fileTargetFullPath) throws IOException {
		InputStream in;
		try {
			in = new FileInputStream(fileSourceFullPath);
		} catch (FileNotFoundException e) {
			Logger.getUtilsLogger().logError(
					NLS.bind(FrameworkUtilsMessages.FileUtils_19,
							fileSourceFullPath), e);
			throw e;
		}
		return copyFile(fileTargetFullPath, in);
	}

	/**
	 * Normalize the input folder path to use '/' separators (which valid on any
	 * OS) and adds '/' at the end of the path if it is missing.
	 * 
	 * @param inputFolderPath
	 *            The input folder path to normalize
	 * @return The normalized path
	 */
	public static String normalizeFolderPath(String inputFolderPath) {
		inputFolderPath = inputFolderPath.replace(File.separatorChar, '/'); // Work
																			// with
																			// '/'
																			// separators
																			// (valid
																			// for
																			// any
																			// OS)
		if (!inputFolderPath.endsWith("/")) { //$NON-NLS-1$
			inputFolderPath = inputFolderPath + "/"; //$NON-NLS-1$
		}
		return inputFolderPath;
	}

	/**
	 * Rename a file
	 * 
	 * @param oldFilePath
	 * @param newFilePath
	 */
	public static void renameFile(String oldFilePath, String newFilePath) {
		File oldFile = new File(oldFilePath);
		File newFile = new File(newFilePath);
		oldFile.renameTo(newFile);
	}
}
