/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.ogee.utils.activator.Activator;
import org.eclipse.swt.widgets.Shell;

public class DialogUtils {
	private static final String LINE_SEPARATOR = "line.separator"; //$NON-NLS-1$

	/**
	 * Shows JFace ErrorDialog with full stack trace in the details area.
	 */
	public static void errorDialogWithStackTrace(Shell parent, String msg,
			Throwable e) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);

		final String stackTrace = stringWriter.toString(); // stack trace as a
															// string

		// Temp holder of child statuses
		List<Status> childStatuses = new ArrayList<Status>();

		// Split output by OS-independent new-line
		for (String line : stackTrace.split(System.getProperty(LINE_SEPARATOR))) {
			// build and add status
			childStatuses.add(new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					line));
		}

		MultiStatus multiStatus = new MultiStatus(Activator.PLUGIN_ID,
				IStatus.ERROR, childStatuses.toArray(new Status[] {}), // convert to array of statuses																		
				e.getCause().getMessage(), e);

		ErrorDialog.openError(parent, "", msg, multiStatus); //$NON-NLS-1$
	}
}
