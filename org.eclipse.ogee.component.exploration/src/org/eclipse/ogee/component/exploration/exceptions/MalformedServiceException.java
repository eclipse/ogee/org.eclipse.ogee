/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration.exceptions;

import java.net.MalformedURLException;

/**
 * Thrown to indicate that a malformed OData service connection has occurred.
 * 
 */
public class MalformedServiceException extends MalformedURLException {
	private static final long serialVersionUID = -3785695950950660743L;

	/**
	 * Constructs a <code>MalformedServiceException</code> with no detail
	 * message.
	 */
	public MalformedServiceException() {
	}

	/**
	 * Constructs a <code>MalformedServiceException</code> with the specified
	 * detail message.
	 * 
	 * @param msg
	 *            the detail message.
	 */
	public MalformedServiceException(String msg) {
		super(msg);
	}

	public MalformedServiceException(String msg, Throwable t) {
		super(msg);
		setStackTrace(t.getStackTrace());
	}
}
