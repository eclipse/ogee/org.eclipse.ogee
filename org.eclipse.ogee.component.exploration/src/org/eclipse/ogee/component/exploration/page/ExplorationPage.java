/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration.page;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.IPageChangeProvider;
import org.eclipse.jface.dialogs.IPageChangedListener;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.PageChangedEvent;
import org.eclipse.jface.dialogs.StatusDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ogee.component.exploration.activator.Activator;
import org.eclipse.ogee.component.exploration.controller.ExplorationController;
import org.eclipse.ogee.component.exploration.nls.messages.ExplorationMessages;
import org.eclipse.ogee.component.exploration.service.catalog.provider.IServiceCatalogProvider;
import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.exploration.tree.nodes.ServiceNode;
import org.eclipse.ogee.exploration.tree.viewer.ServiceTreeViewer;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.FileUtils;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.service.validation.Result;
import org.eclipse.ogee.utils.service.validation.Result.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

/**
 * Represents an Exploration page.
 */
public class ExplorationPage extends WizardPage implements IPageChangedListener {
	private static final String EMPTY = ""; //$NON-NLS-1$
	private static final String NEW_PROJECT_IMAGE_PATH = "res/images/WIZARD_BANNER_ICON.gif"; //$NON-NLS-1$

	// These filter names are displayed to the user in the file dialog.
	// Note that the inclusion of the actual extension in parentheses is
	// optional, and doesn't have any effect on which files are displayed.
	private static final String[] FILTER_NAMES = { "All Files (*.*)" }; //$NON-NLS-1$

	// These filter extensions are used to filter which files are displayed.
	private static final String[] FILTER_EXTS = { "*.*" }; //$NON-NLS-1$

	// url group controls
	private Composite urlGroup;
	private Button urlGroupRadiobButton;
	private Label serviceUrlLable;
	private Text serviceUrlText;

	// files group controls
	private Composite filesGroup;
	private Button filesGroupRadiobButton;
	private Label metadataFileLabel;
	private Text serviceMetadataUriText;
	private Button browseMetadataButton;
	private Label serviceDocumentFileLabel;
	private Text serviceDocumentUriText;
	private Button browseServiceDocumentButton;

	// validate button for both groups
	// it is public for test perposes only
	public Button goButton;

	// exploration controller
	private ExplorationController controller;
	// validation result
	private Result result;
	// next page was displayed
	private boolean nextPageWasDisplayed;
	// service tree view part
	private ServiceTreeViewer serviceTreeViewer;
	private Button catalogButton;

	private abstract class RunnableWithData implements Runnable {
		protected String data;

		public String getData() {
			return data;
		}

	}

	/**
	 * Constructs a new ExplorationPage.
	 */
	public ExplorationPage() {
		super("ExplorationComponentPage"); //$NON-NLS-1$

		URL imageUrl = Activator.getDefault().getBundle()
				.getEntry(NEW_PROJECT_IMAGE_PATH);
		ImageDescriptor imageDescriptor = ImageDescriptor
				.createFromURL(imageUrl);

		setDescription(ExplorationMessages.Custom_Description);

		setImageDescriptor(imageDescriptor);

		setTitle(ExplorationMessages.GatewayExplorationPage_ExplorationPageTitle);

		this.nextPageWasDisplayed = false;

		setPageComplete(false);
	}

	private void updateServiceInputFields(Result result) {
		if (this.serviceUrlText != null
				&& this.urlGroupRadiobButton.getSelection()
				&& result.isServiceUrlValidation()) {
			this.serviceUrlText.setEditable(false);
			setText(this.serviceUrlText, result.getServiceUrl());

			this.urlGroupRadiobButton.setSelection(true);
			this.filesGroupRadiobButton.setSelection(false);

			enableServiceUrlGroup(true);
			this.serviceUrlText.setEditable(true);
		} else if (this.serviceMetadataUriText != null
				&& this.serviceDocumentUriText != null
				&& this.filesGroupRadiobButton.getSelection()) {
			this.serviceMetadataUriText.setEditable(false);
			this.serviceDocumentUriText.setEditable(false);
			setText(this.serviceMetadataUriText, result.getServiceMetadataUri());
			setText(this.serviceDocumentUriText, result.getServiceDocumentUri());

			this.urlGroupRadiobButton.setSelection(false);
			this.filesGroupRadiobButton.setSelection(true);

			enableServiceUrlGroup(false);
			this.serviceMetadataUriText.setEditable(true);
			this.serviceDocumentUriText.setEditable(true);
		}
	}

	public boolean wasNextPagedDisplayed() {
		return this.nextPageWasDisplayed;
	}

	/**
	 * Sets the page's controller.
	 */
	public void setController(ExplorationController controller) {
		this.controller = controller;
	}

	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			setDescription(ExplorationMessages.Custom_Description);

			// set the matching help context to the page when it's visible
			PlatformUI
					.getWorkbench()
					.getHelpSystem()
					.setHelp(
							getShell(),
							"org.eclipse.ogee.component.exploration.32ccbf5b943449b6bcde672f001aab96LocateODataservice"); //$NON-NLS-1$
		}

		super.setVisible(visible);
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout gridLayout = new GridLayout(1, false);
		container.setLayout(gridLayout);
		GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1);
		gridData.grabExcessHorizontalSpace = true;
		container.setLayoutData(gridData);

		// add page changed listener
		IWizardContainer wContainer = getContainer();
		if (wContainer instanceof IPageChangeProvider) {
			((IPageChangeProvider) wContainer).addPageChangedListener(this);
		}

		// create url group radio button
		createUrlGroupRadioButton(container);
		// create url group
		createUrlGroup(container);
		// create files group radio button
		createFilesGroupRadioButton(container);
		// create files group
		createFilesGroup(container);
		// set url group enabled
		enableServiceUrlGroup(true);

		// set service details label
		Label serviceDetails = new Label(container, SWT.NONE);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		serviceDetails.setLayoutData(gridData);
		serviceDetails.setText(ExplorationMessages.ExplorationPage_21);

		// add service view part
		this.serviceTreeViewer = new ServiceTreeViewer(container);

		int desiredHeight = this.serviceTreeViewer.getTree().getItemHeight()
				* 8 + this.serviceTreeViewer.getTree().getHeaderHeight();
		gridData = new GridData(400, desiredHeight);
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;

		this.serviceTreeViewer.setLayoutData(gridData);

		Result previousResult = this.controller.getPreviousResult();
		handleServiceValidationResult(previousResult);

		setControl(container);
	}

	/**
	 * 
	 * @param parent
	 */
	private void createUrlGroupRadioButton(Composite parent) {
		// create url group radio button
		this.urlGroupRadiobButton = new Button(parent, SWT.RADIO);
		GridData gd_urlGroupRadiobButton = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_urlGroupRadiobButton.verticalIndent = 10;
		urlGroupRadiobButton.setLayoutData(gd_urlGroupRadiobButton);
		this.urlGroupRadiobButton.setSelection(true);
		this.urlGroupRadiobButton
				.setText(ExplorationMessages.ExplorationPage_2);
		this.urlGroupRadiobButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				enableServiceUrlGroup(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// do nothing
			}
		});
	}

	/**
	 * updates service exploration view part
	 * 
	 * @param result
	 * @throws BuilderException
	 */
	private void updateServiceExploration(Result result, EDMXSet odataEdmxSet) {
		if (this.serviceTreeViewer == null) {
			return;
		}

		if ((result.getStatus() == Status.OK || result.getStatus() == Status.CANNOT_BE_CONSUMED)
				&& odataEdmxSet != null) {
			ITreeNode serviceNode = new ServiceNode(result.getServiceName(),
					odataEdmxSet);

			this.serviceTreeViewer.setService(serviceNode);
		} else // ERROR || CUSTOM_ERROR
		{
			this.serviceTreeViewer.clear();
		}
	}

	private void enableServiceUrlGroup(boolean enabled) {
		enableComposite(this.urlGroup, enabled);

		enableComposite(this.filesGroup, !enabled);

		if (enabled) {
			handleServiceUrlModification(getServiceUrl());
		} else {
			if (!isPageComplete()) {
				this.serviceMetadataUriText.notifyListeners(SWT.Modify,
						new Event());
			}
		}
	}

	private void enableComposite(Composite composite, boolean enabled) {
		if (composite == null) {
			return;
		}

		composite.setEnabled(enabled);
		Control[] children = composite.getChildren();
		for (Control child : children) {
			child.setEnabled(enabled);
		}
	}

	/**
	 * 
	 * @param parent
	 */
	private void createFilesGroupRadioButton(Composite parent) {
		// create files group radio button
		this.filesGroupRadiobButton = new Button(parent, SWT.RADIO);
		GridData gd_filesGroupRadiobButton = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_filesGroupRadiobButton.verticalIndent = 20;
		filesGroupRadiobButton.setLayoutData(gd_filesGroupRadiobButton);
		this.filesGroupRadiobButton
				.setText(ExplorationMessages.ExplorationPage_3);
		this.filesGroupRadiobButton
				.addSelectionListener(new SelectionListener() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						enableServiceUrlGroup(false);
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent arg0) {
						// do nothing
					}
				});
	}

	/**
	 * Creates url group
	 * 
	 * @param parent
	 */
	private void createUrlGroup(Composite parent) {
		// create url group
		this.urlGroup = new Composite(parent, SWT.NONE);
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 3,
				1);
		gridData.horizontalIndent = 20;
		gridData.grabExcessHorizontalSpace = true;
		this.urlGroup.setLayoutData(gridData);
		GridLayout gl_urlGroup = new GridLayout(3, false);
		gl_urlGroup.marginWidth = 0;
		this.urlGroup.setLayout(gl_urlGroup);
		// create service url label
		this.serviceUrlLable = new Label(this.urlGroup, SWT.NONE);
		this.serviceUrlLable.setText(ExplorationMessages.Custom_ServiceUrl);
		// set service url text listener
		this.serviceUrlText = new Text(this.urlGroup, SWT.BORDER);
		this.serviceUrlText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));
		this.serviceUrlText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				if (serviceUrlText.getEditable()) {
					String serviceUrl = getServiceUrl();

					handleServiceUrlModification(serviceUrl);
				}
			}
		});

		// create validate button
		createGoButton(this.urlGroup);

		new Label(urlGroup, SWT.NONE);
		new Label(urlGroup, SWT.NONE);
		catalogButton = new Button(this.urlGroup, SWT.NONE);
		handleCatalogButton();

		GridData gd_button = new GridData(SWT.FILL, SWT.FILL, false, false, 1,
				1);
		catalogButton.setLayoutData(gd_button);
		catalogButton.setText(this.controller.getCatalogButtonNameString());
	}

	/**
	 * Handles the behaviour of the Catalog button.
	 * 
	 * @param catalogButton
	 */
	private void handleCatalogButton() {
		final IServiceCatalogProvider serviceCatalogProvider = this.controller
				.getServiceCatalogProvider();

		if (serviceCatalogProvider == null) {
			catalogButton.setVisible(false);
			return;
		}

		catalogButton.setVisible(true);
		catalogButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				StatusDialog dialog = ((StatusDialog) serviceCatalogProvider);
				dialog.setBlockOnOpen(true);
				dialog.open();

				Result result = serviceCatalogProvider
						.getSelectedServiceValidationResult();

				if (dialog.getReturnCode() == StatusDialog.OK) {
					if (result.getStatus() == Result.Status.OK) {
						// create model and publish
						result = controller.createModelAndPublish(result);
					}
					handleServiceValidationResult(result);
				}
			}
		});

	}

	/**
	 * 
	 * @param serviceUrl
	 */
	private void handleServiceUrlModification(String serviceUrl) {
		setPageComplete(false);

		if (serviceUrl == null || serviceUrl.isEmpty()) {
			setValidateButtonEnabled(false);
			setMessage(ExplorationMessages.Custom_Description, INFORMATION);
			return;
		}

		// clears service tree view part
		this.serviceTreeViewer.clear();

		boolean validServiceUrl = this.controller
				.isServiceUrlSyntaxValid(serviceUrl);
		if (validServiceUrl) {
			setValidateButtonEnabled(true);
			setMessage(
					ExplorationMessages.ExplorationPage_PressTheValidateButton,
					INFORMATION);
		} else {
			setMessage(ExplorationMessages.ExplorationPage_5, ERROR);
			setValidateButtonEnabled(false);
		}
	}

	/**
	 * Creates files group
	 * 
	 * @param parent
	 */
	private void createFilesGroup(Composite parent) {
		// create files group
		this.filesGroup = new Composite(parent, SWT.NONE);
		GridLayout gl_filesGroup = new GridLayout(3, false);
		gl_filesGroup.marginWidth = 0;
		this.filesGroup.setLayout(gl_filesGroup);
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 3,
				1);
		gridData.horizontalIndent = 20;
		gridData.grabExcessHorizontalSpace = true;
		this.filesGroup.setLayoutData(gridData);
		// create metadata file label
		this.metadataFileLabel = new Label(this.filesGroup, SWT.NONE);
		this.metadataFileLabel
				.setText(ExplorationMessages.Custom_ServiceMetadataFile);
		// create metadata file path text
		this.serviceMetadataUriText = new Text(this.filesGroup, SWT.BORDER);
		this.serviceMetadataUriText.setLayoutData(new GridData(SWT.FILL,
				SWT.CENTER, true, false, 1, 1));
		this.serviceMetadataUriText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				if (!serviceMetadataUriText.getEditable()) {
					return;
				}

				String metadataUri = getMetadataUri();
				String serviceDocumentUri = getServiceDocumentUri();

				boolean status = handleServiceFilesModification(metadataUri,
						serviceDocumentUri);
				if (status) {
					syncExec(
							new RunnableWithMonitor() {
								public void run() {
									// get metadata uri
									String metadataUri = getMetadataUri();
									// get service document uri
									String serviceDocumentUri = getServiceDocumentUri();
									// validate service url
									this.monitor
											.beginTask(
													ExplorationMessages.ExplorationController_1,
													7);
									result = controller
											.validateServiceAndPublish(
													metadataUri,
													serviceDocumentUri,
													this.monitor);
								}
							}, ExplorationMessages.ExplorationPage_1,
							ExplorationMessages.ExplorationPage_20);

					handleServiceValidationResult(result);
				}
			}
		});
		// create metadata file browse button
		this.browseMetadataButton = new Button(this.filesGroup, SWT.NONE);
		this.browseMetadataButton.setLayoutData(new GridData(SWT.RIGHT,
				SWT.CENTER, false, false, 1, 1));
		this.browseMetadataButton
				.setText(ExplorationMessages.ExplorationPage_11);
		this.browseMetadataButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);

				dialog.setFilterNames(FILTER_NAMES);
				dialog.setFilterExtensions(FILTER_EXTS);

				String path = dialog.open();
				if (path != null) {
					File file = new File(path);
					if (file.isFile()) {
						serviceMetadataUriText.setText(path);
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// do nothing
			}
		});
		// create service document file label
		this.serviceDocumentFileLabel = new Label(this.filesGroup, SWT.NONE);
		this.serviceDocumentFileLabel
				.setText(ExplorationMessages.Custom_ServiceDocumentFile);
		// create service document path text
		this.serviceDocumentUriText = new Text(this.filesGroup, SWT.BORDER);
		this.serviceDocumentUriText.setLayoutData(new GridData(SWT.FILL,
				SWT.CENTER, true, false, 1, 1));
		this.serviceDocumentUriText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				if (!serviceDocumentUriText.getEditable()) {
					return;
				}

				String metadataUri = getMetadataUri();
				String serviceDocumentUri = getServiceDocumentUri();

				boolean status = handleServiceFilesModification(metadataUri,
						serviceDocumentUri);
				if (status) {
					syncExec(
							new RunnableWithMonitor() {
								public void run() {
									this.monitor
											.beginTask(
													ExplorationMessages.ExplorationController_1,
													7);
									// get metadata uri
									this.monitor.internalWorked(1);
									String metadataUri = getMetadataUri();
									this.monitor.internalWorked(1);
									// get service document uri
									String serviceDocumentUri = getServiceDocumentUri();
									this.monitor.internalWorked(1);
									// validate service url
									result = controller
											.validateServiceAndPublish(
													metadataUri,
													serviceDocumentUri,
													this.monitor);
								}
							}, ExplorationMessages.ExplorationPage_1,
							ExplorationMessages.ExplorationPage_20);

					handleServiceValidationResult(result);
				}
			}
		});
		// create service document file browse button
		this.browseServiceDocumentButton = new Button(this.filesGroup, SWT.NONE);
		this.browseServiceDocumentButton.setLayoutData(new GridData(SWT.RIGHT,
				SWT.CENTER, false, false, 1, 1));
		this.browseServiceDocumentButton
				.setText(ExplorationMessages.ExplorationPage_11);
		this.browseServiceDocumentButton
				.addSelectionListener(new SelectionListener() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);

						dialog.setFilterNames(FILTER_NAMES);
						dialog.setFilterExtensions(FILTER_EXTS);

						String path = dialog.open();
						if (path != null) {
							File file = new File(path);
							if (file.isFile()) {
								serviceDocumentUriText.setText(path);
							}
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent arg0) {
						// do nothing
					}
				});
	}

	/**
	 * 
	 * @param metadataUri
	 * @param serviceDocumentUri
	 */
	private boolean handleServiceFilesModification(String metadataUri,
			String serviceDocumentUri) {
		setPageComplete(false);

		if ((metadataUri == null || metadataUri.isEmpty())
				&& (serviceDocumentUri == null || serviceDocumentUri.isEmpty())) {
			setValidateButtonEnabled(false);
			setMessage(ExplorationMessages.Custom_Description, INFORMATION);
			return false;
		}

		if (metadataUri == null || metadataUri.isEmpty()) {
			setValidateButtonEnabled(false);
			setMessage(ExplorationMessages.ExplorationPage_7, ERROR);
			return false;
		}

		if (serviceDocumentUri == null || serviceDocumentUri.isEmpty()) {
			setValidateButtonEnabled(false);
			setMessage(ExplorationMessages.ExplorationPage_18, ERROR);
			return false;
		}

		// clears service tree view part
		this.serviceTreeViewer.clear();

		boolean validMetadataUri = FileUtils.isFileUriValid(metadataUri);
		if (validMetadataUri) {
			boolean validServiceDocument = FileUtils
					.isFileUriValid(serviceDocumentUri);
			if (validServiceDocument) {
				setValidateButtonEnabled(true);
				setMessage(
						ExplorationMessages.ExplorationPage_PressTheValidateButton,
						INFORMATION);
			} else {
				setValidateButtonEnabled(false);
				setMessage(ExplorationMessages.ExplorationPage_6, ERROR);
				return false;
			}
		} else {
			this.goButton.setEnabled(false);
			setMessage(ExplorationMessages.ExplorationPage_10, ERROR);
			return false;
		}

		return true;
	}

	private void createGoButton(Composite parent) {
		// create validate button
		this.goButton = new Button(parent, SWT.NONE);
		GridData gridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false, true, 1,
				1);
		gridData.widthHint = 80;
		this.goButton.setLayoutData(gridData);
		this.goButton.setEnabled(false);
		this.goButton.setText(ExplorationMessages.ExplorationPage_Test);
		// set validate button listener
		this.goButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				handleGoButtonWithProgress();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// do nothing
			}
		});
	}

	private void handleGoButtonWithProgress() {
		try {
			getContainer().run(true, true, new IRunnableWithProgress() {
				@Override
				public void run(final IProgressMonitor monitor)
						throws InvocationTargetException, InterruptedException {
					monitor.beginTask(ExplorationMessages.ExplorationPage_15,
							14);
					RunnableWithData runnable = new RunnableWithData() {
						@Override
						public void run() {
							// get page input url
							data = getServiceUrl();
							if (monitor.isCanceled()) {
								return;
							}
						}
					};
					Display.getDefault().syncExec(runnable);
					monitor.internalWorked(1);
					// validate service url
					result = controller.validateServiceAndPublish(
							runnable.getData(), monitor);
					monitor.internalWorked(1);
					if (monitor.isCanceled()) {
						return;
					}
				}
			});
		} catch (InvocationTargetException e) {
			Logger.getUtilsLogger().logError(
					ExplorationMessages.ExplorationPage_15, e);
		} catch (InterruptedException e) {
			Logger.getUtilsLogger().logError(
					ExplorationMessages.ExplorationPage_15, e);
		}

		handleServiceValidationResult(result);
	}

	/**
	 * 
	 * @param result
	 */
	public void handleServiceValidationResult(Result result) {
		if (result == null) {
			return;
		}

		try {
			Status status = result.getStatus();
			if (status == Status.OK) {
				EDMXSet odataEdmxSet = this.controller.buildEdmxSet(result);

				// Diagnostic diagnostic =
				// this.controller.validateEdmxSet(odataEdmxSet);
				// if (diagnostic.getSeverity() == Diagnostic.ERROR)
				// {
				// updateWithWarningStatus(result, odataEdmxSet, diagnostic);
				// }
				// else // Status.OK
				// {
				updateWithOKStatus(result, odataEdmxSet);
				// }
			} else if (status == Status.ERROR
					|| status == Status.CANNOT_BE_CONSUMED
					|| status == Status.CUSTOM_ERROR) {
				updateWithErrorStatus(result);
			}
		} catch (BuilderException e) {
			Logger.getLogger(ExplorationPage.class).logError(
					ExplorationMessages.CouldNotCreateEdmxSet, e);
			this.serviceTreeViewer.clear();
		}
	}

	private void updateWithOKStatus(Result result, EDMXSet odataEdmxSet) {
		updateServiceInputFields(result);
		setValidateButtonEnabled(false);
		setMessage(result.getMessage(), IMessageProvider.INFORMATION);
		setPageComplete(true);

		updateServiceExploration(result, odataEdmxSet);
	}

	private void updateWithErrorStatus(Result result) {
		updateServiceInputFields(result);

		setPageComplete(false);

		setValidateButtonEnabled(true);

		setMessage(result.getMessage(), IMessageProvider.ERROR);

		Throwable exception = result.getException();
		if (exception != null) {
			Logger.getUtilsLogger().logError(result.getMessage(), exception);
		}

		updateServiceExploration(result, null);
	}

	// private void updateWithWarningStatus(Result result, EDMXSet odataEdmxSet,
	// Diagnostic diagnostic)
	// {
	// updateServiceInputFields(result);
	//
	// setPageComplete(true);
	//
	// setValidateButtonEnabled(false);
	//
	// setMessage(ExplorationMessages.ExplorationPage_0,
	// IMessageProvider.WARNING);
	// Logger.getUtilsLogger().logWarning(diagnostic.getChildren().toString());
	//
	// Throwable exception = diagnostic.getException();
	// if (exception != null)
	// {
	// Logger.getUtilsLogger().logError(diagnostic.getMessage(), exception);
	// }
	//
	// updateServiceExploration(result, odataEdmxSet);
	// }

	public void setValidateButtonEnabled(boolean status) {
		if (this.goButton == null) {
			return;
		}

		this.goButton.setEnabled(status);
	}

	/**
	 * 
	 * @return
	 */
	private String getServiceUrl() {
		if (this.serviceUrlText == null) {
			return EMPTY;
		}

		return this.serviceUrlText.getText().trim();
	}

	/**
	 * Sets text control new value.
	 * 
	 * @param value
	 *            - new text value
	 */
	private void setText(Text text, String value) {
		if (text == null) {
			return;
		}

		if (value == null || value.isEmpty()) {
			return;
		}

		if (value.equals(text.getText())) {
			return;
		}

		text.setText(value.trim());
	}

	/**
	 * @return - ServiceDocumentUri
	 */
	private String getServiceDocumentUri() {
		if (this.serviceDocumentUriText == null) {
			return EMPTY;
		}

		return this.serviceDocumentUriText.getText().trim();
	}

	/**
	 * @return - MetadataUri
	 */
	private String getMetadataUri() {
		if (this.serviceMetadataUriText == null) {
			return EMPTY;
		}

		return this.serviceMetadataUriText.getText().trim();
	}

	@Override
	public void pageChanged(PageChangedEvent event) {
		IWizardPage selectedPage = (IWizardPage) event.getSelectedPage();
		if (selectedPage == this) {
			return;
		}

		IWizardPage nextPage = getWizard().getNextPage(this);
		if (nextPage == selectedPage) { // next button pressed
			this.nextPageWasDisplayed = true;
		}
	}

	/**
	 * Runnable with monitor
	 */
	private abstract class RunnableWithMonitor implements Runnable {
		protected IProgressMonitor monitor;

		public void setMonitor(IProgressMonitor monitor) {
			this.monitor = monitor;
		}
	}

	/**
	 * @param runnable
	 */
	private void syncExec(final RunnableWithMonitor runnable, String taskName,
			String errorMessage) {
		try {
			getWizard().getContainer().run(true, true,
					new IRunnableWithProgress() {
						public void run(final IProgressMonitor monitor)
								throws InvocationTargetException,
								InterruptedException {
							try {
								runnable.setMonitor(monitor);
								monitor.beginTask("Task1", 10); //$NON-NLS-1$
								Display.getDefault().syncExec(runnable);
							} finally {
								monitor.done();
							}
						}
					});
		} catch (InvocationTargetException ex) {
			Logger.getUtilsLogger().logError(taskName, ex);
			MessageDialog.openError(getShell(), errorMessage, ex.getMessage());
		} catch (InterruptedException ex) {
			Logger.getUtilsLogger().logError(taskName, ex);
			MessageDialog.openError(getShell(), errorMessage, ex.getMessage());
		}
	}

	@Override
	public void performHelp() {
		getShell()
				.setData(
						"org.eclipse.ui.help", "org.eclipse.ogee.component.exploration.32ccbf5b943449b6bcde672f001aab96LocateODataservice"); //$NON-NLS-1$ //$NON-NLS-2$
	}
}
