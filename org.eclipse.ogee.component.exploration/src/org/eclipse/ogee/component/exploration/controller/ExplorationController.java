/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ogee.client.connectivity.api.IServerConnectionParameters;
import org.eclipse.ogee.client.exceptions.ParserException;
import org.eclipse.ogee.client.exceptions.RestClientException;
import org.eclipse.ogee.client.model.atom.AppCollection;
import org.eclipse.ogee.client.model.atom.AppService;
import org.eclipse.ogee.client.model.atom.AppWorkspace;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.model.generic.ODataEntry;
import org.eclipse.ogee.component.exploration.ExplorationComponent;
import org.eclipse.ogee.component.exploration.model.ExplorationModel;
import org.eclipse.ogee.component.exploration.nls.messages.ExplorationMessages;
import org.eclipse.ogee.component.exploration.page.ExplorationPage;
import org.eclipse.ogee.component.exploration.service.catalog.provider.CatalogExtentionManager;
import org.eclipse.ogee.component.exploration.service.catalog.provider.IServiceCatalogProvider;
import org.eclipse.ogee.component.exploration.service.catalog.provider.IValidationProvider;
import org.eclipse.ogee.component.exploration.service.model.extensions.ServiceModelConstants;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.builders.ODataModelEDMXSetBuilder;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IValidator;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.StringParsingUtils;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.service.validation.GenericServiceUrlValidator;
import org.eclipse.ogee.utils.service.validation.IServiceUrlValidator;
import org.eclipse.ogee.utils.service.validation.Result;
import org.eclipse.ogee.utils.service.validation.Result.Status;
import org.eclipse.ogee.utils.service.validation.ServiceValidator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * The controller of the Exploration Component.
 */
public class ExplorationController implements IValidationProvider {
	private static final String EMPTY = ""; //$NON-NLS-1$
	private static final String $METADATA = "$metadata"; //$NON-NLS-1$

	private ExplorationPage page;
	private Result previousResult;
	private ExplorationComponent component;

	private IServiceCatalogProvider serviceCatalogProvider;
	private boolean changedNotificationDecision;

	/**
	 * Constructs a new ExplorationComponentController.
	 * 
	 * @param page
	 *            - the page
	 * @param model
	 *            - the model
	 * @param component
	 *            - the component
	 */
	public ExplorationController(ExplorationPage page,
			ExplorationComponent component) {
		this.page = page;
		this.component = component;
		this.previousResult = null;
		this.page.setController(this);
		this.serviceCatalogProvider = CatalogExtentionManager
				.getServiceCatalogProvider(this);
	}

	/**
	 * @return - previous result.
	 */
	public Result getPreviousResult() {
		return this.previousResult;
	}

	/**
	 * @return - the wizard page of this exploration component.
	 */
	public ExplorationPage getPage() {
		return this.page;
	}

	/**
	 * Validate syntax of a service url
	 * 
	 * @param serviceUrl
	 * @return
	 * @throws MalformedURLException
	 */
	public boolean isServiceUrlSyntaxValid(String serviceUrl) {
		if (serviceUrl != null) {
			serviceUrl = serviceUrl.trim();
		}

		if (serviceUrl == null || serviceUrl.isEmpty()) {
			return false;
		}

		// test URL validity.
		try {
			validateURLSyntax(serviceUrl);

			return true;
		} catch (MalformedURLException e) {
			return false;
		}
	}

	/**
	 * Validates the service url and updates the progress monitor.
	 * 
	 * @param monitor
	 *            - progress monitor
	 * @param serviceUri
	 *            - the service's uri
	 * 
	 * @throws FileNotFoundException
	 * @throws ParserException
	 * @throws IOException
	 * @throws RestClientException
	 * 
	 * @return the service's metadata
	 */
	public Result validateServiceUrl(String serviceUrl, IProgressMonitor monitor) {
		IServiceUrlValidator serviceValidator = ServiceValidator
				.getServiceUrlValidator();

		Result result = serviceValidator.validate(serviceUrl, monitor);

		monitor.internalWorked(1);
		if (monitor.isCanceled()) {
			return null;
		}

		if (result.getStatus() == Result.Status.OK) {
			// add here the specific validation of a extending controller
			// classes
			result = this.validateServiceResult(result, monitor);
		}

		return result;
	}

	/**
	 * This method may be overridden by extending classes in order to perform
	 * additional validation of the service result object
	 * 
	 * @param result
	 * @param monitor
	 * @return
	 */
	public Result validateServiceResult(Result result, IProgressMonitor monitor) {
		return result;
	}

	public EDMXSet buildEdmxSet(Result result) throws BuilderException {
		String uri = result.getServiceUrl();
		if (uri == null || uri.isEmpty()) {
			uri = result.getServiceMetadataUri();
		}

		EDMXSet odataEdmxSet = ODataModelEDMXSetBuilder.build(
				result.getEdmx1(), uri);

		return odataEdmxSet;
	}

	public Diagnostic validateEdmxSet(EDMXSet edmxSet) {
		IValidator validator = IModelContext.INSTANCE.createValidator();
		Diagnostic diagnostic = validator.validate(edmxSet);

		return diagnostic;
	}

	/**
	 * Validates the service metadata and service document files and updates the
	 * progress monitor.
	 * 
	 * @param monitor
	 *            - progress monitor
	 * @param metadataUri
	 *            - the service's metadata uri
	 * @param serviceDocumentUri
	 *            - the service's service documentUri uri
	 * 
	 * @throws FileNotFoundException
	 * @throws ParserException
	 * @throws IOException
	 * @throws RestClientException
	 * 
	 * @return the service's metadata
	 */
	public Result validateServiceFiles(String serviceMetadataUri,
			String serviceDocumentUri, IProgressMonitor monitor) {
		Result result = new Result();
		result.setServiceUrlValidation(false);

		try {
			monitor.beginTask(ExplorationMessages.ExplorationController_1, 7);

			Result metadataFileResult = ServiceValidator
					.getServiceMetadataValidator().validate(serviceMetadataUri,
							monitor);
			if (metadataFileResult.getStatus() == Status.OK) {
				result.setEdmx(metadataFileResult.getEdmx1());
				result.setServiceMetadataUri(serviceMetadataUri);
				result.setServiceMetadataXml(metadataFileResult
						.getServiceMetadataXml());
				result.setServiceName(metadataFileResult.getServiceName());

				Result documentFileResult = ServiceValidator
						.getServiceDocumentValidator().validate(
								serviceDocumentUri, monitor);
				if (documentFileResult.getStatus() == Status.OK) {
					result.setServiceDocumentUri(serviceDocumentUri);
					result.setServiceDocumentXml(documentFileResult
							.getServiceDocumentXml());
					result.setAppService(documentFileResult.getAppService());
					result.setServiceUrl(documentFileResult.getServiceUrl());
					result.setBaseUrl(removeURLParameters(documentFileResult
							.getServiceUrl()));
					result = validateMetadataAndServiceDocument(result);
				} else {
					result.setStatus(documentFileResult.getStatus());
					result.setMessage(documentFileResult.getMessage());
				}
			} else {
				result.setStatus(metadataFileResult.getStatus());
				result.setMessage(metadataFileResult.getMessage());
			}

			if (result.getStatus() == Result.Status.OK) {
				// add here the specific validation of a extending controller
				// classes
				result = this.validateServiceResult(result, monitor);
			}
		} finally {
			monitor.done();
		}

		return result;
	}

	/**
	 * 
	 * @param edmx
	 * @param serviceDocument
	 * @return
	 */
	private Result validateMetadataAndServiceDocument(Result result) {
		Edmx edmx = result.getEdmx1();
		AppService appService = result.getAppService();

		AppWorkspace[] appWorkspaces = appService.getAppWorkspaces();
		for (AppWorkspace appWorkspace : appWorkspaces) {
			int foundCollections = 0;
			AppCollection[] collections = appWorkspace.getCollections();
			for (AppCollection appCollection : collections) {
				Schema[] schemas = edmx.getEdmxDataServices().getSchemas();
				for (Schema schema : schemas) {
					EntityContainer[] entityContainers = schema
							.getEntityContainers();
					for (EntityContainer entityContainer : entityContainers) {
						EntitySet[] entitySets = entityContainer
								.getEntitySets();
						for (EntitySet entitySet : entitySets) {
							String entitySetName = entitySet.getName();
							String href = appCollection.getHref();

							if (entitySetName == null || href == null) {
								continue;
							}

							if (entitySetName.trim().equals(href.trim())) {
								foundCollections++;
								break;
							}
						}
					}
				}
			}

			if (foundCollections > 0 && foundCollections == collections.length) {
				return result;
			}
		}

		Result error_result = new Result();
		error_result.setException(null);
		error_result.setStatus(Status.ERROR);
		error_result.setMessage(ExplorationMessages.ExplorationController_4);
		error_result.setServiceMetadataUri(result.getServiceMetadataUri());
		error_result.setServiceDocumentUri(result.getServiceDocumentUri());

		return error_result;
	}

	/**
	 * Removes the parameters from the given url.
	 * 
	 * @param inputUri
	 * @return - the url without the parameters.
	 */
	protected String removeURLParameters(String inputUri) {
		return StringParsingUtils.removeURLParameters(inputUri);
	}

	/**
	 * Parses the metadata XML and add it to the CodeGenerationModel.
	 * 
	 * @param serviceMetadata
	 *            - the metadata XML.
	 * @param serviceDocument
	 *            - the service document.
	 */
	private ExplorationModel createModel(Result result) {
		ExplorationModel model = new ExplorationModel();

		// save server connection details
		IServerConnectionParameters serverConnectionParams = result
				.getServerConnectionParameters();

		model.put(ServiceModelConstants.SERVER_CONNECTION_PARAMETERS,
				serverConnectionParams);
		// save edmx object
		Edmx edmx = result.getEdmx1();
		model.put(ServiceModelConstants.METADATA_OBJECT, edmx);
		// save edmx xml
		String serviceMetadataXml = result.getServiceMetadataXml();
		model.put(ServiceModelConstants.METADATA_XML, serviceMetadataXml);
		// save service document xml
		String serviceDocumentXml = result.getServiceDocumentXml();
		model.put(ServiceModelConstants.SERVICE_DOCUMENT, serviceDocumentXml);
		// save service name
		String serviceName = result.getServiceName();
		model.put(ServiceModelConstants.SERVICE_NAME, serviceName);

		// save service url
		// String removedURLParametersUrl =
		// removeURLParameters(result.getServiceUrl());

		// remove $metadata if exists
		String serviceUrl = result.getServiceUrl();
		if (serviceUrl.contains($METADATA)) {
			serviceUrl = serviceUrl.replace($METADATA, EMPTY);
		}

		// encode the & sign if exists
		serviceUrl = serviceUrl.replace("&", "%26"); //$NON-NLS-1$ //$NON-NLS-2$

		model.put(ServiceModelConstants.SERVICE_URL, serviceUrl);

		model.put(ServiceModelConstants.SERVICE_URL_BASE,
				StringParsingUtils.removeURLParameters(serviceUrl));
		// save service entry
		ODataEntry serviceEntry = result.getServiceEntry();
		model.put(ServiceModelConstants.SERVICE_ENTRY, serviceEntry);
		// save catalog service path
		model.put(ServiceModelConstants.CATALOG_SERVICE_PATH,
				result.getCatalogServicePath());
		// save server type
		model.put(ServiceModelConstants.SERVER_TYPE, result.getServerType());

		return model;
	}

	// TODO: rename method to remove "ForCatalog"
	/**
	 * 
	 * @param gatewayExplorationDialog
	 * @param serviceUrl
	 * @param validateButtonEnabled
	 */
	public Result validateServiceUrlForCatalog(String serviceUrl,
			String message, int messageType, IProgressMonitor monitor) {
		Result result = new Result();
		// if service url is empty or message is of error type
		if (serviceUrl.isEmpty() || messageType == IMessageProvider.ERROR) {
			result.setMessage(message);
			result.setStatus(Status.ERROR);

			return result;
		}
		// if service url syntax is valid but still not validated by gateway
		if (message
				.equals(ExplorationMessages.ExplorationPage_PressTheValidateButton)) { // validate
																						// the
																						// service
																						// url
			result = validateServiceUrl(serviceUrl, new SubProgressMonitor(
					monitor, 8));
		} // if service url syntax is valid and validated by gateway
		else if (message
				.equals(ExplorationMessages.ExplorationPage_ServiceValidContinue)) { // return
																						// the
																						// previous
																						// result
			result = this.previousResult;
		}

		return result;
	}

	/**
	 * @return - the model changed notification decision.
	 */
	private boolean getModelChangedNotificationDecision() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				changedNotificationDecision = MessageDialog
						.openConfirm(
								new Shell(),
								ExplorationMessages.GatewayExplorationPage_TemplateDeletionMsg,
								ExplorationMessages.GatewayExplorationPage_ServiceChangeMsg
										+ ExplorationMessages.GatewayExplorationPage_ServiceChangeMsgCont);
			}
		});

		return changedNotificationDecision;
	}

	/**
	 * Updates model and publishes without validation. Used by catalog button,
	 * when ok is pressed.
	 * 
	 * @param serviceName
	 * @param serviceUrl
	 * @param serviceMetaDataXml
	 * @param edmx
	 * @param serviceDocumentXml
	 * @param result
	 * @param gwConfig
	 * @param servicesEntries
	 */
	protected Result createModelAndPublish(String serviceName,
			String serviceUrl,
			IServerConnectionParameters serverConnectionParams,
			String serviceMetaDataXml, Edmx edmx, String serviceDocumentXml,
			ODataEntry serviceEntry, Result result) {
		// create result
		result.setServiceUrl(serviceUrl);
		result.setServiceName(serviceName);
		result.setServiceMetadataXml(serviceMetaDataXml);
		result.setEdmx(edmx);
		result.setServiceDocumentXml(serviceDocumentXml);
		result.setServerConnection(serverConnectionParams);
		result.setServiceEntry(serviceEntry);

		result = createModelAndPublish(result);

		return result;
	}

	/**
	 * Updates model and publishes without validation.
	 * 
	 * @param result
	 * @return - result
	 */
	public Result createModelAndPublish(Result result) {
		// create model
		ExplorationModel model = createModel(result);
		// publish model
		boolean published = publish(model);
		if (published) {
			this.previousResult = result;
		} else {
			result = this.previousResult;
		}

		return result;
	}

	/**
	 * Validates the given url syntax.
	 * 
	 * @param url
	 * @throws MalformedURLException
	 */
	private void validateURLSyntax(String url) throws MalformedURLException {
		if (!GenericServiceUrlValidator.isServiceUrlSyntaxValid(url)) {
			throw new MalformedURLException(
					ExplorationMessages.ExplorationPage_MALFORMED_URL);
		}
	}

	/**
	 * Publish the model.
	 */
	public boolean publish(ExplorationModel model) {
		boolean nextPagedWasDisplayed = this.page.wasNextPagedDisplayed();
		this.component.setObservableChanged();
		if (nextPagedWasDisplayed) {
			// String prevUrl = (String)
			// this.previousModel.get(ServiceModelConstants.SERVICE_URL);
			String prevUrl = this.previousResult.getServiceUrl();
			String currUrl = (String) model
					.get(ServiceModelConstants.SERVICE_URL);
			// compare service url of the models
			if (currUrl != null && currUrl.equalsIgnoreCase(prevUrl)) { // if
																		// the
																		// urls
																		// are
																		// equal
																		// then
																		// return
				return false;
			}

			boolean changeModel = true;
			// yes - signal page to pop-up if some one is listening
			if (this.component.countComponentObservers() > 0) {
				changeModel = getModelChangedNotificationDecision();
				if (changeModel) {
					// if yes was pressed - signal the control to notify
					// listeners
					this.component.notifyObservers(model);
				} else
				// !changeModel
				{
					this.component.clearObservableChanged();

					return false;
				}
			}
		} else
		// if (!nextPagedWasDisplayed)
		{
			// publish for the first time
			this.component.notifyObservers(model);
		}

		return true;
	}

	/**
	 * Sets the initial data to the page. Mainly used by the Search Console.
	 * Updates the model and publishes it. The data must be validated, otherwise
	 * model will not be updated and published.
	 * 
	 * @param data
	 *            - initial data
	 */
	public Result setInitialData(Map<String, Object> data) {
		Result result = new Result();

		if (data == null || data.isEmpty()) {
			Logger.getUtilsLogger().logWarning(
					ExplorationMessages.ExplorationController_6);
			result.setStatus(Status.ERROR);
			result.setMessage(ExplorationMessages.ExplorationController_6);

			return result;
		}

		String serviceName = (String) data
				.get(ServiceModelConstants.SERVICE_NAME.toString());
		String serviceUrl = (String) data.get(ServiceModelConstants.SERVICE_URL
				.toString());
		String serviceMetaDataXml = (String) data
				.get(ServiceModelConstants.METADATA_XML.toString());
		Edmx edmx = (Edmx) data.get(ServiceModelConstants.METADATA_OBJECT
				.toString());
		String serviceDocumentXml = (String) data
				.get(ServiceModelConstants.SERVICE_DOCUMENT.toString());
		IServerConnectionParameters serverConnectionParams = (IServerConnectionParameters) data
				.get(ServiceModelConstants.SERVER_CONNECTION_PARAMETERS
						.toString());

		if (serviceName == null || serviceUrl == null
				|| serverConnectionParams == null || serviceMetaDataXml == null
				|| edmx == null || serviceDocumentXml == null) {
			Logger.getUtilsLogger().logWarning(
					ExplorationMessages.ExplorationController_9);
			result.setStatus(Status.ERROR);
			result.setMessage(ExplorationMessages.ExplorationController_9);

			return result;
		}

		ODataEntry serviceEntry = (ODataEntry) data
				.get(ServiceModelConstants.SERVICE_ENTRY.toString());
		if (serviceEntry == null) {
			serviceEntry = Result.EMPTY_SERVICE_ENTRY;
		}

		result.setEdmx(edmx);

		// add here the specific validation of a extending controller classes
		result = this.validateServiceResult(result, new NullProgressMonitor());

		// create model and publish
		result = createModelAndPublish(serviceName, serviceUrl,
				serverConnectionParams, serviceMetaDataXml, edmx,
				serviceDocumentXml, serviceEntry, result);

		return result;
	}

	/**
	 * Validates service url. If it is valid, then gets Odata entry of the
	 * service, updates model and publishes it.
	 * 
	 * @param serviceUrl
	 *            - service url
	 * @param monitor
	 * @return
	 */
	public Result validateServiceAndPublish(String serviceUrl,
			IProgressMonitor monitor) {
		monitor.setTaskName(ExplorationMessages.ExplorationController_0);
		Result result = validateServiceUrl(serviceUrl, monitor);

		monitor.internalWorked(1);
		if (monitor.isCanceled()) {
			return null;
		}

		if (result.getStatus() == Status.OK) {
			// create model
			result = createModelAndPublish(result);
		}

		return result;
	}

	/**
	 * Validates the given service details.
	 * 
	 * @param metadataUri
	 * @param serviceDocumentUri
	 * @param monitor
	 * @return
	 */
	public Result validateServiceAndPublish(String metadataUri,
			String serviceDocumentUri, IProgressMonitor monitor) {
		monitor.setTaskName(ExplorationMessages.ExplorationController_1);
		Result result = validateServiceFiles(metadataUri, serviceDocumentUri,
				new SubProgressMonitor(monitor, 7));
		monitor.worked(7);

		if (result.getStatus() == Status.OK) {
			// create model
			result = createModelAndPublish(result);
		}

		return result;
	}

	public IServiceCatalogProvider getServiceCatalogProvider() {
		return serviceCatalogProvider;
	}

	public String getCatalogButtonNameString() {
		if (serviceCatalogProvider == null) {
			return EMPTY;
		}
		return serviceCatalogProvider.getButtonNameString();
	}
}
