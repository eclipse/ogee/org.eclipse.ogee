/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration.model;

import java.util.EnumMap;

import org.eclipse.ogee.component.exploration.service.model.extensions.ServiceModelConstants;

/**
 * Represents an Exploration Component Model.
 */
public class ExplorationModel {
	private EnumMap<ServiceModelConstants, Object> model;

	/**
	 * Constructs a new ExplorationComponentModel.
	 */
	public ExplorationModel() {
		// the model for the UI part of the component
		this.model = new EnumMap<ServiceModelConstants, Object>(
				ServiceModelConstants.class);
	}

	/**
	 * Constructs a new ExplorationComponentModel.
	 */
	public ExplorationModel(EnumMap<ServiceModelConstants, Object> model) {
		this();

		if (model == null || model.isEmpty()) {
			return;
		}

		this.model.putAll(model);
	}

	/**
	 * @return - the public model of this component.
	 */
	public EnumMap<ServiceModelConstants, Object> getModel() {
		return this.model;
	}

	/**
	 * @param key
	 * @return - the matching object of the given key from the model.
	 */
	public Object get(ServiceModelConstants key) {
		return this.model.get(key);
	}

	/**
	 * Saves the given pair of (key,value) in the model.
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public Object put(ServiceModelConstants key, Object value) {
		return model.put(key, value);
	}

	/**
	 * Clean the model.
	 */
	public void clear() {
		this.model.clear();
	}

	/**
	 * @return - true if the model is empty, false otherwise.
	 */
	public boolean isEmpty() {
		if (this.model.isEmpty()) {
			return true;
		}

		return false;
	}
}
