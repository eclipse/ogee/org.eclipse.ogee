/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.olingo.odata2.api.edm.provider.EdmProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderException;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.core.edm.provider.EdmxProvider;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ogee.client.connectivity.impl.ServerConnectionParameters;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.component.exploration.controller.ExplorationController;
import org.eclipse.ogee.component.exploration.model.ExplorationModel;
import org.eclipse.ogee.component.exploration.nls.messages.ExplorationMessages;
import org.eclipse.ogee.component.exploration.page.ExplorationPage;
import org.eclipse.ogee.component.exploration.service.model.extensions.IServiceModel;
import org.eclipse.ogee.component.exploration.service.model.extensions.ServiceModelConstants;
import org.eclipse.ogee.core.extensions.components.Component;
import org.eclipse.ogee.utils.olingo.parser.ConvertOlingoEdmToEdmx;
import org.eclipse.ogee.utils.service.validation.Result;

/**
 * Represents an Exploration component.
 */
public class ExplorationComponent extends Component implements IServiceModel {
	// controller variable
	protected ExplorationController controller;
	// page
	protected ExplorationPage page;
	// model
	protected ExplorationModel model;
	// component id
	public static final String COMPONENT_ID = "org.eclipse.ogee.component.exploration"; //$NON-NLS-1$

	// Olingo Parser
	private EdmProvider edmProvider;
	private ConvertOlingoEdmToEdmx convertOlingoEdmToEdmx;
	private static final String CHARACTER_ENCODING = "UTF-8"; //$NON-NLS-1$

	/**
	 * Constructs a new Exploration Component.
	 */
	public ExplorationComponent() {
		// set the messages to match this component
		ExplorationMessages.initializeMessages(ExplorationMessages.BUNDLE_NAME,
				ExplorationMessages.class);

		this.model = new ExplorationModel();

		this.page = new ExplorationPage();

		this.controller = new ExplorationController(this.page, this);
	}

	@Override
	public void update(Object object) {
		// do nothing
	}

	@Override
	public List<IWizardPage> getWizardPages() {
		List<IWizardPage> pages = new LinkedList<IWizardPage>();
		pages.add(this.page);

		return pages;
	}

	public void notifyObservers(ExplorationModel model) {
		this.model = model;
		notifyComponentObservers((IServiceModel) this);
	}

	@Override
	public EnumMap<ServiceModelConstants, Object> getServiceModel() {
		return this.model.getModel();
	}

	@Override
	public void initialize(Map<String, Object> data) {
		if (data == null || data.isEmpty()) {
			InputStream inputStream = null;
			try {
				data = new HashMap<String, Object>();

				ServerConnectionParameters connParams = new ServerConnectionParameters(
						"host", "port"); //$NON-NLS-1$ //$NON-NLS-2$				
				data.put(ServiceModelConstants.SERVER_CONNECTION_PARAMETERS
						.name(), connParams);
				// set service name
				data.put(ServiceModelConstants.SERVICE_NAME.name(), ""); //$NON-NLS-1$
				// set service metadata url
				data.put(ServiceModelConstants.SERVICE_URL.name(), ""); //$NON-NLS-1$
				// set service url
				data.put(ServiceModelConstants.SERVICE_URL_BASE.name(), ""); //$NON-NLS-1$
				// set service metadata xml
				String metadataXml = readXmlFile("serviceMetadata.xml"); //$NON-NLS-1$
				data.put(ServiceModelConstants.METADATA_XML.name(), metadataXml);
				// set service document xml
				String serviceDocumentXml = readXmlFile("serviceDocument.xml"); //$NON-NLS-1$
				data.put(ServiceModelConstants.SERVICE_DOCUMENT.name(),
						serviceDocumentXml);
				// set service edmx object

				inputStream = new ByteArrayInputStream(
						metadataXml.getBytes(CHARACTER_ENCODING));

				this.edmProvider = new EdmxProvider().parse(inputStream, false);
				this.convertOlingoEdmToEdmx = new ConvertOlingoEdmToEdmx(
						edmProvider);
				Edmx edmx = convertOlingoEdmToEdmx.convertToEdmx();

				data.put(ServiceModelConstants.METADATA_OBJECT.name(), edmx);
			} catch (IOException e) {
				return;
			} catch (EntityProviderException e) {
				return;
			} catch (ODataException e) {
				return;
			} catch (Exception e) {
				return;
			} finally {
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException e) {
						return;
					}
				}

			}

		}

		Result result = this.controller.setInitialData(data);
		this.page.handleServiceValidationResult(result);
	}

	/**
	 * Reads an XML file
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private String readXmlFile(String fileName) throws IOException {
		InputStream inputStream = null;
		BufferedReader bufferedReader = null;
		StringBuilder contents = new StringBuilder();
		Reader reader = null;

		try {
			URL resource = this.getClass().getResource(fileName);
			inputStream = resource.openStream();
			reader = new InputStreamReader(inputStream);

			// use buffering, reading one line at a time
			// FileReader always assumes default encoding is OK!
			bufferedReader = new BufferedReader(reader);
			String line = null; // not declared within while loop

			/*
			 * readLine is a bit quirky : it returns the content of a line MINUS
			 * the newline. it returns null only for the END of the stream. it
			 * returns an empty String if two newlines appear in a row.
			 */
			while ((line = bufferedReader.readLine()) != null) {
				contents.append(line);
				contents.append(System.getProperty("line.separator")); //$NON-NLS-1$
			}

			String string = contents.toString();

			return string;
		} finally {
			if (bufferedReader != null) {
				bufferedReader.close();
			}

			if (reader != null) {
				reader.close();
			}

			if (inputStream != null) {
				inputStream.close();
			}
		}
	}

	public ExplorationController getController() {
		return controller;
	}
}
