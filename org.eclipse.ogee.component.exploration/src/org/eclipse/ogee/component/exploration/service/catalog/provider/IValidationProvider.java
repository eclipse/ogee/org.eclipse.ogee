/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration.service.catalog.provider;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ogee.utils.service.validation.Result;

public interface IValidationProvider {
	/**
	 * This method may be overridden by extending classes in order to perform
	 * additional validation of the service result object
	 * 
	 * @param result
	 * @param monitor
	 * @return
	 */
	public Result validateServiceResult(Result result, IProgressMonitor monitor);

}
