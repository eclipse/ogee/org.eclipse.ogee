/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration.service.catalog.provider;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ogee.component.exploration.activator.Activator;

public class CatalogExtentionManager {

	/**
	 * returns service ServiceCatalogProvider
	 * 
	 * @return IServiceCatalogProvider
	 */
	public static IServiceCatalogProvider getServiceCatalogProvider(
			IValidationProvider validationProvider) {

		try {
			IConfigurationElement[] scpConfigElems = Platform
					.getExtensionRegistry().getConfigurationElementsFor(
							Activator.PLUGIN_ID + ".service_catalog"); //$NON-NLS-1$

			for (IConfigurationElement svConfigElem : scpConfigElems) {
				// load first service catalog provider and exit
				IServiceCatalogProvider serviceCatalogProvider = (IServiceCatalogProvider) svConfigElem
						.createExecutableExtension("ServiceCatalogProvider"); //$NON-NLS-1$

				String buttonNameStringAttribute = svConfigElem
						.getAttribute("buttonNameString");//$NON-NLS-1$
				serviceCatalogProvider
						.setButtonNameString(buttonNameStringAttribute);
				serviceCatalogProvider
						.setValidationProvider(validationProvider);
				return serviceCatalogProvider;

			}
		} catch (CoreException e) {
			return null;
		}

		return null;
	}

}
