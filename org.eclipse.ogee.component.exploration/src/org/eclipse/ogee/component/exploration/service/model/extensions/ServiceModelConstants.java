/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration.service.model.extensions;

import org.eclipse.ogee.client.connectivity.api.IServerConnectionParameters;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.generic.ODataEntry;

/**
 * Represents an OData model constants.
 * 
 */
public enum ServiceModelConstants {
	/**
	 * OData server connection parameters.
	 * <p>
	 * The value object is {@link IServerConnectionParameters}.
	 */
	SERVER_CONNECTION_PARAMETERS,
	/**
	 * OData service url.
	 * <p>
	 * The value object is {@link String}.
	 */
	SERVICE_URL,
	/**
	 * OData service metadata object.
	 * <p>
	 * The value object is {@link Edmx}.
	 */
	METADATA_OBJECT,
	/**
	 * OData service metadata xml.
	 * <p>
	 * The value object is {@link String}.
	 */
	METADATA_XML,
	/**
	 * OData service name.
	 * <p>
	 * The value object is {@link String}.
	 */
	SERVICE_NAME,
	/**
	 * OData service document.
	 * <p>
	 * The value object is {@link String}.
	 */
	SERVICE_DOCUMENT,
	/**
	 * OData service entry.
	 * <p>
	 * The value object is {@link ODataEntry}.
	 */
	SERVICE_ENTRY,
	/**
	 * Catalog service path.
	 * <p>
	 * The value object is {@link String}.
	 */
	CATALOG_SERVICE_PATH,
	/**
	 * Server type.
	 * <p>
	 * The value object is {@link String}.
	 */
	SERVER_TYPE,
	/**
	 * OData service base URL.
	 * <p>
	 * The value object is {@link String}.
	 */
	SERVICE_URL_BASE
}
