/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration.service.model.extensions;

import java.util.EnumMap;

/**
 * Represents an OData model.
 * 
 */
public interface IServiceModel {
	/**
	 * Returns an OData model.
	 * 
	 * @see ServiceModelConstants
	 * @return an OData model
	 */
	public EnumMap<ServiceModelConstants, Object> getServiceModel();
}
