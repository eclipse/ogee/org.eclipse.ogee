/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration.service.catalog.provider;

import org.eclipse.ogee.utils.service.validation.Result;

public interface IServiceCatalogProvider {
	/**
	 * must be invoked after service selection was made
	 * 
	 * @return OData model data
	 */
	public Result getSelectedServiceValidationResult();

	/**
	 * Sets the catalog button String to be displayed
	 * 
	 * @param buttonNameString
	 */
	public void setButtonNameString(String buttonNameString);

	/**
	 * @return catalog button string
	 */
	public String getButtonNameString();

	/**
	 * This method allows extending classes to supply Validation Provider in
	 * order to perform additional validation of the service result object
	 * 
	 * @param result
	 * @param monitor
	 * @return
	 */
	public void setValidationProvider(IValidationProvider validationProvider);
}
