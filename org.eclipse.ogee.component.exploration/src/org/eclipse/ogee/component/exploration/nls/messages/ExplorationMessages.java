/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.component.exploration.nls.messages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.ogee.component.exploration.activator.Activator;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.osgi.util.NLS;

/**
 * The messages of the Exploration component.
 */
public class ExplorationMessages extends NLS {
	public static final String BUNDLE_NAME = "org.eclipse.ogee.component.exploration.nls.messages.exploration"; //$NON-NLS-1$

	public static String Custom_Description;
	public static String Custom_ServiceDocumentFile;
	public static String Custom_ServiceUrl;
	public static String Custom_ServiceMetadataFile;
	public static String Custom_ValidationMessage;
	public static String GatewayExplorationPage_ExplorationPageTitle;
	public static String GatewayExplorationPage_ServiceChangeMsg;
	public static String GatewayExplorationPage_ServiceChangeMsgCont;
	public static String GatewayExplorationPage_TemplateDeletionMsg;
	public static String ExplorationController_0;
	public static String ExplorationController_1;
	public static String ExplorationController_4;
	public static String ExplorationController_6;
	public static String ExplorationController_8;
	public static String ExplorationController_9;
	public static String ExplorationController_10;
	
	public static String ExplorationPage_1;
	public static String ExplorationPage_10;
	public static String ExplorationPage_11;
	public static String ExplorationPage_15;
	public static String ExplorationPage_18;
	public static String ExplorationPage_2;
	public static String ExplorationPage_20;
	public static String ExplorationPage_21;
	public static String ExplorationPage_3;
	public static String ExplorationPage_5;
	public static String ExplorationPage_6;
	public static String ExplorationPage_7;
	public static String ExplorationPage_Test;
	public static String ExplorationPage_MALFORMED_URL;
	public static String ExplorationPage_SERVICE_METADATA_IS_MALFORMED;
	public static String ExplorationPage_PressTheValidateButton;
	public static String ExplorationPage_ServiceValidContinue;
	public static String ExplorationDialog_InvalidService;
	public static String ExplorationDialog_UnknownHost;
	public static String ExplorationDialog_ValidationCanceled;
	public static String CouldNotCreateEdmxSet;
	public static String ObsoleteValidation;
	public static String InvalidXMLResponse;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, ExplorationMessages.class);
	}

	public static void addPropertiesFile(File propsFile) {
		try {
			String classFilePath = ExplorationMessages.class.getResource(
					"ExplorationMessages.class").getPath(); //$NON-NLS-1$
			URL classResource = Activator.getDefault().getBundle()
					.getResource(classFilePath);

			URL classResourceURL = FileLocator.toFileURL(classResource);
			String encodedUrl = classResourceURL.toString().replace(" ", "%20"); //$NON-NLS-1$ //$NON-NLS-2$
			URI uri = new URI(encodedUrl);
			File classFile = new File(uri);
			String propsFileName = propsFile.getName();

			String propsFilePath = classFile.getAbsolutePath().replace(
					"ExplorationMessages.class", propsFileName); //$NON-NLS-1$
			File destFile = new File(propsFilePath);

			copyFile(propsFile, destFile);
		} catch (Exception e) {
			Logger.getUtilsLogger().logError(e);
		}
	}

	private static String copyFile(File srcFile, File destFile)
			throws IOException {
		FileInputStream fin = null;
		FileOutputStream fout = null;

		try {
			fin = new FileInputStream(srcFile);
			fout = new FileOutputStream(destFile);

			byte[] b = new byte[1024];
			int noOfBytes = 0;

			// read bytes from source file and write to destination file
			while ((noOfBytes = fin.read(b)) != -1) {
				fout.write(b, 0, noOfBytes);
			}

			return destFile.getAbsolutePath();
		} finally {
			if (fin != null) {
				fin.close();
			}

			if (fout != null) {
				fout.close();
			}
		}
	}

	protected ExplorationMessages() {

	}
}
