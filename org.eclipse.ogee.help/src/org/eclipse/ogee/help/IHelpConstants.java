package org.eclipse.ogee.help;

/**
 * Represent help constants for the context id inside the contextHelp.xml file
 */
public interface IHelpConstants {
	/**
	 * For help context
	 */
	public static final String ORG_ECLIPSE_UI_HELP = "org.eclipse.ui.help"; //$NON-NLS-1$
	/**
	 * For Configuration Dialog
	 */
	public static final String CONFIG_HELP_CONTEXT_ID = "org.eclipse.ogee.context"; //$NON-NLS-1$
	/**
	 * context id for creating proxy.
	 */
	public static final String NEW_PROXY_HELP = ".eb8de224b8fd4dde95a25f6d856208f7"; //$NON-NLS-1$

	/**
	 * context id for creating new Starter Application Project
	 */
	public static final String NEW_PROJECT_HELP = ".99ba696b4bec44688be3df34db44e382"; //$NON-NLS-1$

	/**
	 * context id for templates
	 */
	public static final String TEMPLATES_HELP = ".99ba696b4bec44688be3df34db44e382tempaltes"; //$NON-NLS-1$

	/**
	 * context id for templates
	 */
	public static final String SERVICE_IMPLEMENTATION_HELP = ".serviceImplementationHelp"; //$NON-NLS-1$
	/**
	 * context id for creating a Blank OData Model
	 */
	public static final String NEW_ODATA_MODEL = "org.eclipse.ogee.core.NewODataModel"; //$NON-NLS-1$
	/**
	 * context id for creating an OData Model Using Service URL
	 */
	public static final String SVC_FROM_URL = "org.eclipse.ogee.core.SvcFromUrl"; //$NON-NLS-1$
	/**
	 * context id for creating an OData Model Using Service Metadata File
	 */
	public static final String SVC_FROM_FILE = "org.eclipse.ogee.core.SvcFromFile"; //$NON-NLS-1$
	/**
	 * context id for creating OData model using Service Catalog
	 */
	//public static final String SERVICE_EXTENSION_CATALOG = "org.eclipse.ogee.core.exploration.SvcExtnCatalog"; //$NON-NLS-1$
	/**
	 * context id for Exploration Page
	 */
	public static final String LOCATE_ODATA_SVC = "org.eclipse.ogee.component.exploration.LocateODataservice";//$NON-NLS-1$
	/**
	 * context id for order and layout of the pages for application in selected
	 * toolkit.
	 */
	public static final String GENERIC_LIST = "org.eclipse.ogee.component.list.GenericList";//$NON-NLS-1$
	/**
	 * Context ID for OData Editor Add EDMX Reference Dialog.
	 */
	public static final String HELP_CONTEXT_ID_REFERENCESDIALOG = "org.eclipse.ogee.designer.edmxreferencedialog"; //$NON-NLS-1$
	/**
	 * Context ID for OData Editor Preferences Dialog.
	 */
	public static final String HELP_CONTEXT_ID_PREFERENCES = "org.eclipse.ogee.designer.odataeditorpreferences"; //$NON-NLS-1$
	/**
	 * Context ID for Model Export
	 */
	public static final String MODEL_EXPORT = "org.eclipse.ogee.export.ModelExport"; //$NON-NLS-1$
	/**
	 * Context ID for configuring the connection settings.
	 */
	public static final String CONFIG_CONN = "org.eclipse.ogee.common.config_conn"; //$NON-NLS-1$
	/**
	 * Context ID for defining the connection settings to a host, and the
	 * configuration settings for installed toolkits.
	 */
	public static final String CONN_SETTINGS = "org.eclipse.ogee.common.conn_settings"; //$NON-NLS-1$
	/**
	 * Context ID for component form
	 */
	public static final String COMPONENT_FORM = "org.eclipse.ogee.component.form.Component_Form"; //$NON-NLS-1$
	/**
	 * Context ID for Ogee Preference Page
	 */
	public static final String ODATADEVELOPMENT_PREF = "org.eclipse.ogee.utils.ODataDevelopment_Pref"; //$NON-NLS-1$
	/**
	 * Context ID for OData Editor OData Model Page
	 */
	public static final String HELP_CONTEXT_ID = "org.eclipse.ogee.designer.odataeditor"; //$NON-NLS-1$

	/**
	 * Context ID for OData Editor EDMX References Page
	 */
	public static final String HELP_CONTEXT_ID_EDMXREFERENCES = "org.eclipse.ogee.designer.odataeditoredmxreferences"; //$NON-NLS-1$

	/**
	 * Context ID for Service Catalog
	 */
	public static final String SERVICE_CATALOG = "org.eclipse.ogee.core.exploration.Service_Catalog"; //$NON-NLS-1$
}
