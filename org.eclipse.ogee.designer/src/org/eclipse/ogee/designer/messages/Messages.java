/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.messages;

import org.eclipse.osgi.util.NLS;

/**
 * Message class to be used for NLS. All String messages are stored in the
 * properties file and accessed via constants from this class
 * 
 */
public final class Messages extends NLS {

	private static final String BUNDLE_NAME = "org.eclipse.ogee.designer.messages.messages"; //$NON-NLS-1$

	// /////////////// START OF MESSAGE CONSTANTS /////////////////

	/**
	 * Description of the Warning message for the renaming of Entity Type in
	 * different canvas
	 */
	public static String ODATAPASTE_ENTITYTYPE_RENAME;

	/**
	 * Description of the Warning message for the renaming of Function Import in
	 * different canvas
	 */
	public static String ODATAPASTE_FUNCTIONIMPORT_RENAME;

	/**
	 * Description of the Warning message for the renaming of Complex Type in
	 * different canvas
	 */
	public static String ODATAPASTE_COMPLEXTYPE_RENAME;

	/**
	 * Description of the Warning message when return type of Function Import is
	 * not copied in different canvas
	 */
	public static String ODATAPASTE_FUNCTIONIMPORT_RETURNTYPE_NOTCOPIED;

	/**
	 * Display Name For Entity Type
	 */
	public static String ODATAEDITOR_ENTITYTYPE_NAME;

	/**
	 * Description for Entity Type
	 */
	public static String ODATAEDITOR_ENTITYTYPE_DESC;

	/**
	 * Display Name For Referenced Entity Type
	 */
	public static String ODATAEDITOR_REFERENCED_ENTITYTYPE_NAME;

	/**
	 * Description for Referenced Entity Type
	 */
	public static String ODATAEDITOR_REFERENCED_ENTITYTYPE_DESC;

	/**
	 * Default Name for Entity Type
	 */
	public static String ODATAEDITOR_ENTITYTYPE_DEFAULTNAME;

	/**
	 * Special characters are not allowed in the entity name
	 */
	public static String ODATAEDITOR_SPECIAL_CHAR_MSG;

	/**
	 * First letter of the entity name must be a character
	 */
	public static String ODATAEDITOR_FIRSTLETTER_CHAR_MSG;

	/**
	 * Entity name can not be Empty
	 */
	public static String ODATAEDITOR_ENTITYTYPE_EMPTYNAME_MSG;
	
	/**
	 * Entity name should not be greater than 128
	 */
	public static String ODATAEDITOR_ENTITYTYPE_NAMELENGTH_MSG;

	/**
	 * Special characters are not allowed in the Enum member value
	 */
	public static String ODATAEDITOR_SPECIAL_CHAR_MSG_VALUE;

	/**
	 * Only numbers are allowed in the enum member value
	 */
	public static String ODATAEDITOR_NUMERIC_MSG_VALUE;

	/**
	 * Only numbers are allowed in the enum member value
	 */
	public static String ODATAEDITOR_RANGE_MSG_VALUE;

	/**
	 * Member value length
	 */
	public static String ODATAEDITOR_MEMBER_MAXLENGTH_MSG_VALUE;

	/**
	 * Error message for open editor failed.
	 */
	public static String ODATADIAGRAM_CREATOR_IMPL1;

	/**
	 * Generic error message
	 */
	public static String ODATADIAGRAM_CREATOR_IMPL2;

	/**
	 * Default Entity collection name
	 */
	public static String ODATAEDITOR_ENTITYCOLLECTION_NAME;

	/**
	 * Default Entity Sets section name
	 */
	public static String ODATAEDITOR_ENTITYSET_NAME;

	/**
	 * Default Entity Properties Section name
	 */
	public static String ODATAEDITOR_PROPERTIES_NAME;

	/**
	 * Default Property name
	 */
	public static String ODATAEDITOR_PROPERTIES_DEFAULT_NAME;

	/**
	 * Default Entity Navigation Properties Section name
	 */
	public static String ODATAEDITOR_NAVIGATION_PROPERTIES_NAME;

	/**
	 * Menu help to add entity sets
	 */
	public static String ODATAEDITOR_ENTITYSET_MENU;
	/**
	 * Menu description as tooltip to add entity sets
	 */
	public static String ODATAEDITOR_ENTITYSET_MENU_DESC;
	/**
	 * Menu help to add properties
	 */
	public static String ODATAEDITOR_PROPERTIES_MENU;
	/**
	 * Menu description as tooltip to add properties
	 */
	public static String ODATAEDITOR_PROPERTIES_MENU_DESC;
	/**
	 * Entity name
	 */
	public static String ODATAEDITOR_ENTITY;
	/**
	 * Default text for property name
	 */
	public static String ODATAEDITOR_PROPERTIES_DIALOG_PROPERTY;
	/**
	 * Menu help to add navigation properties
	 */
	public static String ODATAEDITOR_NAVIGATION_PROPERTIES_MENU;

	/**
	 * Description for add navigation properties Menu
	 */
	public static String ODATAEDITOR_NAVIGATION_PROPERTIES_MENU_DESC;
	/**
	 * Default text for navigation property name
	 */
	public static String ODATAEDITOR_NAVIGATION_PROPERTY;

	/**
	 * Default text for Expand/Collapse
	 */
	public static String ODATAEDITOR_EXPANDCOLLAPSE_LABEL;

	/**
	 * Default text for Palette Compartment Associations
	 */
	public static String ODATAEDITOR_TOOLBEHAVIORPROVIDER_ASSOC;

	/**
	 * Default text for Palette Compartment Objects
	 */
	public static String ODATAEDITOR_TOOLBEHAVIORPROVIDER_OBJ;

	/**
	 * Default description for Delete button
	 */
	public static String DEFAULTDELETEFEATURE_DEFAULT_MESSAGE;

	/**
	 * Default text for Delete message
	 */
	public static String DEFAULTDELETEFEATURE_2_XMSG;

	/**
	 * Default text for Delete message
	 */
	public static String DEFAULTDELETEFEATURE_3_XMSG;

	/**
	 * Default text for Delete message
	 */
	public static String DEFAULTDELETEFEATURE_4_XMSG;

	/**
	 * Default text for Delete message
	 */
	public static String DEFAULTDELETEFEATURE_5_XFLD;

	/**
	 * Delete message for Entity Artifact
	 */
	public static String DEFAULTDELETEFEATURE_ENTITY_MSG;

	/**
	 * Delete message for Association Artifact
	 */
	public static String DEFAULTDELETEFEATURE_ASSOC_MSG;

	/**
	 * Name for Association Artifact
	 */
	public static String ODATAEDITOR_ASSOC_NAME;
	/**
	 * Description for Association Artifact
	 */
	public static String ODATAEDITOR_ASSOC_DESC;

	/**
	 * Name for Bidirectional Association Artifact
	 */
	public static String ODATAEDITOR_BIDIRECTIONAL_ASSOC_NAME;

	/**
	 * Description for Bidirectional Association Artifact
	 */
	public static String ODATAEDITOR_BIDIRECTIONAL_ASSOC_DESC;
	/**
	 * Name for Unidirectional Association Artifact
	 */
	public static String ODATAEDITOR_UNIDIRECTIONAL_ASSOC_NAME;

	/**
	 * Description for Unidirectional Association Artifact
	 */
	public static String ODATAEDITOR_UNIDIRECTIONAL_ASSOC_DESC;

	/**
	 * Name for Unidirectional Association Artifact for menu
	 */
	public static String ODATAEDITOR_UNIDIRECTIONAL_ASSOC_MENU;

	/**
	 * Description for Unidirectional Association Artifact for menu
	 */
	public static String ODATAEDITOR_UNIDIRECTIONAL_ASSOC_MENUDESC;

	/**
	 * Name for Function Import
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT;

	/**
	 * Deafult Name for Function Import
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_DEFAULT_NAME;
	/**
	 * Description for Function Import
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_DESC;
	/**
	 * Description for Function Import Menu
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_MENU;
	/**
	 * Description for Function Import Simple Menu
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_SIMPLEMENU;
	/**
	 * Description for Function Import Complex Menu
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_COMPLEXMENU;
	/**
	 * Description for Function Import Entity Menu
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_ENTITYMENU;

	/**
	 * Description for Function Import Enum Menu
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_ENUMMENU;

	/**
	 * Description for Function Import Simple Menu
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_SIMPLEMENU_DESC;
	/**
	 * Description for Function Import Complex Menu
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_COMPLEXMENU_DESC;
	/**
	 * Description for Function Import Entity Menu
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_ENTITYMENU_DESC;

	/**
	 * Description for Function Import Enum Menu
	 */
	public static String ODATAEDITOR_FUNCTION_IMPORT_ENUMMENU_DESC;

	/**
	 * Menu help to add parameters
	 */
	public static String ODATAEDITOR_PARAMETERS_MENU;
	/**
	 * Menu description as tooltip to add parameters
	 */
	public static String ODATAEDITOR_PARAMETERS_MENU_DESC;
	/**
	 * Default text for parameter name
	 */
	public static String ODATAEDITOR_PARAMETERS_DIALOG_PROPERTY;

	/**
	 * Default Parameters Section name
	 */
	public static String ODATAEDITOR_PARAMETERS_NAME;

	/**
	 * Default Return Type Section name
	 */
	public static String ODATAEDITOR_RETURN_TYPE_NAME;

	/**
	 * Display Name For Complex Type
	 */
	public static String ODATAEDITOR_COMPLEXTYPE_NAME;
	/**
	 * Description For Complex Type
	 */
	public static String ODATAEDITOR_COMPLEXTYPE_DESC;

	/**
	 * Display Name For Complex Type Property
	 */
	public static String ODATAEDITOR_COMPLEXPROPERTY_NAME;
	/**
	 * Description For Complex Type Property
	 */
	public static String ODATAEDITOR_COMPLEXPROPERTY_DESC;
	/**
	 * Complex Type name
	 */
	public static String ODATAEDITOR_COMPLEXTYPE_DEFAULTNAME;

	/**
	 * Default Name for Enum Type
	 */
	public static String ODATAEDITOR_ENUMTYPE_DEFAULTNAME;
	/**
	 * Display Name For Enum Type
	 */
	public static String ODATAEDITOR_ENUMTYPE_NAME;
	/**
	 * Description For Enum Type
	 */
	public static String ODATAEDITOR_ENUMTYPE_DESC;
	/**
	 * Display Name For Enum Type Property
	 */
	public static String ODATAEDITOR_ENUMMEMBER_NAME;
	/**
	 * Enum Type name
	 */
	public static String ODATAEDITOR_ENUMMEMBER_DEFAULTNAME;

	/**
	 * Menu help to add enum members
	 */
	public static String ODATAEDITOR_ENUMMEMBER_MENU;
	/**
	 * Menu description as tooltip to add enum member
	 */
	public static String ODATAEDITOR_ENUMMEMBER_MENU_DESC;
	/**
	 * Display Name For Complex Type Property
	 */
	public static String ODATAEDITOR_ENUMPROPERTY_NAME;
	/**
	 * Description For Complex Type Property
	 */
	public static String ODATAEDITOR_ENUMPROPERTY_DESC;
	/**
	 * Undefined String
	 */
	public static String ODATAEDITOR_UNDEFINED_NAME;

	/**
	 * Dircet Editing
	 */
	public static String ODATAEDITOR_DIRECTEDITING;

	/**
	 * Role From String
	 */
	public static String ODATAEDITOR_ROLE_DEFAULTFROM;

	/**
	 * Role To String
	 */
	public static String ODATAEDITOR_ROLE_DEFAULTTO;

	/**
	 * Show/Hide usage
	 */
	public static String ODATAEDITOR_LINK_USAGE;

	/**
	 * Layout feature name
	 */
	public static String ODATAEDITOR_LAYOUT_NAME;
	/**
	 * Layout feature description
	 */
	public static String ODATAEDITOR_LAYOUT_DESC;

	/**
	 * Context menu feature name
	 */
	public static String ODATAEDITOR_CONTEXT_MENU;

	/**
	 * Context menu feature description
	 */
	public static String ODATAEDITOR_CONTEXT_MENU_DESCRIPTION;

	/**
	 * Tool tip for Expand button.
	 */
	public static String EXPAND_ALL_TOOL_TIP;

	/**
	 * Text for Expand button.
	 */
	public static String EXPAND_ALL_TEXT_DISPLAY;

	/**
	 * Tool tip for Collapse button.
	 */
	public static String COLLAPSE_ALL_TOOL_TIP;

	/**
	 * Text for Collapse button.
	 */
	public static String COLLAPSE_ALL_TEXT_DISPLAY;

	/**
	 * Tool tip for "Show/Hide All Usages" button.
	 */
	public static String SHOW_USAGE_TOOL_TIP;

	/**
	 * Text for "Show/Hide All Usages" button.
	 */
	public static String SHOW_USAGE_TEXT_DISPLAY;

	/**
	 * Text forContext menu property
	 */
	public static String ODATAEDITOR_CONTEXTMENU_SIMPLE;

	/**
	 * Text for Context menu complex property
	 */
	public static String ODATAEDITOR_CONTEXTMENU_COMPLEXMENU;

	/**
	 * Text for Context menu entity
	 */
	public static String ODATAEDITOR_CONTEXTMENU_ENTITYMENU;

	/**
	 * Text for Context menu enum
	 */
	public static String ODATAEDITOR_CONTEXTMENU_ENUMMENU;

	/**
	 * Text for Context menu Navigation
	 */
	public static String ODATAEDITOR_CONTEXTMENU_NAVMENU;

	/**
	 * Text for Context menu separator
	 */
	public static String ODATAEDITOR_CONTEXTMENU_SEPARATORMENU;

	/**
	 * Text for Context menu Diagram
	 */
	public static String ODATAEDITOR_CONTEXTMENU_DIAGRAMMENU;

	/**
	 * Text for Context menu Expand
	 */
	public static String ODATAEDITOR_CONTEXTMENU_EXPMENU;

	/**
	 * Text for Context menu Layout
	 */
	public static String ODATAEDITOR_CONTEXTMENU_LAYOUTMENU;

	/**
	 * Text for Context menu Collapse
	 */
	public static String ODATAEDITOR_CONTEXTMENU_COLLMENU;

	/**
	 * Text for LayoutChoice
	 */
	public static String ODATAEDITOR_TEXT_LAYOUTCHOICE;

	/**
	 * Text for Layout Preference Description
	 */
	public static String ODATAEDITOR_LAYOUT_PREFERENCE_DESC;

	/**
	 * Text for Layout Preference Selection Text
	 */
	public static String ODATAEDITOR_LAYOUT_PREFERENCE_TEXT;
	/**
	 * Message class to be used for NLS.
	 */
	private static Messages nls;

	/**
	 * Display Name for View Menu Item
	 */
	public static String ODATAEDITOR_GRAPHICSACTIONBARCONTRIBUTOR_0_XMEN;

	/**
	 * Display Name for Align menu item
	 */
	public static String ODATAEDITOR_ACTIONBARCONTRIBUTOR_0_XMEN;

	/**
	 * Title for Add EDMX wizard page.
	 */
	public static String ADD_EDMX_DIALOG_MSG;

	/**
	 * Description for Add EDMX wizard page.
	 */
	public static String ADD_EDMX_DIALOG_DESC;

	/**
	 * Text for "Go" button.
	 */
	public static String GO_BUTTON;

	/**
	 * Text for "Browse..." button.
	 */
	public static String BROWSE_BUTTON;

	/**
	 * Text for "Add" button.
	 */
	public static String ADD_BUTTON;

	/**
	 * Text for "Remove" button.
	 */
	public static String REMOVE_BUTTON;

	/**
	 * Tooltip for "Remove" button.
	 */
	public static String REMOVE_BUTTON_TOOLTIP;

	/**
	 * Text for "Cancel" button.
	 */
	public static String CANCEL_BUTTON;

	/**
	 * Text for "Add..." button.
	 */
	public static String ADD_CONTD_BUTTON;

	/**
	 * Tooltip for "Add..." button.
	 */
	public static String ADD_CONTD_BUTTON_TOOLTIP;
	/**
	 * Text for "Restore Defaults" button.
	 */
	public static String RESTORE_DEFAULTS_BUTTON;

	/**
	 * Tooltip for "Restore Defaults" button.
	 */
	public static String RESTORE_DEFAULTS_BUTTON_TOOLTIP;

	/**
	 * Title for "Restore Defaults" information dialog.
	 */
	public static String RESTORE_DIALOG_TITLE;

	/**
	 * Message for "Restore Defaults" information dialog.
	 */
	public static String RESTORE_DIALOG_MSG;

	/**
	 * Message for Validating Service.
	 */
	public static String ADD_EDMX_DIALOG_VALIDATING_SERVICE;

	/**
	 * Text for "Service URL" button.
	 */
	public static String ADD_EDMX_DIALOG_SERVICE_URL_BUTTON;

	/**
	 * Text for "Service Meta-data" button.
	 */
	public static String ADD_EDMX_DIALOG_FILE_BUTTON;

	/**
	 * Message for Service URL selection.
	 */
	public static String ADD_EDMX_DIALOG_SERVICE_URL_MSG;

	/**
	 * Message for metadata file selection.
	 */
	public static String ADD_EDMX_DIALOG_FILE_MSG;

	/**
	 * Message for Invalid service URL syntax.
	 */
	public static String ADD_EDMX_DIALOG_INVALID_URL;

	/**
	 * Label for Service Details.
	 */
	public static String ADD_EDMX_DIALOG_LABEL_SERVICE;

	/**
	 * Title for Service Details.
	 */
	public static String ADD_EDMX_DIALOG_TITLE;

	/**
	 * Filter name for All Files.
	 */
	public static String FILE_BROWSER_ALL_FILES;

	/**
	 * Title for File Browser.
	 */
	public static String ADD_EDMX_DIALOG_FILE_BROWSER_TITLE;

	/**
	 * Heading for Referenced Schemas.
	 */
	public static String EDMX_REF_PAGE_HEADING;

	/**
	 * Section header for Referenced Schemas.
	 */
	public static String EDMX_REF_PAGE_SEC_HEADING;

	/**
	 * Label for Referenced Schemas.
	 */
	public static String EDMX_REF_PAGE_LABEL;
	/**
	 * Label text for total count.
	 */
	public static String EDMX_REF_PAGE_TOTAL;
	/**
	 * Title for "Remove EDMX Reference" confirmation dialog message.
	 */
	public static String EDMX_REF_PAGE_REMOVE_EDMX_TITLE;

	/**
	 * Message for "Remove EDMX Reference" confirmation dialog message.
	 */
	public static String EDMX_REF_PAGE_REMOVE_EDMX_MSG;

	/**
	 * Title for "Remove EDMX Reference" dialog message.
	 */
	public static String EDMX_REF_PAGE_REMOVE_EDMX_DIALOG_TITLE;

	/**
	 * Message for "Remove EDMX Reference" dialog message.
	 */
	public static String EDMX_REF_PAGE_REMOVE_DIALOG_EDMX_MSG;
	/**
	 * Error message for "Remove EDMX Reference".
	 */
	public static String EDMX_REF_PAGE_REMOVE_ERROR_MSG;

	/**
	 * Warning message for "Remove EDMX Reference".
	 */
	public static String EDMX_REF_PAGE_REMOVE_WARNING_MSG;

	/**
	 * General Error message for "Remove EDMX Reference".
	 */
	public static String EDMX_REF_PAGE_REMOVE_ERROR_MSG2;

	/**
	 * Text for "OData Model" tab in OData multi-page editor.
	 */
	public static String ODATA_MULTIPAGE_EDITOR_MODEL_TAB;

	/**
	 * Text for "References" tab in OData multi-page editor.
	 */
	public static String ODATA_MULTIPAGE_EDITOR_REF_TAB;
	/**
	 * Text for expand all.
	 */
	public static String EDMX_REF_PAGE_EXPAND_ALL;

	/**
	 * Text for collapse all.
	 */
	public static String EDMX_REF_PAGE_COLLAPSE_ALL;

	/**
	 * Text for Schema details title.
	 */
	public static String SCHEMA_DETAILS_TITLE;

	/**
	 * Message for schema details section.
	 */
	public static String SCHEMA_DETAILS_MSG;

	/**
	 * Text for referenced schema label.
	 */
	public static String SCHEMA_DETAILS_REF_SCHEMA_NAMESPACE;

	/**
	 * Tooltip for referenced schema label.
	 */
	public static String SCHEMA_DETAILS_REF_SCHEMA_NAMESPACE_TOOLTIP;

	/**
	 * Text for current schema namespace label.
	 */
	public static String SCHEMA_DETAILS_CURRENT_SCHEMA_NAMESPACE;

	/**
	 * Tooltip for current schema namespace label.
	 */
	public static String SCHEMA_DETAILS_CURRENT_SCHEMA_NAMESPACE_TOOLTIP;

	/**
	 * Text for Alias text box label.
	 */
	public static String SCHEMA_DETAILS_REF_SCHEMA_ALIAS;

	/**
	 * Tooltip for Alias text box label.
	 */
	public static String SCHEMA_DETAILS_REF_SCHEMA_ALIAS_TOOLTIP;

	/**
	 * Message for Alias text box.
	 */
	public static String SCHEMA_DETAILS_REF_SCHEMA_ALIAS_MSG;

	/**
	 * Message for Alias validation.
	 */
	public static String ALIAS_VALIDATION1;

	/**
	 * Message for Alias validation.
	 */
	public static String ALIAS_VALIDATION2;

	/**
	 * Text for "Update" command.
	 */
	public static String ODATA_UPDATE_COMMAND;

	/**
	 * Text for "CUT" Action.
	 */
	public static String ODATA_CUTACTION_LABEL;

	/**
	 * ToolTip for CUT Action
	 */
	public static String ODATA_CUTACTION_TOOLTIP;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME0;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME1;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME2;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME3;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME4;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME5;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME6;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME7;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME8;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME9;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME10;

	/**
	 * Progress task name.
	 */
	public static String PROGRESS_TASK_NAME11;

	/**
	 * Entity name can not be Empty
	 */
	public static String ODATAEDITOR_EMPTYVALUE_MSG;

	/**
	 * Can Not Edit Value
	 */
	public static String ODATAEDITOR_CAN_NOT_EDIT_VALUE;
	// /////////////// END OF MESSAGE CONSTANTS /////////////////

	public static String ODataMultiPageEditor_0;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	/**
	 * 
	 * @return Messages
	 */
	public static Messages getNLS() {

		if (nls == null) {
			synchronized (Messages.class) {
				if (nls == null) {
					nls = new Messages();
				}
			}
		}

		return nls;
	}

	private Messages() {

		super();
	}

}
