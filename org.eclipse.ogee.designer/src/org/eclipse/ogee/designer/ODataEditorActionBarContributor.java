/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer;

import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.graphiti.ui.editor.DiagramEditorActionBarContributor;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.RetargetAction;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Provides some standard-actions and adds them to the toolbar or the top
 * level-menu. Some of the standard-actions are: undo/redo, delete, copy/paste,
 * alignment, zooming & expand/collapse OData artifacts.
 * 
 */
public class ODataEditorActionBarContributor extends
		DiagramEditorActionBarContributor {

	/**
	 * Creates and initializes all Actions.
	 */
	@Override
	protected void buildActions() {

		super.buildActions();

		// This is added to disable the CUT action in EDIT
		addRetargetAction((RetargetAction) ActionFactory.CUT.create(PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()));

		// add an "Expand All" action to expand all OData artifacts from
		// graphical editor tool bar.
		final RetargetAction expandAllAction = new RetargetAction(
				IODataEditorConstants.ACTION_ID_EXPAND_ALL,
				Messages.EXPAND_ALL_TEXT_DISPLAY, IAction.AS_PUSH_BUTTON);
		expandAllAction.setImageDescriptor(AbstractUIPlugin
				.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
						ODataImageProvider.IMG_EDIT_EXPANDALL));

		addRetargetAction(expandAllAction);
		// add a "Collapse All" action to collapse all OData artifacts from
		// graphical editor tool bar.
		final RetargetAction collapseAllAction = new RetargetAction(
				IODataEditorConstants.ACTION_ID_COLLAPSE_ALL,
				Messages.COLLAPSE_ALL_TEXT_DISPLAY, IAction.AS_PUSH_BUTTON);

		collapseAllAction.setImageDescriptor(AbstractUIPlugin
				.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
						ODataImageProvider.IMG_EDIT_COLLAPSEALL));
		addRetargetAction(collapseAllAction);
		// add a "Show/Hide All Usages" action to show/hide usage of all Types
		// from graphical editor tool bar.
		final RetargetAction usageAction = new RetargetAction(
				IODataEditorConstants.ACTION_ID_SHOW_HIDE_USAGES,
				Messages.SHOW_USAGE_TEXT_DISPLAY, IAction.AS_CHECK_BOX);
		usageAction.setImageDescriptor(AbstractUIPlugin
				.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
						ODataImageProvider.IMG_FILE_PATH_DISP_REL));
		addRetargetAction(usageAction);
	}

	/**
	 * @see org.eclipse.gef.ui.actions.ActionBarContributor
	 */
	@Override
	protected void declareGlobalActionKeys() {

		super.declareGlobalActionKeys();
	}

	/**
	 * Adds Actions to the given IToolBarManager, which is displayed above the
	 * editor.
	 */
	@Override
	public void contributeToToolBar(IToolBarManager tbm) {

		super.contributeToToolBar(tbm);

		// Add a button in graphical editor tool bar to Expand all OData
		// artifacts.
		tbm.add(getAction(IODataEditorConstants.ACTION_ID_EXPAND_ALL));
		// Add a button in graphical editor tool bar to Collapse all OData
		// artifacts.
		tbm.add(getAction(IODataEditorConstants.ACTION_ID_COLLAPSE_ALL));
		// Add a Separator after the expand/collapse buttons.
		tbm.add(new Separator());
		// Add a button in graphical editor tool bar to Show/Hide Usages of all
		// the OData types.
		tbm.add(getAction(IODataEditorConstants.ACTION_ID_SHOW_HIDE_USAGES));
		// Add a Separator after the "Show/Hide All Usages" button.
		tbm.add(new Separator());
	}

	/**
	 * Adds Actions to the given IMenuManager, which is displayed as the
	 * main-menu of Eclipse.
	 */
	@Override
	public void contributeToMenu(IMenuManager menubar) {
		IMenuManager editMenu = menubar
				.findMenuUsingPath(IWorkbenchActionConstants.M_EDIT);

		if (editMenu != null) {
			MenuManager alignments = new MenuManager(
					Messages.ODATAEDITOR_ACTIONBARCONTRIBUTOR_0_XMEN);
			alignments.add(getAction(GEFActionConstants.ALIGN_LEFT));
			alignments.add(getAction(GEFActionConstants.ALIGN_CENTER));
			alignments.add(getAction(GEFActionConstants.ALIGN_RIGHT));
			alignments.add(new Separator());
			alignments.add(getAction(GEFActionConstants.ALIGN_TOP));
			alignments.add(getAction(GEFActionConstants.ALIGN_MIDDLE));
			alignments.add(getAction(GEFActionConstants.ALIGN_BOTTOM));
			alignments.add(new Separator());
			alignments.add(getAction(GEFActionConstants.MATCH_WIDTH));
			alignments.add(getAction(GEFActionConstants.MATCH_HEIGHT));
			editMenu.insertAfter(ActionFactory.SELECT_ALL.getId(), alignments);
		}

		// Create view menu ...
		MenuManager viewMenu = new MenuManager(
				Messages.ODATAEDITOR_GRAPHICSACTIONBARCONTRIBUTOR_0_XMEN);
		viewMenu.add(getAction(GEFActionConstants.ZOOM_IN));
		viewMenu.add(getAction(GEFActionConstants.ZOOM_OUT));
		viewMenu.add(getAction(GEFActionConstants.TOGGLE_GRID_VISIBILITY));
		viewMenu.add(getAction(GEFActionConstants.TOGGLE_SNAP_TO_GEOMETRY));

		// ... and add it. The position of the view menu differs depending on
		// which menus exist (see Bugzilla
		// https://bugs.eclipse.org/bugs/show_bug.cgi?id=381437)
		if (editMenu != null) {
			// Edit menu exists --> place view menu directly in front of it
			menubar.insertAfter(IWorkbenchActionConstants.M_EDIT, viewMenu);
		} else if (menubar.findMenuUsingPath(IWorkbenchActionConstants.M_FILE) != null) {
			// File menu exists --> place view menu behind it
			menubar.insertAfter(IWorkbenchActionConstants.M_FILE, viewMenu);
		} else {
			// Add view menu as first entry
			IContributionItem[] contributionItems = menubar.getItems();
			if (contributionItems != null && contributionItems.length > 0) {
				// Any menu exists --> place view menu in front of it it
				menubar.insertBefore(contributionItems[0].getId(), viewMenu);
			} else {
				// No item exists --> simply add view menu
				menubar.add(viewMenu);
			}
		}

		IMenuManager fileMenu = menubar
				.findMenuUsingPath(IWorkbenchActionConstants.M_FILE);
		if (fileMenu != null) {
			// Might not be available in RCP case
			fileMenu.insertAfter(ActionFactory.EXPORT.getId(),
					getAction(IODataEditorConstants.SAVEIMAGE_ACTION_ID));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.actions.ActionBarContributor#addRetargetAction(org
	 * .eclipse.ui.actions.RetargetAction)
	 */
	@Override
	protected void addRetargetAction(RetargetAction action) {
		String actionId = action.getId();
		if (IODataEditorConstants.REMOVE_ACTION_ID.equalsIgnoreCase(actionId)
				|| IODataEditorConstants.UPDATE_ACTION_ID
						.equalsIgnoreCase(actionId)) {
			return;
		}
		super.addRetargetAction(action);
	}

	@Override
	public void setActiveEditor(IEditorPart editor) {

		try {
			super.setActiveEditor(editor);
		} catch (Exception e) {
			// do not show errors.
		}
	}

}
