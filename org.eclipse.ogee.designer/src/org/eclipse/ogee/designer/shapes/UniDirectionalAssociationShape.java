/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.shapes;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.GraphicsAlgorithmContainer;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.ogee.designer.contexts.AssociationAddConnectionContext;
import org.eclipse.ogee.designer.types.AssociationType;
import org.eclipse.ogee.designer.types.MultiplicityType;
import org.eclipse.ogee.designer.utils.AssociationUtil;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.StyleUtil;

/**
 * This creates the line for Association which connects two Entity Types
 * 
 */

public class UniDirectionalAssociationShape extends AbstractODataShape {

	private Connection connection;
	private Polyline line;

	/**
	 * Constructor
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param featureProvider
	 *            IFeatureProvider instance
	 * @param context
	 *            IAddContext instance
	 */
	public UniDirectionalAssociationShape(ContainerShape container,
			IFeatureProvider featureProvider, IAddContext context,
			Connection connection) {
		super(container, featureProvider, context);
		this.connection = connection;
	}

	@Override
	public void createControls(ContainerShape container, IAddContext context) {

		line = gaService.createPolyline(connection);
		line.setForeground(manageColor(IODataEditorConstants.LINE_COLOR_ASSOC));
		line.setFilled(false);
		line.setLineWidth(IODataEditorConstants.ARTIFACT_BORDER_WIDTH);
		line.setLineVisible(true);

		link(connection, context.getNewObject());
		MultiplicityType sourceMultiplicity = AssociationUtil.getMultiplicity(
				context, true);
		MultiplicityType targetMultiplicity = AssociationUtil.getMultiplicity(
				context, false);
		ConnectionDecorator cd = peCreateService.createConnectionDecorator(
				connection, false, 0.0, true);
		createSourceDecorator(cd, sourceMultiplicity);
		link(cd,
				((AssociationAddConnectionContext) context)
						.getSourceRoleObject());

		ConnectionDecorator cd1 = peCreateService.createConnectionDecorator(
				connection, false, 1.0, true);
		createDestinationDecorator(cd1, targetMultiplicity);
		link(cd1,
				((AssociationAddConnectionContext) context)
						.getTargetRoleObject());

		if ((((AssociationAddConnectionContext) context).getAssociationType() == AssociationType.NONE)) {

			cd.getGraphicsAlgorithm().setLineVisible(false);
			cd1.getGraphicsAlgorithm().setLineVisible(false);
		}

		ConnectionDecorator leftCardDecorator = peCreateService
				.createConnectionDecorator(connection, true, 0.05, true);
		Text leftCardText = gaService.createText(leftCardDecorator);
		leftCardText.setValue(AssociationUtil
				.getMultiplicityValue(sourceMultiplicity));
		leftCardText.setStyle(StyleUtil
				.getDefaultStyleForCardinalityText(featureProvider
						.getDiagramTypeProvider().getDiagram()));
		// link Source Role for Connection Decorators
		link(leftCardDecorator,
				((AssociationAddConnectionContext) context)
						.getSourceRoleObject());

		ConnectionDecorator rightCardDecorator = peCreateService
				.createConnectionDecorator(connection, true, 0.90, true);
		Text rightCardText = gaService.createText(rightCardDecorator);
		rightCardText.setValue(AssociationUtil
				.getMultiplicityValue(targetMultiplicity));
		rightCardText.setStyle(StyleUtil
				.getDefaultStyleForCardinalityText(featureProvider
						.getDiagramTypeProvider().getDiagram()));
		// link Target Role for Connection Decorators
		link(rightCardDecorator,
				((AssociationAddConnectionContext) context)
						.getTargetRoleObject());

	}

	@Override
	public GraphicsAlgorithm[] getControls() {
		return new GraphicsAlgorithm[] { line };
	}

	@Override
	public PictogramElement[] getPictogramElements() {
		return new PictogramElement[] { connection };
	}

	private Polyline createDestinationDecorator(
			GraphicsAlgorithmContainer gaContainer,
			MultiplicityType multiplicity) {
		IGaService gaService = Graphiti.getGaService();
		Polyline polyline = null;
		polyline = gaService.createPolyline(gaContainer, new int[] { -10, 5, 0,
				0, -10, -5 });
		polyline.setForeground(manageColor(IODataEditorConstants.LINE_COLOR_ASSOC));
		polyline.setLineWidth(2);
		return polyline;
	}

	private Polyline createSourceDecorator(
			GraphicsAlgorithmContainer gaContainer,
			MultiplicityType multiplicity) {

		IGaService gaService = Graphiti.getGaService();
		Polyline polyline = null;
		polyline = gaService.createPolyline(gaContainer, new int[] { -10, 5, 0,
				0, -10, -5 });
		polyline.setForeground(manageColor(IODataEditorConstants.LINE_COLOR_ASSOC));
		polyline.setLineWidth(2);
		polyline.setLineVisible(false);
		return null;
	}

}
