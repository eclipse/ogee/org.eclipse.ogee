/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.shapes;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.mm.GraphicsAlgorithmContainer;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;
import org.eclipse.graphiti.util.IColorConstant;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.PropertyUtil;

/**
 * This creates the line for link usage
 * 
 */

public class LinkUsageShape implements IODataShape {

	private Connection connection;
	private Polyline line;
	private IFeatureProvider featureProvider;

	/**
	 * Holds the additional properties and the values associated with it
	 */
	private Map<String, String> propertyValues = new HashMap<String, String>();
	private Diagram parentContainer;
	private ICreateConnectionContext context;

	/**
	 * Creation service for GraphicsAlgorithams
	 */
	protected IGaService gaService = Graphiti.getGaService();

	/**
	 * Creation service for PictogramElements
	 */
	protected IPeCreateService peCreateService = Graphiti.getPeCreateService();

	/**
	 * Constructor
	 * 
	 * @param featureProvider
	 *            IFeatureProvider instance
	 * @param context
	 *            IAddContext instance
	 */
	public LinkUsageShape(IFeatureProvider featureProvider,
			ICreateConnectionContext context) {

		this.featureProvider = featureProvider;
		this.parentContainer = this.featureProvider.getDiagramTypeProvider()
				.getDiagram();
		this.context = context;
	}

	/**
	 * Calls the child class methods to create the UI and add the associated
	 * behaviors
	 */
	public void createContents() {
		createControls(this.parentContainer, this.context);
	
	}

	private void createControls(Diagram diagram,
			ICreateConnectionContext connectionContext) {

		this.connection = this.peCreateService
				.createFreeFormConnection(diagram);

		this.connection.setStart(connectionContext.getSourceAnchor());
		this.connection.setEnd(connectionContext.getTargetAnchor());

		this.line = this.gaService.createPolyline(this.connection);
		this.line
				.setForeground(manageColor(IODataEditorConstants.LINE_COLOR_ASSOC));
		this.line.setFilled(Boolean.FALSE);
		this.line.setLineWidth(Integer
				.valueOf(IODataEditorConstants.ARTIFACT_BORDER_WIDTH));
		this.line.setLineVisible(Boolean.TRUE);
		this.line
				.setLineStyle(org.eclipse.graphiti.mm.algorithms.styles.LineStyle.DOT);
		this.connection.setGraphicsAlgorithm(this.line);

		ConnectionDecorator cd = this.peCreateService
				.createConnectionDecorator(this.connection, false, 0.0, true);
		createDestinationLinkDecorator(cd);
		PropertyUtil.setAsLinkConncetion(this.connection);

	}

	@Override
	public GraphicsAlgorithm[] getControls() {
		return new GraphicsAlgorithm[] { this.line };
	}

	@Override
	public PictogramElement[] getPictogramElements() {
		return new PictogramElement[] { this.connection };
	}

	/**
	 * Sets an ID for the different available shape types
	 * 
	 * @param id
	 *            ID for the Shape
	 */
	@Override
	public void setId(String id) {
		this.propertyValues.put("ID", id); //$NON-NLS-1$
	}

	/**
	 * returns the Shape ID
	 * 
	 * @return String ID for the shape
	 */
	@Override
	public String getId() {
		return this.propertyValues.get("ID"); //$NON-NLS-1$
	}

	/**
	 * Used in case a unique property has to be set for the shape
	 * 
	 * @param key
	 *            Property name
	 * @param value
	 *            Property value
	 */
	@Override
	public void setData(String key, String value) {
		this.propertyValues.put(key, value);
	}

	/**
	 * Returns the value for the given input
	 * 
	 * @param key
	 *            Property name
	 * @return String Property value
	 */
	@Override
	public String getData(String key) {
		return this.propertyValues.get(key);
	}

	/**
	 * A convenience method for the color handling which simply calls
	 * {@link IGaService#manageColor (org.eclipse.graphiti.mm.pictograms.Diagram.Diagram, IColorConstant)}
	 * to manage a {@link Color} used within the
	 * {@link org.eclipse.graphiti.mm.pictograms.Diagram.Diagram}.
	 * 
	 * @param colorConstant
	 *            The color constant to manage.
	 * 
	 * @return The managed color.
	 */
	protected Color manageColor(IColorConstant colorConstant) {
		return Graphiti.getGaService().manageColor(
				this.featureProvider.getDiagramTypeProvider().getDiagram(),
				colorConstant);
	}

	/*
	 * A method to create the polyline for showing usages
	 * @param  gaContainer
	 *         Container for all the artifacts
	 */
	private Polyline createDestinationLinkDecorator(
			GraphicsAlgorithmContainer gaContainer) {
		IGaService gaService1 = Graphiti.getGaService();
		Polyline polyline = null;
		polyline = gaService1.createPolyline(gaContainer, new int[] { -10, 5, 0,
				0, -10, -5 });
		polyline.setForeground(manageColor(IODataEditorConstants.LINE_COLOR_ASSOC));
		polyline.setLineWidth(Integer.valueOf(IODataEditorConstants.LINE_SECONDARY_WIDTH));
		return polyline;
	}

}
