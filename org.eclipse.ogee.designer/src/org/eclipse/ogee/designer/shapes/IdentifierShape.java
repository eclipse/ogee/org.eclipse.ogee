/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.shapes;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.designer.utils.StyleUtil;

/**
 * This creates a shape which has the Name and the Icon The name can be a header
 * for an artifact or the element Icons are associated images for each type of
 * artifact
 * 
 */
public class IdentifierShape extends AbstractODataShape {

	/**
	 * Artifact name.
	 */
	protected String artifactName;

	/**
	 * Icon path
	 */
	protected String imageRef; // Icon path
	/**
	 * Text Control instance
	 */
	protected Text text;

	/**
	 * Text shape instance
	 */
	protected Shape textShape;
	/**
	 * Image Shape Instance.
	 */
	protected Shape imageShape;
	/**
	 * Image controls to display icon.
	 */
	protected Image image; // Icon
	/**
	 * Whether the Text is header text
	 */
	protected boolean isHeader = false;

	/**
	 * Whether the Text is Objectheader text
	 */
	protected boolean isObjectHeader = false;

	/**
	 * ID to distinguish between Identifiers ( ENTITY_NAME, ENTITY_SET_NAME,
	 * ENTITY_PROPERTY_NAME,NAVIGATION_PROPERTY_NAME etc.)
	 */
	protected String shapeID;

	/**
	 * Domain object instance,this should be linked with shape.
	 */
	protected Object domainObject;

	/**
	 * Container shape for Image and Text shapes.
	 */
	protected ContainerShape identifierContainer;

	/**
	 * Parent Container for Image and Text controls.
	 */
	protected Rectangle identifierRectangle;

	/**
	 * Domain object instance,this should be linked with Image.
	 */
	private Object imgRefObject;

	/**
	 * Constructor
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param artifactName
	 *            String
	 * @param imageRef
	 *            String Icon path
	 * @param featureProvider
	 *            IFeatureProvider instance
	 * @param context
	 *            IAddContext instance
	 * @param domainObject
	 *            business Object
	 * @param imgRefObject
	 * @param isHeader
	 *            boolean true if header
	 * @param shapeID
	 *            String
	 * @param isObjectHeader
	 */
	public IdentifierShape(ContainerShape container, String artifactName,
			String imageRef, IFeatureProvider featureProvider,
			IAddContext context, Object domainObject, Object imgRefObject,
			boolean isHeader, String shapeID, boolean isObjectHeader) {
		super(container, featureProvider, context);
		this.artifactName = artifactName;
		this.imageRef = imageRef;
		this.isHeader = isHeader;
		this.shapeID = shapeID;
		this.domainObject = domainObject;
		this.isObjectHeader = isObjectHeader;
		this.imgRefObject = imgRefObject;
	}

	/**
	 * Constructor
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param artifactName
	 *            String
	 * @param imageRef
	 *            String Icon path
	 * @param featureProvider
	 *            IFeatureProvider instance
	 * @param context
	 *            IAddContext instance
	 * @param domainObject
	 *            business Object
	 * @param isHeader
	 *            boolean true if header
	 * @param shapeID
	 *            String
	 * @param isObjectHeader
	 */
	public IdentifierShape(ContainerShape container, String artifactName,
			String imageRef, IFeatureProvider featureProvider,
			IAddContext context, Object domainObject, boolean isHeader,
			String shapeID, boolean isObjectHeader) {
		this(container, artifactName, imageRef, featureProvider, context,
				domainObject, domainObject, isHeader, shapeID, isObjectHeader);
	}

	/**
	 * Creates the UI and objects for each identifier shape consisting of an
	 * image and text.
	 * 
	 * Rectangle is created as parent of Image and Text.
	 * 
	 * Selection of ImageShape will be decided with
	 * PropertyUtil.isImageSelectable method.
	 * 
	 * @param parentContainer
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 */
	@Override
	public void createControls(ContainerShape parentContainer,
			IAddContext context) {

		EList<Shape> children = parentContainer.getChildren();
		int top = IODataEditorConstants.RECTANGLE_MARGIN;
		GraphicsAlgorithm graphicsAlgorithm = parentContainer
				.getGraphicsAlgorithm();

		if (children.size() > 0) {
			/*
			 * If, an already created shape exists, position below the previous
			 * one
			 */
			Shape previousShape = children.get(children.size() - 1);
			top = previousShape.getGraphicsAlgorithm().getHeight()
					+ previousShape.getGraphicsAlgorithm().getY()
					+ IODataEditorConstants.RECTANGLE_MARGIN;
		}
		/* A container to be used as a group of shapes */
		this.identifierContainer = this.peCreateService.createContainerShape(
				parentContainer, true);

		// Add Shape ID as Identifier
		PropertyUtil.setTypeIdentifier(this.identifierContainer);

		/* Holds the Shapes for Image and Text */
		this.identifierRectangle = this.gaService
				.createPlainRectangle(this.identifierContainer);
		this.identifierRectangle.setLineVisible(Boolean.FALSE);

		/* Image */
		this.imageShape = this.peCreateService.createShape(
				this.identifierContainer,
				PropertyUtil.isImageSelectable(this.shapeID));
		this.image = this.gaService.createImage(this.imageShape, this.imageRef);
		this.gaService.setLocationAndSize(this.image,
				IODataEditorConstants.IMAGE_LEFT_MARGIN, 0,
				IODataEditorConstants.IMAGE_WIDTH,
				IODataEditorConstants.IMAGE_HEIGHT);
		link(this.imageShape, this.imgRefObject);
		/* Text */
		this.textShape = this.peCreateService.createShape(
				this.identifierContainer, false);
		this.text = this.gaService
				.createText(this.textShape, this.artifactName);
		this.text.setHorizontalAlignment(Orientation.ALIGNMENT_LEFT);
		this.text.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
		/* Set if Bold is needed */
		if (this.isHeader) {
			if (this.isObjectHeader) {
				this.text
						.setStyle(StyleUtil
								.getDefaultStyleForObjectHeaderText(this.featureProvider
										.getDiagramTypeProvider().getDiagram()));
			} else {
				this.text.setStyle(StyleUtil
						.getDefaultStyleForHeaderText(this.featureProvider
								.getDiagramTypeProvider().getDiagram()));
			}
		} else {
			this.text.setStyle(StyleUtil
					.getDefaultStyleForText(this.featureProvider
							.getDiagramTypeProvider().getDiagram()));
		}
		this.gaService.setLocationAndSize(this.text,
				IODataEditorConstants.IMAGE_WIDTH
						+ IODataEditorConstants.IMAGE_LEFT_MARGIN, 0,
				graphicsAlgorithm.getWidth()
						- IODataEditorConstants.IMAGE_WIDTH
						- IODataEditorConstants.TEXTBOX_RIGHT_MARGIN,
				IODataEditorConstants.TEXT_DEFAULT_HEIGHT);
		link(this.textShape, this.domainObject);
		this.gaService.setLocationAndSize(this.identifierRectangle, 0, top,
				graphicsAlgorithm.getWidth(),
				IODataEditorConstants.INNER_RECTANGLE_INITIAL_HEIGHT);

		this.identifierRectangle.setStyle(StyleUtil
				.getDefaultSelectedStyle(this.featureProvider
						.getDiagramTypeProvider().getDiagram()));

		link(this.identifierContainer, this.domainObject);

		// Store the shape id for all the controls.
		PropertyUtil.setID(this.identifierContainer, this.shapeID);
		PropertyUtil.setID(this.textShape, this.shapeID);
		PropertyUtil.setID(this.imageShape, this.shapeID);
	}

	/**
	 * Adds Editable behavior to the text
	 * 
	 * @param parentContainer
	 *            ContainerShape instance
	 */
	@Override
	public void addBehaviour(ContainerShape parentContainer) {
		this.featureProvider.getDirectEditingInfo().setMainPictogramElement(
				parentContainer);
		this.featureProvider.getDirectEditingInfo().setPictogramElement(
				this.textShape);
		this.featureProvider.getDirectEditingInfo().setGraphicsAlgorithm(
				this.text);

	}

	/**
	 * this method is responsible to enable the edit mode for text field.
	 */
	public void enableDirectEdit() {
		this.featureProvider.getDirectEditingInfo().setMainPictogramElement(
				this.identifierContainer);
		this.featureProvider.getDirectEditingInfo().setPictogramElement(
				this.textShape);
		this.featureProvider.getDirectEditingInfo().setGraphicsAlgorithm(
				this.text);
		this.featureProvider.getDirectEditingInfo().setActive(true);
		this.featureProvider.getDiagramTypeProvider().getDiagramBehavior()
				.refresh();
	}

	/**
	 * returns an array of Container Shape,Image Shape and Text Shape.
	 */
	@Override
	public PictogramElement[] getPictogramElements() {
		return new PictogramElement[] { this.identifierContainer,
				this.textShape, this.imageShape };
	}

	/**
	 * returns an array of Rectangle, Image And Text.
	 */
	@Override
	public GraphicsAlgorithm[] getControls() {
		return new GraphicsAlgorithm[] { this.identifierRectangle, this.image,
				this.text };
	}

	/**
	 * Identifier Shape will be selected.
	 */
	public void updateSelection() {
		this.featureProvider.getDiagramTypeProvider().getDiagramBehavior()
				.getDiagramContainer()
				.setPictogramElementForSelection(this.identifierContainer);
	}

}
