/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.shapes;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.designer.utils.StyleUtil;
/**
 * The extended inner rectangle for an artifact element Creates the UI and the
 * graphics for the artifact elements with default sizes and properties
 * This is used to create two fields in the same row as opposed to one in the
 * super class.
 * 
 */
public class ExtendedIdentifierShape extends IdentifierShape {
	/**
	 * Text Control instance
	 */
	private Text text1;
	/**
	 * Artifact name.
	 */
	protected String artifactValue;

	private Text textLabel;

	/**
	 * Text shape instance
	 */
	private Shape textShape1;

	/**
	 * Label Text shape instance
	 */
	private Shape textShapeLabel;

	protected String shapeValueID;

	/**
	 * Default constructor 
	 * @param container ContainerShape
	 * @param artifactKeyName String
	 * @param artifactValue String
	 * @param imageRef String
	 * @param featureProvider IFeatureProvider
	 * @param context IAddContext
	 * @param domainObject Object
	 * @param isHeader boolean
	 * @param shapeKeyID String
	 * @param shapeValueID String
	 */
	public ExtendedIdentifierShape(ContainerShape container,
			String artifactKeyName, String artifactValue, String imageRef,
			IFeatureProvider featureProvider, IAddContext context,
			Object domainObject, boolean isHeader, String shapeKeyID,
			String shapeValueID) {
		super(container, artifactKeyName, imageRef, featureProvider, context,
				domainObject, isHeader, shapeKeyID, false);
		this.artifactName = artifactKeyName;
		this.artifactValue = artifactValue;
		this.imageRef = imageRef;
		this.isHeader = isHeader;
		this.shapeID = shapeKeyID;
		this.shapeValueID = shapeValueID;
		this.domainObject = domainObject;
		this.isObjectHeader = false;
	}

	/**
	 * Creates the UI and objects for each identifier shape consisting of an
	 * image and text.
	 * 
	 * Rectangle is created as parent of Image and Text.
	 * 
	 * Selection of ImageShape will be decided with
	 * PropertyUtil.isImageSelectable method.
	 * 
	 * @param parentContainer
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 */
	@Override
	public void createControls(ContainerShape parentContainer,
			IAddContext context) {

		EList<Shape> children = parentContainer.getChildren();
		int top = IODataEditorConstants.RECTANGLE_MARGIN;
		GraphicsAlgorithm graphicsAlgorithm = parentContainer
				.getGraphicsAlgorithm();

		if (children.size() > 0) {
			/*
			 * If, an already created shape exists, position below the previous
			 * one
			 */
			Shape previousShape = children.get(children.size() - 1);
			top = previousShape.getGraphicsAlgorithm().getHeight()
					+ previousShape.getGraphicsAlgorithm().getY()
					+ IODataEditorConstants.RECTANGLE_MARGIN;
		}
		/* A container to be used as a group of shapes */
		this.identifierContainer = this.peCreateService.createContainerShape(
				parentContainer, true);

		// Add Shape ID as Identifier
		PropertyUtil.setTypeIdentifier(this.identifierContainer);

		/* Holds the Shapes for Image and Text */
		this.identifierRectangle = this.gaService
				.createPlainRectangle(this.identifierContainer);
		this.identifierRectangle.setLineVisible(Boolean.FALSE);

		/* Image */
		this.imageShape = this.peCreateService.createShape(
				this.identifierContainer,
				PropertyUtil.isImageSelectable(this.shapeID));
		this.image = this.gaService.createImage(this.imageShape, this.imageRef);

		this.gaService.setLocationAndSize(this.image,
				IODataEditorConstants.IMAGE_LEFT_MARGIN, 0,
				IODataEditorConstants.IMAGE_WIDTH,
				IODataEditorConstants.IMAGE_HEIGHT);
		link(this.imageShape, this.domainObject);
		/* Text */
		this.textShape = this.peCreateService.createShape(
				this.identifierContainer, true);
		this.textShapeLabel = this.peCreateService.createShape(
				this.identifierContainer, false);
		this.textShape1 = this.peCreateService.createShape(
				this.identifierContainer, true);

		this.text = this.gaService
				.createText(this.textShape, this.artifactName);
		this.text.setHorizontalAlignment(Orientation.ALIGNMENT_LEFT);
		this.text.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);

		this.textLabel = this.gaService.createText(this.textShapeLabel, ":");  //$NON-NLS-1$
		this.textLabel.setHorizontalAlignment(Orientation.ALIGNMENT_LEFT);
		this.textLabel.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
		this.textLabel.setStyle(StyleUtil
				.getDefaultStyleForObjectHeaderText(this.featureProvider
						.getDiagramTypeProvider().getDiagram()));

		this.text1 = this.gaService.createText(this.textShape1,
				this.artifactValue);
		this.text1.setHorizontalAlignment(Orientation.ALIGNMENT_LEFT);
		this.text1.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);

		this.text.setStyle(StyleUtil
				.getDefaultStyleForText(this.featureProvider
						.getDiagramTypeProvider().getDiagram()));
		this.text1.setStyle(StyleUtil
				.getDefaultStyleForText(this.featureProvider
						.getDiagramTypeProvider().getDiagram()));
		int textWidth = (graphicsAlgorithm.getWidth()
				- IODataEditorConstants.IMAGE_LEFT_MARGIN
				- IODataEditorConstants.IMAGE_WIDTH
				- IODataEditorConstants.IMAGE_LEFT_MARGIN
				- IODataEditorConstants.TEXTBOX_RIGHT_MARGIN - IODataEditorConstants.SEMICOLON_WIDTH) / 2;

		this.gaService.setLocationAndSize(this.text,
				IODataEditorConstants.IMAGE_LEFT_MARGIN
						+ IODataEditorConstants.IMAGE_WIDTH
						+ IODataEditorConstants.IMAGE_LEFT_MARGIN, 0,
				textWidth, IODataEditorConstants.TEXT_DEFAULT_HEIGHT);

		this.gaService.setLocationAndSize(this.textLabel,
				IODataEditorConstants.IMAGE_LEFT_MARGIN
						+ IODataEditorConstants.IMAGE_WIDTH
						+ IODataEditorConstants.IMAGE_LEFT_MARGIN + textWidth,
				0, IODataEditorConstants.SEMICOLON_WIDTH,
				IODataEditorConstants.TEXT_DEFAULT_HEIGHT);

		this.gaService.setLocationAndSize(this.text1,
				IODataEditorConstants.IMAGE_LEFT_MARGIN
						+ IODataEditorConstants.IMAGE_WIDTH
						+ IODataEditorConstants.IMAGE_LEFT_MARGIN + textWidth
						+ IODataEditorConstants.SEMICOLON_WIDTH, 0, textWidth,
				IODataEditorConstants.TEXT_DEFAULT_HEIGHT);

		link(this.textShape, this.domainObject);
		link(this.textShape1, this.domainObject);

		this.gaService.setLocationAndSize(this.identifierRectangle, 0, top,
				graphicsAlgorithm.getWidth(),
				IODataEditorConstants.INNER_RECTANGLE_INITIAL_HEIGHT);

		this.identifierRectangle.setStyle(StyleUtil
				.getDefaultSelectedStyle(this.featureProvider
						.getDiagramTypeProvider().getDiagram()));

		link(this.identifierContainer, this.domainObject);

		// Store the shape id for all the controls.
		PropertyUtil.setID(this.identifierContainer, this.shapeID);
		PropertyUtil.setID(this.textShape, this.shapeID);
		PropertyUtil.setID(this.textShape1, this.shapeValueID);
		PropertyUtil.setID(this.imageShape, this.shapeID);
	}

}
