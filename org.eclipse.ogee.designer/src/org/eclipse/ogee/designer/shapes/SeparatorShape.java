/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.shapes;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;

/**
 * This creates the separator lines in the UI artifact There are two lines to
 * get the depth effect
 * 
 */
public class SeparatorShape extends AbstractODataShape {

	/**
	 * Shape for primary Line.
	 */
	private Shape primaryLineShape;
	
	/**
	 * Shape for Secondary Line.
	 */
	private Shape secondaryLineShape;
	
	/**
	 * Lighter primary line is the line at the top 
	 * 
	 */
	private Polyline primaryLine;
	
	/**
	 *  Darker secondary line just below the primary line 
	 */
	private Polyline secondaryLine;

	/**
	 * Constructor
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param featureProvider
	 *            IFeatureProvider instance
	 * @param context
	 *            IAddContext instance
	 */
	public SeparatorShape(ContainerShape container,
			IFeatureProvider featureProvider, IAddContext context) {
		super(container, featureProvider, context);
	}

	/**
	 * In order to show Shadow effect two lines are created.
	 */	
	@Override
	public void createControls(ContainerShape parentContainer, IAddContext context) {
		EList<Shape> children = parentContainer.getChildren();
		int top = IODataEditorConstants.RECTANGLE_MARGIN;
		if (children.size() > 0) {
			/*
			 * If, an already created shape exists, position below the previous
			 * one
			 */
			Shape previousShape = children.get(children.size() - 1);
			top = previousShape.getGraphicsAlgorithm().getHeight()
					+ previousShape.getGraphicsAlgorithm().getY()
					+ IODataEditorConstants.RECTANGLE_MARGIN;
		}
		// create shape for line
		this.primaryLineShape =this.peCreateService.createShape(parentContainer, false);
		this.secondaryLineShape = this.peCreateService.createShape(parentContainer, false);

		// create and set graphics algorithm
		// Primary line goes on top and secondary just below it
		// Width is 1px less than the artifact width to get the right effect
		this.primaryLine = this.gaService.createPolyline(this.primaryLineShape, new int[] { 
				0, 0, parentContainer.getGraphicsAlgorithm().getWidth() - 1, 0 });
		this.secondaryLine = this.gaService.createPolyline(this.secondaryLineShape, new int[] {
				0, 1, parentContainer.getGraphicsAlgorithm().getWidth() - 1, 1 });
		// Y being set to be manipulated later in cases of re layout and resize
		// If, Y points are set on create() above for points, we cannot get the
		// Y for the line
		this.primaryLine.setY(top);
		this.secondaryLine.setY(top + 1);
		// Default Colors for the lines
		this.primaryLine.setForeground(this.gaService.manageColor(this.featureProvider
				.getDiagramTypeProvider().getDiagram(),
				IODataEditorConstants.LINE_COLOR));
		this.secondaryLine.setForeground(this.gaService.manageColor(this.featureProvider
				.getDiagramTypeProvider().getDiagram(),
				IODataEditorConstants.LINE_SECONDARY_COLOR));
		this.primaryLine.setLineWidth(Integer.valueOf(IODataEditorConstants.LINE_PRIMARY_WIDTH));
		this.secondaryLine.setLineWidth(Integer.valueOf(IODataEditorConstants.LINE_SECONDARY_WIDTH));
		this.primaryLine.setFilled(Boolean.FALSE);
		this.secondaryLine.setFilled(Boolean.FALSE);
		this.primaryLine.setLineVisible(Boolean.TRUE);
		this.secondaryLine.setLineVisible(Boolean.TRUE);

		link(this.primaryLineShape, context.getNewObject());
		link(this.secondaryLineShape, context.getNewObject());

	}

	/**
	 * returns Primary Line shape in an array.
	 */
	@Override
	public PictogramElement[] getPictogramElements() {
		return new PictogramElement[] {this.primaryLineShape };
	}

	/**
	 * returns Primary Line in an array.
	 */
	@Override
	public GraphicsAlgorithm[] getControls() {
		return new GraphicsAlgorithm[] { this.primaryLine };
	}

}
