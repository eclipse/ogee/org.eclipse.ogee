/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.shapes;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.designer.utils.StyleUtil;

/**
 * The basic inner rectangle for an artifact elements Creates the UI and the
 * graphics for the artifact elements with default sizes and properties
 * 
 */
public class ExtensionArtifactShape extends AbstractODataShape {

	/**
	 *  Type of rectangle 
	 */
	private RectangleType rectangleType;
	
	/**
	 * 	Shape instance for rectangle.
	 */
	private ContainerShape containerShape;
	
	/**
	 * created rectangle instance.
	 */
	private Rectangle rectangle;
	
	/**
	 * ID to differentiate between shapes ( PROPERTY_RECTANGLE, ENTITYSET_RECTANGLE etc). 
	 */
	private String shapeID;

	/**
	 * Constructor
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param featureProvider
	 *            IFeatureProvider instance
	 * @param context
	 *            IAddContext instance
	 * @param rectangleType
	 *            Type of rectangle needed
	 * @param shapeID
	 *            ID for each artifact element for which the rectangle is
	 *            created
	 */
	public ExtensionArtifactShape(ContainerShape container,
			IAddContext context, IFeatureProvider featureProvider,
			RectangleType rectangleType, String shapeID) {
		super(container, featureProvider, context);
		this.rectangleType = rectangleType;
		this.shapeID = shapeID;
	}

	/**
	 * Creates the rectangle of the given type in the constructor.
	 * 
	 * @param parentContainer
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 */
	@Override
	public void createControls(ContainerShape parentContainer, IAddContext context) {
		EList<Shape> children = parentContainer.getChildren();
		int top = IODataEditorConstants.RECTANGLE_MARGIN;
		GraphicsAlgorithm graphicsAlgorithm = parentContainer.getGraphicsAlgorithm();

		if (children.size() > 0) {
			/*
			 * If, an already created shape exists, position below the previous
			 * one
			 */
			Shape previousShape = children.get(children.size() - 1);
			top = previousShape.getGraphicsAlgorithm().getHeight()
					+ previousShape.getGraphicsAlgorithm().getY()
					+ IODataEditorConstants.RECTANGLE_MARGIN;
		}

		this.containerShape = this.peCreateService.createContainerShape(parentContainer, true);
		this.rectangle = null;
		switch (this.rectangleType) {
		case PLAINRECTANGLE:
			this.rectangle = this.gaService.createPlainRectangle(this.containerShape);
			this.rectangle.setLineVisible(Boolean.FALSE);
			this.rectangle.setStyle(StyleUtil.getDefaultSelectedStyle(this.featureProvider.getDiagramTypeProvider().getDiagram()));
			break;
		case INVISIBLERECTANGLE:
			this.rectangle = this.gaService.createInvisibleRectangle(this.containerShape);
			break;
		case RECTANGLE:
			this.rectangle = this.gaService.createRectangle(this.containerShape);
			break;
		default:
			break;
		}
		// Default size and positioning
		this.gaService.setLocationAndSize(this.rectangle, 0, top,
				graphicsAlgorithm.getWidth(),
				IODataEditorConstants.INNER_RECTANGLE_INITIAL_HEIGHT);
		link(this.containerShape, context.getNewObject());
		// Link the shape and ID
		PropertyUtil.setID(this.containerShape, this.shapeID);
	}
	

	/* (non-Javadoc)
	 * @see org.eclipse.ogee.designer.shapes.AbstractODataShape#getPictogramElements()
	 */
	@Override
	public PictogramElement[] getPictogramElements() {
		return new PictogramElement[] { this.containerShape };
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ogee.designer.shapes.AbstractODataShape#getControls()
	 */
	@Override
	public GraphicsAlgorithm[] getControls() {
		return new GraphicsAlgorithm[] {this.rectangle };
	}
}
