/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.designer.shapes;

import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * The main shape which will be used to create different artifacts It has
 * default method signatures for creating the artifacts These methods need to be
 * overridden by the implementing classes
 * 
 */
public interface IODataShape {

	/**
	 * Sets a name for the different available shape types
	 * 
	 * @param id
	 *            ID for the Shape
	 */
	public void setId(String id);

	/**
	 * returns the Shape ID
	 * 
	 * @return String ID for the shape
	 */
	public String getId();

	/**
	 * Used in case a unique property has to be set for the shape
	 * 
	 * @param key
	 *            Property name
	 * @param value
	 *            Property value
	 */
	public void setData(String key, String value);

	/**
	 * Returns the value for the given input
	 * 
	 * @param key
	 *            Property name
	 * @return String Property value
	 */
	public String getData(String key);

	/**
	 * Returns all child shapes for this parent shape
	 * 
	 * @return Shape[] child shapes
	 */
	public PictogramElement[] getPictogramElements();

	/**
	 * Returns all graphic algorithms for this parent shape
	 * 
	 * @return GraphicsAlgorithm[] controls
	 */
	public GraphicsAlgorithm[] getControls();
}
