/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.shapes;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.StyleUtil;

/**
 * The basic outer rectangle for a function import. Creates the UI and the
 * graphics for the artifact with default sizes and properties
 * 
 */
public class BasicFunctionImportShape extends AbstractODataShape {

	/**
	 * Parent Shape for Rounded Rectangle.
	 */
	private ContainerShape containerShape;

	/**
	 * Outer polygon instance.
	 */
	private Polygon functionImportPolygon;

	/**
	 * Is the shape a referenced shape
	 */
	private boolean isReferencedShape = false;

	/**
	 * Constructor
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param featureProvider
	 *            IFeatureProvider instance
	 * @param context
	 *            IAddContext instance
	 */
	public BasicFunctionImportShape(ContainerShape container,
			IFeatureProvider featureProvider, IAddContext context,
			boolean isReferencedShape) {
		super(container, featureProvider, context);
		this.isReferencedShape = isReferencedShape;
	}

	/**
	 * Creates the outer shape using Graphiti APIs Sets the location as the drop
	 * location and default size Default Style is SAP default style
	 * 
	 * @param parentContainer
	 *            ContainerShape instance
	 */
	@Override
	public void createControls(ContainerShape parentContainer,
			IAddContext context) {
		this.containerShape = this.peCreateService.createContainerShape(
				parentContainer, true);
		int xy[] = new int[] { 0, 0, 155, 0,
				IODataEditorConstants.RECTANGLE_INITIAL_WIDTH, 20, 155,
				IODataEditorConstants.RECTANGLE_INITIAL_HEIGHT, 0,
				IODataEditorConstants.RECTANGLE_INITIAL_HEIGHT };
		this.functionImportPolygon = this.gaService.createPolygon(
				this.containerShape, xy);
		this.gaService.setLocationAndSize(this.functionImportPolygon,
				context.getX(), context.getY(),
				IODataEditorConstants.RECTANGLE_INITIAL_WIDTH,
				IODataEditorConstants.RECTANGLE_INITIAL_HEIGHT);

		if (!isReferencedShape) {
			this.functionImportPolygon.setStyle(StyleUtil
					.getDefaultStyleForArtifact(this.featureProvider
							.getDiagramTypeProvider().getDiagram()));
		} else {
			this.functionImportPolygon.setStyle(StyleUtil
					.getReferencedStyleForArtifact(this.featureProvider
							.getDiagramTypeProvider().getDiagram()));
		}
		createBorder();
		link(this.containerShape, context.getNewObject());
		this.peCreateService.createChopboxAnchor(this.containerShape);
	}

	/**
	 * Sets the border for the rectangle
	 */
	private void createBorder() {
		// Creates and sets the border properties
		if (this.functionImportPolygon != null) {
			this.functionImportPolygon.setLineStyle(LineStyle.SOLID);
			this.functionImportPolygon.setLineVisible(Boolean.TRUE);
			this.functionImportPolygon.setLineWidth(Integer
					.valueOf(IODataEditorConstants.ARTIFACT_BORDER_WIDTH));
			this.functionImportPolygon.setTransparency(Double
					.valueOf(IODataEditorConstants.ARTIFACT_TRANSPARENCY));
			if (!isReferencedShape) {
				this.functionImportPolygon.setForeground(this.gaService
						.manageColor(this.featureProvider
								.getDiagramTypeProvider().getDiagram(),
								IODataEditorConstants.ARTIFACT_BORDER));
			} else {
				this.functionImportPolygon
						.setForeground(this.gaService
								.manageColor(
										this.featureProvider
												.getDiagramTypeProvider()
												.getDiagram(),
										IODataEditorConstants.REFERENCED_ARTIFACT_BORDER));
			}
		}
	}

	/**
	 * returns Container Shape for Rectangle in an array.
	 */
	@Override
	public PictogramElement[] getPictogramElements() {
		return new PictogramElement[] { this.containerShape };
	}

	/**
	 * returns rounded rectangle instance in an array.
	 */
	@Override
	public GraphicsAlgorithm[] getControls() {
		return new GraphicsAlgorithm[] { this.functionImportPolygon };
	}

}
