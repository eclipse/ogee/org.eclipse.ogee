/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.shapes;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;
import org.eclipse.graphiti.util.IColorConstant;

/**
 * Abstract shape class. Inheriting classes need to define the abstract methods.
 * 
 */
public abstract class AbstractODataShape implements IODataShape {
	/**
	 * Holds the additional properties and the values associated with it
	 */
	private Map<String, String> propertyValues = new HashMap<String, String>();

	/**
	 * Graphiti Feature provider Instance
	 */
	protected IFeatureProvider featureProvider;

	/**
	 * Creation service for GraphicsAlgorithams
	 */
	protected IGaService gaService = Graphiti.getGaService();

	/**
	 * Creation service for PictogramElements
	 */
	protected IPeCreateService peCreateService = Graphiti.getPeCreateService();

	/**
	 * Parent Container
	 */

	protected ContainerShape container;

	/**
	 * Add Context instance, which holds the domain object
	 */
	private IAddContext context;

	/**
	 * Constructor
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param featureProvider
	 *            IFeatureProvider instance
	 * @param context
	 *            IAddContext instance
	 */
	public AbstractODataShape(ContainerShape container,
			IFeatureProvider featureProvider, IAddContext context) {
		this.container = container;
		this.featureProvider = featureProvider;
		this.context = context;
	}

	/**
	 * Calls the child class methods to create the UI and add the associated
	 * behaviors
	 */
	public void createContents() {
		createControls(this.container, this.context);
		addBehaviour(this.container);
	}

	/**
	 * This method should be overridden by subclasses for adding additional
	 * behaviors.
	 * 
	 * @param parentContainer
	 * 
	 */
	public void addBehaviour(ContainerShape parentContainer) {
		// To be implemented BY SUBCLASSES
	}

	/**
	 * This method should be overridden by subclasses to define the controls.
	 * 
	 * @param parentContainer
	 * @param addContext
	 */
	public abstract void createControls(ContainerShape parentContainer,
			IAddContext addContext);

	/**
	 * A convenience method for the color handling which simply calls
	 * {@link IGaService#manageColor
	 * (org.eclipse.graphiti.mm.pictograms.Diagram.Diagram, IColorConstant)}
	 * to manage a {@link Color} used within the
	 *  {@link org.eclipse.graphiti.mm.pictograms.Diagram.Diagram}.
	 * 
	 * @param colorConstant
	 *            The color constant to manage.
	 * 
	 * @return The managed color.
	 */
	protected Color manageColor(IColorConstant colorConstant) {
		return Graphiti.getGaService().manageColor(
				this.featureProvider.getDiagramTypeProvider().getDiagram(),
				colorConstant);
	}

	/**
	 * Helper method to link a {@link PictogramElement} to a domain object.
	 * 
	 * @param pe
	 *            The pictogram element
	 * @param businessObject
	 *            The domain object
	 */
	protected void link(PictogramElement pe, Object businessObject) {
		link(pe, new Object[] { businessObject });
	}

	/**
	 * Helper method to link a {@link PictogramElement} to a number of domain
	 * objects.
	 * 
	 * @param pe
	 *            The pictogram element
	 * @param businessObjects
	 *            The business objects as an array
	 */
	protected void link(PictogramElement pe, Object businessObjects[]) {
		this.featureProvider.link(pe, businessObjects);
	}

	/**
	 * Sets a name for the different available shape types
	 * 
	 * @param id
	 *            ID for the Shape
	 */
	@Override
	public void setId(String id) {
		this.propertyValues.put("ID", id);  //$NON-NLS-1$
	}

	/**
	 * returns the Shape ID
	 * 
	 * @return String ID for the shape
	 */
	@Override
	public String getId() {
		return this.propertyValues.get("ID");  //$NON-NLS-1$
	}

	/**
	 * Used in case a unique property has to be set for the shape
	 * 
	 * @param key
	 *            Property name
	 * @param value
	 *            Property value
	 */
	@Override
	public void setData(String key, String value) {
		this.propertyValues.put(key, value);
	}

	/**
	 * Returns the value for the given input
	 * 
	 * @param key
	 *            Property name
	 * @return String Property value
	 */
	@Override
	public String getData(String key) {
		return this.propertyValues.get(key);
	}

	/**
	 * 
	 * @see org.eclipse.ogee.designer.shapes.IODataShape#
	 * getPictogramElements()
	 */
	@Override
	public abstract PictogramElement[] getPictogramElements();

	/**
	 * 
	 * @see
	 * org.eclipse.ogee.designer.shapes.IODataShape#getControls()
	 */
	@Override
	public abstract GraphicsAlgorithm[] getControls();
}
