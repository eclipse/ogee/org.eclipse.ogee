/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.graphiti.ui.editor.DefaultPersistencyBehavior;
import org.eclipse.graphiti.ui.editor.DefaultUpdateBehavior;
import org.eclipse.graphiti.ui.editor.DiagramBehavior;
import org.eclipse.graphiti.ui.editor.IDiagramContainerUI;
import org.eclipse.ogee.designer.actions.ODataCutAction;
import org.eclipse.ogee.designer.actions.ODataSelectAllAction;
import org.eclipse.ogee.designer.providers.ODataDiagramContextMenuProvider;

public class ODataDiagramBehavior extends DiagramBehavior {

	private IDiagramContainerUI diagramContainer;
	private ODataDomainModelChangeListener odataModelChangeListener;

	/**
	 * Default Constructor
	 * 
	 * @param diagramContainer
	 */
	public ODataDiagramBehavior(IDiagramContainerUI diagramContainer) {

		super(diagramContainer);
		this.diagramContainer = diagramContainer;
	}

	/*
	 * Just a public re-write (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.graphiti.ui.editor.DiagramBehavior#createPersistencyBehavior
	 * ()
	 */
	@Override
	public DefaultPersistencyBehavior createPersistencyBehavior() {
		return super.createPersistencyBehavior();
	}

	/*
	 * Just a public re-write (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.graphiti.ui.editor.DiagramBehavior#migrateDiagramModelIfNecessary
	 * ()
	 */
	@Override
	public void migrateDiagramModelIfNecessary() {
		super.migrateDiagramModelIfNecessary();
	}

	/**
	 * Registers additional listeners specified in DiagramEditorComp Sub class.
	 */
	@Override
	public void registerBusinessObjectsListener() {

		super.registerBusinessObjectsListener();

		final TransactionalEditingDomain editingDomain = this
				.getEditingDomain();
		if (editingDomain != null) {
			this.odataModelChangeListener = new ODataDomainModelChangeListener();
			editingDomain.addResourceSetListener(this.odataModelChangeListener);
		}
	}

	/**
	 * shouldRegisterContextMenu value will be taken from DiagramEditorComp sub
	 * class.
	 */
	@Override
	protected boolean shouldRegisterContextMenu() {

		// This is done to prevent other plugin contributions to popup menu
		return false;
	}

	/**
	 * Additional actions can be registered from DiagramEditorComp sub class.
	 */
	@Override
	protected void initActionRegistry(ZoomManager zoomManager) {
		super.initActionRegistry(zoomManager);

		// This is added to disable the CUT action in EDIT
		registerAction(new ODataCutAction(getDiagramContainer()
				.getWorkbenchPart()));
		registerAction(new ODataSelectAllAction(getDiagramContainer()
				.getWorkbenchPart()));
	}

	@Override
	public ContextMenuProvider createContextMenuProvider() {
		return new ODataDiagramContextMenuProvider(
				((ODataEditor) this.diagramContainer).getGraphicalViewer(),
				((ODataEditor) this.diagramContainer).getActionRegistry(),
				this.getConfigurationProvider(), getDiagramTypeProvider());
	}

	/**
	 * Update Behavior will be registered from DiagramEditorComp sub class.
	 */
	@Override
	protected DefaultUpdateBehavior createUpdateBehavior() {
		return new ODataEditorUpdateBehavior(
				(ODataEditor) this.diagramContainer);
	}

}
