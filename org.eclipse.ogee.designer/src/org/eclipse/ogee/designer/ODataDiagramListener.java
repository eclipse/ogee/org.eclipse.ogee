/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IDirectEditingInfo;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.contexts.NavigationPropertyContext;
import org.eclipse.ogee.designer.features.ODataAddComplexPropertyFeature;
import org.eclipse.ogee.designer.features.ODataAddEntitySetFeature;
import org.eclipse.ogee.designer.features.ODataAddEnumMemberFeature;
import org.eclipse.ogee.designer.features.ODataAddEnumPropertyFeature;
import org.eclipse.ogee.designer.features.ODataAddNavigationPropertyFeature;
import org.eclipse.ogee.designer.features.ODataAddParameterFeature;
import org.eclipse.ogee.designer.features.ODataAddPropertyFeature;
import org.eclipse.ogee.designer.features.ODataCollapsibleFeature;
import org.eclipse.ogee.designer.features.ODataDoubleClickFeature;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataFeatureProvider;
import org.eclipse.ogee.designer.providers.ODataToolBehaviorProvider;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;

public class ODataDiagramListener implements MouseListener, KeyListener {

	ODataEditor designEditor;

	private TransactionalEditingDomain editingDomain;

	private ODataFeatureProvider featureProvider;

	/**
	 * @param designEditor
	 */
	public ODataDiagramListener(ODataEditor designEditor) {
		super();
		this.designEditor = designEditor;
		this.editingDomain = designEditor.getEditingDomain();
		IDiagramTypeProvider dtp = designEditor.getDiagramTypeProvider();
		if (dtp != null) {
			this.featureProvider = (ODataFeatureProvider) dtp
					.getFeatureProvider();
		}
	}

	/**
	 * 
	 */
	public void registerListeners() {
		GraphicalViewer graphicalViewer = this.designEditor
				.getGraphicalViewer();
		if (graphicalViewer != null) {
			Control control = graphicalViewer.getControl();
			if (control != null) {
				control.addMouseListener(this);
				control.addKeyListener(this);
			}

		}

	}

	/**
	 * 
	 */
	public void removeListeners() {
		GraphicalViewer graphicalViewer = this.designEditor
				.getGraphicalViewer();
		if (graphicalViewer != null) {
			Control control = graphicalViewer.getControl();
			if (control != null) {
				control.removeMouseListener(this);
				control.removeKeyListener(this);
			}
		}

	}

	/**
	 * 
	 * 
	 * @see
	 * org.eclipse.swt.events.MouseListener#mouseDown(org.eclipse.swt.events
	 * .MouseEvent)
	 */
	@Override
	public void mouseDown(MouseEvent arg0) {
		PictogramElement[] selectedPes = this.designEditor
				.getSelectedPictogramElements();

		if (selectedPes.length == 1) {
			final PictogramElement pe = selectedPes[0];
			if (pe.getGraphicsAlgorithm() instanceof Image
					&& PropertyUtil.isTitle(pe)
					&& arg0.getSource() instanceof FigureCanvas) {

				ODataToolBehaviorProvider currentToolBehaviorProvider = (ODataToolBehaviorProvider) this.designEditor
						.getDiagramTypeProvider()
						.getCurrentToolBehaviorProvider();

				// If both Scroll bars are present in order to find the exact
				// location in Graphical editor
				org.eclipse.draw2d.geometry.Point viewLocation = ((FigureCanvas) arg0
						.getSource()).getViewport().getViewLocation();
				Point point = new Point(arg0.x + viewLocation.x, arg0.y
						+ viewLocation.y);
				if (currentToolBehaviorProvider.isLocationExists(pe, point)) {
					this.editingDomain.getCommandStack().execute(
							new RecordingCommand(this.editingDomain,
									Messages.ODATAEDITOR_EXPANDCOLLAPSE_LABEL) {

								@Override
								public void doExecute() {
									ODataCollapsibleFeature collapsableFe = new ODataCollapsibleFeature(
											getFeatureProvider());
									CustomContext context = new CustomContext();
									context.setPictogramElements(new PictogramElement[] { (PictogramElement) pe
											.eContainer() });
									collapsableFe.execute(context);
								}
							});
				}
			}
		}
	}

	/**
	 * Adds a new property on Enter Key press 
	 * based on Key Event
	 * @see
	 * org.eclipse.swt.events.KeyListener#keyPressed(org.eclipse.swt.events.
	 * KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent arg0) {
		PictogramElement[] selectedPes = this.designEditor
				.getSelectedPictogramElements();
		if (selectedPes.length == 1 && selectedPes[0] != null) {
			final PictogramElement pe = selectedPes[0];
			if (arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR) {
				if ((pe instanceof ContainerShape || (PropertyUtil
						.isEnumMember(pe) || PropertyUtil.isEnumMemberValue(pe)))
						&& PropertyUtil.isEnterKeyEnabled(pe)
						&& !this.designEditor.isReadOnly()) {

					this.editingDomain.getCommandStack().execute(
							new RecordingCommand(this.editingDomain) {

								@Override
								public void doExecute() {

									CustomContext context = new CustomContext();
									PictogramElement topContainer = ODataShapeUtil
											.getTopContainer(pe);
									context.setPictogramElements(new PictogramElement[] { topContainer });

									if (PropertyUtil.isProperty(pe)) {
										Object businessObject = getFeatureProvider()
												.getBusinessObjectForPictogramElement(
														pe);
										if (ArtifactUtil
												.isComplexTypeProperty(businessObject)) {
											ODataAddComplexPropertyFeature complexTypeFeature = new ODataAddComplexPropertyFeature(
													getFeatureProvider(),
													getFeatureProvider()
															.getArtifactFactory(),
													getFeatureProvider()
															.getShapesFactory());
											complexTypeFeature.execute(context);
										} else if (ArtifactUtil
												.isEnumTypeProperty(businessObject)) {
											ODataAddEnumPropertyFeature enumTypeFeature = new ODataAddEnumPropertyFeature(
													getFeatureProvider(),
													getFeatureProvider()
															.getArtifactFactory(),
													getFeatureProvider()
															.getShapesFactory());
											enumTypeFeature.execute(context);
										} else {
											ODataAddPropertyFeature addFeature = new ODataAddPropertyFeature(
													getFeatureProvider(),
													getFeatureProvider()
															.getArtifactFactory(),
													getFeatureProvider()
															.getShapesFactory());
											addFeature.setKeyValue(pe);
											addFeature.execute(context);
										}

									} else if (PropertyUtil.isEntitySet(pe)) {

										ODataAddEntitySetFeature addFeature = new ODataAddEntitySetFeature(
												getFeatureProvider(),
												getFeatureProvider()
														.getArtifactFactory(),
												getFeatureProvider()
														.getShapesFactory());
										addFeature.execute(context);

									} else if (PropertyUtil.isEnumMember(pe)
											|| PropertyUtil
													.isEnumMemberValue(pe)) {

										ODataAddEnumMemberFeature addFeature = new ODataAddEnumMemberFeature(
												getFeatureProvider(),
												getFeatureProvider()
														.getArtifactFactory(),
												getFeatureProvider()
														.getShapesFactory());
										addFeature.execute(context);

									} else if (PropertyUtil.isParameter(pe)) {

										ODataAddParameterFeature addFeature = new ODataAddParameterFeature(
												getFeatureProvider(),
												getFeatureProvider()
														.getArtifactFactory(),
												getFeatureProvider()
														.getShapesFactory());
										addFeature.execute(context);

									} else if (PropertyUtil
											.isNavigationProperty(pe)) {
										NavigationPropertyContext navPropertyContext = new NavigationPropertyContext(
												ODataShapeUtil
														.getTopContainer(pe));
										navPropertyContext.setEditMode(true);

										ODataAddNavigationPropertyFeature navPropertyFeature = new ODataAddNavigationPropertyFeature(
												getFeatureProvider(),
												getFeatureProvider()
														.getArtifactFactory(),
												getFeatureProvider()
														.getShapesFactory());
										navPropertyFeature
												.execute(navPropertyContext);
									}

								}

								@Override
								public String getLabel() {
									String lbl = ""; //$NON-NLS-1$

									if (PropertyUtil.isProperty(pe)) {
										Object businessObject = getFeatureProvider()
												.getBusinessObjectForPictogramElement(
														pe);
										if (ArtifactUtil
												.isComplexTypeProperty(businessObject)) {
											lbl = Messages.ODATAEDITOR_COMPLEXPROPERTY_NAME;
										} else {
											lbl = Messages.ODATAEDITOR_PROPERTIES_MENU;
										}

									} else if (PropertyUtil.isEntitySet(pe)) {

										lbl = Messages.ODATAEDITOR_ENTITYSET_MENU;

									} else if (PropertyUtil.isParameter(pe)) {

										lbl = Messages.ODATAEDITOR_PARAMETERS_MENU;

									} else if (PropertyUtil
											.isNavigationProperty(pe)) {

										lbl = Messages.ODATAEDITOR_NAVIGATION_PROPERTIES_MENU;

									} else if (PropertyUtil.isEnumMember(pe)) {
										lbl = Messages.ODATAEDITOR_ENUMMEMBER_MENU;
									}

									return lbl;
								}

							});

				}

			} else if (arg0.keyCode == SWT.ARROW_RIGHT
					&& PropertyUtil.isCollapsed(pe)
					|| arg0.keyCode == SWT.ARROW_LEFT
					&& PropertyUtil.isExpanded(pe)) {

				this.editingDomain.getCommandStack().execute(
						new RecordingCommand(this.editingDomain,
								Messages.ODATAEDITOR_EXPANDCOLLAPSE_LABEL) {

							@Override
							public void doExecute() {
								ODataCollapsibleFeature collapsableFe = new ODataCollapsibleFeature(
										getFeatureProvider());
								CustomContext context = new CustomContext();
								context.setPictogramElements(new PictogramElement[] { pe });
								collapsableFe.execute(context);
							}

						});
			} else if (arg0.keyCode == SWT.F2) {

				if (pe instanceof ContainerShape
						&& PropertyUtil.isTopContainer(pe)) {
					this.editingDomain.getCommandStack().execute(
							new RecordingCommand(this.editingDomain,
									Messages.ODATAEDITOR_DIRECTEDITING) {

								@Override
								public void doExecute() {

									CustomContext context = new CustomContext();
									context.setPictogramElements(new PictogramElement[] { pe });
									ODataDoubleClickFeature editingFeature = new ODataDoubleClickFeature(
											getFeatureProvider());
									if (editingFeature.canExecute(context)) {
										editingFeature.execute(context);
									}
								}

							});
				} else if (pe instanceof ContainerShape
						&& PropertyUtil.isTypeIdentifier(pe)) {
					if (((ContainerShape) pe).getChildren().size() == 4) {
						this.editingDomain.getCommandStack().execute(
								new RecordingCommand(this.editingDomain,
										Messages.ODATAEDITOR_DIRECTEDITING) {

									@Override
									public void doExecute() {

										Shape textShape = ((ContainerShape) pe)
												.getChildren().get(1);
										if (textShape.getGraphicsAlgorithm() instanceof Text) {
											IDirectEditingInfo directEditingInfo = getFeatureProvider()
													.getDirectEditingInfo();
											directEditingInfo
													.setMainPictogramElement(textShape);
											directEditingInfo
													.setPictogramElement(textShape);
											directEditingInfo
													.setGraphicsAlgorithm(textShape
															.getGraphicsAlgorithm());
											directEditingInfo.setActive(true);
											ODataDiagramListener.this.designEditor
													.getDiagramBehavior()
													.refresh();

										}
									}
								});
					}
				} else if (PropertyUtil.isEnumMember(pe)
						|| PropertyUtil.isEnumMemberValue(pe)) {

					this.editingDomain.getCommandStack().execute(
							new RecordingCommand(this.editingDomain,
									Messages.ODATAEDITOR_DIRECTEDITING) {

								@Override
								public void doExecute() {

									if (pe.getGraphicsAlgorithm() instanceof Text) {
										IDirectEditingInfo directEditingInfo = getFeatureProvider()
												.getDirectEditingInfo();
										directEditingInfo
												.setMainPictogramElement(pe);
										directEditingInfo
												.setPictogramElement(pe);
										directEditingInfo.setGraphicsAlgorithm(pe
												.getGraphicsAlgorithm());
										directEditingInfo.setActive(true);
										ODataDiagramListener.this.designEditor
												.getDiagramBehavior().refresh();

									}

								}

							});
				}

			}
		}
	}

	/**
	 * @return
	 */
	protected ODataFeatureProvider getFeatureProvider() {
		return this.featureProvider;
	}

	@Override
	public void mouseDoubleClick(MouseEvent arg0) {
		// Not required to implement.

	}

	@Override
	public void mouseUp(MouseEvent arg0) {
		// Not required to implement.

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// Not required to implement.

	}

}
