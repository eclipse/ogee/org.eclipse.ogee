/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.List;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.impl.AbstractCreateConnectionFeature;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.contexts.AssociationAddConnectionContext;
import org.eclipse.ogee.designer.contexts.NavigationPropertyContext;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.types.AssociationType;
import org.eclipse.ogee.designer.types.MultiplicityType;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.AssociationUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Role;

public class ODataCreateBiDirectionalAssociationFeature extends
		AbstractCreateConnectionFeature {
	/**
	 * ShapesFactory instance used to create all the shapes.
	 */
	private ShapesFactory shapesFactory;

	/**
	 * ArifactFactory instance used to create all the Business objects.
	 */
	private ArtifactFactory artifactFactory;

	public ODataCreateBiDirectionalAssociationFeature(IFeatureProvider fp,
			ShapesFactory shapesFactory, ArtifactFactory artifactFactory) {
		super(fp, Messages.ODATAEDITOR_BIDIRECTIONAL_ASSOC_NAME,
				Messages.ODATAEDITOR_BIDIRECTIONAL_ASSOC_DESC);
		this.shapesFactory = shapesFactory;
		this.artifactFactory = artifactFactory;
	}

	@Override
	public String getCreateName() {
		return Messages.ODATAEDITOR_BIDIRECTIONAL_ASSOC_NAME;
	}

	@Override
	public String getCreateDescription() {
		return Messages.ODATAEDITOR_BIDIRECTIONAL_ASSOC_DESC;
	}

	/**
	 * Returns true if the association can be created
	 * 
	 * @return boolean true if association can be created
	 */
	@Override
	public boolean canStartConnection(ICreateConnectionContext context) {
		// check for right domain object instance (Only if Entity Type)
		PictogramElement topContainer = ODataShapeUtil.getTopContainer(context
				.getSourcePictogramElement());
		if (topContainer != null
				&& getBusinessObjectForPictogramElement(topContainer) instanceof EntityType
				&& !(ArtifactUtil
						.isReferencedEntityType(getBusinessObjectForPictogramElement(topContainer)))) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the Image associated with the object
	 * 
	 * @return String ImagePath
	 */
	@Override
	public String getCreateImageId() {
		return ODataImageProvider.IMG_BI_ASSOCIATION;
	}

	@Override
	public boolean canCreate(ICreateConnectionContext context) {
		boolean canCreate = false;
		PictogramElement sourcePictogramElement = ODataShapeUtil
				.getTopContainer(context.getSourcePictogramElement());
		PictogramElement targetPictogramElement = ODataShapeUtil
				.getTopContainer(context.getTargetPictogramElement());

		// check for right domain object instance below
		Object source = getBusinessObjectForPictogramElement(sourcePictogramElement);
		Object target = getBusinessObjectForPictogramElement(targetPictogramElement);
		if (source != null && target != null && source instanceof EntityType
				&& target instanceof EntityType
				&& !(ArtifactUtil.isReferencedEntityType(target))) {
			canCreate = true;
		}
		return canCreate;
	}

	@Override
	public Connection create(ICreateConnectionContext context) {

		Connection newConnection = null;

		AssociationUtil.updateAssociationContext(context);

		// get EClasses which should be connected
		Anchor sourceAnchor = context.getSourceAnchor();
		EntityType source = getEntityType(sourceAnchor);
		Anchor targetAnchor = context.getTargetAnchor();
		EntityType target = getEntityType(targetAnchor);

		if (source != null && target != null) {

			// create new business object
			Association association = this.artifactFactory
					.createAssociation(source, target, MultiplicityType.ONE,
							MultiplicityType.MANY);
			NavigationPropertyContext sourceNavContext = new NavigationPropertyContext(
					sourceAnchor.getParent(), targetAnchor.getParent(),
					association);
			sourceNavContext.setSourceMultuplicity(MultiplicityType.ONE);
			sourceNavContext.setTargetMultuplicity(MultiplicityType.MANY);
			sourceNavContext
					.setAssociationType(source == target ? AssociationType.SELF
							: AssociationType.BIDIR);
			ODataAddNavigationPropertyFeature soureceNavPropFeature = new ODataAddNavigationPropertyFeature(
					this.getFeatureProvider(), this.artifactFactory,
					this.shapesFactory);
			soureceNavPropFeature.execute(sourceNavContext);

			NavigationPropertyContext targetNavContext = new NavigationPropertyContext(
					targetAnchor.getParent(), sourceAnchor.getParent(),
					association);
			targetNavContext
					.setAssociationType(source == target ? AssociationType.SELF
							: AssociationType.BIDIR);
			targetNavContext.setSourceMultuplicity(MultiplicityType.MANY);
			targetNavContext.setTargetMultuplicity(MultiplicityType.ONE);
			if (source == target) {
				targetNavContext.setSecondNavPropertyForSelf(true);
			}
			ODataAddNavigationPropertyFeature targetNavPropFeature = new ODataAddNavigationPropertyFeature(
					this.getFeatureProvider(), this.artifactFactory,
					this.shapesFactory);
			targetNavPropFeature.execute(targetNavContext);

			// add connection for business object
			AssociationAddConnectionContext addContext = new AssociationAddConnectionContext(
					sourceAnchor, targetAnchor);
			((AssociationAddConnectionContext) addContext)
					.setAssociationType(AssociationType.BIDIR);
			addContext.setSourceMultiplicity(MultiplicityType.ONE);
			addContext.setTargetMultiplicity(MultiplicityType.MANY);

			// set Source and Target Roles for Connection Decorators
			List<Role> ends = association.getEnds();

			addContext.setSourceRoleObject(ends.get(0));
			addContext.setTargetRoleObject(ends.get(1));

			addContext.setUniDirectionalAssociation(false);
			addContext.setNewObject(association);
			newConnection = (Connection) getFeatureProvider().addIfPossible(
					addContext);

		}
		// activate direct editing after object creation
		getFeatureProvider().getDirectEditingInfo().setActive(true);
		return newConnection;
	}

	/**
	 * Returns the EClass belonging to the anchor, or null if not available.
	 */
	private EntityType getEntityType(Anchor anchor) {
		if (anchor != null) {
			Object object = getBusinessObjectForPictogramElement(anchor
					.getParent());
			if (object instanceof EntityType) {
				return (EntityType) object;
			}
		}
		return null;
	}

	@Override
	public boolean canUndo(IContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDoneChanges() {
		// TODO Auto-generated method stub
		return false;
	}
}
