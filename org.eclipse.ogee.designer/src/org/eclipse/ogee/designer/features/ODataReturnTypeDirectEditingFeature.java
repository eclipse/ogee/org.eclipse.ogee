/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.impl.AbstractDirectEditingFeature;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.api.IODataDiagramCreator;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;

/**
 * This class will provide the editing functionality on text elements of an
 * artifact if it is of type return type in a function import
 * 
 */
public class ODataReturnTypeDirectEditingFeature extends
		AbstractDirectEditingFeature {

	private static final String EMPTY_STRING = "";//$NON-NLS-1$
	/**
	 * ArtifactFactory instance used to create all the Business objects.
	 */
	private ArtifactFactory artifactFactory;
	private EObject prevObject;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactfactory
	 *            Artifact factory instance is passed
	 * @param shapesFactory
	 *            Shapes factory instance is passed
	 */
	public ODataReturnTypeDirectEditingFeature(IFeatureProvider fp,
			ArtifactFactory artifactfactory, ShapesFactory shapesFactory) {
		super(fp);
		this.artifactFactory = artifactfactory;
	}

	/**
	 * Specifies the field type. Text field, Combo Box, etc.
	 */
	@Override
	public int getEditingType() {
		return TYPE_DROPDOWN_READ_ONLY;
	}

	/**
	 * Returns the value of the text which the field currently has
	 * 
	 * @return String the current field value
	 */
	@Override
	public String getInitialValue(IDirectEditingContext context) {

		PictogramElement pe = context.getPictogramElement();
		Object obj = getBusinessObjectForPictogramElement(pe);
		String initialName = EMPTY_STRING;
		if (obj instanceof ReturnEntityTypeUsage) {
			ReturnEntityTypeUsage returnEntityTypeUsage = (ReturnEntityTypeUsage) obj;
			EntityType entityType = returnEntityTypeUsage.getEntityType();
			if (entityType != null
					&& ArtifactUtil.isReferencedEntityType(entityType)) {
				initialName = ArtifactUtil.getArtifactDisplayName(
						this.artifactFactory, entityType);
				return initialName;
			}
		} else if (obj instanceof ComplexTypeUsage) {
			ComplexTypeUsage complexTypeUsage = (ComplexTypeUsage) obj;
			ComplexType complexType = complexTypeUsage.getComplexType();
			if (complexType != null
					&& ArtifactUtil.isReferencedComplexType(complexType)) {
				initialName = ArtifactUtil.getArtifactDisplayName(
						this.artifactFactory, complexType);
				return initialName;
			}
		} else if (obj instanceof EnumTypeUsage) {
			EnumTypeUsage enumTypeUsage = (EnumTypeUsage) obj;
			EnumType enumType = enumTypeUsage.getEnumType();
			if (enumType != null && ArtifactUtil.isReferencedEnumType(enumType)) {
				initialName = ArtifactUtil.getArtifactDisplayName(
						this.artifactFactory, enumType);
				return initialName;
			}
		}
		initialName = ArtifactUtil.getInitialName(obj);
		return initialName != null ? initialName : EMPTY_STRING;
	}

	/**
	 * Populates the given combo boxes for the return type
	 */
	@Override
	public String[] getPossibleValues(IDirectEditingContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object obj = getBusinessObjectForPictogramElement(pe);
		if (obj instanceof SimpleTypeUsage) {
			EDMTypes[] values = EDMTypes.values();
			List<String> types = new ArrayList<String>();
			for (int i = 0; i < values.length; i++) {
				EDMTypes edmType = values[i];
				types.add(edmType.getLiteral());
			}
			return types.toArray(new String[types.size()]);
		} else if (obj instanceof ComplexTypeUsage) {
			List<ComplexType> complexTypes = this.artifactFactory
					.getAllComplexTypesIncludingReferenced();
			List<String> types = new ArrayList<String>();
			for (int i = 0; i < complexTypes.size(); i++) {
				ComplexType complexType = complexTypes.get(i);
				types.add(ArtifactUtil.getArtifactDisplayName(
						this.artifactFactory, complexType));
			}
			types.add(EMPTY_STRING);
			return types.toArray(new String[types.size()]);
		} else if (obj instanceof EnumTypeUsage) {
			List<EnumType> enumTypes = this.artifactFactory
					.getAllEnumTypesIncludingReferenced();
			List<String> types = new ArrayList<String>();
			for (int i = 0; i < enumTypes.size(); i++) {
				EnumType enumType = enumTypes.get(i);
				types.add(ArtifactUtil.getArtifactDisplayName(
						this.artifactFactory, enumType));
			}
			types.add(EMPTY_STRING);
			return types.toArray(new String[types.size()]);
		} else if (obj instanceof ReturnEntityTypeUsage) {
			List<EntityType> entityTypes = this.artifactFactory
					.getAllEntitiesIncludingReferenced();
			List<String> types = new ArrayList<String>();
			for (int i = 0; i < entityTypes.size(); i++) {
				EntityType entityType = entityTypes.get(i);
				types.add(ArtifactUtil.getArtifactDisplayName(
						this.artifactFactory, entityType));
			}
			types.add(EMPTY_STRING);
			return types.toArray(new String[types.size()]);
		} else {
			return super.getPossibleValues(context);
		}

	}

	/**
	 * Returns if the field can be directly edited in the UI
	 * 
	 * @param context
	 *            IDirectEditingContext instance
	 * @return boolean true if the field is editable
	 */
	@Override
	public boolean canDirectEdit(IDirectEditingContext context) {
		GraphicsAlgorithm ga = context.getGraphicsAlgorithm();
		String id = PropertyUtil.getID(ga.getPictogramElement());
		return (PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME.equals(id))
				&& ga instanceof Text;
	}

	/**
	 * Sets the new value to the UI and the object Also makes the corresponding
	 * changes to other elements
	 * 
	 * @param context
	 *            IDirectEditingContext instance
	 */
	@Override
	public void setValue(String value, IDirectEditingContext context) {

		PictogramElement pe = context.getPictogramElement();
		Object obj = getBusinessObjectForPictogramElement(pe);
		EObject currentObject = null;

		if (ArtifactUtil.isFunctionImportReturnType(obj)) {

			IFunctionReturnTypeUsage returnType = (IFunctionReturnTypeUsage) obj;
			if (returnType instanceof SimpleTypeUsage) {
				SimpleType simpleType = ((SimpleTypeUsage) returnType)
						.getSimpleType();
				simpleType.setType(EDMTypes.get(value));
			} else if (returnType instanceof ComplexTypeUsage) {
				List<ComplexType> complexTypes = this.artifactFactory
						.getAllComplexTypesIncludingReferenced();
				ComplexTypeUsage complexTypeUsage = (ComplexTypeUsage) returnType;
				this.prevObject = complexTypeUsage.getComplexType();
				if (value.trim().length() == 0
						|| value.equals(Messages.ODATAEDITOR_UNDEFINED_NAME)) {
					complexTypeUsage.setComplexType(null);
				} else {
					for (ComplexType complexType : complexTypes) {
						String complexTypeDisplayName = ArtifactUtil
								.getArtifactDisplayName(this.artifactFactory,
										complexType);
						if (value.equals(complexTypeDisplayName)) {
							complexTypeUsage.setComplexType(complexType);
							currentObject = complexType;
							break;
						}
					}
				}

			} else if (returnType instanceof EnumTypeUsage) {
				List<EnumType> enumTypes = this.artifactFactory
						.getAllEnumTypesIncludingReferenced();
				EnumTypeUsage enumTypeUsage = (EnumTypeUsage) returnType;
				this.prevObject = enumTypeUsage.getEnumType();
				if (value.trim().length() == 0
						|| value.equals(Messages.ODATAEDITOR_UNDEFINED_NAME)) {
					enumTypeUsage.setEnumType(null);
				} else {
					for (EnumType enumType : enumTypes) {
						String enumDisplayName = ArtifactUtil
								.getArtifactDisplayName(this.artifactFactory,
										enumType);
						if (value.equals(enumDisplayName)) {
							enumTypeUsage.setEnumType(enumType);
							currentObject = enumType;
							break;
						}
					}
				}

			} else if (returnType instanceof ReturnEntityTypeUsage) {
				List<EntityType> entityTypes = this.artifactFactory
						.getAllEntitiesIncludingReferenced();
				ReturnEntityTypeUsage returnEntityTypeUsage = (ReturnEntityTypeUsage) returnType;
				this.prevObject = returnEntityTypeUsage.getEntityType();
				if (value.trim().length() == 0
						|| value.equals(Messages.ODATAEDITOR_UNDEFINED_NAME)) {
					returnEntityTypeUsage.setEntityType(null);
				} else {
					for (EntityType entityType : entityTypes) {
						String entityDisplayName = ArtifactUtil
								.getArtifactDisplayName(this.artifactFactory,
										entityType);
						if (value.equals(entityDisplayName)) {
							returnEntityTypeUsage.setEntityType(entityType);
							currentObject = entityType;
							break;
						}
					}
				}
			}
		}

		if (this.prevObject != null || currentObject != null) {
			// in order to refresh the Graphiti view
			IODataDiagramCreator.INSTANCE.refreshEditor(obj, this.prevObject,
					currentObject);
		}

		// Update the UI
		ODataShapeUtil.updatePictogramElement(pe, getFeatureProvider());
	}
}
