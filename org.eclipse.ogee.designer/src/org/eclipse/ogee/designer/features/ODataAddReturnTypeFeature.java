/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.api.IODataDiagramCreator;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.types.FunctionImportReturnType;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.SimpleTypeUsageImpl;

/**
 * This class is used to add return type for function import
 * 
 */
public class ODataAddReturnTypeFeature extends AbstractCustomFeature {
	/**
	 * artifact factory
	 */
	private ArtifactFactory artifactFactory;
	/**
	 * shapes factory
	 */
	private ShapesFactory shapesFactory;

	/**
	 * To differentiate between KEY and NON KEY property.
	 */
	private FunctionImportReturnType returnType;

	/**
	 * Return type usage
	 */
	Object returnTypeUsage;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactFactory
	 *            ArtifactFactory instance is passed
	 * @param shapesFactory
	 *            ShapesFactory instance is passed
	 * @param returnType
	 */
	public ODataAddReturnTypeFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory,
			FunctionImportReturnType returnType) {
		super(fp);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
		this.returnType = returnType;
	}

	/**
	 * Returns if a return type can be added(The feature can be executed). In
	 * this case this has to be done only in case of Function Imports.
	 * 
	 * @param context
	 *            ICustomContext instance is passed
	 */
	@Override
	public boolean canExecute(ICustomContext context) {
		boolean ret = false;
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof FunctionImport
					&& ((FunctionImport) bo).getReturnType() == null) {
				ret = true;
			}
		}
		return ret;
	}

	@Override
	public void execute(ICustomContext customContext) {
		AddContext addContext;
		PictogramElement[] pes = customContext.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof FunctionImport) {
				PictogramElement mainPictogramElement = pes[0];
				FunctionImport funcImp = (FunctionImport) bo;
				EList<Shape> children = ((ContainerShape) mainPictogramElement)
						.getChildren();
				IdentifierShape identifierShape = null;
				ContainerShape sectionShape;
				Shape titleImage;
				for (Shape shape : children) {
					/*
					 * Create new return type and its shape in the specified
					 * area
					 */
					if (PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_RECTANGLE
							.equals(PropertyUtil.getID(shape))) {
						sectionShape = (ContainerShape) shape;
						returnTypeUsage = this.artifactFactory
								.createReturnType(funcImp, this.returnType, null);
						addContext = new AddContext();
						addContext.setNewObject(returnTypeUsage);

						/* Visual shape */
						identifierShape = this.shapesFactory
								.createIdentifierShape(
										sectionShape,
										getNameForReturnType(),
										getImageForReturnType(),
										addContext,
										returnTypeUsage,
										false,
										PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME);
						titleImage = ODataLayoutUtil
								.expandSection(sectionShape);
						if (titleImage != null) {
							ODataShapeUtil.updatePictogramElement(titleImage,
									getFeatureProvider());
						}

						/* Update the layout */
						ODataShapeUtil.layoutPictogramElement(
								mainPictogramElement, getFeatureProvider());

						if (returnTypeUsage != null) {
							// in order to refresh the Grphiti view
							IODataDiagramCreator.INSTANCE.refreshEditor(
									returnTypeUsage, null, returnTypeUsage);
						}
						identifierShape.updateSelection();
						identifierShape.enableDirectEdit();
						break;
					}
				}

			}
		}

	}

	@Override
	public String getName() {
		return returnType.getDisplayName();
	}

	@Override
	public String getDescription() {
		return returnType.getDescription();
	}

	@Override
	public String getImageId() {
		return returnType.getImageID();
	}

	@Override
	public boolean isAvailable(IContext context) {
		return true;
	}

	/**
	 * Creates a unique name for the new return type
	 * 
	 * @return String new unique name
	 */
	private String getNameForReturnType() {

		String name = null;
		if (this.returnTypeUsage != null) {
			if (this.returnTypeUsage instanceof SimpleTypeUsage) {
				name = ((SimpleTypeUsage) this.returnTypeUsage).getSimpleType()
						.getType().getLiteral();
			} else if (this.returnTypeUsage instanceof ComplexTypeUsage) {
				if (((ComplexTypeUsage) this.returnTypeUsage).getComplexType() != null)
					name = ((ComplexTypeUsage) this.returnTypeUsage)
							.getComplexType().getName();
			} else if (this.returnTypeUsage instanceof EnumTypeUsage) {
				if (((EnumTypeUsage) this.returnTypeUsage).getEnumType() != null)
					name = ((EnumTypeUsage) this.returnTypeUsage).getEnumType()
							.getName();
			} else if (this.returnTypeUsage instanceof ReturnEntityTypeUsage) {
				if (((ReturnEntityTypeUsage) this.returnTypeUsage)
						.getEntityType() != null)
					name = ((ReturnEntityTypeUsage) this.returnTypeUsage)
							.getEntityType().getName();
			}
		}
		if (name == null) {
			name = Messages.ODATAEDITOR_UNDEFINED_NAME;
		}
		return name;

	}

	/**
	 * Returns the image path
	 * 
	 * @return String image path
	 */
	private String getImageForReturnType() {
		String imageId = null;
		if (ArtifactUtil.isCollectionReturnType(this.returnTypeUsage)) {
			if (this.returnTypeUsage instanceof SimpleTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_PROPERTY_COLLN;
			} else if (this.returnTypeUsage instanceof ComplexTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_CPLX_TYP_COLLN;
			} else if (this.returnTypeUsage instanceof EnumTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_ENUM_COLLN;
			} else if (this.returnTypeUsage instanceof ReturnEntityTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_ENTY_SET_COLLN;
			}
		} else {
			if (this.returnTypeUsage instanceof SimpleTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_PROPERTY;
			} else if (this.returnTypeUsage instanceof ComplexTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_CMPLX_TYPE;
			} else if (this.returnTypeUsage instanceof EnumTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_ENUM;
			} else if (this.returnTypeUsage instanceof ReturnEntityTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_ENTITY;
			}
		}
		return imageId;
	}
}
