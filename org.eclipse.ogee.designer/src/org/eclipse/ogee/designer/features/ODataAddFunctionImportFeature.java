/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.List;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.BasicFunctionImportShape;
import org.eclipse.ogee.designer.shapes.ExtensionArtifactShape;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.SimpleTypeUsageImpl;

public class ODataAddFunctionImportFeature extends AbstractAddShapeFeature {

	/* artifact factory */
	ArtifactFactory artifactFactory;
	/* shapes factory */
	ShapesFactory shapesFactory;

	List<Property> propertyList;
	/**
	 * Name and Icon group of the Function Import
	 */
	private IdentifierShape functionImportNameIdentifier;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactFactory
	 *            ArtifactFactory instance is passed
	 * @param shapesFactory
	 *            ShapesFactory instance is passed
	 */
	public ODataAddFunctionImportFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		super(fp);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
	}

	/**
	 * Returns true if the target is the Diagram as the function import can only
	 * be added on a Diagram
	 * 
	 * @param context
	 *            ICreateContext instance
	 * @return boolean true if function import can be added
	 */
	@Override
	public boolean canAdd(IAddContext context) {

		return ArtifactUtil.isFunctionImport(context.getNewObject())
				&& context.getTargetContainer() instanceof Diagram;
	}

	/**
	 * This adds the shape to the diagram after drag and drop from the palette
	 * 
	 * @param context
	 *            IAddContext instance
	 * @return PictogramElement the main container
	 */
	@Override
	public PictogramElement add(IAddContext context) {

		// Target Diagram
		Diagram targetDiagram = (Diagram) context.getTargetContainer();

		// Create the Artifact (Outer Rectangle)
		BasicFunctionImportShape artifactshape = this.shapesFactory
				.createFunctionImportShape(targetDiagram, context);

		PictogramElement shapes[] = artifactshape.getPictogramElements();
		ContainerShape functionImportcontainer = (ContainerShape) shapes[0];

		// Add elements to the Outer Shell
		addName(functionImportcontainer, context); // Add the Entity Name
		addParameters(functionImportcontainer, context); // Add the Parameters
		addReturnType(functionImportcontainer, context); // Add the return types

		// call the layout feature
		ODataShapeUtil.layoutPictogramElement(functionImportcontainer,
				getFeatureProvider());

		// As soon as entity is added to the diagram Name should be made
		// editable
		this.functionImportNameIdentifier.enableDirectEdit();

		return functionImportcontainer;
	}

	/**
	 * Adds the Name of the FunctionImport to the Container
	 * 
	 * @param functionImportcontainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addName(ContainerShape functionImportcontainer,
			IAddContext context) {
		Object businessObject = context.getNewObject();
		this.functionImportNameIdentifier = this.shapesFactory
				.createIdentifierShape(functionImportcontainer,
						ArtifactUtil.getFunctionImportName(businessObject),
						ODataImageProvider.IMG_FUNC_IMP, context,
						businessObject, true, PropertyUtil.FUNCTION_IMPORT_NAME);
		this.shapesFactory
				.createPolyLineShape(functionImportcontainer, context);
	}

	/**
	 * Adds the parameters to the function import container
	 * 
	 * @param functionImportcontainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addParameters(ContainerShape functionImportcontainer,
			IAddContext context) {

		FunctionImport functionImport = (FunctionImport) context.getNewObject();
		List<Parameter> parameters = functionImport.getParameters();

		boolean isExpanded = (parameters != null && parameters.size() > 0) ? true
				: false;

		this.shapesFactory.createCollapsableTitleShape(functionImportcontainer,
				Messages.ODATAEDITOR_PARAMETERS_NAME, context, isExpanded);
		ExtensionArtifactShape invisibleRectangle = this.shapesFactory
				.createCollapsableRectangle(functionImportcontainer, context,
						PropertyUtil.FUNCTION_IMPORT_PARAMETER_RECTANGLE,
						isExpanded);
		ContainerShape container = ((ContainerShape) invisibleRectangle
				.getPictogramElements()[0]);

		if (parameters != null) {
			for (Parameter parameter : parameters) {
				this.shapesFactory.createIdentifierShape(container,
						ArtifactUtil.getParameterName(parameter),
						ODataImageProvider.IMG_PARAMETER, context, parameter,
						false, PropertyUtil.FUNCTION_IMPORT_PARAMETER_NAME);
			}
		}

		this.shapesFactory
				.createPolyLineShape(functionImportcontainer, context);
	}

	/**
	 * Adds the return types to the container
	 * 
	 * @param functionImportcontainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addReturnType(ContainerShape functionImportcontainer,
			IAddContext context) {

		FunctionImport functionImport = (FunctionImport) context.getNewObject();
		IFunctionReturnTypeUsage returnType = functionImport.getReturnType();

		boolean isExpanded = returnType != null ? true : false;

		this.shapesFactory.createCollapsableTitleShape(functionImportcontainer,
				Messages.ODATAEDITOR_RETURN_TYPE_NAME, context, isExpanded);
		ExtensionArtifactShape invisibleRectangle = this.shapesFactory
				.createCollapsableRectangle(functionImportcontainer, context,
						PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_RECTANGLE,
						isExpanded);
		ContainerShape container = ((ContainerShape) invisibleRectangle
				.getPictogramElements()[0]);

		if (returnType != null) {

			if (ArtifactUtil.isCollectionReturnType(returnType)) {
				if (returnType instanceof SimpleTypeUsageImpl) {
					this.shapesFactory.createIdentifierShape(container,
							ArtifactUtil.getReturnTypeName(returnType),
							ODataImageProvider.IMG_PROPERTY_COLLN, context,
							returnType, false,
							PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME);
				} else if (returnType instanceof ComplexTypeUsageImpl) {
					this.shapesFactory.createIdentifierShape(container,
							ArtifactUtil.getReturnTypeName(returnType),
							ODataImageProvider.IMG_CPLX_TYP_COLLN, context,
							returnType, false,
							PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME);
				} else if (returnType instanceof EnumTypeUsageImpl) {
					this.shapesFactory.createIdentifierShape(container,
							ArtifactUtil.getReturnTypeName(returnType),
							ODataImageProvider.IMG_ENUM_COLLN, context,
							returnType, false,
							PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME);
				} else if (returnType instanceof ReturnEntityTypeUsageImpl) {
					this.shapesFactory.createIdentifierShape(container,
							ArtifactUtil.getReturnTypeName(returnType),
							ODataImageProvider.IMG_ENTY_SET_COLLN, context,
							returnType, false,
							PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME);
				}

			} else {

				if (returnType instanceof SimpleTypeUsageImpl) {
					this.shapesFactory.createIdentifierShape(container,
							ArtifactUtil.getReturnTypeName(returnType),
							ODataImageProvider.IMG_PROPERTY, context,
							returnType, false,
							PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME);
				} else if (returnType instanceof ComplexTypeUsageImpl) {
					this.shapesFactory.createIdentifierShape(container,
							ArtifactUtil.getReturnTypeName(returnType),
							ODataImageProvider.IMG_CMPLX_TYPE, context,
							returnType, false,
							PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME);
				} else if (returnType instanceof EnumTypeUsageImpl) {
					this.shapesFactory.createIdentifierShape(container,
							ArtifactUtil.getReturnTypeName(returnType),
							ODataImageProvider.IMG_ENUM, context, returnType,
							false,
							PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME);
				} else if (returnType instanceof ReturnEntityTypeUsageImpl) {
					this.shapesFactory.createIdentifierShape(container,
							ArtifactUtil.getReturnTypeName(returnType),
							ODataImageProvider.IMG_ENTITY, context, returnType,
							false,
							PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME);
				}
			}

		}
	}

	@Override
	public boolean canUndo(IContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDoneChanges() {
		// TODO Auto-generated method stub
		return false;
	}

}