/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.layout.ILayout;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.utils.logger.Logger;

/**
 * This class will be called from the visualizer class and will use current
 * layout algorithm to visualize the static OData models. The models can be
 * opened from an EDMX file, a URI or from the service catalog in Eclipse.
 * 
 */
public class ODataLayoutFeature extends AbstractCustomFeature {

	/**
	 * The Current Layout selected in Eclipse Preferences or from context menu
	 */
	private ILayout currentLayout;

	/**
	 * @param fp
	 * @param currentLayout
	 */
	public ODataLayoutFeature(IFeatureProvider fp, ILayout currentLayout) {
		super(fp);
		this.currentLayout = currentLayout;
	}

	@Override
	public String getDescription() {
		return Messages.ODATAEDITOR_LAYOUT_DESC;
	}

	@Override
	public String getName() {
		return Messages.ODATAEDITOR_LAYOUT_NAME;
	}

	/**
	 * Execute the feature if there are elements that need to be displayed and
	 * if the Diagram exists
	 */
	@Override
	public boolean canExecute(ICustomContext context) {
		PictogramElement[] pes = context.getPictogramElements();
		return this.currentLayout.canLayout(pes);
	}

	/**
	 * Gets all the objects (entities, complex types, associations and function
	 * imports) and applies a current layout algorithm to create the
	 * visualization of the static model.
	 * 
	 * @param context
	 *            ICustomContext instance is passed
	 */
	@Override
	public void execute(ICustomContext context) {

		PictogramElement[] pes = context.getPictogramElements();
		try {
			this.currentLayout.layout(getFeatureProvider(), pes);
		} catch (Exception e1) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e1);
			// In case the custom layout has any exception, fall back to default
			// layout.
			this.currentLayout = ODataLayoutUtil
					.getCurrentLayout(IODataEditorConstants.DEFAULT_LAYOUT_ID);
			try {
				this.currentLayout.layout(getFeatureProvider(), pes);
			} catch (Exception e2) {
				Logger.getLogger(Activator.PLUGIN_ID).logError(e2);
			}
		}
	}

}
