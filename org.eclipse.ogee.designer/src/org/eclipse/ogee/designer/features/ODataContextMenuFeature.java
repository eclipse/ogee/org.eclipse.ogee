/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.EnumSet;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.ogee.designer.contextmenu.IBindingContextMenu;
import org.eclipse.ogee.designer.contextmenu.IContextMenu;
import org.eclipse.ogee.designer.contextmenu.UIElementType;
import org.eclipse.ogee.designer.messages.Messages;

/**
 * This class will be used for Context Menu implementation.
 * 
 */
public class ODataContextMenuFeature extends AbstractCustomFeature {

	/**
	 * The Current Context Menu selected
	 */
	private IContextMenu currentContextMenu;
	private EObject businessObject;
	private PictogramElement pe ;

	/**
	 * @param fp
	 * @param currentContextMenu
	 */
	public ODataContextMenuFeature(DiagramEditor editor ,
			IContextMenu currentContextMenu, EObject businessObject) {
		super(editor.getDiagramTypeProvider().getFeatureProvider());
		this.currentContextMenu = currentContextMenu;
		this.businessObject = businessObject;
		this.pe = editor.getDiagramTypeProvider().getFeatureProvider().getPictogramElementForBusinessObject(businessObject);
	}


	@Override
	public String getDescription() {
		return Messages.ODATAEDITOR_CONTEXT_MENU_DESCRIPTION;
	}

	@Override
	public String getName() {
		return Messages.ODATAEDITOR_CONTEXT_MENU;
	}

	/**
	 * To return all UIElement set of diagram types to be implemented
	 * 
	 * @return
	 */
	public EnumSet<UIElementType> getElements() {
		EnumSet<UIElementType> elementSet = this.currentContextMenu
				.getApplicableElements();
		return elementSet;
	}

	/**
	 * Execute the feature if there are elements that need to be displayed
	 * 
	 */
	@Override
	public boolean canExecute(ICustomContext context) {
		return true;
	}

	/**
	 * Execute the action of the context menu selected
	 * 
	 * @param context
	 *            ICustomContext instance is passed
	 */
	@Override
	public void execute(ICustomContext context) {
		this.currentContextMenu.menuAction(businessObject);
		if(currentContextMenu instanceof IBindingContextMenu){
			((IBindingContextMenu) currentContextMenu).setBusinessObject(this.businessObject);
			((IBindingContextMenu) currentContextMenu).setPictogramElement(this.pe);
		}
	}

}
