/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IDirectEditingInfo;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.utils.PropertyUtil;

public class ODataDoubleClickFeature extends AbstractCustomFeature {

	/**
	 * @param fp
	 */
	public ODataDoubleClickFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public String getName() {
		return Messages.ODATAEDITOR_DIRECTEDITING;
	}

	@Override
	public boolean canExecute(ICustomContext context) {

		boolean canExecute = false;
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			PictogramElement pe = pes[0];
			canExecute = PropertyUtil.isTopContainer(pe)
					&& pe instanceof ContainerShape;

		}
		return canExecute;
	}

	@Override
	public void execute(ICustomContext context) {
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			PictogramElement pe = pes[0];
			if (pe instanceof ContainerShape && PropertyUtil.isTopContainer(pe)) {
				EList<Shape> children = ((ContainerShape) pe).getChildren();
				if (children.size() > 0) {
					ContainerShape firstChild = (ContainerShape) children
							.get(0);
					if (PropertyUtil.isTypeIdentifier(firstChild)) {
						EList<Shape> subchildren = firstChild.getChildren();
						PictogramElement textShape = subchildren.size() == 2 ? subchildren
								.get(1) : null;
						if (textShape != null) {
							IDirectEditingInfo directEditingInfo = getFeatureProvider()
									.getDirectEditingInfo();
							directEditingInfo
									.setMainPictogramElement(firstChild);
							directEditingInfo.setPictogramElement(textShape);
							directEditingInfo.setGraphicsAlgorithm(textShape
									.getGraphicsAlgorithm());
							directEditingInfo.setActive(true);
							getFeatureProvider().getDiagramTypeProvider()
									.getDiagramBehavior().refresh();
						}
					}

				}

			}

		}
	}

}
