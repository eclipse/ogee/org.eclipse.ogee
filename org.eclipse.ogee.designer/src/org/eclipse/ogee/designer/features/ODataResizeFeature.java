/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.impl.DefaultResizeShapeFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;

public class ODataResizeFeature extends DefaultResizeShapeFeature {

	public ODataResizeFeature(IFeatureProvider fp) {
		super(fp);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean canResizeShape(IResizeShapeContext context) {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * resizes the shape
	 * 
	 * @param context
	 *            IResizeShapeContext
	 */
	@Override
	public void resizeShape(IResizeShapeContext context) {
		// Inner rectangle heights
		ODataLayoutUtil.updateContainerHeights(
				(ContainerShape) context.getPictogramElement(),
				context.getHeight());
		super.resizeShape(context);
	}

}
