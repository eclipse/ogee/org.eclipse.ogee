/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.List;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.BasicArtifactShape;
import org.eclipse.ogee.designer.shapes.ExtensionArtifactShape;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl;

public class ODataAddEntityTypeFeature extends AbstractAddShapeFeature {

	/* artifact factory */
	ArtifactFactory artifactFactory;
	/* shapes factory */
	ShapesFactory shapesFactory;

	List<Property> propertyList;
	/**
	 * Name and Icon group of the Entity Type
	 */
	private IdentifierShape entityNameIdentifier;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactFactory
	 *            ArtifactFactory instance is passed
	 * @param shapesFactory
	 *            ShapesFactory instance is passed
	 */
	public ODataAddEntityTypeFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		super(fp);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
	}

	/**
	 * Returns true if the target is the Diagram as the Entity can only be added
	 * on a Diagram
	 * 
	 * @param context
	 *            ICreateContext instance
	 * @return boolean true if Entity can be added
	 */
	@Override
	public boolean canAdd(IAddContext context) {

		return ArtifactUtil.isEntityType(context.getNewObject())
				&& context.getTargetContainer() instanceof Diagram;
	}

	/**
	 * This adds the shape to the diagram after drag and drop from the palette
	 * 
	 * @param context
	 *            IAddContext instance
	 * @return PictogramElement the main container
	 */
	@Override
	public PictogramElement add(IAddContext context) {

		// Target Diagram
		Diagram targetDiagram = (Diagram) context.getTargetContainer();

		// Create the Artifact (Outer Rectangle)
		BasicArtifactShape artifactshape = this.shapesFactory
				.createArtifactShape(targetDiagram, context);

		PictogramElement shapes[] = artifactshape.getPictogramElements();
		ContainerShape entitytypecontainer = (ContainerShape) shapes[0];

		// Add elements to the Outer Shell
		addName(entitytypecontainer, context); // Add the Entity Name
		addEntitySets(entitytypecontainer, context); // Add the Entity Sets
		addProperties(entitytypecontainer, context); // Add the Entity
														// properties
		addNavigationProperties(entitytypecontainer, context); // Add the Entity
																// navigation
																// properties

		// call the layout feature
		ODataShapeUtil.layoutPictogramElement(entitytypecontainer,
				getFeatureProvider());

		// As soon as entity is added to the diagram Name should be made
		// editable
		this.entityNameIdentifier.enableDirectEdit();

		return entitytypecontainer;
	}

	/**
	 * Adds the Name of the Entity Type to the Container
	 * 
	 * @param entitytypecontainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addName(ContainerShape entitytypecontainer, IAddContext context) {
		Object businessObject = context.getNewObject();
		this.entityNameIdentifier = this.shapesFactory.createIdentifierShape(
				entitytypecontainer,
				ArtifactUtil.getEntityTypeName(businessObject),
				ODataImageProvider.IMG_ENTITY, context, businessObject, true,
				PropertyUtil.ENTITY_NAME);
		this.shapesFactory.createPolyLineShape(entitytypecontainer, context);
	}

	/**
	 * Adds the default Entity Set to the container
	 * 
	 * @param entitytypecontainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addEntitySets(ContainerShape entitytypecontainer,
			IAddContext context) {
		EntityType entityType = (EntityType) context.getNewObject();
		List<EntitySet> entitySets = entityType.getEntitySets();
		boolean isExpanded = entitySets != null && entitySets.size() > 0 ? true
				: false;
		this.shapesFactory.createCollapsableTitleShape(entitytypecontainer,
				Messages.ODATAEDITOR_ENTITYSET_NAME, context, isExpanded);
		ExtensionArtifactShape invisibleRectangle = this.shapesFactory
				.createCollapsableRectangle(entitytypecontainer, context,
						PropertyUtil.ENTITYSET_RECTANGLE, isExpanded);
		ContainerShape container = ((ContainerShape) invisibleRectangle
				.getPictogramElements()[0]);
		if (entitySets != null) {
			for (EntitySet entitySet : entitySets) {
				this.shapesFactory.createIdentifierShape(container,
						ArtifactUtil.getEntitySetName(entitySet),
						ODataImageProvider.IMG_ENTITY_SET, context, entitySet,
						false, PropertyUtil.ENTITY_SET_NAME);
			}
		}

		this.shapesFactory.createPolyLineShape(entitytypecontainer, context);
	}

	/**
	 * Adds the properties to the container
	 * 
	 * @param entitytypecontainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addProperties(ContainerShape entitytypecontainer,
			IAddContext context) {

		EntityType entityType = (EntityType) context.getNewObject();
		List<Property> keyProperties = entityType.getKeys();

		List<Property> properties = entityType.getProperties();
		boolean isExpanded = (keyProperties != null && keyProperties.size() > 0)
				|| (properties != null && properties.size() > 0) ? true : false;

		this.shapesFactory.createCollapsableTitleShape(entitytypecontainer,
				Messages.ODATAEDITOR_PROPERTIES_NAME, context, isExpanded);
		ExtensionArtifactShape invisibleRectangle = this.shapesFactory
				.createCollapsableRectangle(entitytypecontainer, context,
						PropertyUtil.PROPERTY_RECTANGLE, isExpanded);
		ContainerShape container = ((ContainerShape) invisibleRectangle
				.getPictogramElements()[0]);

		if (keyProperties != null) {
			for (Property keyProperty : keyProperties) {
				this.shapesFactory.createPropertyShape(container,
						ArtifactUtil.getPropertyName(keyProperty),
						ODataImageProvider.IMG_KEY_PROPERTY, context,
						keyProperty, keyProperty.getType(), false,
						PropertyUtil.ENTITY_PROPERTY_NAME);
			}
		}

		if (properties != null) {
			for (Property property : properties) {
				IPropertyTypeUsage type = property.getType();
				if (type instanceof ComplexTypeUsageImpl) {
					this.shapesFactory.createPropertyShape(container,
							ArtifactUtil.getPropertyName(property),
							ODataImageProvider.IMG_CMPLX_TYPE, context,
							property, property.getType(), false,
							PropertyUtil.ENTITY_PROPERTY_NAME);
				} else if (type instanceof EnumTypeUsageImpl) {
					this.shapesFactory.createPropertyShape(container,
							ArtifactUtil.getPropertyName(property),
							ODataImageProvider.IMG_ENUM, context, property,
							property.getType(), false,
							PropertyUtil.ENTITY_PROPERTY_NAME);
				} else {
					this.shapesFactory.createPropertyShape(container,
							ArtifactUtil.getPropertyName(property),
							ODataImageProvider.IMG_PROPERTY, context, property,
							property.getType(), false,
							PropertyUtil.ENTITY_PROPERTY_NAME);
				}

			}

		}

		this.shapesFactory.createPolyLineShape(entitytypecontainer, context);
	}

	/**
	 * Adds the navigation properties to the container
	 * 
	 * @param entitytypecontainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addNavigationProperties(ContainerShape entitytypecontainer,
			IAddContext context) {

		EntityType entityType = (EntityType) context.getNewObject();
		List<NavigationProperty> navProperties = entityType
				.getNavigationProperties();

		boolean isExpanded = (navProperties != null && navProperties.size() > 0) ? true
				: false;

		this.shapesFactory.createCollapsableTitleShape(entitytypecontainer,
				Messages.ODATAEDITOR_NAVIGATION_PROPERTIES_NAME, context,
				isExpanded);
		ExtensionArtifactShape invisibleRectangle = this.shapesFactory
				.createCollapsableRectangle(entitytypecontainer, context,
						PropertyUtil.NAVIGATIONPROPERTY_RECTANGLE, isExpanded);

		ContainerShape container = ((ContainerShape) invisibleRectangle
				.getPictogramElements()[0]);

		if (navProperties != null) {
			for (NavigationProperty navProperty : navProperties) {
				this.shapesFactory.createIdentifierShape(container,
						ArtifactUtil.getNavPropertyName(navProperty),
						ODataImageProvider.IMG_NAV_PROPERTY, context,
						navProperty, false,
						PropertyUtil.NAVIGATION_PROPERTY_NAME);
			}
		}

	}

	@Override
	public boolean canUndo(IContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDoneChanges() {
		// TODO Auto-generated method stub
		return false;
	}

}