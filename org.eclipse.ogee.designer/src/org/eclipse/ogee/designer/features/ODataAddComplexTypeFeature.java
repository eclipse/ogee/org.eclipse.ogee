/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.List;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.BasicComplexTypeShape;
import org.eclipse.ogee.designer.shapes.ExtensionArtifactShape;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl;

public class ODataAddComplexTypeFeature extends AbstractAddShapeFeature {

	/* artifact factory */
	ArtifactFactory artifactFactory;
	/* shapes factory */
	ShapesFactory shapesFactory;

	List<Property> propertyList;
	/**
	 * Name and Icon group of the Complex Type
	 */
	private IdentifierShape complexTypeNameIdentifier;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactFactory
	 *            ArtifactFactory instance is passed
	 * @param shapesFactory
	 *            ShapesFactory instance is passed
	 */
	public ODataAddComplexTypeFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		super(fp);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
	}

	public boolean canAdd(IAddContext context) {

		return ArtifactUtil.isComplexType(context.getNewObject())
				&& context.getTargetContainer() instanceof Diagram;

	}

	@Override
	public PictogramElement add(IAddContext context) {
		// Target Diagram
		Diagram targetDiagram = (Diagram) context.getTargetContainer();

		// Create the Artifact (Outer Rectangle)
		BasicComplexTypeShape artifactshape = this.shapesFactory
				.createComplexTypeShape(targetDiagram, context);

		PictogramElement shapes[] = artifactshape.getPictogramElements();
		ContainerShape complexTypeContainer = (ContainerShape) shapes[0];

		// Add elements to the Outer Shell
		addName(complexTypeContainer, context); // Add the Complex Type Name
		addProperties(complexTypeContainer, context); // Add the Complex Type
														// Properties
		// call the layout feature
		ODataShapeUtil.layoutPictogramElement(complexTypeContainer,
				getFeatureProvider());

		// As soon as Complex Type is added to the diagram, Name should be made
		// editable
		this.complexTypeNameIdentifier.enableDirectEdit();

		return complexTypeContainer;
	}

	/**
	 * Adds the Name of the Complex Type to the Container
	 * 
	 * @param complexTypeContainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addName(ContainerShape complexTypeContainer,
			IAddContext context) {
		Object businessObject = context.getNewObject();
		this.complexTypeNameIdentifier = this.shapesFactory
				.createIdentifierShape(complexTypeContainer,
						ArtifactUtil.getComplexTypeName(businessObject),
						ODataImageProvider.IMG_CMPLX_TYPE, context,
						businessObject, true, PropertyUtil.COMPLEX_NAME);
		this.shapesFactory.createPolyLineShape(complexTypeContainer, context);
	}

	/**
	 * Adds the properties to the container
	 * 
	 * @param complexTypeContainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addProperties(ContainerShape complexTypeContainer,
			IAddContext context) {

		ComplexType complexType = (ComplexType) context.getNewObject();
		List<Property> properties = complexType.getProperties();

		boolean isExpanded = (properties != null && properties.size() > 0) ? true
				: false;

		this.shapesFactory.createCollapsableTitleShape(complexTypeContainer,
				Messages.ODATAEDITOR_PROPERTIES_NAME, context, isExpanded);
		ExtensionArtifactShape invisibleRectangle = this.shapesFactory
				.createCollapsableRectangle(complexTypeContainer, context,
						PropertyUtil.PROPERTY_RECTANGLE, isExpanded);
		ContainerShape container = ((ContainerShape) invisibleRectangle
				.getPictogramElements()[0]);

		if (properties != null) {
			for (Property property : properties) {
				IPropertyTypeUsage type = property.getType();
				if (type instanceof ComplexTypeUsageImpl) {
					this.shapesFactory.createPropertyShape(container,
							ArtifactUtil.getPropertyName(property),
							ODataImageProvider.IMG_CMPLX_TYPE, context,
							property, property.getType(), false,
							PropertyUtil.ENTITY_PROPERTY_NAME);
				} else if (type instanceof EnumTypeUsageImpl) {
					this.shapesFactory.createPropertyShape(container,
							ArtifactUtil.getPropertyName(property),
							ODataImageProvider.IMG_ENUM, context, property,
							property.getType(), false,
							PropertyUtil.ENTITY_PROPERTY_NAME);
				} else {
					this.shapesFactory.createPropertyShape(container,
							ArtifactUtil.getPropertyName(property),
							ODataImageProvider.IMG_PROPERTY, context, property,
							property.getType(), false,
							PropertyUtil.ENTITY_PROPERTY_NAME);
				}

			}
		}

	}

	@Override
	public boolean canUndo(IContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDoneChanges() {
		// TODO Auto-generated method stub
		return false;
	}

}