/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.utils.ArtifactNameUtil;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;

/**
 * This class is used to add entity set in the context menu of the entity type.
 * On clicking on the option entity set of the context menu, one entity set will
 * be added under "Entity Sets" with some proposed name depending on the entity
 * name.
 * 
 * 
 */
public class ODataAddEntitySetFeature extends AbstractCustomFeature {

	/* artifact factory */
	ArtifactFactory artifactFactory;
	/* shapes factory */
	ShapesFactory shapesFactory;

	List<Property> propertyList;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactFactory
	 *            ArtifactFactory instance is passed
	 * @param shapesFactory
	 *            ShapesFactory instance is passed
	 */
	public ODataAddEntitySetFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		super(fp);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
	}

	/**
	 * Returns if a property can be added(The feature can be executed). In this
	 * case this has to be done only in case of Entity Types or Complex Types.
	 * 
	 * @param context
	 *            ICustomContext instance is passed
	 */
	@Override
	public boolean canExecute(ICustomContext context) {
		boolean ret = false;
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof EntityType) {
				ret = true;
			}
		}
		return ret;
	}

	@Override
	public String getName() {
		return Messages.ODATAEDITOR_ENTITYSET_MENU;
	}

	@Override
	public String getDescription() {
		return Messages.ODATAEDITOR_ENTITYSET_MENU_DESC;
	}

	@Override
	public String getImageId() {
		return ODataImageProvider.IMG_ENTITY_SET;
	}

	@Override
	public boolean isAvailable(IContext context) {
		return true;
	}

	/**
	 * Check for user input and create the new entity sets
	 * 
	 * @see org.eclipse.graphiti.features.custom.ICustomFeature#execute(org.eclipse.graphiti.features.context.ICustomContext)
	 */
	@Override
	public void execute(ICustomContext context) {

		AddContext addContext;

		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof EntityType) {
				PictogramElement mainPictogramElement = pes[0];
				EntityType etype = (EntityType) bo;

				String eSetName = getEntitySetName(etype);
				if (eSetName != null && eSetName.trim().length() != 0) {
					EList<Shape> children = ((ContainerShape) mainPictogramElement)
							.getChildren();
					ContainerShape sectionShape;
					Shape titleImage;
					IdentifierShape identifierShape;
					for (Shape shape : children) {
						/*
						 * Create new entity set and its shape in the specified
						 * area
						 */
						if (PropertyUtil.ENTITYSET_RECTANGLE
								.equals(PropertyUtil.getID(shape))) {

							sectionShape = (ContainerShape) shape;
							EntitySet eSet = this.artifactFactory
									.createEntitySet(etype, eSetName);
							addContext = new AddContext();
							addContext.setNewObject(eSet);

							/* Visual shape */
							identifierShape = this.shapesFactory
									.createIdentifierShape(sectionShape,
											eSetName,
											ODataImageProvider.IMG_ENTITY_SET,
											addContext, eSet, false,
											PropertyUtil.ENTITY_SET_NAME);

							titleImage = ODataLayoutUtil
									.expandSection(sectionShape);
							if (titleImage != null) {
								ODataShapeUtil.updatePictogramElement(
										titleImage, getFeatureProvider());

							}
							/* Update the layout */
							ODataShapeUtil.layoutPictogramElement(
									mainPictogramElement, getFeatureProvider());

							identifierShape.updateSelection();
							identifierShape.enableDirectEdit();
							break;
						}

					}
				}

			}
		}
	}

	/**
	 * This method is used to get the entity set name. It will provide the name
	 * proposal for entity set
	 */
	private String getEntitySetName(EntityType etype) {
		Schema schema = this.artifactFactory.getSchema();
		String name = null;
		int count;
		String entityName = etype.getName();
		List<String> entitySetNames = new ArrayList<String>();

		/*
		 * for (ListIterator<EntitySet> it = entitySets.listIterator(); it
		 * .hasNext();) { es = it.next(); esn = es.getName();
		 * entitySetNames.add(esn); count++; }
		 */
		entitySetNames = ArtifactUtil.getEntSetAssocSetFuncImpNames(schema);
		count = entitySetNames.size();
		// Entity Name + Set e.g. CustomerSet
		name = entityName + Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME;
		entitySetNames.add(entityName
				+ Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME + count);
		name = ArtifactNameUtil.generateUniqueName(entitySetNames, entityName,
				(entityName + Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME),
				Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME,
				IODataEditorConstants.MAX_LENGTH);
		entitySetNames.add(name);
		return name;

	}

	/**
	 * This method is used to get the name proposal for entity set
	 * 
	 * @param name
	 * @param uniqueEntitySetID
	 * @param numberList
	 * @param associations
	 * @return
	 */
	/*
	 * private String getNameProposal(String entityName, String name,
	 * EList<EntitySet> entitySets) {
	 * 
	 * final String onlyCharacter = "[a-zA-Z]"; //$NON-NLS-1$ int
	 * uniqueEntitySetID = 1;
	 * 
	 * List<Integer> numberList = new ArrayList<Integer>(); String newName =
	 * name;
	 * 
	 * Pattern nonChar = Pattern.compile(onlyCharacter,
	 * Pattern.CASE_INSENSITIVE);
	 * 
	 * for (Iterator<EntitySet> iterator = entitySets.iterator(); iterator
	 * .hasNext();) { numberList = getNextNumberForName(entityName, name,
	 * numberList, nonChar, iterator);
	 * 
	 * }
	 * 
	 * for (Iterator<Integer> iterator1 = numberList.iterator(); iterator1
	 * .hasNext();) {
	 * 
	 * if (!numberList.contains(Integer.valueOf(uniqueEntitySetID))) { if
	 * (uniqueEntitySetID != 1) { newName = name + uniqueEntitySetID; } break; }
	 * uniqueEntitySetID++; } return newName; }
	 */

	/**
	 * @param baseName
	 * @param name
	 * @param numberList
	 * @param trunEntityName
	 * @param nonChar
	 * @param iterator
	 * @return
	 */
	/*
	 * public List<Integer> getNextNumberForName(String baseName, String name,
	 * List<Integer> numberList, Pattern nonChar, Iterator<EntitySet> iterator)
	 * {
	 * 
	 * String trunObjectName; EntitySet prevObjectSet; String prevObjcetSetName;
	 * int indx; String objectSetsubString; String objectSetNumber; int index;
	 * boolean b;
	 * 
	 * prevObjectSet = iterator.next(); prevObjcetSetName =
	 * prevObjectSet.getName(); if (name.equalsIgnoreCase(prevObjcetSetName)) {
	 * numberList.add(Integer.valueOf(1)); return numberList; }
	 * 
	 * trunObjectName = baseName;
	 * 
	 * if (prevObjcetSetName.startsWith(trunObjectName +
	 * Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME)) {
	 * 
	 * indx = prevObjcetSetName.indexOf(trunObjectName);
	 * 
	 * if (indx != -1) {
	 * 
	 * objectSetsubString = prevObjcetSetName.substring(indx + name.length());
	 * 
	 * Matcher m = nonChar.matcher(objectSetsubString); b = m.find(); if
	 * (objectSetsubString.trim().length() > 0 && b == false) {
	 * 
	 * objectSetNumber = objectSetsubString;
	 * 
	 * try {
	 * 
	 * index = Integer.parseInt(objectSetNumber); // Added 1 to get next value
	 * to calculate the part // of the entity name
	 * numberList.add(Integer.valueOf(index));
	 * 
	 * } catch (NumberFormatException e) { // Digest...don't log } } }
	 * 
	 * } return numberList; }
	 */

}
