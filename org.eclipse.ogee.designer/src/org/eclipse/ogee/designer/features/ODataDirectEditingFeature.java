/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.impl.AbstractDirectEditingFeature;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.swt.widgets.Display;

/**
 * This class will provide the editing functionality on text elements of an
 * artifact
 * 
 */
public class ODataDirectEditingFeature extends AbstractDirectEditingFeature {

	/**
	 * Structural Feature Name for "Name"
	 */
	public static final String NAME_STRFEATURE = "name"; //$NON-NLS-1$

	public static final String EMPTY_STRING = ""; //$NON-NLS-1$

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactfactory
	 *            Artifact factory instance is passed
	 * @param shapesFactory
	 *            Shapes factory instance is passed
	 */
	public ODataDirectEditingFeature(IFeatureProvider fp,
			ArtifactFactory artifactfactory, ShapesFactory shapesFactory) {
		super(fp);
	}

	/**
	 * Specifies the field type. Text field, Combo Box, etc.
	 */
	@Override
	public int getEditingType() {
		return TYPE_TEXT;
	}

	/**
	 * Returns the value of the text which the field currently has
	 * 
	 * @return String the current field value
	 */
	@Override
	public String getInitialValue(IDirectEditingContext context) {

		PictogramElement pe = context.getPictogramElement();
		Object obj = getBusinessObjectForPictogramElement(pe);
		if (PropertyUtil.isEnumMemberValue(pe) && obj instanceof EnumMember) {
			EnumMember currentEnumMember = (EnumMember) obj;
			String newNameEnum = currentEnumMember.getValue() != null ? String
					.valueOf(currentEnumMember.getValue().getValueObject())
					: EMPTY_STRING;
			return newNameEnum;
		}
		return ArtifactUtil.getInitialName(obj);
	}

	/**
	 * Validates the String input
	 * 
	 * @param context
	 *            IDirectEditingContext instance
	 * @return String The error message if input is not valid
	 */
	@Override
	public String checkValueValid(String value, IDirectEditingContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object obj = getBusinessObjectForPictogramElement(pe);
		if (PropertyUtil.isEnumMemberValue(pe) && obj instanceof EnumMember) {
			return ArtifactUtil.validateMemberValue((EnumMember) obj, value);
		}
		return ArtifactUtil.validateName(value);
	}

	/**
	 * Returns if the field can be directly edited in the UI
	 * 
	 * @param context
	 *            IDirectEditingContext instance
	 * @return boolean true if the field is editable
	 */
	@Override
	public boolean canDirectEdit(IDirectEditingContext context) {
		GraphicsAlgorithm ga = context.getGraphicsAlgorithm();
		String id = PropertyUtil.getID(ga.getPictogramElement());
		return (PropertyUtil.ENTITY_NAME.equals(id)
				|| PropertyUtil.ENTITY_SET_NAME.equals(id)
				|| PropertyUtil.ENTITY_PROPERTY_NAME.equals(id)
				|| PropertyUtil.NAVIGATION_PROPERTY_NAME.equals(id)
				|| PropertyUtil.FUNCTION_IMPORT_NAME.equals(id)
				|| PropertyUtil.FUNCTION_IMPORT_PARAMETER_NAME.equals(id)
				|| PropertyUtil.ENUM_NAME.equals(id)
				|| PropertyUtil.ENUM_MEMBER_NAME.equals(id)
				|| PropertyUtil.ENUM_MEMBER_VALUE.equals(id) || PropertyUtil.COMPLEX_NAME
					.equals(id)) && ga instanceof Text;
	}

	/**
	 * Sets the new value to the UI and the object Also makes the corresponding
	 * changes to other elements
	 * 
	 * @param context
	 *            IDirectEditingContext instance
	 */
	@Override
	public void setValue(String value, IDirectEditingContext context) {

		PictogramElement pe = context.getPictogramElement();
		Object obj = getBusinessObjectForPictogramElement(pe);
		if (PropertyUtil.isEnumMemberValue(pe) && obj instanceof EnumMember) {
			EnumMember currentEnumMember = (EnumMember) obj;
			if (value.trim().length() > 0) {
				ArtifactUtil.updateEnumMemberValue(value, currentEnumMember);
				// Update the UI
				ODataShapeUtil.updatePictogramElement(pe, getFeatureProvider());
			} else {
				MessageDialog.openError(Display.getCurrent().getActiveShell(),
						Messages.ODATAEDITOR_CAN_NOT_EDIT_VALUE,
						Messages.ODATAEDITOR_EMPTYVALUE_MSG);
			}

		} else if (obj instanceof EObject) {
			EObject currentObject = (EObject) obj;

			EStructuralFeature nameStrFeature = currentObject.eClass()
					.getEStructuralFeature(NAME_STRFEATURE);
			if (nameStrFeature != null) {
				currentObject.eSet(nameStrFeature, value);
				// Update the UI
				ODataShapeUtil.updatePictogramElement(pe, getFeatureProvider());
			}
		}

	}
}
