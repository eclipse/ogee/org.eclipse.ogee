/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.contexts.NavigationPropertyContext;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.NavigationProperty;

/**
 * Custom Feature Provider to add properties to the Entity type
 * 
 */
public class ODataAddNavigationPropertyFeature extends AbstractCustomFeature {

	/* artifact factory */
	ArtifactFactory artifactFactory;
	/* shapes factory */
	ShapesFactory shapesFactory;

	List<NavigationProperty> propertyList;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactFactory
	 *            ArtifactFactory instance is passed
	 * @param shapesFactory
	 *            ShapesFactory instance is passed
	 */
	public ODataAddNavigationPropertyFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		super(fp);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
	}

	/**
	 * Returns if a navigation property can be added(The feature can be
	 * executed). In this case this has to be done only in case of Entity Types.
	 * 
	 * @param context
	 *            ICustomContext instance is passed
	 */
	@Override
	public boolean canExecute(ICustomContext context) {

		boolean ret = false;
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof EntityType) {
				ret = true;
			}
		}
		return ret;
	}

	@Override
	public String getName() {
		return Messages.ODATAEDITOR_NAVIGATION_PROPERTIES_MENU;
	}

	@Override
	public String getDescription() {
		return Messages.ODATAEDITOR_NAVIGATION_PROPERTIES_MENU_DESC;
	}

	@Override
	public String getImageId() {
		return ODataImageProvider.IMG_NAV_PROPERTY;
	}

	@Override
	public boolean isAvailable(IContext context) {
		return true;
	}

	/**
	 * Check for user input and create the new navigation property
	 * 
	 * @param context
	 *            ICustomContext instance
	 * @see org.eclipse.graphiti.features.custom.ICustomFeature#execute(org.eclipse.graphiti.features.context.ICustomContext)
	 */
	@Override
	public void execute(ICustomContext context) {

		String navPropName;
		AddContext addContext;
		Object prop;
		ICustomContext targetContext = context;
		// Added to fix the issue 784877
		if (!(targetContext instanceof NavigationPropertyContext)
				&& targetContext instanceof CustomContext) {
			NavigationPropertyContext navContext = new NavigationPropertyContext(
					targetContext.getPictogramElements()[0]);
			navContext.setEditMode(true);
			targetContext = navContext;
		}

		if (targetContext instanceof NavigationPropertyContext) {
			NavigationPropertyContext navigationPropertyContext = (NavigationPropertyContext) targetContext;
			PictogramElement mainPictogramElement = navigationPropertyContext
					.getSourcePictogramElement();
			PictogramElement targetPictogramElement = navigationPropertyContext
					.getTargetPictogramElement();
			Association association = (Association) navigationPropertyContext
					.getAssociation();

			EntityType sourceEntityType = null;
			EntityType targetEntityType = null;

			List<NavigationProperty> navPropertyList = new ArrayList<NavigationProperty>();

			if (getBusinessObjectForPictogramElement(mainPictogramElement) instanceof EntityType) {
				sourceEntityType = (EntityType) getBusinessObjectForPictogramElement(mainPictogramElement);
				if (targetPictogramElement != null) {
					targetEntityType = (EntityType) getBusinessObjectForPictogramElement(targetPictogramElement);

				}
				navPropertyList.addAll(sourceEntityType
						.getNavigationProperties());

				EList<Shape> children = ((ContainerShape) mainPictogramElement)
						.getChildren();
				IdentifierShape identifierShape;
				Shape titleImage;
				for (Shape shape : children) {
					/*
					 * Create new property and its shape in the specified area
					 */
					if (PropertyUtil.NAVIGATIONPROPERTY_RECTANGLE
							.equals(PropertyUtil.getID(shape))) {
						prop = this.artifactFactory
								.createDefaultNavigationProperty(
										sourceEntityType, targetEntityType,
										association, navigationPropertyContext,
										navigationPropertyContext
												.isSecondNavPropertyForSelf());
						navPropName = ((NavigationProperty) prop).getName();
						addContext = new AddContext();
						addContext.setNewObject(prop);
						/* Visual shape */
						identifierShape = this.shapesFactory
								.createIdentifierShape((ContainerShape) shape,
										navPropName,
										ODataImageProvider.IMG_NAV_PROPERTY,
										addContext, prop, false,
										PropertyUtil.NAVIGATION_PROPERTY_NAME);
						// To expand the section
						titleImage = ODataLayoutUtil
								.expandSection((ContainerShape) shape);
						if (titleImage != null) {
							ODataShapeUtil.updatePictogramElement(titleImage,
									getFeatureProvider());
						}
						/* Update the layout */
						ODataShapeUtil.layoutPictogramElement(
								mainPictogramElement, getFeatureProvider());

						// To update the selection
						identifierShape.updateSelection();
						if (navigationPropertyContext.isEditMode()) {
							identifierShape.enableDirectEdit();
						}
						break;
					}
				}

			}

		}
	}
}
