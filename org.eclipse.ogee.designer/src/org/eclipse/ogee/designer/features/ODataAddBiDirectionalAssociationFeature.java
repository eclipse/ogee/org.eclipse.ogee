/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.List;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.impl.AbstractAddFeature;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IPeCreateService;
import org.eclipse.ogee.designer.contexts.AssociationAddConnectionContext;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.utils.AssociationUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.Property;

public class ODataAddBiDirectionalAssociationFeature extends AbstractAddFeature {
	/* artifact factory */
	ArtifactFactory artifactFactory;
	/* shapes factory */
	ShapesFactory shapesFactory;

	List<Property> propertyList;

	public ODataAddBiDirectionalAssociationFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		super(fp);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
	}

	@Override
	public boolean canAdd(IAddContext context) {
		// check for right domain object instance below
		boolean connectionContext = context instanceof AssociationAddConnectionContext;
		if (connectionContext) {
			boolean isUniDirectional = ((AssociationAddConnectionContext) context)
					.isUniDirectionalAssociation();
			return (connectionContext
					&& context.getNewObject() instanceof Association && !isUniDirectional);
		}
		return false;
	}

	@Override
	public PictogramElement add(IAddContext context) {

		IAddConnectionContext addConContext = (IAddConnectionContext) context;
		IPeCreateService peCreateService = Graphiti.getPeCreateService();
		Diagram targetDiagram = (Diagram) context.getTargetContainer();

		// Connection Object

		Anchor sourceAnchor = addConContext.getSourceAnchor();
		AnchorContainer source = sourceAnchor.getParent();
		Anchor targetAnchor = addConContext.getTargetAnchor();
		AnchorContainer target = targetAnchor.getParent();

		int connectionCount = AssociationUtil.getConnections(sourceAnchor,
				targetAnchor);

		Connection connection = peCreateService
				.createFreeFormConnection(getDiagram());
		connection.setStart(sourceAnchor);
		connection.setEnd(targetAnchor);
		// Create Association Shape
		this.shapesFactory.createBiDirectionalAssociationShape(targetDiagram,
				context, connection);

		if (source == target) {
			AssociationUtil.drawSelfAssociation(connection, connectionCount);
		} else if (connectionCount > 0) {
			ODataShapeUtil.addConnectionBendPoint(getFeatureProvider(),
					connection, sourceAnchor, targetAnchor);

		}

		return connection;
	}

	@Override
	public boolean canUndo(IContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDoneChanges() {
		// TODO Auto-generated method stub
		return false;
	}
}
