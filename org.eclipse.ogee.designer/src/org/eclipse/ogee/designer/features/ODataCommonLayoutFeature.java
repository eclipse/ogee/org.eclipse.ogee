/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.impl.AbstractLayoutFeature;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;

public class ODataCommonLayoutFeature extends AbstractLayoutFeature {

	public ODataCommonLayoutFeature(IFeatureProvider fp) {
		// TODO Auto-generated constructor stub
		super(fp);
	}

	@Override
	public boolean canLayout(ILayoutContext context) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean layout(ILayoutContext context) {
		// TODO Auto-generated method stub
		return ODataLayoutUtil.layout(context);
	}

	@Override
	public boolean canUndo(IContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDoneChanges() {
		// TODO Auto-generated method stub
		return false;
	}
}
