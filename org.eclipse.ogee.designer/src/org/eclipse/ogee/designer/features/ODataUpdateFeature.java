/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.AbstractUpdateFeature;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.SimpleTypeUsageImpl;

/**
 * This class will provide the update functionality on all the artifacts if the
 * name is changed
 * 
 */
public class ODataUpdateFeature extends AbstractUpdateFeature {

	private static final String EMPTY_STRING = "";//$NON-NLS-1$

	/**
	 * @param fp
	 */
	public ODataUpdateFeature(IFeatureProvider fp) {
		super(fp);

	}

	@Override
	public boolean canUpdate(IUpdateContext context) {

		Object currentObject = getBusinessObjectForPictogramElement(context
				.getPictogramElement());

		return ArtifactUtil.isEditable(currentObject);
	}

	@Override
	public IReason updateNeeded(IUpdateContext context) {

		PictogramElement pe = context.getPictogramElement();
		Object currentObject = getBusinessObjectForPictogramElement(pe);
		Shape shape = (Shape) pe;
		IReason reason = Reason.createFalseReason();
		GraphicsAlgorithm graphicsAlgorithm = shape.getGraphicsAlgorithm();
		if (currentObject instanceof EnumType
				&& PropertyUtil.isEnumUndderlyingType(pe)) {
			if (graphicsAlgorithm instanceof Text) {

				Text text = (Text) graphicsAlgorithm;
				if (((EnumType) currentObject).getUnderlyingType() != null) {
					String newNameEnum = ((EnumType) currentObject)
							.getUnderlyingType().getName();
					reason = !newNameEnum.equals(text.getValue()) ? Reason
							.createTrueReason() : reason;
					return reason;
				}
			}
		} else if (PropertyUtil.isEnumMemberValue(pe)
				&& currentObject instanceof EnumMember) {
			EnumMember currentEnumMember = (EnumMember) currentObject;
			if (graphicsAlgorithm instanceof Text) {
				Text text = (Text) graphicsAlgorithm;
				String newNameEnum = currentEnumMember.getValue() != null ? String
						.valueOf(currentEnumMember.getValue().getValueObject())
						: EMPTY_STRING;
				reason = !newNameEnum.equals(text.getValue()) ? Reason
						.createTrueReason() : reason;
				return reason;
			}
		}
		String initialName = ArtifactUtil.getInitialName(currentObject);
		String newName = initialName != null ? initialName : EMPTY_STRING;

		if (newName != null) {
			if (graphicsAlgorithm instanceof Text) {

				if ((ArtifactUtil.isReferencedEntityType(currentObject))) {
					newName = ((EntityType) currentObject).getName();
				}
				Text text = (Text) graphicsAlgorithm;
				reason = !newName.equals(text.getValue()) ? Reason
						.createTrueReason() : reason;

			} else if (graphicsAlgorithm instanceof Image) {

				if (PropertyUtil.isReturnType(shape)
						|| PropertyUtil.isProperty(shape)) {
					if (ODataShapeUtil.isPropertyUpdateNeeded(
							(EObject) currentObject, shape)) {
						reason = Reason.createTrueReason();
					}
				} else {
					PictogramElement parentPE = (PictogramElement) shape
							.eContainer();
					if (parentPE != null && PropertyUtil.isTitle(parentPE)) {
						boolean canBeExapnded = PropertyUtil
								.isExpanded(parentPE)
								|| PropertyUtil.isCollapsed(parentPE);
						if (canBeExapnded) {
							String id = ((Image) (graphicsAlgorithm)).getId();
							if (((ODataImageProvider.IMG_EXPAND).equals(id) && PropertyUtil
									.isCollapsed(parentPE))
									|| ((ODataImageProvider.IMG_COLLAPSE)
											.equals(id) && PropertyUtil
											.isExpanded(parentPE))) {
								reason = Reason.createTrueReason();

							}
						}
					} else if (currentObject instanceof EntityType) {
						if (ODataShapeUtil.isPropertiesUpdateNeeded(
								getFeatureProvider(), currentObject, shape)) {
							// key -Non key property update
							reason = Reason.createTrueReason();
						}

					}
				}
			}
		}

		return reason;
	}

	@Override
	public boolean update(IUpdateContext context) {

		Object currentObject = getBusinessObjectForPictogramElement(context
				.getPictogramElement());

		Shape shape = (Shape) context.getPictogramElement();
		String newName = EMPTY_STRING;
		if (currentObject instanceof EnumType
				&& PropertyUtil.isEnumUndderlyingType(context
						.getPictogramElement())) {
			if (((EnumType) currentObject).getUnderlyingType() != null) {
				newName = ((EnumType) currentObject).getUnderlyingType()
						.getName();
			}
		} else if (PropertyUtil
				.isEnumMemberValue(context.getPictogramElement())
				&& currentObject instanceof EnumMember) {
			EnumMember currentEnumMember = (EnumMember) currentObject;

			newName = currentEnumMember.getValue() != null ? String
					.valueOf(currentEnumMember.getValue().getValueObject())
					: EMPTY_STRING;
		}

		else {
			String initialName = ArtifactUtil.getInitialName(currentObject);
			newName = initialName != null ? initialName : EMPTY_STRING;
		}

		if (ArtifactUtil.isFunctionImportReturnType(currentObject)
				|| PropertyUtil.isProperty(shape)) {

			if (shape.getGraphicsAlgorithm() instanceof Image) {

				Image currentImage = (Image) shape.getGraphicsAlgorithm();
				if (ArtifactUtil.isCollectionReturnType(currentObject)) {
					if (currentObject instanceof SimpleTypeUsageImpl) {
						currentImage
								.setId(ODataImageProvider.IMG_PROPERTY_COLLN);
					} else if (currentObject instanceof ComplexTypeUsageImpl) {
						currentImage
								.setId(ODataImageProvider.IMG_CPLX_TYP_COLLN);
					} else if (currentObject instanceof EnumTypeUsageImpl) {
						currentImage.setId(ODataImageProvider.IMG_ENUM_COLLN);
					} else if (currentObject instanceof ReturnEntityTypeUsageImpl) {
						currentImage
								.setId(ODataImageProvider.IMG_ENTY_SET_COLLN);
					}
				} else {
					if (currentObject instanceof SimpleTypeUsageImpl) {

						boolean isKey = false;
						EObject property = ((SimpleTypeUsage) currentObject)
								.eContainer();
						if (property instanceof Property
								&& property.eContainer() instanceof EntityType) {
							EntityType entityType = (EntityType) property
									.eContainer();
							isKey = entityType.getKeys().contains(property);
						}
						currentImage
								.setId(isKey ? ODataImageProvider.IMG_KEY_PROPERTY
										: (ODataImageProvider.IMG_PROPERTY));

						currentImage.setId(ODataImageProvider.IMG_PROPERTY);
					} else if (currentObject instanceof ComplexTypeUsageImpl) {
						currentImage.setId(ODataImageProvider.IMG_CMPLX_TYPE);
					} else if (currentObject instanceof EnumTypeUsageImpl) {
						boolean isKey = false;
						EObject property = ((EnumTypeUsageImpl) currentObject)
								.eContainer();
						if (property instanceof Property
								&& property.eContainer() instanceof EntityType) {
							EntityType entityType = (EntityType) property
									.eContainer();
							isKey = entityType.getKeys().contains(property);
						}
						currentImage
								.setId(isKey ? ODataImageProvider.IMG_KEY_PROPERTY
										: ODataImageProvider.IMG_ENUM);
					} else if (currentObject instanceof ReturnEntityTypeUsageImpl) {
						currentImage.setId(ODataImageProvider.IMG_ENTITY);
					}
				}

				return true;
			} else if (shape.getGraphicsAlgorithm() instanceof Text) {

				Text text = (Text) shape.getGraphicsAlgorithm();

				if (newName.trim().length() == 0) {
					text.setValue(Messages.ODATAEDITOR_UNDEFINED_NAME);
				} else {
					text.setValue(newName);
				}

				return true;
			}
		}

		if (shape.getGraphicsAlgorithm() instanceof Text) {

			// Update for Entity Name
			if (ArtifactUtil.isEntityType(currentObject) && newName != null) {
				if (!(ArtifactUtil.isReferencedEntityType(currentObject))) {

					Text text = (Text) shape.getGraphicsAlgorithm();
					String previousEntityName = text.getValue();
					text.setValue(newName);

					ODataShapeUtil.updateEntityName(getFeatureProvider(),
							shape, newName, previousEntityName);
				} else {
					Text text = (Text) shape.getGraphicsAlgorithm();

					text.setValue(((EntityType) currentObject).getName());
				}
			} else {
				Text text = (Text) shape.getGraphicsAlgorithm();
				text.setValue(newName);
			}
			// Update for All effected Shapes.
			ODataShapeUtil.updateReferedShapes((EObject) currentObject,
					getFeatureProvider(), false);

			return true;
		}

		PictogramElement parentPE = (PictogramElement) shape.eContainer();
		if (parentPE != null && PropertyUtil.isTitle(parentPE)) {
			if (shape.getGraphicsAlgorithm() instanceof Image) {

				Image currentImage = (Image) shape.getGraphicsAlgorithm();
				currentImage
						.setId(PropertyUtil.isExpanded(parentPE) ? ODataImageProvider.IMG_EXPAND
								: ODataImageProvider.IMG_COLLAPSE);
				return true;
			}
		} else if (shape.getGraphicsAlgorithm() instanceof Image
				&& currentObject instanceof EntityType) {
			// key -Non key property update
			ODataShapeUtil.updateEntityProperties(getFeatureProvider(),
					currentObject, shape);
			return true;

		}

		return false;
	}

}
