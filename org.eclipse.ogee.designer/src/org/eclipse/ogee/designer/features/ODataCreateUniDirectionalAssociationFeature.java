/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.impl.AbstractCreateConnectionFeature;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;

public class ODataCreateUniDirectionalAssociationFeature extends
		AbstractCreateConnectionFeature {
	/**
	 * ShapesFactory instance used to create all the shapes.
	 */
	private ShapesFactory shapesFactory;

	/**
	 * ArifactFactory instance used to create all the Business objects.
	 */
	private ArtifactFactory artifactFactory;

	public ODataCreateUniDirectionalAssociationFeature(IFeatureProvider fp,
			ShapesFactory shapesFactory, ArtifactFactory artifactFactory) {
		super(fp, Messages.ODATAEDITOR_UNIDIRECTIONAL_ASSOC_NAME,
				Messages.ODATAEDITOR_UNIDIRECTIONAL_ASSOC_DESC);
		this.shapesFactory = shapesFactory;
		this.artifactFactory = artifactFactory;
	}

	@Override
	public String getCreateName() {
		return Messages.ODATAEDITOR_UNIDIRECTIONAL_ASSOC_NAME;
	}

	@Override
	public String getCreateDescription() {
		return Messages.ODATAEDITOR_UNIDIRECTIONAL_ASSOC_DESC;
	}

	/**
	 * Returns true if the association can be created
	 * 
	 * @return boolean true if association can be created
	 */
	@Override
	public boolean canStartConnection(ICreateConnectionContext context) {
		return ODataShapeUtil.canStartUniAssociation(context,
				getFeatureProvider());
	}

	/**
	 * Returns the Image associated with the object
	 * 
	 * @return String ImagePath
	 */
	@Override
	public String getCreateImageId() {
		return ODataImageProvider.IMG_SIN_ASSOCIATION;
	}

	@Override
	public boolean canCreate(ICreateConnectionContext context) {
		return ODataShapeUtil.canCreateUniAssociation(context,
				getFeatureProvider());
	}

	@Override
	public Connection create(ICreateConnectionContext context) {

		return ODataShapeUtil.createUniAssociation(context,
				getFeatureProvider(), this.artifactFactory, this.shapesFactory);
	}

	@Override
	public boolean canUndo(IContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDoneChanges() {
		// TODO Auto-generated method stub
		return false;
	}
}
