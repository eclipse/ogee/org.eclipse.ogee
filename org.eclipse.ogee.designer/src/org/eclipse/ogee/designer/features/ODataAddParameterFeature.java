/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.utils.ArtifactNameUtil;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.Parameter;

/**
 * Custom Feature Provider to add parameters to the function import
 */
public class ODataAddParameterFeature extends AbstractCustomFeature {

	/**
	 * artifact factory
	 */
	private ArtifactFactory artifactFactory;
	/**
	 * shapes factory
	 */
	private ShapesFactory shapesFactory;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactFactory
	 *            ArtifactFactory instance is passed
	 * @param shapesFactory
	 *            ShapesFactory instance is passed
	 */
	public ODataAddParameterFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		super(fp);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
	}

	/**
	 * Returns if a parameter can be added(The feature can be executed). In this
	 * case this has to be done only in case of Function Imports.
	 * 
	 * @param context
	 *            ICustomContext instance is passed
	 */
	@Override
	public boolean canExecute(ICustomContext context) {
		boolean ret = false;
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof FunctionImport) {
				ret = true;
			}
		}
		return ret;
	}

	@Override
	public String getName() {
		return Messages.ODATAEDITOR_PARAMETERS_MENU;
	}

	@Override
	public String getDescription() {
		return Messages.ODATAEDITOR_PARAMETERS_MENU_DESC;
	}

	@Override
	public String getImageId() {
		return ODataImageProvider.IMG_PARAMETER;
	}

	@Override
	public boolean isAvailable(IContext context) {
		return true;
	}

	/**
	 * Check for user input and create the new parameter
	 * 
	 * @param context
	 *            ICustomContext instance
	 * @see org.eclipse.graphiti.features.custom.ICustomFeature#execute(org.eclipse.graphiti.features.context.ICustomContext)
	 */
	@Override
	public void execute(ICustomContext context) {
		String parameterName;

		AddContext addContext;
		Object parameter;

		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof FunctionImport) {
				PictogramElement mainPictogramElement = pes[0];
				FunctionImport funcImp = (FunctionImport) bo;

				List<Parameter> parameterList = new ArrayList<Parameter>();
				parameterList.addAll(funcImp.getParameters());
				parameterName = getNameForParameter(parameterList);

				if (parameterName != null && parameterName.trim().length() != 0) {
					EList<Shape> children = ((ContainerShape) mainPictogramElement)
							.getChildren();
					IdentifierShape identifierShape = null;
					ContainerShape sectionShape;
					Shape titleImage;
					for (Shape shape : children) {
						/*
						 * Create new function import parameter and its shape in
						 * the specified area
						 */
						if (PropertyUtil.FUNCTION_IMPORT_PARAMETER_RECTANGLE
								.equals(PropertyUtil.getID(shape))) {
							sectionShape = (ContainerShape) shape;
							parameter = this.artifactFactory.createParameter(
									funcImp, parameterName);
							addContext = new AddContext();
							addContext.setNewObject(parameter);

							/* Visual shape */
							identifierShape = this.shapesFactory
									.createIdentifierShape(
											sectionShape,
											parameterName,
											ODataImageProvider.IMG_PARAMETER,
											addContext,
											parameter,
											false,
											PropertyUtil.FUNCTION_IMPORT_PARAMETER_NAME);
							titleImage = ODataLayoutUtil
									.expandSection(sectionShape);
							if (titleImage != null) {
								ODataShapeUtil.updatePictogramElement(
										titleImage, getFeatureProvider());

							}

							/* Update the layout */
							ODataShapeUtil.layoutPictogramElement(
									mainPictogramElement, getFeatureProvider());

							identifierShape.updateSelection();
							identifierShape.enableDirectEdit();
							break;
						}
					}
				}

			}
		}
	}

	/**
	 * Creates a unique name for the new Parameter
	 * 
	 * @param parameters
	 * @return String new unique name
	 */
	private String getNameForParameter(List<Parameter> parameters) {
		List<String> parameterNames = new ArrayList<String>();
		String pn = null;
		Parameter p;
		for (ListIterator<Parameter> it = parameters.listIterator(); it
				.hasNext();) {
			p = it.next();
			pn = p.getName();
			parameterNames.add(pn);
		}
		String newParamName = ArtifactNameUtil.generateUniqueName(
				parameterNames,
				Messages.ODATAEDITOR_PARAMETERS_DIALOG_PROPERTY,
				IODataEditorConstants.MAX_LENGTH);
		return newParamName;
	}

	/*
	 * private String getNameForParameter(List<Parameter> parameters) { int
	 * uniqueParametersID = 1; String parameterName; StringBuffer newName = new
	 * StringBuffer( Messages.ODATAEDITOR_PARAMETERS_DIALOG_PROPERTY);
	 * List<Integer> numberList = new ArrayList<Integer>(); Parameter parameter;
	 * StringTokenizer st; String parameterNumber; int index; for
	 * (Iterator<Parameter> iterator = parameters.iterator(); iterator
	 * .hasNext();) { parameter = iterator.next(); parameterName =
	 * parameter.getName(); if (parameterName
	 * .startsWith(Messages.ODATAEDITOR_PARAMETERS_DIALOG_PROPERTY)) { st = new
	 * StringTokenizer(parameterName,
	 * Messages.ODATAEDITOR_PARAMETERS_DIALOG_PROPERTY); if (parameterName
	 * .equalsIgnoreCase(Messages.ODATAEDITOR_PARAMETERS_DIALOG_PROPERTY)) {
	 * numberList.add(Integer.valueOf(1)); } while (st.hasMoreTokens()) {
	 * parameterNumber = st.nextToken(); if (parameterNumber != null &&
	 * parameterNumber.trim().length() > 0) { try { index =
	 * Integer.parseInt(parameterNumber);
	 * numberList.add(Integer.valueOf(index)); } catch (NumberFormatException e)
	 * { // Digest...don't log } } } } } for (Iterator<Integer> iterator =
	 * numberList.iterator(); iterator .hasNext();) { if
	 * (!numberList.contains(Integer.valueOf(uniqueParametersID))) { if
	 * (uniqueParametersID != 1) { newName.append(uniqueParametersID); } break;
	 * } uniqueParametersID++; } return newName.toString(); }
	 */
}
