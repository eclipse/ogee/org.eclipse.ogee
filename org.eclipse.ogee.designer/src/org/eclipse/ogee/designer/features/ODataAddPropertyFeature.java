/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.utils.ArtifactNameUtil;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.designer.utils.ODataPropertyComparator;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Property;

/**
 * Custom Feature Provider to add properties to the Entity type
 * 
 */
public class ODataAddPropertyFeature extends AbstractCustomFeature {

	/**
	 * artifact factory
	 */
	private ArtifactFactory artifactFactory;
	/**
	 * shapes factory
	 */
	private ShapesFactory shapesFactory;

	/**
	 * To differentiate between KEY and NON KEY property.
	 */
	private boolean isKeyValue;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactFactory
	 *            ArtifactFactory instance is passed
	 * @param shapesFactory
	 *            ShapesFactory instance is passed
	 */
	public ODataAddPropertyFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		super(fp);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
	}

	/**
	 * Returns if a property can be added(The feature can be executed). In this
	 * case this has to be done only in case of Entity Types or Complex Types.
	 * 
	 * @param context
	 *            ICustomContext instance is passed
	 */
	@Override
	public boolean canExecute(ICustomContext context) {
		boolean ret = false;
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof EntityType || bo instanceof ComplexType) {
				ret = true;
			}
		}
		return ret;
	}

	@Override
	public String getName() {
		return Messages.ODATAEDITOR_PROPERTIES_MENU;
	}

	@Override
	public String getDescription() {
		return Messages.ODATAEDITOR_PROPERTIES_MENU_DESC;
	}

	@Override
	public String getImageId() {
		return ODataImageProvider.IMG_PROPERTY;
	}

	@Override
	public boolean isAvailable(IContext context) {
		return true;
	}

	/**
	 * Check for user input and create the new property
	 * 
	 * @param context
	 *            ICustomContext instance
	 * @see org.eclipse.graphiti.features.custom.ICustomFeature#execute(org.eclipse.graphiti.features.context.ICustomContext)
	 */
	@Override
	public void execute(ICustomContext context) {
		String propName;
		AddContext addContext;
		Property prop;
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof EntityType) {
				PictogramElement mainPictogramElement = pes[0];
				EntityType etype = (EntityType) bo;
				List<Property> propertyList = new ArrayList<Property>();
				propertyList.addAll(etype.getProperties());
				propertyList.addAll(etype.getKeys());
				propName = getNameForProperty(bo, propertyList);

				if (propName != null && propName.trim().length() != 0) {
					EList<Shape> children = ((ContainerShape) mainPictogramElement)
							.getChildren();
					IdentifierShape identifierShape = null;
					ContainerShape sectionShape;
					String imageId;
					Shape titleImage;
					for (Shape shape : children) {
						/*
						 * Create new property and its shape in the specified
						 * area
						 */
						if (PropertyUtil.PROPERTY_RECTANGLE.equals(PropertyUtil
								.getID(shape))) {
							sectionShape = (ContainerShape) shape;

							imageId = ODataImageProvider.IMG_PROPERTY;
							if (this.isKeyValue) {
								imageId = ODataImageProvider.IMG_KEY_PROPERTY;
							}

							prop = this.artifactFactory.createProperty(etype,
									propName, this.isKeyValue);
							addContext = new AddContext();
							addContext.setNewObject(prop);

							/* Visual shape */
							identifierShape = this.shapesFactory
									.createPropertyShape(sectionShape,
											propName, imageId, addContext,
											prop, prop.getType(), false,
											PropertyUtil.ENTITY_PROPERTY_NAME);
							if (this.isKeyValue) {
								// For Sorting key and Non key properties
								ECollections.sort(sectionShape.getChildren(),
										new ODataPropertyComparator(
												getFeatureProvider()));
							}
							titleImage = ODataLayoutUtil
									.expandSection(sectionShape);
							if (titleImage != null) {
								ODataShapeUtil.updatePictogramElement(
										titleImage, getFeatureProvider());

							}
							/* Update the layout */
							ODataShapeUtil.layoutPictogramElement(
									mainPictogramElement, getFeatureProvider());

							identifierShape.updateSelection();
							identifierShape.enableDirectEdit();
							break;
						}
					}
				}

			} else if (bo instanceof ComplexType) {
				PictogramElement mainPictogramElement = pes[0];
				ComplexType ctype = (ComplexType) bo;

				List<Property> propertyList = new ArrayList<Property>();
				propertyList.addAll(ctype.getProperties());
				propName = getNameForProperty(bo, propertyList);

				if (propName != null && propName.trim().length() != 0) {
					EList<Shape> children = ((ContainerShape) mainPictogramElement)
							.getChildren();
					IdentifierShape identifierShape = null;
					ContainerShape sectionShape;
					Shape titleImage;
					for (Shape shape : children) {
						/*
						 * Create new property and its shape in the specified
						 * area
						 */
						if (PropertyUtil.PROPERTY_RECTANGLE.equals(PropertyUtil
								.getID(shape))) {
							sectionShape = (ContainerShape) shape;
							prop = this.artifactFactory.createProperty(ctype,
									propName, false);
							addContext = new AddContext();
							addContext.setNewObject(prop);

							/* Visual shape */
							identifierShape = this.shapesFactory
									.createPropertyShape(sectionShape,
											propName,
											ODataImageProvider.IMG_PROPERTY,
											addContext, prop, prop.getType(),
											false,
											PropertyUtil.ENTITY_PROPERTY_NAME);
							titleImage = ODataLayoutUtil
									.expandSection(sectionShape);
							if (titleImage != null) {
								ODataShapeUtil.updatePictogramElement(
										titleImage, getFeatureProvider());

							}
							/* Update the layout */
							ODataShapeUtil.layoutPictogramElement(
									mainPictogramElement, getFeatureProvider());

							identifierShape.updateSelection();
							identifierShape.enableDirectEdit();
							break;
						}
					}
				}

			}
		}
	}

	/**
	 * Creates a unique name for the new Entity
	 * 
	 * @param properties
	 * @return String new unique name
	 */
	private String getNameForProperty(Object bo, List<Property> properties) {
		List<String> propertyNames = new ArrayList<String>();
		String pn = null;
		Property p;
		EntityType e;
		ComplexType c;
		String objName = null;

		if (bo instanceof EntityType) {
			e = (EntityType) bo;
			objName = e.getName();
		} else if (bo instanceof ComplexType) {
			c = (ComplexType) bo;
			objName = c.getName();
		}
		propertyNames.add(objName);

		for (ListIterator<Property> it = properties.listIterator(); it
				.hasNext();) {
			p = it.next();
			pn = p.getName();
			if (!(pn.contains(Messages.ODATAEDITOR_PROPERTIES_DEFAULT_NAME))) {
				propertyNames.add(pn);
			}

		}
		// propertyNames = getEntTypeCmplxTypePropAssocNames(schema);
		String newPropName = ArtifactNameUtil.generateUniqueName(propertyNames,
				Messages.ODATAEDITOR_PROPERTIES_DIALOG_PROPERTY,
				IODataEditorConstants.MAX_LENGTH);
		return newPropName;
	}

	/*
	 * private String getNameForProperty(List<Property> properties) { int
	 * uniquePropertyID = 1; String propertyName; StringBuffer newName = new
	 * StringBuffer( Messages.ODATAEDITOR_PROPERTIES_DIALOG_PROPERTY);
	 * List<Integer> numberList = new ArrayList<Integer>(); Property property;
	 * StringTokenizer st; String propertyNumber; int index; for
	 * (Iterator<Property> iterator = properties.iterator(); iterator
	 * .hasNext();) { property = iterator.next(); propertyName =
	 * property.getName(); if (propertyName
	 * .startsWith(Messages.ODATAEDITOR_PROPERTIES_DIALOG_PROPERTY)) { st = new
	 * StringTokenizer(propertyName,
	 * Messages.ODATAEDITOR_PROPERTIES_DIALOG_PROPERTY); if (propertyName
	 * .equalsIgnoreCase(Messages.ODATAEDITOR_PROPERTIES_DIALOG_PROPERTY)) {
	 * numberList.add(Integer.valueOf(1)); } while (st.hasMoreTokens()) {
	 * propertyNumber = st.nextToken(); if (propertyNumber != null &&
	 * propertyNumber.trim().length() > 0) { try { index =
	 * Integer.parseInt(propertyNumber); numberList.add(Integer.valueOf(index));
	 * } catch (NumberFormatException e) { // Digest...don't log } } } } } for
	 * (Iterator<Integer> iterator = numberList.iterator(); iterator
	 * .hasNext();) { if
	 * (!numberList.contains(Integer.valueOf(uniquePropertyID))) { if
	 * (uniquePropertyID != 1) { newName.append(uniquePropertyID); } break; }
	 * uniquePropertyID++; } return newName.toString(); }
	 */

	/**
	 * @param pe
	 */
	public void setKeyValue(PictogramElement pe) {

		Object businessObject = getBusinessObjectForPictogramElement(pe);
		this.isKeyValue = ArtifactUtil.isKeyProperty(businessObject);
	}

	/**
	 * @param iskey
	 *            this method is created only for PDEJunit purpose.
	 */
	public void setAsKey(boolean iskey) {
		this.isKeyValue = iskey;
	}
}
