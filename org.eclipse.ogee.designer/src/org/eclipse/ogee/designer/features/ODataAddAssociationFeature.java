/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.impl.AbstractCreateConnectionFeature;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;

/**
 * This pattern class is created for 'Association' domain Object in OData Model.
 * This contains all the methods required to create and update an Association's
 * Shape and BusinessObject. An association will also have an Association Set.
 * This is used to make the feature work from the context menu pad This will
 * create a navigation property also.
 * 
 */
public class ODataAddAssociationFeature extends AbstractCreateConnectionFeature {

	/**
	 * artifact factory
	 */
	private ArtifactFactory artifactFactory;
	/**
	 * shapes factory
	 */
	private ShapesFactory shapesFactory;

	/**
	 * Default constructor
	 * 
	 * @param fp
	 * @param artifactFactory
	 * @param shapesFactory
	 */
	public ODataAddAssociationFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		super(fp, Messages.ODATAEDITOR_ASSOC_NAME,
				Messages.ODATAEDITOR_ASSOC_DESC);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
	}

	/**
	 * Returns true if the association can be started on an object
	 * 
	 * @return boolean true if association can be started on an object
	 */
	@Override
	public boolean canStartConnection(ICreateConnectionContext context) {
		return ODataShapeUtil.canStartUniAssociation(context,
				getFeatureProvider());
	}

	/**
	 * Returns true if the association can be created
	 * 
	 * @return boolean true if association can be created
	 */
	@Override
	public boolean canCreate(ICreateConnectionContext context) {
		return ODataShapeUtil.canCreateUniAssociation(context,
				getFeatureProvider());
	}

	@Override
	public Connection create(ICreateConnectionContext context) {

		return ODataShapeUtil.createUniAssociation(context,
				getFeatureProvider(), this.artifactFactory, this.shapesFactory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.graphiti.features.impl.AbstractCreateConnectionFeature#
	 * getCreateImageId()
	 */
	@Override
	public String getCreateImageId() {
		return ODataImageProvider.IMG_SIN_ASSOCIATION;
	}

	@Override
	public boolean canUndo(IContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDoneChanges() {
		// TODO Auto-generated method stub
		return false;
	}

}