/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;

/**
 * Custom Feature Provider to add members to the Enum type A member will be a
 * name-value pair which will have numerical values
 * 
 */
public class ODataAddEnumMemberFeature extends AbstractCustomFeature {

	/**
	 * artifact factory
	 */
	private ArtifactFactory artifactFactory;
	/**
	 * shapes factory
	 */
	private ShapesFactory shapesFactory;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * @param artifactFactory
	 *            ArtifactFactory instance is passed
	 * @param shapesFactory
	 *            ShapesFactory instance is passed
	 */
	public ODataAddEnumMemberFeature(IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		super(fp);
		this.artifactFactory = artifactFactory;
		this.shapesFactory = shapesFactory;
	}

	/**
	 * Returns if a parameter can be added(The feature can be executed). In this
	 * case this has to be done only in case of Function Imports.
	 * 
	 * @param context
	 *            ICustomContext instance is passed
	 */
	@Override
	public boolean canExecute(ICustomContext context) {
		boolean ret = false;
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof EnumType) {
				ret = true;
			}
		}
		return ret;
	}

	@Override
	public String getName() {
		return Messages.ODATAEDITOR_ENUMMEMBER_MENU;
	}

	@Override
	public String getDescription() {
		return Messages.ODATAEDITOR_ENUMMEMBER_MENU_DESC;
	}

	@Override
	public String getImageId() {
		return ODataImageProvider.IMG_ENUM;
	}

	@Override
	public boolean isAvailable(IContext context) {
		return true;
	}

	/**
	 * Check for user input and create the new parameter
	 * 
	 * @param context
	 *            ICustomContext instance
	 * @see org.eclipse.graphiti.features.custom.ICustomFeature#execute(org.eclipse.graphiti.features.context.ICustomContext)
	 */
	@Override
	public void execute(ICustomContext context) {
		String enumMemberName;

		AddContext addContext;
		Object member;

		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
			if (bo instanceof EnumType) {
				PictogramElement mainPictogramElement = pes[0];
				EnumType enumType = (EnumType) bo;

				List<EnumMember> enumMemberList = new ArrayList<EnumMember>();
				enumMemberList.addAll(enumType.getMembers());
				enumMemberName = getNameForMember(enumMemberList);

				if (enumMemberName != null
						&& enumMemberName.trim().length() != 0) {
					EList<Shape> children = ((ContainerShape) mainPictogramElement)
							.getChildren();
					IdentifierShape identifierShape = null;
					ContainerShape sectionShape;
					Shape titleImage;
					for (Shape shape : children) {
						/*
						 * Create new function import parameter and its shape in
						 * the specified area
						 */
						if (PropertyUtil.ENUM_MEMBER_RECTANGLE
								.equals(PropertyUtil.getID(shape))) {
							sectionShape = (ContainerShape) shape;
							member = this.artifactFactory.createEnumMember(
									enumType, enumMemberName);
							addContext = new AddContext();
							addContext.setNewObject(member);

							/* Visual shape */
							identifierShape = this.shapesFactory
									.createMemberShape(
											sectionShape,
											ArtifactUtil
													.getEnumMemberName(member),
											ArtifactUtil
													.getEnumMemberValue(member),
											ODataImageProvider.IMG_PROPERTY,
											addContext, member, false);

							titleImage = ODataLayoutUtil
									.expandSection(sectionShape);
							if (titleImage != null) {
								ODataShapeUtil.updatePictogramElement(
										titleImage, getFeatureProvider());

							}

							/* Update the layout */
							ODataShapeUtil.layoutPictogramElement(
									mainPictogramElement, getFeatureProvider());

							identifierShape.updateSelection();
							identifierShape.enableDirectEdit();
							break;
						}
					}
				}

			}
		}
	}

	/**
	 * Creates a unique name for the new Parameter
	 * 
	 * @param parameters
	 * @return String new unique name
	 */
	private String getNameForMember(List<EnumMember> members) {
		int uniqueMemberID = 1;
		String memberName;
		StringBuffer newName = new StringBuffer(
				Messages.ODATAEDITOR_ENUMMEMBER_DEFAULTNAME);
		List<Integer> numberList = new ArrayList<Integer>();
		EnumMember member;
		StringTokenizer st;
		String memberNumber;
		int index;
		for (Iterator<EnumMember> iterator = members.iterator(); iterator
				.hasNext();) {
			member = iterator.next();
			memberName = member.getName();
			if (memberName
					.startsWith(Messages.ODATAEDITOR_ENUMMEMBER_DEFAULTNAME)) {
				st = new StringTokenizer(memberName,
						Messages.ODATAEDITOR_ENUMMEMBER_DEFAULTNAME);
				if (memberName
						.equalsIgnoreCase(Messages.ODATAEDITOR_ENUMMEMBER_DEFAULTNAME)) {
					numberList.add(Integer.valueOf(1));
				}
				while (st.hasMoreTokens()) {
					memberNumber = st.nextToken();
					if (memberNumber != null
							&& memberNumber.trim().length() > 0) {
						try {
							index = Integer.parseInt(memberNumber);
							numberList.add(Integer.valueOf(index));
						} catch (NumberFormatException e) {
							// Digest...don't log
						}
					}
				}
			}
		}
		for (Iterator<Integer> iterator = numberList.iterator(); iterator
				.hasNext();) {
			if (!numberList.contains(Integer.valueOf(uniqueMemberID))) {
				if (uniqueMemberID != 1) {
					newName.append(uniqueMemberID);
				}
				break;
			}
			uniqueMemberID++;
		}
		return newName.toString();
	}
}
