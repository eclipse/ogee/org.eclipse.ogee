/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;

public class ODataCollapsibleFeature extends AbstractCustomFeature {

	/**
	 * @param fp
	 */
	public ODataCollapsibleFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public String getName() {
		return Messages.ODATAEDITOR_EXPANDCOLLAPSE_LABEL;
	}

	@Override
	public boolean canExecute(ICustomContext context) {

		boolean canExecute = false;
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			PictogramElement pe = pes[0];
			canExecute = PropertyUtil.isTitle(pe);

		}
		return canExecute;
	}

	@Override
	public void execute(ICustomContext context) {

		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			PictogramElement pe = pes[0];
			if (PropertyUtil.isTitle(pe)) {
				PictogramElement parentPE = (PictogramElement) pe.eContainer();
				EList<Shape> children = ((ContainerShape) parentPE)
						.getChildren();
				int index = children.indexOf(pe);
				Shape shape = children.get(index + 1);
				if (shape instanceof ContainerShape) {
					ContainerShape rectShape = (ContainerShape) shape;
					if (PropertyUtil.isExpanded(pe)) {
						PropertyUtil.setCollapsed(pe);
						PropertyUtil.setCollapsed(rectShape);

					} else {
						PropertyUtil.setExpanded(pe);
						PropertyUtil.setExpanded(rectShape);

					}
					ODataShapeUtil.updatePictogramElement(((ContainerShape) pe)
							.getChildren().get(0), getFeatureProvider());
					ODataShapeUtil.layoutPictogramElement(parentPE,
							getFeatureProvider());
				}

			}

		}
	}

}
