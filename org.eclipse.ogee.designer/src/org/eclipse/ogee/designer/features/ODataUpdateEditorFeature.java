/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.ogee.designer.contexts.ODataUpdateEditorContext;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataDecoratorUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.NavigationProperty;

public class ODataUpdateEditorFeature extends AbstractCustomFeature {

	private IFeatureProvider fp;

	/**
	 * This class is used to update the editor with the change of the related
	 * parameters in the tab property It by-passes the ODataUpdateFeature even
	 * if it updates in the editor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 * 
	 */
	public ODataUpdateEditorFeature(IFeatureProvider fp) {
		super(fp);
		this.fp = fp;
	}

	@Override
	public void execute(ICustomContext context) {

		EObject affectedShowUsageObj = null;
		EObject affectedShowPrevUsageObj = null;

		if (context instanceof ODataUpdateEditorContext) {

			ODataUpdateEditorContext editorContext = (ODataUpdateEditorContext) context;
			Object affectedObject = editorContext.getAffectedObject();

			Object prevValue = editorContext.getPrevValue();

			Object newValue = editorContext.getNewValue();

			if (affectedObject instanceof NavigationProperty
					&& newValue instanceof Association) {

				final NavigationProperty navProperty = (NavigationProperty) affectedObject;
				final Association prevAssociation = (Association) prevValue;
				final Association newAssociation = (Association) newValue;

				ODataDecoratorUtil.updateDecorators(getFeatureProvider(),
						navProperty, prevAssociation, newAssociation);

			} else {

				if (ArtifactUtil.isComplexTypeProperty(affectedObject)) {
					if (prevValue != null) {
						affectedShowPrevUsageObj = ((ComplexTypeUsage) prevValue)
								.getComplexType();
					}
					affectedShowUsageObj = ((ComplexTypeUsage) newValue)
							.getComplexType();

				} else if (ArtifactUtil.isEnumTypeProperty(affectedObject)) {

					if (prevValue != null) {
						affectedShowPrevUsageObj = ((EnumTypeUsage) prevValue)
								.getEnumType();
					}
					affectedShowUsageObj = ((EnumTypeUsage) newValue)
							.getEnumType();

				} else if (ArtifactUtil
						.isFunctionImportReturnType(affectedObject)
						|| ArtifactUtil.isParameter(affectedObject)) {

					affectedShowUsageObj = ArtifactUtil
							.getObjectFromTypeUsage((EObject) newValue);
					affectedShowPrevUsageObj = ArtifactUtil
							.getObjectFromTypeUsage((EObject) prevValue);

				} else {
					affectedShowPrevUsageObj = (EObject) prevValue;
					affectedShowUsageObj = (EObject) newValue;
				}

				if (affectedShowUsageObj != null
						|| affectedShowPrevUsageObj != null) {

					ODataShapeUtil.updateShowUsageLinks(this.fp,
							affectedShowPrevUsageObj, affectedShowUsageObj);
				}
			}

		}

	}

}
