/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICopyContext;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.ui.features.AbstractCopyFeature;
import org.eclipse.ogee.designer.utils.ArtifactContainer;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.PropertyContainer;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;

/**
 * This class will provide functionality to copy the selected diagram element or
 * business objects to clipboard
 * 
 */
public class ODataCopyFeature extends AbstractCopyFeature {

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 */
	public ODataCopyFeature(IFeatureProvider fp) {
		super(fp);
	}

	/**
	 * Copy the diagram elements to clipboard
	 * 
	 * @param context
	 *            ICopyContext instance
	 * @see org.eclipse.graphiti.features.ICopyFeature#execute(org.eclipse.graphiti.features.context.ICopyContext)
	 */
	@Override
	public void copy(ICopyContext context) {
		// get the business-objects for all pictogram-elements
		PictogramElement[] pes = context.getPictogramElements();
		Object[] bos = new Object[pes.length];
		Object containerObject = null;
		EObject copiedObj = null;
		PictogramElement pe = null;
		boolean isKey = false;
		Schema schema = null;
		EntityType baseType = null;
		for (int i = 0; i < pes.length; i++) {
			pe = pes[i];
			bos[i] = getBusinessObjectForPictogramElement(pe);
			// Copy Eobject before putting to clipboard
			copiedObj = EcoreUtil.copy((EObject) bos[i]);
			// Property object is now added to property container
			if (bos[i] instanceof Property) {
				isKey = ArtifactUtil.isKeyProperty(bos[i]);
				containerObject = ((EObject) bos[i]).eContainer().eContainer();
				if (containerObject != null
						&& containerObject instanceof Schema) {
					schema = (Schema) containerObject;
					bos[i] = new PropertyContainer((Property) copiedObj, isKey,
							schema);
				}
			} else {

				if (bos[i] instanceof EntityType) {
					baseType = ((EntityType) bos[i]).getBaseType();
					containerObject = ((EObject) bos[i]).eContainer();
				} else if (bos[i] instanceof ComplexType) {
					containerObject = ((EObject) bos[i]).eContainer();
				} else if (bos[i] instanceof FunctionImport) {
					containerObject = ((EObject) bos[i]).eContainer()
							.eContainer();
				} else { // parameters and return type
					containerObject = ((EObject) bos[i]).eContainer()
							.eContainer().eContainer();
				}
				if (containerObject != null
						&& containerObject instanceof Schema) {
					schema = (Schema) containerObject;
					bos[i] = new ArtifactContainer(schema, copiedObj, baseType);

				}
			}
		}
		// put all business objects to the clipboard
		putToClipboard(bos);

	}

	/**
	 * Returns if a selected diagram element can be copied(The feature can be
	 * executed).
	 * 
	 * @param context
	 *            ICopyContext instance is passed
	 */
	@Override
	public boolean canCopy(ICopyContext context) {

		final PictogramElement[] pes = context.getPictogramElements();
		/* nothing is selected */
		if (pes == null || pes.length == 0) {
			return false;
		}
		Object bo = null;
		for (PictogramElement pe : pes) {
			bo = getBusinessObjectForPictogramElement(pe);
			if ((bo instanceof EntityType) || (bo instanceof ComplexType)
					|| (bo instanceof FunctionImport)) {
				if (pe.eContainer() instanceof Diagram)
					return true;
				else
					return false;
			} else if (bo instanceof Property || bo instanceof Parameter) {
				return true;
			} else if (bo instanceof ComplexTypeUsage
					|| bo instanceof SimpleTypeUsage
					|| bo instanceof ReturnEntityTypeUsage) {
				return true;
			}
		}
		return false;
	}
}
