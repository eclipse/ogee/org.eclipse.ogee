package org.eclipse.ogee.designer.features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.graphiti.datatypes.ILocation;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IPasteContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.features.context.impl.DeleteContext;
import org.eclipse.graphiti.features.context.impl.MultiDeleteInfo;
import org.eclipse.graphiti.internal.datatypes.impl.LocationImpl;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.editor.DiagramEditorInput;
import org.eclipse.graphiti.ui.features.AbstractPasteFeature;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PlatformUI;

import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.ODataEditorInput;
import org.eclipse.ogee.designer.ODataMultiPageEditor;
import org.eclipse.ogee.designer.api.IODataDiagramCreator;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataFeatureProvider;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.types.FunctionImportReturnType;
import org.eclipse.ogee.designer.utils.ArtifactContainer;
import org.eclipse.ogee.designer.utils.ArtifactNameUtil;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.designer.utils.ODataPropertyComparator;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyContainer;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.EntitySetImpl;
import org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl;
import org.eclipse.ogee.model.odata.impl.ParameterImpl;
import org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.SimpleTypeUsageImpl;
import org.eclipse.ogee.utils.logger.Logger;

/**
 * This class will provide functionality to paste the copied diagram elements
 * 
 * @author I070981,I072748,I035983
 * 
 */
public class ODataPasteFeature extends AbstractPasteFeature {

	private HashMap<String, List<EObject>> propertyObjects = new HashMap<String, List<EObject>>();
	private HashMap<String, EObject> funcImportObjects = new HashMap<String, EObject>();
	private HashMap<String, EObject> newReferencedObjects = new HashMap<String, EObject>();

	private static final Logger logger = Logger.getLogger(Activator.PLUGIN_ID);
	/**
	 * returns true if selected canvas is same as copied business objects
	 * otherwise false.
	 */
	private boolean isSameCanvas;
	/**
	 * Return type usage
	 */
	private Object copiedreturnType;

	/**
	 * artifact factory
	 */
	private ArtifactFactory artifactFactory;
	/**
	 * shapes factory
	 */
	private ShapesFactory shapesFactory;

	private IFeatureProvider fp;

	/**
	 * Constructor
	 * 
	 * @param fp
	 *            Feature provider instance is passed
	 */

	public ODataPasteFeature(IFeatureProvider fp) {
		super(fp);
		this.fp = fp;
		this.artifactFactory = ((ODataFeatureProvider) fp).getArtifactFactory();
		this.shapesFactory = ((ODataFeatureProvider) fp).getShapesFactory();
	}

	/**
	 * Returns if a diagram element can be pasted(The feature can be executed).
	 * In this case this has to be done only in case of Entity Types or Complex
	 * Types, Properties, Function Import.
	 * 
	 * @param context
	 *            IPasteContext instance is passed
	 */
	@Override
	public boolean canPaste(IPasteContext context) {

		// Paste disabled for Read only model
		if (isReadOnlyModel()) {
			return false;
		}
		// only support pasting directly in the diagram (nothing else selected)
		PictogramElement[] pes = context.getPictogramElements();
		Object businessObj = getBusinessObjectForPictogramElement(pes[0]);

		Object[] fromClipboard = getFromClipboard();

		boolean hasOnlyTopArtifacts = false;
		boolean hasOnlyProperties = false;
		boolean hasOnlyParameters = false;
		boolean hasOnlyReturnType = false;

		boolean hasBoth = false;

		if (fromClipboard == null || fromClipboard.length == 0) {
			return false;
		}
		ArtifactContainer eObjConatiner = null;
		EObject eObj = null;
		PictogramElement pe = null;
		for (Object object : fromClipboard) {
			if (object instanceof ArtifactContainer) {
				eObjConatiner = (ArtifactContainer) object;
				eObj = eObjConatiner.geteObject();
				if (eObj instanceof EntityType || eObj instanceof ComplexType
						|| eObj instanceof FunctionImport) {
					if (hasOnlyProperties || hasOnlyParameters
							|| hasOnlyReturnType) {
						hasBoth = true;
					} else {
						hasOnlyTopArtifacts = true;
					}

				} else if (eObj instanceof ComplexTypeUsage
						|| eObj instanceof SimpleTypeUsage
						|| eObj instanceof ReturnEntityTypeUsage) {

					if (hasOnlyTopArtifacts || hasOnlyProperties) {
						hasBoth = true;
					} else {
						hasOnlyReturnType = true;
					}
				} else if (eObj instanceof Parameter) {
					if (hasOnlyTopArtifacts || hasOnlyProperties) {
						hasBoth = true;
					} else {
						hasOnlyParameters = true;
					}

				}
			} else if (object instanceof PropertyContainer) {
				if (hasOnlyTopArtifacts || hasOnlyParameters
						|| hasOnlyReturnType) {
					hasBoth = true;
				} else {
					hasOnlyProperties = true;
				}
			}
		}
		if (hasBoth) {
			return false;
		}
		if (hasOnlyProperties) {
			if (businessObj instanceof EntityType
					|| businessObj instanceof ComplexType
					|| businessObj instanceof Property
					|| businessObj instanceof EntitySetImpl
					|| businessObj instanceof NavigationPropertyImpl) {

				return true;
			}
		}
		if (hasOnlyTopArtifacts) {
			return isDiagramElement(pes[0]);
		}
		if (hasOnlyParameters) {
			if (businessObj instanceof ComplexTypeUsage
					|| businessObj instanceof SimpleTypeUsage
					|| businessObj instanceof ReturnEntityTypeUsage
					|| businessObj instanceof ParameterImpl
					|| businessObj instanceof FunctionImport) {
				return true;

			}
		}
		if (hasOnlyReturnType) {
			if (businessObj instanceof ComplexTypeUsage
					|| businessObj instanceof SimpleTypeUsage
					|| businessObj instanceof ReturnEntityTypeUsage
					|| businessObj instanceof ParameterImpl) {

				pe = (PictogramElement) pes[0].eContainer().eContainer();
				businessObj = getBusinessObjectForPictogramElement(pe);
			}
			if (businessObj instanceof FunctionImport) {
				if (((FunctionImport) businessObj).getReturnType() == null) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * This method returns true selected shape is diagram element and false if
	 * its section headers
	 * 
	 * @param pe
	 * @return boolean
	 */
	private boolean isDiagramElement(PictogramElement pe) {
		if (pe.eContainer() instanceof Diagram || pe instanceof Diagram) {
			return true;
		}
		return false;
	}

	/**
	 * Check for copied diagram elements and paste the object
	 * 
	 * @param context
	 *            IPasteContext instance
	 * @see org.eclipse.graphiti.features.IPasteFeature#execute(org.eclipse.graphiti.features.context.IPasteContext)
	 */
	@Override
	public void paste(IPasteContext context) {
		Object[] objects = getFromClipboard();
		PictogramElement[] pes = context.getPictogramElements();
		// Check for canvas where objects need to be pasted whether it's same as
		// copied object or not.
		this.isSameCanvas = getVisibleCanvas(objects, pes[0]);

		ArtifactContainer eObjContainer = null;
		Schema schema = null;
		EntityType baseType = null;
		EObject eObj = null;
		PropertyContainer propContainer = null;
		Property propObj = null;
		boolean isKeyProperty = false;
		for (Object object : objects) {
			if (object instanceof ArtifactContainer) {
				eObjContainer = (ArtifactContainer) object;
				if (this.isSameCanvas) {
					schema = eObjContainer.getSchema();
				} else {
					schema = getSchemaForDiagramElement(pes[0]);
				}

				baseType = eObjContainer.getBaseType();
				eObj = eObjContainer.geteObject();
				if (eObj instanceof EntityType) {

					pasteEntityType(pes, eObj, schema, context, baseType);
				} else if (eObj instanceof ComplexType) {

					pasteComplexType(pes, eObj, schema, context);
				} else if (eObj instanceof FunctionImport) {
					pasteFuntionImport(pes, eObj, schema, context);
				} else if (eObj instanceof Parameter) {
					pasteParameter(pes, eObj);
				} else if (eObj instanceof ReturnEntityTypeUsage
						|| eObj instanceof SimpleTypeUsage
						|| eObj instanceof ComplexTypeUsage) {
					pasteFunctionImportReturnType(pes, eObj);
				}
			} else if (object instanceof PropertyContainer) {
				propContainer = (PropertyContainer) object;
				propObj = propContainer.getProperty();
				isKeyProperty = propContainer.isKey();
				pasteProperty(pes, propObj, isKeyProperty);
			}

		}

		createLink();
		if (!this.isSameCanvas) {
			// if its different canvas then refresh all the artifacts including
			// the return types etc.
			refreshAllArtifacts();
		}
	}

	/**
	 * This method will check for any referenced object for all the artifacts
	 * and set it respectively. This method will be called only in case of
	 * different canvas.
	 */
	private void refreshAllArtifacts() {

		if (!this.funcImportObjects.isEmpty()) {
			String name = null;
			EObject newRefObj = null;
			ComplexType ctype = null;
			EntityType etype = null;
			FunctionImport funcImport = null;

			List<String> names = new ArrayList<String>(
					this.funcImportObjects.keySet());
			for (ListIterator<String> it = names.listIterator(); it.hasNext();) {
				name = it.next();
				funcImport = (FunctionImport) this.funcImportObjects.get(name);
				if (!this.newReferencedObjects.isEmpty()
						&& this.newReferencedObjects.containsKey(name)) {

					newRefObj = this.newReferencedObjects.get(name);
					if (newRefObj instanceof EntityType) {
						etype = (EntityType) newRefObj;
						((ReturnEntityTypeUsage) funcImport.getReturnType())
								.setEntityType(etype);
						ODataLinkUsageFeature linkUsageFeature = new ODataLinkUsageFeature(
								fp,
								((ODataFeatureProvider) fp)
										.getArtifactFactory(),
								((ODataFeatureProvider) fp).getShapesFactory(),
								true);
						final EList<Shape> shapes = this.getDiagram()
								.getChildren();
						if (!shapes.isEmpty()) {
							for (Shape shape : shapes) {
								if (PropertyUtil.isTopContainer(shape)) {
									CustomContext customContext = new CustomContext();
									customContext
											.setPictogramElements(new PictogramElement[] { shape });
									if (linkUsageFeature
											.canExecute(customContext)) {
										if (PropertyUtil.isShowUsage(shape)) {
											linkUsageFeature
													.execute(customContext);
										}
									}

								}
							}
						}

					} else if (newRefObj instanceof ComplexType) {
						ctype = (ComplexType) newRefObj;
						((ComplexTypeUsage) funcImport.getReturnType())
								.setComplexType(ctype);

						createLink();
					}

				} else if (!(funcImport.getReturnType() instanceof SimpleTypeUsage)) {
					deleteReturnType(funcImport);
				}
			}

		}

		if (!this.propertyObjects.isEmpty()) {

			String name = null;
			EObject newRefObj = null;
			ComplexType ctype = null;
			Property property = null;
			List<EObject> propObjects = null;
			List<String> names = new ArrayList<String>(
					this.propertyObjects.keySet());

			for (ListIterator<String> it = names.listIterator(); it.hasNext();) {
				name = it.next();
				propObjects = this.propertyObjects.get(name);
				for (EObject ob : propObjects) {
					if (ob instanceof Property) {
						property = (Property) ob;

						if (!this.newReferencedObjects.isEmpty()
								&& this.newReferencedObjects.containsKey(name)) {

							newRefObj = this.newReferencedObjects.get(name);
							if (newRefObj instanceof ComplexType) {
								ctype = (ComplexType) newRefObj;
								((ComplexTypeUsage) property.getType())
										.setComplexType(ctype);
							}

							createLink();
						} else {
							((ComplexTypeUsage) property.getType())
									.setComplexType(null);
						}
					}
				}
			}
		}
	}

	/**
	 * This method will create link for Show usage feature when paste is done on
	 * different canvas.
	 */
	private void createLink() {
		ODataLinkUsageFeature linkUsageFeature = new ODataLinkUsageFeature(fp,
				((ODataFeatureProvider) fp).getArtifactFactory(),
				((ODataFeatureProvider) fp).getShapesFactory(), true);
		final EList<Shape> shapes = this.getDiagram().getChildren();
		if (!shapes.isEmpty()) {
			for (Shape shape : shapes) {
				if (PropertyUtil.isTopContainer(shape)) {
					CustomContext customContext = new CustomContext();
					customContext
							.setPictogramElements(new PictogramElement[] { shape });
					if (linkUsageFeature.canExecute(customContext)) {
						if (PropertyUtil.isShowUsage(shape)) {
							linkUsageFeature.execute(customContext);
						}
					}

				}
			}
		}
	}

	/**
	 * This method will delete the return type in provided function import. This
	 * method will be only called in case of different schema.
	 * 
	 * @param func
	 */
	private void deleteReturnType(FunctionImport func) {
		PictogramElement parent = null;
		PictogramElement returnTypeContaineShape = null;
		boolean canDelete = false;
		DeleteContext delcontext = null;
		ODataDeleteFeature delfeature = null;
		parent = getFeatureProvider()
				.getPictogramElementForBusinessObject(func);

		returnTypeContaineShape = getFirstChildPictogramElement(parent,
				PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_RECTANGLE);

		delcontext = new DeleteContext(returnTypeContaineShape);
		delcontext.setMultiDeleteInfo(new MultiDeleteInfo(false, false, 1));
		delfeature = (ODataDeleteFeature) getFeatureProvider()
				.getDeleteFeature(delcontext);
		canDelete = delfeature.canDelete(delcontext);
		if (canDelete) {
			// Add the warning to the error log
			logger.logWarning(NLS.bind(
					Messages.ODATAPASTE_FUNCTIONIMPORT_RETURNTYPE_NOTCOPIED,
					func.getName()));

			delfeature.execute(delcontext);
		}
	}

	/**
	 * This method will return the return type present in provided Function
	 * Import pictogram element
	 * 
	 * @param mainPictogramElement
	 * @param propertyName
	 * @return return type in Function Import
	 */
	private static PictogramElement getFirstChildPictogramElement(
			PictogramElement mainPictogramElement, String propertyName) {

		EList<Shape> children = ((ContainerShape) mainPictogramElement)
				.getChildren();
		EList<Shape> subChildren = null;
		for (Shape shape : children) {

			if (propertyName.equals(PropertyUtil.getID(shape))) {

				if (shape instanceof ContainerShape) {
					subChildren = ((ContainerShape) shape).getChildren();
					if (subChildren != null) {
						return subChildren.get(0);
					}
				}
			}
		}
		return null;
	}

	/**
	 * This method will return the current schema
	 * 
	 * @param pe
	 * @return schema
	 */
	private Schema getSchemaForDiagramElement(PictogramElement pe) {

		Schema schema = null;
		EDMXSet edmxSet = null;
		if ((pe instanceof Diagram) || (pe instanceof EObject)) {
			if ((pe.eResource() != null)
					&& (pe.eResource().getContents().size() > 1)) {
				Object edmxSetObj = pe.eResource().getContents().get(1);

				if (edmxSetObj instanceof EDMXSet) {
					edmxSet = (EDMXSet) edmxSetObj;
					schema = ArtifactUtil.getDefaultSchema(edmxSet);
				}
			}
		}
		return schema;

	}

	/**
	 * This method will paste Entity Type diagram element
	 * 
	 * @param pes
	 * @param object
	 * @param schema
	 * @param context
	 * @param baseType
	 */
	private void pasteEntityType(PictogramElement[] pes, EObject object,
			Schema schema, IPasteContext context, EntityType baseType) {
		EntityType newEntity = null;
		List<EntityType> entities = null;
		List<String> entityNames = null;
		String entityName = null;

		newEntity = (EntityType) object;
		PictogramElement pe = getPictogarmElementInListForGivenObject(pes,
				newEntity);

		/*
		 * get the copy of entity type from entity type copied on clipboard
		 */
		EntityType copiedEntityType = EcoreUtil.copy(newEntity);
		if (copiedEntityType != null) {
			copiedEntityType.getNavigationProperties().clear();
			copiedEntityType.setBaseType(baseType);
			EObject objcontainer = schema;
			if (objcontainer instanceof Schema) {
				entities = ((Schema) objcontainer).getEntityTypes();
				entityNames = new ArrayList<String>();
				String en = null;
				EntityType etype = null;
				for (ListIterator<EntityType> it = entities.listIterator(); it
						.hasNext();) {
					etype = it.next();
					en = etype.getName();
					entityNames.add(en);

				}
				entityName = ArtifactNameUtil.generateUniqueName(entityNames,
						newEntity.getName(), IODataEditorConstants.MAX_LENGTH);
				copiedEntityType.setName(entityName);
				this.artifactFactory.createDefaultEntitySet(copiedEntityType);
				entities.add(copiedEntityType);
			}
			if (!this.isSameCanvas) {

				if (!newEntity.getName().equals(copiedEntityType.getName())) {

					// Add the warning to the error log
					logger.logWarning(NLS.bind(
							Messages.ODATAPASTE_ENTITYTYPE_RENAME,
							newEntity.getName(), copiedEntityType.getName()));
				}
				this.newReferencedObjects.put(newEntity.getName(),
						copiedEntityType);
				List<Property> propertyList = new ArrayList<Property>();
				propertyList.addAll(copiedEntityType.getProperties());
				Property prop = null;
				for (ListIterator<Property> it = propertyList.listIterator(); it
						.hasNext();) {
					prop = it.next();
					addPropertyToHashMap(prop);
				}
			}
		}
		addCopiedObjectToContext(context, copiedEntityType, pe);
	}

	/**
	 * This method will paste ComplexType Diagram Element.
	 * 
	 * @param pes
	 * @param object
	 * @param schema
	 * @param context
	 */
	private void pasteComplexType(PictogramElement[] pes, EObject object,
			Schema schema, IPasteContext context) {
		String complexTypeName = null;
		ComplexType newComplexType = (ComplexType) object;
		PictogramElement pe = getPictogarmElementInListForGivenObject(pes,
				newComplexType);
		/*
		 * get the copy of complex type from complex type copied on clipboard
		 */
		ComplexType copiedComplexType = EcoreUtil.copy(newComplexType);
		if (copiedComplexType != null) {
			EObject objContainer = schema;
			if (objContainer instanceof Schema) {

				List<ComplexType> complexType = ((Schema) objContainer)
						.getComplexTypes();
				List<String> complexTypeNames = new ArrayList<String>();
				String cn = null;
				ComplexType ctype = null;
				for (ListIterator<ComplexType> it = complexType.listIterator(); it
						.hasNext();) {
					ctype = it.next();
					cn = ctype.getName();
					complexTypeNames.add(cn);
				}
				complexTypeName = ArtifactNameUtil.generateUniqueName(
						complexTypeNames, newComplexType.getName(),
						IODataEditorConstants.MAX_LENGTH);
				copiedComplexType.setName(complexTypeName);
				((Schema) objContainer).getComplexTypes()
						.add(copiedComplexType);
			}

			if (!this.isSameCanvas) {

				if (!newComplexType.getName().equals(
						copiedComplexType.getName())) {
					// Add the warning to the error log
					logger.logWarning(NLS.bind(
							Messages.ODATAPASTE_COMPLEXTYPE_RENAME,
							newComplexType.getName(),
							copiedComplexType.getName()));
				}
				this.newReferencedObjects.put(newComplexType.getName(),
						copiedComplexType);

				List<Property> propertyList = new ArrayList<Property>();
				propertyList.addAll(copiedComplexType.getProperties());
				Property prop = null;
				for (ListIterator<Property> it = propertyList.listIterator(); it
						.hasNext();) {
					prop = it.next();
					addPropertyToHashMap(prop);
				}
			}
			addCopiedObjectToContext(context, copiedComplexType, pe);
		}
	}

	/**
	 * This method will add the provided property to hash map.
	 * 
	 * @param prop
	 */
	private void addPropertyToHashMap(Property prop) {
		if (prop.getType() instanceof ComplexTypeUsage) {
			ComplexType cType = ((ComplexTypeUsage) prop.getType())
					.getComplexType();
			if (cType != null) {
				List<EObject> propObjects = this.propertyObjects.get(cType
						.getName());
				List<EObject> propList = new ArrayList<EObject>();
				if (propObjects != null) {
					propList.addAll(propObjects);
				}
				propList.add(prop);
				this.propertyObjects.put(cType.getName(), propList);

			}
		}
	}

	/**
	 * This method will paste Function Import diagram element
	 * 
	 * @param pes
	 * @param object
	 * @param schema
	 * @param context
	 */

	private void pasteFuntionImport(PictogramElement[] pes, EObject object,
			Schema schema, IPasteContext context) {
		List<FunctionImport> functionImport = new ArrayList<FunctionImport>();
		FunctionImport newFunctionImport = (FunctionImport) object;
		PictogramElement pe = getPictogarmElementInListForGivenObject(pes,
				newFunctionImport);
		/*
		 * get the copy of function import from the function import copied on
		 * clipboard
		 */
		FunctionImport copiedFunctionImport = EcoreUtil.copy(newFunctionImport);
		if (copiedFunctionImport != null) {

			if (schema.getContainers().size() == 0) {
				EntityContainer entityContainer = OdataFactory.eINSTANCE
						.createEntityContainer();
				entityContainer.setName(schema.getNamespace());
				entityContainer.setDefault(true);
				schema.getContainers().add(entityContainer);
			}

			for (int i = 0; i < schema.getContainers().size(); i++) {
				functionImport.addAll((schema.getContainers().get(i))
						.getFunctionImports());
				(schema.getContainers().get(i)).getFunctionImports().add(
						copiedFunctionImport);
			}

			String fn = null;
			List<String> funcImportNames = new ArrayList<String>();
			FunctionImport f = null;
			for (ListIterator<FunctionImport> it = functionImport
					.listIterator(); it.hasNext();) {
				f = it.next();
				fn = f.getName();
				funcImportNames.add(fn);

			}

			String functionImportName = ArtifactNameUtil.generateUniqueName(
					funcImportNames, newFunctionImport.getName(),
					IODataEditorConstants.MAX_LENGTH);
			copiedFunctionImport.setName(functionImportName);

			if (!this.isSameCanvas) {

				if (!newFunctionImport.getName().equals(
						copiedFunctionImport.getName())) {
					// Add the warning to the error log
					logger.logWarning(NLS.bind(
							Messages.ODATAPASTE_FUNCTIONIMPORT_RENAME,
							newFunctionImport.getName(),
							copiedFunctionImport.getName()));
				}
			}
			if (copiedFunctionImport.getReturnType() != null) {
				if (copiedFunctionImport.getReturnType() instanceof ReturnEntityTypeUsage) {

					EntityType eType = ((ReturnEntityTypeUsage) copiedFunctionImport
							.getReturnType()).getEntityType();
					if (eType != null) {
						if (this.isSameCanvas) {
							EList<EntityType> entityTypeList = schema
									.getEntityTypes();
							if (entityTypeList.isEmpty()
									|| !entityTypeList.contains(eType)) {
								((ReturnEntityTypeUsage) copiedFunctionImport
										.getReturnType()).setEntityType(null);
								((ReturnEntityTypeUsage) copiedFunctionImport
										.getReturnType()).setEntitySet(null);
							}
						} else {
							this.funcImportObjects.put(eType.getName(),
									copiedFunctionImport);
						}
					}
				} else if (copiedFunctionImport.getReturnType() instanceof ComplexTypeUsage) {
					ComplexType cType = ((ComplexTypeUsage) copiedFunctionImport
							.getReturnType()).getComplexType();
					if (cType != null) {
						if (this.isSameCanvas) {
							EList<ComplexType> complexTypeList = schema
									.getComplexTypes();
							if (complexTypeList.isEmpty()
									|| !complexTypeList.contains(cType)) {
								((ComplexTypeUsage) copiedFunctionImport
										.getReturnType()).setComplexType(null);
							}
						} else {
							this.funcImportObjects.put(cType.getName(),
									copiedFunctionImport);
						}
					}
				}

			}
			addCopiedObjectToContext(context, copiedFunctionImport, pe);
		}

	}

	/**
	 * This method will paste return type parameters in the same or different
	 * function import object
	 * 
	 * @param pes
	 * @param object
	 */
	private void pasteFunctionImportReturnType(PictogramElement[] pes,
			Object object) {
		AddContext addContext;
		FunctionImport funcImp = null;
		SimpleType type = null;
		// For section Headers and objects
		PictogramElement mainPictogramElement = getTopPictogramElement(pes[0]);
		Object bo = getBusinessObjectForPictogramElement(mainPictogramElement);
		if (bo instanceof FunctionImport) {
			funcImp = (FunctionImport) bo;
		}
		FunctionImportReturnType returnType = null;
		if (object instanceof ReturnEntityTypeUsage) {
			returnType = FunctionImportReturnType.ENTITY;
		} else if (object instanceof SimpleTypeUsage) {
			returnType = FunctionImportReturnType.SIMPLE;
			type = ((SimpleTypeUsage) object).getSimpleType();
		} else if (object instanceof ComplexTypeUsage) {
			returnType = FunctionImportReturnType.COMPLEX;
		}
		this.copiedreturnType = this.artifactFactory.createReturnType(funcImp,
				returnType, type);
		if (pes != null && pes.length == 1) {
			if (bo instanceof FunctionImport) {
				EList<Shape> children = ((ContainerShape) mainPictogramElement)
						.getChildren();
				IdentifierShape identifierShape = null;
				ContainerShape sectionShape;
				Shape titleImage;
				for (Shape shape : children) {
					/*
					 * Create new return type and its shape in the specified
					 * area
					 */
					if (PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_RECTANGLE
							.equals(PropertyUtil.getID(shape))) {
						sectionShape = (ContainerShape) shape;
						addContext = new AddContext();
						addContext.setNewObject(this.copiedreturnType);
						identifierShape = this.shapesFactory
								.createIdentifierShape(
										sectionShape,
										getNameForReturnType(),
										getImageForReturnType(),
										addContext,
										this.copiedreturnType,
										false,
										PropertyUtil.FUNCTION_IMPORT_RETURN_TYPE_NAME);
						titleImage = ODataLayoutUtil
								.expandSection(sectionShape);
						if (titleImage != null) {
							ODataShapeUtil.updatePictogramElement(titleImage,
									getFeatureProvider());
						}
						/* Update the layout */
						ODataShapeUtil.layoutPictogramElement(
								mainPictogramElement, getFeatureProvider());
						if (this.copiedreturnType != null) {
							// in order to refresh the Graphiti view
							IODataDiagramCreator.INSTANCE.refreshEditor(
									this.copiedreturnType, null,
									this.copiedreturnType);
						}
						identifierShape.updateSelection();
						identifierShape.enableDirectEdit();
						break;
					}
				}
			}
		}
	}

	/**
	 * This method will paste the property within same object or different
	 * objects
	 * 
	 * @param pes
	 * @param object
	 */
	private void pasteProperty(PictogramElement[] pes, Object object,
			boolean isKeyProperty) {
		AddContext addContext;
		Property newProperty = (Property) object;
		/* get the copy of property from the property copied on clipboard */
		Property copiedProperty = EcoreUtil.copy(newProperty);
		if (copiedProperty != null) {
			String imageId = ODataImageProvider.IMG_PROPERTY;
			Object propTypeObj = ((Property) copiedProperty).getType();
			if (propTypeObj instanceof ComplexTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_CMPLX_TYPE;

				if (!this.isSameCanvas) {
					addPropertyToHashMap(copiedProperty);
				}
			}
			PictogramElement mainPictogramElement = getTopPictogramElement(pes[0]);
			Object bo = getBusinessObjectForPictogramElement(mainPictogramElement);
			if (bo instanceof EntityType) {
				EntityType selectedEntityType = (EntityType) bo;
				List<Property> propertyList = new ArrayList<Property>();
				propertyList.addAll(selectedEntityType.getProperties());
				propertyList.addAll(selectedEntityType.getKeys());
				List<String> propertyNames = new ArrayList<String>();
				String pn = null;
				Property p = null;
				for (ListIterator<Property> it = propertyList.listIterator(); it
						.hasNext();) {
					p = it.next();
					pn = p.getName();
					propertyNames.add(pn);
				}
				String newPropName = ArtifactNameUtil.generateUniqueName(
						propertyNames, newProperty.getName(),
						IODataEditorConstants.MAX_LENGTH);
				copiedProperty.setName(newPropName);

				EList<Shape> children = ((ContainerShape) mainPictogramElement)
						.getChildren();
				IdentifierShape identifierShape = null;
				ContainerShape sectionShape = null;
				Shape titleImage;
				for (Shape shape : children) {
					/*
					 * Paste copied property and its shape in the specified area
					 */

					if (PropertyUtil.PROPERTY_RECTANGLE.equals(PropertyUtil
							.getID(shape))) {
						sectionShape = (ContainerShape) shape;
						if (isKeyProperty) {
							imageId = ODataImageProvider.IMG_KEY_PROPERTY;
						}
						if (isKeyProperty) {
							selectedEntityType.getKeys().add(copiedProperty);
						} else {
							selectedEntityType.getProperties().add(
									copiedProperty);
						}
						addContext = new AddContext();
						addContext.setNewObject(copiedProperty);
						/* Visual shape */
						identifierShape = this.shapesFactory
								.createPropertyShape(sectionShape, newPropName,
										imageId, addContext, copiedProperty,
										copiedProperty.getType(), false,
										PropertyUtil.ENTITY_PROPERTY_NAME);
						if (isKeyProperty) {
							// For Sorting key and Non key properties
							ECollections.sort(sectionShape.getChildren(),
									new ODataPropertyComparator(
											getFeatureProvider()));
						}
						titleImage = ODataLayoutUtil
								.expandSection(sectionShape);
						if (titleImage != null) {
							ODataShapeUtil.updatePictogramElement(titleImage,
									getFeatureProvider());
						}
						/* Update the layout */
						ODataShapeUtil.layoutPictogramElement(
								mainPictogramElement, getFeatureProvider());
						identifierShape.updateSelection();
						identifierShape.enableDirectEdit();
						break;
					}
				}
			} else if (bo instanceof ComplexType) {
				ComplexType complexType = (ComplexType) bo;
				List<Property> propertyList = new ArrayList<Property>();
				propertyList.addAll(complexType.getProperties());

				List<String> cmplxpropertyNames = new ArrayList<String>();
				String cpn = null;
				Property p = null;
				for (ListIterator<Property> it = propertyList.listIterator(); it
						.hasNext();) {
					p = it.next();
					cpn = p.getName();
					cmplxpropertyNames.add(cpn);
				}
				String newCmplxPropName = ArtifactNameUtil.generateUniqueName(
						cmplxpropertyNames, newProperty.getName(),
						IODataEditorConstants.MAX_LENGTH);
				copiedProperty.setName(newCmplxPropName);

				EList<Shape> children = ((ContainerShape) mainPictogramElement)
						.getChildren();
				IdentifierShape identifierShape = null;
				ContainerShape sectionShape;
				Shape titleImage;
				for (Shape shape : children) {
					/*
					 * Create new property and its shape in the specified area
					 */
					if (PropertyUtil.PROPERTY_RECTANGLE.equals(PropertyUtil
							.getID(shape))) {
						sectionShape = (ContainerShape) shape;

						if (copiedProperty.getType() instanceof ComplexTypeUsage) {
							ComplexTypeUsage cu = (ComplexTypeUsage) copiedProperty
									.getType();
							ComplexType ct = cu.getComplexType();
							if (ct != null) {
								String name = cu.getComplexType().getName();
								if (name.equals(complexType.getName())) {
									cu.setComplexType(null);
								}
							}
						}
						complexType.getProperties().add(copiedProperty);
						addContext = new AddContext();
						addContext.setNewObject(copiedProperty);

						/* Visual shape */
						identifierShape = this.shapesFactory
								.createPropertyShape(sectionShape,
										newCmplxPropName, imageId, addContext,
										copiedProperty,
										copiedProperty.getType(), false,
										PropertyUtil.ENTITY_PROPERTY_NAME);
						titleImage = ODataLayoutUtil
								.expandSection(sectionShape);
						if (titleImage != null) {
							ODataShapeUtil.updatePictogramElement(titleImage,
									getFeatureProvider());
						}
						ODataShapeUtil.layoutPictogramElement(
								mainPictogramElement, getFeatureProvider());
						identifierShape.updateSelection();
						identifierShape.enableDirectEdit();
						break;
					}
				} // for loop ends
			}

		}
	}

	/**
	 * This method will paste function import parameters in the same or
	 * different function import object
	 * 
	 * @param pes
	 * @param object
	 */

	private void pasteParameter(PictogramElement[] pes, Object object) {
		AddContext addContext;
		String parameterName;
		Parameter newParameter = (Parameter) object;
		/* get the copy of parameter from parameter copied on clipboard */
		Parameter copiedParameter = EcoreUtil.copy(newParameter);
		if (pes != null && pes.length == 1 && copiedParameter != null) {

			// For section Headers and objects
			PictogramElement mainPictogramElement = getTopPictogramElement(pes[0]);
			Object bo = getBusinessObjectForPictogramElement(mainPictogramElement);
			if (bo instanceof FunctionImport) {
				// PictogramElement mainPictogramElement = pes[0];
				FunctionImport funcImp = (FunctionImport) bo;
				List<Parameter> parameterList = new ArrayList<Parameter>();
				parameterList.addAll(funcImp.getParameters());

				List<String> parameterNames = new ArrayList<String>();
				String param = null;
				Parameter p = null;
				for (ListIterator<Parameter> it = parameterList.listIterator(); it
						.hasNext();) {
					p = it.next();
					param = p.getName();
					parameterNames.add(param);
				}
				String newParamName = ArtifactNameUtil.generateUniqueName(
						parameterNames, newParameter.getName(),
						IODataEditorConstants.MAX_LENGTH);
				copiedParameter.setName(newParamName);

				parameterName = copiedParameter.getName();
				if (parameterName != null && parameterName.trim().length() != 0) {
					EList<Shape> children = ((ContainerShape) mainPictogramElement)
							.getChildren();
					IdentifierShape identifierShape = null;
					ContainerShape sectionShape;
					Shape titleImage;
					for (Shape shape : children) {
						/*
						 * Create new function import parameter and its shape in
						 * the specified area
						 */
						if (PropertyUtil.FUNCTION_IMPORT_PARAMETER_RECTANGLE
								.equals(PropertyUtil.getID(shape))) {
							sectionShape = (ContainerShape) shape;
							addContext = new AddContext();
							addContext.setNewObject(copiedParameter);
							funcImp.getParameters().add(copiedParameter);
							identifierShape = this.shapesFactory
									.createIdentifierShape(
											sectionShape,
											parameterName,
											ODataImageProvider.IMG_PARAMETER,
											addContext,
											copiedParameter,
											false,
											PropertyUtil.FUNCTION_IMPORT_PARAMETER_NAME);
							titleImage = ODataLayoutUtil
									.expandSection(sectionShape);
							if (titleImage != null) {
								ODataShapeUtil.updatePictogramElement(
										titleImage, getFeatureProvider());

							}
							ODataShapeUtil.layoutPictogramElement(
									mainPictogramElement, getFeatureProvider());

							identifierShape.updateSelection();
							identifierShape.enableDirectEdit();
							break;
						}
					}
				}
			}
		}
	}

	/**
	 * Returns the image path
	 * 
	 * @return String image path
	 */
	private String getImageForReturnType() {
		String imageId = null;
		if (ArtifactUtil.isCollectionReturnType(this.copiedreturnType)) {
			if (this.copiedreturnType instanceof SimpleTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_PROPERTY_COLLN;
			} else if (this.copiedreturnType instanceof ComplexTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_CPLX_TYP_COLLN;
			} else if (this.copiedreturnType instanceof EnumTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_ENUM_COLLN;
			} else if (this.copiedreturnType instanceof ReturnEntityTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_ENTY_SET_COLLN;
			}
		} else {
			if (this.copiedreturnType instanceof SimpleTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_PROPERTY;
			} else if (this.copiedreturnType instanceof ComplexTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_CMPLX_TYPE;
			} else if (this.copiedreturnType instanceof EnumTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_ENUM;
			} else if (this.copiedreturnType instanceof ReturnEntityTypeUsageImpl) {
				imageId = ODataImageProvider.IMG_ENTITY;
			}
		}
		return imageId;
	}

	/**
	 * Creates a unique name for the new return type
	 * 
	 * @return String new unique name
	 */
	private String getNameForReturnType() {

		String name = null;
		if (this.copiedreturnType != null) {
			if (this.copiedreturnType instanceof SimpleTypeUsage) {
				name = ((SimpleTypeUsage) this.copiedreturnType)
						.getSimpleType().getType().getLiteral();
			} else if (this.copiedreturnType instanceof ComplexTypeUsage) {
				if (((ComplexTypeUsage) this.copiedreturnType).getComplexType() != null)
					name = ((ComplexTypeUsage) this.copiedreturnType)
							.getComplexType().getName();
			} else if (this.copiedreturnType instanceof EnumTypeUsage) {
				if (((EnumTypeUsage) this.copiedreturnType).getEnumType() != null)
					name = ((EnumTypeUsage) this.copiedreturnType)
							.getEnumType().getName();
			} else if (this.copiedreturnType instanceof ReturnEntityTypeUsage) {
				if (((ReturnEntityTypeUsage) this.copiedreturnType)
						.getEntityType() != null)
					name = ((ReturnEntityTypeUsage) this.copiedreturnType)
							.getEntityType().getName();
			}
		}
		if (name == null) {
			name = Messages.ODATAEDITOR_UNDEFINED_NAME;
		}
		return name;

	}

	/**
	 * This method will return top PictogramElement
	 * 
	 * @param pe
	 * @return PictogramElement
	 */
	private PictogramElement getTopPictogramElement(PictogramElement pe) {
		PictogramElement mainPictogramElement = pe;
		if (PropertyUtil.isTitle(pe)
				|| PropertyUtil.isContainerHeader(mainPictogramElement)) {
			mainPictogramElement = (PictogramElement) mainPictogramElement
					.eContainer();
		} else if (!PropertyUtil.isTopContainer(mainPictogramElement)) {
			mainPictogramElement = (PictogramElement) mainPictogramElement
					.eContainer().eContainer();
		}
		return mainPictogramElement;
	}

	/**
	 * This method will add the copied object to context.
	 * 
	 * @param context
	 * @param object
	 */
	private void addCopiedObjectToContext(IPasteContext context, Object object,
			PictogramElement pe) {

		AddContext addContext = new AddContext();
		Diagram diagram = null;
		ILocation newObjLoc;

		if (pe != null) {
			if (pe instanceof Diagram) {
				diagram = (Diagram) pe;
				newObjLoc = new LocationImpl(context.getX(), context.getY());
			} else if (pe instanceof ContainerShape) // Cascading Fix
			{

				diagram = (Diagram) pe.eContainer();
				Shape shape = (Shape) pe;
				newObjLoc = Graphiti.getPeService()
						.getLocationRelativeToDiagram(shape);

			} else {
				diagram = getFeatureProvider().getDiagramTypeProvider()
						.getDiagram();

				newObjLoc = new LocationImpl(PropertyUtil.DIAGRAM_ORIGIN_X,
						PropertyUtil.DIAGRAM_ORIGIN_Y);

			}

			if (newObjLoc != null) {
				newObjLoc = getFinalLocationToPasteObject(newObjLoc);
				addContext.setLocation(newObjLoc.getX(), newObjLoc.getY());

				addContext.setTargetContainer(diagram);
				addContext.setNewObject(object);
				addGraphicalRepresentation(addContext, object);
			}
		}
	}

	/**
	 * This method will return the desired location (after cascading) where
	 * EObject can be pasted.
	 * 
	 * @param locRelToDiagram
	 * @return ILocation(x,y)
	 */
	private ILocation getFinalLocationToPasteObject(ILocation locRelToDiagram) {
		boolean flag = false;
		do {
			locRelToDiagram.setX(locRelToDiagram.getX()
					+ PropertyUtil.CASCADE_CONSTANT);
			locRelToDiagram.setY(locRelToDiagram.getY()
					+ PropertyUtil.CASCADE_CONSTANT);
			flag = isObjectPresent(locRelToDiagram);

		} while (flag);

		return locRelToDiagram;
	}

	/**
	 * This method will return whether object is present in diagram for provided
	 * location.
	 * 
	 * @param desiredLoc
	 * @return boolean
	 */
	private boolean isObjectPresent(ILocation desiredLoc) {
		Diagram currentDiagram = getFeatureProvider().getDiagramTypeProvider()
				.getDiagram();

		EList<Shape> shapes = currentDiagram.getChildren();
		if (shapes.size() > 0) {
			ILocation newObjLoc = null;
			for (Shape s : shapes) {
				if (s instanceof ContainerShape) {
					newObjLoc = Graphiti.getPeService()
							.getLocationRelativeToDiagram(s);

					if (desiredLoc.equals(newObjLoc)) {
						return true;
					}
				}
			}

		}
		return false;
	}

	/**
	 * This method will return PictogramElement for provided EObject in
	 * PictogramElements list.
	 * 
	 * @param pes
	 * @param object
	 * @return PictogramElement
	 */
	private PictogramElement getPictogarmElementInListForGivenObject(
			PictogramElement[] pes, Object object) {

		if (pes != null && pes.length == 1) {
			return pes[0];
		}

		PictogramElement pet = getFeatureProvider()
				.getPictogramElementForBusinessObject(object);

		Shape shape = (Shape) pet;
		ILocation newObjLoc = Graphiti.getPeService()
				.getLocationRelativeToDiagram(shape);

		if (isObjectPresent(newObjLoc)) {
			return pet;
		} else {
			return pes[0];
		}

	}

	/**
	 * This method will check whether selected canvas is same as copied.
	 * 
	 * @param fromClipboard
	 * @param pes
	 * @return true if same canvas is selected else false
	 */
	private boolean getVisibleCanvas(Object[] fromClipboard,
			PictogramElement pes) {
		if ((pes instanceof Diagram) || (pes instanceof EObject)) {
			if ((pes.eResource() != null)
					&& (pes.eResource().getContents().size() > 1)) {
				Object edmxSetObj = pes.eResource().getContents().get(1);

				if (edmxSetObj instanceof EDMXSet) {
					EDMXSet edmxSet = (EDMXSet) edmxSetObj;
					List<Schema> schemata = ArtifactUtil.getSchemas(edmxSet);

					Object copiedObj = fromClipboard[0];
					if (copiedObj instanceof ArtifactContainer) {
						ArtifactContainer eObjConatiner = (ArtifactContainer) copiedObj;
						Schema schema = eObjConatiner.getSchema();
						if (!schemata.contains(schema)) {
							return false;
						}
					} else if (copiedObj instanceof PropertyContainer) {
						PropertyContainer eObjConatiner = (PropertyContainer) copiedObj;
						Schema schema = eObjConatiner.getSchema();
						if (!schemata.contains(schema)) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * Returns true in case of Read Only Model.
	 * 
	 * @return boolean - true, for read-only model.
	 */

	public boolean isReadOnlyModel() {

		boolean isReadOnly = false;

		final IEditorPart editor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (editor instanceof ODataMultiPageEditor) {
			final IEditorInput editorInput = editor.getEditorInput();
			// Editor is invoked from IODataDiagramCreator#createDiagram() API.
			if (editorInput instanceof ODataEditorInput) {

				final URI uri = ((ODataEditorInput) editorInput).getUri();
				final String uriString = uri.trimFragment().toPlatformString(
						true);
				IFile file = ResourcesPlugin.getWorkspace().getRoot()
						.getFile(new Path(uriString));
				if (file.isReadOnly()) {
					isReadOnly = true;
				} else {
					isReadOnly = ((ODataEditorInput) editorInput)
							.getDiagramInput().isReadOnlyMode();
				}
			}
			// Editor is invoked from Project Explorer.
			else if (editorInput instanceof IFileEditorInput) {
				IFile file = ((IFileEditorInput) editorInput).getFile();
				if (file.isReadOnly()) {
					isReadOnly = true;
				} else {
					isReadOnly = ((IFileEditorInput) editorInput).getFile()
							.isReadOnly();
				}
			}
			// Editor is invoked when eclipse IDE is launched.
			else if (editorInput instanceof DiagramEditorInput) {
				final URI uri = ((DiagramEditorInput) editorInput).getUri();
				final String uriString = uri.trimFragment().toPlatformString(
						true);
				IFile file = ResourcesPlugin.getWorkspace().getRoot()
						.getFile(new Path(uriString));
				if (file.isReadOnly()) {
					isReadOnly = true;
				} else {
					isReadOnly = file.isReadOnly();
				}
			}
		}

		return isReadOnly;
	}

}
