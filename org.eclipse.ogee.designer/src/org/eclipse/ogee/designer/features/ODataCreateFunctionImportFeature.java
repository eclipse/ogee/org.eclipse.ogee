/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;

public class ODataCreateFunctionImportFeature extends AbstractCreateFeature {
	/**
	 * ArifactFactory instance used to create all the Business objects.
	 */
	private ArtifactFactory artifactFactory;

	public ODataCreateFunctionImportFeature(IFeatureProvider fp,
			ShapesFactory shapesFactory, ArtifactFactory artifactFactory) {
		super(fp, Messages.ODATAEDITOR_FUNCTION_IMPORT,
				Messages.ODATAEDITOR_FUNCTION_IMPORT_DESC);
		this.artifactFactory = artifactFactory;
	}

	/**
	 * Returns true if the target is Diagram only as the function import can
	 * only be created on a Diagram
	 * 
	 * @param context
	 *            ICreateContext instance
	 * @return boolean true if function import can be created
	 */
	@Override
	public boolean canCreate(ICreateContext context) {

		return context.getTargetContainer() instanceof Diagram;
	}

	@Override
	public Object[] create(ICreateContext context) {

		// Function Import creation
		Object functionImport = this.artifactFactory
				.createDefaultFunctionImport();

		ODataShapeUtil.addGraphicalRepresentation(context, functionImport,
				getFeatureProvider());
		return new Object[] { functionImport };
	}

	@Override
	public String getCreateName() {
		return Messages.ODATAEDITOR_FUNCTION_IMPORT;
	}

	@Override
	public String getCreateDescription() {
		return Messages.ODATAEDITOR_FUNCTION_IMPORT_DESC;
	}

	/**
	 * Returns the Image associated with the object
	 * 
	 * @return String ImagePath
	 */
	@Override
	public String getCreateImageId() {
		return ODataImageProvider.IMG_FUNC_IMP;
	}

	@Override
	public boolean canUndo(IContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDoneChanges() {
		// TODO Auto-generated method stub
		return false;
	}
}
