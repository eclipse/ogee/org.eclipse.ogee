/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.pages;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.service.ODataEditorService;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.exploration.tree.nodes.ServiceNode;
import org.eclipse.ogee.exploration.tree.viewer.ServiceTreeViewer;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.builders.ODataModelEDMXSetBuilder;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.service.validation.Result;
import org.eclipse.ogee.utils.service.validation.Result.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.PlatformUI;

/**
 * The following implementation creates a WizardPage to add EDMX reference
 * into the current domain model.
 * 
 */
public class AddEDMXWizardPage extends WizardPage {

	private Button radioURI;
	private Button radioFile;
	private Text locationText;
	private Button btnGo;
	private Button btnBrowse;
	private ServiceTreeViewer serviceTreeViewer;

	private boolean isURI;
	private EDMXSet currentEdmxSet;
	private Result validationResult;
	private Result referencedEdmxResult;
	private boolean isFileBrowsed;

	protected static final String EMPTY_STRING = ""; //$NON-NLS-1$
	protected static final Map<String, Object> cacheUrlResult = new HashMap<String, Object>();
	protected static final Map<String, Object> cacheFileResult = new HashMap<String, Object>();

	/**
	 * Create the Add EDMX References dialog.
	 * 
	 * @param parentShell
	 * @param edmxSet
	 */
	public AddEDMXWizardPage(Shell parentShell, EDMXSet edmxSet) {
		
		super("AddEDMXWizardPage"); //$NON-NLS-1$
		
		setTitle(Messages.ADD_EDMX_DIALOG_MSG);
		setDescription(Messages.ADD_EDMX_DIALOG_DESC);
		setPageComplete(false);

		this.currentEdmxSet = edmxSet;
		cacheUrlResult.clear();
		cacheFileResult.clear();
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	public void createControl(final Composite parent) {

		final Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		Composite composite_radio = new Composite(container, SWT.NONE);
		GridData gd_composite_radio = new GridData(SWT.LEFT, SWT.FILL, false,
				false, 1, 1);
		gd_composite_radio.widthHint = -1;
		composite_radio.setLayoutData(gd_composite_radio);
		composite_radio.setLayout(new GridLayout(2, false));

		Composite composite_uri = new Composite(container, SWT.NONE);
		composite_uri.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				false, 1, 1));
		composite_uri.setLayout(new GridLayout(3, false));

		// text box for file/url location
		this.locationText = new Text(composite_uri, SWT.BORDER);
		this.locationText.setMessage(Messages.ADD_EDMX_DIALOG_SERVICE_URL_MSG);
		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text.widthHint = -1;
		this.locationText.setLayoutData(gd_text);

		// push button for "Go"
		this.btnGo = new Button(composite_uri, SWT.NONE);
		GridData gd_btnGo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1,
				1);
		gd_btnGo.widthHint = 65;
		this.btnGo.setLayoutData(gd_btnGo);
		this.btnGo.setText(Messages.GO_BUTTON);

		// push button for file browse
		this.btnBrowse = new Button(composite_uri, SWT.NONE);
		GridData gd_btnBrowse = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_btnBrowse.widthHint = -1;
		this.btnBrowse.setLayoutData(gd_btnBrowse);
		this.btnBrowse.setText(Messages.BROWSE_BUTTON);

		composite_uri.setTabList(new Control[] { this.locationText, this.btnGo,
				this.btnBrowse });

		this.btnGo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				validateServiceUrl();
			}
		});

		this.btnBrowse.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				final String fileLocation = openFileBrowser(parent.getShell());
				if (fileLocation != null && !fileLocation.isEmpty()) {
					isFileBrowsed = true;
					validateMetadataFile(fileLocation);
				}
			}
		});

		// radio button for "Service URL"
		this.radioURI = new Button(composite_radio, SWT.RADIO);
		this.radioURI.setText(Messages.ADD_EDMX_DIALOG_SERVICE_URL_BUTTON);
		this.radioURI.setSelection(true);
		this.radioURI.setFocus();
		
		this.radioURI.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				if (radioURI.getSelection()) {
					serviceUriSelected();
				}
			}
		});

		// radio button for "Service Metadata File"
		this.radioFile = new Button(composite_radio, SWT.RADIO);
		this.radioFile.setText(Messages.ADD_EDMX_DIALOG_FILE_BUTTON);
		this.radioFile.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				if (radioFile.getSelection()) {
					metadataFileSelected();
				}
			}
		});

		// Listener for 'Enter' key after selecting metadata file radio button.
		// This is added because a default action is required when this radio
		// button is selected
		this.radioFile.addKeyListener(new KeyListener() {
			
			@Override
			public void keyPressed(KeyEvent e) {
				
				if (e.keyCode == SWT.CR) {
					if (getTextLocation().getText() == EMPTY_STRING) {
						final String fileLocation = openFileBrowser(parent.getShell());
						if (fileLocation != null && !fileLocation.isEmpty()) {
							isFileBrowsed = true;
							validateMetadataFile(fileLocation);
						}
					}
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// Nothing to be done when Key is released
			}
		});

		// composite_radio.setTabList(new Control[] { this.radioURI,
		// this.radioFile });

		this.locationText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				
				String cachedUrl = (String) cacheFileResult.get(IODataEditorConstants.CACHED_URL_KEY);
				
				if (!isFileBrowsed && !(cachedUrl != null && cachedUrl.equalsIgnoreCase(locationText.getText()))) {
					handleModifyLocationText();
				}
			}
		});
		
		// Listener for 'Enter' key after entering the Service URL
		this.locationText.addListener(SWT.DefaultSelection, new Listener() {

			@Override
			public void handleEvent(Event e) {
				
				if (isURI() && isBtnGoEnabled()) {
					validateServiceUrl();
				}
			}
		});

		// service heading label
		final Label lblServiceDetails = new Label(container, SWT.NONE);
		GridData gd_lblNewLabel = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_lblNewLabel.widthHint = -1;
		lblServiceDetails.setLayoutData(gd_lblNewLabel);
		lblServiceDetails.setText(Messages.ADD_EDMX_DIALOG_LABEL_SERVICE);

		container.setTabList(new Control[] { composite_radio, composite_uri });

		// tree viewer to display service details
		this.serviceTreeViewer = new ServiceTreeViewer(container);
		final Tree tree = this.serviceTreeViewer.getTree();
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		// By default service URL radio button is selected. Handle this case.
		serviceUriSelected();
		
		setControl(container);
	}

	protected void validateServiceUrl() {

		this.btnBrowse.setEnabled(false);
		setMessage(Messages.ADD_EDMX_DIALOG_VALIDATING_SERVICE,
				IMessageProvider.INFORMATION);
		
		final String serviceUrl = getTextLocation().getText().trim();
		
		IRunnableWithProgress runnable = new IRunnableWithProgress() {

			@Override
			public void run(final IProgressMonitor monitor) {
				
				monitor.beginTask(Messages.PROGRESS_TASK_NAME11, IProgressMonitor.UNKNOWN);
				validationResult = ODataEditorService.validateServiceUrl(serviceUrl, monitor);
				
				// Check if the user pressed "cancel"
                if (monitor.isCanceled()) {
                    monitor.done();
                    return;
                }
				monitor.done();
			}
		};
		try {
			getWizard().getContainer().run(true, true, runnable);
		} catch (InvocationTargetException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		} catch (InterruptedException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		}
		handleUrlValidation(this.validationResult);
	}

	protected void validateMetadataFile(final String fileLocation) {
		
		setMessage(Messages.ADD_EDMX_DIALOG_VALIDATING_SERVICE,
				IMessageProvider.INFORMATION);
		
		IRunnableWithProgress runnable = new IRunnableWithProgress() {

			@Override
			public void run(final IProgressMonitor monitor) {
				
				monitor.beginTask(Messages.PROGRESS_TASK_NAME9, IProgressMonitor.UNKNOWN);
				validationResult = ODataEditorService.validateServiceMetadataFile(fileLocation, monitor);
				// Check if the user pressed "cancel"
                if (monitor.isCanceled()) {
                    monitor.done();
                    return;
                }
				monitor.done();
			}
		};
		try {
			getWizard().getContainer().run(true, true, runnable);
		} catch (InvocationTargetException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		} catch (InterruptedException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		}
		handleFileValidation(fileLocation, this.validationResult);
	}
	
	protected void handleFileValidation(final String fileLocation,
			final Result result) {
		
		if (fileLocation != null) {
			cacheFileResult.put(IODataEditorConstants.CACHED_URL_KEY, fileLocation);
			getTextLocation().setText(fileLocation);
			if (result != null) {
				if (Status.OK == result.getStatus()) {
					// updates service tree view part
					try {
						updateServiceExploration(result);
						setPageComplete(true);
						setMessage(result.getMessage(),
								IMessageProvider.INFORMATION);
						cacheFileResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, result.getMessage());
						cacheFileResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.INFORMATION);
					} catch (BuilderException exception) {
						this.isFileBrowsed = false;
						getServiceTreeViewer().clear();
						setPageComplete(false);
						setMessage(exception.getMessage(), IMessageProvider.ERROR);
						cacheFileResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, exception.getMessage());
						cacheFileResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.ERROR);
					}
				} else if (Status.CUSTOM_ERROR == result.getStatus()) {
					try {
						updateServiceExploration(result);
					} catch (BuilderException e) {
						this.isFileBrowsed = false;
						getServiceTreeViewer().clear();
						setPageComplete(false);
						setMessage(e.getMessage(), IMessageProvider.ERROR);
						cacheFileResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, e.getMessage());
						cacheFileResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.ERROR);
					}
					setPageComplete(true);
					setMessage(result.getMessage(), IMessageProvider.WARNING);
				} else {
					this.isFileBrowsed = false;
					getServiceTreeViewer().clear();
					setPageComplete(false);
					setMessage(result.getMessage(), IMessageProvider.ERROR);
					cacheFileResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, result.getMessage());
					cacheFileResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.ERROR);
				}
			}
		}
	}

	protected void handleUrlValidation(final Result result) {
		
		if (result != null) {
			if (Status.OK == result.getStatus()) {
				// updates service tree view part
				try {
					updateServiceExploration(result);
					setPageComplete(true);
					setMessage(result.getMessage(), IMessageProvider.INFORMATION);
					cacheUrlResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.INFORMATION);
					cacheUrlResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, result.getMessage());
				} catch (BuilderException exception) {
					getServiceTreeViewer().clear();					
					setPageComplete(false);
					setMessage(exception.getMessage(), IMessageProvider.ERROR);
					cacheUrlResult.clear();
					cacheUrlResult.put(IODataEditorConstants.CACHED_URL_KEY, getTextLocation().getText());
					cacheUrlResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.ERROR);
					cacheUrlResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, exception.getMessage());
				}
			} else if (Status.CUSTOM_ERROR == result.getStatus()) {
				getServiceTreeViewer().clear();				
				setPageComplete(false);
				setMessage(result.getMessage(),	IMessageProvider.WARNING);
				cacheUrlResult.clear();
				cacheUrlResult.put(IODataEditorConstants.CACHED_URL_KEY, getTextLocation().getText());
				cacheUrlResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.WARNING);
				cacheUrlResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, result.getMessage());
			} else {
				getServiceTreeViewer().clear();
				setPageComplete(false);
				setMessage(result.getMessage(), IMessageProvider.ERROR);
				cacheUrlResult.clear();
				cacheUrlResult.put(IODataEditorConstants.CACHED_URL_KEY, getTextLocation().getText());
				cacheUrlResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.ERROR);
				cacheUrlResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, result.getMessage());
			}
		}
	}

	protected void serviceUriSelected() {

		setURI(true);
		this.btnGo.setEnabled(false);
		this.btnBrowse.setEnabled(false);
		setPageComplete(false);
		this.radioURI.setSelection(true);
		this.radioFile.setSelection(false);
		
		if (cacheUrlResult.isEmpty()) {
			// clears service tree view part
			getServiceTreeViewer().clear();		
			getTextLocation().setText(EMPTY_STRING);
			getTextLocation().setMessage(Messages.ADD_EDMX_DIALOG_SERVICE_URL_MSG);
			setMessage(null);
		} else {
			getTextLocation().setText((String) cacheUrlResult.get(IODataEditorConstants.CACHED_URL_KEY));
			ITreeNode treeNode = (ITreeNode) cacheUrlResult.get(IODataEditorConstants.CACHED_SERVICE_NODE_KEY);
			if (treeNode != null) {
				this.referencedEdmxResult = (Result) cacheUrlResult.get(IODataEditorConstants.CACHED_RESULT_KEY);
				getServiceTreeViewer().setService(treeNode);
				setPageComplete(true);
			} else {
				getServiceTreeViewer().clear();
				setPageComplete(false);
			}
			int status = IMessageProvider.NONE;
			Object objStatus = cacheUrlResult.get(IODataEditorConstants.CACHED_STATUS_KEY);
			if (objStatus instanceof Integer) {
				status = ((Integer) objStatus).intValue();
			}
			setMessage((String) cacheUrlResult.get(IODataEditorConstants.CACHED_MESSAGE_KEY), status);
			if (IMessageProvider.ERROR == status) {
				getServiceTreeViewer().clear();
			}
		}
	}

	protected void metadataFileSelected() {

		setURI(false);
		this.btnGo.setEnabled(false);
		this.btnBrowse.setEnabled(true);
		setPageComplete(false);
		this.radioFile.setSelection(true);
		this.radioURI.setSelection(false);
				
		if (cacheFileResult.isEmpty()) {
			getServiceTreeViewer().clear();
			getTextLocation().setText(EMPTY_STRING);
			getTextLocation().setMessage(Messages.ADD_EDMX_DIALOG_FILE_MSG);
			setMessage(null);
		} else {
			getTextLocation().setText((String) cacheFileResult.get(IODataEditorConstants.CACHED_URL_KEY));
			ITreeNode treeNode = (ITreeNode) cacheFileResult.get(IODataEditorConstants.CACHED_SERVICE_NODE_KEY);
			if (treeNode != null) {
				this.referencedEdmxResult = (Result) cacheFileResult.get(IODataEditorConstants.CACHED_RESULT_KEY);
				getServiceTreeViewer().setService(treeNode);
				setPageComplete(true);
			}
			int status = IMessageProvider.NONE;
			Object objStatus = cacheFileResult.get(IODataEditorConstants.CACHED_STATUS_KEY);
			if (objStatus instanceof Integer) {
				status = ((Integer) objStatus).intValue();
			}
			setMessage((String) cacheFileResult.get(IODataEditorConstants.CACHED_MESSAGE_KEY), status);
			if (IMessageProvider.ERROR == status) {
				getServiceTreeViewer().clear();
			}
		}
	}

	protected void handleModifyLocationText() {

		setPageComplete(false);
		final String serviceUrl = getTextLocation().getText();
		if (serviceUrl == null || serviceUrl.isEmpty()) {
			this.btnGo.setEnabled(false);
			return;
		}
		if (isURI()) {
			cacheUrlResult.put(IODataEditorConstants.CACHED_URL_KEY, serviceUrl);
			final boolean isValidSyntax = ODataEditorService
					.isServiceUrlSyntaxValid(serviceUrl);
			if (isValidSyntax) {
				this.btnGo.setEnabled(true);
				setMessage(null);
			} else {
				this.btnGo.setEnabled(false);
				setMessage(Messages.ADD_EDMX_DIALOG_INVALID_URL,
						IMessageProvider.WARNING);
			}
			// clears service tree view part
			getServiceTreeViewer().clear();
		} else {
			cacheFileResult.put(IODataEditorConstants.CACHED_URL_KEY, serviceUrl);
			IRunnableWithProgress runnable = new IRunnableWithProgress() {

				@Override
				public void run(final IProgressMonitor monitor) {
					
					monitor.beginTask(Messages.PROGRESS_TASK_NAME9, IProgressMonitor.UNKNOWN);
					validationResult = ODataEditorService.validateServiceMetadataFile(serviceUrl, monitor);
					// Check if the user pressed "cancel"
	                if (monitor.isCanceled()) {
	                    monitor.done();
	                    return;
	                }
					monitor.done();
				}
			};
			try {
				getWizard().getContainer().run(true, true, runnable);
			} catch (InvocationTargetException e) {
				Logger.getLogger(Activator.PLUGIN_ID).logError(e);
			} catch (InterruptedException e) {
				Logger.getLogger(Activator.PLUGIN_ID).logError(e);
			}
			handleFileResult(this.validationResult);
		}
	}

	protected static String openFileBrowser(Shell parent) {

		final FileDialog fileDialog = new FileDialog(parent, SWT.OPEN);
		fileDialog.setFilterNames(new String[] { Messages.FILE_BROWSER_ALL_FILES });
		fileDialog.setFilterExtensions(new String[] { IODataEditorConstants.FILTER_EXT_ALL_FILES });
		fileDialog.setText(Messages.ADD_EDMX_DIALOG_FILE_BROWSER_TITLE);
		final String fileName = fileDialog.open();

		return fileName;
	}

	/**
	 * Updates service exploration tree view part.
	 * 
	 * @param result
	 * @throws BuilderException
	 */
	protected void updateServiceExploration(Result result)
			throws BuilderException {
		
		boolean isFileResult = false;
		
		if (this.serviceTreeViewer == null) {
			return;
		}
		if (Status.OK == result.getStatus()) {
			String url = result.getServiceUrl();
			if (url == null || url.isEmpty()) {
				url = result.getServiceMetadataUri();
				this.isFileBrowsed = false; // reset the flag.
				isFileResult = true;
			}
			final EDMXSet odataEdmxSet = ODataModelEDMXSetBuilder.build(
					result.getEdmx1(), url);
			final ITreeNode serviceNode = new ServiceNode(
					result.getServiceName(), odataEdmxSet);
			if (isFileResult) {
				cacheFileResult.clear();
				cacheFileResult.put(IODataEditorConstants.CACHED_URL_KEY, getTextLocation().getText());
				cacheFileResult.put(IODataEditorConstants.CACHED_SERVICE_NODE_KEY, serviceNode);
				cacheFileResult.put(IODataEditorConstants.CACHED_RESULT_KEY, result);
				this.isFileBrowsed = false;
			} else {
				cacheUrlResult.clear();
				cacheUrlResult.put(IODataEditorConstants.CACHED_URL_KEY, url);
				cacheUrlResult.put(IODataEditorConstants.CACHED_SERVICE_NODE_KEY, serviceNode);
				cacheUrlResult.put(IODataEditorConstants.CACHED_RESULT_KEY, result);
			}
			this.serviceTreeViewer.setService(serviceNode);
			this.referencedEdmxResult = result;
		} else {
			this.serviceTreeViewer.clear();
		}
	}
	
	protected void handleFileResult(final Result result) {
		
		if (result == null) {
			return;
		}
		final Status status = result.getStatus();
		final String message = result.getMessage();
		if (status == Status.OK) {
			// updates service tree view part
			try {
				updateServiceExploration(result);
				setPageComplete(true);
				setMessage(message, IMessageProvider.INFORMATION);
				cacheFileResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.INFORMATION);
				cacheFileResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, message);
			} catch (BuilderException e) {
				setPageComplete(false);
				getServiceTreeViewer().clear();
				setMessage(e.getMessage(), IMessageProvider.ERROR);
				cacheFileResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.ERROR);
				cacheFileResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, e.getMessage());
			}
		} else {
			setPageComplete(false);
			getServiceTreeViewer().clear();
			setMessage(message, IMessageProvider.ERROR);
			cacheFileResult.put(IODataEditorConstants.CACHED_STATUS_KEY, IMessageProvider.ERROR);
			cacheFileResult.put(IODataEditorConstants.CACHED_MESSAGE_KEY, message);
		}
	}

	/**
	 * @return textLocation
	 */
	public Text getTextLocation() {

		return this.locationText;
	}

	/**
	 * @param textLocation
	 */
	public void setTextLocation(Text textLocation) {

		this.locationText = textLocation;
	}

	/**
	 * @return boolean - is URI?
	 */
	public boolean isURI() {

		return this.isURI;
	}

	/**
	 * @param isURL 
	 */
	public void setURI(boolean isURL) {

		this.isURI =  isURL;
	}

	/**
	 * @return ServiceTreeViewer
	 */
	public ServiceTreeViewer getServiceTreeViewer() {

		return this.serviceTreeViewer;
	}
	
	/**
	 * @return EDMXSet
	 */
	public EDMXSet getCurrentEdmxSet() {
		
		return this.currentEdmxSet;
	}

	/**
	 * @param currentEDMXSet 
	 */
	public void setCurrentEdmxSet(EDMXSet currentEDMXSet) {
		
		this.currentEdmxSet = currentEDMXSet;
	}

	/**
	 * @return Result
	 */
	public Result getReferencedEdmxResult() {
		
		return this.referencedEdmxResult;
	}

	/**
	 * @param referencedEDMXResult
	 */
	public void setReferencedEdmxResult(Result referencedEDMXResult) {
		
		this.referencedEdmxResult = referencedEDMXResult;
	}

	/**
	 * @return true if Go button isEnabled.
	 */
	public boolean isBtnGoEnabled() {
		
		return this.btnGo.isEnabled();
	}
	
	@Override
	public void performHelp() {
		
		super.performHelp();
		
		PlatformUI.getWorkbench().getHelpSystem().setHelp(getControl(),
				IODataEditorConstants.HELP_CONTEXT_ID_REFERENCESDIALOG);
		
		
	}

}
