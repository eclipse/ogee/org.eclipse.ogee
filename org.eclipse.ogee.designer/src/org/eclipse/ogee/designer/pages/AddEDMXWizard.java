/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.pages;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.imp.util.addreferencehandler.api.IODataModelAddReferenceHandler;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.swt.widgets.Shell;

/**
 * The following implementation creates a Wizard to provide a provision to add
 * external EDMX references.
 * 
 */
public class AddEDMXWizard extends Wizard {

	private AddEDMXWizardPage page;
	private EDMXSet currentEdmxSet;
	private Shell parentShell;

	/**
	 * @param shell
	 * @param edmxSet
	 */
	public AddEDMXWizard(Shell shell, EDMXSet edmxSet) {

		this.currentEdmxSet = edmxSet;
		this.parentShell = shell;

		setWindowTitle(Messages.ADD_EDMX_DIALOG_TITLE);
		setForcePreviousAndNextButtons(false);
		setNeedsProgressMonitor(true);
	}

	@Override
	public void addPages() {

		super.addPages();
		this.page = new AddEDMXWizardPage(this.parentShell, this.currentEdmxSet);
		addPage(this.page);
	}

	@Override
	public boolean performFinish() {

		if (this.page.isPageComplete()) {
			// Disable the add button
			this.page.setPageComplete(false);
			// Invoking addReference() API to add the
			// EDMX reference in the current domain model.
			if (this.page.getReferencedEdmxResult() != null
					&& this.page.getCurrentEdmxSet() != null) {
				final IStatus status = IODataModelAddReferenceHandler.INSTANCE
						.addReference(this.page.getReferencedEdmxResult(),
								this.page.getCurrentEdmxSet());
				if (status == null) {
					this.page.setMessage(Messages.ODATADIAGRAM_CREATOR_IMPL2,
							IMessageProvider.ERROR);
					return false;
				} else if (IStatus.OK == status.getCode()) {
					this.page.setMessage(status.getMessage(),
							IMessageProvider.INFORMATION);
					return true;
				} else {
					this.page.setMessage(status.getMessage(),
							IMessageProvider.ERROR);
					return false;
				}
			}
			this.page.setMessage(Messages.ODATADIAGRAM_CREATOR_IMPL2,
					IMessageProvider.ERROR);
			return false;
		}
		// Disable the add button
		this.page.setPageComplete(false);
		return false;
	}

}
