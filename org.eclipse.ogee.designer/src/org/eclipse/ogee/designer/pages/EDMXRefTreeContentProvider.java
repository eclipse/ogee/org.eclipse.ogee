/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.pages;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;

/**
 * Content provider implementation for TreeViewer.
 * 
 */
public class EDMXRefTreeContentProvider implements ITreeContentProvider {

	@Override
	public void dispose() {
		// do nothing
	}

	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
		// do nothing
	}

	@Override
	public Object[] getChildren(Object element) {

		if (element instanceof EDMXReference) {
			return (((EDMXReference) element).getReferencedEDMX()
					.getDataService().getSchemata()).toArray();
		}

		return null;
	}

	@Override
	public Object[] getElements(Object inputElement) {

		return ((List<?>) inputElement).toArray();
	}

	@Override
	public Object getParent(Object element) {

		if (element instanceof Schema) {
			final Schema schema = (Schema) element;
			final EObject eObj = schema.eContainer();
			if (eObj instanceof EDMXSet) {
				final EDMXSet edmxSetLocal = (EDMXSet) eObj;
				final EList<EDMXReference> edmxReferences = edmxSetLocal
						.getMainEDMX().getReferences();
				for (EDMXReference edmxReference : edmxReferences) {
					if (edmxReference.getReferencedEDMX().getDataService()
							.getSchemata().contains(schema)) {
						return edmxReference;
					}
				}
			}
		}

		return null;
	}

	@Override
	public boolean hasChildren(Object element) {

		if (element instanceof EDMXReference) {
			return ((EDMXReference) element).getReferencedEDMX()
					.getDataService().getSchemata().size() > 0;
		}

		return false;
	}

}
