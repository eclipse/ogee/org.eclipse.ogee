/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.pages;

import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.dialogs.PatternFilter;

/**
 * PatternFilter implementation for EDMX Reference page. The PatternFilter is
 * sub-classed so that a matched node's children also be shown. The current
 * implementation will select an element if any of its ancestors are a match.
 * 
 */
public class EDMXReferenceFilter extends PatternFilter {

	@Override
	protected boolean isLeafMatch(Viewer viewer, Object element) {

		final String labelText = ((ILabelProvider) ((StructuredViewer) viewer)
				.getLabelProvider()).getText(element);

		if (labelText == null) {
			return false;
		}

		return (wordMatches(labelText) ? true : isChildMatch(viewer, element));
	}

	private boolean isChildMatch(Viewer viewer, Object element) {

		final Object parent = ((ITreeContentProvider) ((AbstractTreeViewer) viewer)
				.getContentProvider()).getParent(element);

		if (parent != null) {
			return (isLeafMatch(viewer, parent) ? true : isChildMatch(viewer,
					parent));
		}

		return false;
	}

}
