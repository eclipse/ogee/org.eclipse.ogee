/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.pages;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

/**
 * The WizardDialog is sub-classed in order to rename the Finish
 * button to Add & add window title icon.
 */
public class AddEDMXWizardDialog extends WizardDialog {

	/**
	 * @param parentShell
	 * @param newWizard
	 */
	public AddEDMXWizardDialog(Shell parentShell, IWizard newWizard) {

		super(parentShell, newWizard);
		
		setPageSize(650, 300);
		setBlockOnOpen(true);
		setTitle(Messages.ADD_EDMX_DIALOG_TITLE);
		setDefaultImage(EDMXReferencePage.img_ref_schemas);
		setHelpAvailable(true);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		super.createButtonsForButtonBar(parent);

		Button button = getButton(IDialogConstants.FINISH_ID);
		button.setText(Messages.ADD_BUTTON);
	}

}
