/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.pages;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.ODataEditor;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.service.ODataEditorService;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IResourceContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.ObjectInUseException;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.dialogs.FilteredTree;
import org.eclipse.ui.dialogs.PatternFilter;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * The following implementation creates a "EDMX References" page in the
 * multi-page OData editor.
 * 
 */
public class EDMXReferencePage extends Composite {

	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());

	private TreeViewer treeViewer;
	private Button btnAdd;
	private Button btnRemove;
	private Label lblTotal;
	private Text txtAlias;
	private Label referencedNamespace;
	private Combo comboCurrentSchemas;

	private EDMXSet currentEDMXSet;
	private ODataEditor diagramEditor;
	private boolean isReferenceRemoved;
	private int lastTreeCount;

	protected static final String EMPTY_STRING = ""; //$NON-NLS-1$
	
	protected static final Image img_ref_schemas = getImage("referencedschemas.gif"); //$NON-NLS-1$
	protected static final Image img_url = getImage("ServiceUrl.png"); //$NON-NLS-1$
	protected static final Image img_file = getImage("metadata_file.png"); //$NON-NLS-1$
	protected static final Image img_schema = getImage("odata.png"); //$NON-NLS-1$

	/**
	 * Create the composite for References page.
	 * 
	 * @param parent
	 *            Composite
	 * @param edmxSet
	 *            EDMXSet
	 * @param isReadOnly
	 *            boolean
	 * @param site
	 *            IEditorSite
	 * @param designEditor
	 *            ODataEditor
	 */
	public EDMXReferencePage(final Composite parent, final EDMXSet edmxSet,
			final boolean isReadOnly, final IEditorSite site,
			final ODataEditor designEditor) {

		super(parent, SWT.NONE);
		setLayout(new GridLayout(1, false));

		this.currentEDMXSet = edmxSet;
		this.diagramEditor = designEditor;
		this.isReferenceRemoved = false;

		final ScrolledForm refSchemaForm = this.formToolkit
				.createScrolledForm(this);
		GridData gd_refSchemaForm = new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1);
		gd_refSchemaForm.widthHint = -1;
		gd_refSchemaForm.minimumWidth = -1;
		refSchemaForm.setLayoutData(gd_refSchemaForm);
		this.formToolkit.paintBordersFor(refSchemaForm);
		refSchemaForm.setText(Messages.EDMX_REF_PAGE_HEADING);
		refSchemaForm.setImage(img_ref_schemas);
		this.formToolkit.decorateFormHeading(refSchemaForm.getForm());
		Composite body = refSchemaForm.getBody();
		this.formToolkit.paintBordersFor(body);
		refSchemaForm.getBody().setLayout(new GridLayout(2, true));
		
		// Composite for EDMX References section.
		final Composite composite_left = this.formToolkit.createComposite(
				refSchemaForm.getBody(), SWT.NONE);
		composite_left.setLayout(new GridLayout(1, true));
		GridData gd_composite_left = new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1);
		gd_composite_left.widthHint = 100;
		gd_composite_left.heightHint = 100;
		composite_left.setLayoutData(gd_composite_left);
		this.formToolkit.paintBordersFor(composite_left);

		Section sctnReferences = this.formToolkit.createSection(composite_left,
				ExpandableComposite.COMPACT | Section.DESCRIPTION
						| ExpandableComposite.TITLE_BAR);
		sctnReferences.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		this.formToolkit.paintBordersFor(sctnReferences);
		sctnReferences.setText(Messages.EDMX_REF_PAGE_SEC_HEADING);

		this.formToolkit.createLabel(composite_left,
				Messages.EDMX_REF_PAGE_LABEL, SWT.NONE);

		Composite composite_left_1 = this.formToolkit.createComposite(
				composite_left, SWT.NONE);
		composite_left_1.setLayout(new GridLayout(2, false));
		composite_left_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1));
		this.formToolkit.paintBordersFor(composite_left_1);

		// PatternFilter is used in conjunction with FilteredTree.
		final PatternFilter filter = new EDMXReferenceFilter();
		filter.setIncludeLeadingWildcard(true);

		// FilteredTree is simple control that provides a text widget and a tree
		// viewer. The contents of the text widget are used to drive a
		// PatternFilter that is on the viewer.
		final FilteredTree tree = new FilteredTree(composite_left_1, SWT.BORDER
				| SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL, filter, true);
		tree.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		this.treeViewer = tree.getViewer();
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		this.formToolkit.paintBordersFor(tree);

		// Start - Adding "expand/collapse all" actions to References section
		// tool bar.
		final IAction expandAll = new Action(Messages.EDMX_REF_PAGE_EXPAND_ALL,
				IAction.AS_PUSH_BUTTON) {

			@Override
			public void run() {
				getTreeViewer().expandAll();
			}
		};
		expandAll.setImageDescriptor(AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID, ODataImageProvider.IMG_EDIT_EXPANDALL));
		expandAll.setEnabled(true);

		final IAction collapseAll = new Action(Messages.EDMX_REF_PAGE_COLLAPSE_ALL,
				IAction.AS_PUSH_BUTTON) {

			@Override
			public void run() {
				getTreeViewer().collapseAll();
			}
		};
		collapseAll.setImageDescriptor(AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID, ODataImageProvider.IMG_EDIT_COLLAPSEALL));
		collapseAll.setEnabled(true);

		ToolBarManager toolBarManager = new ToolBarManager(SWT.FLAT);
		ToolBar toolbar = toolBarManager.createControl(sctnReferences);
		toolbar.setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_HAND));

		toolBarManager.add(expandAll);
		toolBarManager.add(collapseAll);
		toolBarManager.update(true);
		sctnReferences.setTextClient(toolbar);
		// End - Adding "expand/collapse all" actions to References section tool
		// bar.

		Composite composite_left_2 = new Composite(composite_left_1, SWT.NONE);
		GridData gd_composite_2 = new GridData(SWT.FILL, SWT.FILL, false, true,
				1, 1);
		composite_left_2.setLayoutData(gd_composite_2);
		this.formToolkit.adapt(composite_left_2);
		this.formToolkit.paintBordersFor(composite_left_2);
		composite_left_2.setLayout(new GridLayout(1, false));

		// this label is required for layouting.
		Label label = new Label(composite_left_2, SWT.NONE);
		label.setText(EMPTY_STRING);
		
		// Add button to add an EDMX Reference.
		this.btnAdd = new Button(composite_left_2, SWT.PUSH);
		this.btnAdd.setText(Messages.ADD_CONTD_BUTTON);
		this.btnAdd.setToolTipText(Messages.ADD_CONTD_BUTTON_TOOLTIP);
		GridData gd_btnAdd = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_btnAdd.widthHint = -1;
		this.btnAdd.setLayoutData(gd_btnAdd);
		this.formToolkit.adapt(this.btnAdd, true, true);
		if (isReadOnly) {
			this.btnAdd.setEnabled(false);
		}
		
		// Restore Defaults button to add missing registered vocabularies.
		final Button btnRestoreDefaults = new Button(composite_left_2, SWT.PUSH);
		btnRestoreDefaults.setText(Messages.RESTORE_DEFAULTS_BUTTON);
		btnRestoreDefaults.setToolTipText(Messages.RESTORE_DEFAULTS_BUTTON_TOOLTIP);
		GridData gd_btnRestoreDefaults  = new GridData(
				SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_btnRestoreDefaults.widthHint = -1;
		btnRestoreDefaults.setLayoutData(gd_btnRestoreDefaults);
		this.formToolkit.adapt(btnRestoreDefaults, true, true);
		if (isReadOnly) {
			btnRestoreDefaults.setEnabled(false);
		}
		
		btnRestoreDefaults.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				
				restoreRegisteredVocabularies(parent);
			}
		});
		
		btnRestoreDefaults.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent keyEvent) {
				
				if (keyEvent.keyCode == SWT.Selection) {
					restoreRegisteredVocabularies(parent);
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// do nothing				
			}
		});

		// Remove button to remove an EDMX Reference.
		this.btnRemove = new Button(composite_left_2, SWT.PUSH);
		this.btnRemove.setText(Messages.REMOVE_BUTTON);
		this.btnRemove.setToolTipText(Messages.REMOVE_BUTTON_TOOLTIP);
		GridData gd_btnRemove = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_btnRemove.widthHint = -1;
		this.btnRemove.setLayoutData(gd_btnRemove);
		this.formToolkit.adapt(this.btnRemove, true, true);
		this.btnRemove.setEnabled(false);

		Composite composite_left_3 = this.formToolkit.createComposite(
				composite_left_2, SWT.NONE);
		GridData gd_composite_3 = new GridData(SWT.LEFT, SWT.BOTTOM, true,
				true, 1, 1);
		gd_composite_3.heightHint = 120;
		composite_left_3.setLayoutData(gd_composite_3);
		this.formToolkit.paintBordersFor(composite_left_3);
		// Total label to show edmx references count in tree view.
		this.lblTotal = this.formToolkit.createLabel(composite_left_3,
				Messages.EDMX_REF_PAGE_TOTAL, SWT.NONE);
		this.lblTotal.setBounds(0, 106, 55, 15);
		this.lblTotal.setForeground(getDisplay().getSystemColor(
				SWT.COLOR_DARK_BLUE));
		
		// Composite for schema details section.
		final Composite composite_right = this.formToolkit.createComposite(
				refSchemaForm.getBody(), SWT.NONE);
		composite_right.setLayout(new GridLayout(1, true));
		GridData gd_composite_right = new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1);
		gd_composite_right.widthHint = 100;
		composite_right.setLayoutData(gd_composite_right);
		this.formToolkit.paintBordersFor(composite_right);

		// Add EDMX Reference when add button is pushed.
		this.btnAdd.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {

				addEDMXReference(parent);
			}
		});
		
		this.btnAdd.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent keyEvent) {
				
				if (keyEvent.keyCode == SWT.Selection) {
					addEDMXReference(parent);
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// do nothing				
			}
		});

		// Remove EDMX Reference when remove button is pushed
		this.btnRemove.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {

				removeEDMXReference(parent);
			}
		});
		
		this.btnRemove.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent keyEvent) {
				
				if (keyEvent.keyCode == SWT.Selection) {
					removeEDMXReference(parent);
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// do nothing				
			}
		});

		final ITreeContentProvider contentProvider = new EDMXRefTreeContentProvider();
		this.treeViewer.setContentProvider(contentProvider);

		final ILabelProvider labelProvider = new EDMXRefTreeLabelProvider(tree);
		this.treeViewer.setLabelProvider(labelProvider);

		this.treeViewer.setUseHashlookup(true);

		// add double click listener to expand/collapse current tree selection.
		this.treeViewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {

				handleDoubleClickEvent(event);
			}
		});

		// on selection of schema from tree view, populate the schema details
		// section.
		this.treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				if (event.getSelection() instanceof TreeSelection) {
					final TreeSelection selection = (TreeSelection) event.getSelection();
					final Object selectedObj = selection.getFirstElement();
					createSchemaDetailsSection(selectedObj,	refSchemaForm, composite_left,
							composite_right, isReadOnly);
				}
			}
		});
		// Set the TreeViewer input.
		refreshTreeInput();
	}

	/**
	 * Creates the Schema details section.
	 * 
	 * @param refSchemaForm
	 * @param composite_left
	 * @param composite_right
	 * @param isReadOnly 
	 */
	public void createSchemaSection(ScrolledForm refSchemaForm,
			Composite composite_left, Composite composite_right, boolean isReadOnly) {

		removeSchemaSection(composite_right);

		Section sctnSchemaDetails = this.formToolkit.createSection(
				composite_right, ExpandableComposite.COMPACT
						| Section.DESCRIPTION | ExpandableComposite.TITLE_BAR);
		sctnSchemaDetails.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));
		this.formToolkit.paintBordersFor(sctnSchemaDetails);
		sctnSchemaDetails.setText(Messages.SCHEMA_DETAILS_TITLE);

		this.formToolkit.createLabel(composite_right, Messages.SCHEMA_DETAILS_MSG, SWT.NONE);

		Composite composite_right_1 = this.formToolkit.createComposite(
				composite_right, SWT.NONE);
		composite_right_1.setLayout(new GridLayout(2, false));
		composite_right_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1));
		this.formToolkit.paintBordersFor(composite_right_1);

		Label lblNamespaceTitle = this.formToolkit.createLabel(
				composite_right_1, Messages.SCHEMA_DETAILS_REF_SCHEMA_NAMESPACE, SWT.NONE);
		lblNamespaceTitle.setForeground(getDisplay().getSystemColor(
				SWT.COLOR_DARK_BLUE));
		lblNamespaceTitle.setToolTipText(Messages.SCHEMA_DETAILS_REF_SCHEMA_NAMESPACE_TOOLTIP);

		this.referencedNamespace = this.formToolkit.createLabel(
				composite_right_1, EMPTY_STRING, SWT.NONE);
		this.referencedNamespace.setLayoutData(new GridData(SWT.LEFT,
				SWT.CENTER, true, false, 1, 1));

		Label lblCurrentSchemaTitle = this.formToolkit.createLabel(
				composite_right_1, Messages.SCHEMA_DETAILS_CURRENT_SCHEMA_NAMESPACE, SWT.NONE);
		lblCurrentSchemaTitle.setToolTipText(Messages.SCHEMA_DETAILS_CURRENT_SCHEMA_NAMESPACE_TOOLTIP);
		lblCurrentSchemaTitle.setForeground(getDisplay().getSystemColor(
				SWT.COLOR_DARK_BLUE));

		this.comboCurrentSchemas = new Combo(composite_right_1, SWT.READ_ONLY);
		this.comboCurrentSchemas.setLayoutData(new GridData(SWT.LEFT,
				SWT.CENTER, true, false, 1, 1));
		this.formToolkit.adapt(this.comboCurrentSchemas);
		this.formToolkit.paintBordersFor(this.comboCurrentSchemas);

		Label lblAlias = this.formToolkit.createLabel(composite_right_1,
				Messages.SCHEMA_DETAILS_REF_SCHEMA_ALIAS, SWT.NONE);
		lblAlias.setToolTipText(Messages.SCHEMA_DETAILS_REF_SCHEMA_ALIAS_TOOLTIP);
		lblAlias.setForeground(getDisplay().getSystemColor(SWT.COLOR_DARK_BLUE));

		this.txtAlias = this.formToolkit.createText(composite_right_1,
				EMPTY_STRING, SWT.NONE);
		this.txtAlias.setText(EMPTY_STRING);
		this.txtAlias.setMessage(Messages.SCHEMA_DETAILS_REF_SCHEMA_ALIAS_MSG);
		this.txtAlias.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		if (isReadOnly) {
			this.txtAlias.setEnabled(false);
		}

		composite_right_1.setTabList(new Control[] { this.txtAlias });

		refSchemaForm.getBody().setTabList(
				new Control[] { composite_left, composite_right });
	}

	/**
	 * @param composite_right
	 */
	public static void removeSchemaSection(Composite composite_right) {

		if (composite_right.getChildren().length > 0) {
			final Control[] children = composite_right.getChildren();
			for (Control control : children) {
				control.dispose();
			}
			composite_right.redraw();
		}
	}

	@Override
	public boolean setFocus() {

		if (this.treeViewer != null) {
			// When "References" tab is opened, set the focus on the EDMX
			// References tree view.
			// Select the first element in the tree if available.
			final Tree referenceTree = this.treeViewer.getTree();
			if (referenceTree != null && referenceTree.getItemCount() > 0) {
				if (this.treeViewer.getControl() != null) {
					try {
						this.treeViewer.getControl().setFocus();
					} catch (Exception e) {
						// do nothing
					}
				}
			} else {
				this.btnAdd.setFocus();
			}
		} else {
			this.btnAdd.setFocus();
		}

		return true;
	}
	
	/**
	 * Updates the tree viewer input and total count.
	 * 
	 * @return true, if reference tree is refreshed.
	 */
	public boolean refreshTreeInput() {
		
		boolean isTreeRefreshed = true;
		int currentTreeCount = 0;

		if (this.currentEDMXSet != null) {
			final List<EDMXReference> validEDMXRef = new ArrayList<EDMXReference>();
			final EDMX mainEdmx = this.currentEDMXSet.getMainEDMX();
			final List<EDMXReference> edmxReferences = mainEdmx.getReferences();
			for (EDMXReference edmxRef : edmxReferences) {
				if (edmxRef.getReferencedEDMX().getURI() != null) {
					validEDMXRef.add(edmxRef);
				}
			}
			this.treeViewer.setInput(validEDMXRef);
			currentTreeCount = validEDMXRef.size();
			this.lblTotal.setText(Messages.EDMX_REF_PAGE_TOTAL + currentTreeCount);
			this.treeViewer.refresh(true);
			
			// Select the first element in the tree if available.
			final Tree referenceTree = this.treeViewer.getTree();
			if (referenceTree != null && referenceTree.getItemCount() > 0) {
				referenceTree.setSelection(referenceTree.getItem(0));
			}			
			if (this.lastTreeCount == currentTreeCount) {
				isTreeRefreshed = false;
			}
		}
		this.lastTreeCount = currentTreeCount;
		
		return isTreeRefreshed;
	}

	protected void addEDMXReference(Composite parent) {
		
		AddEDMXWizard wizard = new AddEDMXWizard(parent.getShell(),
				this.currentEDMXSet);
		AddEDMXWizardDialog wizardDialog = new AddEDMXWizardDialog(
				parent.getShell(), wizard);
		
		// status == 0, if "EDMX reference" is added successfully.
		if (Window.OK == wizardDialog.open()) {
			refreshTreeInput();
			this.diagramEditor.getDiagramBehavior().refreshPalette();
		}
	}

	protected void removeEDMXReference(final Composite parent) {

		final IStructuredSelection selection = (IStructuredSelection) getTreeViewer()
				.getSelection();
		if (selection.isEmpty()) {
			MessageDialog.openInformation(parent.getShell(),
					Messages.EDMX_REF_PAGE_REMOVE_EDMX_TITLE,
					Messages.EDMX_REF_PAGE_REMOVE_EDMX_MSG);
			return;
		}
		final MessageDialog dialog = new MessageDialog(parent.getShell(),
				Messages.EDMX_REF_PAGE_REMOVE_EDMX_DIALOG_TITLE, null,
				Messages.EDMX_REF_PAGE_REMOVE_DIALOG_EDMX_MSG,
				MessageDialog.CONFIRM, new String[] { Messages.REMOVE_BUTTON,
						Messages.CANCEL_BUTTON }, 0);
		final int result = dialog.open();
		// result == 0, if user confirmed on removal of
		// "EDMX reference".
		if (result == Window.OK) {
			try {
				final IResourceContext resourceContext = IModelContext.INSTANCE
						.getResourceContext(this.currentEDMXSet);
				final Iterator<?> selections = selection.iterator();
				final TransactionalEditingDomain editingDomain = this.diagramEditor.getEditingDomain();
				if (editingDomain != null) {
					editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
						
						@Override
						public void doExecute() {

							Object selected;
							EDMXReference edmxReference;
							EDMX referencedEDMX = null;
							boolean isObjectInUseException = false;

							while (selections.hasNext()) {
								isReferenceRemoved = false;
								selected = selections.next();
								if (selected instanceof EDMXReference) {
									edmxReference = (EDMXReference) selected;
									try {
										referencedEDMX = edmxReference.getReferencedEDMX();
										if (resourceContext != null && referencedEDMX != null) {
											resourceContext.removeEDMX(referencedEDMX);
											refreshTreeInput();
											btnRemove.setEnabled(false);
											diagramEditor.getDiagramBehavior().refreshPalette();
											ODataShapeUtil.removeReferencedEntityShapes(edmxReference, diagramEditor);
											isReferenceRemoved = true;
										}
									} catch (ModelAPIException e) {
										if (e instanceof ObjectInUseException) {
											isObjectInUseException = true;
										}
									}
								}
							}
							if (isObjectInUseException && isReferenceRemoved) {
								MessageDialog.openWarning(parent.getShell(),
										Messages.EDMX_REF_PAGE_REMOVE_EDMX_DIALOG_TITLE,
										Messages.EDMX_REF_PAGE_REMOVE_WARNING_MSG);
							} else if (isObjectInUseException) {
								MessageDialog.openError(parent.getShell(),
										Messages.EDMX_REF_PAGE_REMOVE_EDMX_DIALOG_TITLE,
										Messages.EDMX_REF_PAGE_REMOVE_ERROR_MSG);
							}
						}
					});
					if (!this.isReferenceRemoved) {
						// to handle editor dirty state if nothing is changed.
						editingDomain.getCommandStack().undo();
					}
				}
			} catch (ModelAPIException e) {
				MessageDialog.openError(parent.getShell(), Messages.EDMX_REF_PAGE_REMOVE_EDMX_DIALOG_TITLE,
						Messages.EDMX_REF_PAGE_REMOVE_ERROR_MSG2);
			}
		}
	}

	protected void handleDoubleClickEvent(DoubleClickEvent event) {

		final ISelection selection = event.getSelection();
		if (selection instanceof IStructuredSelection) {
			Object item = ((IStructuredSelection) selection).getFirstElement();
			if (item == null) {
				return;
			}
			if (getTreeViewer().getExpandedState(item)) {
				getTreeViewer().collapseToLevel(item, 1);
			} else {
				getTreeViewer().expandToLevel(item, 1);
			}
		}
	}

	protected void createSchemaDetailsSection(Object selectedObj,
			ScrolledForm refSchemaForm, Composite composite_left,
			Composite composite_right, boolean isReadOnly) {

		if (selectedObj instanceof Schema) {
			// Referenced schema cannot be removed.
			this.btnRemove.setEnabled(false);
			createSchemaSection(refSchemaForm, composite_left, composite_right, isReadOnly);
			composite_right.layout();
			final Schema referencedSchema = (Schema) selectedObj;
			getNamespace().setText(referencedSchema.getNamespace());
			getNamespace().pack();

			// get the current schemata.
			final EList<Schema> currentSchemata = getEdmxSet().getMainEDMX()
					.getDataService().getSchemata();
			// populate the list of current schema namespace in combo box.
			final List<String> currentSchemaNamespaceList = new ArrayList<String>();
			for (Schema currentSchema : currentSchemata) {
				currentSchemaNamespaceList.add(currentSchema.getNamespace());
			}
			getComboCurrentSchemas().setItems(
					currentSchemaNamespaceList
							.toArray(new String[currentSchemaNamespaceList
									.size()]));
			getComboCurrentSchemas().select(0);
			getComboCurrentSchemas().pack();

			// get the referenced alias on page load.
			EList<Using> usings;
			for (Schema currentSchema : currentSchemata) {
				if (currentSchema.getNamespace().equals(
						getComboCurrentSchemas().getText())) {
					usings = currentSchema.getUsings();
					for (Using using : usings) {
						if (using.getUsedNamespace().equals(referencedSchema)) {
							getTxtAlias().setText(using.getAlias());
						}
					}
				}
			}

			// on current schema combo box selection change, update the
			// referenced alias text.
			getComboCurrentSchemas().addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetDefaultSelected(
						SelectionEvent defaultSelectionEvent) {
					// do nothing
				}

				@Override
				public void widgetSelected(SelectionEvent selectionEvent) {

					final Object source = selectionEvent.getSource();
					if (source instanceof Combo) {
						final Combo combo = (Combo) source;
						final String currentNamespace = combo.getText();
						populateCurrentSchemataComboBox(currentNamespace, currentSchemata);
					}
				}
			});
			
			// Create the error decoration for the Alias text UI component
			final ControlDecoration decoration = new ControlDecoration(
					getTxtAlias(), SWT.LEFT | SWT.BOTTOM);
			final FieldDecoration fieldDecoration = FieldDecorationRegistry
				    .getDefault().getFieldDecoration(FieldDecorationRegistry.DEC_ERROR);
			decoration.setImage(fieldDecoration.getImage());
			// Always show error decoration
			decoration.setShowOnlyOnFocus(false);
			decoration.hide();

			getTxtAlias().addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent modifyEvent) {
					
					final String error = handleAliasValidation(decoration);

					if (!getComboCurrentSchemas().isFocusControl() &&
							(error == null || error.isEmpty())) {
						handleAlias(currentSchemata, referencedSchema);
					}
				}
			});
		} else {
			if (isReadOnly) {
				this.btnRemove.setEnabled(false);
			} else {
				this.btnRemove.setEnabled(true);
			}
			removeSchemaSection(composite_right);
		}
	}

	protected void handleAlias(final EList<Schema> currentSchemata,
			final Schema referencedSchema) {

		final TransactionalEditingDomain editingDomain = this.diagramEditor.getEditingDomain();
		editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
			
			@Override
			protected void doExecute() {

				boolean isAliasUpdated = false;
				Schema selectedCurrentSchema = null;

				final String selectedCurrentSchemaNamespace = getComboCurrentSchemas()
						.getText();
				for (Schema currentSchema : currentSchemata) {
					if (selectedCurrentSchemaNamespace
							.equals(currentSchema.getNamespace())) {
						selectedCurrentSchema = currentSchema;
						break;
					}
				}
				if (selectedCurrentSchema != null) {
					// remove existing usings from selected current
					// schema for the referenced schema.
					final EList<Using> currentSchemaUsings = selectedCurrentSchema.getUsings();
					for (Using currentSchemaUsing : currentSchemaUsings) {
						if (referencedSchema.getNamespace().equals(
								currentSchemaUsing.getUsedNamespace().getNamespace())) {
							if (getTxtAlias().getText().isEmpty()) {
								selectedCurrentSchema.getUsings().remove(currentSchemaUsing);
							} else {
								currentSchemaUsing.setAlias(getTxtAlias().getText());
							}
							isAliasUpdated = true;
							break;
						}
					}
					if (!isAliasUpdated) {
						// Create & add used schema alias to current
						// schema usings.
						final Using using = OdataFactory.eINSTANCE.createUsing();
						if (using != null) {
							using.setAlias(getTxtAlias().getText());
							using.setUsedNamespace(referencedSchema);
							selectedCurrentSchema.getUsings().add(using);
						}
					}
				}
			}
		});
	}

	protected void populateCurrentSchemataComboBox(
			final String currentNamespace, final EList<Schema> currentSchemata) {

		boolean isAliasSet = false;
		EList<Using> usings;

		for (Schema currentSchema : currentSchemata) {
			if (currentSchema.getNamespace().equals(currentNamespace)) {
				usings = currentSchema.getUsings();
				for (Using using : usings) {
					if (using.getUsedNamespace().getNamespace()
							.equals(getNamespace().getText())) {
						getTxtAlias().setText(using.getAlias());
						isAliasSet = true;
						break;
					}
				}
			}
		}
		if (!isAliasSet) {
			getTxtAlias().setText(EMPTY_STRING);
		}
	}
	
	protected void restoreRegisteredVocabularies(final Composite parent) {
		
		boolean isRestored = ODataEditorService.restoreRegisteredVocabularies(getEdmxSet());
		if (isRestored) {
			refreshTreeInput();
		} else {
			MessageDialog.openInformation(parent.getShell(),
					Messages.RESTORE_DIALOG_TITLE, Messages.RESTORE_DIALOG_MSG);
		}
	}
	
	protected String handleAliasValidation(final ControlDecoration decoration) {
		
		final String error = ArtifactUtil.validateAlias(getTxtAlias().getText());
		if (error != null) {
			decoration.setDescriptionText(error);
			decoration.show();
		} else {
			decoration.hide();
		}
		
		return error;
	}
	
	protected EDMXSet getEdmxSet() {

		return this.currentEDMXSet;
	}

	protected TreeViewer getTreeViewer() {

		return this.treeViewer;
	}

	protected Text getTxtAlias() {

		return this.txtAlias;
	}
	
	protected void setTxtAlias(Text alias) {

		this.txtAlias = alias;
	}

	protected Label getNamespace() {

		return this.referencedNamespace;
	}

	protected Combo getComboCurrentSchemas() {

		return this.comboCurrentSchemas;
	}

	/*
	 * Helper method to load the icon images.
	 */
	private static Image getImage(final String file) {

		Bundle bundle = FrameworkUtil.getBundle(Activator.class);
		URL url = FileLocator.find(bundle, new Path("icons/" + file), null); //$NON-NLS-1$
		ImageDescriptor image = ImageDescriptor.createFromURL(url);

		return image.createImage();
	}

}
