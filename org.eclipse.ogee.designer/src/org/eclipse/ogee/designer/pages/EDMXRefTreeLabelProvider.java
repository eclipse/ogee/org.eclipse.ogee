/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.pages;

import org.eclipse.jface.viewers.IFontProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.ogee.designer.service.ODataEditorService;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.dialogs.FilteredTree;
import org.eclipse.ui.dialogs.PatternFilter;

/**
 * ILabelProvider is implemented to provide the text and/or image for the label
 * of a given element.
 * 
 * IFontProvider is implemented IFontProvider and in the getFont method we need
 * to call the FilteredTree.getBoldFont method. This method takes a
 * PatternFilter as an argument. This is the filter that is used to select which
 * elements are shown in bold. Since we just want our true matches ( and not
 * their children) to be bolded, we cannot use our modified
 * ShowChildrenPatterFilter. Instead we can just use the regular PatternFilter
 * here.
 * 
 */
public class EDMXRefTreeLabelProvider implements ILabelProvider, IFontProvider {

	private FilteredTree filteredTree;
	private PatternFilter filterForBoldElements = new PatternFilter();

	/**
	 * Default constructor.
	 * 
	 * @param tree
	 */
	public EDMXRefTreeLabelProvider(FilteredTree tree) {

		super();
		this.filteredTree = tree;
	}

	@Override
	public String getText(Object element) {

		if (element instanceof EDMXReference) {
			return ((EDMXReference) element).getReferencedEDMX().getURI();
		} else if (element instanceof Schema) {
			return ((Schema) element).getNamespace();
		}
		return ""; //$NON-NLS-1$
	}

	@Override
	public Image getImage(Object element) {

		if (element instanceof EDMXReference) {
			final EDMXReference edmxReference = (EDMXReference) element;
			final String uri = edmxReference.getReferencedEDMX().getURI();
			if (uri == null || uri.isEmpty()) {
				return EDMXReferencePage.img_ref_schemas;
			} else if (ODataEditorService.isServiceUrlSyntaxValid(uri)) {
				return EDMXReferencePage.img_url;
			}
			return EDMXReferencePage.img_file;
		} else if (element instanceof Schema) {
			return EDMXReferencePage.img_schema;
		}
		return null;
	}

	@Override
	public Font getFont(Object element) {

		return FilteredTree.getBoldFont(element, this.filteredTree,
				this.filterForBoldElements);
	}

	@Override
	public void addListener(ILabelProviderListener arg0) {
		// do nothing

	}

	@Override
	public void dispose() {
		// do nothing

	}

	@Override
	public boolean isLabelProperty(Object arg0, String arg1) {
		// do nothing
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener arg0) {
		// do nothing
	}

}
