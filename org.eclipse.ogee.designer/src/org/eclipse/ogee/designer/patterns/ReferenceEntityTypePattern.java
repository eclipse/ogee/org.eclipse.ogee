/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.patterns;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.pattern.AbstractPattern;
import org.eclipse.ogee.designer.contexts.AddReferencedEntityContext;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.BasicArtifactShape;
import org.eclipse.ogee.designer.shapes.ExtensionArtifactShape;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;

/**
 * This pattern class is created for 'Referenced EntityType' object in OData Model.
 * This contains all the methods required to create and update Referenced Entity Type's
 * Shape and BusinessObject.
 * 
 */
public class ReferenceEntityTypePattern extends AbstractPattern {

	/**
	 * ShapesFactory instance used to create all the shapes.
	 */
	private ShapesFactory shapesFactory;

	/**
	 * ArifactFactory instance used to create all the Business objects.
	 */
	private ArtifactFactory artifactFactory;

	/**
	 * Name and Icon group of the Entity
	 */
	private IdentifierShape entityNameIdentifier;

	/**
	 * Constructor. Initializes ShapesFactory and Artifact Factory
	 * 
	 * @param shapesFactory
	 *            ShapesFactory instance
	 * @param artifactFactory
	 *            ArtifactFactory instance
	 */
	public ReferenceEntityTypePattern(ShapesFactory shapesFactory,
			ArtifactFactory artifactFactory) {
		super(null);
		this.shapesFactory = shapesFactory;
		this.artifactFactory = artifactFactory;
	}

	/**
	 * Returns Referenced Entity Type Display Name.
	 * 
	 * @return String the Name of the Referenced Entity
	 */
	@Override
	public String getCreateName() {
		return Messages.ODATAEDITOR_REFERENCED_ENTITYTYPE_NAME;
	}

	/**
	 * Returns Referenced Entity Type Description.
	 * 
	 * @return String Referenced Entity Type Description
	 */
	@Override
	public String getCreateDescription() {
		return Messages.ODATAEDITOR_REFERENCED_ENTITYTYPE_DESC;
	}

	/**
	 * Returns Image ID.
	 * 
	 * @return String Image ID
	 */
	@Override
	public String getCreateImageId() {
		return ODataImageProvider.IMG_ENTITY_REFERENCE;
	}

	@Override
	public boolean isMainBusinessObjectApplicable(Object mainBusinessObject) {

		return ArtifactUtil.isEntityType(mainBusinessObject);

	}

	/**
	 * Returns true if the target is Diagram only as the Entity can only be
	 * created on a Diagram
	 * 
	 * @param context
	 *            ICreateContext instance
	 * @return boolean true if Entity can be created
	 */
	@Override
	public boolean canCreate(ICreateContext context) {

		return context.getTargetContainer() instanceof Diagram;
	}

	@Override
	public Object[] create(ICreateContext context) {

		// Entity Type creation
		Object entityType = this.artifactFactory.createReferencedEntityType();

		ODataShapeUtil.addReferencedGraphicalRepresentation(context, entityType,
				getFeatureProvider());
		return new Object[] { entityType };
	}

	@Override
	protected boolean isPatternRoot(PictogramElement pictogramElement) {
		Object domainObject = getBusinessObjectForPictogramElement(pictogramElement);
		return isMainBusinessObjectApplicable(domainObject);
	}

	@Override
	protected boolean isPatternControlled(PictogramElement pictogramElement) {
		Object domainObject = getBusinessObjectForPictogramElement(pictogramElement);
		return isMainBusinessObjectApplicable(domainObject);
	}

	/**
	 * Returns true if the target is the Diagram as the Entity can only be added
	 * on a Diagram
	 * 
	 * @param context
	 *            ICreateContext instance
	 * @return boolean true if Entity can be added
	 */
	@Override
	public boolean canAdd(IAddContext context) {

		return ArtifactUtil.isEntityType(context.getNewObject())
				&& context.getTargetContainer() instanceof Diagram;
	}

	/**
	 * Returns true if the layout has been updated
	 * 
	 * @param context
	 *            ICreateContext instance
	 * @return boolean true layout changed
	 */
	@Override
	public boolean layout(ILayoutContext context) {
		return ODataLayoutUtil.layout(context);

	}

	/**
	 * This adds the shape to the diagram after drag and drop from the palette
	 * 
	 * @param context
	 *            IAddContext instance
	 * @return PictogramElement the main container
	 */
	@Override
	public PictogramElement add(IAddContext context) {

		// Target Diagram
		Diagram targetDiagram = (Diagram) context.getTargetContainer();

		// Create the Artifact (Outer Rectangle)
		BasicArtifactShape artifactshape = this.shapesFactory
				.createReferencedArtifactShape(targetDiagram, context, true);

		PictogramElement shapes[] = artifactshape.getPictogramElements();
		ContainerShape entitytypecontainer = (ContainerShape) shapes[0];

		// Add elements to the Outer Shell
		addName(entitytypecontainer, context); // Add the Entity Name
		addEntitySets(entitytypecontainer, context); // Add the Entity Sets

		// call the layout feature
		ODataShapeUtil.layoutPictogramElement(entitytypecontainer,
				getFeatureProvider());

		// As soon as entity is added to the diagram Name should be made
		// editable
		this.entityNameIdentifier.enableDirectEdit();

		return entitytypecontainer;
	}

	/**
	 * Adds the Name of the Entity Type to the Container
	 * 
	 * @param entitytypecontainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addName(ContainerShape entitytypecontainer, IAddContext context) {
		Object businessObject = context.getNewObject();
		this.entityNameIdentifier = this.shapesFactory.createIdentifierShape(
				entitytypecontainer,
				ArtifactUtil.getEntityTypeName(businessObject),
				ODataImageProvider.IMG_ENTITY, context, businessObject, true,
				PropertyUtil.REFERENCED_ENTITY_NAME);
		this.shapesFactory.createPolyLineShape(entitytypecontainer, context);
	}

	/**
	 * Adds the default Entity Set to the container
	 * 
	 * @param entitytypecontainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addEntitySets(ContainerShape entitytypecontainer,
			IAddContext context) {
		EntityType entityType = (EntityType) context.getNewObject();
		List<EntitySet> newEntitySets = new ArrayList<EntitySet>();
		//Only for import scenario we will add the entities
		if(context instanceof AddReferencedEntityContext && ((AddReferencedEntityContext)context).isImported()){
			EntityContainer cont = artifactFactory.getEntityContainer();
			List<EntitySet> entitySets = cont.getEntitySets();
			for (EntitySet entitySet : entitySets) {
				if(entitySet.getType() == entityType){
					newEntitySets.add(entitySet);
				}
			}
		}
		boolean isExpanded = newEntitySets != null && newEntitySets.size() > 0 ? true
				: false;
		this.shapesFactory.createCollapsableTitleShape(entitytypecontainer,
				Messages.ODATAEDITOR_ENTITYSET_NAME, context, isExpanded);
		ExtensionArtifactShape invisibleRectangle = this.shapesFactory
				.createCollapsableRectangle(entitytypecontainer, context,
						PropertyUtil.ENTITYSET_RECTANGLE, isExpanded);
		ContainerShape container = ((ContainerShape) invisibleRectangle
				.getPictogramElements()[0]);
		if (newEntitySets != null) {
			for (EntitySet entitySet : newEntitySets) {
				this.shapesFactory.createIdentifierShape(container,
						ArtifactUtil.getEntitySetName(entitySet),
						ODataImageProvider.IMG_ENTITY_SET, context, entitySet,
						false, PropertyUtil.ENTITY_SET_NAME);
			}
		}
	}

	/**
	 * Returns true if, the entity shape can be updated
	 * 
	 * @param context
	 *            IUpdateContext
	 * @return boolean true if, the entity shape can be updated
	 */
	@Override
	public boolean canUpdate(IUpdateContext context) {
		Object obj = getBusinessObjectForPictogramElement(context
				.getPictogramElement());
		return ArtifactUtil.isEntityType(obj);
	}

	/**
	 * resizes the shape
	 * 
	 * @param context
	 *            IResizeShapeContext
	 */
	@Override
	public void resizeShape(IResizeShapeContext context) {
		// Inner rectangle heights
		ODataLayoutUtil.updateContainerHeights(
				(ContainerShape) context.getPictogramElement(),
				context.getHeight());
		super.resizeShape(context);
	}

}