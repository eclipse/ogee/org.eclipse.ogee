/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.patterns;

import java.util.List;

import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.pattern.AbstractPattern;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.BasicEnumTypeShape;
import org.eclipse.ogee.designer.shapes.ExtensionArtifactShape;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;

/**
 * This pattern class is created for 'EnumType' domain Object in OData Model.
 * This contains all the methods required to create and update Enum Type's Shape
 * and BusinessObject.
 * 
 */
public class EnumTypePattern extends AbstractPattern {

	/**
	 * ShapesFactory instance used to create all the shapes.
	 */
	private ShapesFactory shapesFactory;

	/**
	 * ArifactFactory instance used to create all the Business objects.
	 */
	private ArtifactFactory artifactFactory;

	/**
	 * Name and Icon group of the Enum Type
	 */
	private IdentifierShape enumTypeNameIdentifier;

	/**
	 * Constructor. Initializes ShapesFactory and Artifact Factory
	 * 
	 * @param shapesFactory
	 *            ShapesFactory instance
	 * @param artifactFactory
	 *            ArtifactFactory instance
	 */
	public EnumTypePattern(ShapesFactory shapesFactory,
			ArtifactFactory artifactFactory) {
		super(null);
		this.shapesFactory = shapesFactory;
		this.artifactFactory = artifactFactory;
	}

	/**
	 * Returns Enum Type Display Name.
	 * 
	 * @return String the Name of the Enum Type
	 */
	@Override
	public String getCreateName() {
		return Messages.ODATAEDITOR_ENUMTYPE_NAME;
	}

	/**
	 * Returns Enum Type Description.
	 * 
	 * @return String Enum Type Description
	 */
	@Override
	public String getCreateDescription() {
		return Messages.ODATAEDITOR_ENUMTYPE_DESC;
	}

	/**
	 * Returns Image ID.
	 * 
	 * @return String Image ID
	 */
	@Override
	public String getCreateImageId() {
		return ODataImageProvider.IMG_ENUM;
	}

	/**
	 * Returns true if the target is the Diagram as the Enum Type can only be
	 * added on a Diagram
	 * 
	 * @param context
	 *            ICreateContext instance
	 * @return boolean true if Enum Type can be added
	 */
	@Override
	public boolean canAdd(IAddContext context) {
		return ArtifactUtil.isEnumType(context.getNewObject())
				&& context.getTargetContainer() instanceof Diagram;
	}

	/**
	 * This adds the shape to the diagram after drag and drop from the pallette
	 * 
	 * @param context
	 *            IAddContext instance
	 * @return PictogramElement the main container
	 */
	@Override
	public PictogramElement add(IAddContext context) {

		// Target Diagram
		Diagram targetDiagram = (Diagram) context.getTargetContainer();

		// Create the Artifact (Outer Rectangle)
		BasicEnumTypeShape artifactshape = this.shapesFactory
				.createEnumTypeShape(targetDiagram, context);

		PictogramElement shapes[] = artifactshape.getPictogramElements();
		ContainerShape enumTypeContainer = (ContainerShape) shapes[0];

		// Add elements to the Outer Shell
		addName(enumTypeContainer, context); // Add the Enum Type Name

		addMembers(enumTypeContainer, context);

		// call the layout feature
		ODataShapeUtil.layoutPictogramElement(enumTypeContainer,
				getFeatureProvider());

		// As soon as Enum Type is added to the diagram, Name should be made
		// editable
		this.enumTypeNameIdentifier.enableDirectEdit();

		return enumTypeContainer;
	}

	/**
	 * Adds the Name of the Enum Type to the Container
	 * 
	 * @param enumTypeContainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addName(ContainerShape enumTypeContainer, IAddContext context) {
		Object businessObject = context.getNewObject();
		this.enumTypeNameIdentifier = this.shapesFactory.createIdentifierShape(
				enumTypeContainer,
				ArtifactUtil.getEnumTypeName(businessObject),
				ODataImageProvider.IMG_ENUM, context, businessObject, true,
				PropertyUtil.ENUM_NAME);
		this.shapesFactory.createPolyLineShape(enumTypeContainer, context);
	}

	/**
	 * Adds the members to the container
	 * 
	 * @param enumTypeContainer
	 *            ContainerShape
	 * @param context
	 *            IAddContext
	 */
	private void addMembers(ContainerShape enumTypeContainer,
			IAddContext context) {

		EnumType enumType = (EnumType) context.getNewObject();
		List<EnumMember> members = enumType.getMembers();

		boolean isExpanded = (members != null && members.size() > 0) ? true
				: false;

		this.shapesFactory.createCollapsableTitleShape(enumTypeContainer,
				Messages.ODATAEDITOR_ENUMMEMBER_NAME, context, isExpanded);
		ExtensionArtifactShape invisibleRectangle = this.shapesFactory
				.createCollapsableRectangle(enumTypeContainer, context,
						PropertyUtil.ENUM_MEMBER_RECTANGLE, isExpanded);
		ContainerShape container = ((ContainerShape) invisibleRectangle
				.getPictogramElements()[0]);

		if (members != null) {
			for (EnumMember member : members) {
				this.shapesFactory
						.createMemberShape(container,
								ArtifactUtil.getEnumMemberName(member),
								ArtifactUtil.getEnumMemberValue(member),
								ODataImageProvider.IMG_PROPERTY, context,
								member, false);
			}
		}

	}

	/**
	 * Returns true if the target is Diagram only as the Enum Type can only be
	 * created on a Diagram
	 * 
	 * @param context
	 *            ICreateContext instance
	 * @return boolean true if Enum Type can be created
	 */
	@Override
	public boolean canCreate(ICreateContext context) {
		return context.getTargetContainer() instanceof Diagram;
	}

	@Override
	public Object[] create(ICreateContext context) {
		// Enum Type creation
		Object enumType = this.artifactFactory.createDefaultEnumType();

		ODataShapeUtil.addGraphicalRepresentation(context, enumType,
				getFeatureProvider());
		return new Object[] { enumType };
	}

	/**
	 * Returns true if the layout has been updated
	 * 
	 * @param context
	 *            ICreateContext instance
	 * @return boolean true layout changed
	 */
	@Override
	public boolean layout(ILayoutContext context) {
		return ODataLayoutUtil.layout(context);
	}

	/**
	 * Returns true if, the enum type shape can be updated
	 * 
	 * @param context
	 *            IUpdateContext
	 * @return boolean true if, the enum type shape can be updated
	 */
	@Override
	public boolean canUpdate(IUpdateContext context) {
		Object obj = getBusinessObjectForPictogramElement(context
				.getPictogramElement());
		return ArtifactUtil.isEnumType(obj);
	}

	/**
	 * resizes the shape
	 * 
	 * @param context
	 *            IResizeShapeContext
	 */
	@Override
	public void resizeShape(IResizeShapeContext context) {
		// Inner rectangle heights
		ODataLayoutUtil.updateContainerHeights(
				(ContainerShape) context.getPictogramElement(),
				context.getHeight());
		ODataLayoutUtil
				.layout(new LayoutContext(context.getPictogramElement()));
		super.resizeShape(context);
	}

	@Override
	public boolean isMainBusinessObjectApplicable(Object mainBusinessObject) {
		return ArtifactUtil.isEnumType(mainBusinessObject);
	}

	@Override
	protected boolean isPatternControlled(PictogramElement pictogramElement) {
		Object domainObject = getBusinessObjectForPictogramElement(pictogramElement);
		return isMainBusinessObjectApplicable(domainObject);
	}

	@Override
	protected boolean isPatternRoot(PictogramElement pictogramElement) {
		Object domainObject = getBusinessObjectForPictogramElement(pictogramElement);
		return isMainBusinessObjectApplicable(domainObject);
	}

}
