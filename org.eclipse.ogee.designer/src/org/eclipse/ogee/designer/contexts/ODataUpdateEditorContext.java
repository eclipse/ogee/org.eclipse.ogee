/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.contexts;

import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * This will creates a context for the affected object and also with the
 * previous value and new value by extending CustomContext
 * 
 * 
 */
public class ODataUpdateEditorContext extends CustomContext {

	private Object affectedObject;
	private Object prevValue;
	private Object newValue;

	/**
	 * @return Object
	 */
	public Object getAffectedObject() {
		return affectedObject;
	}

	/**
	 * @return Object
	 */
	public Object getPrevValue() {
		return prevValue;
	}

	/**
	 * @return Object
	 */
	public Object getNewValue() {
		return newValue;
	}

	/**
	 * @param affectedObject
	 *            Affected object instance is passed
	 * @param prevValue
	 *            Previous value instance is passed
	 * @param newValue
	 *            New value instance is passed
	 * 
	 */
	public ODataUpdateEditorContext(Object affectedObject, Object prevValue,
			Object newValue) {

		this.affectedObject = affectedObject;
		this.prevValue = prevValue;
		this.newValue = newValue;

	}

	/**
	 * @param pictogramElements
	 */
	public ODataUpdateEditorContext(PictogramElement[] pictogramElements) {
		super(pictogramElements);
	}

}
