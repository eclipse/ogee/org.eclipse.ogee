/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.contexts;

import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.types.AssociationType;
import org.eclipse.ogee.designer.types.MultiplicityType;

/**
 * This will creates a context for the navigation properties Extending the
 * default add connection context there are added properties which are needed
 * for a custom navigation property implementation
 * 
 */
public class NavigationPropertyContext extends CustomContext {
	private PictogramElement sourcePictogramElement;
	private PictogramElement targetPictogramElement;
	private AssociationType associationType = AssociationType.NONE;

	private boolean isEditMode = false;

	private boolean isSecondNavPropertyForSelf = false;

	public boolean isSecondNavPropertyForSelf() {
		return isSecondNavPropertyForSelf;
	}

	public void setSecondNavPropertyForSelf(boolean isSecondNavPropertyForSelf) {
		this.isSecondNavPropertyForSelf = isSecondNavPropertyForSelf;
	}

	public AssociationType getAssociationType() {
		return associationType;
	}

	public void setAssociationType(AssociationType associationType) {
		this.associationType = associationType;
	}

	private MultiplicityType sourceMultuplicity;

	public MultiplicityType getSourceMultuplicity() {
		return sourceMultuplicity;
	}

	public void setSourceMultuplicity(MultiplicityType sourceMultuplicity) {
		this.sourceMultuplicity = sourceMultuplicity;
	}

	public MultiplicityType getTargetMultuplicity() {
		return targetMultuplicity;
	}

	public void setTargetMultuplicity(MultiplicityType targetMultuplicity) {
		this.targetMultuplicity = targetMultuplicity;
	}

	private MultiplicityType targetMultuplicity;
	private Object association;

	public NavigationPropertyContext(PictogramElement sourcePictogramElement,
			PictogramElement targetPictogramElement, Object association) {
		super(new PictogramElement[] { sourcePictogramElement });
		this.sourcePictogramElement = sourcePictogramElement;
		this.targetPictogramElement = targetPictogramElement;
		this.association = association;
	}

	public NavigationPropertyContext(PictogramElement sourcePictogramElement) {
		super(new PictogramElement[] { sourcePictogramElement });
		this.sourcePictogramElement = sourcePictogramElement;
	}

	public PictogramElement getSourcePictogramElement() {
		return sourcePictogramElement;
	}

	public void setSourcePictogramElement(
			PictogramElement sourcePictogramElement) {
		this.sourcePictogramElement = sourcePictogramElement;
	}

	public PictogramElement getTargetPictogramElement() {
		return targetPictogramElement;
	}

	public void setTargetPictogramElement(
			PictogramElement targetPictogramElement) {
		this.targetPictogramElement = targetPictogramElement;
	}

	public Object getAssociation() {
		return association;
	}

	public void setAssociation(Object association) {
		this.association = association;
	}

	public void setEditMode(boolean editMode) {
		this.isEditMode = editMode;
	}

	public boolean isEditMode() {
		return isEditMode;
	}
}
