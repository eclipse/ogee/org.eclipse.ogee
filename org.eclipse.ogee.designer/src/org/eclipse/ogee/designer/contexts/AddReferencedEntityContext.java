/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.contexts;

import org.eclipse.graphiti.features.context.IAreaContext;
import org.eclipse.graphiti.features.context.impl.AddContext;

/**
 * Add context for referenced entities
 * 
 */

public class AddReferencedEntityContext extends AddContext {
	private boolean isImported = false;

	/**
	 * @param isImported
	 *            the isImported to set
	 */
	public void setImported(boolean isImported) {
		this.isImported = isImported;
	}

	/**
	 * @return the isImported
	 */
	public boolean isImported() {
		return isImported;
	}

	public AddReferencedEntityContext(IAreaContext context, Object newObject) {
		super(context, newObject);
	}

	public AddReferencedEntityContext() {
		super();
	}
}
