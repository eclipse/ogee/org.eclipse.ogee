/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.contexts;

import org.eclipse.graphiti.features.context.impl.DeleteContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * This class is used to create the context for the link usage. This is required
 * to hide the usage.
 * 
 */
public class LinkUsageContext extends DeleteContext {

	/**
	 * @param pictogramElement
	 */
	public LinkUsageContext(PictogramElement pictogramElement) {
		super(pictogramElement);
	}

	/**
	 * @return boolean
	 */
	public boolean isDeletable() {

		return true;

	}

}
