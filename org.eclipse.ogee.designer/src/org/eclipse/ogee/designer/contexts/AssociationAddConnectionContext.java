/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.contexts;

import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.ogee.designer.types.AssociationType;
import org.eclipse.ogee.designer.types.MultiplicityType;

/**
 * This will creates a context for the associations Extending the default add
 * connection context there are added properties which are needed for a custom
 * association implementation
 * 
 */
public class AssociationAddConnectionContext extends AddConnectionContext {

	boolean isUniDirectionalAssociation;
	MultiplicityType sourceMultiplicity;

	Object targetRoleObject;

	Object sourceRoleObject;

	public Object getTargetRoleObject() {
		return targetRoleObject;
	}

	public void setTargetRoleObject(Object targetRoleObject) {
		this.targetRoleObject = targetRoleObject;
	}

	public Object getSourceRoleObject() {
		return sourceRoleObject;
	}

	public void setSourceRoleObject(Object sourceRoleObject) {
		this.sourceRoleObject = sourceRoleObject;
	}

	public MultiplicityType getSourceMultiplicity() {
		return sourceMultiplicity;
	}

	public void setSourceMultiplicity(MultiplicityType sourceMultiplicity) {
		this.sourceMultiplicity = sourceMultiplicity;
	}

	public MultiplicityType getTargetMultiplicity() {
		return targetMultiplicity;
	}

	public void setTargetMultiplicity(MultiplicityType targetMultiplicity) {
		this.targetMultiplicity = targetMultiplicity;
	}

	MultiplicityType targetMultiplicity;
	private AssociationType associationType = AssociationType.NONE;

	public boolean isUniDirectionalAssociation() {
		return associationType.equals(AssociationType.NONE)
				|| associationType.equals(AssociationType.UNIDIR);
	}

	public void setUniDirectionalAssociation(boolean isUniDirectionalAssociation) {
		this.isUniDirectionalAssociation = isUniDirectionalAssociation;
	}

	public AssociationAddConnectionContext(Anchor sourceAnchor,
			Anchor targetAnchor) {
		super(sourceAnchor, targetAnchor);
	}

	public void setAssociationType(AssociationType associationType) {
		this.associationType = associationType;
	}

	public AssociationType getAssociationType() {
		return this.associationType;
	}

}
