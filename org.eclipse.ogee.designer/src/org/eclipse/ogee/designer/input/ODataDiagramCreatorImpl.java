/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.input;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.services.GraphitiUi;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.ODataEditor;
import org.eclipse.ogee.designer.ODataEditorInput;
import org.eclipse.ogee.designer.ODataMultiPageEditor;
import org.eclipse.ogee.designer.api.IODataDiagramCreator;
import org.eclipse.ogee.designer.api.IODataDiagramInput;
import org.eclipse.ogee.designer.contexts.ODataUpdateEditorContext;
import org.eclipse.ogee.designer.features.ODataUpdateEditorFeature;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.service.ODataEditorService;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveRegistry;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

/**
 * The following class creates an OData Service model and opens up an OData
 * graphical editor.
 * 
 */
public final class ODataDiagramCreatorImpl implements IODataDiagramCreator {

	private static IODataDiagramCreator instance;
	private static final Logger logger = Logger.getLogger(Activator.PLUGIN_ID);

	/**
	 * Default constructor made private to make the class singleton.
	 */
	private ODataDiagramCreatorImpl() {
		// private constructor to make the class singleton.
	}

	/**
	 * Returns an instance of the IODataDiagramCreator implementation class.
	 * Double-checked locking
	 * 
	 * @return an instance of the IODataDiagramCreator implementation.
	 */
	public static IODataDiagramCreator getInstance() {

		if (instance == null) {
			synchronized (IODataDiagramCreator.class) {
				if (instance == null) {
					instance = new ODataDiagramCreatorImpl();
				}
			}
		}

		return instance;
	}

	@Override
	public void createDiagram(IODataDiagramInput diagramInput)
			throws CoreException {

		logger.trace("method_entry"); //$NON-NLS-1$
		final long start = System.nanoTime();

		IProgressMonitor monitor = diagramInput.getProgressMonitor();
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}
		monitor.beginTask(Messages.PROGRESS_TASK_NAME0, 100); // Total 100%

		monitor.setTaskName(Messages.PROGRESS_TASK_NAME1);
		if (!isValidDiagramInput(diagramInput)) {
			monitor.setCanceled(true);
			logger.trace("The createDiagram() API is invoked with missing mandatory input parameters"); //$NON-NLS-1$
			Logger.getLogger(Activator.PLUGIN_ID).log(
					Messages.ODATADIAGRAM_CREATOR_IMPL2);
			throw new CoreException(new Status(IStatus.ERROR,
					Activator.PLUGIN_ID, Messages.ODATADIAGRAM_CREATOR_IMPL2));
		}
		logger.trace(
				"isReadOnlyMode: %1$b", Boolean.valueOf(diagramInput.isReadOnlyMode())); //$NON-NLS-1$
		// to visualize OData model in read only mode, create a temp file.
		if (diagramInput.isReadOnlyMode()) {
			logger.trace("The createDiagram() API is invoked in ReadOnlyMode"); //$NON-NLS-1$
			final IWorkspaceRoot root = ResourcesPlugin.getWorkspace()
					.getRoot();
			final String serviceName = diagramInput.getServiceName();
			final String gwConnectionName = diagramInput.getGWConnectionName();
			// Create and open a hidden project.
			final String prjName = IODataEditorConstants.OEDITOR_TEMPPRJ;
			IProject project = root.getProject(prjName);

			// Check if already temporary project is present if yes than use
			// existing otherwise create new project.
			try {
				IProject[] allProjects = ResourcesPlugin.getWorkspace()
						.getRoot().getProjects(IContainer.INCLUDE_HIDDEN);
				for (int count = 0; count <= allProjects.length - 1; count++) {
					if (allProjects[count].getName().startsWith(
							IODataEditorConstants.OEDITOR_TEMPPRJ)) {
						project = allProjects[count];
						break;
					}

				}
			} catch (Exception ex) {
				Logger.getLogger(Activator.PLUGIN_ID).logError(ex);
				project.setHidden(true);
			}
			try {
				if (!project.exists()) {
					monitor.setTaskName(Messages.PROGRESS_TASK_NAME2 + prjName);
					project.create(new NullProgressMonitor());
				}
				if (!project.isOpen()) {
					project.open(new NullProgressMonitor());
				}
				if (!project.isHidden()) {
					project.setHidden(true);
				}
			} catch (CoreException e) {
				Logger.getLogger(Activator.PLUGIN_ID).logError(e);
				// re-create the project in case of any exceptions.
				monitor.setTaskName(Messages.PROGRESS_TASK_NAME3 + prjName);
				project.delete(true, new NullProgressMonitor());
				project.create(new NullProgressMonitor());
				project.open(new NullProgressMonitor());
				project.setHidden(true);
			}
			monitor.worked(5); // completed 5%
			final String fileName = gwConnectionName + "_" + serviceName
					+ "." + IODataEditorConstants.TEMPFILE_EXTENSION; //$NON-NLS-1$
			monitor.setTaskName(Messages.PROGRESS_TASK_NAME4 + fileName);
			final IFile tempDiagramFile = project.getFile(fileName);
			diagramInput.setDiagramFile(tempDiagramFile);
		}
		logger.trace(
				"The odata file location is: %1$s. The file content is: %2$s", //$NON-NLS-1$
				diagramInput.getDiagramFile().getLocation().toString(),
				diagramInput.getEDMXSet().toString());
		final boolean isBlankModel = isBlankModel(diagramInput.getEDMXSet());
		logger.trace("isBlankModel: %1$b", Boolean.valueOf(isBlankModel)); //$NON-NLS-1$
		monitor.setTaskName(Messages.PROGRESS_TASK_NAME5);
		final IPath diagramPath = diagramInput.getDiagramFile().getFullPath();
		final String diagramName = diagramPath.removeFileExtension()
				.lastSegment();
		final URI uri = URI.createPlatformResourceURI(diagramPath.toString(),
				true);
		final Diagram diagram = Graphiti.getPeCreateService().createDiagram(
				IODataEditorConstants.ODATA_DIAGRAM_TYPE, diagramName, true);
		logger.trace("Diagram created: %1$s", diagram.toString()); //$NON-NLS-1$
		// Create & save the model file in file system specified in the uri.
		boolean isCreated = ODataEditorService.createODataFile(uri, diagram,
				diagramInput.getEDMXSet());
		if (!isCreated) {
			logger.trace("Error in method ODataFileService.createODataFile()"); //$NON-NLS-1$
			Logger.getLogger(Activator.PLUGIN_ID).log(
					Messages.ODATADIAGRAM_CREATOR_IMPL2);
			throw new CoreException(new Status(IStatus.ERROR,
					Activator.PLUGIN_ID, Messages.ODATADIAGRAM_CREATOR_IMPL2));
		}
		monitor.worked(5); // completed 10%
		logger.trace("Diagram file created @: %1$s", uri.toString()); //$NON-NLS-1$
		final String providerId = GraphitiUi.getExtensionManager()
				.getDiagramTypeProviderId(diagram.getDiagramTypeId());
		ODataEditorInput editorInput = new ODataEditorInput(
				EcoreUtil.getURI(diagram), providerId);
		editorInput.setBlankModel(isBlankModel);
		editorInput.setDiagramInput(diagramInput);
		editorInput.setGWConnectionName(diagramInput.getGWConnectionName());

		if (diagramInput.isOpenEditor()) {
			logger.trace(
					"Invoked OData Editor with editor input: %1$s", editorInput.toString()); //$NON-NLS-1$
			monitor.setTaskName(Messages.PROGRESS_TASK_NAME6); // completed 10%
			openEditor(editorInput);
			monitor.worked(20); // completed 100%
			logger.trace("OData Editor opened successfully"); //$NON-NLS-1$
		}
		editorInput = null;
		logger.trace("method_exit"); //$NON-NLS-1$
		final long end = System.nanoTime();
		final double milliseconds = (end - start) / (double) (1000 * 1000);
		logger.trace(
				"ODataDiagramCreatorImpl#createDiagram() API executed in %1$s milli-seconds.", String.valueOf(milliseconds)); //$NON-NLS-1$
		monitor.done();
	}

	/**
	 * Returns true in case of Read Only Model.
	 * 
	 * @return boolean - true, for read-only model.
	 */
	@Override
	public boolean isReadOnlyModel() {

		boolean isReadOnly = false;

		final IEditorPart editor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (editor instanceof ODataMultiPageEditor) {
			final IEditorInput editorInput = editor.getEditorInput();
			if (editorInput instanceof ODataEditorInput) {
				isReadOnly = ((ODataEditorInput) editorInput).getDiagramInput()
						.isReadOnlyMode();
			}
		}

		return isReadOnly;
	}

	@Override
	public IODataDiagramInput createDiagramInput() {

		return new ODataDiagramInputImpl();
	}

	/**
	 * This method is responsible to refresh the editor from other workbench
	 * parts on model update.
	 * 
	 * @param affectedObject
	 *            Current Artifact
	 * @param prevValue
	 *            Value before model Update
	 * @param newValue
	 *            Value After model Update
	 */
	@Override
	public void refreshEditor(Object affectedObject, Object prevValue,
			Object newValue) {

		IEditorPart activeEditor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (activeEditor instanceof ODataMultiPageEditor) {
			ODataEditor designEditor = ((ODataMultiPageEditor) activeEditor)
					.getDesignEditor();
			final IFeatureProvider featureProvider = designEditor
					.getDiagramTypeProvider().getFeatureProvider();
			TransactionalEditingDomain editingDomain = designEditor
					.getEditingDomain();
			final ODataUpdateEditorContext context = new ODataUpdateEditorContext(
					affectedObject, prevValue, newValue);
			final ODataUpdateEditorFeature updateFeature = new ODataUpdateEditorFeature(
					featureProvider);
			editingDomain.getCommandStack().execute(
					new RecordingCommand(editingDomain) {
						@Override
						public void doExecute() {
							updateFeature.execute(context);
						}

						@Override
						public String getLabel() {
							return Messages.ODATA_UPDATE_COMMAND;
						}
					});
		}
	}

	/**
	 * Return the OData model file given a path to the .odata model file.
	 * 
	 * @param fullPath
	 *            - path of the OData model file
	 * @return an IFile for the OData model file.
	 */
	public static IFile getModelFile(final IPath fullPath) {

		return ResourcesPlugin.getWorkspace().getRoot()
				.getFile(fullPath.makeAbsolute());
	}

	/**
	 * The following method checks if the OData service model is empty.
	 * 
	 * @param edmxSet
	 *            EDMXSet
	 * 
	 * @return boolean - if the model passed from wizard framework is empty.
	 */
	private static boolean isBlankModel(final EDMXSet edmxSet) {

		boolean isBlankModel = false;

		if (ArtifactUtil.getAllArtifacts(edmxSet).size() == 0) {
			isBlankModel = true;
		}

		return isBlankModel;
	}

	/**
	 * Opens up an OData Graphical editor with the ODataEditorInput.
	 * 
	 * @param editorInput
	 *            - instance of ODataDiagramEditorInput
	 */
	private static void openEditor(final ODataEditorInput editorInput) {

		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					IPerspectiveRegistry reg = PlatformUI.getWorkbench()
							.getPerspectiveRegistry();
					IPerspectiveDescriptor odataPersp = reg
							.findPerspectiveWithId(IODataEditorConstants.ODATA_PERSPECTIVE_ID);
					PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage().setPerspective(odataPersp);
					PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage()
							.openEditor(editorInput, ODataEditor.EDITOR_ID);
				} catch (PartInitException e) {
					Logger.getLogger(Activator.PLUGIN_ID).log(e.getMessage());
					final IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.ODATADIAGRAM_CREATOR_IMPL1, e);
					ErrorDialog.openError(PlatformUI.getWorkbench()
							.getDisplay().getActiveShell(),
							Messages.ODATADIAGRAM_CREATOR_IMPL2,
							e.getMessage(), status);
				} catch (Exception e) {
					Logger.getLogger(Activator.PLUGIN_ID).log(e.getMessage());
					final IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.ODATADIAGRAM_CREATOR_IMPL1, e);
					ErrorDialog.openError(PlatformUI.getWorkbench()
							.getDisplay().getActiveShell(),
							Messages.ODATADIAGRAM_CREATOR_IMPL2,
							e.getMessage(), status);
				}
			}
		});
	}

	/*
	 * The following method checks if mandatory input parameters are set in
	 * diagramInput.
	 */
	private static boolean isValidDiagramInput(
			final IODataDiagramInput diagramInput) {

		boolean isValid = true;

		if (diagramInput == null) {
			isValid = false;
		} else {
			if (diagramInput.isReadOnlyMode()) {
				if (diagramInput.getEDMXSet() == null
						|| diagramInput.getGWConnectionName().isEmpty()) {
					isValid = false;
				}
			} else {
				if (diagramInput.getDiagramFile() == null
						|| diagramInput.getDiagramFile().getLocation() == null
						|| diagramInput.getEDMXSet() == null) {
					isValid = false;
				}
			}
		}

		return isValid;
	}

}
