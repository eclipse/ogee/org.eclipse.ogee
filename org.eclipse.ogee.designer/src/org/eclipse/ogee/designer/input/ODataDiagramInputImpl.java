/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.input;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ogee.designer.api.IODataDiagramInput;
import org.eclipse.ogee.model.odata.EDMXSet;

public class ODataDiagramInputImpl implements IODataDiagramInput {

	private IFile diagramFile = null;
	private EDMXSet edmxSet = null;
	private IProgressMonitor monitor = null;
	private boolean isOpenEditor = true;
	private boolean isReadOnlyMode = false;
	private String connectionName = ""; //$NON-NLS-1$
	private String serviceName = ""; //$NON-NLS-1$
	private Integer serviceVersion;

	@Override
	public IFile getDiagramFile() {

		return this.diagramFile;
	}

	@Override
	public void setDiagramFile(final IFile diagramFile) {

		this.diagramFile = diagramFile;
	}

	/**
	 * @return boolean - if true, open the OData editor.
	 */
	@Override
	public boolean isOpenEditor() {

		return this.isOpenEditor;
	}

	/**
	 * @param isOpenEditor
	 *            - by default the parameter is set to true. If we intend not to
	 *            open the OData Editor set the parameter to false.
	 */
	@Override
	public void setOpenEditor(boolean isOpenEditor) {

		this.isOpenEditor = isOpenEditor;
	}

	@Override
	public EDMXSet getEDMXSet() {

		return this.edmxSet;
	}

	@Override
	public void setEDMXSet(EDMXSet edmxSet) {

		this.edmxSet = edmxSet;
	}

	@Override
	public boolean isReadOnlyMode() {

		return this.isReadOnlyMode;
	}

	@Override
	public void setReadOnlyMode(boolean isReadOnlyMode) {

		this.isReadOnlyMode = isReadOnlyMode;
	}

	@Override
	public IProgressMonitor getProgressMonitor() {

		return this.monitor;
	}

	@Override
	public void setProgressMonitor(IProgressMonitor monitor) {

		this.monitor = monitor;
	}

	@Override
	public String getGWConnectionName() {

		return this.connectionName;
	}

	@Override
	public void setGWConnectionName(String connectionName) {

		this.connectionName = connectionName;
	}

	@Override
	public String getServiceName() {

		return this.serviceName;
	}

	@Override
	public void setServiceName(String serviceName) {

		this.serviceName = serviceName;
	}

	@Override
	public Integer getServiceVersion() {
		
		return this.serviceVersion;
	}

	@Override
	public void setServiceVersion(Integer serviceVersion) {
		
		this.serviceVersion = serviceVersion;
	}

}
