/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.visualizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.platform.IDiagramContainer;
import org.eclipse.ogee.designer.ODataEditor;
import org.eclipse.ogee.designer.types.ColumnType;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.designer.visualizer.ODataEntityComparator.SortingField;
import org.eclipse.ogee.designer.visualizer.ODataEntityComparator.SortingType;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.FunctionImport;

/**
 * This class is responsible to define default layout functionality of all the
 * artifacts.
 * 
 */
public class ODataArtifactLayout {

	private List<ODataArtifactNode> entitiesWithoutAssoc = new ArrayList<ODataArtifactNode>();
	private List<ODataArtifactNode> entitiesWithAssoc = new ArrayList<ODataArtifactNode>();
	private IFeatureProvider fp;
	private PictogramElement[] pes;

	/**
	 * Default Constructor
	 */
	public ODataArtifactLayout(IFeatureProvider fp, PictogramElement... pes) {
		this.fp = fp;
		this.pes = pes;
	}

	/**
	 * Layout all the artifacts
	 * 
	 * @return List<ODatArtifactNode> Entities with association
	 */
	public void layoutArtifacts() {
		int rowCount = 0;

		if (pes != null && pes.length == 1 && pes[0] instanceof Diagram) {
			Diagram diagram = (Diagram) pes[0];

			// Get all the Artifacts
			SortedMap<ColumnType, List<Shape>> shapesMap = getAllTopContainers(
					diagram, fp);

			// Get all the NonSelf Associations and remove more than one
			// association between the same entities
			List<Connection> associations = getAllNonSelfAssociations(diagram);

			// Populate entity and association count to a list and separate
			// entities with and without associations
			populateAssociationCount(shapesMap.get(ColumnType.ENTITYTYPE),
					associations);
			// Sort the entities without associations in the ascending order of
			// height
			ODataEntityComparator comparatorRowHeight = new ODataEntityComparator(
					SortingType.ASCENDING, SortingField.ROWHEIGHT);
			Collections.sort(entitiesWithoutAssoc, comparatorRowHeight);
			// Sort the entities list in the descending order of association
			// count. Following code sorts the objects using the comparator
			ODataEntityComparator comparatorDesc = new ODataEntityComparator(
					SortingType.DESCENDING, SortingField.ASSOCCOUNT);
			Collections.sort(entitiesWithAssoc, comparatorDesc);
			// Assign row and column number for each of the entity and return
			// the maximum number of rows
			rowCount = updateRowAndColumn();
			// Sort the entities in the ascending order of row number and column
			// number
			ODataEntityComparator comparatorRow = new ODataEntityComparator(
					SortingType.ASCENDING, SortingField.ROWNUMBER);
			Collections.sort(entitiesWithoutAssoc, comparatorRow);
			Collections.sort(entitiesWithAssoc, comparatorRow);
			// Assign Row Height for all the entities
			assignRowHeight();
			// Assign X and Y coordinates for all entities
			updateXYPosition(rowCount, shapesMap);
		}
	}

	/**
	 * Getter to get all entities without associations. Entities with only self
	 * associations are considered as without associations
	 * 
	 * @return List<ODataArtifactNode> Entities without associations
	 */
	public List<ODataArtifactNode> getEntitiesWithoutAssoc() {
		return entitiesWithoutAssoc;
	}

	/**
	 * Getter to get all entities with associations. Entities with only self
	 * associations are not included
	 * 
	 * @return List<ODataArtifactNode> Entities with associations
	 */
	public List<ODataArtifactNode> getEntitiesWithAssoc() {
		return entitiesWithAssoc;
	}

	/**
	 * Populate all the artifacts into a sorted map
	 * 
	 */
	private SortedMap<ColumnType, List<Shape>> getAllTopContainers(
			Diagram diagram, IFeatureProvider fp) {
		EList<Shape> children = diagram.getChildren();

		SortedMap<ColumnType, List<Shape>> shapesMap = new TreeMap<ColumnType, List<Shape>>();
		shapesMap.put(ColumnType.ENTITYTYPE, new ArrayList<Shape>());
		shapesMap.put(ColumnType.FUNCTIONIMPORT, new ArrayList<Shape>());
		shapesMap.put(ColumnType.COMPLEXTYPE, new ArrayList<Shape>());
		shapesMap.put(ColumnType.ENUMTYPE, new ArrayList<Shape>());

		for (Shape shape : children) {
			if (PropertyUtil.isTopContainer(shape)) {
				if (fp.getBusinessObjectForPictogramElement(shape) instanceof EntityType) {
					shapesMap.get(ColumnType.ENTITYTYPE).add(shape);
				} else if (fp.getBusinessObjectForPictogramElement(shape) instanceof ComplexType) {
					shapesMap.get(ColumnType.COMPLEXTYPE).add(shape);
				} else if (fp.getBusinessObjectForPictogramElement(shape) instanceof EnumType) {
					shapesMap.get(ColumnType.ENUMTYPE).add(shape);
				} else if (fp.getBusinessObjectForPictogramElement(shape) instanceof FunctionImport) {
					shapesMap.get(ColumnType.FUNCTIONIMPORT).add(shape);
				}
			}
		}
		return shapesMap;
	}

	/**
	 * Return all the non self-associations in the diagram
	 */
	private List<Connection> getAllNonSelfAssociations(Diagram diagram) {

		Shape source, target, source1, target1;
		Anchor sourceAnchor, targetAnchor;
		boolean assocExist;
		EList<Connection> connections = diagram.getConnections();
		List<Connection> associations = new ArrayList<Connection>();

		for (Connection connection : connections) {
			sourceAnchor = connection.getStart();
			targetAnchor = connection.getEnd();

			if (sourceAnchor == null || targetAnchor == null) {
				continue;
			}
			source = (Shape) sourceAnchor.getParent();
			target = (Shape) targetAnchor.getParent();
			// Filter self associations and more than one association between
			// the same entities
			if (source != target) {
				assocExist = false;
				for (Connection conn : associations) {
					source1 = (Shape) conn.getStart().getParent();
					target1 = (Shape) conn.getEnd().getParent();
					// Check if an association already exists between the same
					// entities
					if ((source == source1 && target == target1)
							|| (source == target1 && target == source1)) {
						assocExist = true;
						break;
					}
				}
				if (assocExist == false) {
					associations.add(connection);
				}
			}
		}
		return associations;
	}

	/**
	 * Populate the association count and the associated entities list in the
	 * ODataArtifactNode. Self associations are not counted. More than 1
	 * association between the same entities are considered as 1
	 */
	private void populateAssociationCount(List<Shape> entityShapes,
			List<Connection> associations) {
		int assocCount;
		Shape source, target;
		List<Shape> associatedEntities;
		ODataArtifactNode entityNode;

		for (Shape entShape : entityShapes) {
			associatedEntities = new ArrayList<Shape>();
			assocCount = 0;
			for (Connection conn : associations) {
				source = (Shape) conn.getStart().getParent();
				target = (Shape) conn.getEnd().getParent();
				if (source == entShape) {
					associatedEntities.add(target);
					assocCount = assocCount + 1;
				}
				if (target == entShape) {
					associatedEntities.add(source);
					assocCount = assocCount + 1;
				}
			}
			if (assocCount == 0) {
				entityNode = new ODataArtifactNode((ContainerShape) entShape);
				entityNode.setAssocCount(0);
				entitiesWithoutAssoc.add(entityNode);
			} else {
				entityNode = new ODataArtifactNode((ContainerShape) entShape);
				entityNode.setAssocCount(assocCount);
				entityNode.setAssociatedEntities(associatedEntities);
				entitiesWithAssoc.add(entityNode);
			}
		}
	}

	/**
	 * Set Row and Column numbers for the entities
	 */
	private int updateRowAndColumn() {

		int rowNum = 0, colNum = 0, rowNumMainEnt = 0, entCount = 0;
		List<ODataArtifactNode> associatedEntNodes;
		List<Shape> associatedEntities;

		// Assign row and column numbers to the entities without
		// Associations
		entCount = entitiesWithoutAssoc.size();
		for (ODataArtifactNode ent : entitiesWithoutAssoc) {
			if (rowNum == 0) {
				rowNum = 1;
			}
			// For Odd rows increase the column number, for even rows
			// decrease the column number
			if ((rowNum % 2) != 0) {
				colNum = colNum + 1;
				if (colNum > 4) {
					// Place the entities in even rows in the increasing order
					// of height
					colNum = entCount - (rowNum * 4);
					if (colNum > 4) {
						colNum = 4;
					}
					rowNum = rowNum + 1;
				}
			} else {
				colNum = colNum - 1;
				if (colNum < 1) {
					colNum = 1;
					rowNum = rowNum + 1;
				}
			}
			ent.setRowNumber(rowNum);
			ent.setColumnNumber(colNum);
		}

		// Assign row and column numbers to the entities with associations
		for (ODataArtifactNode ent : entitiesWithAssoc) {
			associatedEntNodes = new ArrayList<ODataArtifactNode>();

			// Find the Associated Entities for an association and sort
			// this in the increasing order of their association count and
			// height
			associatedEntities = ent.getAssociatedEntities();
			for (Shape associatedEnt : associatedEntities) {
				for (ODataArtifactNode ent1 : entitiesWithAssoc) {
					if (associatedEnt == ent1.getParentShape()) {
						associatedEntNodes.add(ent1);
						break;
					}
				}
			}

			ODataEntityComparator comparatorAsc = new ODataEntityComparator(
					SortingType.ASCENDING, SortingField.ASSOCCOUNT);
			Collections.sort(associatedEntNodes, comparatorAsc);

			if (ent.getRowNumber() == 0 && ent.getColumnNumber() == 0) {
				rowNum = rowNum + 1;
				colNum = 2;
				rowNumMainEnt = rowNum; // Row number of last placed main
										// entity
				ent.setRowNumber(rowNum);
				ent.setColumnNumber(colNum);

				// Assign row and column number for the associated entities
				// Position the entities whose height is less than or equal to
				// the
				// height of the main entity. If none of the entities are of
				// less
				// height than the main entity, go to next row
				if (ent.getAssocCount() > 3) {
					for (ODataArtifactNode ent1 : associatedEntNodes) {
						if (ent1.getRowNumber() == 0
								&& ent1.getColumnNumber() == 0
								&& ent.getParentShape().getGraphicsAlgorithm()
										.getHeight() >= ent1.getParentShape()
										.getGraphicsAlgorithm().getHeight()) {
							switch (colNum) {
							case 1:
								colNum = 3;
								break;
							case 2:
								colNum = 1;
								break;
							case 3:
								colNum = 4;
								break;
							default:
								break;
							}
							ent1.setRowNumber(rowNum);
							ent1.setColumnNumber(colNum);
							if (colNum == 4) {
								break;
							}
						}
					}
					// Set colNum as 4 so that in the next for loop right row
					// and column will be assigned
					colNum = 4;
				}
			}

			for (ODataArtifactNode ent1 : associatedEntNodes) {
				if (ent1.getRowNumber() == 0 && ent1.getColumnNumber() == 0) {
					switch (colNum) {
					case 1:
						colNum = 3;
						break;
					case 2:
						colNum = 1;
						if (rowNum != rowNumMainEnt) {
							rowNum = rowNum + 1;
						}
						break;
					case 3:
						colNum = 4;
						break;
					case 4:
						if (rowNum == rowNumMainEnt) {
							rowNum = rowNum + 1;
							colNum = 1;
						} else {
							colNum = 2;
						}
						break;
					default:
						break;
					}
					ent1.setRowNumber(rowNum);
					ent1.setColumnNumber(colNum);
				}
			}
		}
		return rowNum;
	}

	/**
	 * Set the Row Height for all the entities. Row Height is the height of the
	 * tallest entity in the row
	 */
	private void assignRowHeight() {
		int prevRowNumber = 1;
		int maxRowHeight = 0;
		int rowHeight = 0;
		int rowNumber = 0;
		List<ODataArtifactNode> entRowHeight = new ArrayList<ODataArtifactNode>();

		for (ODataArtifactNode ent : entitiesWithoutAssoc) {
			rowNumber = ent.getRowNumber();
			// When one row is complete assign row height to all entities in
			// that row
			if (prevRowNumber < rowNumber) {
				for (ODataArtifactNode entNode : entRowHeight) {
					entNode.setRowHeight(maxRowHeight);
				}
				maxRowHeight = 0;
				entRowHeight = new ArrayList<ODataArtifactNode>();
			}
			// Find maximum row height
			rowHeight = ent.getParentShape().getGraphicsAlgorithm().getHeight();
			maxRowHeight = Math.max(rowHeight, maxRowHeight);
			entRowHeight.add(ent);
			prevRowNumber = rowNumber;
		}

		for (ODataArtifactNode ent : entitiesWithAssoc) {
			rowNumber = ent.getRowNumber();
			if (prevRowNumber < rowNumber) {
				for (ODataArtifactNode entNode : entRowHeight) {
					entNode.setRowHeight(maxRowHeight);
				}
				maxRowHeight = 0;
				entRowHeight = new ArrayList<ODataArtifactNode>();
			}

			rowHeight = ent.getParentShape().getGraphicsAlgorithm().getHeight();
			maxRowHeight = Math.max(rowHeight, maxRowHeight);
			entRowHeight.add(ent);
			prevRowNumber = rowNumber;
		}
	}

	/**
	 * Set the X and Y coordinates for each of the entity shapes
	 */
	private void updateXYPosition(int rowNum,
			SortedMap<ColumnType, List<Shape>> sortedMap) {

		final int offsetX = 100;
		int colWidth = 300;
		final int colHeight = 100;
		int columnNumber;
		int xPos = 0;
		int yPos = 50;
		ODataArtifactNode prevNode = null;
		int prevRowHeight = 0;
		GraphicsAlgorithm currentGA;
		int entityNodeRowHeight, currentRowHeight;
		int funcImports, complexTypes;
		int maximum = 0;

		// Get the width of the editor and divide into 4 parts to get the
		// maximum possible column width. If this is greater than 300 use this
		// width, else use 300. This will not work during import
		IDiagramContainer diagramEditor = fp.getDiagramTypeProvider()
				.getDiagramBehavior().getDiagramContainer();
		if (diagramEditor instanceof ODataEditor) {
			GraphicalViewer graphicalViewer = ((ODataEditor) diagramEditor)
					.getGraphicalViewer();
			if (graphicalViewer != null) {
				FigureCanvas figureCanvas = (FigureCanvas) graphicalViewer
						.getControl();
				maximum = figureCanvas.getViewport().getHorizontalRangeModel()
						.getExtent();
				colWidth = Math.max(300, maximum / 4);
			}

		}

		for (ODataArtifactNode entityNode : entitiesWithoutAssoc) {
			columnNumber = entityNode.getColumnNumber();
			xPos = offsetX + colWidth * (columnNumber - 1);
			currentGA = entityNode.getParentShape().getGraphicsAlgorithm();
			entityNodeRowHeight = entityNode.getRowHeight();
			currentRowHeight = entityNodeRowHeight != 0 ? entityNodeRowHeight
					: currentGA.getHeight();
			if ((prevNode != null)
					&& (prevNode.getRowNumber() < entityNode.getRowNumber())) {
				yPos = yPos + prevNode.getRowHeight() + colHeight;
				prevRowHeight = currentRowHeight;
			}
			currentGA.setX(xPos);
			currentGA.setY(yPos);
			prevNode = entityNode;

			prevRowHeight = Math.max(prevRowHeight, currentRowHeight);

		}

		// Entities with Associations has to be laid out in the next row
		for (ODataArtifactNode entityNode : entitiesWithAssoc) {
			columnNumber = entityNode.getColumnNumber();
			xPos = offsetX + colWidth * (entityNode.getColumnNumber() - 1);
			currentGA = entityNode.getParentShape().getGraphicsAlgorithm();
			entityNodeRowHeight = entityNode.getRowHeight();
			currentRowHeight = entityNodeRowHeight != 0 ? entityNodeRowHeight
					: currentGA.getHeight();
			if ((prevNode != null)
					&& (prevNode.getRowNumber() < entityNode.getRowNumber())) {
				yPos = yPos + prevNode.getRowHeight() + colHeight;
				prevRowHeight = currentRowHeight;
			}
			currentGA.setX(xPos);
			currentGA.setY(yPos);
			prevNode = entityNode;

			prevRowHeight = Math.max(prevRowHeight, currentRowHeight);

		}

		List<Shape> functionImportShapes = sortedMap
				.get(ColumnType.FUNCTIONIMPORT);
		prevRowHeight = updateShapes(offsetX, colWidth, colHeight, yPos,
				prevRowHeight, functionImportShapes);
		funcImports = functionImportShapes.size();
		yPos = funcImports > 0 ? functionImportShapes.get(funcImports - 1)
				.getGraphicsAlgorithm().getY() : yPos;

		List<Shape> complexTypeShapes = sortedMap.get(ColumnType.COMPLEXTYPE);
		prevRowHeight = updateShapes(offsetX, colWidth, colHeight, yPos,
				prevRowHeight, complexTypeShapes);

		complexTypes = complexTypeShapes.size();
		yPos = complexTypes > 0 ? complexTypeShapes.get(complexTypes - 1)
				.getGraphicsAlgorithm().getY() : yPos;

		List<Shape> enumTypeShapes = sortedMap.get(ColumnType.ENUMTYPE);
		prevRowHeight = updateShapes(offsetX, colWidth, colHeight, yPos,
				prevRowHeight, enumTypeShapes);
	}

	/**
	 * @param offsetX
	 * @param colWidth
	 * @param colHeight
	 * @param yPos
	 * @param previousRowHeight
	 * @param functionImportShapes
	 * @return
	 */
	public int updateShapes(final int offsetX, final int colWidth,
			final int colHeight, int yPos, int previousRowHeight,
			List<Shape> functionImportShapes) {
		int xPos = 0;
		int columnNum = 0;
		int rowHeight = previousRowHeight;
		int prevRowHeight = 0;
		int polyHeight = 0;
		for (Shape shape : functionImportShapes) {
			if (columnNum == 0) {
				yPos = yPos + (rowHeight == 0 ? 0 : rowHeight + colHeight);
			}
			xPos = offsetX + (colWidth * columnNum);
			if (shape.getGraphicsAlgorithm() instanceof Polygon) {
				EList<Point> points = ((Polygon) (shape.getGraphicsAlgorithm()))
						.getPoints();
				polyHeight = points.get(3).getY();
				prevRowHeight = Math.max(prevRowHeight, polyHeight);
			} else {
				prevRowHeight = Math.max(prevRowHeight, shape
						.getGraphicsAlgorithm().getHeight());
			}
			shape.getGraphicsAlgorithm().setX(xPos);
			shape.getGraphicsAlgorithm().setY(yPos);
			columnNum++;

			if (columnNum > 3) {
				columnNum = 0;
				columnNum = 0;
				rowHeight = prevRowHeight;
				prevRowHeight = 0;
			}
		}
		if (prevRowHeight != 0) {
			rowHeight = prevRowHeight;
		}
		return rowHeight;
	}
}