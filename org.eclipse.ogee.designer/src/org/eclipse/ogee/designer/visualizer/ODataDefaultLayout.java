/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.visualizer;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.layout.ILayout;

/**
 * This class is responsible to define default layout functionality.
 * 
 * 
 */
public class ODataDefaultLayout implements ILayout {

	/**
	 * Default Constructor.
	 */
	public ODataDefaultLayout() {
		// Default Constructor
	}

	/**
	 * Returns true if the layout can be done
	 * 
	 * @see
	 * org.eclipse.ogee.designer.layout.ILayout#canLayout(org.
	 * eclipse.graphiti.mm.pictograms.PictogramElement[])
	 */
	@Override
	public boolean canLayout(PictogramElement... pes) {
		if (pes != null && pes.length == 1 && pes[0] instanceof Diagram) {
			return true;
		}
		return false;
	}

	/**
	 * Layouts the given objects
	 * Calls the artifact layout first to layout all domain objects
	 * Calls the association layout then to layout the associations
	 * 
	 * @see
	 * org.eclipse.ogee.designer.layout.ILayout#layout(org.eclipse
	 * .graphiti.features.IFeatureProvider,
	 * org.eclipse.graphiti.mm.pictograms.PictogramElement[])
	 */

	@Override
	public void layout(IFeatureProvider fp, PictogramElement... pes) {

		if (pes != null && pes.length == 1 && pes[0] instanceof Diagram) {

			ODataArtifactLayout artifactLayout = new ODataArtifactLayout(fp,
					pes);
			artifactLayout.layoutArtifacts();
			ODataAssociationLayout associationLayout = new ODataAssociationLayout(
					fp);
			associationLayout.layoutAssociations(artifactLayout
					.getEntitiesWithAssoc());
		}

	}

}
