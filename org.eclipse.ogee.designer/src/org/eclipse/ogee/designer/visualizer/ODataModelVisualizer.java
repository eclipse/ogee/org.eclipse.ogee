/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.visualizer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.ogee.designer.ODataEditorInput;
import org.eclipse.ogee.designer.contexts.AddReferencedEntityContext;
import org.eclipse.ogee.designer.contexts.AssociationAddConnectionContext;
import org.eclipse.ogee.designer.features.ODataLayoutFeature;
import org.eclipse.ogee.designer.types.AssociationType;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.AssociationUtil;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;

/**
 * Implementation to visualize the OData model
 * 
 */
public class ODataModelVisualizer {

	private IDiagramTypeProvider dtp;

	/**
	 * Constructor
	 * 
	 * @param dtp
	 *            IDiagramTypeProvider
	 */
	public ODataModelVisualizer(final IDiagramTypeProvider dtp) {

		this.setDiagramTypeProvider(dtp);
	}

	/**
	 * Get all the objects from the model and call the visualization API Loops
	 * through all schemata and gets all entities, associations, complex types,
	 * function imports and enum types and calls visualize to get the UI
	 * representation of the objects
	 * 
	 * @param input
	 *            ODataEditorInput
	 * 
	 */
	public void visualizeModel(final ODataEditorInput input) {

		EList<SchemaClassifier> schemaClassifiers;

		final DataService odataService = input.getDiagramInput().getEDMXSet()
				.getMainEDMX().getDataService();
		final EList<Schema> schemaList = odataService.getSchemata();
		final List<EntityType> entityTypes = new ArrayList<EntityType>();
		final List<Association> associations = new ArrayList<Association>();
		final List<ComplexType> complexTypes = new ArrayList<ComplexType>();
		final List<EnumType> enumTypes = new ArrayList<EnumType>();
		final List<FunctionImport> functionImports = new ArrayList<FunctionImport>();

		for (Schema schema : schemaList) {
			schemaClassifiers = schema.getClassifiers();
			for (SchemaClassifier schemaClassifier : schemaClassifiers) {
				if (SchemaClassifier.SERVICE_VALUE == schemaClassifier
						.getValue()) {
					entityTypes.addAll(schema.getEntityTypes());
					associations.addAll(schema.getAssociations());
					complexTypes.addAll(schema.getComplexTypes());
					enumTypes.addAll(schema.getEnumTypes());
					if (schema.getContainers().size() > 0) {
						for (int i = 0; i < schema.getContainers().size(); i++) {
							functionImports.addAll((schema.getContainers()
									.get(i)).getFunctionImports());
						}
					}
				}
			}
		}

		// Show all entities
		for (EntityType entityType : entityTypes) {
			this.visualizeObj(entityType);
		}
		// Show all associations
		for (Association association : associations) {
			this.visualiseAssociation(association);
		}
		// Show all complex types
		for (ComplexType complexType : complexTypes) {
			this.visualizeObj(complexType);
		}
		// Show all function imports
		for (FunctionImport funcImport : functionImports) {
			this.visualizeObj(funcImport);
		}
		// Show all enum types
		for (EnumType enumType : enumTypes) {
			this.visualizeObj(enumType);
		}

		// Disable direct edit on the last object once the file is imported
		this.getDiagramTypeProvider().getFeatureProvider()
				.getDirectEditingInfo().setActive(false);

		/*
		 * // Updating the Progress Monitor
		 * ODataEditorService.updateProgressMonitor(null, 15, input
		 * .getDiagramInput().getProgressMonitor()); // completed 30%
		 */
		input.getDiagramInput().getProgressMonitor().worked(15);

		ODataLayoutFeature odlf = new ODataLayoutFeature(this
				.getDiagramTypeProvider().getFeatureProvider(),
				ODataLayoutUtil.getCurrentLayout(null));

		CustomContext context = new CustomContext();
		context.setPictogramElements(new PictogramElement[] { getDiagramTypeProvider()
				.getDiagram() });

		if (odlf.canExecute(context)) {
			odlf.execute(context);
		}
		/*
		 * // Updating the Progress Monitor
		 * ODataEditorService.updateProgressMonitor(null, 35, input
		 * .getDiagramInput().getProgressMonitor()); // completed 65%
		 */input.getDiagramInput().getProgressMonitor().worked(35);

		// Create the new OData objects to be visualized
		final ILayoutContext layoutContext = new LayoutContext(this
				.getDiagramTypeProvider().getDiagram());
		this.getDiagramTypeProvider().getFeatureProvider()
				.layoutIfPossible(layoutContext);
	}

	/**
	 * Get the DiagramTypeprovider
	 * 
	 * @return IDiagramTypeProvider
	 */
	public IDiagramTypeProvider getDiagramTypeProvider() {

		return this.dtp;
	}

	/**
	 * Set the DiagramTypeprovider
	 * 
	 * @param dtp
	 */
	public void setDiagramTypeProvider(final IDiagramTypeProvider dtp) {

		this.dtp = dtp;
	}

	/**
	 * Creates the objects for the complex types, entities and the function
	 * imports
	 * 
	 * @param eObject
	 */
	private void visualizeObj(final EObject eObject) {

		AddContext addContext = null;
		if (eObject instanceof EntityType) {
			if (ArtifactUtil.isReferencedEntityType((EntityType) eObject)) {
				addContext = new AddReferencedEntityContext();
				((AddReferencedEntityContext) addContext).setImported(true);
			} else {
				addContext = new AddContext();
			}
		} else {
			addContext = new AddContext();
		}
		addContext.setTargetContainer(this.getDiagramTypeProvider()
				.getDiagram());
		addContext.setNewObject(eObject);
		this.getDiagramTypeProvider().getFeatureProvider()
				.addIfPossible(addContext);
	}

	/**
	 * Creates all the associations
	 * 
	 * @param association
	 */
	private void visualiseAssociation(Association association) {
		AssociationType associationType = null;
		Role dependent = null;
		Role principal = null;
		List<Role> roles = association.getEnds();
		/*
		 * The following code will get the Roles from the association The roles
		 * will give the source and target entities and the association type can
		 * be derived from them
		 */
		if (roles.size() >= 2) {
			associationType = AssociationType.NONE;
			Role role1 = roles.get(0);
			Role role2 = roles.get(1);
			// If, both roles are present
			if (role1 != null && role2 != null) {
				EntityType entity1 = role1.getType();
				EntityType entity2 = role2.getType();
				// Get entities and if, self association
				if (entity1 == entity2) {
					principal = role1;
					dependent = role2;
					associationType = getEntityAssociationType(association,
							associationType, entity1, true);

				} else {
					// Else, Assign association type
					associationType = AssociationType.NONE;
					associationType = getEntityAssociationType(association,
							associationType, entity1, false);
					switch (associationType) {
					case UNIDIR:
						dependent = role1;
						principal = role2;
						break;
					default:
						break;
					}
					associationType = getEntityAssociationType(association,
							associationType, entity2, false);
					switch (associationType) {
					case UNIDIR:
						dependent = role2;
						principal = role1;
						break;
					default:
						break;
					}

					if (dependent == null && principal == null) {
						principal = role1;
						dependent = role2;
					}

				}
			}
		}

		/*
		 * Once the roles are derived from the above code the
		 * multiplicity(cardinality) can be set for the association ends
		 */
		if (principal != null && dependent != null) {

			EntityType source = principal.getType();
			EntityType target = dependent.getType();
			Anchor sourceAnchor = getAnchor(getPictogramElement(source));
			Anchor targetAnchor = getAnchor(getPictogramElement(target));
			if (sourceAnchor == null || targetAnchor == null) {
				return;
			}
			final AssociationAddConnectionContext addContext = new AssociationAddConnectionContext(
					sourceAnchor, targetAnchor);
			addContext.setAssociationType(associationType);
			addContext.setSourceMultiplicity(AssociationUtil
					.getMultiplicityEnum(principal.getMultiplicity()));
			addContext.setTargetMultiplicity(AssociationUtil
					.getMultiplicityEnum(dependent.getMultiplicity()));
			addContext.setNewObject(association);
			addContext.setSourceRoleObject(principal);
			addContext.setTargetRoleObject(dependent);

			// The new association is added
			this.dtp.getFeatureProvider().addIfPossible(addContext);
		}
	}

	private AssociationType getEntityAssociationType(Association association,
			AssociationType associationType, EntityType entity1, boolean isSelf) {

		if (entity1 != null) {
			EList<NavigationProperty> navigationProperties = entity1
					.getNavigationProperties();
			for (NavigationProperty navigationProperty : navigationProperties) {
				if (navigationProperty.getRelationship() == association) {
					switch (associationType) {
					case NONE:
						associationType = AssociationType.UNIDIR;
						if (!isSelf) {
							return associationType;
						}
						break;
					case UNIDIR:
						associationType = AssociationType.BIDIR;
						return associationType;
					default:
						return associationType;
					}
				}
			}
		}

		return associationType;
	}

	private PictogramElement getPictogramElement(EObject selObj) {
		// Find the Pictogram Elements for the given domain
		// object via the standard link service
		final List<PictogramElement> referencingPes = Graphiti.getLinkService()
				.getPictogramElements(getDiagramTypeProvider().getDiagram(),
						selObj);
		// for selected object with the Top Container set,
		// add only the PictogramElement with the Top
		// Container.
		for (PictogramElement pe : referencingPes) {
			EObject businessObject = Graphiti.getLinkService()
					.getBusinessObjectForLinkedPictogramElement(pe);

			if (PropertyUtil.isTopContainer(pe) && businessObject == selObj) {
				return pe;
			}
		}
		return null;
	}

	private Anchor getAnchor(PictogramElement container) {
		if (container instanceof Anchor) {
			return (Anchor) container;
		} else if (container instanceof AnchorContainer) {
			return Graphiti.getPeService().getChopboxAnchor(
					(AnchorContainer) container);
		}
		return null;
	}

}
