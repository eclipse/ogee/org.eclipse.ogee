/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.visualizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.ogee.designer.utils.AssociationUtil;
import org.eclipse.ogee.designer.visualizer.ODataArtifactNode.EntityQuadrant;

/**
 * This is a layout helper class for the associations in the model file. It uses
 * X and Y calculations to bend the associations in certain ways so that they do
 * not overlap the entities or other objects
 * 
 */
public class ODataAssociationLayout {

	/*
	 * Offset for Y positions
	 */
	private static final int YOFFSET = 10;
	/*
	 * Offset for X positions
	 */
	private static final int XOFFSET = 12;
	private IFeatureProvider fp;

	/**
	 * Default Constructor.
	 */
	public ODataAssociationLayout(IFeatureProvider fp) {
		this.fp = fp;
	}

	/**
	 * Layouts the associations for the given entities The associations are
	 * drawn for incoming associations outgoing associations and the self
	 * associations respectively. The number of connections between the entities
	 * is taken into account for the associations
	 * 
	 * @param entityNodes
	 *            list of entities
	 */
	public void layoutAssociations(List<ODataArtifactNode> entityNodes) {
		if (entityNodes != null && entityNodes.size() > 0) {
			List<Connection> incomingAssociations = null;
			ODataArtifactNode targetNode = null;
			boolean isFirstConnection = false;

			updateEntityNodes(fp, entityNodes);
			UniqueEList<Connection> drawnConnections = new UniqueEList<Connection>();

			for (ODataArtifactNode oDataArtifactNode : entityNodes) {

				HashMap<EntityQuadrant, List<Connection>> entityQuadrantAssociations = oDataArtifactNode
						.getEntityQuadrantAssociations();

				Set<EntityQuadrant> keySet = entityQuadrantAssociations
						.keySet();
				for (EntityQuadrant entityQuadrant : keySet) {
					List<Connection> associationList = entityQuadrantAssociations
							.get(entityQuadrant);
					for (Connection connection : associationList) {

						if (!drawnConnections.contains(connection)) {
							((FreeFormConnection) connection).getBendpoints()
									.clear();
							AnchorContainer source = connection.getStart()
									.getParent();
							AnchorContainer target = connection.getEnd()
									.getParent();

							ODataArtifactNode sourceNode = getArtifactNode(
									(Shape) source, entityNodes);
							ODataArtifactNode targetArtifactNode = getArtifactNode(
									(Shape) target, entityNodes);
							// Check if connection is incoming/outgoing
							boolean isOutgoing = false;
							if (oDataArtifactNode == sourceNode) {
								targetNode = targetArtifactNode;
								isOutgoing = true;
							} else {
								targetNode = sourceNode;
								isOutgoing = false;
							}

							incomingAssociations = getTargetNodeConnections(
									targetNode, connection);
							// Related connections between the same entities
							List<Connection> relatedConnections = AssociationUtil
									.getRelatedConnections(
											oDataArtifactNode.getParentShape(),
											targetNode.getParentShape());
							if (relatedConnections != null
									&& relatedConnections.indexOf(connection) == 0) {
								isFirstConnection = true;
							} else {
								isFirstConnection = false;
							}

							// For outgoing associations
							if (isOutgoing) {

								addNewBendpoints(
										oDataArtifactNode,
										targetNode,
										((FreeFormConnection) connection),
										associationList.size(),
										incomingAssociations.size(),
										associationList.indexOf(connection) + 1,
										incomingAssociations
												.indexOf(connection) + 1,
										isFirstConnection);
							} else {
								// For incoming associations
								addNewBendpoints(
										targetNode,
										oDataArtifactNode,
										((FreeFormConnection) connection),
										incomingAssociations.size(),
										associationList.size(),
										incomingAssociations
												.indexOf(connection) + 1,
										associationList.indexOf(connection) + 1,
										isFirstConnection);
							}

							drawnConnections.add(connection);
						}
					}
				}
			}
		}
		// For self associations
		adaptSelfBendPoints(getSelfConnections(fp.getDiagramTypeProvider()
				.getDiagram()));
	}

	/**
	 * Gets associations related to the target node of the given connection
	 * 
	 * @param oDataArtifactNode
	 * @param connection
	 * @return
	 */
	private List<Connection> getTargetNodeConnections(
			ODataArtifactNode oDataArtifactNode, Connection connection) {

		HashMap<EntityQuadrant, List<Connection>> entityQuadrantAssociations = oDataArtifactNode
				.getEntityQuadrantAssociations();

		Set<EntityQuadrant> keySet = entityQuadrantAssociations.keySet();
		for (EntityQuadrant entityQuadrant : keySet) {
			List<Connection> associationList = entityQuadrantAssociations
					.get(entityQuadrant);
			if (associationList.contains(connection)) {
				return associationList;
			}
		}
		return new ArrayList<Connection>();
	}

	/**
	 * Updates the entities with quadrant descriptions
	 * 
	 * @param fp
	 * @param entityNodes
	 */
	private void updateEntityNodes(IFeatureProvider fp,
			List<ODataArtifactNode> entityNodes) {
		if (entityNodes != null && entityNodes.size() > 0) {
			ContainerShape entityShape = null;
			EList<Connection> incomingAssociations = null;
			ContainerShape targetShape = null;
			ODataArtifactNode targetNode = null;
			for (ODataArtifactNode oDataArtifactNode : entityNodes) {
				entityShape = oDataArtifactNode.getParentShape();
				EList<Connection> outgoingAssociations = (EList<Connection>) AssociationUtil
						.getOutgoingAssociations(entityShape);
				incomingAssociations = (EList<Connection>) AssociationUtil
						.getIncomingAssociations(entityShape);

				for (Connection connection : outgoingAssociations) {
					targetShape = (ContainerShape) connection.getEnd()
							.getParent();

					targetNode = getArtifactNode(targetShape, entityNodes);

					int sourceColNum = oDataArtifactNode.getColumnNumber();
					int sourceRowNum = oDataArtifactNode.getRowNumber();

					int targetColNum = targetNode.getColumnNumber();
					int targetRowNum = targetNode.getRowNumber();

					assignQuadrant(oDataArtifactNode, connection, sourceColNum,
							sourceRowNum, targetColNum, targetRowNum);

				}

				for (Connection connection : incomingAssociations) {
					ContainerShape sourceShape = (ContainerShape) connection
							.getStart().getParent();

					ODataArtifactNode sourceNode = getArtifactNode(sourceShape,
							entityNodes);

					int sourceColNum = sourceNode.getColumnNumber();
					int sourceRowNum = sourceNode.getRowNumber();

					int targetColNum = oDataArtifactNode.getColumnNumber();
					int targetRowNum = oDataArtifactNode.getRowNumber();

					assignQuadrant(oDataArtifactNode, connection, sourceColNum,
							sourceRowNum, targetColNum, targetRowNum);

				}

			}

		}
	}

	/**
	 * Assigns quadrants to which the association belongs
	 * 
	 * @param oDataArtifactNode
	 * @param connection
	 * @param sourceColNum
	 * @param sourceRowNum
	 * @param targetColNum
	 * @param targetRowNum
	 */
	public void assignQuadrant(ODataArtifactNode oDataArtifactNode,
			Connection connection, int sourceColNum, int sourceRowNum,
			int targetColNum, int targetRowNum) {
		if (sourceColNum == targetColNum) {
			if (sourceRowNum == targetRowNum) {
				// Nothing happens here. Self associations.
			} else if (sourceRowNum < targetRowNum) {
				oDataArtifactNode.addToList(EntityQuadrant.WS, connection);
			} else if (sourceRowNum > targetRowNum) {
				oDataArtifactNode.addToList(EntityQuadrant.WN, connection);
			}
		} else if (sourceColNum < targetColNum) {
			if (sourceRowNum == targetRowNum) {
				oDataArtifactNode.addToList(EntityQuadrant.NE, connection);
			} else if (sourceRowNum < targetRowNum) {
				if ((targetRowNum - sourceRowNum) > 1) {
					oDataArtifactNode.addToList(EntityQuadrant.ES, connection);
				} else if ((targetRowNum - sourceRowNum) == 1) {
					oDataArtifactNode.addToList(EntityQuadrant.SE, connection);
				}

			} else if (sourceRowNum > targetRowNum) {
				if ((sourceRowNum - targetRowNum) > 1) {
					oDataArtifactNode.addToList(EntityQuadrant.EN, connection);
				} else if ((sourceRowNum - targetRowNum) == 1) {
					oDataArtifactNode.addToList(EntityQuadrant.NE, connection);
				}
			}
		} else if (sourceColNum > targetColNum) {
			if (sourceRowNum == targetRowNum) {
				oDataArtifactNode.addToList(EntityQuadrant.NE, connection);
			} else if (sourceRowNum < targetRowNum) {
				if ((targetRowNum - sourceRowNum) > 1) {
					oDataArtifactNode.addToList(EntityQuadrant.WS, connection);
				} else if ((targetRowNum - sourceRowNum) == 1) {
					oDataArtifactNode.addToList(EntityQuadrant.SW, connection);
				}

			} else if (sourceRowNum > targetRowNum) {
				if ((sourceRowNum - targetRowNum) > 1) {
					oDataArtifactNode.addToList(EntityQuadrant.WN, connection);
				} else if ((sourceRowNum - targetRowNum) == 1) {
					oDataArtifactNode.addToList(EntityQuadrant.NW, connection);
				}

			}
		}
	}

	/**
	 * This method adds the x, y positions of bendpoints based on a given
	 * scenario(distance in rows & columns) between the source and target nodes
	 * 
	 * @param sourceNode
	 * @param targetNode
	 * @param connection
	 * @param outConnectionsSize
	 * @param inConnectionsSize
	 * @param connectionIndex
	 * @param incomingConnectionIndex
	 * @param isFirstConnection
	 */
	private void addNewBendpoints(ODataArtifactNode sourceNode,
			ODataArtifactNode targetNode, FreeFormConnection connection,
			int outConnectionsSize, int inConnectionsSize, int connectionIndex,
			int incomingConnectionIndex, boolean isFirstConnection) {

		int sourceColNum = sourceNode.getColumnNumber();
		int sourceRowNum = sourceNode.getRowNumber();
		int sourceX = sourceNode.getX();
		int sourceY = sourceNode.getY();
		int sourceHeight = sourceNode.getRowHeight();
		int sourceWidth = sourceNode.getWidth();

		int targetColNum = targetNode.getColumnNumber();
		int targetRowNum = targetNode.getRowNumber();
		int targetX = targetNode.getX();
		int targetY = targetNode.getY();
		int targetHeight = targetNode.getHeight();
		int targetWidth = targetNode.getWidth();

		float firstBendPointX = 0.0f;
		float firstBendPointY = 0.0f;
		float secondBendPointX = 0.0f;
		float secondBendPointY = 0.0f;
		float thirdBendPointX = 0.0f;
		float thirdBendPointY = 0.0f;

		// If the entities are in the same column
		if (sourceColNum == targetColNum) {

			if (sourceRowNum == targetRowNum) {
				// This should never happen(self association)
			} else if (sourceRowNum < targetRowNum) {
				/*
				 * --------------If the entities are in same column and the
				 * target entity is just above source entity-------------
				 */
				if ((targetRowNum - sourceRowNum) == 1 && isFirstConnection) {
					verticalAdjacentFirstConnectionBehavior();
				} else {
					Point p;
					int max = Math.max(outConnectionsSize, inConnectionsSize);
					firstBendPointX = (max * 12);
					firstBendPointX = (sourceX - (firstBendPointX - ((max - connectionIndex) * XOFFSET)));
					firstBendPointY = sourceY + (sourceHeight / 1.5f)
							+ ((max - connectionIndex) * 10);
					p = Graphiti.getGaService().createPoint(
							Math.round(firstBendPointX),
							Math.round(firstBendPointY));
					connection.getBendpoints().add(p);

					secondBendPointX = firstBendPointX;
					secondBendPointY = targetY + (targetHeight / 3f)
							- ((max - incomingConnectionIndex) * YOFFSET);
					p = Graphiti.getGaService().createPoint(
							Math.round(secondBendPointX),
							Math.round(secondBendPointY));
					connection.getBendpoints().add(p);
				}

			}/*
			 * --------------If the entities are in same---------------------
			 * column and the target entity is below source entity-------------
			 */
			else if (sourceRowNum > targetRowNum) {
				/*
				 * --------------If the entities are in same column and the
				 * target entity is just below source entity-------------
				 */
				if ((sourceRowNum - targetRowNum) == 1 && isFirstConnection) {
					verticalAdjacentFirstConnectionBehavior();
				} else {
					Point p;
					int max = Math.max(outConnectionsSize, inConnectionsSize);
					firstBendPointX = (max * XOFFSET);
					firstBendPointX = (sourceX - (firstBendPointX - ((max - incomingConnectionIndex) * XOFFSET)));
					firstBendPointY = sourceY + (sourceHeight / 3f)
							- ((max - incomingConnectionIndex) * YOFFSET);
					p = Graphiti.getGaService().createPoint(
							Math.round(firstBendPointX),
							Math.round(firstBendPointY));
					connection.getBendpoints().add(p);

					secondBendPointX = firstBendPointX;
					secondBendPointY = targetY + (targetHeight / 1.5f)
							+ ((max - incomingConnectionIndex) * YOFFSET);
					p = Graphiti.getGaService().createPoint(
							Math.round(secondBendPointX),
							Math.round(secondBendPointY));
					connection.getBendpoints().add(p);
				}
			}
		}// ****End :::::: If entities are in same column*****
			// If source entity coulmn is to the left of target entity coulmn
		else if (sourceColNum < targetColNum) {
			// ****Start :::::: Source row same as target row***
			if (sourceRowNum == targetRowNum) {
				Point p;
				int max = Math.max(outConnectionsSize, inConnectionsSize);
				firstBendPointY = (max * XOFFSET);

				firstBendPointY = (sourceY - (firstBendPointY - ((max - connectionIndex) * XOFFSET)));

				firstBendPointX = sourceX + (sourceWidth / 1.5f)
						+ ((max - connectionIndex) * YOFFSET);

				p = Graphiti.getGaService().createPoint(
						Math.round(firstBendPointX),
						Math.round(firstBendPointY));
				connection.getBendpoints().add(p);

				secondBendPointY = firstBendPointY;
				secondBendPointX = targetX + (targetWidth / 3f)
						- ((max - incomingConnectionIndex) * YOFFSET);
				p = Graphiti.getGaService().createPoint(
						Math.round(secondBendPointX),
						Math.round(secondBendPointY));
				connection.getBendpoints().add(p);
			}// ****End :::::: Source row same as target row***
				// ****Start :::::: Source row above target row***
			else if (sourceRowNum < targetRowNum) {
				// If the target row is more than one row below the source row
				if (targetRowNum - sourceRowNum > 1) {
					// If the target column is more than one column right of the
					// source column
					if (targetColNum - sourceColNum > 1) {

						Point p;
						int max = Math.max(outConnectionsSize,
								inConnectionsSize);
						firstBendPointX = sourceX + (sourceWidth / 1.5f)
								- ((max - connectionIndex) * YOFFSET);
						firstBendPointY = (max * XOFFSET);
						firstBendPointY = (sourceY + sourceHeight + (firstBendPointY - ((max - connectionIndex) * XOFFSET)));
						p = Graphiti.getGaService().createPoint(
								Math.round(firstBendPointX),
								Math.round(firstBendPointY));
						connection.getBendpoints().add(p);

						firstBendPointX = (max * XOFFSET);
						thirdBendPointX = targetX
								- (firstBendPointX - ((max - connectionIndex) * XOFFSET));
						thirdBendPointY = targetY + (targetHeight / 3f)
								- ((max - incomingConnectionIndex) * YOFFSET);

						secondBendPointX = thirdBendPointX;
						secondBendPointY = firstBendPointY;

						p = Graphiti.getGaService().createPoint(
								Math.round(secondBendPointX),
								Math.round(secondBendPointY));
						connection.getBendpoints().add(p);

						p = Graphiti.getGaService().createPoint(
								Math.round(thirdBendPointX),
								Math.round(thirdBendPointY));
						connection.getBendpoints().add(p);

					}
					// If the target column is one column right of the source
					// column
					else {

						Point p;
						int max = Math.max(outConnectionsSize,
								inConnectionsSize);
						firstBendPointX = (max * XOFFSET);
						firstBendPointX = (sourceX + sourceWidth + (firstBendPointX - ((max - connectionIndex) * XOFFSET)));
						firstBendPointY = sourceY + (sourceHeight / 1.5f)
								- ((max - connectionIndex) * YOFFSET);
						p = Graphiti.getGaService().createPoint(
								Math.round(firstBendPointX),
								Math.round(firstBendPointY));
						connection.getBendpoints().add(p);

						secondBendPointX = firstBendPointX;
						secondBendPointY = targetY + (targetHeight / 3f)
								+ ((max - incomingConnectionIndex) * YOFFSET);
						p = Graphiti.getGaService().createPoint(
								Math.round(secondBendPointX),
								Math.round(secondBendPointY));
						connection.getBendpoints().add(p);
					}
				}
				// If the target row is one row below the source row
				else {
					Point p;
					int max = Math.max(outConnectionsSize, inConnectionsSize);
					firstBendPointX = sourceX + (sourceWidth / 1.5f)
							+ ((max - connectionIndex) * YOFFSET);
					firstBendPointY = (max * XOFFSET);
					firstBendPointY = (sourceY + sourceHeight + (firstBendPointY - ((max - connectionIndex) * XOFFSET)));
					p = Graphiti.getGaService().createPoint(
							Math.round(firstBendPointX),
							Math.round(firstBendPointY));
					connection.getBendpoints().add(p);

					secondBendPointX = targetX + (targetWidth / 3f)
							+ ((max - incomingConnectionIndex) * YOFFSET);
					secondBendPointY = firstBendPointY;
					p = Graphiti.getGaService().createPoint(
							Math.round(secondBendPointX),
							Math.round(secondBendPointY));
					connection.getBendpoints().add(p);
				}
			} // ****End :::::: Source row above target row***
				// ****Start :::::: Source row below target row***
			else if (sourceRowNum > targetRowNum) {
				// If the target row is more than one row above the source row
				if (sourceRowNum - targetRowNum > 1) {
					// If the target column is more than one column to the left
					// of the source column
					if (targetColNum - sourceColNum > 1) {
						Point p;
						int max = Math.max(outConnectionsSize,
								inConnectionsSize);
						firstBendPointX = sourceX + (sourceWidth / 1.5f)
								- ((max - connectionIndex) * 12);
						firstBendPointY = (max * 20);
						firstBendPointY = (sourceY - (firstBendPointY - ((max - connectionIndex) * 20)));
						p = Graphiti.getGaService().createPoint(
								Math.round(firstBendPointX),
								Math.round(firstBendPointY));
						connection.getBendpoints().add(p);

						firstBendPointX = (max * 20);
						thirdBendPointX = targetX
								- (firstBendPointX - ((max - connectionIndex) * 20));
						thirdBendPointY = targetY + (targetHeight / 1.5f)
								- ((max - incomingConnectionIndex) * 12);

						secondBendPointX = thirdBendPointX;
						secondBendPointY = firstBendPointY;

						p = Graphiti.getGaService().createPoint(
								Math.round(secondBendPointX),
								Math.round(secondBendPointY));
						connection.getBendpoints().add(p);

						p = Graphiti.getGaService().createPoint(
								Math.round(thirdBendPointX),
								Math.round(thirdBendPointY));
						connection.getBendpoints().add(p);
					}
					// If the target column is one column to the left of the
					// source column
					else {
						Point p;
						int max = Math.max(outConnectionsSize,
								inConnectionsSize);
						firstBendPointX = (max * 20);
						firstBendPointX = (sourceX + sourceWidth + (firstBendPointX - ((max - connectionIndex) * 20)));
						firstBendPointY = sourceY + (sourceHeight / 3f)
								- ((max - connectionIndex) * 12);
						p = Graphiti.getGaService().createPoint(
								Math.round(firstBendPointX),
								Math.round(firstBendPointY));
						connection.getBendpoints().add(p);

						secondBendPointX = firstBendPointX;
						secondBendPointY = targetY + (targetHeight / 1.5f)
								+ ((max - incomingConnectionIndex) * 12);
						p = Graphiti.getGaService().createPoint(
								Math.round(secondBendPointX),
								Math.round(secondBendPointY));
						connection.getBendpoints().add(p);
					}
				}
				// If the target row is one row above the source row
				else {

					Point p;
					int max = Math.max(outConnectionsSize, inConnectionsSize);
					firstBendPointX = sourceX + (sourceWidth / 1.5f)
							+ ((max - connectionIndex) * YOFFSET);
					firstBendPointY = (max * XOFFSET);
					firstBendPointY = (sourceY - (firstBendPointY - ((max - connectionIndex) * XOFFSET)));
					p = Graphiti.getGaService().createPoint(
							Math.round(firstBendPointX),
							Math.round(firstBendPointY));
					connection.getBendpoints().add(p);

					secondBendPointX = targetX + (targetWidth / 3f)
							+ ((max - incomingConnectionIndex) * YOFFSET);
					secondBendPointY = firstBendPointY;
					p = Graphiti.getGaService().createPoint(
							Math.round(secondBendPointX),
							Math.round(secondBendPointY));
					connection.getBendpoints().add(p);
				}

			}// ****End :::::: Source row below target row***

		}// ****End :::::: Source column one column away from target***

		// ****Start :::::: If source column after the target column
		else if (sourceColNum > targetColNum) {

			// ****Start :::::: Source row same as target row
			if (sourceRowNum == targetRowNum) {
				Point p;
				int max = Math.max(outConnectionsSize, inConnectionsSize);
				firstBendPointX = sourceX + (sourceWidth / 3f)
						- ((max - connectionIndex) * YOFFSET);
				firstBendPointY = (max * XOFFSET);
				firstBendPointY = (sourceY - (firstBendPointY - ((max - connectionIndex) * XOFFSET)));
				p = Graphiti.getGaService().createPoint(
						Math.round(firstBendPointX),
						Math.round(firstBendPointY));
				connection.getBendpoints().add(p);

				secondBendPointX = targetX + (targetWidth / 1.5f)
						+ ((max - incomingConnectionIndex) * YOFFSET);
				secondBendPointY = firstBendPointY;
				p = Graphiti.getGaService().createPoint(
						Math.round(secondBendPointX),
						Math.round(secondBendPointY));
				connection.getBendpoints().add(p);
			}// ****End :::::: Source row above target row
				// ****Start :::::: Source row below target row
			else if (sourceRowNum < targetRowNum) {
				if (targetRowNum - sourceRowNum > 1) {
					if (sourceColNum - targetColNum > 1) {

						Point p;
						int max = Math.max(outConnectionsSize,
								inConnectionsSize);
						firstBendPointX = sourceX + (sourceWidth / 3f)
								- ((max - connectionIndex) * 12);
						firstBendPointY = (max * 20);
						firstBendPointY = (sourceY + sourceHeight + (firstBendPointY - ((max - connectionIndex) * 20)));
						p = Graphiti.getGaService().createPoint(
								Math.round(firstBendPointX),
								Math.round(firstBendPointY));
						connection.getBendpoints().add(p);

						firstBendPointX = (max * 20);
						thirdBendPointX = targetX
								+ targetWidth
								+ (firstBendPointX - ((max - connectionIndex) * 20));
						thirdBendPointY = targetY + (targetHeight / 3f)
								- ((max - incomingConnectionIndex) * 12);

						secondBendPointX = thirdBendPointX;
						secondBendPointY = firstBendPointY;

						p = Graphiti.getGaService().createPoint(
								Math.round(secondBendPointX),
								Math.round(secondBendPointY));
						connection.getBendpoints().add(p);

						p = Graphiti.getGaService().createPoint(
								Math.round(thirdBendPointX),
								Math.round(thirdBendPointY));
						connection.getBendpoints().add(p);

					} else {
						Point p;
						int max = Math.max(outConnectionsSize,
								inConnectionsSize);
						firstBendPointX = (max * 20);
						firstBendPointX = (sourceX - (firstBendPointX - ((max - connectionIndex) * 20)));
						firstBendPointY = sourceY + (sourceHeight / 1.5f)
								- ((max - connectionIndex) * 12);
						p = Graphiti.getGaService().createPoint(
								Math.round(firstBendPointX),
								Math.round(firstBendPointY));
						connection.getBendpoints().add(p);

						secondBendPointX = firstBendPointX;
						secondBendPointY = targetY + (targetHeight / 3f)
								+ ((max - incomingConnectionIndex) * 12);
						p = Graphiti.getGaService().createPoint(
								Math.round(secondBendPointX),
								Math.round(secondBendPointY));
						connection.getBendpoints().add(p);
					}
				} else {

					// If the target is one row above
					// if ((targetRowNum - sourceRowNum) == 1) {
					Point p;
					int max = Math.max(outConnectionsSize, inConnectionsSize);
					firstBendPointX = sourceX + (sourceWidth / 3f)
							- ((max - connectionIndex) * 12);
					firstBendPointY = (max * 20);
					firstBendPointY = (sourceY + sourceHeight + (firstBendPointY - ((max - connectionIndex) * 20)));
					p = Graphiti.getGaService().createPoint(
							Math.round(firstBendPointX),
							Math.round(firstBendPointY));
					connection.getBendpoints().add(p);

					secondBendPointX = targetX + (targetWidth / 1.5f)
							+ ((max - incomingConnectionIndex) * 12);
					secondBendPointY = firstBendPointY;
					p = Graphiti.getGaService().createPoint(
							Math.round(secondBendPointX),
							Math.round(secondBendPointY));
					connection.getBendpoints().add(p);

				}

			}// ****End :::::: Source row above target row
				// ****Start :::::: Source row below target row
			else if (sourceRowNum > targetRowNum) {
				// If the target row is more than one row above the source row
				if (sourceRowNum - targetRowNum > 1) {
					if (sourceColNum - targetColNum > 1) {
						Point p;
						int max = Math.max(outConnectionsSize,
								inConnectionsSize);
						firstBendPointX = sourceX + (sourceWidth / 3f)
								- ((max - connectionIndex) * 12);
						firstBendPointY = (max * 20);
						firstBendPointY = (sourceY - (firstBendPointY - ((max - connectionIndex) * 20)));
						p = Graphiti.getGaService().createPoint(
								Math.round(firstBendPointX),
								Math.round(firstBendPointY));
						connection.getBendpoints().add(p);

						firstBendPointX = (max * 20);
						thirdBendPointX = targetX
								+ targetWidth
								+ (firstBendPointX - ((max - connectionIndex) * 20));
						thirdBendPointY = targetY + (targetHeight / 1.5f)
								- ((max - incomingConnectionIndex) * 12);

						secondBendPointX = thirdBendPointX;
						secondBendPointY = firstBendPointY;

						p = Graphiti.getGaService().createPoint(
								Math.round(secondBendPointX),
								Math.round(secondBendPointY));
						connection.getBendpoints().add(p);

						p = Graphiti.getGaService().createPoint(
								Math.round(thirdBendPointX),
								Math.round(thirdBendPointY));
						connection.getBendpoints().add(p);

					} else {

						Point p;
						int max = Math.max(outConnectionsSize,
								inConnectionsSize);
						firstBendPointX = (max * 20);
						firstBendPointX = (sourceX - (firstBendPointX - ((max - connectionIndex) * 20)));
						firstBendPointY = sourceY + (sourceHeight / 3f)
								- ((max - connectionIndex) * 12);
						p = Graphiti.getGaService().createPoint(
								Math.round(firstBendPointX),
								Math.round(firstBendPointY));
						connection.getBendpoints().add(p);

						secondBendPointX = firstBendPointX;
						secondBendPointY = targetY + (targetHeight / 1.5f)
								+ ((max - incomingConnectionIndex) * 12);
						p = Graphiti.getGaService().createPoint(
								Math.round(secondBendPointX),
								Math.round(secondBendPointY));
						connection.getBendpoints().add(p);
					}
				}
				// If the target row is one row above the source row
				else {

					Point p;
					int max = Math.max(outConnectionsSize, inConnectionsSize);
					firstBendPointX = sourceX + (sourceWidth / 3f)
							- ((max - connectionIndex) * 12);
					firstBendPointY = (max * 20);
					firstBendPointY = (sourceY - (firstBendPointY - ((max - connectionIndex) * 20)));
					p = Graphiti.getGaService().createPoint(
							Math.round(firstBendPointX),
							Math.round(firstBendPointY));
					connection.getBendpoints().add(p);

					secondBendPointX = targetX + (targetWidth / 1.5f)
							+ ((max - incomingConnectionIndex) * 12);
					secondBendPointY = firstBendPointY;
					p = Graphiti.getGaService().createPoint(
							Math.round(secondBendPointX),
							Math.round(secondBendPointY));
					connection.getBendpoints().add(p);
				}

			}// ****End :::::: Source row below target row

		}

	}

	private ODataArtifactNode getArtifactNode(Shape shape,
			List<ODataArtifactNode> nodes) {
		for (ODataArtifactNode oDataArtifactNode : nodes) {
			if (oDataArtifactNode.getParentShape() == shape) {
				return oDataArtifactNode;
			}
		}
		return null;
	}

	/**
	 * Returns all the self associations in the model
	 * 
	 * @return Map<Connection, Point>
	 */
	private Map<Connection, Point> getSelfConnections(Diagram diagram) {
		IGaService gaService = Graphiti.getGaService();
		Map<Connection, Point> selfAssociations = new HashMap<Connection, Point>();
		EList<Connection> connections = diagram.getConnections();
		// All associations for which the target and source are same
		// are self associations and returned
		AnchorContainer source;
		AnchorContainer target;
		GraphicsAlgorithm p;
		Point start;
		for (Connection connection : connections) {
			if (connection.getParent() != null && connection.getEnd() != null) {
				source = connection.getStart().getParent();
				target = connection.getEnd().getParent();
				if (source == target) {
					p = source.getGraphicsAlgorithm();
					start = gaService.createPoint(p.getX(), p.getY());
					selfAssociations.put(connection, start);
				}
			}
		}
		return selfAssociations;
	}

	/**
	 * Re layout the self associations
	 * {@link org.eclipse.graphiti.mm.pictograms.Anchor} location to the new
	 * location
	 * 
	 * @param selfAssociations
	 *            The {@link Map} of initial
	 *            {@link org.eclipse.graphiti.mm.pictograms.Anchor} location
	 *            {@link Point} per {@link Connection}s
	 */
	private void adaptSelfBendPoints(Map<Connection, Point> selfAssociations) {
		for (Connection connection : selfAssociations.keySet()) {
			((FreeFormConnection) connection).getBendpoints().clear();
			List<Connection> selfAssociations2 = AssociationUtil
					.getSelfAssociations(((ContainerShape) connection
							.getStart().getParent()));
			AssociationUtil.drawSelfAssociation(connection,
					selfAssociations2.indexOf(connection));
		}
	}

	private void verticalAdjacentFirstConnectionBehavior() {
		// For the first connection straight line between entities
		// Therefore no implementation necessary. Default
		// implementation should automatically work.
	}

}
