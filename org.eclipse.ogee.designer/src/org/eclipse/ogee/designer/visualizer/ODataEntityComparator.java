/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.visualizer;

import java.util.Comparator;

/**
 * This is a comparator class used to sort the Entities based on the sortingField
 * and sortingType
 * 
 * 
 */
public class ODataEntityComparator implements Comparator<ODataArtifactNode> {

	public enum SortingType {
		ASCENDING,

		DESCENDING, ;

	}

	public enum SortingField {
		ASSOCCOUNT,

		ROWNUMBER,

		ROWHEIGHT, ;

	}

	private SortingType sortingType;
	private SortingField sortingField;

	/**
	 * Default Constructor
	 */
	public ODataEntityComparator(SortingType type, SortingField field) {
		super();
		this.sortingType = type;
		this.sortingField = field;
	}

	@Override
	public int compare(ODataArtifactNode node0, ODataArtifactNode node1) {
		int status = 0;
		switch (sortingField) {
		// Sort the entities based on the association count and the sorting
		// type(Ascending/Descending)
		case ASSOCCOUNT:
			if (SortingType.ASCENDING.equals(sortingType)) {
				status = node0.getAssocCount() - node1.getAssocCount();
				if (status == 0) {
					status = node0.getParentShape().getGraphicsAlgorithm()
							.getHeight()
							- node1.getParentShape().getGraphicsAlgorithm()
									.getHeight();
				}
			} else {
				status = node1.getAssocCount() - node0.getAssocCount();
			}
			break;
		// Sort the entities based on the row number ascending
		case ROWNUMBER:
			status = node0.getRowNumber() - node1.getRowNumber();
			if (status == 0) {
				status = node0.getColumnNumber() - node1.getColumnNumber();
			}
			break;
		// Sort the entities based on the row height ascending
		case ROWHEIGHT:
			status = node0.getParentShape().getGraphicsAlgorithm().getHeight()
					- node1.getParentShape().getGraphicsAlgorithm().getHeight();
			break;
		default:
			break;
		}
		return status;
	}
}
