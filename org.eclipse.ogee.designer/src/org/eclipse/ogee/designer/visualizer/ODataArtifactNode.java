/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.visualizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Shape;

/**
 * This is a wrapper over the domain objects which need to be displayed in the
 * editor once a file has been imported or layout has been called on an existing
 * model. This helps to associate some variables needed for layout with the
 * domain objects
 * 
 */
public class ODataArtifactNode {

	private int columnNumber = 0;
	private int rowNumber = 0;
	private ContainerShape parentShape = null;
	private int assocCount;

	private int selfAssocCount;

	/**
	 * Return the associations associated to the entity as a count Self
	 * associations are not counted More than one association is counted as one
	 * 
	 * @return int association count of this entity
	 */
	public int getAssocCount() {
		return assocCount;
	}

	/**
	 * Set the association count of this entity Self associations are not
	 * counted More than one association is counted as one
	 * 
	 * @param assocCount
	 *            int association count
	 */
	public void setAssocCount(int assocCount) {
		this.assocCount = assocCount;
	}

	/**
	 * Return the self associations associated to the entity as a count
	 * 
	 * @return int self association count of this entity
	 */
	public int getSelfAssocCount() {
		return selfAssocCount;
	}

	/**
	 * Set the self association count of this entity
	 * 
	 * @param assocCount
	 *            int self association count
	 */
	public void setSelfAssocCount(int selfAssocCount) {
		this.selfAssocCount = selfAssocCount;
	}

	/**
	 * Max height of the 4 entities in the row
	 * 
	 * @return int max of entity heights in a row
	 */
	public int getRowHeight() {
		return rowHeight;
	}

	/**
	 * Max height of the 4 entities in the row
	 * 
	 * @param int max of entity heights in a row
	 */
	public void setRowHeight(int rowHeight) {
		this.rowHeight = rowHeight;
	}

	/**
	 * Returns a list of entities to which this entity has an association
	 * 
	 * @return List<Shape> Associated Entities
	 */
	public List<Shape> getAssociatedEntities() {
		return associatedEntities;
	}

	/**
	 * Set list of entities to which this entity has an association
	 * 
	 * @param List
	 *            <Shape> Associated Entities
	 */
	public void setAssociatedEntities(List<Shape> associatedEntities) {
		this.associatedEntities = associatedEntities;
	}

	private int rowHeight;
	List<Shape> associatedEntities = new ArrayList<Shape>();

	public enum EntityQuadrant {
		NE, EN, ES, SE, SW, WS, WN, NW;
	}

	private HashMap<EntityQuadrant, List<Connection>> entityQuadrantAssociations = new HashMap<EntityQuadrant, List<Connection>>();

	/**
	 * @return the entityQuadrantAssociations
	 */
	public HashMap<EntityQuadrant, List<Connection>> getEntityQuadrantAssociations() {
		return entityQuadrantAssociations;
	}

	/**
	 * Return X position
	 * 
	 * @return the x position
	 */
	public int getX() {
		return this.parentShape.getGraphicsAlgorithm().getX();
	}

	/**
	 * Set X position
	 * 
	 * @param x
	 *            the x to set
	 */
	public void setX(int x) {
	}

	/**
	 * Return Y position
	 * 
	 * @return the y position
	 */
	public int getY() {
		return this.parentShape.getGraphicsAlgorithm().getY();
	}

	/**
	 * Set Y position
	 * 
	 * @param y
	 *            the y to set
	 */
	public void setY(int y) {
	}

	/**
	 * Get Height
	 * 
	 * @return the height
	 */
	public int getHeight() {
		return this.parentShape.getGraphicsAlgorithm().getHeight();
	}

	/**
	 * Set Height
	 * 
	 * @param height
	 *            the height to set
	 */
	public void setHeight(int height) {
	}

	/**
	 * Get Width
	 * 
	 * @return the width
	 */
	public int getWidth() {
		return this.parentShape.getGraphicsAlgorithm().getWidth();
	}

	/**
	 * Set Width
	 * 
	 * @param width
	 *            the width to set
	 */
	public void setWidth(int width) {
	}

	/**
	 * Get Column to which the object is assigned
	 * 
	 * @return the columnNumber
	 */
	public int getColumnNumber() {
		return columnNumber;
	}

	/**
	 * Set Column to which the object is assigned
	 * 
	 * @param columnNumber
	 *            the columnNumber to set
	 */
	public void setColumnNumber(int columnNumber) {
		this.columnNumber = columnNumber;
	}

	/**
	 * Get Row to which the object is assigned
	 * 
	 * @return the rowNumber
	 */
	public int getRowNumber() {
		return rowNumber;
	}

	/**
	 * Set Row to which the object is assigned
	 * 
	 * @param rowNumber
	 *            the rowNumber to set
	 */
	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	/**
	 * Get parent Shape of the object
	 * 
	 * @return the parentShape
	 */
	public ContainerShape getParentShape() {
		return parentShape;
	}

	/**
	 * Set parent Shape of the object
	 * 
	 * @param parentShape
	 *            the parentShape to set
	 */
	public void setParentShape(ContainerShape parentShape) {
		this.parentShape = parentShape;
	}

	/**
	 * Constructor
	 * 
	 * @param parentShape
	 */
	public ODataArtifactNode(ContainerShape parentShape) {
		this.parentShape = parentShape;

	}

	/**
	 * Add the association to one of the quadrant lists
	 * 
	 * @param entityQuadrant
	 * @param connection
	 */
	public void addToList(EntityQuadrant entityQuadrant, Connection connection) {

		List<Connection> list = entityQuadrantAssociations.get(entityQuadrant);
		if (list == null) {
			list = new ArrayList<Connection>();
			entityQuadrantAssociations.put(entityQuadrant, list);
		}
		list.add(connection);
	}
}
