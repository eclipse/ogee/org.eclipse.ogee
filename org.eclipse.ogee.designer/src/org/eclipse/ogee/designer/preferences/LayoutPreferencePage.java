/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.ODataLayoutUtil;
import org.eclipse.ogee.help.IHelpConstants;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;


/**
 * This class represents a preference page that is contributed to the
 * Preferences dialog for Layout Options of OData Editor.
 */
public class LayoutPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	private RadioGroupFieldEditor grpFieldEditor;
	/**
	 * Constructor and also Setting the description for the Preference Page
	 */
	public LayoutPreferencePage() {

		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription(Messages.ODATAEDITOR_LAYOUT_PREFERENCE_DESC+"\n\n");	  //$NON-NLS-1$	
	}

	/**
	 * Creates the field editors for different Layout Options.
	 */
	@Override
	public void createFieldEditors() {
		
		this.grpFieldEditor = new RadioGroupFieldEditor(IPreferenceConstants.LAYOUT_CHOICE,
				Messages.ODATAEDITOR_LAYOUT_PREFERENCE_TEXT, 1,
				ODataLayoutUtil.getLayoutOptions(), getFieldEditorParent());
		addField(this.grpFieldEditor);
		
	}

	@Override
	public void init(IWorkbench workbench) {
		//Nothing to implement
	}
	
	@Override
	public void performHelp() {

		PlatformUI
				.getWorkbench()
				.getHelpSystem()
				.setHelp(getControl(),IHelpConstants.HELP_CONTEXT_ID_PREFERENCES);
						//IODataEditorConstants.HELP_CONTEXT_ID_PREFERENCES);
						
		super.performHelp();
	}


}