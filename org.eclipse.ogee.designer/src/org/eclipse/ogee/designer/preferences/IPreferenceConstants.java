/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.preferences;

import org.eclipse.ogee.designer.messages.Messages;


/**
 * Constant definitions for plug-in preferences
 * 
 */
public interface IPreferenceConstants {

	/**
	 * String constant to store the current layout 
	 */
	public static final String LAYOUT_CHOICE = Messages.ODATAEDITOR_TEXT_LAYOUTCHOICE;

}
