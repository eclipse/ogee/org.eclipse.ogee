/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.mm.algorithms.styles.AdaptedGradientColoredAreas;
import org.eclipse.graphiti.mm.algorithms.styles.GradientColoredArea;
import org.eclipse.graphiti.mm.algorithms.styles.GradientColoredAreas;
import org.eclipse.graphiti.mm.algorithms.styles.LocationType;
import org.eclipse.graphiti.mm.algorithms.styles.StylesFactory;
import org.eclipse.graphiti.mm.algorithms.styles.StylesPackage;
import org.eclipse.graphiti.util.ColorUtil;
import org.eclipse.graphiti.util.IGradientType;
import org.eclipse.graphiti.util.IPredefinedRenderingStyle;

/**
 * Gradients are defined here which shall be used for background colors of
 * domain model objects
 */
public class ODataPredefinedStyle implements IPredefinedRenderingStyle {

	/**
	 * The color-areas, which are used for default elements with the ID
	 */
	private static GradientColoredAreas getBlueWhiteGlossDefaultAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		final List<GradientColoredArea> gcas = gradientColoredAreas
				.getGradientColor();

		addGradientColoredArea(
				gcas,
				"F8FBFE", 0, LocationType.LOCATION_TYPE_ABSOLUTE_START, "F8FBFE", 1, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);
		addGradientColoredArea(
				gcas,
				"EDF5FC", 1, LocationType.LOCATION_TYPE_ABSOLUTE_START, "EDF5FC", 2, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);
		addGradientColoredArea(
				gcas,
				"DEEDFA", 2, LocationType.LOCATION_TYPE_ABSOLUTE_START, "DEEDFA", 3, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);
		addGradientColoredArea(
				gcas,
				"D4E7F8", 3, LocationType.LOCATION_TYPE_ABSOLUTE_START, "FAFBFC", 2, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		addGradientColoredArea(
				gcas,
				"E2E5E9", 2, LocationType.LOCATION_TYPE_ABSOLUTE_END, "E2E5E9", 0, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_DEFAULT);
		return gradientColoredAreas;
	}

	/**
	 * The color-areas, which are used for primary selected elements with the ID
	 */
	private static GradientColoredAreas getBlueWhiteGlossPrimarySelectedAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_PRIMARY_SELECTED);
		final List<GradientColoredArea> gcas = gradientColoredAreas
				.getGradientColor();

		addGradientColoredArea(
				gcas,
				"EEF6FD", 0, LocationType.LOCATION_TYPE_ABSOLUTE_START, "EEF6FD", 1, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);
		addGradientColoredArea(
				gcas,
				"D0E6F9", 1, LocationType.LOCATION_TYPE_ABSOLUTE_START, "D0E6F9", 2, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);
		addGradientColoredArea(
				gcas,
				"ACD2F4", 2, LocationType.LOCATION_TYPE_ABSOLUTE_START, "ACD2F4", 3, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);
		addGradientColoredArea(
				gcas,
				"81B9EA", 3, LocationType.LOCATION_TYPE_ABSOLUTE_START, "AAD0F2", 2, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		addGradientColoredArea(
				gcas,
				"9ABFE0", 2, LocationType.LOCATION_TYPE_ABSOLUTE_END, "9ABFE0", 0, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		return gradientColoredAreas;
	}

	/**
	 * The color-areas, which are used for secondary selected elements with the
	 */
	private static GradientColoredAreas getBlueWhiteGlossSecondarySelectedAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_SECONDARY_SELECTED);
		final List<GradientColoredArea> gcas = gradientColoredAreas
				.getGradientColor();

		addGradientColoredArea(
				gcas,
				"F5F9FE", 0, LocationType.LOCATION_TYPE_ABSOLUTE_START, "F5F9FE", 1, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);
		addGradientColoredArea(
				gcas,
				"E2EFFC", 1, LocationType.LOCATION_TYPE_ABSOLUTE_START, "E2EFFC", 2, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);
		addGradientColoredArea(
				gcas,
				"CBE3F9", 2, LocationType.LOCATION_TYPE_ABSOLUTE_START, "CBE3F9", 3, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);
		addGradientColoredArea(
				gcas,
				"BBDAF7", 3, LocationType.LOCATION_TYPE_ABSOLUTE_START, "C5E0F7", 2, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		addGradientColoredArea(
				gcas,
				"B2CDE5", 2, LocationType.LOCATION_TYPE_ABSOLUTE_END, "B2CDE5", 0, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		return gradientColoredAreas;
	}

	/**
	 * The color-areas, which are used for elements where an action is allowed
	 * with the ID
	 */
	private static GradientColoredAreas getBlueWhiteGlossActionAllowedAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_ACTION_ALLOWED);
		final List<GradientColoredArea> gcas = gradientColoredAreas
				.getGradientColor();

		addGradientColoredArea(
				gcas,
				"99CC00", 0, LocationType.LOCATION_TYPE_ABSOLUTE_START, "339966", 0, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		return gradientColoredAreas;
	}

	/**
	 * The color-areas, which are used for elements where an action is forbidden
	 * with the ID {@link #BLUE_WHITE_GLOSS_ID}.
	 */
	private static GradientColoredAreas getBlueWhiteGlossActionForbiddenAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_ACTION_FORBIDDEN);
		final List<GradientColoredArea> gcas = gradientColoredAreas
				.getGradientColor();

		addGradientColoredArea(
				gcas,
				"FFCC00", 0, LocationType.LOCATION_TYPE_ABSOLUTE_START, "FF6600", 0, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		return gradientColoredAreas;
	}

	public static AdaptedGradientColoredAreas getBlueWhiteGlossAdaptions() {
		final AdaptedGradientColoredAreas agca = StylesFactory.eINSTANCE
				.createAdaptedGradientColoredAreas();
		agca.setDefinedStyleId(BLUE_WHITE_GLOSS_ID);
		agca.setGradientType(IGradientType.VERTICAL);
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_DEFAULT,
				getBlueWhiteGlossDefaultAreas());
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_PRIMARY_SELECTED,
				getBlueWhiteGlossPrimarySelectedAreas());
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_SECONDARY_SELECTED,
				getBlueWhiteGlossSecondarySelectedAreas());
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_ACTION_ALLOWED,
				getBlueWhiteGlossActionAllowedAreas());
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_ACTION_FORBIDDEN,
				getBlueWhiteGlossActionForbiddenAreas());
		return agca;
	}

	public static AdaptedGradientColoredAreas getAdaptedGradientColoredAreas(
			String id) {
		if (BLUE_WHITE_GLOSS_ID.equals(id)) {
			return getBlueWhiteGlossAdaptions();
		} else {
			return null;
		}

	}

	protected static void addGradientColoredArea(
			List<GradientColoredArea> gcas, String colorStart,
			int locationValueStart, LocationType locationTypeStart,
			String colorEnd, int locationValueEnd, LocationType locationTypeEnd) {

		final GradientColoredArea gca = StylesFactory.eINSTANCE
				.createGradientColoredArea();
		gcas.add(gca);
		gca.setStart(StylesFactory.eINSTANCE.createGradientColoredLocation());
		gca.getStart().setColor(StylesFactory.eINSTANCE.createColor());
		gca.getStart()
				.getColor()
				.eSet(StylesPackage.eINSTANCE.getColor_Blue(),
						ColorUtil.getBlueFromHex(colorStart));
		gca.getStart()
				.getColor()
				.eSet(StylesPackage.eINSTANCE.getColor_Green(),
						ColorUtil.getGreenFromHex(colorStart));
		gca.getStart()
				.getColor()
				.eSet(StylesPackage.eINSTANCE.getColor_Red(),
						ColorUtil.getRedFromHex(colorStart));
		gca.getStart().setLocationType(locationTypeStart);
		gca.getStart().setLocationValue(locationValueStart);
		gca.setEnd(StylesFactory.eINSTANCE.createGradientColoredLocation());
		gca.getEnd().setColor(StylesFactory.eINSTANCE.createColor());
		gca.getEnd()
				.getColor()
				.eSet(StylesPackage.eINSTANCE.getColor_Blue(),
						ColorUtil.getBlueFromHex(colorEnd));
		gca.getEnd()
				.getColor()
				.eSet(StylesPackage.eINSTANCE.getColor_Green(),
						ColorUtil.getGreenFromHex(colorEnd));
		gca.getEnd()
				.getColor()
				.eSet(StylesPackage.eINSTANCE.getColor_Red(),
						ColorUtil.getRedFromHex(colorEnd));
		gca.getEnd().setLocationType(locationTypeEnd);
		gca.getEnd().setLocationValue(locationValueEnd);
	}

	public static AdaptedGradientColoredAreas getSelectedAdaptions() {
		final AdaptedGradientColoredAreas agca = StylesFactory.eINSTANCE
				.createAdaptedGradientColoredAreas();
		agca.setDefinedStyleId("Selected_ID"); //$NON-NLS-1$
		agca.setGradientType(IGradientType.VERTICAL);
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_DEFAULT,
				getSelectedDefaultAreas());
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_PRIMARY_SELECTED,
				getPrimarySelectedDefaultAreas());
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_SECONDARY_SELECTED,
				getSecondarySelectedDefaultAreas());
		return agca;
	}

	/**
	 * The color-areas, which are used for default elements with the ID
	 */
	private static GradientColoredAreas getSelectedDefaultAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_DEFAULT);
		return gradientColoredAreas;
	}

	/**
	 * The color-areas, which are used for primary selected elements with the ID
	 */
	private static GradientColoredAreas getPrimarySelectedDefaultAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_PRIMARY_SELECTED);
		final List<GradientColoredArea> gcas = gradientColoredAreas
				.getGradientColor();

		addGradientColoredArea(
				gcas,
				"000000", 0, LocationType.LOCATION_TYPE_ABSOLUTE_START, "EEEEED", 1, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);

		addGradientColoredArea(
				gcas,
				"81B9EA", 1, LocationType.LOCATION_TYPE_ABSOLUTE_START, "81B9EA", 1, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);

		addGradientColoredArea(
				gcas,
				"EEEEED", 1, LocationType.LOCATION_TYPE_ABSOLUTE_END, "000000", 0, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);

		return gradientColoredAreas;
	}

	/**
	 * The color-areas, which are used for primary selected elements with the ID
	 */
	private static GradientColoredAreas getSecondarySelectedDefaultAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_PRIMARY_SELECTED);
		final List<GradientColoredArea> gcas = gradientColoredAreas
				.getGradientColor();

		addGradientColoredArea(
				gcas,
				"000000", 0, LocationType.LOCATION_TYPE_ABSOLUTE_START, "EEEEED", 1, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_START);

		addGradientColoredArea(
				gcas,
				"BBDAF7", 1, LocationType.LOCATION_TYPE_ABSOLUTE_START, "BBDAF7", 1, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);

		addGradientColoredArea(
				gcas,
				"EEEEED", 1, LocationType.LOCATION_TYPE_ABSOLUTE_END, "000000", 0, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);

		return gradientColoredAreas;
	}

	/**
	 * @return {@link AdaptedGradientColoredAreas} color-areas for
	 *         {@link #LIGHT_GRAY_ID} with the adaptations:
	 *         {@link #STYLE_ADAPTATION_DEFAULT},
	 *         {@link #STYLE_ADAPTATION_PRIMARY_SELECTED}.
	 */
	public static AdaptedGradientColoredAreas getLightGrayAdaptions() {
		final AdaptedGradientColoredAreas agca = StylesFactory.eINSTANCE
				.createAdaptedGradientColoredAreas();
		agca.setDefinedStyleId(LIGHT_GRAY_ID);
		agca.setGradientType(IGradientType.VERTICAL);
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_DEFAULT,
				getLightGrayDefaultAreas());
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_PRIMARY_SELECTED,
				getLightGrayPrimarySelectedAreas());
		agca.getAdaptedGradientColoredAreas().add(
				IPredefinedRenderingStyle.STYLE_ADAPTATION_SECONDARY_SELECTED,
				getLightGraySecondarySelectedAreas());
		return agca;
	}

	/**
	 * The color-areas, which are used for default elements with the ID
	 * {@link #LIGHT_GRAY_ID}.
	 */
	private static GradientColoredAreas getLightGrayDefaultAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_DEFAULT);
		final EList<GradientColoredArea> gcas = gradientColoredAreas
				.getGradientColor();

		addGradientColoredArea(
				gcas,
				"F5F5ED", 0, LocationType.LOCATION_TYPE_ABSOLUTE_START, "F5F5ED", 0, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		return gradientColoredAreas;
	}

	/**
	 * The color-areas, which are used for primary selected elements with the ID
	 * {@link #LIGHT_GRAY_ID}.
	 */
	private static GradientColoredAreas getLightGrayPrimarySelectedAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_PRIMARY_SELECTED);
		final EList<GradientColoredArea> gcas = gradientColoredAreas
				.getGradientColor();

		addGradientColoredArea(
				gcas,
				"D6D6D0", 0, LocationType.LOCATION_TYPE_ABSOLUTE_START, "D6D6D0", 0, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		return gradientColoredAreas;
	}

	/**
	 * The color-areas, which are used for secondary selected elements with the
	 * ID {@link #LIGHT_GRAY_ID}.
	 */
	private static GradientColoredAreas getLightGraySecondarySelectedAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE
				.createGradientColoredAreas();
		gradientColoredAreas
				.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_SECONDARY_SELECTED);
		final EList<GradientColoredArea> gcas = gradientColoredAreas
				.getGradientColor();

		addGradientColoredArea(
				gcas,
				"E5E5Df", 0, LocationType.LOCATION_TYPE_ABSOLUTE_START, "E5E5Df", 0, //$NON-NLS-1$ //$NON-NLS-2$
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		return gradientColoredAreas;
	}

}
