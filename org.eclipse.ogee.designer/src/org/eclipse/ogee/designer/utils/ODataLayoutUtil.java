/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.datatypes.IDimension;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.RoundedRectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.layout.ILayout;
import org.eclipse.ogee.designer.preferences.IPreferenceConstants;
import org.eclipse.ogee.utils.logger.Logger;

/**
 * This class contains utility methods for Layout.
 * 
 */
public class ODataLayoutUtil {

	private static String[][] layoutOptions;
	private static String[][] contextMenuLayoutOptions;

	/**
	 * Returns true if the layout has been updated
	 * 
	 * @param context
	 *            ICreateContext instance
	 * @return boolean true layout changed
	 */
	public static boolean layout(ILayoutContext context) {
		boolean anythingChanged = false;
		ContainerShape containerShape = (ContainerShape) context
				.getPictogramElement();

		if (!(containerShape instanceof Diagram)) {
			anythingChanged = checkInitialSize(containerShape);

			updateExpandedRectangles(containerShape);

			// Update the child shapes
			boolean childrenChanged = updateChildren(containerShape);

			// Mark the shape as changed even if only internal shapes changed
			if (childrenChanged) {
				anythingChanged = childrenChanged;
			}
			// Redoing the layout for all self associations
			if (anythingChanged) {
				AssociationUtil.updateSelfAssociationLayout(containerShape);
			}
		}
		return anythingChanged;
	}

	/**
	 * This method is responsible to maintain the initial size.
	 * 
	 * @param containerShape
	 * @return
	 */
	private static boolean checkInitialSize(ContainerShape containerShape) {
		boolean anythingChanged = false;

		GraphicsAlgorithm containerGa = containerShape.getGraphicsAlgorithm();

		// If, the shape is resized to a lesser dimension than the default,
		// resize to default
		if (containerGa.getHeight() < IODataEditorConstants.RECTANGLE_INITIAL_HEIGHT) {
			containerGa
					.setHeight(IODataEditorConstants.RECTANGLE_INITIAL_HEIGHT);
			anythingChanged = true;
		}
		if (containerGa.getWidth() < IODataEditorConstants.RECTANGLE_INITIAL_WIDTH) {
			containerGa.setWidth(IODataEditorConstants.RECTANGLE_INITIAL_WIDTH);
			anythingChanged = true;
		}

		return anythingChanged;
	}

	/**
	 * This updates Container Heights for all Container Shapes (With Rectangle
	 * as GA). This method will be called on Resize feature.
	 * 
	 * @param containerShape
	 *            Container Shape
	 * @param rectangleHeight
	 */
	public static void updateContainerHeights(ContainerShape containerShape,
			int rectangleHeight) {

		EList<Shape> children = containerShape.getChildren();

		int increasedHeight = 0;
		int increasedPart = 0;
		Rectangle rectangle = null;
		Polygon polygon = null;
		int previousHeight = 0;
		int newHeight = 0;
		int subLastShapeBottom = 0;
		int height = 0;
		Shape currentShape = null;
		EList<Shape> subChildren = null;
		int size2 = 0;

		// Determining the amount in px by which the internal shapes will be
		// resized if the UI is resized manually or by adding new elements
		if (children.size() > 0) {
			// Graphics Algorithm for the last shape in the set e.g. Navigation
			// properties
			GraphicsAlgorithm lastShapeGA = children.get(children.size() - 1)
					.getGraphicsAlgorithm();

			// last shape bottom is also the previous bottom
			int lastShapeBottom = lastShapeGA.getY() + lastShapeGA.getHeight();
			// new bottom - old bottom gives the increase in height
			increasedHeight = (rectangleHeight - lastShapeBottom - IODataEditorConstants.RECTANGLE_MARGIN);
			// Rectangles are set to be expanded if anything has been added to
			// them
			int expandedRectangles = getExpandedRectangles(containerShape);
			// Only expanded rectangles need to be stretched for resizing the
			// Entity
			if (expandedRectangles > 0) {
				increasedPart = increasedHeight / expandedRectangles;
			}

		}

		for (int i = 0; i < children.size(); i++) {
			currentShape = children.get(i);
			if (!PropertyUtil.isTypeIdentifier(currentShape)) {

				GraphicsAlgorithm graphicsAlgorithm = currentShape
						.getGraphicsAlgorithm();

				if (graphicsAlgorithm instanceof Rectangle
						&& PropertyUtil.isExpanded(currentShape)) {
					rectangle = ((Rectangle) graphicsAlgorithm);
					previousHeight = rectangle.getHeight();
					newHeight = previousHeight + increasedPart;
					subLastShapeBottom = 0;

					subChildren = ((ContainerShape) currentShape).getChildren();

					size2 = subChildren.size();
					if (size2 > 0) {
						GraphicsAlgorithm subLastShapeGA = subChildren.get(
								subChildren.size() - 1).getGraphicsAlgorithm();
						subLastShapeBottom = subLastShapeGA.getY()
								+ subLastShapeGA.getHeight();
					}
					height = newHeight > subLastShapeBottom ? newHeight
							: subLastShapeBottom;
					PropertyUtil.setContainerHeight(currentShape, height);
					// Recursive call for children.
					updateContainerHeights((ContainerShape) currentShape,
							height);
				} else if (graphicsAlgorithm instanceof Polygon
						&& PropertyUtil.isExpanded(currentShape)) {
					polygon = ((Polygon) graphicsAlgorithm);
					previousHeight = polygon.getHeight();
					newHeight = previousHeight + increasedPart;
					subLastShapeBottom = 0;

					subChildren = ((ContainerShape) currentShape).getChildren();

					size2 = subChildren.size();
					if (size2 > 0) {
						GraphicsAlgorithm subLastShapeGA = subChildren.get(
								subChildren.size() - 1).getGraphicsAlgorithm();
						subLastShapeBottom = subLastShapeGA.getY()
								+ subLastShapeGA.getHeight();
					}
					height = newHeight > subLastShapeBottom ? newHeight
							: subLastShapeBottom;
					PropertyUtil.setContainerHeight(currentShape, height);
					// Recursive call for children.
					updateContainerHeights((ContainerShape) currentShape,
							height);
				}

			}
		}

	}

	/**
	 * This method is responsible to update the Container Heights (With
	 * Rectangle as GA) and and its child containers(With Rectangle as GA). This
	 * method will be called from Delete Feature.
	 * 
	 * @param currentShape
	 */
	public static void updateContainerHeightsOnDelete(
			ContainerShape currentShape) {

		if (!PropertyUtil.isTypeIdentifier(currentShape)) {

			GraphicsAlgorithm graphicsAlgorithm = currentShape
					.getGraphicsAlgorithm();

			if ((graphicsAlgorithm instanceof Rectangle || graphicsAlgorithm instanceof Polygon)
					&& PropertyUtil.isExpanded(currentShape)) {
				int subLastShapeBottom = 0;
				Shape childShape = null;
				GraphicsAlgorithm ChildGA = null;
				EList<Shape> subChildren = currentShape.getChildren();

				int size2 = subChildren.size();
				if (size2 > 0) {
					int index = subChildren.size() > 1 ? 2 : 1;
					GraphicsAlgorithm subLastShapeGA = subChildren.get(
							subChildren.size() - index).getGraphicsAlgorithm();
					if (index == 1) {
						subLastShapeBottom = subLastShapeGA.getHeight()
								+ IODataEditorConstants.RECTANGLE_MARGIN;
					} else {
						subLastShapeBottom = subLastShapeGA.getY()
								+ subLastShapeGA.getHeight()
								+ IODataEditorConstants.RECTANGLE_MARGIN;
					}

				}
				PropertyUtil.setContainerHeight(currentShape,
						subLastShapeBottom);
				for (int i = 0; i < subChildren.size(); i++) {
					childShape = subChildren.get(i);
					if (!PropertyUtil.isTypeIdentifier(currentShape)) {

						ChildGA = currentShape.getGraphicsAlgorithm();

						if ((ChildGA instanceof Rectangle || graphicsAlgorithm instanceof Polygon)
								&& PropertyUtil.isExpanded(childShape)) {

							// Recursive call for children.
							updateContainerHeightsOnDelete((ContainerShape) childShape);
						}

					}
				}
			}
		}
	}

	/**
	 * Holds the algorithm to change the layout of the container
	 * 
	 * @param containerShape
	 *            ContainerShape instance
	 * @return boolean true layout changed
	 */
	private static boolean updateChildren(ContainerShape containerShape) {
		boolean anythingChanged = false;
		GraphicsAlgorithm containerGA = containerShape.getGraphicsAlgorithm();
		int newOuterRectangleWidth = containerGA.getWidth();
		EList<Shape> children = containerShape.getChildren();

		anythingChanged = updateChildshapes(anythingChanged,
				newOuterRectangleWidth, children);

		// Graphics Algorithm for the last shape in the set e.g. Navigation
		// properties
		GraphicsAlgorithm lastShapeGA = children.get(children.size() - 1)
				.getGraphicsAlgorithm();

		// last shape bottom is also the previous bottom
		int lastShapeBottom = lastShapeGA.getY() + lastShapeGA.getHeight()
				+ IODataEditorConstants.RECTANGLE_MARGIN;
		if (containerGA instanceof Rectangle
				|| containerGA instanceof RoundedRectangle) {
			containerGA.setHeight(lastShapeBottom);
		} else if (containerGA instanceof Polygon) {
			int lastWidth = lastShapeGA.getWidth();
			Polygon polygon = (Polygon) containerGA;
			EList<Point> points = polygon.getPoints();
			int i = 0;
			for (Iterator<Point> iterator = points.iterator(); iterator
					.hasNext(); i++) {
				Point point = iterator.next();
				if (point.getX() > 0) {
					if (points.size() == 4) {
						point.setX(lastWidth);
					} else if (points.size() == 5) {
						if (i == 1) {
							point.setX(lastWidth - 20);
						} else {
							point.setX(lastWidth);
						}
					}
				}
				if (point.getY() > 20) {
					point.setY(lastShapeBottom);
				}
			}
		}
		return anythingChanged;
	}

	private static boolean updateChildshapes(boolean anythingChanged,
			int newOuterRectangleWidth, EList<Shape> children) {
		boolean changed = anythingChanged;
		Shape currentShape;
		GraphicsAlgorithm graphicsAlgorithm;
		IGaService gaService;
		IDimension size;
		Polyline polyline;
		Point secondPoint;
		Point newSecondPoint;
		Rectangle rectangle;
		Polygon polygon;
		int subLastShapeBottom;
		int size2;
		GraphicsAlgorithm previousAlgorithm;
		EList<Shape> subChildren;
		// For all elements resize has to be done according to the needs
		for (int i = 0; i < children.size(); i++) {

			currentShape = children.get(i);
			graphicsAlgorithm = currentShape.getGraphicsAlgorithm();
			gaService = Graphiti.getGaService();
			size = gaService.calculateSize(graphicsAlgorithm);
			// If, the width has changed for the outer rectangle
			// Update all child shapes to the same width
			if (newOuterRectangleWidth != size.getWidth()) {
				// Update separators
				if (graphicsAlgorithm instanceof Polyline) {
					polyline = (Polyline) graphicsAlgorithm;
					secondPoint = polyline.getPoints().get(1);
					newSecondPoint = gaService.createPoint(
							newOuterRectangleWidth - 1, secondPoint.getY());
					polyline.getPoints().set(1, newSecondPoint);

					changed = true;
				} else if (graphicsAlgorithm instanceof Rectangle) {// Update
																	// the inner
																	// rectangles
					((Rectangle) graphicsAlgorithm)
							.setWidth(newOuterRectangleWidth);
					updateControls((ContainerShape) currentShape);
					changed = true;
				} else if (graphicsAlgorithm instanceof Polygon) {// Update
					// the inner
					// rectangles
					polygon = (Polygon) graphicsAlgorithm;
					EList<Point> points = polygon.getPoints();
					float width = polygon.getWidth();
					float maxX = 0;
					maxX = newOuterRectangleWidth;
					// Compute scale factor
					float scaleY = width / maxX;
					if (scaleY != 1) {
						for (Iterator<Point> iterator = points.iterator(); iterator
								.hasNext();) {
							Point point = iterator.next();
							if (point.getX() > 0) {
								point.setX(newOuterRectangleWidth);
							}
						}
					}
					// ((Polygon) graphicsAlgorithm)
					// .setWidth(newOuterRectangleWidth);
					updateControls((ContainerShape) currentShape);
					changed = true;
				}
			}
			// Update the identifiers (texts and icons) together
			if (!PropertyUtil.isTypeIdentifier(currentShape)) {
				if (graphicsAlgorithm instanceof Rectangle
						&& PropertyUtil.isExpanded(currentShape)) {
					rectangle = ((Rectangle) graphicsAlgorithm);
					Long containerHeight = PropertyUtil
							.getContainerHeight(currentShape);
					subLastShapeBottom = 0;

					containerHeight = PropertyUtil
							.getContainerHeight(currentShape);

					subChildren = ((ContainerShape) currentShape).getChildren();
					updateChildshapes(changed, newOuterRectangleWidth,
							subChildren);
					size2 = subChildren.size();
					if (size2 > 0) {
						GraphicsAlgorithm subLastShapeGA = subChildren.get(
								subChildren.size() - 1).getGraphicsAlgorithm();
						subLastShapeBottom = subLastShapeGA.getY()
								+ subLastShapeGA.getHeight();
					}

					rectangle
							.setHeight(containerHeight != null
									&& containerHeight.intValue() > subLastShapeBottom ? containerHeight
									.intValue() : subLastShapeBottom);
					changed = true;
				} else if (graphicsAlgorithm instanceof Polygon
						&& PropertyUtil.isExpanded(currentShape)) {
					polygon = ((Polygon) graphicsAlgorithm);
					Long containerHeight = PropertyUtil
							.getContainerHeight(currentShape);
					subLastShapeBottom = 0;

					containerHeight = PropertyUtil
							.getContainerHeight(currentShape);

					subChildren = ((ContainerShape) currentShape).getChildren();
					updateChildshapes(changed, newOuterRectangleWidth,
							subChildren);
					size2 = subChildren.size();
					if (size2 > 0) {
						GraphicsAlgorithm subLastShapeGA = subChildren.get(
								subChildren.size() - 1).getGraphicsAlgorithm();
						subLastShapeBottom = subLastShapeGA.getY()
								+ subLastShapeGA.getHeight();
					}

					int newHeight = containerHeight != null
							&& containerHeight.intValue() > subLastShapeBottom ? containerHeight
							.intValue() : subLastShapeBottom;
					polygon = (Polygon) graphicsAlgorithm;
					EList<Point> points = polygon.getPoints();
					float width = polygon.getWidth();
					float maxX = 0;
					maxX = newHeight;
					// Compute scale factor
					float scaleY = width / maxX;
					if (scaleY != 1) {
						for (Iterator<Point> iterator = points.iterator(); iterator
								.hasNext();) {
							Point point = iterator.next();
							if (point.getX() > 0) {
								point.setX(newHeight);
							}
						}
					}
					changed = true;
				}
			}
			// Update the outer rectangle size
			previousAlgorithm = getPreviousGA(graphicsAlgorithm, i, children);
			if (previousAlgorithm != null) {
				graphicsAlgorithm.setY(previousAlgorithm.getY()
						+ previousAlgorithm.getHeight()
						+ IODataEditorConstants.RECTANGLE_MARGIN);
			} else {
				graphicsAlgorithm.setY(IODataEditorConstants.RECTANGLE_MARGIN);
			}

		}
		return changed;
	}

	/**
	 * Returns the number of rectangles which have children and are expanded
	 * There are currently three rectangles. (Entity Set, Properties, Navigation
	 * Properties)
	 * 
	 * @param containerShape
	 * @return int
	 */
	private static int getExpandedRectangles(ContainerShape containerShape) {

		EList<Shape> children = containerShape.getChildren();
		int count = 0;
		Shape currentShape;
		for (int i = 0; i < children.size(); i++) {
			currentShape = children.get(i);
			if (!PropertyUtil.isTypeIdentifier(currentShape)) {

				if (PropertyUtil.isExpanded(currentShape)) {
					((ContainerShape) currentShape).setVisible(true);
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * 
	 * 
	 * @param containerShape
	 */
	private static void updateExpandedRectangles(ContainerShape containerShape) {

		GraphicsAlgorithm containerGA = containerShape.getGraphicsAlgorithm();
		EList<Shape> children = containerShape.getChildren();

		// Determining the amount in px by which the internal shapes will be
		// resized if the UI is resized manually or by adding new elements
		if (children.size() > 0) {
			// Graphics Algorithm for the last shape in the set e.g. Navigation
			// properties
			GraphicsAlgorithm lastShapeGA = children.get(children.size() - 1)
					.getGraphicsAlgorithm();
			Shape currentShape = null;

			int outerRectangleBottom = containerGA.getHeight();
			// last shape bottom is also the previous bottom
			int lastShapeBottom = lastShapeGA.getY() + lastShapeGA.getHeight();
			// new bottom - old bottom gives the increase in height
			int increasedHeight = (outerRectangleBottom - lastShapeBottom - IODataEditorConstants.RECTANGLE_MARGIN);
			// Rectangles are set to be expanded if anything has been added to
			// them

			for (int i = 0; i < children.size(); i++) {
				currentShape = children.get(i);
				if (!PropertyUtil.isTypeIdentifier(currentShape)) {

					if (PropertyUtil.isExpanded(currentShape)) {
						((ContainerShape) currentShape).setVisible(true);
					} else if (PropertyUtil.isCollapsed(currentShape)) {
						if (increasedHeight == 0) {
							containerShape.getGraphicsAlgorithm().setHeight(
									containerShape.getGraphicsAlgorithm()
											.getHeight()
											- currentShape
													.getGraphicsAlgorithm()
													.getHeight());
						}
						currentShape.getGraphicsAlgorithm().setWidth(0);
						currentShape.getGraphicsAlgorithm().setHeight(0);
						((ContainerShape) currentShape).setVisible(false);
					}
				}
			}

		}

	}

	/**
	 * 
	 * @param container
	 * @return
	 */
	private static boolean updateControls(ContainerShape container) {

		boolean anythingChanged = false;
		EList<Shape> children = container.getChildren();

		int newOuterRectangleWidth = container.getGraphicsAlgorithm()
				.getWidth();
		Shape currentShape;
		GraphicsAlgorithm graphicsAlgorithm;
		IGaService gaService;
		IDimension size;
		Polyline polyline;
		Point secondPoint;
		Point newSecondPoint;
		for (int i = 0; i < children.size(); i++) {
			currentShape = children.get(i);
			graphicsAlgorithm = currentShape.getGraphicsAlgorithm();
			gaService = Graphiti.getGaService();
			size = gaService.calculateSize(graphicsAlgorithm);

			if (newOuterRectangleWidth != size.getWidth()) {
				if (graphicsAlgorithm instanceof Polyline) {
					polyline = (Polyline) graphicsAlgorithm;
					secondPoint = polyline.getPoints().get(1);
					newSecondPoint = gaService.createPoint(
							newOuterRectangleWidth - 1, secondPoint.getY());
					polyline.getPoints().set(1, newSecondPoint);
					anythingChanged = true;
				} else if (graphicsAlgorithm instanceof Text) {
					if (children.size() == 4) {
						if ((i == 1 || i == 3)) {
							Shape shape3 = children.get(3);
							GraphicsAlgorithm ga3 = shape3
									.getGraphicsAlgorithm();
							int increasedWidth = newOuterRectangleWidth
									- ga3.getX()
									- ga3.getWidth()
									- IODataEditorConstants.TEXTBOX_RIGHT_MARGIN;
							gaService.setWidth(graphicsAlgorithm,
									graphicsAlgorithm.getWidth()
											+ (increasedWidth / 2));
						}
						if (i == 2 || i == 3) {
							Shape prevShape = children.get(i - 1);
							GraphicsAlgorithm prevGa = prevShape
									.getGraphicsAlgorithm();
							graphicsAlgorithm.setX(prevGa.getX()
									+ prevGa.getWidth());
						}

					} else {
						gaService
								.setWidth(
										graphicsAlgorithm,
										newOuterRectangleWidth
												- graphicsAlgorithm.getX()
												- IODataEditorConstants.TEXTBOX_RIGHT_MARGIN);
					}

					anythingChanged = true;
				} else if (graphicsAlgorithm instanceof Rectangle) {
					((Rectangle) graphicsAlgorithm)
							.setWidth(newOuterRectangleWidth);
					anythingChanged = updateControls((ContainerShape) currentShape);
					anythingChanged = true;
				} else if (graphicsAlgorithm instanceof Polygon) {
					// ((Polygon) graphicsAlgorithm)
					// .setWidth(newOuterRectangleWidth);
					Polygon polygon = (Polygon) graphicsAlgorithm;
					EList<Point> points = polygon.getPoints();
					float width = polygon.getWidth();
					float maxX = 0;
					maxX = newOuterRectangleWidth;
					// Compute scale factor
					float scaleY = width / maxX;
					if (scaleY != 1) {
						for (Iterator<Point> iterator = points.iterator(); iterator
								.hasNext();) {
							Point point = iterator.next();
							if (point.getX() > 0) {
								point.setX(newOuterRectangleWidth);
							}
						}
					}
					anythingChanged = updateControls((ContainerShape) currentShape);
					anythingChanged = true;
				}
			}
		}
		return anythingChanged;
	}

	/**
	 * gets the previous graphic properties for comparison with the new
	 * dimensions
	 * 
	 * @param currentGA
	 * @param index
	 * @param children
	 * @return
	 */
	private static GraphicsAlgorithm getPreviousGA(GraphicsAlgorithm currentGA,
			int index, List<Shape> children) {
		int i = index - 1;
		if (i >= 0) {
			GraphicsAlgorithm previousGA = children.get(i)
					.getGraphicsAlgorithm();
			if (previousGA instanceof Image) {
				return getPreviousGA(currentGA, i, children);
			} else if (previousGA instanceof Polyline
					&& currentGA instanceof Polyline) {
				return getPreviousGA(currentGA, i, children);
			} else {

				return previousGA;

			}
		}
		return null;
	}

	/**
	 * Expands the Container Shape.
	 * 
	 * @param shape
	 * @return Title Image
	 */
	public static Shape expandSection(ContainerShape shape) {
		Shape titleImage = null;

		if (!PropertyUtil.isExpanded(shape)) {
			EObject eContainer = shape.eContainer();
			if (eContainer instanceof ContainerShape) {
				ContainerShape entityContainer = (ContainerShape) eContainer;
				EList<Shape> children = entityContainer.getChildren();
				if (children != null && children.size() > 0) {
					int index = children.indexOf(shape);
					if (index > 0) {
						Shape titleShape = children.get(index - 1);
						if (PropertyUtil.isTitle(titleShape)) {
							PropertyUtil.setExpanded(shape);
							PropertyUtil.setExpanded(titleShape);
							titleImage = ((ContainerShape) titleShape)
									.getChildren().get(0);
						}
					}
				}

			}
		}

		return titleImage;
	}

	/**
	 * If the selected Object is Identifier Shape then Section Title will be
	 * found.
	 * 
	 * @param identifierShape
	 * @return Title Image
	 */
	public static Shape getSectionTitle(final ContainerShape identifierShape) {

		Shape titleImage = null;

		if (PropertyUtil.isTypeIdentifier(identifierShape)) {
			final ContainerShape shape = (ContainerShape) identifierShape
					.eContainer();
			if (PropertyUtil.isCollapsed(shape)) {
				final EObject eContainer = shape.eContainer();
				if (eContainer instanceof ContainerShape) {
					final ContainerShape entityContainer = (ContainerShape) eContainer;
					final EList<Shape> children = entityContainer.getChildren();
					if (children != null && children.size() > 0) {
						final int index = children.indexOf(shape);
						if (index > 0) {
							final Shape titleShape = children.get(index - 1);
							if (PropertyUtil.isTitle(titleShape)) {
								titleImage = titleShape;
							}
						}
					}
				}
			}
		}

		return titleImage;
	}

	/**
	 * This method returns all extension available for layout extension point.
	 * 
	 * @return array of IConfigurationElements
	 */
	public static IConfigurationElement[] getLayoutExtensions() {

		IConfigurationElement[] layoutConfigElems = Platform
				.getExtensionRegistry().getConfigurationElementsFor(
						IODataEditorConstants.LAYOUT_EXTENSIONPOINT_ID);

		return layoutConfigElems;
	}

	/**
	 * Returns Current Layout,Which is defined in Preference Store. As Current
	 * Layout can be changed form Layout Preferences.
	 * 
	 * @param layoutId
	 * 
	 * @return ILayout Instance.
	 */
	public static ILayout getCurrentLayout(String layoutId) {

		ILayout currentLayout = null;
		String layoutID = layoutId;

		try {

			if (layoutID == null) {
				layoutID = Activator.getDefault().getPreferenceStore()
						.getString(IPreferenceConstants.LAYOUT_CHOICE);
			}

			IConfigurationElement[] layoutConfigElems = getLayoutExtensions();
			for (IConfigurationElement iConfigurationElement : layoutConfigElems) {
				String id = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_ID);
				if (id.equals(layoutID)) {
					currentLayout = (ILayout) iConfigurationElement
							.createExecutableExtension(IODataEditorConstants.LAYOUT_ATTRIBUTE_CLASS);
					break;
				}

			}
		} catch (CoreException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		}

		return currentLayout;
	}

	/**
	 * This method is used in creating UI in Layout Preference Page. Creates a
	 * String Array with id & name with all available layouts.
	 * 
	 * 
	 * @return String Array.
	 */
	public static String[][] getLayoutOptions() {

		if (layoutOptions == null) {
			IConfigurationElement[] layoutExtensions = getLayoutExtensions();
			Map<String, IConfigurationElement> filteredExtensions = new LinkedHashMap<String, IConfigurationElement>();

			for (IConfigurationElement iConfigurationElement : layoutExtensions) {
				String id = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_ID);
				String className = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_CLASS);
				String displayName = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_NAME);
				if (id != null && !id.isEmpty() && className != null
						&& !className.isEmpty() && displayName != null
						&& !displayName.isEmpty()) {
					try {
						ILayout currentLayout = (ILayout) iConfigurationElement
								.createExecutableExtension(IODataEditorConstants.LAYOUT_ATTRIBUTE_CLASS);
						if (currentLayout != null) {
							if (!id.equals(IODataEditorConstants.DEFAULT_LAYOUT_ASSOCIATIONS_ID)) {
								filteredExtensions.put(id,
										iConfigurationElement);
							}

						}
					} catch (CoreException e) {
						Logger.getLogger(Activator.PLUGIN_ID).logError(e);
					}

				}
			}
			int counter = 0;
			Collection<String> filteredKeys = filteredExtensions.keySet();
			layoutOptions = new String[filteredKeys.size()][2];
			for (String key : filteredKeys) {
				IConfigurationElement iConfigurationElement = filteredExtensions
						.get(key);
				String name = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_NAME);
				layoutOptions[counter][0] = name;
				String id = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_ID);
				layoutOptions[counter++][1] = id;
			}
		}

		return layoutOptions;
	}

	/**
	 * This method is used in creating UI in Editor Context Menu. Creates a
	 * String Array with id & name with all available layouts including Layout
	 * Only Associations.
	 * 
	 * 
	 * @return String Array.
	 */
	public static String[][] getContextMenuLayoutOptions() {

		if (contextMenuLayoutOptions == null) {
			IConfigurationElement[] layoutExtensions = getLayoutExtensions();

			Map<String, IConfigurationElement> filteredExtensions = new LinkedHashMap<String, IConfigurationElement>();

			for (IConfigurationElement iConfigurationElement : layoutExtensions) {
				String id = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_ID);
				String className = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_CLASS);
				String displayName = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_NAME);
				if (id != null && !id.isEmpty() && className != null
						&& !className.isEmpty() && displayName != null
						&& !displayName.isEmpty()) {
					try {
						ILayout currentLayout = (ILayout) iConfigurationElement
								.createExecutableExtension(IODataEditorConstants.LAYOUT_ATTRIBUTE_CLASS);
						if (currentLayout != null) {
							filteredExtensions.put(id, iConfigurationElement);
						}

					} catch (CoreException e) {
						Logger.getLogger(Activator.PLUGIN_ID).logError(e);
					}

				}
			}
			int counter = 0;
			Collection<String> filteredKeys = filteredExtensions.keySet();
			contextMenuLayoutOptions = new String[filteredKeys.size()][2];
			for (String key : filteredKeys) {
				IConfigurationElement iConfigurationElement = filteredExtensions
						.get(key);
				String name = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_NAME);
				contextMenuLayoutOptions[counter][0] = name;
				String id = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_ID);
				contextMenuLayoutOptions[counter++][1] = id;
			}
		}

		return contextMenuLayoutOptions;
	}

	/**
	 * This method is responsible to define the default layout from available
	 * Layout extensions. "isDefault"attribute will be checked in order decide
	 * whether current Layout is default or not.
	 * 
	 * @return defaultLayoutId
	 */
	public static String getDefaultLayoutId() {

		IConfigurationElement[] layoutExtensions = getLayoutExtensions();
		String defaultLayoutId = ""; //$NON-NLS-1$
		for (IConfigurationElement iConfigurationElement : layoutExtensions) {
			String isDefault = iConfigurationElement
					.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_ISDEFAULT);
			if (Boolean.valueOf(isDefault).booleanValue()) {
				defaultLayoutId = iConfigurationElement
						.getAttribute(IODataEditorConstants.LAYOUT_ATTRIBUTE_ID);

			}

		}

		return defaultLayoutId;
	}

}
