/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import org.eclipse.graphiti.util.ColorConstant;
import org.eclipse.graphiti.util.IColorConstant;
import org.eclipse.ogee.designer.Activator;

/**
 * The Interface IODataEditorConstants. Holds constants to be used across editor
 * code
 * 
 */
public interface IODataEditorConstants {

	/**
	 * The constant for OData Diagram Type in Graphiti.
	 */
	static final String ODATA_DIAGRAM_TYPE = "ODataDiagramType"; //$NON-NLS-1$

	/**
	 * UTF-8 Encoding
	 */
	static final String ENCODING_UTF8 = "UTF-8"; //$NON-NLS-1$

	/**
	 * OData Editor temporary model file extension.
	 */
	static final String TEMPFILE_EXTENSION = "odata2d"; //$NON-NLS-1$

	/**
	 * OData Editor temporary project prefix.
	 */
	static final String OEDITOR_TEMPPRJ = "_@OEditor_Service_Catalog_View_TmpPrj@"; //$NON-NLS-1$
	/**
	 * Property View ID
	 */
	static final String PROPERTY_VIEW_ID = "org.eclipse.ui.views.PropertySheet"; //$NON-NLS-1$

	/**
	 * Problem Marker ID
	 */
	static final String PROBLEM_MARKER_ID = "org.eclipse.ogee.model.ProblemMarker"; //$NON-NLS-1$

	/**
	 * UpdateAction ID
	 */
	public static final String UPDATE_ACTION_ID = "predefined update action"; //$NON-NLS-1$

	/**
	 * Remove Action ID
	 */
	public static final String REMOVE_ACTION_ID = "predefined remove action"; //$NON-NLS-1$

	/**
	 * Save As Image Action ID
	 */
	public static final String SAVEIMAGE_ACTION_ID = "export_diagram_action"; //$NON-NLS-1$

	/**
	 * Context ID for OData Editor. The context ID is maintained in plugin.xml.
	 */
	static final String ODATA_EDITOR_CONTEXT_ID = "org.eclipse.ogee.designer.odataEditorScope"; //$NON-NLS-1$

	/**
	 * Action ID for "Show/Hide All Usages of Types" check button.
	 */
	public static final String ACTION_ID_SHOW_HIDE_USAGES = "org.eclipse.ogee.designer.action.show_hide_usages"; //$NON-NLS-1$

	/**
	 * Action Definition ID for Show usage action. This is the command id
	 * maintained in plugin.xml.
	 */
	public static final String ACTION_DEF_ID_SHOW_HIDE_USAGES = "org.eclipse.ogee.designer.command.show_hide_usages"; //$NON-NLS-1$

	/**
	 * Action ID for Collapse All action.
	 */
	public static final String ACTION_ID_COLLAPSE_ALL = "org.eclipse.ogee.designer.action.collapse_all"; //$NON-NLS-1$

	/**
	 * Action Definition ID for Collapse All action. This is the command id
	 * maintained in plugin.xml.
	 */
	public static final String ACTION_DEF_ID_COLLAPSE_ALL = "org.eclipse.ogee.designer.command.collapse_all"; //$NON-NLS-1$

	/**
	 * Action ID for Expand All action.
	 */
	public static final String ACTION_ID_EXPAND_ALL = "org.eclipse.ogee.designer.action.expand_all"; //$NON-NLS-1$

	/**
	 * Action Definition ID for Expand All action. This is the command id
	 * maintained in plugin.xml.
	 */
	public static final String ACTION_DEF_ID_EXPAND_ALL = "org.eclipse.ogee.designer.command.expand_all"; //$NON-NLS-1$	

	/**
	 * Action Definition ID for Layout action. This is the command id maintained
	 * in plugin.xml.
	 */
	public static final String ACTION_DEF_ID_LAYOUT = "org.eclipse.ogee.designer.command.layout"; //$NON-NLS-1$

	/**
	 * Action Definition ID for Layout action. This is the command id maintained
	 * in plugin.xml.
	 */
	public static final String LAYOUT_MENU_ID = "org.eclipse.ogee.designer.layout"; //$NON-NLS-1$

	/**
	 * Action Definition ID for Layout action. This is the command id maintained
	 * in plugin.xml.
	 */
	public static final String DEFAULT_LAYOUT_ID = "org.eclipse.ogee.designer.defaultlayout"; //$NON-NLS-1$

	/**
	 * Action Definition ID for Context Menu action. This is the command id
	 * maintained in plugin.xml.
	 */
	public static final String CONTEXT_MENU_ID = "org.eclipse.ogee.designer.contextmenu"; //$NON-NLS-1$

	/**
	 * Context ID for OData Editor OData Model Page..
	 */
	public static final String HELP_CONTEXT_ID = "org.eclipse.ogee.designer.odataeditor"; //$NON-NLS-1$

	/**
	 * Context ID for OData Editor EDMX References Page
	 */
	public static final String HELP_CONTEXT_ID_EDMXREFERENCES = "org.eclipse.ogee.designer.odataeditoredmxreferences"; //$NON-NLS-1$     

	/**
	 * Context ID for OData Editor Preferences Dialog.
	 */
	public static final String HELP_CONTEXT_ID_PREFERENCES = "org.eclipse.ogee.designer.odataeditorpreferences"; //$NON-NLS-1$

	/**
	 * Context ID for OData Editor Add EDMX Reference Dialog.
	 */
	public static final String HELP_CONTEXT_ID_REFERENCESDIALOG = "org.eclipse.ogee.designer.edmxreferencedialog"; //$NON-NLS-1$

	/**
	 * Color constant for outer rectangle of the artifact
	 */
	public static final IColorConstant ARTIFACT_BORDER = new ColorConstant(
			"6283A7"); //$NON-NLS-1$

	/**
	 * Color constant for outer rectangle of the artifact
	 */
	public static final IColorConstant REFERENCED_ARTIFACT_BORDER = new ColorConstant(
			89, 89, 89);

	/**
	 * Restricted length for Simple Identifier
	 */
	public static final int MAX_LENGTH = 128;

	/**
	 * Width of the outer rectangle of the artifact
	 */
	public static final int ARTIFACT_BORDER_WIDTH = 2;

	/**
	 * Transparency of the outer rectangle of the artifact
	 */
	public static final double ARTIFACT_TRANSPARENCY = 0.0;

	/**
	 * Outer rectangle's corner dimension
	 */
	public static final int RECTANGLE_CORNER_WIDTH = 6;

	/**
	 * Outer rectangle's corner dimension
	 */
	public static final int RECTANGLE_CORNER_HEIGHT = 6;

	/**
	 * Outer rectangle's initial dimension
	 */
	public static final int RECTANGLE_INITIAL_WIDTH = 175;

	/**
	 * Outer rectangle's initial dimension
	 */
	public static final int RECTANGLE_INITIAL_HEIGHT = 112;

	/**
	 * Inner rectangle's initial margin from the previous shape
	 */
	public static final int RECTANGLE_MARGIN = 4;

	/**
	 * Inner rectangle's initial dimension
	 */
	public static final int INNER_RECTANGLE_INITIAL_WIDTH = 175;

	/**
	 * Inner rectangle's initial dimension
	 */
	public static final int INNER_RECTANGLE_INITIAL_HEIGHT = 18;

	/**
	 * Inner icon's initial margin from the left border
	 */
	public static final int IMAGE_LEFT_MARGIN = 5;

	/**
	 * Inner icon's initial dimension
	 */
	public static final int IMAGE_WIDTH = 22;

	/**
	 * Inner icon's initial dimension
	 */
	public static final int IMAGE_HEIGHT = 16;

	/**
	 * Polyline primary color
	 */
	public static final IColorConstant LINE_COLOR = new ColorConstant("BAB7A6"); //$NON-NLS-1$

	/**
	 * Polyline secondary color
	 */
	public static final IColorConstant LINE_SECONDARY_COLOR = new ColorConstant(
			255, 255, 255);

	/**
	 * Polyline primary width
	 */
	public static final int LINE_PRIMARY_WIDTH = 1;

	/**
	 * Polyline secondary width
	 */
	public static final int LINE_SECONDARY_WIDTH = 1;

	/**
	 * Text box's margin from right
	 */
	public static final int TEXTBOX_RIGHT_MARGIN = 16;

	/**
	 * Text default height
	 */
	public static final int TEXT_DEFAULT_HEIGHT = 16;

	/**
	 * Text Color
	 */
	public static final IColorConstant ODATA_TEXT_FOREGROUND = new ColorConstant(
			0, 0, 0);

	/**
	 * Foreground for artifacts - to be used if default is not desired
	 */
	public static final IColorConstant ODATA_FOREGROUND = new ColorConstant(98,
			131, 167);

	/**
	 * Background for artifacts - to be used if default is not desired
	 */
	public static final IColorConstant ODATA_BACKGROUND = new ColorConstant(
			187, 218, 247);

	/**
	 * Association Polyline color in not selected state
	 */
	public static final IColorConstant LINE_COLOR_ASSOC = new ColorConstant(
			"6283A7"); //$NON-NLS-1$

	/**
	 * Association cardinality color in not selected state
	 */
	public static final IColorConstant CARD_COLOR_ASSOC = new ColorConstant(
			"2e4053"); //$NON-NLS-1$

	/**
	 * Suffix for association set name defaults
	 */
	public static final String DEFAULT_NAME_SUFFIX = "Set"; //$NON-NLS-1$

	/**
	 * Colon width
	 */
	public static final int SEMICOLON_WIDTH = 10;

	/**
	 * String Constant for Layout Extension Point ID
	 */
	public static final String LAYOUT_EXTENSIONPOINT_ID = Activator.PLUGIN_ID
			+ ".layout"; //$NON-NLS-1$

	/**
	 * String Constant for Extension Menu Extension Point ID
	 */
	public static final String CONTEXT_MENU_EXTENSIONPOINT_ID = Activator.PLUGIN_ID
			+ ".contextmenu"; //$NON-NLS-1$

	/**
	 * String Constant for Image Provider Extension Point ID
	 */
	public static final String IMAGE_PROVIDER_EXTENSIONPOINT_ID = Activator.PLUGIN_ID
			+ ".imageprovider"; //$NON-NLS-1$

	/**
	 * String Constant for Image provider Extension Attribute "id"
	 */

	public static final String IMAGE_PROVIDER_ATTRIBUTE_ID = "id";
	/**
	 * String Constant for Extension Menu Extension Point ID
	 */
	public static final String CONTEXT_MENU_BINDING_EXTENSIONPOINT_ID = Activator.PLUGIN_ID
			+ ".bindingcontextmenu"; //$NON-NLS-1$

	/**
	 * String Constant for Context Menu Extension Attribute "name"
	 */
	public static final String CONTEXT_MENU_ATTRIBUTE_NAME = "name"; //$NON-NLS-1$

	/**
	 * String Constant for Context Menu Extension Attribute "id"
	 */
	public static final String CONTEXT_MENU_ATTRIBUTE_ID = "id"; //$NON-NLS-1$

	/**
	 * String Constant for Context Menu Extension Attribute "parent_name"
	 */
	public static final String CONTEXT_MENU_ATTRIBUTE_PARENT_NAME = "parent_name"; //$NON-NLS-1$

	/**
	 * String Constant for Context Menu Extension Attribute "icon"
	 */
	public static final String CONTEXT_MENU_ATTRIBUTE_ICON = "icon"; //$NON-NLS-1$

	/**
	 * String Constant for Context Menu Extension Attribute "key_binding"
	 */
	public static final String CONTEXT_MENU_ATTRIBUTE_KEY_BINDING = "key_binding"; //$NON-NLS-1$

	/**
	 * String Constant for Context Menu Extension Attribute "class"
	 */
	public static final String CONTEXT_MENU_ATTRIBUTE_CLASS = "class"; //$NON-NLS-1$

	/**
	 * String Constant for Layout Extension Attribute "class"
	 */
	public static final String LAYOUT_ATTRIBUTE_CLASS = "class"; //$NON-NLS-1$

	/**
	 * String Constant for Layout Extension Attribute "name"
	 */
	public static final String LAYOUT_ATTRIBUTE_NAME = "name"; //$NON-NLS-1$

	/**
	 * String Constant for Layout Extension Attribute "id"
	 */
	public static final String LAYOUT_ATTRIBUTE_ID = "id"; //$NON-NLS-1$

	/**
	 * String Constant for Layout Extension Attribute "isDefault"
	 */
	public static final String LAYOUT_ATTRIBUTE_ISDEFAULT = "isDefault"; //$NON-NLS-1$

	/**
	 * String Constant for filter extension for All Files
	 */
	public static final String FILTER_EXT_ALL_FILES = "*.*"; //$NON-NLS-1$

	/**
	 * Constant for cached status key
	 */
	public static final String CACHED_STATUS_KEY = "status"; //$NON-NLS-1$

	/**
	 * Constant for cached message key
	 */
	public static final String CACHED_MESSAGE_KEY = "message"; //$NON-NLS-1$

	/**
	 * Constant for cached URL key
	 */
	public static final String CACHED_URL_KEY = "url"; //$NON-NLS-1$

	/**
	 * Constant for cached Service Node key
	 */
	public static final String CACHED_SERVICE_NODE_KEY = "serviceNode"; //$NON-NLS-1$

	/**
	 * Constant for cached Result key
	 */
	public static final String CACHED_RESULT_KEY = "result"; //$NON-NLS-1$

	/**
	 * Action Definition ID for Layout Associations Only action. This is the
	 * command id maintained in plugin.xml.
	 */
	public static final String DEFAULT_LAYOUT_ASSOCIATIONS_ID = "org.eclipse.ogee.designer.layout.AssociationsLayout"; //$NON-NLS-1$

	/**
	 * The OData Perspective Id
	 */
	public static final String ODATA_PERSPECTIVE_ID = "org.eclipse.ogee.utils.perspective"; //$NON-NLS-1$

	/**
	 * String Constant for Selection Extension Attribute "name"
	 */
	public static final String ODATA_ARTIFACT_SELECTION_ATTRIBUTE_NAME = Activator.PLUGIN_ID
			+ ".artifactselection"; //$NON-NLS-1$

	/**
	 * String Constant for artifact selection Extension Attribute "id"
	 */
	public static final String ARTIFACT_SELECTION_ATTRIBUTE_ID = "id"; //$NON-NLS-1$

	/**
	 * String Constant for artifact selection Extension Attribute "class"
	 */
	public static final String ARTIFACT_SELECTION_ATTRIBUTE_CLASS = "class"; //$NON-NLS-1$

}
