/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.algorithms.styles.Style;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.graphics.Font;

/**
 * This class has different methods to style the UI elements such as texts,
 * rectangles and lines. This sets the default properties for these elements'
 * properties such as colors, sizes, and styles
 * 
 */
public class StyleUtil {

	/**
	 * Sets common values for the elements such as line
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	public static Style getStyleForCommonValues(Diagram diagram) {
		final String styleId = "COMMON-VALUES"; //$NON-NLS-1$
		IGaService gaService = Graphiti.getGaService();
		// Is style already persisted?
		Style style = gaService.findStyle(diagram, styleId);
		if (style == null) { // style not found - create new style
			style = gaService.createPlainStyle(diagram, styleId);
			setValues(style);
		}
		return style;
	}

	/**
	 * Sets default values for an artifact colors and gradients
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	public static Style getDefaultStyleForArtifact(Diagram diagram) {
		final String styleId = "ODATA-DEFAULT-ARTIFACT"; //$NON-NLS-1$
		IGaService gaService = Graphiti.getGaService();

		// this is a child style of the common-values-style
		Style parentStyle = getStyleForCommonValues(diagram);
		Style style = gaService.findStyle(parentStyle, styleId);

		if (style == null) { // style not found - create new style
			style = gaService.createPlainStyle(parentStyle, styleId);
		}
		// default colors
		gaService.setRenderingStyle(style,
				ODataPredefinedStyle.getBlueWhiteGlossAdaptions());
		return style;

	}

	/**
	 * Sets values for an artifact colors and gradients when the shape is a
	 * referenced shape Currently grey is used.
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	public static Style getReferencedStyleForArtifact(Diagram diagram) {
		final String styleId = "ODATA-REFERENCED-ARTIFACT"; //$NON-NLS-1$
		IGaService gaService = Graphiti.getGaService();

		// this is a child style of the common-values-style
		Style parentStyle = getStyleForCommonValues(diagram);
		Style style = gaService.findStyle(parentStyle, styleId);

		if (style == null) { // style not found - create new style
			style = gaService.createPlainStyle(parentStyle, styleId);
		}
		// default colors
		gaService.setRenderingStyle(style,
				ODataPredefinedStyle.getLightGrayAdaptions());
		return style;

	}

	/**
	 * Sets default values for a selected domain object
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	public static Style getDefaultSelectedStyle(Diagram diagram) {
		final String styleId = "ODATA-DEFAULT-OBJECT"; //$NON-NLS-1$
		IGaService gaService = Graphiti.getGaService();

		// this is a child style of the common-values-style
		Style parentStyle = getStyleForCommonValues(diagram);
		Style style = gaService.findStyle(parentStyle, styleId);

		if (style == null) { // style not found - create new style
			style = gaService.createPlainStyle(parentStyle, styleId);
		}
		// default colors
		gaService.setRenderingStyle(style,
				ODataPredefinedStyle.getSelectedAdaptions());
		return style;

	}

	/**
	 * Sets artifact colors if default is not required Colors can be set in the
	 * Constants.
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	public static Style getStyleForArtifact(Diagram diagram) {
		final String styleId = "ODATA-ARTIFACT"; //$NON-NLS-1$
		IGaService gaService = Graphiti.getGaService();

		// this is a child style of the common-values-style
		Style parentStyle = getStyleForCommonValues(diagram);
		Style style = gaService.findStyle(parentStyle, styleId);

		if (style == null) { // style not found - create new style
			style = gaService.createPlainStyle(parentStyle, styleId);
			style.setFilled(true);
			style.setForeground(gaService.manageColor(diagram,
					IODataEditorConstants.ODATA_FOREGROUND));
			style.setBackground(gaService.manageColor(diagram,
					IODataEditorConstants.ODATA_BACKGROUND));
		}
		return style;

	}

	/**
	 * Sets Header text properties such as font, size and color Can be changed
	 * through Constants or Properties file
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	public static Style getDefaultStyleForHeaderText(Diagram diagram) {
		final String styleId = "ODATA-HEADER-TEXT"; //$NON-NLS-1$
		IGaService gaService = Graphiti.getGaService();

		// this is a child style of the common-values-style
		Style parentStyle = getStyleForCommonValues(diagram);
		Style style = gaService.findStyle(parentStyle, styleId);

		if (style == null) { // style not found - create new style
			style = gaService.createPlainStyle(parentStyle, styleId);
			setTextValues(diagram, gaService, style);
		}
		style.setFont(gaService.manageFont(diagram, getFontName(),
				getFontSize(), false, true));

		return style;
	}

	/**
	 * Sets Header text properties such as font, size and color Can be changed
	 * through Constants or Properties file
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	public static Style getDefaultStyleForObjectHeaderText(Diagram diagram) {
		final String styleId = "ODATA-OBJECT-HEADER-TEXT"; //$NON-NLS-1$
		IGaService gaService = Graphiti.getGaService();

		// this is a child style of the common-values-style
		Style parentStyle = getStyleForCommonValues(diagram);
		Style style = gaService.findStyle(parentStyle, styleId);

		if (style == null) { // style not found - create new style
			style = gaService.createPlainStyle(parentStyle, styleId);
			setTextValues(diagram, gaService, style);
		}
		style.setFont(gaService.manageFont(diagram, getFontName(),
				getFontSize(), false, true));
		return style;
	}

	/**
	 * Sets Cardinality text properties such as font, size and color Can be
	 * changed through Constants or Properties file
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	public static Style getDefaultStyleForCardinalityText(Diagram diagram) {
		final String styleId = "ODATA-CARD-TEXT"; //$NON-NLS-1$
		IGaService gaService = Graphiti.getGaService();

		// this is a child style of the common-values-style
		Style parentStyle = getStyleForCommonValues(diagram);
		Style style = gaService.findStyle(parentStyle, styleId);

		if (style == null) { // style not found - create new style
			style = gaService.createPlainStyle(parentStyle, styleId);
			setTextValues(diagram, gaService, style);
		}
		style.setFont(gaService.manageFont(diagram, getFontName(),
				getFontSize(), false, true));
		style.setForeground(gaService.manageColor(diagram,
				IODataEditorConstants.CARD_COLOR_ASSOC));
		return style;
	}

	/**
	 * Sets normal text properties such as font, size and color Can be changed
	 * through Constants or Properties file
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	public static Style getDefaultStyleForText(Diagram diagram) {
		final String styleId = "ODATA-TEXT"; //$NON-NLS-1$
		IGaService gaService = Graphiti.getGaService();

		// this is a child style of the common-values-style
		Style parentStyle = getStyleForCommonValues(diagram);
		Style style = gaService.findStyle(parentStyle, styleId);

		if (style == null) { // style not found - create new style
			style = gaService.createPlainStyle(parentStyle, styleId);
			setTextValues(diagram, gaService, style);
		}
		style.setFont(gaService.manageFont(diagram, getFontName(),
				getFontSize(), false, false));
		return style;
	}

	/**
	 * Sets decorator text properties such as font, size and color Can be
	 * changed through Constants or Properties file
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 * @see org.eclipse.graphiti.mm.pictograms.Diagram
	 * @see org.eclipse.ogee.designer.utils.IODataEditorConstants
	 */
	public static Style getStyleForDecorator(Diagram diagram) {
		final String styleId = "ODATA-DECORATOR-TEXT"; //$NON-NLS-1$
		IGaService gaService = Graphiti.getGaService();

		// this is a child style of the common-values-style
		Style parentStyle = getStyleForCommonValues(diagram);
		Style style = gaService.findStyle(parentStyle, styleId);

		if (style == null) { // style not found - create new style
			style = gaService.createPlainStyle(parentStyle, styleId);
			setTextValues(diagram, gaService, style);
		}
		style.setFont(gaService.manageDefaultFont(diagram, true, false));
		return style;
	}

	/**
	 * Sets normal text properties such as alignment, angle and foreground Can
	 * be changed through Constants or Properties file
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	private static void setTextValues(Diagram diagram, IGaService gaService,
			Style style) {
		style.setFilled(false);
		style.setAngle(0);
		style.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
		style.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
		style.setForeground(gaService.manageColor(diagram,
				IODataEditorConstants.ODATA_TEXT_FOREGROUND));
	}

	/**
	 * Sets normal line properties such as line style, width and transparency
	 * 
	 * @param diagram
	 *            Diagram instance
	 * @return Style which already has default values
	 */
	private static void setValues(Style style) {
		style.setLineStyle(LineStyle.SOLID);
		style.setLineVisible(true);
		style.setLineWidth(2);
		style.setTransparency(0.0);
	}

	private static String getFontName() {
		Font defaultFont = JFaceResources.getDialogFont();
		return defaultFont.getFontData()[0].getName();
	}

	private static int getFontSize() {
		Font defaultFont = JFaceResources.getDialogFont();
		return defaultFont.getFontData()[0].getHeight();
	}
}
