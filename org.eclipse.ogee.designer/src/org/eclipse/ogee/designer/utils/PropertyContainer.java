/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;

/**
 * This class will act as a container for copied Property object and its Key
 * Property
 */
public class PropertyContainer extends EObjectImpl {

	/**
	 * property object
	 */
	private Property property;
	/**
	 * key
	 */
	private boolean isKey;

	/**
	 * schema
	 */
	private Schema schema;

	/**
	 * getter for schema of property Object
	 * 
	 * @return schema
	 */
	public Schema getSchema() {
		return this.schema;
	}

	/**
	 * setter for schema object
	 * 
	 * @param schema
	 */
	public void setSchema(Schema schema) {
		this.schema = schema;
	}

	/**
	 * getter for property object
	 * 
	 * @return property object
	 */
	public Property getProperty() {
		return this.property;
	}

	/**
	 * setter for property object
	 * 
	 * @param property
	 */
	public void setProperty(Property property) {
		this.property = property;
	}

	/**
	 * Returns whether copied property is Key or not
	 * 
	 * @return true if copied property is Key else false
	 */
	public boolean isKey() {
		return this.isKey;
	}

	/**
	 * setter for key
	 * 
	 * @param isKey
	 */
	public void setKey(boolean isKey) {
		this.isKey = isKey;
	}

	/**
	 * Constructor
	 * 
	 * @param prop
	 * @param isKey
	 */
	public PropertyContainer(Property prop, boolean isKey, Schema schema) {
		super();
		this.property = prop;
		this.isKey = isKey;
		this.schema = schema;

	}

}
