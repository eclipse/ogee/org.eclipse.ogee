/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import java.util.Iterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Role;

/**
 * This class is used to update the decorator of the connection depending on the
 * change of the tab property values
 * 
 */
public class ODataDecoratorUtil {

	/**
	 * @param featureProvider
	 *            Feature provider instance is passed
	 * @param navProperty
	 *            Navigation property instance is passed
	 * @param prevAssociation
	 *            Previous association instance is passed
	 * @param newAssociation
	 *            New association instance is passed
	 */
	public static void updateDecorators(final IFeatureProvider featureProvider,
			final NavigationProperty navProperty,
			final Association prevAssociation, final Association newAssociation) {

		// Check if relationship of the Navigation property is updated
		if (prevAssociation != newAssociation) {

			boolean isVisible = false;

			Role fromRole = navProperty.getFromRole();

			PictogramElement[] connectionPE = featureProvider
					.getAllPictogramElementsForBusinessObject(prevAssociation);
			if (prevAssociation != null) {

				// Method is called to delete the decorators for the previous
				// association as isVisible is passed as false
				removeDecorators(featureProvider, prevAssociation, isVisible,
						false, fromRole, connectionPE);

			}

			PictogramElement[] connectionPE1 = featureProvider
					.getAllPictogramElementsForBusinessObject(newAssociation);

			if (newAssociation != null) {

				// Method is called to add the decorators for the updated
				// association
				addDecorators(featureProvider, newAssociation, fromRole,
						connectionPE1);

			}
		}
	}

	/**
	 * To delete the decorators for the previous association as isVisible is
	 * passed as false
	 * 
	 * @param featureProvider
	 *            Feature provider instance is passed
	 * @param prevAssociation
	 *            Previous association instance is passed
	 * @param isVisible
	 *            isVisible instance is passed
	 * @param fromRole
	 *            Source role instance is passed
	 * @param connectionPE
	 *            Pictogram element of the connection instance is passed
	 */
	public static void removeDecorators(final IFeatureProvider featureProvider,
			final Association prevAssociation, boolean isVisible,
			boolean isFromDelete, Role fromRole, PictogramElement[] connectionPE) {

		boolean isSelf = false;
		int count = 0;
		boolean isChanged = true;
		NavigationProperty navProp = null;
		EList<Role> roles = prevAssociation.getEnds();

		if (roles.size() > 1) {
			if (roles.get(0).getType() == roles.get(1).getType()) {
				isSelf = true;
			}
		}

		EList<NavigationProperty> navProps = fromRole.getType()
				.getNavigationProperties();

		// Check all the navigation properties of the source entity type
		for (Iterator<NavigationProperty> iterator = navProps.iterator(); iterator
				.hasNext();) {

			navProp = iterator.next();
			if (navProp.getRelationship() == prevAssociation) {

				if (isFromDelete) {
					count++;

					// If there is only one Navigation property
					// and it is self association, then change the
					// decorator otherwise not
					if (isSelf) {
						if (count > 2) {
							isChanged = false;
						} else {
							isChanged = true;
						}

					} else {
						if (count > 1) {

							isChanged = false;
							break;
						}
					}

				} else {
					++count;
					// If there is only one Navigation property
					// and it is self association, then change the
					// decorator otherwise not
					if (isSelf) {
						if (count > 1) {
							isChanged = false;
							break;
						} else {
							isChanged = true;
						}

					} else if (count >= 1) {
						isChanged = false;
						break;
					}
				}

			}

		}

		if (isChanged) {
			// Method is called to set the decorators invisible
			setArrowInvisible(featureProvider, connectionPE, fromRole,
					isVisible, isSelf);
		}
	}

	/**
	 * Method is called to add the decorators for the updated association
	 * 
	 * @param featureProvider
	 *            Feature provider instance is passed
	 * @param newAssociation
	 *            New association instance is passed
	 * @param fromRole
	 *            Source role instance is passed
	 * @param connectionPE1
	 *            Pictogram element of connection instance is passed
	 */
	private static void addDecorators(final IFeatureProvider featureProvider,
			final Association newAssociation, Role fromRole,
			PictogramElement[] connectionPE1) {

		boolean isVisible;
		int count;
		boolean isChanged;
		EList<Role> roles = newAssociation.getEnds();
		NavigationProperty navProp = null;

		boolean isSelf = false;

		// Check whether it is self-association or not
		if (roles.size() > 1) {
			if (roles.get(0).getType() == roles.get(1).getType()) {
				isSelf = true;
			}
		}
		EList<NavigationProperty> navProps = fromRole.getType()
				.getNavigationProperties();
		isChanged = true;
		count = 0;

		// Check all the navigation properties of the source entity type
		for (Iterator<NavigationProperty> iterator = navProps.iterator(); iterator
				.hasNext();) {

			navProp = iterator.next();
			if (navProp.getRelationship() == newAssociation) {
				count++;
				// If there is only one Navigation property
				// and it is self association, then change the
				// decorator otherwise not
				if (count > 1 && isSelf) {
					isChanged = true;
					break;
				} else if (count > 1) {
					isChanged = false;
				}

			}

		}

		if (isChanged) {

			isVisible = true;
			// Method is called to set the decorators visible
			setArrowInvisible(featureProvider, connectionPE1, fromRole,
					isVisible, isSelf);
		}
	}

	/**
	 * 
	 * To set the decorators invisible
	 * 
	 * @param featureProvider1
	 *            Feature provider instance is passed
	 * @param connectionPE
	 *            Pictogram element of connection instance is passed
	 * @param fromRole
	 *            Source role instance is passed
	 * @param isSelf
	 *            isSelf instance is passed
	 */
	private static void setArrowInvisible(
			final IFeatureProvider featureProvider1,
			PictogramElement[] connectionPE, Role fromRole, boolean isVisible,
			boolean isSelf) {

		PictogramElement pictogramElement2 = connectionPE[0];
		EList<ConnectionDecorator> connectionDecorators = ((Connection) pictogramElement2)
				.getConnectionDecorators();

		for (Iterator<ConnectionDecorator> iterator = connectionDecorators
				.iterator(); iterator.hasNext();) {

			ConnectionDecorator connectionDecorator = iterator.next();
			GraphicsAlgorithm graphicsAlgorithm = connectionDecorator
					.getGraphicsAlgorithm();

			if (graphicsAlgorithm != null) {

				// Check all the GAs of the connection decorators whether
				// it is polyline i.e. arrow head
				Object businessObjectForPictogramElement = featureProvider1
						.getBusinessObjectForPictogramElement(graphicsAlgorithm
								.getPictogramElement());
				Role role = (Role) businessObjectForPictogramElement;
				if (graphicsAlgorithm instanceof Polyline) {
					Polyline new_name = (Polyline) graphicsAlgorithm;
					if (!role.getType().equals(fromRole.getType())) {

						new_name.setLineVisible(Boolean.valueOf(isVisible));
					}

					// If it is self-association
					if (isSelf) {
						if (!isVisible
								&& new_name.getLineVisible().booleanValue()) {
							new_name.setLineVisible(Boolean.valueOf(isVisible));
							break;
						}
						if (isVisible
								&& !new_name.getLineVisible().booleanValue()) {
							new_name.setLineVisible(Boolean.valueOf(isVisible));
							break;
						}
					}

				}
			}

		}
	}

}
