/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.types.AssociationType;
import org.eclipse.ogee.designer.types.MultiplicityType;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IResourceContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.IntegerValue;
import org.eclipse.ogee.model.odata.Multiplicity;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.FunctionImportImpl;
import org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.SimpleTypeUsageImpl;
import org.eclipse.ogee.utils.logger.Logger;

/**
 * This Class contains utility methods for business objects creation.
 * 
 */

public class ArtifactUtil {

	private static final String EMPTY_STRING = ""; //$NON-NLS-1$
	private static final String nonSpclCharacter = "[^a-zA-Z_0-9]"; //$NON-NLS-1$
	private static final String noSplCharsExceptMinus = "[^0-9_-]"; //$NON-NLS-1$
	private static final String onlyCharacter = "[^a-z_]"; //$NON-NLS-1$
	private static final String onlyNumber = "[^0-9]"; //$NON-NLS-1$
	private static final String DELIMITER = "."; //$NON-NLS-1$

	/**
	 * Returns true if the object is EntityType
	 * 
	 * @param currentObject
	 * @return boolean true if the object is EntityType
	 */
	public static boolean isEntityType(Object currentObject) {

		return currentObject instanceof EntityType;
	}

	/**
	 * Returns true if the object is referenced( from an EDMX reference)
	 * EntityType
	 * 
	 * @param currentObject
	 * @return boolean true if the object is referenced EntityType
	 */
	public static boolean isReferencedEntityType(Object currentObject) {

		if (currentObject != null && currentObject instanceof EntityType) {
			try {
				EntityType newEntity = (EntityType) currentObject;

				Schema schema = (Schema) (newEntity.eContainer());
				EDMXSet edmxset = (EDMXSet) (schema.eContainer());
				IResourceContext resourceContext = IModelContext.INSTANCE
						.getResourceContext(edmxset);

				if (newEntity != null) {
					return !(resourceContext.isInMainScope(newEntity));
				}
			} catch (ModelAPIException e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * Returns true if the object is referenced( from an EDMX reference)
	 * EntityType
	 * 
	 * @param currentObject
	 * @return boolean true if the object is referenced EntityType
	 */
	public static boolean isReferencedComplexType(Object currentObject) {

		if (currentObject != null && currentObject instanceof ComplexType) {
			try {
				ComplexType newComplexType = (ComplexType) currentObject;

				Schema schema = (Schema) (newComplexType.eContainer());
				EDMXSet edmxset = (EDMXSet) (schema.eContainer());
				IResourceContext resourceContext = IModelContext.INSTANCE
						.getResourceContext(edmxset);

				if (newComplexType != null) {
					return !(resourceContext
							.isInMainScope((EObject) newComplexType));
				}
			} catch (ModelAPIException e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * Returns true if the object is referenced( from an EDMX reference)
	 * EntityType
	 * 
	 * @param currentObject
	 * @return boolean true if the object is referenced EntityType
	 */
	public static boolean isReferencedEnumType(Object currentObject) {

		if (currentObject != null && currentObject instanceof EnumType) {
			try {
				EnumType newEnumType = (EnumType) currentObject;

				Schema schema = (Schema) (newEnumType.eContainer());
				EDMXSet edmxset = (EDMXSet) (schema.eContainer());
				IResourceContext resourceContext = IModelContext.INSTANCE
						.getResourceContext(edmxset);

				if (newEnumType != null) {
					return !(resourceContext
							.isInMainScope((EObject) newEnumType));
				}
			} catch (ModelAPIException e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * This method returns all the entities which are contained in the schemas
	 * that are referenced by the current model
	 * 
	 * @param parentSchema
	 * @return
	 */
	public static List<EntityType> getReferencedEntities(Schema parentSchema) {
		List<EntityType> referencedEntities = new ArrayList<EntityType>();
		EList<EDMXReference> references = getReferencedSchemas(parentSchema);
		EDMX referencedEDMX = null;
		EList<Schema> schemata = null;
		for (EDMXReference edmxReference : references) {
			referencedEDMX = edmxReference.getReferencedEDMX();
			schemata = referencedEDMX.getDataService().getSchemata();
			for (Schema schema : schemata) {
				referencedEntities.addAll(schema.getEntityTypes());

			}
		}
		return referencedEntities;
	}

	/**
	 * Extracts the schema from the EObject<br>
	 * <b>Do not</b> use this method in case your EObject is not part of the
	 * schema<br>
	 * 
	 * @param eObject
	 * @return
	 */
	public static Schema getSchema(EObject eObject) {
		EObject schema = eObject;
		while (schema != null && !(schema instanceof Schema)) {
			schema = schema.eContainer();
		}
		if (schema instanceof Schema)
			return (Schema) schema;
		else
			return null;

	}

	/**
	 * This method returns all the schemas which are referenced in the given
	 * schema
	 * 
	 * @param parentSchema
	 * @return
	 */
	public static EList<EDMXReference> getReferencedSchemas(Schema parentSchema) {
		EDMXSet edmxset = (EDMXSet) (parentSchema.eContainer());
		EDMX mainEDMX = edmxset.getMainEDMX();
		EList<EDMXReference> references = mainEDMX.getReferences();
		return references;
	}

	/**
	 * Returns true if the object is Role
	 * 
	 * @param currentObject
	 * @return boolean true if the object is Role
	 */
	public static boolean isRole(Object currentObject) {

		return currentObject instanceof Role;
	}

	/**
	 * returns the Entity Name if the Object is of type Entity
	 * 
	 * @param currentObject
	 * @return String Entity Name
	 */
	public static String getEntityTypeName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof EntityType) {
			name = ((EntityType) currentObject).getName();
		}
		return name;
	}

	/**
	 * returns the Enum Name if the Object is of type Enum
	 * 
	 * @param currentObject
	 * @return String Enum Name
	 */
	public static String getEnumTypeName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof EnumType) {
			name = ((EnumType) currentObject).getName();
		}
		return name;
	}

	/**
	 * returns the FunctionImport Name if the Object is of type FunctionImport
	 * 
	 * @param currentObject
	 * @return String FunctionImport Name
	 */
	public static String getFunctionImportName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof FunctionImport) {
			name = ((FunctionImport) currentObject).getName();
		}
		return name;
	}

	/**
	 * returns the EntitySet Name if the Object is of type EntitySet
	 * 
	 * @param currentObject
	 * @return String EntitySet Name
	 */
	public static String getEntitySetName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof EntitySet) {
			name = ((EntitySet) currentObject).getName();
		}
		return name;
	}

	/**
	 * Returns true if the object is Property
	 * 
	 * @param currentObject
	 * @return boolean true if the object is Property
	 */
	public static boolean isProperty(Object currentObject) {

		return currentObject instanceof Property;
	}

	/**
	 * Returns true if the object is Property
	 * 
	 * @param currentObject
	 * @return boolean true if the object is Property
	 */
	public static boolean isEntitySet(Object currentObject) {

		return currentObject instanceof EntitySet;
	}

	/**
	 * Returns true if the object is ComplexType
	 * 
	 * @param currentObject
	 * @return boolean true if the object is ComplexType
	 */
	public static boolean isComplexType(Object currentObject) {

		return currentObject instanceof ComplexType;
	}

	/**
	 * Returns true if the object is EnumType
	 * 
	 * @param currentObject
	 * @return boolean true if the object is EnumType
	 */
	public static boolean isEnumType(Object currentObject) {

		return currentObject instanceof EnumType;
	}

	/**
	 * Returns true if the object is EnumMemberType
	 * 
	 * @param currentObject
	 * @return boolean true if the object is EnumMember
	 */
	public static boolean isEnumMemberType(Object currentObject) {

		return currentObject instanceof EnumMember;
	}

	/**
	 * Returns true if the object is EnumMember
	 * 
	 * @param currentObject
	 * @return boolean true if the object is EnumMember
	 */
	public static boolean isEnumMember(Object currentObject) {
		return currentObject instanceof EnumMember;

	}

	/**
	 * Returns true if the object is ComplexTypeProperty
	 * 
	 * @param currentObject
	 * @return boolean true if the object is ComplexType Property
	 */
	public static boolean isComplexTypeProperty(Object currentObject) {
		boolean status = false;
		if (currentObject instanceof Property) {
			status = ((Property) currentObject).getType() instanceof ComplexTypeUsage;
		}
		return status;
	}

	/**
	 * Returns true if the object is EnumTypeProperty
	 * 
	 * @param currentObject
	 * @return boolean true if the object is EnumType Property
	 */
	public static boolean isEnumTypeProperty(Object currentObject) {
		boolean status = false;
		if (currentObject instanceof Property) {
			status = ((Property) currentObject).getType() instanceof EnumTypeUsage;
		}
		return status;
	}

	/**
	 * returns the Property Name if the Object is of type Property
	 * 
	 * @param currentObject
	 * @return String Property Name
	 */
	public static String getPropertyName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof Property) {
			name = ((Property) currentObject).getName();
		}

		return name;
	}

	/**
	 * returns the Parameter Name if the Object is of type Parameter
	 * 
	 * @param currentObject
	 * @return String Parameter Name
	 */
	public static String getParameterName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof Parameter) {
			name = ((Parameter) currentObject).getName();
		}

		return name;
	}

	/**
	 * returns the EnumMember Name if the Object is of type EnumMember
	 * 
	 * @param currentObject
	 * @return String EnumMember Name
	 */
	public static String getEnumMemberName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof EnumMember) {
			name = ((EnumMember) currentObject).getName();
		}

		return name;
	}

	/**
	 * returns the EnumMember Value if the Object is of type EnumMember
	 * 
	 * @param currentObject
	 * @return String EnumMember Value
	 */
	public static String getEnumMemberValue(Object currentObject) {
		Object value = null;
		String result = EMPTY_STRING;
		if (currentObject instanceof EnumMember) {

			EnumMember enumMember = (EnumMember) currentObject;
			value = enumMember.getValue();
			if (value != null) {
				result = enumMember.getValue() != null ? String
						.valueOf(enumMember.getValue().getValueObject())
						: EMPTY_STRING;
			}
		}

		return result;
	}

	/**
	 * returns the Return Type Name if the Object is of type
	 * IFunctionReturnTypeUsage
	 * 
	 * @param currentObject
	 * @return String IFunctionReturnTypeUsage Name
	 */
	public static String getReturnTypeName(Object currentObject) {
		String name = null;
		if (currentObject instanceof ComplexTypeUsage
				&& ((ComplexTypeUsage) currentObject).getComplexType() != null) {
			name = ((ComplexTypeUsage) currentObject).getComplexType()
					.getName();
		} else if (currentObject instanceof EnumTypeUsage
				&& ((EnumTypeUsage) currentObject).getEnumType() != null) {
			name = ((EnumTypeUsage) currentObject).getEnumType().getName();
		} else if (currentObject instanceof SimpleTypeUsage
				&& ((SimpleTypeUsage) currentObject).getSimpleType() != null) {
			name = ((SimpleTypeUsage) currentObject).getSimpleType().getType()
					.getLiteral();
		} else if (currentObject instanceof ReturnEntityTypeUsage
				&& ((ReturnEntityTypeUsage) currentObject).getEntityType() != null) {
			name = ((ReturnEntityTypeUsage) currentObject).getEntityType()
					.getName();
		}

		return name;
	}

	/**
	 * returns the Underlying Type Name if the Object is of type EnumType
	 * 
	 * @param currentObject
	 * @return String IFunctionReturnTypeUsage Name
	 */
	public static String getUnderlyingTypeName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof EDMTypes) {
			name = ((EDMTypes) currentObject).getName();
		}
		return name;
	}

	/**
	 * returns the Property Name if the Object is of type Property
	 * 
	 * @param currentObject
	 * @return String Property Name
	 */
	public static String getNavPropertyName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof NavigationProperty) {
			name = ((NavigationProperty) currentObject).getName();
		}

		return name;
	}

	/**
	 * Returns true if the object is an Association
	 * 
	 * @param currentObject
	 * @return boolean true if the object is an Association
	 */
	public static boolean isAssociation(Object currentObject) {

		return currentObject instanceof Association;
	}

	/**
	 * Returns true if the object is a FunctionImport
	 * 
	 * @param currentObject
	 * @return boolean true if the object is an FunctionImport
	 */
	public static boolean isFunctionImport(Object currentObject) {

		return currentObject instanceof FunctionImport;
	}

	/**
	 * Returns true if the object is a Parameter
	 * 
	 * @param currentObject
	 * @return boolean true if the object is an FunctionImport
	 */
	public static boolean isParameter(Object currentObject) {

		return currentObject instanceof Parameter;
	}

	/**
	 * Returns true if the object is a Return Type of Function Import
	 * 
	 * @param currentObject
	 * @return boolean true if the object is an FunctionImport
	 */
	public static boolean isFunctionImportReturnType(Object currentObject) {

		return currentObject instanceof IFunctionReturnTypeUsage;
	}

	/**
	 * Returns true if the object is a Return Type of Function Import
	 * 
	 * @param currentObject
	 * @return boolean true if the object is an FunctionImport
	 */
	public static boolean isCollectionReturnType(Object currentObject) {

		boolean isCollection = false;

		if (currentObject instanceof SimpleTypeUsageImpl) {
			isCollection = ((SimpleTypeUsageImpl) currentObject).isCollection();
		} else if (currentObject instanceof ComplexTypeUsageImpl) {
			isCollection = ((ComplexTypeUsageImpl) currentObject)
					.isCollection();
		} else if (currentObject instanceof EnumTypeUsageImpl) {
			isCollection = ((EnumTypeUsageImpl) currentObject).isCollection();
		} else if (currentObject instanceof ReturnEntityTypeUsageImpl) {
			isCollection = ((ReturnEntityTypeUsageImpl) currentObject)
					.isCollection();
		}

		return isCollection;
	}

	/**
	 * Returns true if the object is an Navigation
	 * 
	 * @param currentObject
	 * @return boolean true if the object is an Navigation
	 */
	public static boolean isNavigation(Object currentObject) {

		return currentObject instanceof NavigationProperty;
	}

	/**
	 * returns the Association Name if the Object is of type Association
	 * 
	 * @param currentObject
	 * @return String Association Name
	 */
	public static String getAssociationName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof Association) {
			name = ((Association) currentObject).getName();
		}

		return name;
	}

	/**
	 * @param name
	 * @return the error message if the validation of the entity name fails
	 */
	public static String validateName(String name) {

		String errorMsg = null;
		boolean b = false;
		boolean b1 = false;

		Pattern nonSpclChar = Pattern.compile(nonSpclCharacter,
				Pattern.CASE_INSENSITIVE);
		Matcher m = nonSpclChar.matcher(name);
		b = m.find();

		Pattern onlyChar = Pattern.compile(onlyCharacter,
				Pattern.CASE_INSENSITIVE);
		if (name.trim().length() > 0) {
			Matcher m1 = onlyChar.matcher(name.substring(0, 1));
			b1 = m1.find();
		}
		if (name.trim().length() > 128) {
			errorMsg = Messages.ODATAEDITOR_ENTITYTYPE_NAMELENGTH_MSG;
		}
		if (name.trim().length() < 1) {
			errorMsg = Messages.ODATAEDITOR_ENTITYTYPE_EMPTYNAME_MSG;
		}
		if (b1) {
			errorMsg = Messages.ODATAEDITOR_FIRSTLETTER_CHAR_MSG;
		} else if (b) {
			errorMsg = Messages.ODATAEDITOR_SPECIAL_CHAR_MSG;
		}

		return errorMsg;
	}

	/**
	 * @param name
	 * @return the error message if the validation of the enum member value
	 *         fails
	 */
	public static String validateMemberValue(EnumMember enumMember, String value) {

		String errorMsg = null;
		boolean b = false;
		boolean b1 = false;

		Pattern nonSpclCharExceptMinus = Pattern.compile(noSplCharsExceptMinus,
				Pattern.CASE_INSENSITIVE);
		Matcher m = nonSpclCharExceptMinus.matcher(value);
		b = m.find();

		Pattern onlyNumbers = Pattern.compile(onlyNumber,
				Pattern.CASE_INSENSITIVE);

		if (value.trim().length() > 0) {
			if (value.substring(0, 1).equalsIgnoreCase("-")) { //$NON-NLS-1$
				Matcher m1 = onlyNumbers.matcher(value.substring(1,
						value.length()));
				b1 = m1.find();
				if (!b && !b1) {
					if (value.length() > 20) {
						errorMsg = Messages.ODATAEDITOR_MEMBER_MAXLENGTH_MSG_VALUE;
					}
				}
			} else {
				Matcher m2 = onlyNumbers.matcher(value);
				b1 = m2.find();
				if (!b1) {
					if (value.length() > 19) {
						errorMsg = Messages.ODATAEDITOR_MEMBER_MAXLENGTH_MSG_VALUE;
					}
				}
			}

		}

		if (b1) {
			errorMsg = Messages.ODATAEDITOR_NUMERIC_MSG_VALUE;
		} else if (b) {
			errorMsg = Messages.ODATAEDITOR_SPECIAL_CHAR_MSG_VALUE;
		}
		if (errorMsg == null) {
			EnumType enumType = (EnumType) enumMember.eContainer();
			if (value.trim().length() > 0) {
				Object enumMemeberValue = validateEnumMemeberValue(
						enumType.getUnderlyingType(), value);
				if (enumMemeberValue == null) {
					errorMsg = Messages.ODATAEDITOR_RANGE_MSG_VALUE;
				}
			}

		}

		return errorMsg;
	}

	public static String getAliasName(Schema currentSchema,
			Schema referencedSchema) {
		String aliasName = null;
		EList<Using> usings = currentSchema.getUsings();
		for (Using using : usings) {
			if (referencedSchema.equals(using.getUsedNamespace())) {
				aliasName = using.getAlias();
				return aliasName;
			}

		}
		return referencedSchema.getNamespace();
	}

	public static String getArtifactDisplayName(
			ArtifactFactory artifactFactory, EObject currentObject) {

		StringBuffer name = new StringBuffer();
		if (isReferencedEntityType(currentObject)
				|| isReferencedComplexType(currentObject)
				|| isReferencedEnumType(currentObject)) {
			Schema schema = getSchema(currentObject);
			if (schema != null) {
				String alias = getAliasName(artifactFactory.getSchema(), schema);
				if (alias != null && alias.length() > 0) {
					name.append(alias);
					name.append(DELIMITER);
				}
			}
		}
		name.append(getInitialName(currentObject));
		return name.toString();

	}

	/**
	 * Get the name depending on the type of the object Currently it is valid
	 * for entity name entity set and property
	 * 
	 * @param obj
	 * @return String
	 */
	public static String getInitialName(Object obj) {

		String name = null;

		if (ArtifactUtil.isEntityType(obj)) {
			EntityType etype = (EntityType) obj;
			name = etype.getName();
		} else if (ArtifactUtil.isEntitySet(obj)) {

			EntitySet eset = (EntitySet) obj;
			name = eset.getName();

		} else if (ArtifactUtil.isProperty(obj)) {

			Property entityProperty = (Property) obj;
			name = entityProperty.getName();

		} else if (ArtifactUtil.isNavigation(obj)) {

			NavigationProperty navProperty = (NavigationProperty) obj;
			name = navProperty.getName();

		} else if (ArtifactUtil.isComplexType(obj)) {
			ComplexType ctype = (ComplexType) obj;
			name = ctype.getName();
		} else if (ArtifactUtil.isEnumType(obj)) {

			EnumType enumType = (EnumType) obj;
			name = enumType.getName();

		} else if (ArtifactUtil.isEnumMember(obj)) {

			EnumMember enumMember = (EnumMember) obj;
			name = enumMember.getName();

		} else if (ArtifactUtil.isFunctionImport(obj)) {

			FunctionImport funcImp = (FunctionImport) obj;
			name = funcImp.getName();
		} else if (ArtifactUtil.isRole(obj)) {
			// multiplicity of Role for Connection Decorators
			Role role = (Role) obj;
			name = role.getMultiplicity().getLiteral();

		} else if (ArtifactUtil.isParameter(obj)) {

			Parameter param = (Parameter) obj;
			name = param.getName();

		} else if (ArtifactUtil.isFunctionImportReturnType(obj)) {
			if (obj instanceof SimpleTypeUsage) {
				name = ((SimpleTypeUsage) obj).getSimpleType().getType()
						.getLiteral();
			} else if (obj instanceof ComplexTypeUsage) {
				if (((ComplexTypeUsage) obj).getComplexType() != null)
					name = ((ComplexTypeUsage) obj).getComplexType().getName();
			} else if (obj instanceof ReturnEntityTypeUsage) {
				if (((ReturnEntityTypeUsage) obj).getEntityType() != null)
					name = ((ReturnEntityTypeUsage) obj).getEntityType()
							.getName();
			} else {
				if (((EnumTypeUsage) obj).getEnumType() != null)
					name = ((EnumTypeUsage) obj).getEnumType().getName();
			}
		} else {
			name = null;
		}

		return name;
	}

	/**
	 * Truncate the Default Name of the artifact to 40 characters and remove the
	 * special characters from the default name so that the name is a valid one
	 * 
	 * @param name
	 * @return String valid name
	 */
	public static String getValidDefaultName(String name) {

		name = name.replaceAll(nonSpclCharacter, EMPTY_STRING);
		return name;
	}

	/**
	 * For the fields for which editing is required
	 * 
	 * @param obj
	 * @return boolean
	 */
	public static boolean isEditable(Object obj) {

		return (ArtifactUtil.isEntityType(obj) || ArtifactUtil.isProperty(obj)
				|| ArtifactUtil.isEntitySet(obj)
				|| ArtifactUtil.isNavigation(obj)
				|| ArtifactUtil.isComplexType(obj)
				|| ArtifactUtil.isEnumType(obj)
				|| ArtifactUtil.isEnumMemberType(obj)
				|| ArtifactUtil.isFunctionImport(obj)
				|| ArtifactUtil.isRole(obj) || ArtifactUtil.isParameter(obj) || ArtifactUtil
					.isFunctionImportReturnType(obj));
	}

	/**
	 * Get Entity Types of the Association
	 * 
	 * @param Association
	 * @return EList<EntityType>
	 */
	public static List<EntityType> getEntityTypesForAssociation(
			Association association) {
		List<EntityType> entityTypes = new ArrayList<EntityType>();
		Role assocEnd1 = association.getEnds().get(0);
		Role assocEnd2 = association.getEnds().get(1);
		if (assocEnd1 != null) {
			entityTypes.add(assocEnd1.getType());
		}
		if (assocEnd2 != null) {
			entityTypes.add(assocEnd2.getType());
		}
		return entityTypes;
	}

	/**
	 * Get NavigationProperty for the Association
	 * 
	 * @param association
	 * 
	 * @return EList<EntityType>
	 */
	public static List<NavigationProperty> getNavigationPropertyForAssociation(
			Association association) {

		List<NavigationProperty> navProperties = new ArrayList<NavigationProperty>();
		EntityType entityType1 = getEntityTypesForAssociation(association).get(
				0);
		EntityType entityType2 = getEntityTypesForAssociation(association).get(
				1);
		List<NavigationProperty> navPropertyChildren;

		if (entityType1 != null) {
			navPropertyChildren = entityType1.getNavigationProperties();
			for (NavigationProperty navProperty : navPropertyChildren) {
				if (navProperty.getRelationship() == association) {
					navProperties.add(navProperty);
				}
			}
		}
		if (entityType2 != null) {
			navPropertyChildren = null;
			navPropertyChildren = entityType2.getNavigationProperties();
			for (NavigationProperty navProperty : navPropertyChildren) {
				if (navProperty.getRelationship() == association) {
					navProperties.add(navProperty);
				}
			}
		}
		return navProperties;
	}

	/**
	 * @param currentObject
	 * @return String
	 */
	public static String getDeleteMessage(Object currentObject) {
		String message = null;
		if (currentObject instanceof EntityType) {
			message = Messages.DEFAULTDELETEFEATURE_ENTITY_MSG;
		} else if (currentObject instanceof Association) {
			message = Messages.DEFAULTDELETEFEATURE_ASSOC_MSG;
		}
		return message;

	}

	/**
	 * Returns the representation of the model object.
	 * 
	 * @param edmxSet
	 * @return Schema object
	 */
	public static Schema getDefaultSchema(final EDMXSet edmxSet) {

		EList<SchemaClassifier> schemaClassifiers;
		Schema modelSchema = null;

		if (edmxSet != null && edmxSet.getMainEDMX() != null) {
			final DataService odataService = edmxSet.getMainEDMX()
					.getDataService();
			if (odataService != null) {
				final EList<Schema> schemaList = odataService.getSchemata();
				for (Schema schema : schemaList) {
					schemaClassifiers = schema.getClassifiers();
					for (SchemaClassifier schemaClassifier : schemaClassifiers) {
						if (schemaClassifier != null
								&& SchemaClassifier.SERVICE_VALUE == schemaClassifier
										.getValue()) {
							modelSchema = schema;
							break;
						}
					}
				}
			}
		}

		return modelSchema;
	}

	/**
	 * Returns the representation of the model object.
	 * 
	 * @param edmxSet
	 * @return List of Schema objects.
	 */
	public static List<Schema> getSchemas(final EDMXSet edmxSet) {

		EList<SchemaClassifier> schemaClassifiers;
		final List<Schema> schemas = new ArrayList<Schema>();

		if (edmxSet != null && edmxSet.getMainEDMX() != null) {
			final DataService odataService = edmxSet.getMainEDMX()
					.getDataService();
			if (odataService != null) {
				final EList<Schema> schemaList = odataService.getSchemata();
				for (Schema schema : schemaList) {
					schemaClassifiers = schema.getClassifiers();
					for (SchemaClassifier schemaClassifier : schemaClassifiers) {
						if (schemaClassifier != null
								&& SchemaClassifier.SERVICE_VALUE == schemaClassifier
										.getValue()) {
							schemas.add(schema);
						}
					}
				}
			}
		}

		return schemas;
	}

	/**
	 * Returns default Entity Container which holds all entity sets.
	 * 
	 * @return Entity Container
	 */
	public static EntityContainer getDefaultEntityContainer(
			final EDMXSet edmxSet) {
		final DataService odataService = edmxSet.getMainEDMX().getDataService();
		final EList<Schema> schemaList = odataService.getSchemata();
		for (Schema schema : schemaList) {
			List<EntityContainer> containers = schema.getContainers();
			for (EntityContainer entityContainer : containers) {
				if (entityContainer.isDefault()) {
					return entityContainer;
				}
			}
		}
		return null;

	}

	/**
	 * Creates a unique name for the new Entity
	 * 
	 * @return String new unique name
	 */
	/*
	 * public static String getNameForEntity(Schema schema) {
	 * 
	 * List<EntityType> entities = schema.getEntityTypes(); int uniqueEntityID =
	 * 1; String entityName; StringBuffer newName = new
	 * StringBuffer(Messages.ODATAEDITOR_ENTITY); List<Integer> numberList = new
	 * ArrayList<Integer>(); EntityType entityType; String entityNumber;
	 * StringTokenizer st; int index; for (Iterator<EntityType> iterator =
	 * entities.iterator(); iterator .hasNext();) { entityType =
	 * iterator.next(); entityName = entityType.getName(); if
	 * (entityName.startsWith(Messages.ODATAEDITOR_ENTITY)) { st = new
	 * StringTokenizer(entityName, Messages.ODATAEDITOR_ENTITY); if
	 * (entityName.equalsIgnoreCase(Messages.ODATAEDITOR_ENTITY)) {
	 * numberList.add(1); } while (st.hasMoreTokens()) { entityNumber =
	 * st.nextToken(); if (entityNumber != null && entityNumber.trim().length()
	 * > 0) { try { index = Integer.parseInt(entityNumber);
	 * numberList.add(index); } catch (NumberFormatException e) { //
	 * Digest...don't log } } } } }
	 * 
	 * for (Iterator<Integer> iterator = numberList.iterator(); iterator
	 * .hasNext();) { if (!numberList.contains(uniqueEntityID)) {
	 * 
	 * if (uniqueEntityID != 1) { newName.append(uniqueEntityID);
	 * 
	 * } break; } uniqueEntityID++;
	 * 
	 * } return newName.toString(); }
	 */
	public static String getNameForEntity(Schema schema) {
		List<String> names = new ArrayList<String>();
		String newEntityName = null;
		names = getEntTypeCmplxTypeAssocNames(schema);
		newEntityName = ArtifactNameUtil.generateUniqueName(names,
				Messages.ODATAEDITOR_ENTITYTYPE_DEFAULTNAME,
				IODataEditorConstants.MAX_LENGTH);
		return newEntityName;

	}

	/**
	 * Creates a unique name for the new FunctionImport
	 * 
	 * @return String new unique name
	 */
	public static String getNameForFunctionImport(Schema schema) {
		List<String> funcImportNames = new ArrayList<String>();
		funcImportNames = getEntSetAssocSetFuncImpNames(schema);
		String newFunctionImportName = ArtifactNameUtil.generateUniqueName(
				funcImportNames,
				Messages.ODATAEDITOR_FUNCTION_IMPORT_DEFAULT_NAME,
				IODataEditorConstants.MAX_LENGTH);
		return newFunctionImportName;

	}

	/*
	 * public static String getNameForFunctionImport(Schema schema) {
	 * 
	 * List<FunctionImport> functionImports = null; if (schema != null &&
	 * schema.getContainers().size() > 0) {
	 * 
	 * functionImports = ((EntityContainer) schema.getContainers().get(0))
	 * .getFunctionImports(); }
	 * 
	 * int uniqueFunctionImportID = 1; String functionImporName; StringBuffer
	 * newName = new StringBuffer(
	 * Messages.ODATAEDITOR_FUNCTION_IMPORT_DEFAULT_NAME); List<Integer>
	 * numberList = new ArrayList<Integer>(); FunctionImport functionImport;
	 * StringTokenizer st; String functionImportNumber; int index; if
	 * (functionImports != null) { for (Iterator<FunctionImport> iterator =
	 * functionImports.iterator(); iterator .hasNext();) { functionImport =
	 * iterator.next(); functionImporName = functionImport.getName(); if
	 * (functionImporName
	 * .startsWith(Messages.ODATAEDITOR_FUNCTION_IMPORT_DEFAULT_NAME)) { st =
	 * new StringTokenizer(functionImporName,
	 * Messages.ODATAEDITOR_FUNCTION_IMPORT_DEFAULT_NAME); if (functionImporName
	 * .equalsIgnoreCase(Messages.ODATAEDITOR_FUNCTION_IMPORT_DEFAULT_NAME)) {
	 * numberList.add(1); } while (st.hasMoreTokens()) { functionImportNumber =
	 * st.nextToken(); if (functionImportNumber != null &&
	 * functionImportNumber.trim().length() > 0) { try { index =
	 * Integer.parseInt(functionImportNumber); numberList.add(index); } catch
	 * (NumberFormatException e) { // Digest...don't log } } } } } } for
	 * (Iterator<Integer> iterator = numberList.iterator(); iterator
	 * .hasNext();) { if (!numberList.contains(uniqueFunctionImportID)) { if
	 * (uniqueFunctionImportID != 1) { newName.append(uniqueFunctionImportID); }
	 * break; } uniqueFunctionImportID++; } return newName.toString(); }
	 */

	/**
	 * Creates a unique name for the new EnumType
	 * 
	 * @return String new unique name
	 */
	public static String getNameForEnumType(Schema schema) {

		List<EnumType> enumTypes = null;
		enumTypes = schema.getEnumTypes();

		int uniqueEnumTypeID = 1;
		String enumTypeName;
		StringBuffer newName = new StringBuffer(
				Messages.ODATAEDITOR_ENUMTYPE_DEFAULTNAME);
		List<Integer> numberList = new ArrayList<Integer>();

		if (enumTypes != null) {
			EnumType enumType;
			StringTokenizer st;
			String enumTypeNumber;
			int index;
			for (Iterator<EnumType> iterator = enumTypes.iterator(); iterator
					.hasNext();) {
				enumType = iterator.next();
				enumTypeName = enumType.getName();
				if (enumTypeName
						.startsWith(Messages.ODATAEDITOR_ENUMTYPE_DEFAULTNAME)) {
					st = new StringTokenizer(enumTypeName,
							Messages.ODATAEDITOR_ENUMTYPE_DEFAULTNAME);
					if (enumTypeName
							.equalsIgnoreCase(Messages.ODATAEDITOR_ENUMTYPE_DEFAULTNAME)) {
						numberList.add(1);
					}
					while (st.hasMoreTokens()) {
						enumTypeNumber = st.nextToken();
						if (enumTypeNumber != null
								&& enumTypeNumber.trim().length() > 0) {
							try {
								index = Integer.parseInt(enumTypeNumber);
								numberList.add(index);
							} catch (NumberFormatException e) {
								// Digest...don't log
							}
						}
					}
				}
			}
		}
		for (Iterator<Integer> iterator = numberList.iterator(); iterator
				.hasNext();) {
			if (!numberList.contains(uniqueEnumTypeID)) {
				if (uniqueEnumTypeID != 1) {
					newName.append(uniqueEnumTypeID);
				}
				break;
			}
			uniqueEnumTypeID++;
		}
		return newName.toString();
	}

	/**
	 * Sets the multiplicity to a given role
	 * 
	 * @param role
	 * @param multiplicityType
	 * @return
	 */
	public static Role setMultiplicty(Role role,
			MultiplicityType multiplicityType) {

		switch (multiplicityType) {
		case ONE:
			role.setMultiplicity(Multiplicity.ONE);
			break;
		case MANY:
			role.setMultiplicity(Multiplicity.MANY);
			break;
		case ZEROTOONE:
			role.setMultiplicity(Multiplicity.ZERO_TO_ONE);
			break;
		default:
			break;
		}

		return role;
	}

	/**
	 * 
	 * To get the proper name proposal for the association If the both the enity
	 * names less than 20, concatenate If one of them is greater than 20,
	 * truncate depending on the length of the other entity name
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	/*
	 * public static String getAssociationName(EntityType source, EntityType
	 * target, Schema schema) { String name = null; name = source.getName() +
	 * target.getName(); EList<Association> associations =
	 * schema.getAssociations(); name = getNameProposal(name, associations);
	 * 
	 * return name; }
	 */
	public static String getAssociationName(EntityType source,
			EntityType target, Schema schema) {

		List<String> associationNames = new ArrayList<String>();
		associationNames = getEntTypeCmplxTypeAssocNames(schema);
		int source_len = source.getName().length();
		int target_len = target.getName().length();

		String newAssociationName = null, trunc_source = null, trunc_target = null;

		int total_len = source_len + target_len;
		int offset, x;
		if (total_len > IODataEditorConstants.MAX_LENGTH) { // truncate from
															// longer
			// name till both
			// lengths are equal OR
			// MAX_LENGTH is reached
			offset = total_len - IODataEditorConstants.MAX_LENGTH;
			if (source_len > target_len) {
				// source is greater than target
				source_len -= offset;
			} else if (source_len < target_len) {
				// target is greater than source
				target_len -= offset;
			} else {
				// both are equal
				x = offset / 2;
				source_len -= x;
				target_len -= x;
			}
			trunc_source = source.getName().substring(0, source_len);
			trunc_target = target.getName().substring(0, target_len);
			newAssociationName = trunc_source + trunc_target;

		} else {
			newAssociationName = source.getName() + target.getName();
		}

		newAssociationName = ArtifactNameUtil.generateUniqueName(
				associationNames, newAssociationName,
				IODataEditorConstants.MAX_LENGTH);
		associationNames.add(newAssociationName);
		return newAssociationName;
	}

	/**
	 * this method will check whether current Object is key property or not.
	 * 
	 * @param currentObject
	 * @return isKeyproperty or not
	 */
	public static boolean isKeyProperty(Object currentObject) {
		boolean status = false;
		if (currentObject instanceof Property) {
			Property currentProperty = (Property) currentObject;
			EObject parentContainer = currentProperty.eContainer();
			if (parentContainer instanceof EntityType) {
				EntityType parentEntity = (EntityType) parentContainer;
				status = parentEntity.getKeys().contains(currentProperty);
			}
		}
		return status;
	}

	/**
	 * @param name
	 * @param uniqueEntitySetID
	 * @param numberList
	 * @param associations
	 * @return
	 */
	/*
	 * public static String getNameProposal(String name, EList<Association>
	 * associations) {
	 * 
	 * int uniqueEntitySetID = 1; List<Integer> numberList = new
	 * ArrayList<Integer>(); Association prevAssociation; String
	 * prevAssociationName; int indx; String entitySetsubString; String
	 * eSetNumber; int index;
	 * 
	 * for (Iterator<Association> iterator = associations.iterator(); iterator
	 * .hasNext();) { prevAssociation = iterator.next(); prevAssociationName =
	 * prevAssociation.getName(); if
	 * (name.equalsIgnoreCase(prevAssociationName)) { numberList.add(1);
	 * continue; } if (prevAssociationName.startsWith(name)) { indx =
	 * prevAssociationName.indexOf(name); if (indx != -1) { entitySetsubString =
	 * prevAssociationName.substring(indx + name.length()); if
	 * (entitySetsubString.trim().length() > 0) { eSetNumber =
	 * entitySetsubString; try { index = Integer.parseInt(eSetNumber);
	 * numberList.add(index); } catch (NumberFormatException e) { //
	 * Digest...don't log } } } } } for (Iterator<Integer> iterator1 =
	 * numberList.iterator(); iterator1 .hasNext();) { if
	 * (!numberList.contains(uniqueEntitySetID)) { if (uniqueEntitySetID != 1) {
	 * name = name + uniqueEntitySetID; } break; } uniqueEntitySetID++; } return
	 * name; }
	 */

	/**
	 * Returns a unique name for the Navigation Property
	 * 
	 * @param targetEntityName
	 * @param targetMultiplicity
	 * @param associationtype
	 * @return
	 */
	public static String getNavigationPropertyName(EntityType targetEntity,
			MultiplicityType targetMultiplicity, AssociationType associationtype) {

		int count = 0;
		String suffixN = "N"; //$NON-NLS-1$
		String suffix1 = "1"; //$NON-NLS-1$
		String targetEntityName = targetEntity.getName();
		String navPropName = targetEntityName;
		EList<NavigationProperty> navProps = targetEntity
				.getNavigationProperties();
		String npn = null;
		List<String> navigationPropertyNames = new ArrayList<String>();
		for (ListIterator<NavigationProperty> it = navProps.listIterator(); it
				.hasNext();) {
			NavigationProperty np = it.next();
			npn = np.getName();
			navigationPropertyNames.add(npn);
		}

		String targetEntityNewName = targetEntityName;
		if (MultiplicityType.MANY == targetMultiplicity) {
			if (associationtype == AssociationType.SELF) {
				navPropName = targetEntityNewName + suffixN;
				navigationPropertyNames.add(navPropName + count);
				navPropName = ArtifactNameUtil.generateUniqueName(
						navigationPropertyNames, targetEntityNewName,
						(targetEntityNewName + suffixN), suffixN,
						IODataEditorConstants.MAX_LENGTH);
			} else {
				navPropName = targetEntityNewName
						+ IODataEditorConstants.DEFAULT_NAME_SUFFIX;
				navigationPropertyNames.add(navPropName + count);
				navPropName = ArtifactNameUtil
						.generateUniqueName(
								navigationPropertyNames,
								targetEntityNewName,
								(targetEntityNewName + IODataEditorConstants.DEFAULT_NAME_SUFFIX),
								IODataEditorConstants.DEFAULT_NAME_SUFFIX,
								IODataEditorConstants.MAX_LENGTH);
			}
		} else if (MultiplicityType.ONE == targetMultiplicity) {
			if (associationtype == AssociationType.SELF) {
				navPropName = targetEntityNewName + suffix1;
				navigationPropertyNames.add(navPropName + count);
				navPropName = ArtifactNameUtil.generateUniqueName(
						navigationPropertyNames, targetEntityNewName,
						(targetEntityNewName + suffix1), suffix1,
						IODataEditorConstants.MAX_LENGTH);
			}
		}

		navPropName = navPropName.replaceAll(nonSpclCharacter, EMPTY_STRING);
		return navPropName;
	}

	/**
	 * @param associationType
	 * @param multiplicityType
	 * @param entityName
	 * @param navPropName
	 * @param navProps
	 * @return String
	 */
	public static String getNavPropNameProposal(
			MultiplicityType multiplicityType, AssociationType associationType,
			String entityName, String navPropName,
			EList<NavigationProperty> navProps) {
		// when targetEntity == null
		String npn = null;
		String newNavPropName = navPropName;
		List<String> navigationPropertyNames = new ArrayList<String>();
		for (ListIterator<NavigationProperty> it = navProps.listIterator(); it
				.hasNext();) {
			NavigationProperty np = it.next();
			npn = np.getName();
			navigationPropertyNames.add(npn);
		}
		newNavPropName = ArtifactNameUtil.generateUniqueName(
				navigationPropertyNames, newNavPropName,
				IODataEditorConstants.MAX_LENGTH);
		navigationPropertyNames.add(newNavPropName);
		return newNavPropName;

	}

	/*
	 * public static String getNavPropNameProposal( MultiplicityType
	 * multiplicityType, AssociationType associationType, String entityName,
	 * String navPropName, EList<NavigationProperty> navProps) {
	 * 
	 * final String onlyCharacter = "[a-zA-Z]"; //$NON-NLS-1$ int
	 * uniqueEntitySetID = 1; List<Integer> numberList = new
	 * ArrayList<Integer>(); NavigationProperty prevNavProp; String
	 * prevNavPropName = null; int indx; String entitySetsubString = null;
	 * String eSetNumber; int index; boolean b = false; String navPropNewName =
	 * navPropName;
	 * 
	 * Pattern nonChar = Pattern.compile(onlyCharacter,
	 * Pattern.CASE_INSENSITIVE);
	 * 
	 * if (navProps.size() > 0) {
	 * 
	 * for (Iterator<NavigationProperty> iterator = navProps.iterator();
	 * iterator .hasNext();) { prevNavProp = iterator.next(); prevNavPropName =
	 * prevNavProp.getName(); if (navPropName.equalsIgnoreCase(prevNavPropName))
	 * { numberList.add(1); continue; }
	 * 
	 * if (prevNavPropName.startsWith(entityName)) { indx =
	 * prevNavPropName.indexOf(entityName); if (indx != -1) {
	 * 
	 * try { entitySetsubString = prevNavPropName.substring(indx +
	 * navPropName.length()); } catch (Exception e1) { e1.printStackTrace(); }
	 * 
	 * if (entitySetsubString != null) {
	 * 
	 * Matcher m = nonChar.matcher(entitySetsubString); b = m.find();
	 * 
	 * if (entitySetsubString.trim().length() > 0 && b == false) { eSetNumber =
	 * entitySetsubString; try { index = Integer.parseInt(eSetNumber); // Added
	 * 1 to get next value to calculate // the part of the entity name
	 * numberList.add(Integer.valueOf(index)); } catch (NumberFormatException e)
	 * { // Digest...don't log } } }
	 * 
	 * } } }
	 * 
	 * for (Iterator<Integer> iterator1 = numberList.iterator(); iterator1
	 * .hasNext();) { if
	 * (!numberList.contains(Integer.valueOf(uniqueEntitySetID))) { if
	 * (uniqueEntitySetID != 1) { navPropNewName = navPropName +
	 * uniqueEntitySetID; } break; } uniqueEntitySetID++; }
	 * 
	 * } return navPropNewName;
	 * 
	 * }
	 */

	/**
	 * Returns list of Entity sets related to one Entity.
	 * 
	 * @param entityType
	 * @param entityContainer
	 * @return List of EntitySets
	 */
	public static List<EntitySet> getEntitySets(EntityType entityType,
			EntityContainer entityContainer) {
		List<EntitySet> entitySetList = new ArrayList<EntitySet>();
		EList<EntitySet> entitySets = entityContainer.getEntitySets();
		for (EntitySet entitySet : entitySets) {
			if (entityType == entitySet.getType()) {
				entitySetList.add(entitySet);
			}
		}
		return entitySetList;
	}

	/**
	 * Creates a unique name for the new Complex Type
	 * 
	 * @param schema
	 * 
	 * @return String new unique name
	 */
	public static String getNameForComplexType(Schema schema) {
		List<String> complexTypeNames = new ArrayList<String>();
		String complexTypeName = null;
		complexTypeNames = getEntTypeCmplxTypeAssocNames(schema);
		complexTypeName = ArtifactNameUtil.generateUniqueName(complexTypeNames,
				Messages.ODATAEDITOR_COMPLEXTYPE_DEFAULTNAME,
				IODataEditorConstants.MAX_LENGTH);
		return complexTypeName;
	}

	/*
	 * public static String getNameForComplexType(Schema schema) {
	 * 
	 * List<ComplexType> complexTypes = schema.getComplexTypes(); int
	 * uniqueComplexTypeID = 1; String complexTypeName; StringBuffer newName =
	 * new StringBuffer( Messages.ODATAEDITOR_COMPLEXTYPE_DEFAULTNAME);
	 * List<Integer> numberList = new ArrayList<Integer>(); ComplexType
	 * complexType; StringTokenizer st; String complexTypeNumber; int index; for
	 * (Iterator<ComplexType> iterator = complexTypes.iterator(); iterator
	 * .hasNext();) { complexType = iterator.next(); complexTypeName =
	 * complexType.getName(); if (complexTypeName
	 * .startsWith(Messages.ODATAEDITOR_COMPLEXTYPE_DEFAULTNAME)) { st = new
	 * StringTokenizer(complexTypeName,
	 * Messages.ODATAEDITOR_COMPLEXTYPE_DEFAULTNAME); if (complexTypeName
	 * .equalsIgnoreCase(Messages.ODATAEDITOR_COMPLEXTYPE_DEFAULTNAME)) {
	 * numberList.add(1); } while (st.hasMoreTokens()) { complexTypeNumber =
	 * st.nextToken(); if (complexTypeNumber != null &&
	 * complexTypeNumber.trim().length() > 0) { try { index =
	 * Integer.parseInt(complexTypeNumber); numberList.add(index); } catch
	 * (NumberFormatException e) { // Digest...don't log } } } } } for
	 * (Iterator<Integer> iterator = numberList.iterator(); iterator
	 * .hasNext();) { if (!numberList.contains(uniqueComplexTypeID)) { if
	 * (uniqueComplexTypeID != 1) { newName.append(uniqueComplexTypeID); }
	 * break; } uniqueComplexTypeID++; } return newName.toString(); }
	 */

	/**
	 * returns the Complex Type Name if the Object is of type Complex Type
	 * 
	 * @param currentObject
	 * @return String Complex Type Name
	 */
	public static String getComplexTypeName(Object currentObject) {
		String name = EMPTY_STRING;
		if (currentObject instanceof ComplexType) {
			name = ((ComplexType) currentObject).getName();
		}
		return name;
	}

	/**
	 * The following method returns the selectable Domain Object.
	 * 
	 * @param eObject
	 *            - EObject, the domain object.
	 * @return EObject - the corresponding selectable object.
	 */
	public static EObject getSelectableObject(final EObject eObject) {

		EObject selectableObject = null;

		// Selectable Domain Object for ReferentialConstraint is Association.
		if (eObject instanceof ReferentialConstraint) {
			final ReferentialConstraint refConst = (ReferentialConstraint) eObject;
			if (refConst != null) {
				selectableObject = refConst.eContainer();
			}
		}
		// Selectable Domain Object for AssociationSet is Association.
		else if (eObject instanceof AssociationSet) {
			final AssociationSet assocSet = (AssociationSet) eObject;
			if (assocSet != null) {
				selectableObject = assocSet.getAssociation();
			}
		}
		// Selectable Domain Object for AssociationSetEnd is Association.
		else if (eObject instanceof AssociationSetEnd) {
			final AssociationSetEnd assocSetEnd = (AssociationSetEnd) eObject;
			if (assocSetEnd != null) {
				final AssociationSet assocSet = (AssociationSet) assocSetEnd
						.eContainer();
				if (assocSet != null) {
					selectableObject = assocSet.getAssociation();
				}
			}
		}
		// Selectable Domain Object for Role is Association.
		else if (eObject instanceof Role) {
			final Role role = (Role) eObject;
			if (role != null) {
				selectableObject = role.eContainer();
			}
		}
		// Selectable Domain Object for ComplexTypeUsage is corresponding
		// Property.
		else if (eObject instanceof ComplexTypeUsage) {
			final ComplexTypeUsage cmplxTypUsg = (ComplexTypeUsage) eObject;
			if (cmplxTypUsg != null) {
				if (cmplxTypUsg.eContainer() instanceof FunctionImportImpl) {
					selectableObject = eObject;
				} else {
					selectableObject = cmplxTypUsg.eContainer();
				}
			}
		}
		// Selectable Domain Object for EnumTypeUsage is corresponding
		// Property.
		else if (eObject instanceof EnumTypeUsage) {
			final EnumTypeUsage enumTypUsg = (EnumTypeUsage) eObject;
			if (enumTypUsg != null) {
				selectableObject = enumTypUsg.eContainer();
			}
		}
		// The eObject is selectable object.
		else {
			selectableObject = eObject;
		}

		return selectableObject;
	}

	/**
	 * Returns a list of OData artifacts in the model.
	 * 
	 * @param edmxSet
	 *            EDMXSet
	 * @return OData artifacts in the model.
	 */
	public static List<EObject> getAllArtifacts(final EDMXSet edmxSet) {

		final List<EObject> eObjList = new ArrayList<EObject>();
		final List<Schema> schemas = getSchemas(edmxSet);
		for (Schema schema : schemas) {
			eObjList.addAll(schema.getEntityTypes());
			eObjList.addAll(schema.getComplexTypes());
			eObjList.addAll(schema.getEnumTypes());
			if (schema.getContainers().size() > 0) {
				for (int i = 0; i < schema.getContainers().size(); i++) {
					eObjList.addAll((schema.getContainers().get(i))
							.getFunctionImports());
				}
			}
		}

		return eObjList;
	}

	/**
	 * Returns EDMXSet from OData Service Model.
	 * 
	 * @param model
	 *            - OData Service Model.
	 * @return EDMXSet
	 */
	public static EDMXSet getEDMXSetFromModel(final Object model) {

		EDMXSet edmxSet = null;

		if (model instanceof XMIResource) {
			final Resource resource = (XMIResource) model;
			if (resource != null) {
				final EList<EObject> resources = resource.getContents();
				if (resources.size() == 2) {
					final EObject eObj = resources.get(1);
					if (eObj instanceof EDMXSet) {
						edmxSet = (EDMXSet) eObj;
					} else {
						edmxSet = (EDMXSet) resources.get(0);
					}
				}
			}
		}

		return edmxSet;
	}

	/**
	 * @param obj
	 * @return Schema
	 */
	public static Schema getParentSchema(EObject obj) {
		if (obj == null) {
			return null;
		}
		if (obj instanceof Schema) {
			return (Schema) obj;
		}
		if (obj.eContainer() == null) {
			return null;
		}
		return getParentSchema(obj.eContainer());
	}

	/**
	 * To get the type by passing TypeUsage or tyepe
	 * 
	 * @param typeUsage
	 * @return Object
	 */
	public static EObject getObjectFromTypeUsage(EObject typeUsage) {

		EObject propertyUsage = null;
		if (typeUsage != null) {

			if (typeUsage instanceof ComplexTypeUsage) {

				propertyUsage = ((ComplexTypeUsage) typeUsage).getComplexType();
			} else if (typeUsage instanceof EnumTypeUsage) {

				propertyUsage = ((EnumTypeUsage) typeUsage).getEnumType();
			} else if (typeUsage instanceof EntityTypeUsage) {

				propertyUsage = ((EntityTypeUsage) typeUsage).getEntityType();
			} else if (typeUsage instanceof ReturnEntityTypeUsage) {

				propertyUsage = ((ReturnEntityTypeUsage) typeUsage)
						.getEntityType();
			} else if (typeUsage instanceof ComplexType
					|| typeUsage instanceof EntityType
					|| typeUsage instanceof EnumType) {

				propertyUsage = typeUsage;
			}
		}

		return propertyUsage;
	}

	/**
	 * This method is responsible to update the enum member value.
	 * 
	 * @param value
	 * @param currentEnumMember
	 */
	public static void updateEnumMemberValue(String value,
			EnumMember currentEnumMember) {
		try {
			EnumType enumType = (EnumType) currentEnumMember.eContainer();

			EDMTypes underlyingType = enumType.getUnderlyingType();
			IntegerValue integerValue = currentEnumMember.getValue();
			if (integerValue != null
					&& underlyingType.equals(integerValue.getEDMType())) {
				integerValue.setValueObject(getEnumMemeberValue(underlyingType,
						value));
			} else {
				OdataFactory odataFactory = OdataFactory.eINSTANCE;
				switch (underlyingType) {
				case SBYTE:
					integerValue = odataFactory.createSByteValue();
					break;
				case BYTE:
					integerValue = odataFactory.createByteValue();
					break;
				case INT16:
					integerValue = odataFactory.createInt16Value();
					break;

				case INT32:
					integerValue = odataFactory.createInt32Value();
					break;

				case INT64:
					integerValue = odataFactory.createInt64Value();
					break;

				default:
					// Nothing to implement.
					break;
				}
				if (integerValue != null) {
					currentEnumMember.setValue(integerValue);
					integerValue.setValueObject(getEnumMemeberValue(
							underlyingType, value));
				}

			}

		} catch (NumberFormatException nfe) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(nfe);
		}
	}

	/**
	 * 
	 * This method is responsible to create appropriate value for specified
	 * Underlying Type.
	 * 
	 * @param edmType
	 * @param value
	 * @return Object
	 */
	public static Object getEnumMemeberValue(EDMTypes edmType, String value) {
		Object object = null;
		switch (edmType) {
		case SBYTE:
		case BYTE:
			object = Byte.valueOf(value);
			break;

		case INT16:
			object = Short.valueOf(value);
			break;
		case INT32:
			object = Integer.valueOf(value);
			break;

		case INT64:
			object = Long.valueOf(value);
			break;

		default:
			// Nothing to implement.
			break;
		}
		return object;
	}

	/**
	 * This method is responsible to validate value against Edmtype.
	 * 
	 * @param edmType
	 * @param value
	 * @return valid Object
	 */
	public static Object validateEnumMemeberValue(EDMTypes edmType, String value) {
		Object object = null;
		try {

			switch (edmType) {
			case SBYTE:
			case BYTE:
				object = Byte.valueOf(value);
				break;
			case INT16:
				object = Short.valueOf(value);
				break;
			case INT32:
				object = Integer.valueOf(value);
				break;
			case INT64:
				object = Long.valueOf(value);

				break;

			default:
				// Nothing to implement.
				break;
			}
		} catch (NumberFormatException e) {
			// No need to implement.
		}
		return object;
	}

	/**
	 * Validates the schema alias.
	 * 
	 * @param alias
	 * @return the error message if the validation of alias fails
	 */
	public static String validateAlias(String alias) {

		String errorMsg = null;
		boolean b = false;
		boolean b1 = false;

		Pattern nonSpclChar = Pattern.compile(nonSpclCharacter,
				Pattern.CASE_INSENSITIVE);
		Matcher m = nonSpclChar.matcher(alias);
		b = m.find();

		Pattern onlyChar = Pattern.compile(onlyCharacter,
				Pattern.CASE_INSENSITIVE);
		if (alias.trim().length() > 0) {
			Matcher m1 = onlyChar.matcher(alias.substring(0, 1));
			b1 = m1.find();
		}
		if (b1) {
			errorMsg = Messages.ALIAS_VALIDATION1;
		} else if (b) {
			errorMsg = Messages.ALIAS_VALIDATION2;
		}

		return errorMsg;
	}

	/**
	 * Find the schema corresponding to the namespace in the edmxSet.
	 * 
	 * @param namespace
	 * @param edmxSet
	 * 
	 * @return Schema
	 */
	public static Schema findSchema(final String namespace,
			final EDMXSet edmxSet) {

		if (namespace != null) {
			final EList<Schema> schemata = edmxSet.getSchemata();
			for (Schema schema : schemata) {
				if (namespace.equals(schema.getNamespace())) {
					return schema;
				}
			}
		}
		return null;
	}

	/**
	 * Generates the list of names for all EntityTypes,ComplexTypes and
	 * Associations for unique name generation
	 * 
	 * @param schema
	 * @return names
	 */
	public static List<String> getEntTypeCmplxTypeAssocNames(Schema schema) {
		List<EntityType> entities = schema.getEntityTypes();
		List<ComplexType> cmplx = schema.getComplexTypes();
		List<Association> assoc = schema.getAssociations();
		EntityType e;
		Association a;
		ComplexType c;

		List<String> names = new ArrayList<String>();
		String en = null;
		for (ListIterator<EntityType> it = entities.listIterator(); it
				.hasNext();) {
			e = it.next();
			en = e.getName();
			names.add(en);
		}
		for (ListIterator<ComplexType> it = cmplx.listIterator(); it.hasNext();) {
			c = it.next();
			en = c.getName();
			names.add(en);
		}
		for (ListIterator<Association> it = assoc.listIterator(); it.hasNext();) {
			a = it.next();
			en = a.getName();
			names.add(en);
		}

		return names;
	}

	/**
	 * Generates the list of names for all EntitySets,AssociationSets and
	 * FunctionImports for unique name generation
	 * 
	 * @param schema
	 * @return names
	 */
	public static List<String> getEntSetAssocSetFuncImpNames(Schema schema) {
		List<EntitySet> es = new ArrayList<EntitySet>();
		List<FunctionImport> functionImport = new ArrayList<FunctionImport>();
		List<AssociationSet> as = new ArrayList<AssociationSet>();

		EntitySet e;
		AssociationSet a;
		FunctionImport f;
		int i;
		String n = null;
		List<String> names = new ArrayList<String>();
		// EntitySets
		for (i = 0; i < schema.getContainers().size(); i++) {
			es.addAll((schema.getContainers().get(i)).getEntitySets());
		}
		for (ListIterator<EntitySet> it = es.listIterator(); it.hasNext();) {
			e = it.next();
			n = e.getName();
			names.add(n);
		}

		// Function Imports
		for (i = 0; i < schema.getContainers().size(); i++) {
			functionImport.addAll((schema.getContainers().get(i))
					.getFunctionImports());
		}

		for (ListIterator<FunctionImport> it = functionImport.listIterator(); it
				.hasNext();) {
			f = it.next();
			n = f.getName();
			names.add(n);

		}
		// association Sets
		for (i = 0; i < schema.getContainers().size(); i++) {
			as.addAll((schema.getContainers().get(i)).getAssociationSets());
		}

		for (ListIterator<AssociationSet> it = as.listIterator(); it.hasNext();) {
			a = it.next();
			n = a.getName();
			names.add(n);
		}
		return names;
	}
}
