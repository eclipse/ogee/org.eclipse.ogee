/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.IPictogramElementContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IPeService;

/**
 * This Class contains utility methods for properties to be associated with
 * shapes.
 * 
 */
public class PropertyUtil {

	/**
	 * Constant for Cascading of EObjects
	 */
	public static final int CASCADE_CONSTANT = 22;
	
	/**
	 * Current Diagram's Origin point(x)
	 */
	public static final int DIAGRAM_ORIGIN_X = 0;
	
	/**
	 * Current Diagram's Origin point(y)
	 */
	public static final int DIAGRAM_ORIGIN_Y = 0;
	/**
	 * PictogramElement Service Instance.
	 */
	private static final IPeService PE_SERVICE = Graphiti.getPeService();

	/**
	 * This constant "Shape Id" used As a key to differentiate between Top
	 * Container,Identifier Shapes and others.
	 */
	public static final String SHAPE_ID = "Shape Id"; //$NON-NLS-1$

	/**
	 * Key-SHAPE_ID
	 * 
	 * This constant is used as Value for Identifier Shapes.
	 */
	public static final String IDNETIFIER = "Identifier"; //$NON-NLS-1$

	/**
	 * Key-SHAPE_ID
	 * 
	 * This constant is used as Value for Top Container (Entity
	 * Shape,Association Shape etc.).
	 */
	public static final String TOP_CONATINER = "Top Container"; //$NON-NLS-1$

	/**
	 * This constant is used as a key to differentiate between different shapes.
	 * Values can be IDentifier
	 * sHAPES:ENTITY_NAME,ENTITY_SET_NAME,ENTITY_PROPERTY_NAME
	 * ,NAVIGATION_PROPERTY_NAME
	 * Rectangles:ENTITYSET_RECTANGLE,PROPERTY_RECTANGLE
	 * ,NAVIGATIONPROPERTY_RECTANGLE Expand/Collapse Section headers: TITLE
	 * 
	 */
	public static final String ID = "ID"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for Expand/Collapse Section headers.
	 */
	public static final String TITLE = "TITLE"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Entity Name' Identifier Shape in an
	 * Entity Shape.
	 */
	public static final String ENTITY_NAME = "ENTITY_NAME"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Referenced Entity Name' Identifier
	 * Shape in an Entity Shape.
	 */
	public static final String REFERENCED_ENTITY_NAME = "REFERENCED_ENTITY_NAME"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Entity Set Name' Identifier Shapes in
	 * an Entity Shape.
	 */
	public static final String ENTITY_SET_NAME = "ENTITY_SET_NAME"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Property Name' Identifier Shapes in
	 * an Entity Shape.
	 */
	public static final String ENTITY_PROPERTY_NAME = "ENTITY_PROPERTY_NAME"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Navigation Property Name' Identifier
	 * Shapes in an Entity Shape.
	 */
	public static final String NAVIGATION_PROPERTY_NAME = "NAVIGATION_PROPERTY_NAME"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Entity Sets Rectangle Shape in an
	 * Entity Shape.
	 */
	public static final String ENTITYSET_RECTANGLE = "ENTITYSET_RECTANGLE"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Enum underlying type' Rectangle Shape
	 * in an Enum Shape.
	 */
	public static final String ENUM_UNDERLYING_TYPE_RECTANGLE = "ENUM_UNDERLYING_TYPE_RECTANGLE"; //$NON-NLS-1$
	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Enum member type' Rectangle Shape in
	 * an Enum Shape.
	 */
	public static final String ENUM_MEMBER_RECTANGLE = "ENUM_MEMBER_RECTANGLE"; //$NON-NLS-1$
	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Property Rectangle Shape in an Entity
	 * Shape.
	 */
	public static final String PROPERTY_RECTANGLE = "PROPERTY_RECTANGLE"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Parameter Rectangle Shape in an
	 * Function Import Shape.
	 */
	public static final String FUNCTION_IMPORT_PARAMETER_RECTANGLE = "FUNCTION_IMPORT_PARAMETER_RECTANGLE"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Return type Rectangle Shape in an
	 * Function Import Shape.
	 */
	public static final String FUNCTION_IMPORT_RETURN_TYPE_RECTANGLE = "FUNCTION_IMPORT_RETURN_TYPE_RECTANGLE"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for Navigation Property Rectangle Shape in
	 * an Entity Shape.
	 */
	public static final String NAVIGATIONPROPERTY_RECTANGLE = "NAVIGATIONPROPERTY_RECTANGLE"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Function Import' Identifier Shape
	 */
	public static final String FUNCTION_IMPORT_NAME = "FUNCTION_IMPORT_NAME"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Enum Name' Identifier Shape in an
	 * Enum Shape.
	 */
	public static final String ENUM_NAME = "ENUM_NAME"; //$NON-NLS-1$
	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Enum Member' Identifier Shape
	 */
	public static final String ENUM_MEMBER_NAME = "ENUM_MEMBER_NAME"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Enum Member Value' Identifier Shape
	 */
	public static final String ENUM_MEMBER_VALUE = "ENUM_MEMBER_VALUE"; //$NON-NLS-1$
	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Enum Underlying type' Identifier
	 * Shape
	 */
	public static final String ENUM_UNDERLYING_TYPE_NAME = "ENUM_UNDERLYING_TYPE_NAME"; //$NON-NLS-1$
	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'Complex Name' Identifier Shape in an
	 * Complex Type Shape.
	 */
	public static final String COMPLEX_NAME = "COMPLEX_NAME"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'FUNCTION IMPORT PARAMETER '
	 */
	public static final String FUNCTION_IMPORT_PARAMETER_NAME = "FUNCTION_IMPORT_PARAMETER_NAME"; //$NON-NLS-1$

	/**
	 * Key ID
	 * 
	 * This constant is used as Value for 'FUNCTION IMPORT RETURN TYPE '
	 */
	public static final String FUNCTION_IMPORT_RETURN_TYPE_NAME = "FUNCTION_IMPORT_RETURN_TYPE_NAME"; //$NON-NLS-1$

	/**
	 * This constant is used as a Key to define the Expanded State a Shape. As
	 * of now It is used for Section Headers and Rectangles
	 * (EntitySets,Properties,Navigation Properties etc).
	 * 
	 */
	public static final String EXPANDED_STATE = "Expanded State"; //$NON-NLS-1$

	/**
	 * Key EXPANDED_STATE
	 * 
	 * This constant is used as Value for Expanded Shapes.
	 */
	public static final String EXPANDED = "Expanded"; //$NON-NLS-1$

	/**
	 * Key EXPANDED_STATE
	 * 
	 * This constant is used as Value for Collapsed Shapes.
	 */
	public static final String COLLAPSED = "Collapsed"; //$NON-NLS-1$
	/**
	 * This constant is used as a Key to define the Usage State a Shape. As of
	 * now It is used for compelx types and Enum types
	 * 
	 */
	public static final String SHOW_USAGE = "Show usage"; //$NON-NLS-1$
	/**
	 * Key SHOW_USAGE
	 * 
	 * This constant is used as Value for Shown.
	 */
	public static final String SHOWN = "Shown"; //$NON-NLS-1$
	/**
	 * Key SHOW_USAGE
	 * 
	 * This constant is used as Value for Shown.
	 */
	public static final String HIDE = "Hide"; //$NON-NLS-1$

	/**
	 * This constant is used as a Key to define the Show Usage State all Shape.
	 * As of now it is used for complex types and Enum types
	 * 
	 */
	public static final String SHOW_ALL_USAGE = "SHOW_ALL_USAGE"; //$NON-NLS-1$

	/**
	 * Key SHOW_ALL_USAGE
	 * 
	 * This constant is used as Value for SHOW_ALL.
	 */
	public static final String SHOW_ALL = "SHOW_ALL"; //$NON-NLS-1$

	/**
	 * Key SHOW_ALL_USAGE
	 * 
	 * This constant is used as Value for HIDE_ALL.
	 */
	public static final String HIDE_ALL = "HIDE_ALL"; //$NON-NLS-1$

	/**
	 * This constant is used as a Key to store the Height for a Shape.
	 * 
	 */
	public static final String CONTAINER_HEIGHT = "Container Height"; //$NON-NLS-1$

	/**
	 * Key SHAPE_TYPE
	 * 
	 */
	private static String SHAPE_TYPE = "shapetype"; //$NON-NLS-1$

	/**
	 * Key SHAPE_TYPE
	 * 
	 * This constant is used as Value for Link Usage.
	 */
	private static String LINK_CONNECTION = "Link Usage"; //$NON-NLS-1$

	/**
	 * This method is responsible to store SHAPED_ID property as IDNETIFIER for
	 * specified PictogramElement.
	 * 
	 * @param pe
	 *            PictogramElement
	 */
	public static final void setTypeIdentifier(PictogramElement pe) {
		PE_SERVICE.setPropertyValue(pe, SHAPE_ID, IDNETIFIER);
	}

	/**
	 * Returns whether Specified PictogramElement has SHAPED_ID Value as
	 * IDNEIFIER or Not.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isTypeIdentifier(PictogramElement pe) {
		return IDNETIFIER.equals(PE_SERVICE.getPropertyValue(pe, SHAPE_ID));
	}

	/**
	 * This method is responsible to store SHAPED_ID property as TOP_CONATINER
	 * for specified PictogramElement.
	 * 
	 * @param pe
	 *            PictogramElement
	 */
	public static void setAsTopContainer(PictogramElement pe) {
		PE_SERVICE.setPropertyValue(pe, SHAPE_ID, TOP_CONATINER);
	}

	/**
	 * Returns whether Specified PictogramElement has SHAPED_ID Value as
	 * TOP_CONATINER or Not.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isTopContainer(PictogramElement pe) {
		return TOP_CONATINER.equals(PE_SERVICE.getPropertyValue(pe, SHAPE_ID));
	}

	/**
	 * This method is responsible to store ID property value.
	 * 
	 * IDentifier sHAPES:ENTITY_NAME,ENTITY_SET_NAME,ENTITY_PROPERTY_NAME,
	 * NAVIGATION_PROPERTY_NAME
	 * Rectangles:ENTITYSET_RECTANGLE,PROPERTY_RECTANGLE
	 * ,NAVIGATIONPROPERTY_RECTANGLE Expand/Collapse Section headers: TITLE
	 * 
	 * @param pe
	 *            PictogramElement
	 * @param value
	 *            String
	 */
	public static void setID(PictogramElement pe, String value) {
		PE_SERVICE.setPropertyValue(pe, ID, value);
	}

	/**
	 * Returns PictogramElement has ID Value.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return Property ID value
	 */
	public static String getID(PictogramElement pe) {
		return PE_SERVICE.getPropertyValue(pe, ID);
	}

	/**
	 * This method is responsible to store EXPANDED_STATE property as EXPANDED
	 * for specified PictogramElement.
	 * 
	 * @param pe
	 *            PictogramElement
	 */
	public static final void setExpanded(PictogramElement pe) {
		PE_SERVICE.setPropertyValue(pe, EXPANDED_STATE, EXPANDED);
	}

	/**
	 * Returns whether Specified PictogramElement has EXPANDED_STATE Value as
	 * EXPANDED or Not.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isExpanded(PictogramElement pe) {
		return EXPANDED.equals(PE_SERVICE.getPropertyValue(pe, EXPANDED_STATE));
	}

	/**
	 * This method is responsible to store EXPANDED_STATE property as EXPANDED
	 * for specified PictogramElement.
	 * 
	 * @param pe
	 *            PictogramElement
	 */
	public static final void setShowUsage(PictogramElement pe) {
		PE_SERVICE.setPropertyValue(pe, SHOW_USAGE, SHOWN);
	}

	/**
	 * This method is responsible to store EXPANDED_STATE property as EXPANDED
	 * for specified PictogramElement.
	 * 
	 * @param pe
	 *            PictogramElement
	 */
	public static final void setHideUsage(PictogramElement pe) {
		PE_SERVICE.setPropertyValue(pe, SHOW_USAGE, HIDE);
	}

	/**
	 * Returns whether Specified PictogramElement has EXPANDED_STATE Value as
	 * EXPANDED or Not.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isShowUsage(PictogramElement pe) {
		return SHOWN.equals(PE_SERVICE.getPropertyValue(pe, SHOW_USAGE));
	}

	/**
	 * This method is responsible to store EXPANDED_STATE property as COLLAPSED
	 * for specified PictogramElement.
	 * 
	 * @param pe
	 *            PictogramElement
	 */
	public static final void setCollapsed(PictogramElement pe) {
		PE_SERVICE.setPropertyValue(pe, EXPANDED_STATE, COLLAPSED);
	}

	/**
	 * Returns whether Specified PictogramElement has EXPANDED_STATE Value as
	 * COLLAPSED or Not.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isCollapsed(PictogramElement pe) {
		return COLLAPSED
				.equals(PE_SERVICE.getPropertyValue(pe, EXPANDED_STATE));
	}

	/**
	 * This method is responsible to store CONTAINER_HEIGHT property with
	 * specified Container Height for a PictogramElement.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @param height
	 *            Container Height
	 */
	public static void setContainerHeight(PictogramElement pe, long height) {
		PE_SERVICE.setPropertyValue(pe, CONTAINER_HEIGHT,
				String.valueOf(height));
	}

	/**
	 * Returns Container Height for a shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return Long Container Height.
	 */
	public static Long getContainerHeight(PictogramElement pe) {
		Long containerHeight = null;
		String propertyValue = PE_SERVICE
				.getPropertyValue(pe, CONTAINER_HEIGHT);
		if (propertyValue != null) {
			containerHeight = Long.valueOf(propertyValue);
		}
		return containerHeight;
	}

	/**
	 * This method is responsible check whether specified Shape is Movable or
	 * not. Only Top Containers (Entity Shape,Association Shape ..etc) are
	 * Movable. *
	 * 
	 * 
	 * @param context
	 *            IPictogramElementContext
	 * @return isMovable
	 */
	public static boolean isMovableShape(IPictogramElementContext context) {
		PictogramElement pe = context.getPictogramElement();
		return isTopContainer(pe);
	}

	/**
	 * This method is responsible check whether specified Shape can be deleted
	 * or not. Top Containers (Entity SHape,Association Shape ..etc) can be
	 * deleted. ENTITY_NAME can not be deleted. SECTION HEADERS can not be
	 * deleted. INSIDE RECTANGLES can not be deleted.
	 * 
	 * @param context
	 *            IPictogramElementContext
	 * @return isMovable
	 */
	public static boolean isDeletableShape(IPictogramElementContext context) {
		PictogramElement pe = context.getPictogramElement();
		return isTopContainer(pe)
				|| (pe instanceof ContainerShape && !isInvisibleRectangle(pe)
						&& !isTitle(pe) && !isContainerHeader(pe) && !isEnumUndderlyingType(pe));
	}

	/**
	 * This method is responsible check whether ContextMenu should be enabled or
	 * not for a specified PictogramElement.
	 * 
	 * @param context
	 *            ICustomContext
	 * @return boolean
	 */
	public static boolean isContextMenuEnabled(ICustomContext context) {
		boolean isEnabled = false;
		PictogramElement[] pictogramElements = context.getPictogramElements();
		if (pictogramElements != null && pictogramElements.length > 0) {
			PictogramElement pictogramElement = pictogramElements[0];
			isEnabled = isTopContainer(pictogramElement)
					|| (pictogramElement instanceof ContainerShape
							&& !isInvisibleRectangle(pictogramElement) && !isEnumUndderlyingType(pictogramElement));
		}
		return isEnabled;
	}

	/**
	 * This method is responsible check whether ContextPad Should be enabled or
	 * not for a specified PictogramElement.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isContextPadEnabled(PictogramElement pe) {
		return isTopContainer(pe);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as TITLE or
	 * Not.This will be true for Section Headers.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isTitle(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null && currentId.equalsIgnoreCase(TITLE);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as Entity Name or
	 * Not.This will be true for Entity Name Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isEntityName(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null && currentId.equalsIgnoreCase(ENTITY_NAME);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as Referenced
	 * Entity Name or Not.This will be true for ENTITY_NAME Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isReferencedEntityName(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null
				&& currentId.equalsIgnoreCase(REFERENCED_ENTITY_NAME);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as ENTITY_NAME or
	 * Not.This will be true for ENTITY_NAME Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isConnectionDecorator(PictogramElement pe) {
		return (pe instanceof ConnectionDecorator);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as
	 * ENTITY_SET_NAME or Not.This will be true for ENTITY_SET_NAME Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isEntitySet(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null && currentId.equalsIgnoreCase(ENTITY_SET_NAME);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as PARAMETER or
	 * Not.This will be true for PARAMETER Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isParameter(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null
				&& currentId.equalsIgnoreCase(FUNCTION_IMPORT_PARAMETER_NAME);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as EnumMember or
	 * Not.This will be true for EnumMember Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isEnumMember(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null
				&& currentId.equalsIgnoreCase(ENUM_MEMBER_NAME);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as
	 * EnumMemberValue or Not.This will be true for EnumMember Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isEnumMemberValue(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null
				&& currentId.equalsIgnoreCase(ENUM_MEMBER_VALUE);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as ReturnType or
	 * Not.This will be true for ReturnType Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isReturnType(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null
				&& currentId.equalsIgnoreCase(FUNCTION_IMPORT_RETURN_TYPE_NAME);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as
	 * ENTITY_PROPERTY_NAME or Not.This will be true for ENTITY_PROPERTY_NAME
	 * Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isProperty(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null
				&& currentId.equalsIgnoreCase(ENTITY_PROPERTY_NAME);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as
	 * ENTITY_PROPERTY_NAME or Not.This will be true for
	 * NAVIGATION_PROPERTY_NAME Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isNavigationProperty(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null
				&& currentId.equalsIgnoreCase(NAVIGATION_PROPERTY_NAME);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as COMPLEX_NAME
	 * or Not.This will be true for COMPLEX_NAME Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isComplexTypeName(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null && currentId.equalsIgnoreCase(COMPLEX_NAME);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as EnumName or
	 * Not.This will be true for EnumName Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isEnumTypeName(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null && currentId.equalsIgnoreCase(ENUM_NAME);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as
	 * FUNCTION_IMPORT_NAME or Not.This will be true for FUNCTION_IMPORT_NAME
	 * Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isFunctionImportName(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null
				&& currentId.equalsIgnoreCase(FUNCTION_IMPORT_NAME);
	}

	/**
	 * returns whether Container header or not.
	 * 
	 * @param pe
	 * @return boolean
	 */
	public static boolean isContainerHeader(PictogramElement pe) {
		return isEntityName(pe) || isFunctionImportName(pe)
				|| isComplexTypeName(pe) || isEnumTypeName(pe);
	}

	/**
	 * Returns whether Specified PictogramElement has ID Value as
	 * EnumUndderlyingType or Not.This will be true for EnumUndderlyingType
	 * Shape.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isEnumUndderlyingType(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null
				&& currentId.equalsIgnoreCase(ENUM_UNDERLYING_TYPE_NAME);
	}

	/**
	 * Image can be selected only for Entity Name and Section Headers.
	 * 
	 * @param currentId
	 * @return Whether Image can be selected or not.
	 */
	public static boolean isImageSelectable(String currentId) {
		return ENTITY_NAME.equalsIgnoreCase(currentId)
				|| COMPLEX_NAME.equalsIgnoreCase(currentId)
				|| FUNCTION_IMPORT_NAME.equalsIgnoreCase(currentId)
				|| FUNCTION_IMPORT_RETURN_TYPE_NAME.equalsIgnoreCase(currentId)
				|| ENUM_NAME.equalsIgnoreCase(currentId)
				|| TITLE.equals(currentId)
				|| ENTITY_PROPERTY_NAME.equals(currentId);
	}

	/**
	 * ENTER KEY functionality enabled for ENTITY_PROPERTY_NAME,
	 * FUNCTION_IMPORT_PARAMETER and ENTITY_SET_NAME.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return Whether Image can be selected or not.
	 */
	public static boolean isEnterKeyEnabled(PictogramElement pe) {
		String currentId = getID(pe);
		return ENTITY_PROPERTY_NAME.equalsIgnoreCase(currentId)
				|| ENTITY_SET_NAME.equalsIgnoreCase(currentId)
				|| FUNCTION_IMPORT_PARAMETER_NAME.equalsIgnoreCase(currentId)
				|| ENUM_MEMBER_NAME.equalsIgnoreCase(currentId)
				|| ENUM_MEMBER_VALUE.equalsIgnoreCase(currentId)
				|| NAVIGATION_PROPERTY_NAME.equalsIgnoreCase(currentId);

	}

	/**
	 * Returns whether specified PictogramElement is Invisible Rectangle.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isInvisibleRectangle(PictogramElement pe) {
		String currentId = getID(pe);
		return currentId != null
				&& (currentId.equalsIgnoreCase(PROPERTY_RECTANGLE)
						|| currentId.equalsIgnoreCase(ENTITYSET_RECTANGLE)
						|| currentId
								.equalsIgnoreCase(NAVIGATIONPROPERTY_RECTANGLE)
						|| currentId
								.equalsIgnoreCase(FUNCTION_IMPORT_PARAMETER_RECTANGLE)
						|| currentId
								.equalsIgnoreCase(FUNCTION_IMPORT_RETURN_TYPE_RECTANGLE)
						|| currentId
								.equalsIgnoreCase(ENUM_UNDERLYING_TYPE_RECTANGLE) || currentId
							.equalsIgnoreCase(ENUM_MEMBER_RECTANGLE));
	}

	/**
	 * @param pe
	 * @return can be updated or not
	 */
	public static boolean canUpdate(PictogramElement pe) {
		boolean canUpdate = false;
		GraphicsAlgorithm graphicsAlgorithm = pe.getGraphicsAlgorithm();
		if (PropertyUtil.isTopContainer(pe)
				|| PropertyUtil.isInvisibleRectangle(pe)) {
			canUpdate = false;
		} else if ((PropertyUtil.isTitle(pe) || PropertyUtil.isReturnType(pe) || PropertyUtil
				.isProperty(pe)) && (graphicsAlgorithm instanceof Image)) {
			canUpdate = true;
		} else if (pe.eContainer() instanceof PictogramElement
				&& PropertyUtil.isTypeIdentifier((PictogramElement) pe
						.eContainer()) && !PropertyUtil.isTitle(pe)
				&& graphicsAlgorithm instanceof Text) {
			canUpdate = true;
		} else if (PropertyUtil.isEntityName(pe)) {
			canUpdate = true;
		} else if (PropertyUtil.isConnectionDecorator(pe)) {
			canUpdate = true;
		}

		return canUpdate;
	}

	/**
	 * This method is responsible to store SHAPED_TYPE property as
	 * LINK_CONNECTION for specified PictogramElement.
	 * 
	 * @param pe
	 *            PictogramElement
	 */
	public static void setAsLinkConncetion(PictogramElement pe) {
		PE_SERVICE.setPropertyValue(pe, SHAPE_TYPE, LINK_CONNECTION);

	}

	/**
	 * Returns whether Specified PictogramElement has SHAPED_TYPE Value as
	 * LINK_CONNECTION or Not.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @return boolean
	 */
	public static boolean isLinkConnection(PictogramElement pe) {
		return LINK_CONNECTION.equals(PE_SERVICE.getPropertyValue(pe,
				SHAPE_TYPE));
	}

	/**
	 * This method is responsible to store SHOW_All_USAGE property as SHOW_ALL
	 * if isShowAll parameter is true & to HIDE_ALL otherwise, for the specified
	 * Diagram.
	 * 
	 * @param diagram
	 * @param isShowAll
	 */
	public static final void setShowAllUsage(Diagram diagram, boolean isShowAll) {

		if (isShowAll) {
			PE_SERVICE.setPropertyValue(diagram, SHOW_ALL_USAGE, SHOW_ALL);
		} else {
			PE_SERVICE.setPropertyValue(diagram, SHOW_ALL_USAGE, HIDE_ALL);
		}
	}

	/**
	 * Returns whether specified Diagram has SHOW_ALL_USAGE value as SHOW_ALL or
	 * not.
	 * 
	 * @param diagram
	 * 
	 * @return boolean true, if SHOW_ALL_USAGE value is SHOW_ALL.
	 */
	public static boolean isShowAllUsage(Diagram diagram) {

		return SHOW_ALL.equals(PE_SERVICE.getPropertyValue(diagram,
				SHOW_ALL_USAGE));
	}

}
