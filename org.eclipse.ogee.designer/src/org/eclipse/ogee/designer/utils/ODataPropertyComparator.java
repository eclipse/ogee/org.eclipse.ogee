/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import java.util.Comparator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.ogee.model.odata.EntityType;

/**
 * This class is responsible to sort Property shapes (key properties and non-key
 * Properties).
 */
public class ODataPropertyComparator implements Comparator<Shape> {

	/**
	 * Feature Provider Instance
	 */
	private IFeatureProvider fp;

	/**
	 * @param fp
	 */
	public ODataPropertyComparator(IFeatureProvider fp) {
		super();
		this.fp = fp;
	}

	/**
	 * Compares all key properties to Non key properties.
	 */
	@Override
	public int compare(Shape containerShape1, Shape containerShape2) {

		int status = 0;
		if (PropertyUtil.isTypeIdentifier(containerShape1)
				&& PropertyUtil.isTypeIdentifier(containerShape2)) {
			EObject parentContainer = containerShape1.eContainer();
			if (parentContainer != null
					&& parentContainer == containerShape2.eContainer()) {
				Object property1 = this.fp
						.getBusinessObjectForPictogramElement(containerShape1);
				Object property2 = this.fp
						.getBusinessObjectForPictogramElement(containerShape2);
				Object parentObject = this.fp
						.getBusinessObjectForPictogramElement((PictogramElement) parentContainer);
				if (parentObject instanceof EntityType) {
					EntityType entity = (EntityType) parentObject;
					int status1 = entity.getKeys().contains(property1) ? 0 : 1;
					int status2 = entity.getKeys().contains(property2) ? 0 : 1;
					status = status1 - status2;
				}
			}
		}
		return status;
	}
}
