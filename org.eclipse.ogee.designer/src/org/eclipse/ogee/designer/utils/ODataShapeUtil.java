/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IAddBendpointFeature;
import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.IDeleteFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.context.IAreaContext;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.context.impl.AddBendpointContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.features.context.impl.DeleteContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.features.context.impl.MultiDeleteInfo;
import org.eclipse.graphiti.features.context.impl.UpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.PictogramLink;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.ogee.designer.contexts.AddReferencedEntityContext;
import org.eclipse.ogee.designer.contexts.AssociationAddConnectionContext;
import org.eclipse.ogee.designer.contexts.NavigationPropertyContext;
import org.eclipse.ogee.designer.factories.ArtifactFactory;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.features.ODataAddNavigationPropertyFeature;
import org.eclipse.ogee.designer.features.ODataLinkUsageFeature;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataFeatureProvider;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.types.AssociationType;
import org.eclipse.ogee.designer.types.MultiplicityType;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl;
import org.eclipse.ogee.model.odata.impl.SimpleTypeUsageImpl;

/**
 * This class utility methods for Shapes.
 * 
 */
public class ODataShapeUtil {

	private static final String EMPTY_STRING = ""; //$NON-NLS-1$

	/**
	 * Deletes the Association object, Association set and it's related
	 * navigation properties also.
	 * 
	 * @param currentObject
	 * @param featureProvider
	 */
	public static void deleteAssociation(Object currentObject,
			IFeatureProvider featureProvider) {
		Association association = (Association) currentObject;

		EObject selectedObj = featureProvider.getDiagramTypeProvider()
				.getDiagram().eResource().getContents().get(1);
		if (selectedObj instanceof EDMXSet) {
			// Find the Association Set corresponding to the Association and
			// delete
			EList<AssociationSet> assocSets = association.getAssociationSets();
			List<AssociationSet> tempList = new ArrayList<AssociationSet>();
			tempList.addAll(assocSets);
			for (AssociationSet assocSet : tempList) {
				assocSets.remove(assocSet);
				EcoreUtil.delete(assocSet, true);
			}

			// Find the Navigation Property corresponding to the association
			// and delete
			PictogramElement entityPE = null;
			EList<Shape> children = null;
			List<Shape> tempchildrenList = null;
			EList<Shape> navPropertychildren = null;
			List<Shape> tempShapeList = null;
			NavigationProperty navProperty = null;
			List<EntityType> entityTypes = ArtifactUtil
					.getEntityTypesForAssociation(association);
			for (EntityType entityType : entityTypes) {
				entityPE = featureProvider
						.getPictogramElementForBusinessObject(entityType);
				children = ((ContainerShape) entityPE).getChildren();
				tempchildrenList = new ArrayList<Shape>();
				tempchildrenList.addAll(children);
				for (Shape shape : tempchildrenList) {
					if (PropertyUtil.NAVIGATIONPROPERTY_RECTANGLE
							.equals(PropertyUtil.getID(shape))) {
						navPropertychildren = ((ContainerShape) shape)
								.getChildren();
						tempShapeList = new ArrayList<Shape>();
						tempShapeList.addAll(navPropertychildren);
						for (Shape navPropertyShape : tempShapeList) {
							// Get the Navigation Property for the
							// Association
							navProperty = (NavigationProperty) (featureProvider
									.getBusinessObjectForPictogramElement(navPropertyShape));
							if (navProperty.getRelationship() == association) {
								DeleteContext deleteContext = new DeleteContext(
										navPropertyShape);
								// Added to avoid pop-up dialog.
								deleteContext
										.setMultiDeleteInfo(new MultiDeleteInfo(
												false, false, 1));
								featureProvider.getDeleteFeature(deleteContext)
										.execute(deleteContext);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Deletes the Entity object, EntitySet and it's related Associations also.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @param featureProvider
	 */
	public static void deleteEntity(PictogramElement pe,
			IFeatureProvider featureProvider) {
		ContainerShape entityPE = ((ContainerShape) pe);

		// Find the EntitySet corresponding to the association
		// and delete

		EList<Shape> children = entityPE.getChildren();
		List<Shape> tempchildrenList = new ArrayList<Shape>();
		tempchildrenList.addAll(children);
		EList<Shape> navPropertychildren = null;
		List<Shape> tempShapeList = null;
		DeleteContext deleteContext = null;
		for (Shape shape : tempchildrenList) {
			if (PropertyUtil.ENTITYSET_RECTANGLE.equals(PropertyUtil
					.getID(shape))) {
				navPropertychildren = ((ContainerShape) shape).getChildren();
				tempShapeList = new ArrayList<Shape>();
				tempShapeList.addAll(navPropertychildren);
				for (Shape entitySetShape : tempShapeList) {
					// Get the Entity Set
					deleteContext = new DeleteContext(entitySetShape);
					// Added to avoid pop-up dialog.
					deleteContext.setMultiDeleteInfo(new MultiDeleteInfo(false,
							false, 1));
					featureProvider.getDeleteFeature(deleteContext).execute(
							deleteContext);

				}
				break;
			}

		}

		List<Connection> associations = AssociationUtil
				.getAllAssociations(entityPE);
		for (Connection connection : associations) {
			deleteContext = new DeleteContext(connection);
			// Added to avoid pop-up dialog.
			deleteContext.setMultiDeleteInfo(new MultiDeleteInfo(false, false,
					1));
			IDeleteFeature deleteFeature = featureProvider
					.getDeleteFeature(deleteContext);
			if (deleteFeature != null && deleteFeature.canDelete(deleteContext)) {
				deleteFeature.execute(deleteContext);
			}

		}
	}

	/**
	 * Updates the Navigation Property object and aligns with the related
	 * Associations also.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @param featureProvider
	 * @param isVisible
	 */
	public static void updateNavigationProperty(PictogramElement pe,
			IFeatureProvider featureProvider, boolean isVisible) {

		Shape navPropPE = ((Shape) pe);

		PictogramElement pictogramElement = navPropPE.getGraphicsAlgorithm()
				.getPictogramElement();
		Object navProperty = featureProvider
				.getBusinessObjectForPictogramElement(pictogramElement);

		Association relationship = ((NavigationProperty) navProperty)
				.getRelationship();

		if (relationship != null) {

			PictogramElement[] connectionPE = featureProvider
					.getAllPictogramElementsForBusinessObject(relationship);

			Role fromRole = ((NavigationProperty) navProperty).getFromRole();

			// Method is called to delete the decorators for the previous
			// association as isVisible is passed as false
			ODataDecoratorUtil.removeDecorators(featureProvider, relationship,
					false, true, fromRole, connectionPE);

		}

	}

	/**
	 * @param context
	 * @param fp
	 * @return connection can be started.
	 */
	public static boolean canStartUniAssociation(
			ICreateConnectionContext context, IFeatureProvider fp) {
		// check for right domain object instance (Only if Entity Type)
		PictogramElement topContainer = ODataShapeUtil.getTopContainer(context
				.getSourcePictogramElement());
		if (topContainer != null
				&& fp.getBusinessObjectForPictogramElement(topContainer) instanceof EntityType
				&& !(ArtifactUtil.isReferencedEntityType(fp
						.getBusinessObjectForPictogramElement(topContainer)))) {
			return true;
		}
		return false;
	}

	/**
	 * Returns true if a uni directional association can be created
	 * 
	 * @param context
	 * @param fp
	 * @return connection can be created.
	 */
	public static boolean canCreateUniAssociation(
			ICreateConnectionContext context, IFeatureProvider fp) {
		boolean canCreate = false;
		PictogramElement sourcePictogramElement = ODataShapeUtil
				.getTopContainer(context.getSourcePictogramElement());
		PictogramElement targetPictogramElement = ODataShapeUtil
				.getTopContainer(context.getTargetPictogramElement());

		// check for right domain object instance below
		Object source = fp
				.getBusinessObjectForPictogramElement(sourcePictogramElement);
		Object target = fp
				.getBusinessObjectForPictogramElement(targetPictogramElement);
		if (source != null && target != null && source instanceof EntityType
				&& target instanceof EntityType
				&& !(ArtifactUtil.isReferencedEntityType(source))) {
			canCreate = true;
		}
		return canCreate;
	}

	/**
	 * Creates a uni directional association
	 * 
	 * @param context
	 * @param fp
	 * @param artifactFactory
	 * @param shapesFactory
	 * @return Created Connection
	 */
	public static Connection createUniAssociation(
			ICreateConnectionContext context, IFeatureProvider fp,
			ArtifactFactory artifactFactory, ShapesFactory shapesFactory) {
		Connection newConnection = null;
		AssociationUtil.updateAssociationContext(context);

		Anchor sourceAnchor = context.getSourceAnchor();
		Anchor targetAnchor = context.getTargetAnchor();

		EntityType source = getEntityType(sourceAnchor, fp);
		EntityType target = getEntityType(targetAnchor, fp);

		if (source != null && target != null) {

			// create new business object
			Association association = artifactFactory.createAssociation(source,
					target, MultiplicityType.ONE, MultiplicityType.MANY);

			NavigationPropertyContext navContext = new NavigationPropertyContext(
					sourceAnchor.getParent(), targetAnchor.getParent(),
					association);
			navContext.setSourceMultuplicity(MultiplicityType.ONE);
			navContext.setTargetMultuplicity(MultiplicityType.MANY);
			navContext
					.setAssociationType(source == target ? AssociationType.SELF
							: AssociationType.UNIDIR);
			ODataAddNavigationPropertyFeature navPropFeature = new ODataAddNavigationPropertyFeature(
					fp, artifactFactory, shapesFactory);
			navPropFeature.execute(navContext);

			// add connection for business object
			AssociationAddConnectionContext addContext = new AssociationAddConnectionContext(
					sourceAnchor, targetAnchor);
			addContext.setSourceMultiplicity(MultiplicityType.ONE);
			addContext.setTargetMultiplicity(MultiplicityType.MANY);

			// set Source and Target Roles for Connection Decorators
			List<Role> ends = association.getEnds();
			addContext.setSourceRoleObject(ends.get(0));
			addContext.setTargetRoleObject(ends.get(1));

			addContext.setUniDirectionalAssociation(true);
			addContext.setAssociationType(AssociationType.UNIDIR);
			addContext.setNewObject(association);
			addContext.setTargetContainer(fp.getDiagramTypeProvider()
					.getDiagram());
			newConnection = (Connection) fp.addIfPossible(addContext);

		}
		return newConnection;
	}

	/**
	 * Returns the anchor, or null if not available.
	 */
	private static EntityType getEntityType(Anchor anchor, IFeatureProvider fp) {

		if (anchor != null) {
			Object object = fp.getBusinessObjectForPictogramElement(anchor
					.getParent());
			if (object instanceof EntityType) {
				return (EntityType) object;
			}
		}

		return null;
	}

	/**
	 * This method is responsible to update the selection on delete.
	 * 
	 * @param fp
	 * @param mainPictogramElement
	 * @param parentPE
	 * @param deleteItemIndex
	 */
	public static void updateSelOnDelete(IFeatureProvider fp,
			PictogramElement mainPictogramElement, PictogramElement parentPE,
			int deleteItemIndex) {
		EList<Shape> children = ((ContainerShape) parentPE).getChildren();
		PictogramElement selPE = parentPE;
		int size = children.size();
		int index = deleteItemIndex == 0 ? 0 : deleteItemIndex - 1;
		if (size > 0 && index >= 0 && index < size) {
			selPE = children.get(index);
		} else {
			EList<Shape> children2 = ((ContainerShape) mainPictogramElement)
					.getChildren();
			int indexOfParentPE = children2.indexOf(parentPE);
			if (indexOfParentPE > 0) {
				selPE = indexOfParentPE < children2.size() ? children2
						.get(indexOfParentPE - 1) : selPE;
			}
		}
		fp.getDiagramTypeProvider().getDiagramBehavior().getDiagramContainer()
				.setPictogramElementForSelection(selPE);
	}

	/**
	 * This method is responsible to add a Bend point for connection.this method
	 * is used to avoid overlapping of connections.
	 * 
	 * 
	 * @param fe
	 *            Feature Provider
	 * @param connection
	 *            Connection
	 * @param sourceAnchor
	 *            Source Container
	 * @param targetAnchor
	 *            Target Container
	 */
	public static void addConnectionBendPoint(IFeatureProvider fe,
			Connection connection, Anchor sourceAnchor, Anchor targetAnchor) {

		AnchorContainer source = sourceAnchor.getParent();
		AnchorContainer target = targetAnchor.getParent();
		if (source != target) {
			org.eclipse.swt.graphics.Point bendPoint = AssociationUtil
					.getBendPoint((FreeFormConnection) connection);
			AddBendpointContext context2 = new AddBendpointContext(
					(FreeFormConnection) connection, bendPoint.x, bendPoint.y,
					0);
			IAddBendpointFeature addBendpointFeature = fe
					.getAddBendpointFeature(context2);
			addBendpointFeature.execute(context2);
		}
	}

	/**
	 * This method is responsible to update all the effected pictogramElements.
	 * 
	 * @param object
	 * @param fp
	 *            feature provider
	 * @param onDelete
	 *            is it called from delete feature
	 */
	public static void updateReferedShapes(EObject object, IFeatureProvider fp,
			boolean onDelete) {

		List<Resource> arrayList = new ArrayList<Resource>();
		arrayList.add(object.eResource());
		Map<EObject, Collection<Setting>> referredMap = EcoreUtil.CrossReferencer
				.find(arrayList);
		Collection<Setting> collection = referredMap.get(object);
		EObject eObject = null;
		EStructuralFeature eStructuralFeature = null;
		IUpdateContext updateContext = null;
		IUpdateFeature updateFeature = null;
		if (collection != null) {
			for (Setting setting : collection) {
				eObject = setting.getEObject();
				if (!(object.eClass().equals(eObject.eClass()))
						&& !(eObject instanceof PictogramLink)) {
					List<PictogramElement> pictogramElements = Graphiti
							.getLinkService().getPictogramElements(
									fp.getDiagramTypeProvider().getDiagram(),
									eObject);
					if (pictogramElements != null) {
						for (PictogramElement pe : pictogramElements) {
							if (pe instanceof Shape
									&& pe.getGraphicsAlgorithm() instanceof Text) {

								if (onDelete) {
									eStructuralFeature = setting
											.getEStructuralFeature();
									if (ArtifactUtil
											.isReferencedEntityType(object)) {
										continue;
									} else if (eStructuralFeature.isMany()) {
										((EList<?>) eObject
												.eGet(eStructuralFeature))
												.remove(object);
									} else {
										eObject.eSet(eStructuralFeature, null);
									}
								}
								updateContext = new UpdateContext(pe);
								updateFeature = fp
										.getUpdateFeature(updateContext);
								if (updateFeature != null
										&& updateFeature.updateNeeded(
												updateContext).toBoolean()) {
									updateFeature.update(updateContext);

								}

							}

						}
					}

				}
			}
		}

		if (ArtifactUtil.isNavigation(object) && onDelete) {
			updateNavigationProperty(
					fp.getPictogramElementForBusinessObject(object), fp,
					onDelete);
		}

	}

	/**
	 * Sets the new value to the UI for entity and the object Also makes the
	 * corresponding changes to other elements
	 * 
	 * @param fp
	 * 
	 * 
	 * @param pe
	 *            PictogramElement
	 * @param value
	 * @param prevEntityName
	 */
	public static void updateEntityName(IFeatureProvider fp,
			PictogramElement pe, String value, String prevEntityName) {

		Object businessObject = fp.getBusinessObjectForPictogramElement(pe);
		if (businessObject instanceof EntityType) {
			EntityType entityType = (EntityType) businessObject;
			entityType.setName(value);

			PictogramElement mainPictogramElement = (PictogramElement) pe
					.eContainer().eContainer();

			EList<Shape> children = ((ContainerShape) mainPictogramElement)
					.getChildren();
			for (Shape shape : children) {

				if (PropertyUtil.ENTITYSET_RECTANGLE.equals(PropertyUtil
						.getID(shape))) {

					EList<Shape> entitySetShapes = ((ContainerShape) shape)
							.getChildren();
					if (entitySetShapes.size() > 0) {

						Shape entitySetShape = entitySetShapes.get(0);

						Shape pe3 = ((ContainerShape) entitySetShape)
								.getChildren().get(1);

						Object currentObj = fp
								.getBusinessObjectForPictogramElement(pe3);
						if (ArtifactUtil.isEntitySet(currentObj)) {
							EntitySet entitySet = (EntitySet) currentObj;
							String prevEntitySetName = prevEntityName
									+ Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME;
							if (prevEntitySetName.equalsIgnoreCase(entitySet
									.getName())) {
								EObject object = entitySet.eContainer();
								if (object instanceof EntityContainer) {
									EList<EntitySet> entitySets = ((EntityContainer) object)
											.getEntitySets();
									String esn = null;
									List<String> entitySetNames = new ArrayList<String>();
									for (ListIterator<EntitySet> it = entitySets
											.listIterator(); it.hasNext();) {
										EntitySet es = it.next();
										esn = es.getName();
										entitySetNames.add(esn);
									}
									String newName = ArtifactNameUtil
											.generateUniqueName(
													entitySetNames,
													value,
													value
															+ Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME,
													Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME,
													IODataEditorConstants.MAX_LENGTH);
									if (newName != null && newName.length() > 0) {
										entitySet.setName(newName);
									} else {
										// This should not happen. This is just
										// to avoid blank name generation.
										entitySet
												.setName(value
														+ Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME);
									}
								}
								UpdateContext context = new UpdateContext(pe3);
								IUpdateFeature updateFeature = fp
										.getUpdateFeature(context);
								IReason updateNeeded = updateFeature
										.updateNeeded(context);
								if (updateNeeded.toBoolean()) {
									updateFeature.execute(context);
								}
							}
						}
					}
				} else if (PropertyUtil.PROPERTY_RECTANGLE.equals(PropertyUtil
						.getID(shape))) {
					EList<Shape> propertyShapes = ((ContainerShape) shape)
							.getChildren();
					if (propertyShapes.size() > 0) {
						Shape propertyShape = propertyShapes.get(0);
						Shape pe1 = ((ContainerShape) propertyShape)
								.getChildren().get(1);
						Object currentObj = fp
								.getBusinessObjectForPictogramElement(pe1);
						if (ArtifactUtil.isKeyProperty(currentObj)) {
							Property property = (Property) currentObj;
							String prevEntitySetName = prevEntityName
									+ Messages.ODATAEDITOR_PROPERTIES_DEFAULT_NAME;
							if (prevEntitySetName.equalsIgnoreCase(property
									.getName())) {
								EList<Property> properties = entityType
										.getProperties();
								String propName = null;
								List<String> propertyNames = new ArrayList<String>();
								for (ListIterator<Property> it = properties
										.listIterator(); it.hasNext();) {
									Property prop = it.next();
									propName = prop.getName();
									propertyNames.add(propName);
								}
								String newName = ArtifactNameUtil
										.generateUniqueName(
												propertyNames,
												value,
												value
														+ Messages.ODATAEDITOR_PROPERTIES_DEFAULT_NAME,
												Messages.ODATAEDITOR_PROPERTIES_DEFAULT_NAME,
												IODataEditorConstants.MAX_LENGTH);
								if (newName != null && newName.length() > 0) {
									property.setName(newName);
								} else {
									// This should not happen. This is just to
									// avoid blank name generation.
									property.setName(value
											+ Messages.ODATAEDITOR_PROPERTIES_DEFAULT_NAME);
								}

								UpdateContext context = new UpdateContext(pe1);
								IUpdateFeature updateFeature = fp
										.getUpdateFeature(context);
								IReason updateNeeded = updateFeature
										.updateNeeded(context);
								if (updateNeeded.toBoolean()) {
									updateFeature.execute(context);
								}
							}
						}
					}
					break;
				}
			}
			AssociationUtil.refreshAssociationNamesForEntity(entityType,
					(ContainerShape) mainPictogramElement, fp, prevEntityName,
					value);
		}
	}

	/**
	 * This method is responsible to update properties in Properties Section.
	 * 
	 * @param fe
	 * @param currentObject
	 * @param shape
	 */
	public static void updateEntityProperties(IFeatureProvider fe,
			Object currentObject, Shape shape) {
		if (ArtifactUtil.isEntityType(currentObject)) {
			EntityType entity = (EntityType) currentObject;
			EList<Property> keys = entity.getKeys();
			List<Shape> keyShapes = new ArrayList<Shape>();
			PictogramElement topContainer = ODataShapeUtil
					.getTopContainer(shape);
			if (topContainer != null) {
				ContainerShape entityShape = (ContainerShape) topContainer;
				ContainerShape sectionShape = null;
				for (Shape childShape : entityShape.getChildren()) {
					if (PropertyUtil.PROPERTY_RECTANGLE.equals(PropertyUtil
							.getID(childShape))) {
						sectionShape = (ContainerShape) childShape;
						break;
					}
				}

				ContainerShape propertyShape = null;
				Shape imageShape = null;
				Image image = null;
				boolean status = false;
				if (sectionShape != null) {
					EList<Shape> propertyShapes = sectionShape.getChildren();
					for (Shape shape2 : propertyShapes) {
						propertyShape = (ContainerShape) shape2;
						imageShape = propertyShape.getChildren().get(0);
						image = (Image) imageShape.getGraphicsAlgorithm();
						status = image.getId().equals(
								ODataImageProvider.IMG_KEY_PROPERTY);
						if (status) {
							keyShapes.add(propertyShape);
						}
					}

					Object currentObj = null;
					String id = EMPTY_STRING;
					// update on properties
					if (keys.size() != keyShapes.size()) {
						for (Shape shape2 : propertyShapes) {
							propertyShape = (ContainerShape) shape2;
							imageShape = propertyShape.getChildren().get(0);
							image = (Image) imageShape.getGraphicsAlgorithm();
							currentObj = fe
									.getBusinessObjectForPictogramElement(propertyShape);
							if (currentObj instanceof Property) {
								IPropertyTypeUsage type = ((Property) currentObj)
										.getType();
								if (type instanceof SimpleTypeUsage) {
									boolean isCollection = ((SimpleTypeUsage) type)
											.isCollection();
									id = keys.contains(currentObj) ? ODataImageProvider.IMG_KEY_PROPERTY
											: isCollection ? ODataImageProvider.IMG_PROPERTY_COLLN
													: ODataImageProvider.IMG_PROPERTY;
									image.setId(id);
								} else if (type instanceof EnumTypeUsage) {
									boolean isCollection = ((EnumTypeUsage) type)
											.isCollection();
									id = keys.contains(currentObj) ? ODataImageProvider.IMG_KEY_PROPERTY
											: isCollection ? ODataImageProvider.IMG_ENUM_COLLN
													: ODataImageProvider.IMG_ENUM;
									image.setId(id);
								}

							}
						}
						// For Sorting key and Non key properties
						ECollections.sort(sectionShape.getChildren(),
								new ODataPropertyComparator(fe));

						layoutPictogramElement(topContainer, fe);

					}
				}
			}

		}
	}

	/**
	 * This method is responsible to check whether UpdateNeeded For properties
	 * in Properties Section In Entity
	 * 
	 * @param fe
	 * @param currentObject
	 * @param shape
	 * @return boolean
	 */
	public static boolean isPropertiesUpdateNeeded(IFeatureProvider fe,
			Object currentObject, Shape shape) {
		boolean isUpdateNeeded = false;
		if (ArtifactUtil.isEntityType(currentObject)) {
			EntityType entity = (EntityType) currentObject;
			EList<Property> keys = entity.getKeys();
			List<Shape> keyShapes = new ArrayList<Shape>();
			PictogramElement topContainer = ODataShapeUtil
					.getTopContainer(shape);
			if (topContainer != null) {
				ContainerShape entityShape = (ContainerShape) topContainer;

				ContainerShape sectionShape = null;
				for (Shape childShape : entityShape.getChildren()) {
					if (PropertyUtil.PROPERTY_RECTANGLE.equals(PropertyUtil
							.getID(childShape))) {
						sectionShape = (ContainerShape) childShape;
						break;
					}
				}

				ContainerShape propertyShape = null;
				Shape imageShape = null;
				Image image = null;
				boolean status = false;
				if (sectionShape != null) {
					EList<Shape> propertyShapes = sectionShape.getChildren();
					for (Shape shape2 : propertyShapes) {
						propertyShape = (ContainerShape) shape2;
						imageShape = propertyShape.getChildren().get(0);
						image = (Image) imageShape.getGraphicsAlgorithm();
						status = image.getId().equals(
								ODataImageProvider.IMG_KEY_PROPERTY);
						if (status) {
							keyShapes.add(propertyShape);
						}
					}

					// update on properties
					if (keys.size() != keyShapes.size()) {
						isUpdateNeeded = true;
					}
				}
			}

		}
		return isUpdateNeeded;
	}

	/**
	 * This method is responsible to update the PictogramElement. This method
	 * can be called with in the features to avoid extra command in Command
	 * stack.
	 * 
	 * @param pe
	 *            PictogramElement
	 * @param featureProvider
	 */
	public static void updatePictogramElement(PictogramElement pe,
			IFeatureProvider featureProvider) {
		UpdateContext updateContext = new UpdateContext(pe);
		IUpdateFeature updateFeature = featureProvider
				.getUpdateFeature(updateContext);
		if (updateFeature != null && updateFeature.canUpdate(updateContext)) {
			IReason updateNeeded = updateFeature.updateNeeded(updateContext);
			if (updateNeeded != null && updateNeeded.toBoolean()) {
				updateFeature.execute(updateContext);

			}
		}
	}

	/**
	 * This method is responsible to update the layout for specified
	 * PictogramElement.
	 * 
	 * @param pe
	 * @param featureProvider
	 * @return Reason
	 */
	public static IReason layoutPictogramElement(PictogramElement pe,
			IFeatureProvider featureProvider) {
		boolean status = false;
		LayoutContext layoutContext = new LayoutContext(pe);
		ILayoutFeature layoutFeature = featureProvider
				.getLayoutFeature(layoutContext);
		if (layoutFeature != null && layoutFeature.canLayout(layoutContext)) {
			layoutFeature.execute(layoutContext);
			status = true;
		}
		return new Reason(status);
	}

	/**
	 * @param context
	 * @param newObject
	 * @param featureProvider
	 */
	public static void addGraphicalRepresentation(IAreaContext context,
			Object newObject, IFeatureProvider featureProvider) {
		AddContext addContext = new AddContext(context, newObject);
		IAddFeature addFeature = featureProvider.getAddFeature(addContext);
		if (addFeature != null && addFeature.canAdd(addContext)) {
			addFeature.execute(addContext);
		}

	}

	/**
	 * @param context
	 * @param newObject
	 * @param featureProvider
	 */
	public static void addReferencedGraphicalRepresentation(
			IAreaContext context, Object newObject,
			IFeatureProvider featureProvider) {
		AddReferencedEntityContext addContext = new AddReferencedEntityContext(
				context, newObject);
		IAddFeature addFeature = featureProvider.getAddFeature(addContext);
		if (addFeature != null && addFeature.canAdd(addContext)) {
			addFeature.execute(addContext);
		}

	}

	/**
	 * @param diagram
	 * @param featureProvider
	 * @return boolean
	 */
	public static boolean deleteAllLinkUsage(Diagram diagram,
			IFeatureProvider featureProvider) {
		boolean isDeleted = false;

		EList<Connection> connections = diagram.getConnections();

		List<Connection> tempList = new ArrayList<Connection>();

		for (Connection connection : connections) {
			if (PropertyUtil.isLinkConnection(connection)) {
				tempList.add(connection);

			}
		}

		isDeleted = tempList.size() > 0 ? true : false;
		DeleteContext deleteContext = null;
		for (Connection linkConnection : tempList) {
			deleteContext = new DeleteContext(linkConnection);
			featureProvider.getDeleteFeature(deleteContext).execute(
					deleteContext);
		}
		return isDeleted;

	}

	/**
	 * Returns top container (EntityShape)
	 * 
	 * @param pe
	 * @return Top Container Shape.
	 */
	public static PictogramElement getTopContainer(PictogramElement pe) {
		if (pe == null) {
			return null;
		}

		if (pe instanceof Diagram) {
			return null;
		}

		if (PropertyUtil.isTopContainer(pe)) {
			return pe;
		}

		if (pe.eContainer() == null
				|| (!(pe.eContainer() instanceof PictogramElement))) {
			return null;
		}

		return getTopContainer((PictogramElement) pe.eContainer());

	}

	/**
	 * @param fp
	 * @param businessObject
	 * @return PictogramElement
	 */
	public static PictogramElement getTopContainerShape(IFeatureProvider fp,
			EObject businessObject) {
		PictogramElement pe = null;
		PictogramElement[] pes = fp
				.getAllPictogramElementsForBusinessObject(businessObject);
		for (PictogramElement pictogramElement : pes) {
			if (PropertyUtil.isTopContainer(pictogramElement)) {
				pe = pictogramElement;
				break;
			}
		}
		return pe;
	}

	/**
	 * This method is used to update the link usages
	 * 
	 * @param fp
	 * @param affectedPrevShowUsageObj
	 * @param affectedShowUsageObj
	 */
	public static void updateShowUsageLinks(IFeatureProvider fp,
			EObject affectedPrevShowUsageObj, EObject affectedShowUsageObj) {
		PictogramElement containerPrevShape = null;
		if (affectedShowUsageObj != null || affectedPrevShowUsageObj != null) {
			ODataLinkUsageFeature linkUsageFeature = new ODataLinkUsageFeature(
					fp, ((ODataFeatureProvider) fp).getArtifactFactory(),
					((ODataFeatureProvider) fp).getShapesFactory(), true);

			PictogramElement containerShape = ODataShapeUtil
					.getTopContainerShape(fp, affectedShowUsageObj);

			if (affectedPrevShowUsageObj != null) {

				containerPrevShape = ODataShapeUtil.getTopContainerShape(fp,
						affectedPrevShowUsageObj);
				callLinkUsageFeature(linkUsageFeature, containerPrevShape);

			}

			callLinkUsageFeature(linkUsageFeature, containerShape);

		}
	}

	/**
	 * This method creates the custom context to call link usage feature
	 * 
	 * @param linkUsageFeature
	 * @param containerShape
	 */
	public static void callLinkUsageFeature(
			ODataLinkUsageFeature linkUsageFeature,
			PictogramElement containerShape) {
		if (containerShape != null && PropertyUtil.isShowUsage(containerShape)) {

			CustomContext customContext = new CustomContext();
			customContext
					.setPictogramElements(new PictogramElement[] { containerShape });
			if (linkUsageFeature.canExecute(customContext)) {

				linkUsageFeature.execute(customContext);
			}
		}
	}

	/**
	 * This method is responsible to remove the referenced entities.
	 * 
	 * @param edmxReference
	 * @param graphicsEditor
	 */
	public static void removeReferencedEntityShapes(
			final EDMXReference edmxReference,
			final DiagramEditor graphicsEditor) {

		final EList<Shape> shapes = graphicsEditor.getDiagramTypeProvider()
				.getDiagram().getChildren();
		final IDiagramTypeProvider dtp = graphicsEditor
				.getDiagramTypeProvider();
		if (!shapes.isEmpty()) {
			List<Shape> tempList = new ArrayList<Shape>();
			tempList.addAll(shapes);
			for (Shape shape : tempList) {
				if (PropertyUtil.isTopContainer(shape)) {
					IFeatureProvider featureProvider = graphicsEditor
							.getDiagramTypeProvider().getFeatureProvider();
					Object businessObject = featureProvider
							.getBusinessObjectForPictogramElement(shape);
					if (isReferencedShape(businessObject, edmxReference)) {
						DeleteContext deleteContext = new DeleteContext(shape);
						// Added to avoid pop-up dialog.
						deleteContext.setMultiDeleteInfo(new MultiDeleteInfo(
								false, false, 1));
						IDeleteFeature deleteFeature = dtp.getFeatureProvider()
								.getDeleteFeature(deleteContext);
						if (deleteFeature != null
								&& deleteFeature.canDelete(deleteContext))
							deleteFeature.execute(deleteContext);
					}
				}
			}
		}
	}

	/**
	 * @param businessObject
	 * @param edmxReference
	 * @return boolean
	 */
	public static boolean isReferencedShape(Object businessObject,
			EDMXReference edmxReference) {
		Schema schema = ArtifactUtil.getSchema((EObject) businessObject);
		return edmxReference.getReferencedEDMX().getDataService().getSchemata()
				.contains(schema);
	}

	/**
	 * @param currentObject
	 * @param shape
	 * @return boolean Varaiable.
	 */
	public static boolean isPropertyUpdateNeeded(EObject currentObject,
			Shape shape) {
		boolean status = false;
		if (ArtifactUtil.isFunctionImportReturnType(currentObject)
				|| PropertyUtil.isProperty(shape)) {

			if (shape.getGraphicsAlgorithm() instanceof Image) {

				Image currentImage = (Image) shape.getGraphicsAlgorithm();
				String id = currentImage.getId();
				if (ArtifactUtil.isCollectionReturnType(currentObject)) {
					if (currentObject instanceof SimpleTypeUsageImpl) {
						status = !(id
								.equals(ODataImageProvider.IMG_PROPERTY_COLLN));
					} else if (currentObject instanceof ComplexTypeUsageImpl) {
						status = !(id
								.equals(ODataImageProvider.IMG_CPLX_TYP_COLLN));
					} else if (currentObject instanceof EnumTypeUsageImpl) {
						status = !(id.equals(ODataImageProvider.IMG_ENUM_COLLN));
					} else if (currentObject instanceof ReturnEntityTypeUsageImpl) {
						status = !(id
								.equals(ODataImageProvider.IMG_ENTY_SET_COLLN));
					}
				} else {
					if (currentObject instanceof SimpleTypeUsageImpl) {
						boolean isKey = false;
						EObject property = ((SimpleTypeUsage) currentObject)
								.eContainer();
						if (property instanceof Property
								&& property.eContainer() instanceof EntityType) {
							EntityType entityType = (EntityType) property
									.eContainer();
							isKey = entityType.getKeys().contains(property);
						}
						status = !(isKey ? id
								.equals(ODataImageProvider.IMG_KEY_PROPERTY)
								: (id.equals(ODataImageProvider.IMG_PROPERTY)));
					} else if (currentObject instanceof ComplexTypeUsageImpl) {
						status = !(id.equals(ODataImageProvider.IMG_CMPLX_TYPE));
					} else if (currentObject instanceof EnumTypeUsageImpl) {

						boolean isKey = false;
						EObject property = ((EnumTypeUsage) currentObject)
								.eContainer();
						if (property instanceof Property
								&& property.eContainer() instanceof EntityType) {
							EntityType entityType = (EntityType) property
									.eContainer();
							isKey = entityType.getKeys().contains(property);
						}
						status = !(isKey ? id
								.equals(ODataImageProvider.IMG_KEY_PROPERTY)
								: (id.equals(ODataImageProvider.IMG_ENUM)));
					} else if (currentObject instanceof ReturnEntityTypeUsageImpl) {
						status = !(id.equals(ODataImageProvider.IMG_ENTITY));
					}
				}

			}
		}

		return status;
	}
}
