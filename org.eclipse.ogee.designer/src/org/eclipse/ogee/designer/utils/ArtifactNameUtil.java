/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This Class contains utility method for generating unique name for copied
 * business objects.
 * 
 */
public class ArtifactNameUtil {

	/**
	 * This method will generate unique name with restricted length for copied
	 * business object.
	 * 
	 * @param list of artifact names
	 * @param artifact name
	 * @param restricted length
	 * @return unique name
	 */

	public static String generateUniqueName(List<String> names, String name,
			int res_length) {

		String uniqueName = name;
		int len;
		int offset = 1;
		int j = 10;
		int k = 1;
		int i;
		if (names.size() == 0) {
			// if the objectList is empty then return first value
			return uniqueName;
		}

		for (i = 2; names.contains(uniqueName); i++) {
			len = uniqueName.length();
			if (len > (res_length - 1)) {
				// if the length is greater than restricted length start
				// deleting characters from the end of string
				if (i == Math.pow(j, k)) {
					offset = offset + k;
				}
				uniqueName = uniqueName.substring(0, res_length - offset);
				uniqueName = uniqueName + i;
			} else {
				// if the length is less than restricted length then increment
				// the counter value
				uniqueName = name + i;
			}
		}

		if (isNumeric(uniqueName.substring(0, 1))) {
			// name shouldn't start with a number
			return name;
		}
		return uniqueName;
	}

	private static boolean isNumeric(String str) {

		try {
			Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
			return false;
		}

		return true;
	}

	/**
	 * This method will generate unique name generation with suffix
	 * 
	 * @param names
	 * @param entityName
	 * @param defaultName
	 * @param suffix
	 * @return unique name
	 */
	public static String generateUniqueName(List<String> names,
			String entityName, String defaultName, String suffix, int res_length) {
		int offset;
		String tempName = defaultName;
		String tempEntityName = entityName;
		String newSuffix = spliter(tempName, suffix);
		while (tempName.length() > res_length) {
			tempName = defaultName;
			offset = 1;
			tempEntityName = tempEntityName.substring(0,
					tempEntityName.length() - offset);
			newSuffix = spliter(tempName, suffix);
			tempName = tempEntityName + newSuffix;
			offset++;
		}
		while (names.contains(tempName)) {
			String tempSuffix = incrementer(newSuffix, suffix);
			tempName = tempEntityName + tempSuffix;
			while (tempName.length() > res_length) {
				offset = 1;
				tempEntityName = tempEntityName.substring(0,
						tempEntityName.length() - offset);
				newSuffix = spliter(tempName, suffix);
				tempName = tempEntityName + newSuffix;
				offset++;
			}
			newSuffix = tempSuffix;
		}
		return tempName;
	};

	private static String spliter(String setName, String suffix) {
		// splits the name into set with first part as entityName and other part
		// as suffix
		String result = null;
		String pattern = suffix;
		Pattern regex = Pattern.compile(pattern);
		Matcher match = regex.matcher(setName);
		if (match.find()) {
			result = setName.substring(match.start(), setName.length());
		}
		return result;
	}

	private static String incrementer(String suffix, String actualSuffix) {
		// increments the value of counter if name is not unique
		String name = null;
		Pattern numPattern = Pattern.compile("-?\\d+"); //$NON-NLS-1$
		Matcher match = numPattern.matcher(suffix);
		if (match.find()) {
			name = actualSuffix + (Integer.parseInt(match.group()) + 1);
		} else {
			name = suffix + "2"; //$NON-NLS-1$
		}
		return name;
	}

}