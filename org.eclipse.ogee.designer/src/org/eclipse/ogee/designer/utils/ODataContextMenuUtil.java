/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.contextmenu.IContextMenu;
import org.eclipse.ogee.utils.logger.Logger;

/**
 * This class will be used for Context Menu utility to retrieve all
 * implementations of context menu extension point.
 * 
 * 
 */
public class ODataContextMenuUtil {
	private static String[][] contextMenuOptions = null;

	public static IConfigurationElement[] getContextMenuExtensions() {

		IConfigurationElement[] extensionMenuConfigElems = Platform
				.getExtensionRegistry().getConfigurationElementsFor(
						IODataEditorConstants.CONTEXT_MENU_EXTENSIONPOINT_ID);
		
		IConfigurationElement[] extensionBindingMenuConfigElems = Platform
				.getExtensionRegistry().getConfigurationElementsFor(
						IODataEditorConstants.CONTEXT_MENU_BINDING_EXTENSIONPOINT_ID);
		
		int arrLen = extensionMenuConfigElems.length + extensionBindingMenuConfigElems.length;
		IConfigurationElement[] extensionTotalMenuConfigElems = new IConfigurationElement[arrLen];
		
		for( int index = 0 ; index < extensionMenuConfigElems.length ; index++){
			extensionTotalMenuConfigElems[index] = extensionMenuConfigElems[index];
		}
		for( int index = 0 ; index < extensionBindingMenuConfigElems.length ; index++){
			extensionTotalMenuConfigElems[extensionMenuConfigElems.length + index] = extensionBindingMenuConfigElems[index];
		}
		
		return extensionTotalMenuConfigElems;
	}

	/**
	 * Returns Current Context Menu.
	 * 
	 * @param contextMenuId
	 * 
	 * @return IContextMenu Instance.
	 */
	public static IContextMenu getCurrentContextMenu(String contextMenuId) {
		IContextMenu currentContextMenu = null;
		String id = null;
		String contextMenuID = contextMenuId;

		try {
			IConfigurationElement[] contextMenuConfigElems = getContextMenuExtensions();
			for (IConfigurationElement iConfigurationElement : contextMenuConfigElems) {
				id = iConfigurationElement
						.getAttribute(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_ID);
				if (id.equals(contextMenuID)) {
					currentContextMenu = (IContextMenu) iConfigurationElement
							.createExecutableExtension(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_CLASS);
					break;
				}

			}
		} catch (CoreException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		}

		return currentContextMenu;
	}

	/**
	 * This method is used in creating UI in Editor Context Menu. Creates a
	 * String Array with name,id,parent_name,icon & key_binding with all
	 * available context menus
	 * 
	 * 
	 * @return String Array.
	 */
	public static String[][] getContextMenuOptions() {
		String id = null;
		String className = null;
		String displayName = null;
		String key_binding = null;
		String icon = null;
		String parent_name = null;
		String name = null;
		String plugin_id = null;
		IContextMenu currentContextMenu = null;

		if (contextMenuOptions == null) {
			IConfigurationElement[] contextMenuExtensions = getContextMenuExtensions();
			Map<String, IConfigurationElement> filteredExtensions = new LinkedHashMap<String, IConfigurationElement>();
			for (IConfigurationElement iConfigurationElement : contextMenuExtensions) {
				id = iConfigurationElement
						.getAttribute(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_ID);
				className = iConfigurationElement
						.getAttribute(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_CLASS);
				displayName = iConfigurationElement
						.getAttribute(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_NAME);
				if (id != null && !id.isEmpty() && className != null
						&& !className.isEmpty() && displayName != null
						&& !displayName.isEmpty()) {
					try {
						currentContextMenu = (IContextMenu) iConfigurationElement
								.createExecutableExtension(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_CLASS);
						if (currentContextMenu != null) {
							filteredExtensions.put(id, iConfigurationElement);
						}

					} catch (CoreException e) {
						Logger.getLogger(Activator.PLUGIN_ID).logError(e);
					}

				}
			}
			int counter = 0;
			Collection<String> filteredKeys = filteredExtensions.keySet();
			contextMenuOptions = new String[filteredKeys.size()][6];
			IConfigurationElement iConfigurationElement = null;
			for (String key : filteredKeys) {
				iConfigurationElement = filteredExtensions.get(key);
				name = iConfigurationElement
						.getAttribute(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_NAME);
				contextMenuOptions[counter][0] = name;
				id = iConfigurationElement
						.getAttribute(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_ID);
				contextMenuOptions[counter][1] = id;
				parent_name = iConfigurationElement
						.getAttribute(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_PARENT_NAME);
				contextMenuOptions[counter][2] = parent_name;
				icon = iConfigurationElement
						.getAttribute(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_ICON);
				contextMenuOptions[counter][3] = icon;
				plugin_id = iConfigurationElement.getContributor().getName();
				contextMenuOptions[counter][4] = plugin_id;
				key_binding = iConfigurationElement
						.getAttribute(IODataEditorConstants.CONTEXT_MENU_ATTRIBUTE_KEY_BINDING);
				contextMenuOptions[counter++][5] = key_binding;
			}
		}
		return contextMenuOptions;
	}

}
