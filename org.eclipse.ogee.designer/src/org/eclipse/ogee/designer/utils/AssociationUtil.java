/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.CreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.DirectEditingContext;
import org.eclipse.graphiti.mm.algorithms.RoundedRectangle;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.ogee.designer.contexts.AssociationAddConnectionContext;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataFeatureProvider;
import org.eclipse.ogee.designer.types.AssociationType;
import org.eclipse.ogee.designer.types.MultiplicityType;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Multiplicity;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.swt.graphics.Point;

/**
 * This Class contains utility methods for business object Association.
 * 
 */
public class AssociationUtil {

	/**
	 * Takes the association and calls to change the positions of the existing
	 * bend points by 40 px to create a new visualization
	 * 
	 * @param conn
	 *            Association
	 */
	public static void drawSelfAssociation(Connection conn, int size) {
		if (conn instanceof FreeFormConnection) {
			// Only if a free form connection was used can
			// the bend point logic be used
			FreeFormConnection ffc = (FreeFormConnection) conn;
			List<org.eclipse.graphiti.mm.algorithms.styles.Point> origBendPoints = ffc
					.getBendpoints();
			// Create new points for the new line visualization
			List<org.eclipse.swt.graphics.Point> newBendPoints = createBendPoints(
					ffc, size);

			if (newBendPoints == null || newBendPoints.size() == 0) {
				return;
			}
			// Set the new x,y positions for the existing bend points
			// of the association to give desired look and feel
			for (int i = 0; i < Math.min(origBendPoints.size(),
					newBendPoints.size() - 1); i++) {
				origBendPoints.set(
						i,
						Graphiti.getGaService().createPoint(
								newBendPoints.get(i + 1).x,
								newBendPoints.get(i + 1).y));
			}
			if ((origBendPoints.size() - newBendPoints.size()) > 0) {
				for (int i = origBendPoints.size() - 1; i >= newBendPoints
						.size(); i--) {
					origBendPoints.remove(i);
				}
			} else if ((origBendPoints.size() - newBendPoints.size()) < 0) {
				for (int i = origBendPoints.size() + 1; i < newBendPoints
						.size() - 1; i++) {
					origBendPoints.add(Graphiti.getGaService().createPoint(
							newBendPoints.get(i).x, newBendPoints.get(i).y));
				}
			}
		}
	}

	/**
	 * This method will take a Connection(Association) and create a midpoint for
	 * it which will then be moved up/down to create the impression of separate
	 * associations visually
	 * 
	 * @param c
	 *            FreeFormConection
	 * @return Point Midpoint of the association line
	 */
	public static Point getSimpleBendPoint(FreeFormConnection c) {
		RoundedRectangle entOuterShape = (RoundedRectangle) c.getStart()
				.getParent().getGraphicsAlgorithm();
		RoundedRectangle destEntOuterShape = (RoundedRectangle) c.getEnd()
				.getParent().getGraphicsAlgorithm();
		int numberOfConnections = getRelatedConnection(c.getStart(), c.getEnd());

		if (numberOfConnections > 0) {
			float entWidth = entOuterShape.getWidth() / 2.0f;
			float entHeight = entOuterShape.getHeight() / 2.0f;
			float destEntWidth = destEntOuterShape.getWidth() / 2.0f;
			float destEntHeight = destEntOuterShape.getHeight() / 2.0f;

			float centerX = entWidth + entOuterShape.getX();
			float centerY = entHeight + entOuterShape.getY();
			float destCenterX = destEntWidth + destEntOuterShape.getX() - 2;
			float destCenterY = destEntHeight + destEntOuterShape.getY();

			int x, y = 0;
			// Midpoint
			x = Math.round(centerX + (destCenterX - centerX) / 2);
			y = Math.round(centerY + (destCenterY - centerY) / 2);
			int rightY = y;
			if (numberOfConnections > 1) {
				rightY = numberOfConnections % 2 == 0 ? y - numberOfConnections
						* 20 : y + numberOfConnections * 20;
			}
			int rightX = x;
			if (numberOfConnections > 1) {
				rightX = numberOfConnections % 2 == 0 ? x - numberOfConnections
						* 10 : x + numberOfConnections * 10;
			}
			Point midPoint = new Point(rightX, rightY);
			return midPoint;
		}
		return null;

	}

	/**
	 * This method will take a Connection(Association) and create a midpoint for
	 * it which will then be moved up/down to create the impression of separate
	 * associations visually
	 * 
	 * @param c
	 *            FreeFormConection
	 * @return Point Midpoint of the association line
	 */
	public static Point addBendPoint(FreeFormConnection c) {
		RoundedRectangle entOuterShape = (RoundedRectangle) c.getStart()
				.getParent().getGraphicsAlgorithm();
		RoundedRectangle destEntOuterShape = (RoundedRectangle) c.getEnd()
				.getParent().getGraphicsAlgorithm();

		List<Connection> allConnections = getRelatedConnections(c.getStart(),
				c.getEnd());

		int index = allConnections.indexOf(c);
		if (index != -1) {
			index = index + 1;
		}
		if (index > 1) {
			float entWidth = entOuterShape.getWidth() / 2.0f;
			float entHeight = entOuterShape.getHeight() / 2.0f;
			float destEntWidth = destEntOuterShape.getWidth() / 2.0f;
			float destEntHeight = destEntOuterShape.getHeight() / 2.0f;

			float centerX = entWidth + entOuterShape.getX();
			float centerY = entHeight + entOuterShape.getY();
			float destCenterX = destEntWidth + destEntOuterShape.getX() - 2;
			float destCenterY = destEntHeight + destEntOuterShape.getY();

			int x, y = 0;
			// Midpoint
			x = Math.round(centerX + (destCenterX - centerX) / 2);
			y = Math.round(centerY + (destCenterY - centerY) / 2);
			int rightY = y;
			if (index > 1) {
				rightY = index % 2 == 0 ? y - index * 20 : y + index * 20;
			}
			int rightX = x;
			if (index > 1) {
				rightX = index % 2 == 0 ? x - index * 10 : x + index * 10;
			}
			Point midPoint = new Point(rightX, rightY);
			return midPoint;
		}
		return null;

	}

	/**
	 * Returns true if the connection between given two entities is first
	 * between the two In this case no visual changes need to be done
	 * 
	 * @param sourceShape
	 * @param targetShape
	 * @return
	 */
	public static boolean isFirstConnection(Anchor sourceShape,
			Anchor targetShape) {
		List<Connection> incomingConnections = sourceShape
				.getIncomingConnections();
		for (Connection connection : incomingConnections) {
			if (connection.getStart().getParent() == targetShape.getParent()) {
				return false;
			}
		}
		List<Connection> outgoingConnections = sourceShape
				.getOutgoingConnections();
		for (Connection connection : outgoingConnections) {
			if (connection.getEnd().getParent() == targetShape.getParent()) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns number of connections between given two entities
	 * 
	 * @param sourceShape
	 * @param targetShape
	 * @return int Number of existing Connections
	 */
	public static int getConnections(Anchor sourceShape, Anchor targetShape) {
		if (sourceShape.getParent() == targetShape.getParent()) {
			return getSelfAssociations((ContainerShape) sourceShape.getParent())
					.size();
		} else {
			return getRelatedConnection(sourceShape, targetShape);
		}
	}

	/**
	 * Returns the bendpoint
	 * 
	 * @param connection
	 * @return
	 */
	public static Point getBendPoint(FreeFormConnection connection) {
		return getSimpleBendPoint(connection);
	}

	/**
	 * Returns the number of connections between two objects
	 * 
	 * @param sourceShape
	 * @param targetShape
	 * @return int
	 */
	public static int getRelatedConnection(Anchor sourceShape,
			Anchor targetShape) {

		int count = 0;
		List<Connection> incomingConnections = sourceShape
				.getIncomingConnections();
		for (Connection connection : incomingConnections) {
			if (connection.getStart().getParent() == targetShape.getParent()) {
				count++;
			}
		}
		List<Connection> outgoingConnections = sourceShape
				.getOutgoingConnections();
		for (Connection connection : outgoingConnections) {
			if (connection.getEnd().getParent() == targetShape.getParent()) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Returns the connections between two objects
	 * 
	 * @param sourceShape
	 * @param targetShape
	 * @return List<Connection>
	 */
	public static List<Connection> getRelatedConnections(Anchor sourceShape,
			Anchor targetShape) {

		List<Connection> finalList = new ArrayList<Connection>();
		List<Connection> incomingConnections = sourceShape
				.getIncomingConnections();
		for (Connection connection : incomingConnections) {
			if (connection.getStart().getParent() == targetShape.getParent()) {
				finalList.add(connection);
			}
		}
		List<Connection> outgoingConnections = sourceShape
				.getOutgoingConnections();
		for (Connection connection : outgoingConnections) {
			if (connection.getEnd().getParent() == targetShape.getParent()) {
				finalList.add(connection);
			}
		}

		if (finalList.size() > 1) {
			Collections.sort(finalList, new Comparator<Connection>() {
				@Override
				public int compare(Connection arg0, Connection arg1) {
					return arg0.hashCode() - arg1.hashCode();
				}
			});
		}

		return finalList;
	}

	/**
	 * Returns the connections between two objects
	 * 
	 * @param sourceShape
	 * @param targetShape
	 * @return List<Connection>
	 */
	public static List<Connection> getRelatedConnections(
			AnchorContainer sourceShape, AnchorContainer targetShape) {

		List<Connection> finalList = new ArrayList<Connection>();
		EList<Anchor> sourceAnchors = sourceShape.getAnchors();
		for (Anchor sourceAnchor : sourceAnchors) {
			List<Connection> incomingConnections = sourceAnchor
					.getIncomingConnections();
			for (Connection connection : incomingConnections) {
				if (connection.getStart().getParent() == targetShape) {
					finalList.add(connection);
				}
			}

			List<Connection> outgoingConnections = sourceAnchor
					.getOutgoingConnections();
			for (Connection connection : outgoingConnections) {
				if (connection.getEnd().getParent() == targetShape) {
					finalList.add(connection);
				}
			}

		}

		if (finalList.size() > 1) {
			Collections.sort(finalList, new Comparator<Connection>() {
				@Override
				public int compare(Connection arg0, Connection arg1) {
					return arg0.hashCode() - arg1.hashCode();
				}
			});
		}

		return finalList;
	}

	/**
	 * Returns the number of connections for an object
	 * 
	 * @param sourceShape
	 * @param targetShape
	 * @return int
	 */
	public static int getConnections(Anchor sourceShape) {

		List<Connection> incomingConnections = sourceShape
				.getIncomingConnections();

		List<Connection> outgoingConnections = sourceShape
				.getOutgoingConnections();

		return incomingConnections.size() + outgoingConnections.size();
	}

	/**
	 * Returns the first connection between given two enities
	 * 
	 * @param sourceShape
	 * @param targetShape
	 * @return
	 */
	public static Connection getFirstConnection(Anchor sourceShape,
			Anchor targetShape) {
		Connection firstConnection = null;
		EList<Connection> incomingSourceConnections = sourceShape
				.getIncomingConnections();
		EList<Connection> outgoingTargetConnections = targetShape
				.getOutgoingConnections();
		EList<Connection> outgoingSourceConnections = sourceShape
				.getOutgoingConnections();
		EList<Connection> incomingtargetConnections = targetShape
				.getIncomingConnections();
		if (!incomingSourceConnections.isEmpty()
				&& !outgoingTargetConnections.isEmpty()) {

			if (incomingSourceConnections.get(0) == outgoingTargetConnections
					.get(0)) {
				firstConnection = incomingSourceConnections.get(0);
			}
		} else if (!outgoingSourceConnections.isEmpty()
				&& !incomingtargetConnections.isEmpty()) {

			if (outgoingSourceConnections.get(0) == incomingtargetConnections
					.get(0)) {
				firstConnection = outgoingSourceConnections.get(0);
			}
		}

		return firstConnection;
	}

	/**
	 * Creates bendpoints for a self association at three 90 degree turns
	 * 
	 * @param c
	 *            FreeFormConnection
	 * @return List<Point> the three bend points
	 */
	public static List<Point> createBendPoints(FreeFormConnection c,
			int selfAssocSize) {
		RoundedRectangle entOuterShape = (RoundedRectangle) c.getStart()
				.getParent().getGraphicsAlgorithm();

		float entW = entOuterShape.getWidth() / 2.0f;
		float entH = entOuterShape.getHeight() / 2.0f;
		int centerX = Math.round(entW + entOuterShape.getX());
		int centerY = Math.round(entH + entOuterShape.getY());

		int x, y;
		List<Point> newPoints = new ArrayList<Point>();
		// Original End point
		x = centerX - Math.round(entW) - 1;
		y = centerY;
		if (selfAssocSize > 0) {
			y = y - (selfAssocSize * 5);
		}

		Point endPoint = new Point(x, y);

		// Original Start point
		x = centerX;
		if (selfAssocSize > 0) {
			x = x + (selfAssocSize * 5);
		}
		y = centerY + Math.round(entH);

		Point startPoint = new Point(x, y);
		newPoints.add(startPoint);

		// First bend
		x = startPoint.x;
		if (selfAssocSize > 0) {
			x = x + (selfAssocSize * 5);
		}
		y = startPoint.y + 18;
		if (selfAssocSize > 0) {
			y = y + (selfAssocSize * 5);
		}
		newPoints.add(new Point(x, y));
		// Second bend, only x changes
		x = (x - Math.round(entW + 18));
		if (selfAssocSize > 0) {
			x = x - (selfAssocSize * 18);
		}
		y = y + (selfAssocSize * 10);
		newPoints.add(new Point(x, y));
		// End point, only y changes

		y = endPoint.y;
		if (selfAssocSize > 0) {
			y = y - (selfAssocSize * 12);
		}
		newPoints.add(new Point(x, y));

		newPoints.add(endPoint);
		return newPoints;
	}

	/**
	 * Returns the multiplicity value for the association end
	 * 
	 * @param context
	 * @param isSource
	 * @return
	 */
	public static MultiplicityType getMultiplicity(IAddContext context,
			boolean isSource) {
		if (context instanceof AssociationAddConnectionContext) {
			if (isSource) {
				MultiplicityType mEnum = ((AssociationAddConnectionContext) context)
						.getSourceMultiplicity();
				return mEnum;
			} else {
				MultiplicityType mEnum = ((AssociationAddConnectionContext) context)
						.getTargetMultiplicity();
				return mEnum;
			}
		}
		return null;
	}

	public static MultiplicityType getMultiplicityEnum(Multiplicity mEnum) {
		switch (mEnum) {
		case ONE:
			return MultiplicityType.ONE;
		case MANY:
			return MultiplicityType.MANY;
		case ZERO_TO_ONE:
			return MultiplicityType.ZEROTOONE;
		default:
			return null;
		}
	}

	/**
	 * Gets the value for the multiplicity
	 * 
	 * @param multiplicity
	 * @return
	 */
	public static String getMultiplicityValue(MultiplicityType multiplicity) {
		switch (multiplicity) {
		case ONE:
			return MultiplicityType.ONE_VALUE;
		case MANY:
			return MultiplicityType.MANY_VALUE;
		case ZEROTOONE:
			return MultiplicityType.ZEROTOONE_VALUE;
		default:
			return MultiplicityType.ONE_VALUE;
		}
	}

	/**
	 * Returns all self associations for the given entity
	 * 
	 * @param container
	 *            entity
	 * @return List<Connection> SelfAssociations
	 */
	public static List<Connection> getSelfAssociations(ContainerShape container) {

		List<Connection> selfConnectionList = new ArrayList<Connection>();

		List<Connection> connectionList = getAllAssociations(container);
		for (Connection connection : connectionList) {
			if (connection.getStart().getParent() == connection.getEnd()
					.getParent()) {
				if (!(selfConnectionList.contains(connection))) {
					selfConnectionList.add(connection);
				}
			}

		}
		return selfConnectionList;
	}

	/**
	 * Returns all associations for the given entity
	 * 
	 * @param container
	 *            entity
	 * @return List<Connection> Associations
	 */
	public static List<Connection> getAllAssociations(ContainerShape container) {
		List<Anchor> anchorList = container.getAnchors();
		UniqueEList<Connection> connectionList = new UniqueEList<Connection>();
		for (Anchor anchor : anchorList) {
			connectionList.addAll(anchor.getOutgoingConnections());
			connectionList.addAll(anchor.getIncomingConnections());
		}
		return connectionList;
	}

	/**
	 * Returns all associations for the given entity
	 * 
	 * @param container
	 *            entity
	 * @return List<Connection> Associations
	 */
	public static List<Connection> getOutgoingAssociations(
			ContainerShape container) {
		List<Anchor> anchorList = container.getAnchors();
		UniqueEList<Connection> connectionList = new UniqueEList<Connection>();
		for (Anchor anchor : anchorList) {
			EList<Connection> outgoingConnections = anchor
					.getOutgoingConnections();
			for (Connection connection : outgoingConnections) {
				if (!PropertyUtil.isLinkConnection(connection)) {
					if (connection.getEnd().eContainer() != connection
							.getStart().eContainer()) {
						connectionList.add(connection);
					}
				}

			}
		}
		return connectionList;
	}

	/**
	 * Returns all associations for the given entity
	 * 
	 * @param container
	 *            entity
	 * @return List<Connection> Associations
	 */
	public static List<Connection> getIncomingAssociations(
			ContainerShape container) {
		List<Anchor> anchorList = container.getAnchors();
		UniqueEList<Connection> connectionList = new UniqueEList<Connection>();
		for (Anchor anchor : anchorList) {
			EList<Connection> incomingConnections = anchor
					.getIncomingConnections();
			for (Connection connection : incomingConnections) {
				if (!PropertyUtil.isLinkConnection(connection)) {
					if (connection.getEnd().eContainer() != connection
							.getStart().eContainer()) {
						connectionList.add(connection);
					}
				}

			}
		}
		return connectionList;
	}

	/**
	 * Updates the UI when the entities are resized to reposition the seelf
	 * association the right way
	 * 
	 * @param container
	 *            entity
	 */
	public static void updateSelfAssociationLayout(ContainerShape container) {

		List<Connection> selfAssociations = getSelfAssociations(container);
		int i = 0;
		for (Iterator<Connection> iterator = selfAssociations.iterator(); iterator
				.hasNext();) {
			Connection connection = (Connection) iterator.next();
			drawSelfAssociation(connection, i++);

		}
	}

	/**
	 * Update the associations anchors
	 * 
	 * @param context
	 * @return ICreateConnectionContext
	 */
	public static ICreateConnectionContext updateAssociationContext(
			ICreateConnectionContext context) {

		if (context != null) {
			PictogramElement sourcePictogramElement = ODataShapeUtil
					.getTopContainer(context.getSourcePictogramElement());
			PictogramElement targetPictogramElement = ODataShapeUtil
					.getTopContainer(context.getTargetPictogramElement());
			if (sourcePictogramElement != null
					&& targetPictogramElement != null) {
				// get EClasses which should be connected
				Anchor sourceAnchor = context.getSourceAnchor();
				if (sourceAnchor == null
						&& sourcePictogramElement instanceof AnchorContainer) {
					sourceAnchor = Graphiti.getPeService().getChopboxAnchor(
							(AnchorContainer) sourcePictogramElement);

				}
				// get EClasses which should be connected
				Anchor targetAnchor = context.getTargetAnchor();
				if (targetAnchor == null
						&& targetPictogramElement instanceof AnchorContainer) {
					targetAnchor = Graphiti.getPeService().getChopboxAnchor(
							(AnchorContainer) targetPictogramElement);
				}
				((CreateConnectionContext) context)
						.setSourceAnchor(sourceAnchor);
				((CreateConnectionContext) context)
						.setTargetAnchor(targetAnchor);
			}
		}

		return context;
	}

	/**
	 * Refreshes the name for the association in case of a name change in Entity
	 * 
	 * @param entityType
	 * @param container
	 * @param fp
	 * @param oldEntityName
	 * @param newEntityName
	 */
	public static void refreshAssociationNamesForEntity(EntityType entityType,
			ContainerShape container, IFeatureProvider fp,
			String oldEntityName, String newEntityName) {

		List<Connection> associations = getAllAssociations(container);
		for (Iterator<Connection> iterator = associations.iterator(); iterator
				.hasNext();) {
			Connection connection = iterator.next();
			// to skip the link connections
			if (PropertyUtil.isLinkConnection(connection)) {
				continue;
			}
			// Source entity with the updated name
			EntityType sourceEntity = (EntityType) (fp
					.getBusinessObjectForPictogramElement(connection.getStart()
							.getParent()));
			// Target entity with the updated name
			EntityType targetEntity = (EntityType) (fp
					.getBusinessObjectForPictogramElement(connection.getEnd()
							.getParent()));
			// Association for the connection pictogram element
			Association association = (Association) (fp
					.getBusinessObjectForPictogramElement(connection));
			Role dependentRole = null;
			Role principalRole = null;
			// Added to check whether the name of one of the entity is changed
			// or not
			if (!(oldEntityName.equalsIgnoreCase(newEntityName))) {

				String assocName = association.getName();
				if (assocName
						.startsWith(sourceEntity.getName() + oldEntityName)
						|| assocName.startsWith(oldEntityName
								+ targetEntity.getName())
						|| assocName.startsWith(oldEntityName + oldEntityName)) {
					// Set the updated name of the association
					String newName = ArtifactUtil.getAssociationName(
							sourceEntity, targetEntity,
							((ODataFeatureProvider) fp).getArtifactFactory()
									.getSchema());
					association.setName(newName);
					updateAssociationSetName(association, fp);

					// Role Update
					// isAlreadySet flag is taken for the bidirectional
					// self-association
					// as both the time dependent role was set only
					// logic works for other kind of associations as well
					boolean isAlreadySet = false;
					for (Role currentRole : association.getEnds()) {
						if (currentRole.getType() == targetEntity
								&& isAlreadySet == false) {
							isAlreadySet = true;
							dependentRole = currentRole;
							if (targetEntity.getName() != null) {
								if (targetEntity.getName().length() > 125) {
									String targetEntityName = targetEntity
											.getName().substring(0, 124);
									currentRole
											.setName(Messages.ODATAEDITOR_ROLE_DEFAULTTO
													+ targetEntityName);
								}
							}

						} else {
							principalRole = currentRole;
							if (sourceEntity.getName() != null) {
								if (sourceEntity.getName().length() > 123) {
									String sourceEntityName = sourceEntity
											.getName().substring(0, 124);
									currentRole
											.setName(Messages.ODATAEDITOR_ROLE_DEFAULTFROM
													+ sourceEntityName);
								}
							}

						}
					}
				}
				// Target entity name is changed
				if (targetEntity.getName() == newEntityName) {
					navPropertyNameChange(fp, sourceEntity, targetEntity,
							association, dependentRole, principalRole,
							oldEntityName);
					// This case is only valid for the change of source entity
					// of bidirectional association
				} else if (sourceEntity.getName() == newEntityName) {
					navPropertyNameChange(fp, targetEntity, sourceEntity,
							association, principalRole, null, oldEntityName);
				}
			}
		}

	}

	/**
	 * This method is used to get the new navigation property name with the
	 * change of the entity type name and also set the new name of the
	 * navigation property
	 * 
	 * @param fp
	 * @param sourceEntity
	 * @param targetEntity
	 * @param association
	 * @param dependentRole
	 */
	private static void navPropertyNameChange(IFeatureProvider fp,
			EntityType sourceEntity, EntityType targetEntity,
			Association association, Role dependentRole, Role principalRole,
			String oldEntityName) {

		PictogramElement entityPE = fp
				.getPictogramElementForBusinessObject(sourceEntity);
		EList<Shape> children = ((ContainerShape) entityPE).getChildren();
		List<Shape> tempchildrenList = new ArrayList<Shape>();
		tempchildrenList.addAll(children);
		// This flag is taken for the self association scenario
		boolean isFirstTime = true;
		for (Shape shape : tempchildrenList) {
			if (PropertyUtil.NAVIGATIONPROPERTY_RECTANGLE.equals(PropertyUtil
					.getID(shape))) {
				EList<Shape> navPropertychildren = ((ContainerShape) shape)
						.getChildren();
				List<Shape> tempShapeList = new ArrayList<Shape>();
				tempShapeList.addAll(navPropertychildren);
				for (Shape navPropertyShape : tempShapeList) {
					// Get the Navigation Property for the
					// Association
					if (PropertyUtil.isTypeIdentifier(navPropertyShape)) {

						List<Shape> childShapes = ((ContainerShape) navPropertyShape)
								.getChildren();
						Shape textShape = childShapes.size() == 2 ? childShapes
								.get(1) : null;
						if (textShape != null) {
							NavigationProperty navProperty = (NavigationProperty) (fp
									.getBusinessObjectForPictogramElement(navPropertyShape));
							if (navProperty.getName().startsWith(oldEntityName)) {
								if (navProperty.getRelationship() == association) {
									DirectEditingContext directEditingContext = new DirectEditingContext(
											textShape,
											textShape.getGraphicsAlgorithm());
									AssociationType associationType = AssociationType.NONE;
									// If it is self association
									if (sourceEntity == targetEntity) {
										associationType = AssociationType.SELF;
										if (isFirstTime == true) {

											isFirstTime = false;
											setUpdatedNavPropName(fp,
													sourceEntity, targetEntity,
													principalRole,
													directEditingContext,
													associationType);
										} else {

											setUpdatedNavPropName(fp,
													sourceEntity, targetEntity,
													dependentRole,
													directEditingContext,
													associationType);
										}
									} else {

										if (dependentRole != null) {

											setUpdatedNavPropName(fp,
													sourceEntity, targetEntity,
													dependentRole,
													directEditingContext,
													associationType);

										}
									}
								}
							}
						}

					}

				}
			}
		}
	}

	/**
	 * This method is used to set the updated navigation property name depending
	 * upon entity name and the multiplicity
	 * 
	 * @param fp
	 * @param sourceEntity
	 * @param targetEntity
	 * @param principalRole
	 * @param isFirstTime
	 * @param directEditingContext
	 * @param associationType
	 * 
	 */
	private static void setUpdatedNavPropName(IFeatureProvider fp,
			EntityType sourceEntity, EntityType targetEntity,
			Role principalRole, DirectEditingContext directEditingContext,
			AssociationType associationType) {
		String newName;
		if (principalRole != null) {

			newName = ArtifactUtil.getNavigationPropertyName(targetEntity,
					getMultiplicityEnum(principalRole.getMultiplicity()),
					associationType);
			newName = ArtifactUtil.getNavPropNameProposal(
					getMultiplicityEnum(principalRole.getMultiplicity()),
					associationType, targetEntity.getName(), newName,
					sourceEntity.getNavigationProperties());
			fp.getDirectEditingFeature(directEditingContext).setValue(newName,
					directEditingContext);
		}

	}

	/**
	 * Updates the association set on the change of association name
	 * 
	 * @param association
	 * @param featureProvider
	 */
	public static void updateAssociationSetName(Association association,
			IFeatureProvider featureProvider) {
		AssociationSet as;
		String assocSetName = null;
		EObject selectedObj = featureProvider.getDiagramTypeProvider()
				.getDiagram().eResource().getContents().get(1);
		if (selectedObj instanceof EDMXSet) {
			final List<Schema> schemas = ArtifactUtil
					.getSchemas((EDMXSet) selectedObj);
			for (Schema schema2 : schemas) {
				if (schema2.getContainers().size() > 0) {
					EList<AssociationSet> assocSets = schema2.getContainers()
							.get(0).getAssociationSets();
					String asname = null;
					List<String> assocSetNames = new ArrayList<String>();
					for (ListIterator<AssociationSet> it = assocSets
							.listIterator(); it.hasNext();) {
						as = it.next();
						asname = as.getName();
						assocSetNames.add(asname);
					}
					for (AssociationSet assocSet : assocSets) {
						if (assocSet.getAssociation() == association) {
							assocSetName = ArtifactNameUtil
									.generateUniqueName(
											assocSetNames,
											association.getName(),
											association.getName()
													+ IODataEditorConstants.DEFAULT_NAME_SUFFIX,
											IODataEditorConstants.DEFAULT_NAME_SUFFIX,
											IODataEditorConstants.MAX_LENGTH);
							if (assocSetName != null
									&& assocSetName.length() > 0) {
								assocSet.setName(assocSetName);
							} else {
								// This should not happen. This is just
								// to avoid blank name generation.
								assocSetName = association.getName()
										+ IODataEditorConstants.DEFAULT_NAME_SUFFIX;
								assocSet.setName(assocSetName);
							}
							return;
						}
					}
				}
			}

		}
	}
}
