/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.utils;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Schema;

/**
 * This class will act as a container for copied Diagram Element and its
 * associated Schema.
 * 
 */
public class ArtifactContainer extends EObjectImpl {
	/**
	 * schema of Diagram Element
	 */
	private Schema schema;
	/**
	 * diagram element
	 */
	private EObject eObject;
	/**
	 * base type of Entity Type
	 */
	private EntityType baseType;

	/**
	 * getter for base type
	 * 
	 * @return base type of provided entity type
	 */
	public EntityType getBaseType() {
		return this.baseType;
	}

	/**
	 * setter for base type of provided entity type
	 * 
	 * @param baseType
	 */
	public void setBaseType(EntityType baseType) {
		this.baseType = baseType;
	}

	/**
	 * getter for schema of provided diagram element
	 * 
	 * @return schema
	 */
	public Schema getSchema() {
		return this.schema;
	}

	/**
	 * setter for schema
	 * 
	 * @param schema
	 */
	public void setSchema(Schema schema) {
		this.schema = schema;
	}

	/**
	 * getter for diagram element
	 * 
	 * @return diagram element
	 */
	public EObject geteObject() {
		return this.eObject;
	}

	/**
	 * setter for diagram element
	 * 
	 * @param eObject
	 */
	public void seteObject(EObject eObject) {
		this.eObject = eObject;
	}

	/**
	 * Constructor
	 * 
	 * @param schema
	 * @param eObject
	 * @param baseType
	 */
	public ArtifactContainer(Schema schema, EObject eObject, EntityType baseType) {
		super();
		this.schema = schema;
		this.eObject = eObject;
		this.baseType = baseType;
	}

}
