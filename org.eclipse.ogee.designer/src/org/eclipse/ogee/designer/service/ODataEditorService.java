/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.service;

import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.service.validation.GenericServiceUrlValidator;
import org.eclipse.ogee.utils.service.validation.IServiceMetadataFileValidator;
import org.eclipse.ogee.utils.service.validation.IServiceUrlValidator;
import org.eclipse.ogee.utils.service.validation.Result;
import org.eclipse.ogee.utils.service.validation.ServiceValidator;

public final class ODataEditorService {

	TransactionalEditingDomain editingDomain;

	private ODataEditorService() {

	}

	/**
	 * The following method creates an OData model file in the Eclipse project.
	 * 
	 * @param diagramResourceUri
	 *            - model file location
	 * @param diagram
	 *            - an instance of OData Diagram
	 * @param edmxSet
	 *            - the representation of the model object.
	 * @return boolean - true, if OData file is created successfully.
	 */
	public static boolean createODataFile(final URI diagramResourceUri,
			final Diagram diagram, final EDMXSet edmxSet) {

		boolean isCreated = false;
		IFile modelFile = null;

		try {
			modelFile = IModelContext.INSTANCE.createModelFile(
					diagramResourceUri, edmxSet, diagram);
			if (modelFile != null) {
				IModelContext.INSTANCE.saveModelFile(modelFile);
				isCreated = true;
			}
		} catch (ModelAPIException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		}

		return isCreated;
	}

	/**
	 * Validates the syntax of service URL.
	 * 
	 * @param serviceUrl
	 * 
	 * @return true if serviceUrl is valid
	 */
	public static boolean isServiceUrlSyntaxValid(final String serviceUrl) {

		return GenericServiceUrlValidator.isServiceUrlSyntaxValid(serviceUrl);
	}

	/**
	 * Validates the service URL and returns the Result.
	 * 
	 * @param serviceUrl
	 * @param monitor
	 * 
	 * @return the service's meta-data
	 */
	public static Result validateServiceUrl(final String serviceUrl,
			final IProgressMonitor monitor) {

		Result result = null;

		final IServiceUrlValidator serviceValidator = ServiceValidator
				.getServiceUrlValidator();
		if (serviceValidator != null) {
			result = serviceValidator
					.validateSimple(serviceUrl.trim(), monitor);
		}

		return result;
	}

	/**
	 * Validates the service meta-data file and returns the Result.
	 * 
	 * @param serviceMetadataUri
	 * @param monitor
	 * 
	 * @return validation result
	 */
	public static Result validateServiceMetadataFile(
			final String serviceMetadataUri, final IProgressMonitor monitor) {

		Result result = null;

		final IServiceMetadataFileValidator serviceMetadataValidator = ServiceValidator
				.getServiceMetadataValidator();
		if (serviceMetadataValidator != null) {
			result = serviceMetadataValidator.validate(
					serviceMetadataUri.trim(), monitor);
		}

		return result;
	}

	/**
	 * The following method adds missing Registered Vocabularies in the current
	 * model EDMX Set.
	 * 
	 * @param edmxSet
	 *            - Current Model EDMX Set.
	 * @return true if Registered Vocabularies are restored.
	 */
	public static boolean restoreRegisteredVocabularies(final EDMXSet edmxSet) {

		return new ODataEditorService().restoreDefaultVocabularies(edmxSet);
	}

	/**
	 * Updates the Progress Monitor.
	 * 
	 * @param taskName
	 * @param worked
	 * @param monitor
	 */
	/*
	 * public static void updateProgressMonitor(final String taskName, final int
	 * worked, final IProgressMonitor monitor) {
	 * 
	 * try { PlatformUI.getWorkbench().getProgressService() .run(true, true, new
	 * IRunnableWithProgress() {
	 * 
	 * @Override public void run(IProgressMonitor progress) {
	 * 
	 * if (taskName != null) { monitor.setTaskName(taskName); }
	 * monitor.worked(worked); } }); } catch (InvocationTargetException e) { //
	 * do not log exceptions } catch (InterruptedException e) { // do not log
	 * exceptions } }
	 */

	/*
	 * The following method adds missing Registered Vocabularies in the current
	 * model EDMX Set.
	 */
	private boolean restoreDefaultVocabularies(final EDMXSet edmxSet) {

		final boolean[] isRestored = new boolean[] { false };

		try {
			final IVocabularyContext context = IModelContext.INSTANCE
					.getVocabularyContext(edmxSet);
			final Iterator<String> namespaceIterator = context
					.getRegisteredVocabularies().iterator();
			this.editingDomain = IModelContext.INSTANCE.getTransaction(edmxSet);
			this.editingDomain.getCommandStack().execute(
					new RecordingCommand(this.editingDomain) {

						@Override
						protected void doExecute() {

							while (namespaceIterator.hasNext()) {
								final String namespace = namespaceIterator
										.next();
								// If the registered vocabulary is missing in
								// the current EDMX set,
								// add missing registered vocabulary.
								if (ArtifactUtil.findSchema(namespace, edmxSet) == null) {
									try {
										final IVocabulary vocabulary = context
												.provideVocabulary(namespace);
										if (vocabulary == null) {
											// TODO: remove undo after Graphiti
											// version update to 0.10.0
											editingDomain.getCommandStack()
													.undo();
										} else {
											isRestored[0] = true;
										}
									} catch (ModelAPIException e) {
										// TODO: remove undo after Graphiti
										// version update to 0.10.0
										editingDomain.getCommandStack().undo();
										Logger.getLogger(Activator.PLUGIN_ID)
												.logError(e);
									}
								}
							}
						}
					});
		} catch (ModelAPIException e) {
			// TODO: remove undo after Graphiti version update to 0.10.0
			this.editingDomain.getCommandStack().undo();
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		}
		if (!isRestored[0]) {
			// TODO: remove undo after Graphiti version update to 0.10.0
			this.editingDomain.getCommandStack().undo();
		}

		return isRestored[0];
	}

}
