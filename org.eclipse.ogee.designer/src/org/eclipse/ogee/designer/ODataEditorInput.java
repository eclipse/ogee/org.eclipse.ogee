/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer;

import org.eclipse.emf.common.util.URI;
import org.eclipse.graphiti.ui.editor.DiagramEditorInput;
import org.eclipse.ogee.designer.api.IODataDiagramInput;

/**
 * The editor input object for ODataDiagramEditor. Wraps the URI of a Diagram
 * and an ID of a diagram type provider for displaying it with a Graphiti
 * diagram editor.
 * 
 */
public final class ODataEditorInput extends DiagramEditorInput {

	private boolean isBlankModel;
	private IODataDiagramInput diagramInput;
	private String connectionName = ""; //$NON-NLS-1$

	/**
	 * Constructor for ODataEditorInput
	 * 
	 * @param diagramUri
	 * @param providerId
	 */
	public ODataEditorInput(URI diagramUri, String providerId) {

		super(diagramUri, providerId);
	}

	/**
	 * @return true if blank model.
	 */
	public boolean isBlankModel() {

		return this.isBlankModel;
	}

	/**
	 * @param isBlankModel
	 *            - set to true for blank model.
	 */
	public void setBlankModel(final boolean isBlankModel) {

		this.isBlankModel = isBlankModel;
	}

	/**
	 * @return IODataDiagramInput
	 */
	public IODataDiagramInput getDiagramInput() {

		return this.diagramInput;
	}

	/**
	 * @param diagramInput
	 */
	public void setDiagramInput(final IODataDiagramInput diagramInput) {

		this.diagramInput = diagramInput;
	}

	/**
	 * @return String - Gateway connection name.
	 */
	public String getGWConnectionName() {

		return this.connectionName;
	}

	/**
	 * @param connectionName
	 *            - Gateway connection name.
	 */
	public void setGWConnectionName(String connectionName) {

		this.connectionName = connectionName;
	}

}
