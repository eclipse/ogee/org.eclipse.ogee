/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.factories;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.providers.ODataImageProvider;
import org.eclipse.ogee.designer.shapes.BasicArtifactShape;
import org.eclipse.ogee.designer.shapes.BasicComplexTypeShape;
import org.eclipse.ogee.designer.shapes.BasicEnumTypeShape;
import org.eclipse.ogee.designer.shapes.BasicFunctionImportShape;
import org.eclipse.ogee.designer.shapes.BiDirectionalAssociationShape;
import org.eclipse.ogee.designer.shapes.ExtendedIdentifierShape;
import org.eclipse.ogee.designer.shapes.ExtensionArtifactShape;
import org.eclipse.ogee.designer.shapes.IdentifierShape;
import org.eclipse.ogee.designer.shapes.LinkUsageShape;
import org.eclipse.ogee.designer.shapes.RectangleType;
import org.eclipse.ogee.designer.shapes.SeparatorShape;
import org.eclipse.ogee.designer.shapes.UniDirectionalAssociationShape;
import org.eclipse.ogee.designer.utils.PropertyUtil;

/**
 * This Factory Class contains creation methods for all the shapes associated
 * with the business objects.
 * 
 */
public class ShapesFactory {

	private IFeatureProvider featureProvider;

	/**
	 * Constructor
	 * 
	 * @param featureProvider
	 *            IFeatureProvider instance
	 */
	public ShapesFactory(IFeatureProvider featureProvider) {
		this.featureProvider = featureProvider;
	}

	/**
	 * Creates an object of the basic artifact which is a rounded rectangle
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @return BasicArtifactShape
	 */
	public BasicArtifactShape createArtifactShape(ContainerShape container,
			IAddContext context) {
		BasicArtifactShape basicArtifactShape = new BasicArtifactShape(
				container, featureProvider, context, false);
		basicArtifactShape.createContents();
		// Top Container
		PictogramElement[] pictogramElements = basicArtifactShape
				.getPictogramElements();
		if (pictogramElements.length > 0) {
			PropertyUtil.setAsTopContainer(pictogramElements[0]);
		}

		if (PropertyUtil.isShowAllUsage((Diagram) container)) {

			PropertyUtil.setShowUsage(pictogramElements[0]);
		}
		return basicArtifactShape;
	}

	/**
	 * Creates an object of the basic function import artifact which is a five
	 * cornered polygon
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @return BasicFunctionImportShape
	 */
	public BasicFunctionImportShape createFunctionImportShape(
			ContainerShape container, IAddContext context) {
		BasicFunctionImportShape basicFunctionImportShape = new BasicFunctionImportShape(
				container, featureProvider, context, false);
		basicFunctionImportShape.createContents();
		// Top Container
		PictogramElement[] pictogramElements = basicFunctionImportShape
				.getPictogramElements();
		if (pictogramElements.length > 0) {
			PropertyUtil.setAsTopContainer(pictogramElements[0]);
		}

		if (PropertyUtil.isShowAllUsage((Diagram) container)) {

			PropertyUtil.setShowUsage(pictogramElements[0]);
		}
		return basicFunctionImportShape;
	}

	/**
	 * Creates an object of the basic enum type artifact which is a four
	 * cornered polygon with two upper corners rounded
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @return BasicFunctionImportShape
	 */
	public BasicEnumTypeShape createEnumTypeShape(ContainerShape container,
			IAddContext context) {
		BasicEnumTypeShape basicEnumTypeShape = new BasicEnumTypeShape(
				container, featureProvider, context, false);
		basicEnumTypeShape.createContents();
		// Top Container
		PictogramElement[] pictogramElements = basicEnumTypeShape
				.getPictogramElements();
		if (pictogramElements.length > 0) {
			PropertyUtil.setAsTopContainer(pictogramElements[0]);
		}

		if (PropertyUtil.isShowAllUsage((Diagram) container)) {

			PropertyUtil.setShowUsage(pictogramElements[0]);
		}
		return basicEnumTypeShape;
	}

	/**
	 * Creates an object of the basic complex type artifact which is a four
	 * cornered polygon with two corners (one upper, one lower) rounded
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @return BasicFunctionImportShape
	 */
	public BasicComplexTypeShape createComplexTypeShape(
			ContainerShape container, IAddContext context) {
		BasicComplexTypeShape basicComplexTypeShape = new BasicComplexTypeShape(
				container, featureProvider, context, false);
		basicComplexTypeShape.createContents();
		// Top Container
		PictogramElement[] pictogramElements = basicComplexTypeShape
				.getPictogramElements();
		if (pictogramElements.length > 0) {
			PropertyUtil.setAsTopContainer(pictogramElements[0]);
		}

		if (PropertyUtil.isShowAllUsage((Diagram) container)) {

			PropertyUtil.setShowUsage(pictogramElements[0]);
		}
		return basicComplexTypeShape;
	}

	/**
	 * Creates an object of the basic artifact which is a rounded rectangle
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @return BasicArtifactShape
	 */
	public BasicArtifactShape createReferencedArtifactShape(
			ContainerShape container, IAddContext context,
			boolean isReferencedShape) {
		BasicArtifactShape basicArtifactShape = new BasicArtifactShape(
				container, featureProvider, context, isReferencedShape);
		basicArtifactShape.createContents();
		// Top Container
		PictogramElement[] pictogramElements = basicArtifactShape
				.getPictogramElements();
		if (pictogramElements.length > 0) {
			PropertyUtil.setAsTopContainer(pictogramElements[0]);
		}
		return basicArtifactShape;
	}

	/**
	 * Creates an identifier shape for Header texts
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param artifactName
	 *            Name
	 * @param imageRef
	 *            Icon path
	 * @param context
	 *            IAddContext instance
	 * @return IdentifierShape
	 */
	public IdentifierShape createTitleIdentifierShape(ContainerShape container,
			String artifactName, String imageRef, IAddContext context) {
		IdentifierShape identifierShape = new IdentifierShape(container,
				artifactName, imageRef, featureProvider, context,
				context.getNewObject(), true, PropertyUtil.TITLE, false);
		identifierShape.createContents();
		return identifierShape;
	}

	/**
	 * Creates an identifier shape for Header texts
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param artifactName
	 *            Name
	 * @param imageRef
	 *            Icon path
	 * @param context
	 *            IAddContext instance
	 * @param isExpanded
	 * @return IdentifierShape
	 */
	public IdentifierShape createCollapsableTitleShape(
			ContainerShape container, String artifactName, IAddContext context,
			boolean isExpanded) {

		String imageRef = isExpanded ? ODataImageProvider.IMG_EXPAND
				: ODataImageProvider.IMG_COLLAPSE;
		IdentifierShape identifierShape = createTitleIdentifierShape(container,
				artifactName, imageRef, context);

		PictogramElement[] pictogramElements = identifierShape
				.getPictogramElements();
		for (PictogramElement pe : pictogramElements) {
			if (isExpanded) {
				PropertyUtil.setExpanded(pe);
			} else {
				PropertyUtil.setCollapsed(pe);
			}

		}

		return identifierShape;
	}

	/**
	 * Creates an identifier shape for Header texts
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param artifactName
	 *            Name
	 * @param context
	 *            IAddContext instance
	 * @param isExpanded
	 * @return IdentifierShape
	 */
	public ExtensionArtifactShape createCollapsableRectangle(
			ContainerShape container, IAddContext context, String artifactName,
			boolean isExpanded) {

		ExtensionArtifactShape currentRectangle = createPlainRectangle(
				container, context, artifactName);

		PictogramElement[] pictogramElements = currentRectangle
				.getPictogramElements();
		for (PictogramElement pe : pictogramElements) {
			if (isExpanded) {
				PropertyUtil.setExpanded(pe);
			} else {
				PropertyUtil.setCollapsed(pe);
			}

		}

		return currentRectangle;
	}

	/**
	 * Creates an identifier shape for simple texts
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param artifactName
	 *            Name
	 * @param imageRef
	 *            Icon path
	 * @param context
	 *            IAddContext instance
	 * @param domainObject
	 * @param isHeader
	 *            boolean true if header
	 * @param shapeID
	 * @return IdentifierShape
	 */
	public IdentifierShape createIdentifierShape(ContainerShape container,
			String artifactName, String imageRef, IAddContext context,
			Object domainObject, boolean isHeader, String shapeID) {
		IdentifierShape identifierShape = new IdentifierShape(container,
				artifactName, imageRef, this.featureProvider, context,
				domainObject, isHeader, shapeID, isHeader);
		identifierShape.createContents();
		return identifierShape;
	}

	/**
	 * Creates an identifier shape for simple texts
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param artifactName
	 *            Name
	 * @param imageRef
	 *            Icon path
	 * @param context
	 *            IAddContext instance
	 * @param domainObject
	 * @param imgRefObject
	 * @param isHeader
	 *            boolean true if header
	 * @param shapeID
	 * @return IdentifierShape
	 */
	public IdentifierShape createPropertyShape(ContainerShape container,
			String artifactName, String imageRef, IAddContext context,
			Object domainObject, Object imgRefObject, boolean isHeader,
			String shapeID) {
		IdentifierShape identifierShape = new IdentifierShape(container,
				artifactName, imageRef, this.featureProvider, context,
				domainObject, domainObject, isHeader, shapeID, isHeader);
		identifierShape.createContents();
		return identifierShape;
	}

	/**
	 * Creates an identifier shape for simple texts
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param artifactName
	 *            Name
	 * @param artifactValue
	 * @param imageRef
	 *            Icon path
	 * @param context
	 *            IAddContext instance
	 * @param domainObject
	 * @param isHeader
	 *            boolean true if header
	 * @return IdentifierShape
	 */
	public IdentifierShape createMemberShape(ContainerShape container,
			String artifactName, String artifactValue, String imageRef,
			IAddContext context, Object domainObject, boolean isHeader) {
		IdentifierShape identifierShape = new ExtendedIdentifierShape(
				container, artifactName, artifactValue, imageRef,
				this.featureProvider, context, domainObject, isHeader,
				PropertyUtil.ENUM_MEMBER_NAME, PropertyUtil.ENUM_MEMBER_VALUE);
		identifierShape.createContents();
		return identifierShape;
	}

	/**
	 * Creates an extension object which is an inner rectangle
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @param shapeID
	 * @return ExtensionArtifactShape
	 */
	public ExtensionArtifactShape createRectangle(ContainerShape container,
			IAddContext context, String shapeID) {
		ExtensionArtifactShape extensionArtifactShape = new ExtensionArtifactShape(
				container, context, featureProvider, RectangleType.RECTANGLE,
				shapeID);
		extensionArtifactShape.createContents();
		return extensionArtifactShape;
	}

	/**
	 * Creates an extension object which is a plain rectangle
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @param shapeID
	 * @return ExtensionArtifactShape
	 */
	public ExtensionArtifactShape createPlainRectangle(
			ContainerShape container, IAddContext context, String shapeID) {
		ExtensionArtifactShape extensionArtifactShape = new ExtensionArtifactShape(
				container, context, featureProvider,
				RectangleType.PLAINRECTANGLE, shapeID);
		extensionArtifactShape.createContents();
		return extensionArtifactShape;
	}

	/**
	 * Creates an extension object which is an invisible rectangle
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @param shapeID
	 * @return ExtensionArtifactShape
	 */
	public ExtensionArtifactShape createInvisibleRectangle(
			ContainerShape container, IAddContext context, String shapeID) {
		ExtensionArtifactShape extensionArtifactShape = new ExtensionArtifactShape(
				container, context, featureProvider,
				RectangleType.INVISIBLERECTANGLE, shapeID);
		extensionArtifactShape.createContents();
		return extensionArtifactShape;
	}

	/**
	 * Creates a line object
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @return PolyLineShape
	 */
	public SeparatorShape createPolyLineShape(ContainerShape container,
			IAddContext context) {
		SeparatorShape polyLineShape = new SeparatorShape(container,
				featureProvider, context);
		polyLineShape.createContents();
		return polyLineShape;
	}

	/**
	 * Creates an association object
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @return PolyLineShape
	 */
	public UniDirectionalAssociationShape createUniDirectionalAssociationShape(
			ContainerShape container, IAddContext context, Connection connection) {
		UniDirectionalAssociationShape polyLineAssociationShape = new UniDirectionalAssociationShape(
				container, featureProvider, context, connection);
		polyLineAssociationShape.createContents();
		// Top Container
		PictogramElement[] pictogramElements = polyLineAssociationShape
				.getPictogramElements();
		if (pictogramElements.length > 0) {
			PropertyUtil.setAsTopContainer(pictogramElements[0]);
		}
		return polyLineAssociationShape;
	}

	/**
	 * Creates a bi dir association object
	 * 
	 * @param container
	 *            ContainerShape instance
	 * @param context
	 *            IAddContext instance
	 * @return PolyLineShape
	 */
	public BiDirectionalAssociationShape createBiDirectionalAssociationShape(
			ContainerShape container, IAddContext context, Connection connection) {
		BiDirectionalAssociationShape polyLineAssociationShape = new BiDirectionalAssociationShape(
				container, featureProvider, context, connection);
		polyLineAssociationShape.createContents();
		// Top Container
		PictogramElement[] pictogramElements = polyLineAssociationShape
				.getPictogramElements();
		if (pictogramElements.length > 0) {
			PropertyUtil.setAsTopContainer(pictogramElements[0]);
		}
		return polyLineAssociationShape;
	}

	/**
	 * Creates an LinkUsage shape
	 * 
	 * @param fp
	 * @param connectionContext
	 * 
	 * @return PolyLineShape
	 */
	public LinkUsageShape createLinkUsageShape(IFeatureProvider fp,
			ICreateConnectionContext connectionContext) {
		LinkUsageShape linkUsageShape = new LinkUsageShape(fp,
				connectionContext);
		linkUsageShape.createContents();

		return linkUsageShape;
	}

}
