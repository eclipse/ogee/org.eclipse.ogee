/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.factories;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.change.ChangeDescription;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.ogee.designer.contexts.NavigationPropertyContext;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.types.FunctionImportReturnType;
import org.eclipse.ogee.designer.types.MultiplicityType;
import org.eclipse.ogee.designer.utils.ArtifactNameUtil;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;

/**
 * This Class contains creation methods for all the business objects.
 * 
 */
public class ArtifactFactory {

	private Schema schema;
	private Diagram diagram;
	private EntityContainer entityContainer;
	private EDMXSet edmxset;

	/**
	 * 
	 * @return Service Object
	 */
	public Schema getSchema() {

		return this.schema;
	}

	/**
	 * Updates Service Object.Mainly used in Unit testing.
	 * 
	 * @param schema
	 */
	public void setSchema(final Schema schema) {

		this.schema = schema;
	}

	/**
	 * Initializer
	 * 
	 * @param dgm
	 *            Diagram
	 */
	public void init(Diagram dgm) {

		this.diagram = dgm;
		if (this.diagram.eResource().getContents().size() > 1) {
			EObject selectedObj = this.diagram.eResource().getContents().get(1);
			if (selectedObj instanceof EDMXSet) {
				this.edmxset = (EDMXSet) selectedObj;
				this.schema = ArtifactUtil
						.getDefaultSchema((EDMXSet) selectedObj);
				this.entityContainer = ArtifactUtil
						.getDefaultEntityContainer((EDMXSet) selectedObj);
			}
		}
	}

	/**
	 * Creates a default Entity Type
	 * 
	 * @return EntityType created EntityType
	 */
	public EntityType createDefaultEntityType() {
		return createEntityType(ArtifactUtil.getNameForEntity(this.schema));
	}

	/**
	 * Creates a referenced Entity Type
	 * 
	 * @return EntityType created EntityType
	 */
	public EntityType createReferencedEntityType() {
		return ArtifactUtil.getReferencedEntities(this.schema).get(0);
	}

	/**
	 * Creates default Entity Container which holds all entity sets.
	 * 
	 * @return Entity Container
	 */
	public EntityContainer getEntityContainer() {

		// Added condition to fix 216980
		if (entityContainer == null
				|| (entityContainer.eContainer() == null || entityContainer
						.eContainer() instanceof ChangeDescription)) {
			entityContainer = OdataFactory.eINSTANCE.createEntityContainer();
			entityContainer.setName(this.schema.getNamespace());
			entityContainer.setDefault(true);
			this.schema.getContainers().add(entityContainer);
		}

		return entityContainer;

	}

	/**
	 * Creates a default Entity Set for the given Entity Type
	 * 
	 * @param entityType
	 *            Related Entity
	 * @return EntitySet created EntitySet
	 */
	public EntitySet createDefaultEntitySet(Object entityType) {

		String name = ""; //$NON-NLS-1$
		String entityName = null;
		List<String> entitySetNames = new ArrayList<String>();
		if (entityType instanceof EntityType) {
			EntityType curentEntity = (EntityType) entityType;
			entityName = curentEntity.getName();
			entitySetNames = ArtifactUtil.getEntSetAssocSetFuncImpNames(schema);

			// Entity Name + Set e.g. CustomerSet
			name = entityName + Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME;
			name = ArtifactNameUtil.generateUniqueName(entitySetNames,
					entityName, name,
					Messages.ODATAEDITOR_ENTITYCOLLECTION_NAME,
					IODataEditorConstants.MAX_LENGTH);
		}
		return createEntitySet(entityType, name);
	}

	/**
	 * Creates a new Entity Type
	 * 
	 * @param entityName
	 * @return EntityType created EntityType
	 */
	public EntityType createEntityType(String entityName) {

		EntityType entityType = OdataFactory.eINSTANCE.createEntityType();
		entityType.setName(entityName);
		this.schema.getEntityTypes().add(entityType);

		return entityType;
	}

	/**
	 * Returns all entities of this model
	 * 
	 * @return List of Entities.
	 */
	public List<EntityType> getAllEntitiesIncludingReferenced() {
		EList<SchemaClassifier> schemaClassifiers;
		final List<EntityType> entityTypes = new ArrayList<EntityType>();
		if (edmxset != null) {
			final DataService odataService = this.edmxset.getMainEDMX()
					.getDataService();
			final EList<Schema> schemaList = odataService.getSchemata();

			for (Schema schema : schemaList) {
				schemaClassifiers = schema.getClassifiers();
				for (SchemaClassifier schemaClassifier : schemaClassifiers) {
					if (SchemaClassifier.SERVICE_VALUE == schemaClassifier
							.getValue()) {
						entityTypes.addAll(schema.getEntityTypes());
						// TODO Start - Enable with OData V4 support
						// List<EntityType> referencedEntities = ArtifactUtil
						// .getReferencedEntities(this.schema);
						// if (referencedEntities != null
						// && referencedEntities.size() > 0) {
						// entityTypes.addAll(referencedEntities);
						// }
						// End - Enable with OData V4 support
					}
				}
			}
		}
		return entityTypes;
	}

	/**
	 * Returns all complex types in this model
	 * 
	 * @return List of Complex Types.
	 */
	public List<ComplexType> getAllComplexTypesIncludingReferenced() {
		EList<SchemaClassifier> schemaClassifiers;
		final List<ComplexType> complexTypes = new ArrayList<ComplexType>();
		if (edmxset != null) {
			final DataService odataService = this.edmxset.getMainEDMX()
					.getDataService();
			final EList<Schema> schemaList = odataService.getSchemata();

			for (Schema schema : schemaList) {
				schemaClassifiers = schema.getClassifiers();
				for (SchemaClassifier schemaClassifier : schemaClassifiers) {
					if (SchemaClassifier.SERVICE_VALUE == schemaClassifier
							.getValue()) {
						complexTypes.addAll(schema.getComplexTypes());
						// TODO Start - Enable with OData V4 support
						// EList<EDMXReference> references =
						// edmxset.getMainEDMX()
						// .getReferences();
						// if (references != null && references.size() > 0) {
						// for (EDMXReference edmxReference : references) {
						// EList<Schema> referencedSchemata = edmxReference
						// .getReferencedEDMX().getDataService()
						// .getSchemata();
						// for (Schema schema2 : referencedSchemata) {
						// complexTypes.addAll(schema2
						// .getComplexTypes());
						// }
						// }
						// }
						// End - Enable with OData V4 support
					}
				}
			}
		}
		return complexTypes;
	}

	/**
	 * Returns all function imports in this model
	 * 
	 * @return List of Complex Types.
	 */
	public List<FunctionImport> getAllFunctionImports() {
		final List<FunctionImport> functionImports = new ArrayList<FunctionImport>();
		if (schema.getContainers().size() > 0) {
			for (int i = 0; i < schema.getContainers().size(); i++) {
				functionImports.addAll((schema.getContainers().get(i))
						.getFunctionImports());
			}
		}
		return functionImports;
	}

	/**
	 * Returns all enum types in this model
	 * 
	 * @return List of Enum Types.
	 */
	public List<EnumType> getAllEnumTypesIncludingReferenced() {
		final List<EnumType> enumTypes = new ArrayList<EnumType>();
		enumTypes.addAll(this.schema.getEnumTypes());
		EList<EDMXReference> references = edmxset.getMainEDMX().getReferences();
		if (references != null && references.size() > 0) {
			for (EDMXReference edmxReference : references) {
				EList<Schema> referencedSchemata = edmxReference
						.getReferencedEDMX().getDataService().getSchemata();
				for (Schema schema2 : referencedSchemata) {
					enumTypes.addAll(schema2.getEnumTypes());
				}
			}
		}
		return enumTypes;

	}

	/**
	 * Creates a new Entity Set for the given Entity Type
	 * 
	 * @param entityType
	 *            Related Entity
	 * @param name
	 *            Entity Set name
	 * @return EntitySet created EntitySet
	 */
	public EntitySet createEntitySet(Object entityType, String name) {

		EntitySet entitySet = null;
		if (entityType instanceof EntityType) {
			EntityType curentEntity = (EntityType) entityType;
			entitySet = OdataFactory.eINSTANCE.createEntitySet();
			entitySet.setName(name);
			entitySet.setType(curentEntity);
			getEntityContainer().getEntitySets().add(entitySet);
		}
		return entitySet;
	}

	/**
	 * Creates a default Function Import
	 * 
	 * @return FunctionImport created FunctionImport
	 */
	public FunctionImport createDefaultFunctionImport() {
		return createFunctionImport(ArtifactUtil
				.getNameForFunctionImport(this.schema));
	}

	/**
	 * Creates a default EnumType
	 * 
	 * @return EnumType created EnumType
	 */
	public EnumType createDefaultEnumType() {
		return createEnumType(ArtifactUtil.getNameForEnumType(this.schema));
	}

	/**
	 * Creates a new EnumType
	 * 
	 * @param enumTypeName
	 * @return EnumType created EnumType
	 */
	public EnumType createEnumType(String enumTypeName) {

		EnumType enumType = OdataFactory.eINSTANCE.createEnumType();
		enumType.setName(enumTypeName);
		enumType.setUnderlyingType(EDMTypes.INT32);
		this.schema.getEnumTypes().add(enumType);
		return enumType;
	}

	/**
	 * Creates a new member for the given enumType
	 * 
	 * @param enumType
	 *            Related EnumType
	 * @param name
	 *            EnumMember name
	 * @return EnumMember created EnumMember
	 */
	public EnumMember createEnumMember(Object enumType, String name) {

		EnumMember member = null;

		if (enumType instanceof EnumType) {
			EnumType currentEnumType = (EnumType) enumType;
			member = OdataFactory.eINSTANCE.createEnumMember();
			member.setName(name);
			currentEnumType.getMembers().add(member);
		}
		return member;
	}

	/**
	 * Creates a new FunctionImport
	 * 
	 * @param functionImportName
	 * @return FunctionImport created FunctionImport
	 */
	public FunctionImport createFunctionImport(String functionImportName) {

		FunctionImport functionImport = OdataFactory.eINSTANCE
				.createFunctionImport();
		functionImport.setName(functionImportName);
		getEntityContainer().getFunctionImports().add(functionImport);

		return functionImport;
	}

	/**
	 * Creates a new parameter for the given function import
	 * 
	 * @param funcImport
	 *            Related FunctionImport
	 * @param name
	 *            Parameter name
	 * @return Parameter created parameter
	 */
	public Parameter createParameter(Object funcImport, String name) {

		Parameter parameter = null;

		if (funcImport instanceof FunctionImport) {
			FunctionImport currentFunctionImport = (FunctionImport) funcImport;
			parameter = OdataFactory.eINSTANCE.createParameter();
			parameter.setName(name);
			SimpleTypeUsage type = OdataFactory.eINSTANCE
					.createSimpleTypeUsage();
			SimpleType simpleType = OdataFactory.eINSTANCE.createSimpleType();
			simpleType.setType(EDMTypes.STRING);
			type.setSimpleType(simpleType);
			parameter.setType(type);
			currentFunctionImport.getParameters().add(parameter);
		}
		return parameter;
	}

	/**
	 * Creates a new return type for the given function import
	 * 
	 * @param funcImport
	 *            Related FunctionImport
	 * @param type
	 *            FunctionImportReturnType
	 * @return IFunctionReturnTypeUsage created ReturnType
	 */
	public IFunctionReturnTypeUsage createReturnType(Object funcImport,
			FunctionImportReturnType type, SimpleType simpleType) {

		IFunctionReturnTypeUsage returnType = null;

		if (funcImport instanceof FunctionImport) {
			FunctionImport currentFunctionImport = (FunctionImport) funcImport;
			if (type == FunctionImportReturnType.SIMPLE) {
				returnType = OdataFactory.eINSTANCE.createSimpleTypeUsage();
				SimpleType newSimpleType = OdataFactory.eINSTANCE
						.createSimpleType();
				if (simpleType == null) {
					newSimpleType.setType(EDMTypes.STRING);
				} else {
					newSimpleType.setType(simpleType.getType());
				}
				((SimpleTypeUsage) returnType).setSimpleType(newSimpleType);
				currentFunctionImport.setReturnType(returnType);
			} else if (type == FunctionImportReturnType.COMPLEX) {
				returnType = OdataFactory.eINSTANCE.createComplexTypeUsage();
				List<ComplexType> allComplexTypesIncludingReferenced = this
						.getAllComplexTypesIncludingReferenced();
				if (allComplexTypesIncludingReferenced != null
						&& allComplexTypesIncludingReferenced.size() > 0) {
					ComplexType complexType = allComplexTypesIncludingReferenced
							.get(0);
					((ComplexTypeUsage) returnType).setComplexType(complexType);
				}
				currentFunctionImport.setReturnType(returnType);
			} else if (type == FunctionImportReturnType.ENTITY) {
				returnType = OdataFactory.eINSTANCE
						.createReturnEntityTypeUsage();
				List<EntityType> allEntitiesIncludingReferenced = this
						.getAllEntitiesIncludingReferenced();
				if (allEntitiesIncludingReferenced != null
						&& allEntitiesIncludingReferenced.size() > 0) {
					EntityType entity = allEntitiesIncludingReferenced.get(0);
					((ReturnEntityTypeUsage) returnType).setEntityType(entity);
				}
				currentFunctionImport.setReturnType(returnType);
			} else {
				returnType = OdataFactory.eINSTANCE.createEnumTypeUsage();
				List<EnumType> allEnumTypesIncludingReferenced = this
						.getAllEnumTypesIncludingReferenced();
				if (allEnumTypesIncludingReferenced != null
						&& allEnumTypesIncludingReferenced.size() > 0) {
					EnumType enumType = allEnumTypesIncludingReferenced.get(0);
					((EnumTypeUsage) returnType).setEnumType(enumType);
				}
				currentFunctionImport.setReturnType(returnType);
			}

		}
		return returnType;
	}

	/**
	 * Creates a Default ObjectID Property for the object.
	 * 
	 * @param entityType
	 *            Related Entity
	 * @return Property Default ObjectID Property
	 */
	public Property createIDProperty(Object entityType) {

		String name = ""; //$NON-NLS-1$
		String pn, entityName = null;
		Property p;
		List<Property> keyList = new ArrayList<Property>();
		List<String> keyPropertyNames = new ArrayList<String>();

		if (entityType instanceof EntityType) {
			EntityType curentEntity = (EntityType) entityType;
			entityName = curentEntity.getName();
			keyList.addAll(curentEntity.getKeys());
			for (ListIterator<Property> it = keyList.listIterator(); it
					.hasNext();) {
				p = it.next();
				pn = p.getName();
				keyPropertyNames.add(pn);
			}
			name = entityName + Messages.ODATAEDITOR_PROPERTIES_DEFAULT_NAME;
			name = ArtifactNameUtil.generateUniqueName(keyPropertyNames,
					entityName, name,
					Messages.ODATAEDITOR_PROPERTIES_DEFAULT_NAME,
					IODataEditorConstants.MAX_LENGTH);

		}
		return createSimpleProperty(entityType, name, true);
	}

	/**
	 * Creates a new simple non complex type property for the given EntityType
	 * 
	 * @param entityType
	 *            Related Entity
	 * @param name
	 *            Property name
	 * @param isKey
	 *            true if the property is a key property
	 * @return Property created property
	 */
	public Property createSimpleProperty(Object entityType, String name,
			boolean isKey) {

		final Property property = createProperty(entityType, name, isKey);
		return property;
	}

	/**
	 * Creates a new property for the given EntityType
	 * 
	 * @param object
	 *            parent object
	 * @param name
	 *            Property name
	 * @param isKey
	 *            true if the property is a key property
	 * @return Property created property
	 */
	public Property createProperty(Object object, String name, boolean isKey) {

		Property property = null;

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(name);
		property.setNullable(!isKey);

		// Set the Type as Edm.String
		final OdataFactory factory = OdataFactory.eINSTANCE;
		final SimpleType simpleType = factory.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		final SimpleTypeUsage stu = factory.createSimpleTypeUsage();
		stu.setSimpleType(simpleType);
		property.setType(stu);

		if (object instanceof EntityType) {
			EntityType curentEntity = (EntityType) object;
			if (isKey) {
				curentEntity.getKeys().add(property);
			} else {
				curentEntity.getProperties().add(property);
			}
		} else if (object instanceof ComplexType) {
			ComplexType curentComplexType = (ComplexType) object;
			curentComplexType.getProperties().add(property);
		}

		return property;
	}

	/**
	 * Creates a new complex type property for the given EntityType or the
	 * complex type
	 * 
	 * @param object
	 *            Related Entity or ComplexType
	 * @param name
	 *            Property name
	 * @return Property created property
	 */
	public Property createComplexTypeProperty(Object object, String name) {

		Property property = null;

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(name);
		property.setNullable(false);

		// Set the Type as Edm.String
		final OdataFactory factory = OdataFactory.eINSTANCE;
		final ComplexTypeUsage ctu = factory.createComplexTypeUsage();
		property.setType(ctu);

		if (object instanceof EntityType) {
			EntityType curentEntity = (EntityType) object;
			curentEntity.getProperties().add(property);
		} else if (object instanceof ComplexType) {
			ComplexType curentComplexType = (ComplexType) object;
			curentComplexType.getProperties().add(property);
		}
		return property;
	}

	/**
	 * Creates a new enum type property for the given EntityType or the complex
	 * type
	 * 
	 * @param object
	 *            Related Entity or ComplexType
	 * @param name
	 *            Property name
	 * @return Property created property
	 */
	public Property createEnumTypeProperty(Object object, String name) {

		Property property = null;

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(name);
		property.setNullable(false);

		// Set the Type as Edm.String
		final OdataFactory factory = OdataFactory.eINSTANCE;
		final EnumTypeUsage etu = factory.createEnumTypeUsage();
		property.setType(etu);

		if (object instanceof EntityType) {
			EntityType curentEntity = (EntityType) object;
			curentEntity.getProperties().add(property);
		} else if (object instanceof ComplexType) {
			ComplexType curentComplexType = (ComplexType) object;
			curentComplexType.getProperties().add(property);
		}
		return property;
	}

	/**
	 * Creates an Association between the two entities Also creates the
	 * Referential constraints, Association Set and the Navigation Property
	 * 
	 * @param source
	 * @param target
	 * @param sourceMultiplicity
	 * @param targetMultiplicity
	 * 
	 * @return Association
	 */
	public Association createAssociation(EntityType source, EntityType target,
			MultiplicityType sourceMultiplicity,
			MultiplicityType targetMultiplicity) {

		Association association = OdataFactory.eINSTANCE.createAssociation();
		association.setName(ArtifactUtil.getValidDefaultName(ArtifactUtil
				.getAssociationName(source, target, this.schema)));

		// Create Source and Target Roles and set the Entity Types
		Role roleSource = createRole(source, true);
		Role roleTarget = createRole(target, false);

		// Set the Association Ends and Cardinality
		List<Role> ends = association.getEnds();
		ArtifactUtil.setMultiplicty(roleSource, sourceMultiplicity);
		ends.add(roleSource);
		ArtifactUtil.setMultiplicty(roleTarget, targetMultiplicity);
		ends.add(roleTarget);
		this.schema.getAssociations().add(association);
		// Create the default AssociationSet for the Association
		createDefaultAssociationSet(association);
		return association;
	}

	/**
	 * Creates a Role and sets the Entity Type
	 * 
	 * @param etype
	 *            Entity Type
	 * @return Role created role
	 */
	public Role createRole(EntityType etype, boolean isSource) {
		Role role = OdataFactory.eINSTANCE.createRole();
		if (isSource) {
			role.setName(Messages.ODATAEDITOR_ROLE_DEFAULTFROM
					+ etype.getName());
			role.setType(etype);
		} else {
			role.setName(Messages.ODATAEDITOR_ROLE_DEFAULTTO + etype.getName());
			role.setType(etype);
		}

		return role;
	}

	/**
	 * Creates a new navigation property for the given Association
	 * 
	 * @param sourceEntity
	 *            Entity assigned to the From Role of the Navigation Property
	 * @param navPropName
	 *            Default Navigation Property Name
	 * @param association
	 *            Association
	 * @return NavigationProperty created navigation property
	 */
	public NavigationProperty createNavigationProperty(EntityType sourceEntity,
			String navPropName, Association association) {

		return createNavigationProperty(sourceEntity, navPropName, association,
				false);
	}

	/**
	 * Creates a new navigation property for the given Association
	 * 
	 * @param sourceEntity
	 *            Entity assigned to the From Role of the Navigation Property
	 * @param navPropName
	 *            Default Navigation Property Name
	 * @param association
	 *            Association
	 * @param isSelfSecondNavProperty
	 * @return NavigationProperty created navigation property
	 */
	public NavigationProperty createNavigationProperty(EntityType sourceEntity,
			String navPropName, Association association,
			boolean isSelfSecondNavProperty) {

		NavigationProperty navProperty = OdataFactory.eINSTANCE
				.createNavigationProperty();
		navProperty.setName(navPropName);
		if (association != null) {
			navProperty.setRelationship(association);
			// Set From and To Roles
			Role assocEnd1 = association.getEnds().get(0);
			Role assocEnd2 = association.getEnds().get(1);
			// Set the Role corresponding to the source entity as the From Role
			// and
			// the other one as To Role
			if (assocEnd1.getType() == sourceEntity && !isSelfSecondNavProperty) {
				navProperty.setFromRole(assocEnd1);
				navProperty.setToRole(assocEnd2);
			} else if (assocEnd2.getType() == sourceEntity) {
				navProperty.setFromRole(assocEnd2);
				navProperty.setToRole(assocEnd1);
			}
		}
		// Add the Navigation Property as a child of the Entity Type
		sourceEntity.getNavigationProperties().add(navProperty);

		return navProperty;
	}

	/**
	 * Creates a default navigation property for the given Association
	 * 
	 * @param sourceEntity
	 *            Entity assigned to the From Role of the Navigation Property
	 * @param targetEntity
	 *            Entity assigned to the From Role of the Navigation Property
	 * @param association
	 *            Association
	 * @param navContext
	 * @return NavigationProperty created navigation property
	 */
	public NavigationProperty createDefaultNavigationProperty(
			EntityType sourceEntity, EntityType targetEntity,
			Association association, NavigationPropertyContext navContext) {
		return createDefaultNavigationProperty(sourceEntity, targetEntity,
				association, navContext, false);

	}

	/**
	 * Creates a default navigation property for the given Association
	 * 
	 * @param sourceEntity
	 *            Entity assigned to the From Role of the Navigation Property
	 * @param targetEntity
	 *            Entity assigned to the From Role of the Navigation Property
	 * @param association
	 *            Association
	 * @param navContext
	 * @return NavigationProperty created navigation property
	 */
	public NavigationProperty createDefaultNavigationProperty(
			EntityType sourceEntity, EntityType targetEntity,
			Association association, NavigationPropertyContext navContext,
			boolean isSelfSecondNavProperty) {
		String newNavPropName = Messages.ODATAEDITOR_NAVIGATION_PROPERTY;
		if (targetEntity != null) {
			newNavPropName = ArtifactUtil.getNavigationPropertyName(
					targetEntity, navContext.getTargetMultuplicity(),
					navContext.getAssociationType());

			newNavPropName = ArtifactUtil.getNavPropNameProposal(
					navContext.getTargetMultuplicity(),
					navContext.getAssociationType(), targetEntity.getName(),
					newNavPropName, sourceEntity.getNavigationProperties());

		} else {

			newNavPropName = ArtifactUtil.getNavPropNameProposal(
					navContext.getTargetMultuplicity(),
					navContext.getAssociationType(),
					Messages.ODATAEDITOR_NAVIGATION_PROPERTY, newNavPropName,
					sourceEntity.getNavigationProperties());

		}

		return createNavigationProperty(sourceEntity, newNavPropName,
				association, isSelfSecondNavProperty);
	}

	/**
	 * Creates an Association Set for the given Association
	 * 
	 * @param association
	 *            Association
	 * @return AssociationSet created AssociationSet
	 */
	public AssociationSet createDefaultAssociationSet(Association association) {
		String assocName = association.getName();
		List<String> assocSetNames = new ArrayList<String>();
		assocSetNames = ArtifactUtil.getEntSetAssocSetFuncImpNames(schema);
		String assocSetName = ArtifactNameUtil.generateUniqueName(
				assocSetNames, assocName, assocName
						+ IODataEditorConstants.DEFAULT_NAME_SUFFIX,
				IODataEditorConstants.DEFAULT_NAME_SUFFIX,
				IODataEditorConstants.MAX_LENGTH);
		return createAssociationSet(assocSetName, association);
	}

	/**
	 * Creates an Association Set for the given Association
	 * 
	 * @param assocSetName
	 * 
	 * @param association
	 *            Association
	 * @return AssociationSet created AssociationSet
	 */
	public AssociationSet createAssociationSet(String assocSetName,
			Association association) {

		AssociationSet associationSet = OdataFactory.eINSTANCE
				.createAssociationSet();
		associationSet.setName(assocSetName);
		associationSet.setAssociation(association);
		List<Role> associationsends = association.getEnds();

		for (Role role : associationsends) {
			EntityType entityType = role.getType();
			// Create AssociationSet end and set the EntitySet
			AssociationSetEnd assocSetEnd = OdataFactory.eINSTANCE
					.createAssociationSetEnd();
			assocSetEnd.setRole(role);
			if (ArtifactUtil.isReferencedEntityType(entityType)) {
				EList<EntitySet> entitySets = entityType.getEntitySets();
				ArrayList<EntitySet> currentScemaEntitySets = new ArrayList<EntitySet>();
				for (EntitySet entitySet1 : entitySets) {
					if (ArtifactUtil.getSchema(entitySet1) == this.schema) {
						currentScemaEntitySets.add(entitySet1);
					}
				}
				if (currentScemaEntitySets != null
						&& currentScemaEntitySets.size() == 1) {
					assocSetEnd.setEntitySet(currentScemaEntitySets.get(0));
				}
			} else {
				if (entityType != null
						&& entityType.getEntitySets().size() == 1) {
					assocSetEnd.setEntitySet(entityType.getEntitySets().get(0));
				}
			}
			associationSet.getEnds().add(assocSetEnd);
		}
		getEntityContainer().getAssociationSets().add(associationSet);

		return associationSet;
	}

	/**
	 * Creates a new Complex Type
	 * 
	 * @param complexTypeName
	 * @return complexType created ComplexType
	 */
	public ComplexType createComplexType(String complexTypeName) {

		ComplexType complexType = OdataFactory.eINSTANCE.createComplexType();
		complexType.setName(complexTypeName);
		this.schema.getComplexTypes().add(complexType);

		return complexType;
	}

	/**
	 * Creates a default Complex Type
	 * 
	 * @return ComplexType created ComplexType
	 */
	public ComplexType createDefaultComplexType() {
		return createComplexType(ArtifactUtil
				.getNameForComplexType(this.schema));
	}

}
