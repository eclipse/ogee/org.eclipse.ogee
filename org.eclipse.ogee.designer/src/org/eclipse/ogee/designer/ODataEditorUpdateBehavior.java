/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.ui.editor.DefaultUpdateBehavior;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.graphiti.ui.editor.IDiagramEditorInput;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.utils.logger.Logger;

/**
 * This overrides the DefaultUpdateBehavior provider class from Graphiti. This
 * is necessary in order to share the transaction/editing domain between Common
 * Navigator Framework (CNF) and the OData editor. This is necessary as the CNF
 * and the OData editor share the same transaction when working with the same
 * OData model.
 * 
 */
public class ODataEditorUpdateBehavior extends DefaultUpdateBehavior {
	private DiagramEditor diagramEditor;

	@Override
	protected void createEditingDomain(IDiagramEditorInput input) {
		super.createEditingDomain(input);
		TransactionalEditingDomain editingDomain = null;

		IFile file = ((ODataEditor) this.diagramEditor).getModelFile();
		try {
			editingDomain = IModelContext.INSTANCE.getTransaction(file);
		} catch (ModelAPIException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		}
		initializeEditingDomain(editingDomain);
	}

	/**
	 * @param diagramEditor
	 */
	public ODataEditorUpdateBehavior(DiagramEditor diagramEditor) {

		super(diagramEditor.getDiagramBehavior());
		this.diagramEditor = diagramEditor;
	}

	@Override
	protected void disposeEditingDomain() {

		TransactionalEditingDomain editingDomain = null;

		IFile file = ((ODataEditor) this.diagramEditor).getModelFile();
		try {
			editingDomain = IModelContext.INSTANCE.getTransaction(file);
			if (editingDomain != null) {
				editingDomain.getCommandStack().flush();
				IModelContext.INSTANCE.removeTransaction(editingDomain);
			}
		} catch (ModelAPIException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		}
	}

}
