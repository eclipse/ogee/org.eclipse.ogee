/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.api;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ogee.model.odata.EDMXSet;

public interface IODataDiagramInput {

	/**
	 * @return org.eclipse.core.resources.IFile
	 */
	IFile getDiagramFile();

	/**
	 * @param diagramFile
	 */
	void setDiagramFile(IFile diagramFile);

	/**
	 * @return org.eclipse.ogee.model.odata.EDMXSet
	 */
	EDMXSet getEDMXSet();

	/**
	 * @param edmxSet
	 */
	void setEDMXSet(EDMXSet edmxSet);

	/**
	 * @return boolean - if true, open the OData editor.
	 */
	boolean isOpenEditor();

	/**
	 * @param isOpenEditor
	 *            - by default the parameter is set to true. If we intend not to
	 *            open the OData Editor set the parameter to false.
	 */
	void setOpenEditor(boolean isOpenEditor);

	/**
	 * @return true if editor is opened in read only mode.
	 */
	boolean isReadOnlyMode();

	/**
	 * @param isReadOnlyMode
	 *            - set to true, if editor should be opened in read only mode.
	 */
	void setReadOnlyMode(boolean isReadOnlyMode);

	/**
	 * @return IProgressMonitor
	 */
	IProgressMonitor getProgressMonitor();

	/**
	 * @param monitor
	 *            - IProgressMonitor
	 */
	void setProgressMonitor(IProgressMonitor monitor);

	/**
	 * @return String - Gateway connection name.
	 */
	String getGWConnectionName();

	/**
	 * @param connectionName
	 *            - Gateway connection name.
	 */
	void setGWConnectionName(String connectionName);

	/**
	 * @return String - Service Name.
	 */
	String getServiceName();

	/**
	 * @param serviceName
	 *            - Service Name.
	 */
	void setServiceName(String serviceName);

	/**
	 * @return Integer - Service Version.
	 */
	Integer getServiceVersion();

	/**
	 * @param serviceVersion
	 *            - Service Version.
	 */
	void setServiceVersion(Integer serviceVersion);

}
