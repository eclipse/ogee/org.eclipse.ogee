/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.api;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.ogee.designer.input.ODataDiagramCreatorImpl;

/**
 * Creates OData Service Model in OData Graphical editor.
 * 
 */
public interface IODataDiagramCreator {

	/**
	 * The singleton instance of the IODataDiagramCreator implementation.
	 */
	IODataDiagramCreator INSTANCE = ODataDiagramCreatorImpl.getInstance();

	/**
	 * Creates an instance of IODataDiagramInput.
	 * 
	 * @return an instance of IODataDiagramInput.
	 */
	IODataDiagramInput createDiagramInput();

	/**
	 * Creates an OData Service Model in OData Graphical editor.
	 * 
	 * @param diagramInput
	 *            - IODataDiagramInput.
	 * @throws CoreException
	 *             in case of exceptions.
	 */
	void createDiagram(IODataDiagramInput diagramInput) throws CoreException;

	/**
	 * Returns true in case of Read Only Model.
	 * 
	 * @return boolean - true, for read-only model.
	 */
	boolean isReadOnlyModel();

	/**
	 * This method is responsible to refresh the editor from other workbench
	 * parts on model update.
	 * 
	 * @param affectedObject
	 *            Current Artifact
	 * @param prevValue
	 *            Value before model Update
	 * @param newValue
	 *            Value After model Update
	 */
	void refreshEditor(Object affectedObject, Object prevValue, Object newValue);
}
