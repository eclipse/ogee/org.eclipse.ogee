/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer;

import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.graphiti.ui.internal.dnd.ObjectsTransferDropTargetListener;
import org.eclipse.graphiti.ui.internal.editor.GraphitiScrollingGraphicalViewer;
import org.eclipse.jface.util.TransferDropTargetListener;

/**
 * The following class implementation disables all external droppings into the
 * diagram.
 * 
 */
public class ODataScrollingGraphicalViewer extends
		GraphitiScrollingGraphicalViewer {

	/**
	 * @param diagramEditor
	 *            - instance of DiagramEditor.
	 */
	public ODataScrollingGraphicalViewer(DiagramEditor diagramEditor) {

		super(diagramEditor.getDiagramBehavior());
	}

	/*
	 * In order to disable all external droppings into the diagram, the object
	 * transfer drop listener (ObjectsTransferDropTargetListener) is filtered
	 * out on adding.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.AbstractEditPartViewer#addDropTargetListener
	 * (org.eclipse.gef.dnd.TransferDropTargetListener)
	 */
	@Override
	public void addDropTargetListener(TransferDropTargetListener listener) {

		if (!(listener instanceof ObjectsTransferDropTargetListener)) {
			super.addDropTargetListener(listener);
		}
	}

}
