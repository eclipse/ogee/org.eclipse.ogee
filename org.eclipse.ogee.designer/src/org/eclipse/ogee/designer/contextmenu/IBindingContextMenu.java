package org.eclipse.ogee.designer.contextmenu;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * This extension needs to be implemented to add an additional  menu item to the
 * already existing context menu items for the specified UI elements in the
 * OData Model Editor and it extends IContextMenu
 * 
 */

public interface IBindingContextMenu extends IContextMenu {
	
	//To pass  handle of the PictogramElement to the extension implementing this interface
	void setPictogramElement(PictogramElement pe);
	//To pass  handle of the PictogramElement to the extension implementing this interface
	void setBusinessObject(EObject businessObject);

}
