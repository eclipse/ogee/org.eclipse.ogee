/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.contextmenu;

import java.util.EnumSet;

import org.eclipse.emf.ecore.EObject;

/**
 * This extension needs to be implemented to add an additional menu item to the
 * already existing context menu items for the specified UI elements in the
 * OData Model Editor
 * 
 */
public interface IContextMenu {
	/**
	 * Action to be performed on click of this menu item
	 */
	public void menuAction(EObject businessObject);

	/**
	 * Return a list of objects to which the menu item can be added
	 * 
	 * @return EnumSet Applicable UIElementType/s
	 */
	public EnumSet<UIElementType> getApplicableElements();
}
