package org.eclipse.ogee.designer.contextmenu;

import java.util.EnumSet;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

public interface IArtifactSelection {
	
	/**
	 * Set the handle to the  PictogramElement selected
	 * 
	 */
	void setSelectedPictogramElement(PictogramElement pe);
	/**
	 * Set the handle to the  EObject selected
	 * 
	 */
	void setSelectedEObjectElement(EObject eo);
	/**
	 * Set the handle to the  PictogramElement deleted
	 * 
	 */
	void setDeletedPictogramElement(PictogramElement pe);
	/**
	 *  Set the handle to the  EObject deleted
	 * 
	 */
	void setDeletedEObjectElement(EObject eo);
	/**
	 * Return a list of objects to which the menu item can be added
	 * 
	 * @return EnumSet Applicable UIElementType/s
	 */
	public EnumSet<UIElementType> getApplicableElements();

}
