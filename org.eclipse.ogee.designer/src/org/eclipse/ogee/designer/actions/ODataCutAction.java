/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.actions;

import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.ActionFactory;

/**
 * This class is written in order to disable the CUT Action Edit Menu. There is
 * no default implementation in Graphiti framework.
 * 
 * 
 */
public class ODataCutAction extends SelectionAction {

	private static final String TOOL_TIP = Messages.ODATA_CUTACTION_LABEL;

	/**
	 * CUT ACtion Message
	 */
	private static final String TEXT1 = Messages.ODATA_CUTACTION_TOOLTIP;

	/**
	 * CUT Action ID
	 */
	public static final String ACTION_ID = ActionFactory.CUT.getId();

	/**
	 * @param part
	 * 
	 */
	public ODataCutAction(IWorkbenchPart part) {
		super(part);
		setId(ACTION_ID);
		setText(TEXT1);
		setToolTipText(TOOL_TIP);
	}

	@Override
	protected boolean calculateEnabled() {
		return false;
	}

	@Override
	public void run() {
		// No need to implement
	}

}