/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.actions;

import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.jface.action.Action;
import org.eclipse.ogee.designer.features.ODataContextMenuFeature;

/**
 * This class is responsible to execute the Context Menu Feature.
 * 
 */
public class ODataContextMenuAction extends Action {
	private final ODataContextMenuFeature oDataContextMenuFeature;
	private ICustomContext contextMenuContext;

	/**
	 * @param contextMenuContext
	 * @param contextMenuFeature
	 * @param diagramEditor
	 * @param contextMenuId
	 */
	public ODataContextMenuAction(DiagramEditor diagramEditor,
			ODataContextMenuFeature contextMenuFeature,
			ICustomContext contextMenuContext, String contextMenuId) {
		super();
		setText(contextMenuFeature.getName());
		setToolTipText(contextMenuFeature.getDescription());
		setId(contextMenuId);
		this.oDataContextMenuFeature = contextMenuFeature;
		this.contextMenuContext = contextMenuContext;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		boolean canExecute = this.oDataContextMenuFeature
				.canExecute(this.contextMenuContext);
		if (canExecute) {
			this.oDataContextMenuFeature.execute(this.contextMenuContext);
		}
	}

}