/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.actions;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.ICustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.jface.action.Action;
import org.eclipse.ogee.designer.utils.PropertyUtil;

/**
 * This class is responsible to add check Action for LinkUsage. It is extending
 * CustomAction from org.eclipse.graphiti.ui.internal.action package.
 * 
 */
public class ODataLinkUsageContextAction extends Action {

	/**
	 * CustomContext Instance.
	 */
	private ICustomContext context;
	private ICustomFeature customFeature;
	private DiagramEditor diagramEditor;

	/**
	 * @param customFeature
	 * @param context
	 * @param diagramEditor
	 */
	public ODataLinkUsageContextAction(DiagramEditor diagramEditor,
			ICustomFeature customFeature, ICustomContext context) {
		super(customFeature.getName());
		this.context = context;
		this.customFeature = customFeature;
		this.diagramEditor = diagramEditor;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#getStyle()
	 */
	/**
	 * Default Style will be check Box.
	 */
	@Override
	public int getStyle() {
		// Infer the style from the value field.
		return AS_CHECK_BOX;
	}

	@Override
	public boolean isEnabled() {
		return getCustomFeature().canExecute(getContext());
	}

	@Override
	public void run() {

		if (this.diagramEditor != null) {
			final IDiagramTypeProvider dtp = this.diagramEditor
					.getDiagramTypeProvider();
			if (dtp != null && dtp.getDiagram() != null) {
				final TransactionalEditingDomain editingDomain = this.diagramEditor
						.getEditingDomain();
				if (editingDomain != null) {
					editingDomain.getCommandStack().execute(
							new RecordingCommand(editingDomain) {
								@Override
								public void doExecute() {
									boolean canExecute = getCustomFeature()
											.canExecute(getContext());
									if (canExecute) {
										getCustomFeature()
												.execute(getContext());
									}
								}

								@Override
								public String getLabel() {
									return getCustomFeature().getName();
								}
							});
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#isChecked()
	 */
	/**
	 * Based On ShowUsage for Specified Pictogram Element CheckBox State will be
	 * returned.
	 */
	@Override
	public boolean isChecked() {
		boolean checked = false;
		PictogramElement[] pes = this.getContext().getPictogramElements();
		if (pes.length > 0) {
			PictogramElement pe = pes[0];
			checked = PropertyUtil.isShowUsage(pe);
		}
		return checked;
	}

	/**
	 * @return the customFeature
	 */
	public ICustomFeature getCustomFeature() {
		return this.customFeature;
	}

	/**
	 * @return the context
	 */
	public ICustomContext getContext() {
		return this.context;
	}

}
