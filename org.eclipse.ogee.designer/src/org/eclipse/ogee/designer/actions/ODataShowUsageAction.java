/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.actions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.jface.action.Action;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.features.ODataLinkUsageFeature;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataFeatureProvider;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;

/**
 * Show/Hide All Usages of Types in the Graphical editor.
 * 
 * 
 */
public class ODataShowUsageAction extends Action {

	private DiagramEditor graphicsEditor;
	private boolean isShown;
	private boolean isHidden;

	/**
	 * @param graphicsEditor
	 */
	public ODataShowUsageAction(DiagramEditor graphicsEditor) {

		super();
		this.graphicsEditor = graphicsEditor;
		setText(Messages.SHOW_USAGE_TEXT_DISPLAY);
		setToolTipText(Messages.SHOW_USAGE_TOOL_TIP);
		setId(IODataEditorConstants.ACTION_ID_SHOW_HIDE_USAGES);
		setActionDefinitionId(IODataEditorConstants.ACTION_DEF_ID_SHOW_HIDE_USAGES);
	}

	/*
	 * The following implementation will show/hide all link usages based on the
	 * state of the toggle button "Show/Hide All Usages" in the eclipse tool
	 * bar.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {

		this.isShown = false;
		this.isHidden = false;

		if (this.graphicsEditor != null) {
			final TransactionalEditingDomain editingDomain = this.graphicsEditor
					.getEditingDomain();
			if (editingDomain != null
					&& editingDomain.getCommandStack() != null) {
				editingDomain.getCommandStack().execute(
						new RecordingCommand(editingDomain) {
							@Override
							public void doExecute() {
								final IDiagramTypeProvider dtp = getGraphicsEditor()
										.getDiagramTypeProvider();
								if (dtp != null) {
									final Diagram odataDiagram = dtp
											.getDiagram();
									if (odataDiagram != null) {
										showHideAllUsages(odataDiagram, dtp);
										// On click of Show Usage All toggle
										// button, if there is no usage to be
										// shown/hidden then do not change
										// state of button.
										if (isShown() || isHidden()) {
											setToggleButtonStatus(odataDiagram);
										}
									}
								}
							}
						});
			}
		}
	}

	/**
	 * @return DiagramEditor - OData Editor instance
	 */
	public DiagramEditor getGraphicsEditor() {
		return this.graphicsEditor;
	}

	/**
	 * @return boolean - isShown
	 */
	public boolean isShown() {
		return this.isShown;
	}

	/**
	 * @return boolean - isHidden
	 */
	public boolean isHidden() {
		return this.isHidden;
	}

	/*
	 * The following method will maintain the status of the
	 * "Show/Hide All Usages" toggle button.
	 */
	protected void setToggleButtonStatus(Diagram odataDiagram) {

		if (odataDiagram != null) {
			if (isChecked()) {
				PropertyUtil.setShowAllUsage(odataDiagram, true);
			} else {
				PropertyUtil.setShowAllUsage(odataDiagram, false);
			}
		}
	}

	/*
	 * The following method will show/hide all link usages based on the state of
	 * the toggle button "Show/Hide All Usages" in the eclipse tool bar.
	 */
	protected void showHideAllUsages(Diagram odataDiagram,
			IDiagramTypeProvider dtp) {

		if (odataDiagram != null) {
			final EList<Shape> pes = odataDiagram.getChildren();
			if (isChecked()) {
				PropertyUtil.setShowAllUsage(odataDiagram, true);
			}
			for (PictogramElement pe : pes) {
				// if the show usage check button is
				// checked, show link usage.
				if (isChecked()) {
					showLinkUsage(pe, dtp);

				} else {
					// if the show usage check button
					// is released, hide link usage.
					hideLinkUsage(pe, dtp);
				}
			}
		}
	}

	/*
	 * The following method will show link usage.
	 */
	private void showLinkUsage(final PictogramElement pe,
			final IDiagramTypeProvider dtp) {

		if (!PropertyUtil.isShowUsage(pe) && PropertyUtil.isTopContainer(pe)) {
			final IFeatureProvider featureProvider = dtp.getFeatureProvider();
			final Object businessObject = featureProvider
					.getBusinessObjectForPictogramElement(pe);
			if (businessObject instanceof EnumType
					|| businessObject instanceof ComplexType
					|| businessObject instanceof EntityType) {
				final ODataLinkUsageFeature linkUsageFeature = new ODataLinkUsageFeature(
						featureProvider,
						((ODataFeatureProvider) featureProvider)
								.getArtifactFactory(), new ShapesFactory(
								featureProvider), true, true);
				final CustomContext context = new CustomContext();
				context.setPictogramElements(new PictogramElement[] { pe });
				linkUsageFeature.execute(context);
				if (linkUsageFeature.isShown()) {
					this.isShown = true;
				}
			}
		}
	}

	/*
	 * The following method will hide link usage.
	 */
	private void hideLinkUsage(final PictogramElement pe,
			final IDiagramTypeProvider dtp) {

		if (PropertyUtil.isShowUsage(pe) && PropertyUtil.isTopContainer(pe)) {
			final IFeatureProvider featureProvider = dtp.getFeatureProvider();
			final Object businessObject = featureProvider
					.getBusinessObjectForPictogramElement(pe);
			if (businessObject instanceof EnumType
					|| businessObject instanceof ComplexType
					|| businessObject instanceof EntityType) {
				final ODataLinkUsageFeature linkUsageFeature = new ODataLinkUsageFeature(
						featureProvider,
						((ODataFeatureProvider) featureProvider)
								.getArtifactFactory(), new ShapesFactory(
								featureProvider), false, true);
				final CustomContext context = new CustomContext();
				context.setPictogramElements(new PictogramElement[] { pe });
				linkUsageFeature.execute(context);
				if (linkUsageFeature.isHidden()) {
					this.isHidden = true;
				}
			}
		}
	}

}
