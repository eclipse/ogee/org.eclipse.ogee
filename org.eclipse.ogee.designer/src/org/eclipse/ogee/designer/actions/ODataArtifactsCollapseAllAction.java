/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.actions;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.jface.action.Action;
import org.eclipse.ogee.designer.features.ODataCollapsibleFeature;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.PropertyUtil;

/**
 * Collapses all OData artifacts on the Graphical editor.
 * 
 */
public class ODataArtifactsCollapseAllAction extends Action {

	private DiagramEditor graphicsEditor;
	private boolean isCollapsed;

	/**
	 * @param graphicsEditor
	 */
	public ODataArtifactsCollapseAllAction(DiagramEditor graphicsEditor) {

		super();
		this.graphicsEditor = graphicsEditor;
		setText(Messages.COLLAPSE_ALL_TEXT_DISPLAY);
		setToolTipText(Messages.COLLAPSE_ALL_TOOL_TIP);
		setId(IODataEditorConstants.ACTION_ID_COLLAPSE_ALL);
		setActionDefinitionId(IODataEditorConstants.ACTION_DEF_ID_COLLAPSE_ALL);
	}

	/*
	 * The following method is invoked if collapse all push button is pressed.
	 * Override this method to collapse all OData artifacts.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {

		this.isCollapsed = false;
		if (this.graphicsEditor != null) {
			final IDiagramTypeProvider dtp = this.graphicsEditor
					.getDiagramTypeProvider();
			if (dtp != null && dtp.getDiagram() != null) {
				final EList<Shape> shapes = dtp.getDiagram().getChildren();
				if (!shapes.isEmpty()) {
					final TransactionalEditingDomain editingDomain = this.graphicsEditor
							.getEditingDomain();
					if (editingDomain != null) {
						editingDomain.getCommandStack().execute(
								new RecordingCommand(editingDomain) {
									@Override
									public void doExecute() {
										for (Shape shape : shapes) {
											collapseGroup(shape, dtp);
										}
									}
								});
						if (!this.isCollapsed) {
							editingDomain.getCommandStack().undo();
						}
					}
				}
			}
		}
	}

	/*
	 * The following method will collapse the expanded group.
	 */
	protected void collapseGroup(final Shape shape,
			final IDiagramTypeProvider dtp) {

		if (PropertyUtil.isTopContainer(shape)) {
			final IFeatureProvider featureProvider = dtp.getFeatureProvider();
			if (featureProvider != null) {
				final Object businessObject = featureProvider
						.getBusinessObjectForPictogramElement(shape);
				if (businessObject instanceof EObject) {
					final EObject eObj = (EObject) businessObject;
					final List<PictogramElement> pes = Graphiti
							.getLinkService().getPictogramElements(
									dtp.getDiagram(), eObj);
					for (PictogramElement pe : pes) {
						if (pe.getGraphicsAlgorithm() instanceof Image
								&& PropertyUtil.isTitle(pe)
								&& PropertyUtil
										.isExpanded((PictogramElement) pe
												.eContainer())) {
							final ODataCollapsibleFeature collapsibleFeature = new ODataCollapsibleFeature(
									dtp.getFeatureProvider());
							final CustomContext context = new CustomContext();
							context.setPictogramElements(new PictogramElement[] { (PictogramElement) pe
									.eContainer() });
							collapsibleFeature.execute(context);
							this.isCollapsed = true;
						}
					}
				}
			}
		}
	}

}
