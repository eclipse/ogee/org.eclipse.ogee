/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.actions;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.jface.action.Action;
import org.eclipse.ogee.designer.features.ODataLayoutFeature;

/**
 * This class is responsible to execute the Layout Feature.
 * 
 */
public class ODataLayoutAction extends Action {

	private DiagramEditor diagramEditor;
	private final ODataLayoutFeature layoutFeature;
	private CustomContext layoutContext;

	/**
	 * @param layoutContext
	 * @param layoutFeature
	 * @param diagramEditor
	 * @param layoutId
	 */
	public ODataLayoutAction(DiagramEditor diagramEditor,
			ODataLayoutFeature layoutFeature, CustomContext layoutContext,
			String layoutId) {

		super();
		this.diagramEditor = diagramEditor;
		setText(layoutFeature.getName());
		setToolTipText(layoutFeature.getDescription());
		setId(layoutId);
		this.layoutFeature = layoutFeature;
		this.layoutContext = layoutContext;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {

		if (this.diagramEditor != null) {
			final IDiagramTypeProvider dtp = this.diagramEditor
					.getDiagramTypeProvider();
			if (dtp != null && dtp.getDiagram() != null) {
				final TransactionalEditingDomain editingDomain = this.diagramEditor
						.getEditingDomain();
				if (editingDomain != null) {
					editingDomain.getCommandStack().execute(
							new RecordingCommand(editingDomain) {
								@Override
								public void doExecute() {
									boolean canExecute = getLayoutFeature()
											.canExecute(getLayoutContext());
									if (canExecute) {
										getLayoutFeature().execute(
												getLayoutContext());
									}
								}

								@Override
								public String getLabel() {
									return getLayoutFeature().getName();
								}
							});

				}
			}
		}
	}

	/**
	 * @return the layoutContext
	 */
	public CustomContext getLayoutContext() {
		return this.layoutContext;
	}

	/**
	 * @param layoutContext
	 *            the layoutContext to set
	 */
	public void setLayoutContext(CustomContext layoutContext) {
		this.layoutContext = layoutContext;
	}

	/**
	 * @return the layoutFeature
	 */
	public ODataLayoutFeature getLayoutFeature() {
		return this.layoutFeature;
	}

}
