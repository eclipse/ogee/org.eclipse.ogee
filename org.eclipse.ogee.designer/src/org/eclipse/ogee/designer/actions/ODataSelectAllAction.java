/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.gef.ui.actions.SelectAllAction;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ogee.designer.ODataEditor;
import org.eclipse.ogee.designer.ODataMultiPageEditor;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ui.IWorkbenchPart;

/**
 * This class is written in order to disable the CUT Action Edit Menu. There is
 * no default implementation in Graphiti framework.
 * 
 */
public class ODataSelectAllAction extends SelectAllAction {

	private IWorkbenchPart part;

	/**
	 * @param part
	 */
	public ODataSelectAllAction(IWorkbenchPart part) {
		super(part);
		this.part = part;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.SelectAllAction#run()
	 */
	@Override
	public void run() {
		if (this.part instanceof ODataMultiPageEditor.DesignEditor) {
			ODataEditor diagramEditor = (ODataEditor) this.part;
			Diagram diagram = diagramEditor.getDiagramTypeProvider()
					.getDiagram();
			List<PictogramElement> shapes = getSelectableChildren(diagram);
			diagramEditor.selectPictogramElements(shapes
					.toArray(new PictogramElement[0]));
		}
	}

	/**
	 * this method is responsible to filter the selectable children.
	 * 
	 * @param diagram
	 * @return
	 */
	private List<PictogramElement> getSelectableChildren(Diagram diagram) {
		List<PictogramElement> shapes = new ArrayList<PictogramElement>();
		EList<Connection> connections = diagram.getConnections();
		shapes.addAll(diagram.getChildren());
		shapes.addAll(diagram.getAnchors());
		for (Connection connection : connections) {
			if (!PropertyUtil.isLinkConnection(connection)) {
				shapes.add(connection);
			}
		}
		return shapes;
	}

}