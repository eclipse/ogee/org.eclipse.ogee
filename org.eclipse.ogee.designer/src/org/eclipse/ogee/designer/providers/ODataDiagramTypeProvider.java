/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.providers;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.platform.IDiagramBehavior;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;

/**
 * Initializes the feature providers and tool behavior providers.
 * 
 */
public class ODataDiagramTypeProvider extends AbstractDiagramTypeProvider {

	/**
	 * OData Feature Provider
	 */
	private ODataFeatureProvider featureProvider;
	private IToolBehaviorProvider[] availableToolBehaviorProviders;

	/**
	 * Default constructor. Creates new ODataDiagramTypeProvider and associates
	 * ODataFeatureProvider as feature provider.
	 */
	public ODataDiagramTypeProvider() {
		super();
		this.featureProvider = new ODataFeatureProvider(this);
		setFeatureProvider(this.featureProvider);
	}

	/**
	 * Returns all available tool behavior providers
	 * 
	 * ODatatoolBehaviourProvider is initialized
	 * 
	 * @return IToolBehaviorProvider[] An array of all registered tool behavior
	 *         providers
	 */
	@Override
	public IToolBehaviorProvider[] getAvailableToolBehaviorProviders() {
		if (this.availableToolBehaviorProviders == null) {
			this.availableToolBehaviorProviders = new IToolBehaviorProvider[] { new ODataToolBehaviorProvider(
					this) };
		}
		return this.availableToolBehaviorProviders;
	}

	/**
	 * Diagram instance will be initialized for Artifact Factory.
	 */
	@Override
	public void init(Diagram diagram, IDiagramBehavior diagramBehavior) {
		super.init(diagram, diagramBehavior);
		initProvider(diagram);
	}

	protected void initProvider(Diagram diagram) {
		this.featureProvider.getArtifactFactory().init(diagram);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.graphiti.dt.AbstractDiagramTypeProvider#
	 * isAutoUpdateAtRuntimeWhenEditorIsSaved()
	 */
	/**
	 * update the editor even if it is not in dirty state.
	 */
	@Override
	public boolean isAutoUpdateAtRuntimeWhenEditorIsSaved() {
		return true;
	}
}
