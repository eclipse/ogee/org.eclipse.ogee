/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.providers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.graphiti.ui.platform.AbstractImageProvider;

/**
 * This registers all icons associated with the Editor plugin. The icons can be
 * accessed through their paths.
 * 
 */
public class ODataImageProvider extends AbstractImageProvider {

	/**
	 * Default Constructor
	 */
	public ODataImageProvider() {

		super();
	}

	/**
	 * The prefix for all identifiers of this image provider
	 */
	protected static final String PREFIX = "org.eclipse.ogee.model.editor.";//$NON-NLS-1$	
	/**
	 * The prefix for SAP identifiers of this image provider to support
	 * additional image id prefixes
	 */
	protected static final String PREFIX_SAP_1 = "com.sap.odata.dte.model.editor."; //$NON-NLS-1$
	protected static final String PREFIX_SAP_2 = "com.sap.odata.dt.framework.model.editor"; //$NON-NLS-1$

	/**
	 * The image identifier for entity type.
	 */
	public static final String IMG_ENTITY = PREFIX + "entity";//$NON-NLS-1$

	/**
	 * The image identifier for complex type.
	 */
	public static final String IMG_CMPLX_TYPE = PREFIX + "cplx_typ";//$NON-NLS-1$

	/**
	 * The image identifier for single directional association.
	 */
	public static final String IMG_SIN_ASSOCIATION = PREFIX + "uni_dir";//$NON-NLS-1$

	/**
	 * The image identifier for bi directional association.
	 */
	public static final String IMG_BI_ASSOCIATION = PREFIX + "bi_dir";//$NON-NLS-1$

	/**
	 * The image identifier for entity set.
	 */
	public static final String IMG_ENTITY_SET = PREFIX + "entityset";//$NON-NLS-1$

	/**
	 * The image identifier for entity set.
	 */
	public static final String IMG_ENTITY_SET_ONE = PREFIX + "entityset_one";//$NON-NLS-1$

	/**
	 * The image identifier for entity set.
	 */
	public static final String IMG_ENTITY_SET_TWO = PREFIX + "entityset_two";//$NON-NLS-1$

	/**
	 * The image identifier for function import.
	 */
	public static final String IMG_FUNC_IMP = PREFIX + "func_imp";//$NON-NLS-1$

	/**
	 * The image identifier for property.
	 */
	public static final String IMG_PROPERTY = PREFIX + "property";//$NON-NLS-1$

	/**
	 * The image identifier for property.
	 */
	public static final String IMG_NAV_PROPERTY = PREFIX + "nav_prop";//$NON-NLS-1$

	/**
	 * The image identifier for expand button.
	 */
	public static final String IMG_EXPAND = PREFIX + "expand";//$NON-NLS-1$

	/**
	 * The image identifier for collapse button.
	 */
	public static final String IMG_COLLAPSE = PREFIX + "collapse";//$NON-NLS-1$

	/**
	 * The image identifier for Key property.
	 */
	public static final String IMG_KEY_PROPERTY = PREFIX + "key";//$NON-NLS-1$

	/**
	 * The image identifier for Errors.
	 */
	public static final String IMG_ERROR = PREFIX + "Error";//$NON-NLS-1$

	/**
	 * The image identifier for ComplexTypeCollection.
	 */
	public static final String IMG_CPLX_TYP_COLLN = PREFIX
			+ "ComplexType_collection";//$NON-NLS-1$

	/**
	 * The image identifier for EntitySetCollection.
	 */
	public static final String IMG_ENTY_SET_COLLN = PREFIX
			+ "entity_set_collection";//$NON-NLS-1$

	/**
	 * The image identifier for Parameter.
	 */
	public static final String IMG_PARAMETER = PREFIX + "parameter";//$NON-NLS-1$

	/**
	 * The image identifier for PropertyCollection.
	 */
	public static final String IMG_PROPERTY_COLLN = PREFIX
			+ "property_collection";//$NON-NLS-1$

	/**
	 * The image identifier for errors icon.
	 */
	public static final String IMG_ERROR_CO = PREFIX + "error_co"; //$NON-NLS-1$

	/**
	 * The image identifier for warning icon.
	 */
	public static final String IMG_WARNING_CO = PREFIX + "warning_co"; //$NON-NLS-1$

	/**
	 * The image identifier for enum icon.
	 */
	public static final String IMG_ENUM = PREFIX + "enum"; //$NON-NLS-1$

	/**
	 * The image identifier for enum collection icon.
	 */
	public static final String IMG_ENUM_COLLN = PREFIX + "enum_colln"; //$NON-NLS-1$

	/**
	 * The image identifier for Show/Hide Usage icon.
	 */
	public static final String IMG_DISPLAY_RELATION = PREFIX
			+ "DisplayRelations"; //$NON-NLS-1$

	/**
	 * The image file path for "Show/Hide All Usages" icon.
	 */
	public static final String IMG_FILE_PATH_DISP_REL = "icons/WhereUsed.png"; //$NON-NLS-1$

	/**
	 * Expand All Image
	 */
	public static final String IMG_EDIT_EXPANDALL = "icons/expandall.gif";//$NON-NLS-1$

	/**
	 * CollapseAll Image
	 */
	public static final String IMG_EDIT_COLLAPSEALL = "icons/collapseall.gif";//$NON-NLS-1$

	/**
	 * The image file path for Entity Reference icon.
	 */
	public static final String IMG_ENTITY_REFERENCE = "icons/EntityReference.gif"; //$NON-NLS-1$

	/**
	 * Adds the paths of the images from the icons folder
	 */
	@Override
	protected void addAvailableImages() {

		List<String> prefixList = new ArrayList<String>();
		prefixList.add(PREFIX_SAP_1);
		prefixList.add(PREFIX_SAP_2);

		// add image identifiers to support additional image id prefixes
		this.addImageFilePath(prefixList);

		addImageFilePath(IMG_ENTITY, "icons/entity.png"); //$NON-NLS-1$
		addImageFilePath(IMG_CMPLX_TYPE, "icons/cplx_typ.png"); //$NON-NLS-1$
		addImageFilePath(IMG_SIN_ASSOCIATION, "icons/uni_dir.gif"); //$NON-NLS-1$
		addImageFilePath(IMG_BI_ASSOCIATION, "icons/bi_dir.gif"); //$NON-NLS-1$
		addImageFilePath(IMG_ENTITY_SET, "icons/entityset.png"); //$NON-NLS-1$
		addImageFilePath(IMG_ENTITY_SET_ONE, "icons/entityset_one.png"); //$NON-NLS-1$
		addImageFilePath(IMG_ENTITY_SET_TWO, "icons/entityset_two.png"); //$NON-NLS-1$
		addImageFilePath(IMG_FUNC_IMP, "icons/func_imp.png"); //$NON-NLS-1$
		addImageFilePath(IMG_PROPERTY, "icons/property.png"); //$NON-NLS-1$
		addImageFilePath(IMG_EXPAND, "icons/expand.png"); //$NON-NLS-1$
		addImageFilePath(IMG_COLLAPSE, "icons/collapse.png"); //$NON-NLS-1$
		addImageFilePath(IMG_KEY_PROPERTY, "icons/key.gif"); //$NON-NLS-1$
		addImageFilePath(IMG_ERROR, "icons/Error.gif"); //$NON-NLS-1$
		addImageFilePath(IMG_NAV_PROPERTY, "icons/nav_prop.gif"); //$NON-NLS-1$
		addImageFilePath(IMG_CPLX_TYP_COLLN, "icons/ComplexType_collection.png"); //$NON-NLS-1$
		addImageFilePath(IMG_ENTY_SET_COLLN, "icons/entity_set_collection.png"); //$NON-NLS-1$
		addImageFilePath(IMG_PARAMETER, "icons/parameter.png"); //$NON-NLS-1$
		addImageFilePath(IMG_PROPERTY_COLLN, "icons/property_collection.png"); //$NON-NLS-1$
		addImageFilePath(IMG_ERROR_CO, "icons/error_co.gif"); //$NON-NLS-1$
		addImageFilePath(IMG_WARNING_CO, "icons/warning_co.gif"); //$NON-NLS-1$
		addImageFilePath(IMG_ENUM, "icons/enum.png"); //$NON-NLS-1$
		addImageFilePath(IMG_ENUM_COLLN, "icons/enum_colln.png"); //$NON-NLS-1$
		addImageFilePath(IMG_DISPLAY_RELATION, "icons/WhereUsed.png"); //$NON-NLS-1$
		addImageFilePath(IMG_ENTITY_REFERENCE, "icons/EntityReference.gif"); //$NON-NLS-1$	

	}

	/**
	 * This method will add the image identifiers to image path to support
	 * additional image id prefixes.
	 */
	private void addImageFilePath(List<String> prefixList) {

		for (String prefix : prefixList) {
			addImageFilePath(prefix + "entity", "icons/entity.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "cplx_typ", "icons/cplx_typ.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "uni_dir", "icons/uni_dir.gif"); //$NON-NLS-1$
			addImageFilePath(prefix + "bi_dir", "icons/bi_dir.gif"); //$NON-NLS-1$
			addImageFilePath(prefix + "entityset", "icons/entityset.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "func_imp", "icons/func_imp.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "property", "icons/property.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "expand", "icons/expand.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "collapse", "icons/collapse.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "key", "icons/key.gif"); //$NON-NLS-1$
			addImageFilePath(prefix + "Error", "icons/Error.gif"); //$NON-NLS-1$
			addImageFilePath(prefix + "nav_prop", "icons/nav_prop.gif"); //$NON-NLS-1$
			addImageFilePath(prefix + "ComplexType_collection",
					"icons/ComplexType_collection.png"); //$NON-NLS-1$
			addImageFilePath(
					prefix + "entity_set_collection", "icons/entity_set_collection.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "parameter", "icons/parameter.png"); //$NON-NLS-1$
			addImageFilePath(
					prefix + "property_collection", "icons/property_collection.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "error_co", "icons/error_co.gif"); //$NON-NLS-1$
			addImageFilePath(prefix + "warning_co", "icons/warning_co.gif"); //$NON-NLS-1$
			addImageFilePath(prefix + "enum", "icons/enum.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "enum_colln", "icons/enum_colln.png"); //$NON-NLS-1$
			addImageFilePath(prefix + "DisplayRelations", "icons/WhereUsed.png"); //$NON-NLS-1$
		}
	}
}
