/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.providers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.graphiti.datatypes.ILocation;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.ICreateConnectionFeature;
import org.eclipse.graphiti.features.ICreateFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.IDoubleClickContext;
import org.eclipse.graphiti.features.context.IPictogramElementContext;
import org.eclipse.graphiti.features.context.impl.CreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.features.custom.ICustomFeature;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.palette.IPaletteCompartmentEntry;
import org.eclipse.graphiti.palette.impl.ConnectionCreationToolEntry;
import org.eclipse.graphiti.palette.impl.ObjectCreationToolEntry;
import org.eclipse.graphiti.palette.impl.PaletteCompartmentEntry;
import org.eclipse.graphiti.platform.IDiagramContainer;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.tb.ContextButtonEntry;
import org.eclipse.graphiti.tb.ContextMenuEntry;
import org.eclipse.graphiti.tb.DefaultContextButtonPadData;
import org.eclipse.graphiti.tb.DefaultToolBehaviorProvider;
import org.eclipse.graphiti.tb.IContextButtonPadData;
import org.eclipse.graphiti.tb.IContextMenuEntry;
import org.eclipse.graphiti.tb.IDecorator;
import org.eclipse.graphiti.util.ILocationInfo;
import org.eclipse.graphiti.util.LocationInfo;
import org.eclipse.ogee.designer.ODataEditor;
import org.eclipse.ogee.designer.contexts.NavigationPropertyContext;
import org.eclipse.ogee.designer.factories.ShapesFactory;
import org.eclipse.ogee.designer.features.ODataAddAssociationFeature;
import org.eclipse.ogee.designer.features.ODataAddComplexPropertyFeature;
import org.eclipse.ogee.designer.features.ODataAddEntitySetFeature;
import org.eclipse.ogee.designer.features.ODataAddEnumMemberFeature;
import org.eclipse.ogee.designer.features.ODataAddNavigationPropertyFeature;
import org.eclipse.ogee.designer.features.ODataAddParameterFeature;
import org.eclipse.ogee.designer.features.ODataAddPropertyFeature;
import org.eclipse.ogee.designer.features.ODataAddReturnTypeFeature;
import org.eclipse.ogee.designer.features.ODataCollapsibleFeature;
import org.eclipse.ogee.designer.features.ODataDoubleClickFeature;
import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.types.FunctionImportReturnType;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.ODataShapeUtil;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.swt.graphics.Point;

/**
 * Tool behavior provider. Used to add behaviors to the menus and artifacts.
 * 
 */
public class ODataToolBehaviorProvider extends DefaultToolBehaviorProvider {
	/**
	 * Creates a new {@link DefaultToolBehaviorProvider}.
	 * 
	 * @param diagramTypeProvider
	 *            the diagram type provider
	 */
	public ODataToolBehaviorProvider(IDiagramTypeProvider diagramTypeProvider) {
		super(diagramTypeProvider);
	}

	/**
	 * This returns the context menu data of the menu around the created
	 * artifact after adding custom buttons. This will eventually be called
	 * internally to show the custom and default buttons in the menu
	 * 
	 * @return IContextButtonPadData
	 * @see IPictogramElementContext
	 */
	@Override
	public IContextButtonPadData getContextButtonPad(
			IPictogramElementContext context) {

		if (this.isReadOnlyModel()) {
			return null;
		}

		// Get the current element on the screen
		PictogramElement selPe = context.getPictogramElement();

		PictogramElement pe = ODataShapeUtil.getTopContainer(selPe);
		if (pe == null || !PropertyUtil.isContextPadEnabled(pe)) {
			return null;
		}

		IContextButtonPadData data = new DefaultContextButtonPadData();
		GraphicsAlgorithm ga = getSelectionBorder(pe);
		if (ga == null) {
			ga = pe.getGraphicsAlgorithm();
		}

		// set default location
		if (ga != null) {
			ILocation origin = getAbsoluteLocation(ga);
			data.getPadLocation().setRectangle(origin.getX(), origin.getY(),
					ga.getWidth(), ga.getHeight());
		}

		// Default buttons
		setGenericContextButtons(data, pe, CONTEXT_BUTTON_DELETE);
		CustomContext customContext = new CustomContext(
				new PictogramElement[] { pe });
		if (getFeatureProvider().getBusinessObjectForPictogramElement(pe) instanceof EntityType) {
			if (!(ArtifactUtil.isReferencedEntityType(getFeatureProvider()
					.getBusinessObjectForPictogramElement(pe)))) {
				addEntityContextButtons(context, pe, data);
			} else {
				// Add custom button for the Add parameter feature
				ContextButtonEntry button = new ContextButtonEntry(
						new ODataAddEntitySetFeature(getFeatureProvider(),
								((ODataFeatureProvider) getFeatureProvider())
										.getArtifactFactory(),
								new ShapesFactory(this.getFeatureProvider())),
						customContext);
				button.setIconId(ODataImageProvider.IMG_ENTITY_SET);
				data.getDomainSpecificContextButtons().add(button);
			}

		} else if (getFeatureProvider()
				.getBusinessObjectForPictogramElement(pe) instanceof FunctionImport) {

			// Add custom button for the Add parameter feature
			ContextButtonEntry button = new ContextButtonEntry(
					new ODataAddParameterFeature(getFeatureProvider(),
							((ODataFeatureProvider) getFeatureProvider())
									.getArtifactFactory(), new ShapesFactory(
									this.getFeatureProvider())), customContext);
			button.setIconId(ODataImageProvider.IMG_PARAMETER);
			data.getDomainSpecificContextButtons().add(button);

			button = new ContextButtonEntry(new ODataAddReturnTypeFeature(
					getFeatureProvider(),
					((ODataFeatureProvider) getFeatureProvider())
							.getArtifactFactory(), new ShapesFactory(
							this.getFeatureProvider()),
					FunctionImportReturnType.SIMPLE), customContext);
			button.setIconId(ODataImageProvider.IMG_PROPERTY);
			data.getDomainSpecificContextButtons().add(button);
			button = new ContextButtonEntry(new ODataAddReturnTypeFeature(
					getFeatureProvider(),
					((ODataFeatureProvider) getFeatureProvider())
							.getArtifactFactory(), new ShapesFactory(
							this.getFeatureProvider()),
					FunctionImportReturnType.COMPLEX), customContext);
			button.setIconId(ODataImageProvider.IMG_CMPLX_TYPE);
			data.getDomainSpecificContextButtons().add(button);
			button = new ContextButtonEntry(new ODataAddReturnTypeFeature(
					getFeatureProvider(),
					((ODataFeatureProvider) getFeatureProvider())
							.getArtifactFactory(), new ShapesFactory(
							this.getFeatureProvider()),
					FunctionImportReturnType.ENTITY), customContext);
			button.setIconId(ODataImageProvider.IMG_ENTITY);
			data.getDomainSpecificContextButtons().add(button);			
		} else if (getFeatureProvider()
				.getBusinessObjectForPictogramElement(pe) instanceof ComplexType) {
			// Add custom button for the Add property feature
			ContextButtonEntry button = new ContextButtonEntry(
					new ODataAddPropertyFeature(getFeatureProvider(),
							((ODataFeatureProvider) getFeatureProvider())
									.getArtifactFactory(), new ShapesFactory(
									this.getFeatureProvider())), customContext);
			button.setIconId(ODataImageProvider.IMG_PROPERTY);
			data.getDomainSpecificContextButtons().add(button);

			// Add custom button for the Add property feature
			ContextButtonEntry complexTypeButton = new ContextButtonEntry(
					new ODataAddComplexPropertyFeature(getFeatureProvider(),
							((ODataFeatureProvider) getFeatureProvider())
									.getArtifactFactory(), new ShapesFactory(
									this.getFeatureProvider())), customContext);
			complexTypeButton.setIconId(ODataImageProvider.IMG_CMPLX_TYPE);
			data.getDomainSpecificContextButtons().add(complexTypeButton);			
		} else if (getFeatureProvider()
				.getBusinessObjectForPictogramElement(pe) instanceof EnumType) {
			// Add custom button for the Add property feature
			ContextButtonEntry button = new ContextButtonEntry(
					new ODataAddEnumMemberFeature(getFeatureProvider(),
							((ODataFeatureProvider) getFeatureProvider())
									.getArtifactFactory(), new ShapesFactory(
									this.getFeatureProvider())), customContext);
			button.setIconId(ODataImageProvider.IMG_PROPERTY);
			data.getDomainSpecificContextButtons().add(button);
		}

		return data;
	}

	private void addEntityContextButtons(IPictogramElementContext context,
			PictogramElement pe, IContextButtonPadData data) {

		// Add Property Feature
		CustomContext customContext = new CustomContext(
				new PictogramElement[] { pe });

		// Add custom button for the Add property feature
		ContextButtonEntry button = new ContextButtonEntry(
				new ODataAddPropertyFeature(getFeatureProvider(),
						((ODataFeatureProvider) getFeatureProvider())
								.getArtifactFactory(), new ShapesFactory(
								this.getFeatureProvider())), customContext);
		button.setIconId(ODataImageProvider.IMG_PROPERTY);
		data.getDomainSpecificContextButtons().add(button);

		// Add custom button for the Add property feature
		ContextButtonEntry complexTypeButton = new ContextButtonEntry(
				new ODataAddComplexPropertyFeature(getFeatureProvider(),
						((ODataFeatureProvider) getFeatureProvider())
								.getArtifactFactory(), new ShapesFactory(
								this.getFeatureProvider())), customContext);
		complexTypeButton.setIconId(ODataImageProvider.IMG_CMPLX_TYPE);
		data.getDomainSpecificContextButtons().add(complexTypeButton);		
		// Add Navigation Property
		NavigationPropertyContext navPropertyContext = new NavigationPropertyContext(
				pe);
		navPropertyContext.setEditMode(true);

		// Add custom button for the Add property feature
		ContextButtonEntry navPropertyBtn = new ContextButtonEntry(
				new ODataAddNavigationPropertyFeature(getFeatureProvider(),
						((ODataFeatureProvider) getFeatureProvider())
								.getArtifactFactory(), new ShapesFactory(
								this.getFeatureProvider())), navPropertyContext);
		navPropertyBtn.setIconId(ODataImageProvider.IMG_NAV_PROPERTY);
		data.getDomainSpecificContextButtons().add(navPropertyBtn);

		CreateConnectionContext connectionContext = new CreateConnectionContext();
		connectionContext.setSourcePictogramElement(pe);
		Anchor anchor = null;
		if (pe instanceof Anchor) {
			anchor = (Anchor) pe;
		} else if (pe instanceof AnchorContainer) {
			// assume, that our shapes always have chop box anchors
			anchor = Graphiti.getPeService().getChopboxAnchor(
					(AnchorContainer) pe);
		}
		connectionContext.setSourceAnchor(anchor);

		// Association From Context Menu Pad
		ContextButtonEntry associationButton = new ContextButtonEntry(null,
				context);
		associationButton
				.setText(Messages.ODATAEDITOR_UNIDIRECTIONAL_ASSOC_MENU);
		associationButton
				.setDescription(Messages.ODATAEDITOR_UNIDIRECTIONAL_ASSOC_MENUDESC);
		associationButton.setIconId(ODataImageProvider.IMG_SIN_ASSOCIATION);
		ODataAddAssociationFeature associationFeature = new ODataAddAssociationFeature(
				getFeatureProvider(),
				((ODataFeatureProvider) getFeatureProvider())
						.getArtifactFactory(), new ShapesFactory(
						this.getFeatureProvider()));

		if (associationFeature.isAvailable(connectionContext)
				&& associationFeature.canStartConnection(connectionContext)) {
			associationButton.addDragAndDropFeature(associationFeature);
		}
		if (associationButton.getDragAndDropFeatures().size() > 0) {
			data.getDomainSpecificContextButtons().add(associationButton);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.graphiti.tb.DefaultToolBehaviorProvider#getContextMenu(org
	 * .eclipse.graphiti.features.context.ICustomContext)
	 */
	@Override
	public IContextMenuEntry[] getContextMenu(ICustomContext context) {
		ICustomContext customContext = context;
		PictogramElement[] pictogramElements = context.getPictogramElements();
		if (pictogramElements != null && pictogramElements.length > 0) {
			ODataFeatureProvider featureProvider = (ODataFeatureProvider) getFeatureProvider();
			PictogramElement pe = pictogramElements[0];
			Object businessObject = featureProvider
					.getBusinessObjectForPictogramElement(pe);
			if (this.isReadOnlyModel()) {

				return NO_CONTEXT_MENU_ENTRIES;
			}

			if (PropertyUtil.isTitle(pe)) {
				PictogramElement pes[] = new PictogramElement[pictogramElements.length];
				int i = 0;
				for (PictogramElement pictogramElement : pictogramElements) {
					pes[i++] = ODataShapeUtil.getTopContainer(pictogramElement);
				}
				((CustomContext) customContext).setPictogramElements(pes);
			}
			if (!PropertyUtil.isContextMenuEnabled(customContext)) {
				return NO_CONTEXT_MENU_ENTRIES;
			}

			if (ArtifactUtil.isFunctionImport(businessObject)) {
				return getFunctionImportMenuEntries(customContext,
						featureProvider);
			} else if (ArtifactUtil.isComplexType(businessObject)) {
				return getComplexTypeMenuEntries(customContext, featureProvider);
			} else if (ArtifactUtil.isEntityType(businessObject)) {
				if (!(ArtifactUtil.isReferencedEntityType(businessObject))) {
					return getEntityTypeMenuEntries(customContext,
							featureProvider);
				}

				// Add custom button for the Add EntitySet feature
				List<IContextMenuEntry> retList = new ArrayList<IContextMenuEntry>();

				ODataAddEntitySetFeature addEntitySetFeature = new ODataAddEntitySetFeature(
						getFeatureProvider(),
						((ODataFeatureProvider) getFeatureProvider())
								.getArtifactFactory(), new ShapesFactory(
								this.getFeatureProvider()));
				retList.add(new ContextMenuEntry(addEntitySetFeature,
						customContext));

				return retList.toArray(NO_CONTEXT_MENU_ENTRIES);

			} else if (ArtifactUtil.isEnumType(businessObject)) {
				return getEnumTypeMenuEntries(customContext, featureProvider);
			}

		}
		return super.getContextMenu(customContext);

	}

	private IContextMenuEntry[] getFunctionImportMenuEntries(
			ICustomContext context, ODataFeatureProvider featureProvider) {
		List<IContextMenuEntry> retList = new ArrayList<IContextMenuEntry>();

		// to add parameter from the context menu of Function Import
		ODataAddParameterFeature addParameterFeature = new ODataAddParameterFeature(
				featureProvider, featureProvider.getArtifactFactory(),
				featureProvider.getShapesFactory());
		retList.add(new ContextMenuEntry(addParameterFeature, context));

		ContextMenuEntry subMenu = new ContextMenuEntry(null, context);
		subMenu.setSubmenu(true);
		subMenu.setText(Messages.ODATAEDITOR_FUNCTION_IMPORT_MENU);
		retList.add(subMenu);

		// to add parameter from the context menu of Function Import
		ODataAddReturnTypeFeature addSimpleTypeFeature = new ODataAddReturnTypeFeature(
				featureProvider, featureProvider.getArtifactFactory(),
				featureProvider.getShapesFactory(),
				FunctionImportReturnType.SIMPLE);
		ContextMenuEntry simpleMenuEntry = new ContextMenuEntry(
				addSimpleTypeFeature, context);

		simpleMenuEntry.setIconId(ODataImageProvider.IMG_PROPERTY);
		simpleMenuEntry.setText(Messages.ODATAEDITOR_CONTEXTMENU_SIMPLE);
		subMenu.add(simpleMenuEntry);

		ODataAddReturnTypeFeature addComplexTypeFeature = new ODataAddReturnTypeFeature(
				featureProvider, featureProvider.getArtifactFactory(),
				featureProvider.getShapesFactory(),
				FunctionImportReturnType.COMPLEX);
		ContextMenuEntry complexMenuEntry = new ContextMenuEntry(
				addComplexTypeFeature, context);
		complexMenuEntry.setIconId(ODataImageProvider.IMG_CMPLX_TYPE);
		complexMenuEntry.setText(Messages.ODATAEDITOR_CONTEXTMENU_COMPLEXMENU);
		subMenu.add(complexMenuEntry);		
		ODataAddReturnTypeFeature addEntityTypeFeature = new ODataAddReturnTypeFeature(
				featureProvider, featureProvider.getArtifactFactory(),
				featureProvider.getShapesFactory(),
				FunctionImportReturnType.ENTITY);
		ContextMenuEntry entityMenuEntry = new ContextMenuEntry(
				addEntityTypeFeature, context);
		entityMenuEntry.setIconId(ODataImageProvider.IMG_ENTITY);
		entityMenuEntry.setText(Messages.ODATAEDITOR_CONTEXTMENU_ENTITYMENU);
		subMenu.add(entityMenuEntry);

		return retList.toArray(NO_CONTEXT_MENU_ENTRIES);
	}

	private IContextMenuEntry[] getComplexTypeMenuEntries(
			ICustomContext context, ODataFeatureProvider featureProvider) {
		List<IContextMenuEntry> retList = new ArrayList<IContextMenuEntry>();

		ContextMenuEntry subMenu = new ContextMenuEntry(null, context);
		subMenu.setSubmenu(true);
		subMenu.setText(Messages.ODATAEDITOR_PROPERTIES_MENU);
		retList.add(subMenu);

		// to add property from the context menu of Complex Type
		ODataAddPropertyFeature addPropertyFeature = new ODataAddPropertyFeature(
				featureProvider, featureProvider.getArtifactFactory(),
				featureProvider.getShapesFactory());
		ContextMenuEntry simpleMenuEntry = new ContextMenuEntry(
				addPropertyFeature, context);
		simpleMenuEntry.setIconId(ODataImageProvider.IMG_PROPERTY);
		simpleMenuEntry.setText(Messages.ODATAEDITOR_CONTEXTMENU_SIMPLE);
		subMenu.add(simpleMenuEntry);

		// to add complex property from the context menu of complex type
		ODataAddComplexPropertyFeature addComplexTypeFeature = new ODataAddComplexPropertyFeature(
				featureProvider, featureProvider.getArtifactFactory(),
				featureProvider.getShapesFactory());
		ContextMenuEntry complexMenuEntry = new ContextMenuEntry(
				addComplexTypeFeature, context);
		complexMenuEntry.setIconId(ODataImageProvider.IMG_CMPLX_TYPE);
		complexMenuEntry.setText(Messages.ODATAEDITOR_CONTEXTMENU_COMPLEXMENU);
		subMenu.add(complexMenuEntry);		
		ContextMenuEntry subMenuLink = new ContextMenuEntry(null, context);
		subMenuLink.setSubmenu(true);
		retList.add(subMenuLink);

		return retList.toArray(NO_CONTEXT_MENU_ENTRIES);
	}

	private IContextMenuEntry[] getEntityTypeMenuEntries(
			ICustomContext context, ODataFeatureProvider featureProvider) {
		List<IContextMenuEntry> retList = new ArrayList<IContextMenuEntry>();

		ContextMenuEntry subMenu = new ContextMenuEntry(null, context);
		subMenu.setSubmenu(true);
		subMenu.setText(Messages.ODATAEDITOR_PROPERTIES_MENU);
		retList.add(subMenu);

		// to add property from the context menu of Complex Type
		ODataAddPropertyFeature addPropertyFeature = new ODataAddPropertyFeature(
				featureProvider, featureProvider.getArtifactFactory(),
				featureProvider.getShapesFactory());
		ContextMenuEntry simpleMenuEntry = new ContextMenuEntry(
				addPropertyFeature, context);
		simpleMenuEntry.setIconId(ODataImageProvider.IMG_PROPERTY);
		simpleMenuEntry.setText(Messages.ODATAEDITOR_CONTEXTMENU_SIMPLE);
		subMenu.add(simpleMenuEntry);

		// to add complex property from the context menu of complex type
		ODataAddComplexPropertyFeature addComplexTypeFeature = new ODataAddComplexPropertyFeature(
				featureProvider, featureProvider.getArtifactFactory(),
				featureProvider.getShapesFactory());
		ContextMenuEntry complexMenuEntry = new ContextMenuEntry(
				addComplexTypeFeature, context);
		complexMenuEntry.setIconId(ODataImageProvider.IMG_CMPLX_TYPE);
		complexMenuEntry.setText(Messages.ODATAEDITOR_CONTEXTMENU_COMPLEXMENU);
		subMenu.add(complexMenuEntry);		
		ContextMenuEntry separatorMenuEntry = new ContextMenuEntry(null,
				context);
		separatorMenuEntry.setSubmenu(false);
		separatorMenuEntry
				.setText(Messages.ODATAEDITOR_CONTEXTMENU_SEPARATORMENU);
		subMenu.add(separatorMenuEntry);

		// to add navigation property from the context menu of complex type
		ODataAddNavigationPropertyFeature addNavPropFeature = new ODataAddNavigationPropertyFeature(
				featureProvider, featureProvider.getArtifactFactory(),
				featureProvider.getShapesFactory());
		NavigationPropertyContext navContext = new NavigationPropertyContext(
				context.getPictogramElements()[0]);
		navContext.setEditMode(true);
		ContextMenuEntry navMenuEntry = new ContextMenuEntry(addNavPropFeature,
				navContext);
		navMenuEntry.setIconId(ODataImageProvider.IMG_NAV_PROPERTY);
		navMenuEntry.setText(Messages.ODATAEDITOR_CONTEXTMENU_NAVMENU);
		subMenu.add(navMenuEntry);

		// to add Show/Hide usage from the context menu of Complex Type
		ODataAddEntitySetFeature addEntitySetFeature = new ODataAddEntitySetFeature(
				featureProvider, featureProvider.getArtifactFactory(),
				featureProvider.getShapesFactory());
		ContextMenuEntry entitySetMenuEntry = new ContextMenuEntry(
				addEntitySetFeature, context);
		entitySetMenuEntry.setIconId(ODataImageProvider.IMG_ENTITY_SET);
		entitySetMenuEntry.setSubmenu(true);
		retList.add(entitySetMenuEntry);

		return retList.toArray(NO_CONTEXT_MENU_ENTRIES);
	}

	private IContextMenuEntry[] getEnumTypeMenuEntries(ICustomContext context,
			ODataFeatureProvider featureProvider) {
		List<IContextMenuEntry> retList = new ArrayList<IContextMenuEntry>();

		// to add complex property from the context menu of complex type
		ODataAddEnumMemberFeature addEnumMemberFeature = new ODataAddEnumMemberFeature(
				getFeatureProvider(),
				((ODataFeatureProvider) getFeatureProvider())
						.getArtifactFactory(), new ShapesFactory(
						this.getFeatureProvider()));
		retList.add(new ContextMenuEntry(addEnumMemberFeature, context));

		ContextMenuEntry subMenuCType = new ContextMenuEntry(null, context);
		subMenuCType.setSubmenu(true);
		subMenuCType.setText(ODataImageProvider.IMG_PROPERTY);
		retList.add(subMenuCType);

		ContextMenuEntry subMenuLink = new ContextMenuEntry(null, context);
		subMenuLink.setSubmenu(true);
		retList.add(subMenuLink);

		return retList.toArray(NO_CONTEXT_MENU_ENTRIES);
	}

	/**
	 * This adds an error/warning image in case of errors being thrown during
	 * the artifact creation. The error icon is currently being added to the
	 * left top corner of the Entity Type
	 * 
	 * @return IDecorator[]
	 * @see PictogramElement
	 */
	@Override
	public IDecorator[] getDecorators(PictogramElement pe) {

		ODataValidationDecoratorProvider decoratorProvider = new ODataValidationDecoratorProvider(
				getFeatureProvider());

		IDecorator[] decorators = decoratorProvider.getDecorators(pe);
		if (decorators != null) {
			return decorators;
		}
		return super.getDecorators(pe);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.graphiti.tb.DefaultToolBehaviorProvider#getSelection(org.
	 * eclipse.graphiti.mm.pictograms.PictogramElement,
	 * org.eclipse.graphiti.mm.pictograms.PictogramElement[])
	 */
	@Override
	public PictogramElement getSelection(PictogramElement pe,
			PictogramElement[] oldSelection) {
		if (PropertyUtil.isInvisibleRectangle(pe)) {
			if (((ContainerShape) pe).getChildren().size() > 0) {
				return ((ContainerShape) pe).getChildren().get(0);
			}
		} else if (PropertyUtil.isEntityName(pe)
				|| (PropertyUtil.isComplexTypeName(pe) || (PropertyUtil
						.isEnumTypeName(pe)
						|| (PropertyUtil.isFunctionImportName(pe)) || (PropertyUtil
							.isReferencedEntityName(pe))))) {
			PictogramElement selectedPE = null;
			if (pe.getGraphicsAlgorithm() instanceof Image
					|| pe.getGraphicsAlgorithm() instanceof Text) {
				selectedPE = (PictogramElement) pe.eContainer().eContainer();
			} else {
				selectedPE = (PictogramElement) pe.eContainer();
			}
			return selectedPE;
		} else if (PropertyUtil.isConnectionDecorator(pe)) {
			PictogramElement selectedPE = null;
			if (pe.getGraphicsAlgorithm() instanceof Text) {
				selectedPE = ((ConnectionDecorator) pe).getConnection();
				return selectedPE;
			}

		}
		return super.getSelection(pe, oldSelection);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.graphiti.tb.DefaultToolBehaviorProvider#getDoubleClickFeature
	 * (org.eclipse.graphiti.features.context.IDoubleClickContext)
	 */
	@Override
	public ICustomFeature getDoubleClickFeature(IDoubleClickContext context) {

		if (this.isReadOnlyModel()) {
			return null;
		}

		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			PictogramElement pe = pes[0];

			if (PropertyUtil.isTitle(pe)) {
				return new ODataCollapsibleFeature(getFeatureProvider());
			} else if (PropertyUtil.isTopContainer(pe)
					&& pe instanceof ContainerShape) {

				return new ODataDoubleClickFeature(getFeatureProvider());
			}
		}
		return super.getDoubleClickFeature(context);
	}

	/**
	 * This method is overridden in order to enable F2 on Identifier shape.
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.graphiti.tb.DefaultToolBehaviorProvider#getLocationInfo(org
	 * .eclipse.graphiti.mm.pictograms.PictogramElement,
	 * org.eclipse.graphiti.util.ILocationInfo)
	 */
	@Override
	public ILocationInfo getLocationInfo(PictogramElement pe,
			ILocationInfo locationInfo) {
		PictogramElement[] selectedPictogramElements = getDiagramTypeProvider()
				.getDiagramBehavior().getDiagramContainer()
				.getSelectedPictogramElements();
		if (selectedPictogramElements != null
				&& selectedPictogramElements.length > 0) {
			// Use the first selected pictogram element for direct editing
			PictogramElement pictogramElement = selectedPictogramElements[0];
			if (pictogramElement instanceof ContainerShape
					&& PropertyUtil.isTypeIdentifier(pe)
					&& !PropertyUtil.isTitle(pe)) {
				ContainerShape shape = (ContainerShape) pictogramElement;
				Shape classNameShape = shape.getChildren().get(1);
				GraphicsAlgorithm nameGA = classNameShape
						.getGraphicsAlgorithm();
				if (nameGA instanceof Text) {
					return new LocationInfo(classNameShape, nameGA);

				}
			}
		}
		return super.getLocationInfo(pe, locationInfo);
	}

	@Override
	public boolean isShowFlyoutPalette() {

		if (this.isReadOnlyModel()) {
			return false;
		}

		return super.isShowFlyoutPalette();
	}

	/**
	 * This method is overridden in order to customize Palette Compartment
	 * names.
	 */
	@Override
	public IPaletteCompartmentEntry[] getPalette() {
		// If the model is opened from the service catalog
		// the pallette need not be shown from the menu also
		if (isReadOnlyModel()) {
			return new IPaletteCompartmentEntry[] {};
		}

		ConnectionCreationToolEntry ccTool;
		ObjectCreationToolEntry objectCreationToolEntry;
		final List<IPaletteCompartmentEntry> compartments = new ArrayList<IPaletteCompartmentEntry>();

		// Palette Compartment Entry for Associations.
		PaletteCompartmentEntry compartmentEntry = new PaletteCompartmentEntry(
				Messages.ODATAEDITOR_TOOLBEHAVIORPROVIDER_ASSOC, null);
		compartments.add(compartmentEntry);

		final IFeatureProvider featureProvider = getFeatureProvider();
		final ICreateConnectionFeature[] createConnectionFeatures = featureProvider
				.getCreateConnectionFeatures();
		if (createConnectionFeatures.length > 0) {
			for (ICreateConnectionFeature createConnectionFeature : createConnectionFeatures) {
				ccTool = new ConnectionCreationToolEntry(
						createConnectionFeature.getCreateName(),
						createConnectionFeature.getCreateDescription(),
						createConnectionFeature.getCreateImageId(),
						createConnectionFeature.getCreateLargeImageId());
				ccTool.addCreateConnectionFeature(createConnectionFeature);
				compartmentEntry.addToolEntry(ccTool);
			}
		}
		// Palette Compartment Entry for Objects.
		compartmentEntry = new PaletteCompartmentEntry(
				Messages.ODATAEDITOR_TOOLBEHAVIORPROVIDER_OBJ, null);
		compartments.add(compartmentEntry);

		final ICreateFeature[] createFeatures = featureProvider
				.getCreateFeatures();

		for (ICreateFeature createFeature : createFeatures) {
			if (createFeature.getName().equalsIgnoreCase(
					Messages.ODATAEDITOR_REFERENCED_ENTITYTYPE_NAME)) {
				if (hasReferencedSchemas()) {
					objectCreationToolEntry = new ObjectCreationToolEntry(
							createFeature.getCreateName(),
							createFeature.getCreateDescription(),
							createFeature.getCreateImageId(),
							createFeature.getCreateLargeImageId(),
							createFeature);
					compartmentEntry.addToolEntry(objectCreationToolEntry);

				}
			} else {
				objectCreationToolEntry = new ObjectCreationToolEntry(
						createFeature.getCreateName(),
						createFeature.getCreateDescription(),
						createFeature.getCreateImageId(),
						createFeature.getCreateLargeImageId(), createFeature);
				compartmentEntry.addToolEntry(objectCreationToolEntry);

			}
		}

		final IPaletteCompartmentEntry[] res = compartments
				.toArray(new IPaletteCompartmentEntry[compartments.size()]);

		return res;
	}

	private boolean hasReferencedSchemas() {
		Schema currentSchema = ((ODataFeatureProvider) getFeatureProvider())
				.getArtifactFactory().getSchema();
		if (currentSchema == null) {
			return false;
		}
		return (ArtifactUtil.getReferencedSchemas(currentSchema) != null && (ArtifactUtil
				.getReferencedEntities(currentSchema).size() > 0));
	}

	/**
	 * @param pe
	 * @param point
	 * @return whether the point exists in the shape area.
	 */
	public boolean isLocationExists(PictogramElement pe, Point point) {
		boolean isLocationExists = true;
		ILocation location = getAbsoluteLocation(pe);

		int xlo = location.getX();
		int ylo = location.getY();
		int width = pe.getGraphicsAlgorithm().getWidth();
		int height = pe.getGraphicsAlgorithm().getWidth();

		int xhi = xlo + width;
		int yhi = ylo + height;

		int x = point.x;
		int y = point.y;

		if (x < xlo || x > xhi || y < ylo || y > yhi) {

			isLocationExists = false;
		}
		return isLocationExists;
	}

	/*
	 * The following method checks if the current model is a read-only model.
	 */
	private boolean isReadOnlyModel() {

		boolean isReadOnlyModel = false;

		final IDiagramContainer diagramEditor = this.getDiagramTypeProvider()
				.getDiagramBehavior().getDiagramContainer();

		if (diagramEditor instanceof ODataEditor) {
			if (((ODataEditor) diagramEditor).isReadOnly()) {
				isReadOnlyModel = true;
			}
		}

		return isReadOnlyModel;
	}
}
