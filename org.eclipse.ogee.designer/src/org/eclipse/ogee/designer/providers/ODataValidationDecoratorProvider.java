/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.providers;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IPeService;
import org.eclipse.graphiti.tb.IDecorator;
import org.eclipse.graphiti.tb.ImageDecorator;
import org.eclipse.ogee.designer.Activator;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.designer.utils.IODataEditorConstants;
import org.eclipse.ogee.designer.utils.PropertyUtil;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.utils.logger.Logger;

/**
 * The Class ODataValidationDecoratorProvider provides functionality to render error
 * markers on the pictogram elements.
 * 
 */
public class ODataValidationDecoratorProvider {

	/*
	 * The interface IPeService provides convenient services for the creation
	 * and layout of pictogram elements.
	 */
	protected IPeService peService;

	/*
	 * The Interface IFeatureProvider provides the set of features which defines
	 * the operations, potentially available in a graphical editor.
	 */
	protected final IFeatureProvider fp;

	/**
	 * Constructor for ODataValidationDecoratorProvider.
	 * 
	 * @param fp - IFeatureProvider
	 */
	public ODataValidationDecoratorProvider(final IFeatureProvider fp) {

		this.fp = fp;
		this.peService = Graphiti.getPeService();
	}

	/**
	 * The following method provides the functionality to render error markers
	 * on the pictogram elements.
	 * 
	 * @param pe - PictogramElement
	 * @return IDecorator[] - error decorators
	 */
	public IDecorator[] getDecorators(final PictogramElement pe) {

		EObject bo = null;
		int markerPriority = 0;
		ImageDecorator imageRenderingDecorator = null;
		String markerSourceId = ""; //$NON-NLS-1$
		IDecorator[] errorDecorators = null;

		if (pe instanceof ConnectionDecorator) {
			final EObject eContainer = ((ConnectionDecorator) pe).eContainer();
			if (eContainer instanceof FreeFormConnection) {
				final FreeFormConnection ffc = (FreeFormConnection) eContainer;
				if (ffc != null && ffc.getLink() != null && ffc.getLink().getBusinessObjects() != null) {
					bo = ffc.getLink().getBusinessObjects().get(0);
				}
			}
		}

		if (bo == null) {
			bo = (EObject) this.fp.getBusinessObjectForPictogramElement(pe);
		}
		if (bo == null || bo.eIsProxy() || bo.eResource() == null) {
			errorDecorators = new IDecorator[] { };
		}
		else {
			// find all error markers for the current resource.
			final IMarker[] markers = this.findMarkers(bo);
			try {
				if (!(markers.length == 0)) {
					for (int i = 0; i < markers.length; i++) {
						markerSourceId = (String) markers[i]
								.getAttribute(IMarker.SOURCE_ID);
						if (this.isValidMarker(bo, markerSourceId, pe)) {
							markerPriority = ((Integer) (markers[i]
									.getAttribute(IMarker.PRIORITY))).intValue();
							if (markerPriority == IMarker.PRIORITY_HIGH) {
								imageRenderingDecorator = new ImageDecorator(
										ODataImageProvider.IMG_ERROR_CO);
								
							}
							if (markerPriority == IMarker.PRIORITY_NORMAL) {
								imageRenderingDecorator = new ImageDecorator(
										ODataImageProvider.IMG_WARNING_CO);
							}
							if (imageRenderingDecorator != null) {
								imageRenderingDecorator.setX(8);
								imageRenderingDecorator.setY(8);
								imageRenderingDecorator
										.setMessage((String) markers[i]
										.getAttribute(IMarker.MESSAGE));
							}
							errorDecorators = new IDecorator[] { imageRenderingDecorator };
						}
					}
				}
			} catch (CoreException e) {
				Logger.getLogger(Activator.PLUGIN_ID).logError(e);
				e.printStackTrace();
			}
		}
		
		return errorDecorators;
	}

	/*
	 * Validates if the marker object corresponds to the PictogramElement.
	 */
	private boolean isValidMarker(final EObject pictogramObj,
			final String markerSourceId, final PictogramElement pe) {

		boolean isValidMarker = false;

		final URI uri = URI.createURI(markerSourceId);
		final EObject markerObj = pictogramObj.eResource().getResourceSet()
				.getEObject(uri, true);
		// get the selectable domain object for the current marker
		final EObject selectableMarkerObj = ArtifactUtil
				.getSelectableObject(markerObj);
		// For domain object Association do not check for isTypeIdentifier.
		if (selectableMarkerObj instanceof Association) {
			isValidMarker = pictogramObj.equals(selectableMarkerObj)
					&& !(PropertyUtil.isTitle(pe));
		} else {
			isValidMarker = pictogramObj.equals(selectableMarkerObj)
					&& PropertyUtil.isTypeIdentifier(pe)
					&& !(PropertyUtil.isTitle(pe));
		}

		return isValidMarker;
	}

	/*
	 * The following method retrieves all error markers for the current
	 * resource.
	 */
	private IMarker[] findMarkers(final EObject o) {

		IMarker[] markers = null;

		final IResource resource = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(o.eResource().getURI().toPlatformString(true));
		try {
			markers = resource.findMarkers(
					IODataEditorConstants.PROBLEM_MARKER_ID, true,
					IResource.DEPTH_INFINITE);
		} catch (CoreException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
			e.printStackTrace();
		}

		return markers;
	}

}
