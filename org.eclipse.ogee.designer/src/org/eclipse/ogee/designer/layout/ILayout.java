/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.layout;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * This interface is responsible to define the layout behavior. This is used in
 * defining layout extension point.
 * 
 */
public interface ILayout {

	/**
	 * This method is responsible to check whether layout can be allowed or not.
	 * 
	 * @param pes
	 *            PictogramElements.
	 * 
	 * @return boolean canLayout.
	 */
	public boolean canLayout(PictogramElement... pes);

	/**
	 * This method is defines Layout functionality.
	 * 
	 * @param fp
	 *            FeatureProvider Instance.
	 * @param pes
	 *            PictogramElements.
	 * 
	 * @throws Exception
	 *             Any exception raised by custom layout implementation.
	 */
	public void layout(IFeatureProvider fp, PictogramElement... pes)
			throws Exception;

}
