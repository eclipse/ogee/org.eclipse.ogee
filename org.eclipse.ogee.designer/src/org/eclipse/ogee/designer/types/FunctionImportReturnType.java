/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.types;

import org.eclipse.ogee.designer.messages.Messages;
import org.eclipse.ogee.designer.providers.ODataImageProvider;

/**
 * ENUM for the different types Return Types of FunctionImport
 * 
 */
public enum FunctionImportReturnType {

	SIMPLE(0, Messages.ODATAEDITOR_FUNCTION_IMPORT_SIMPLEMENU,
			Messages.ODATAEDITOR_FUNCTION_IMPORT_SIMPLEMENU_DESC,
			ODataImageProvider.IMG_FUNC_IMP), 
	COMPLEX(1, Messages.ODATAEDITOR_FUNCTION_IMPORT_COMPLEXMENU,
			Messages.ODATAEDITOR_FUNCTION_IMPORT_COMPLEXMENU_DESC,
			ODataImageProvider.IMG_FUNC_IMP), 
	ENTITY(2, Messages.ODATAEDITOR_FUNCTION_IMPORT_ENTITYMENU,
			Messages.ODATAEDITOR_FUNCTION_IMPORT_ENTITYMENU_DESC,
			ODataImageProvider.IMG_FUNC_IMP),
	ENUM(2, Messages.ODATAEDITOR_FUNCTION_IMPORT_ENUMMENU,
			Messages.ODATAEDITOR_FUNCTION_IMPORT_ENUMMENU_DESC,
			ODataImageProvider.IMG_FUNC_IMP);

	private int id;
	private String displayName;
	private String description;
	private String imageID;

	private FunctionImportReturnType(int id, String displayName,
			String description, String imageID) {
		this.id = id;
		this.displayName = displayName;
		this.imageID = imageID;
		this.description = description;
	}

	public int getId() {
		return id;

	}

	public String getDisplayName() {
		return displayName;

	}

	public String getDescription() {
		return description;

	}

	public String getImageID() {
		return imageID;

	}
}
