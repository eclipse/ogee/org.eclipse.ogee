/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.types;

/**
 * ENUM for the different types of columns for layouting
 * 
 */
public enum ColumnType {

	/**
	 * Entity Type
	 */
	ENTITYTYPE(1),

	/**
	 * ComplexType
	 */
	COMPLEXTYPE(2),

	/**
	 * Function Import
	 */
	FUNCTIONIMPORT(3),

	/**
	 * EnumType
	 */
	ENUMTYPE(4),

	/**
	 * None
	 */
	NONE(0);

	private int id;

	/**
	 * @return id
	 */
	public int getId() {
		return this.id;
	}

	private ColumnType(int id) {
		this.id = id;

	}

}
