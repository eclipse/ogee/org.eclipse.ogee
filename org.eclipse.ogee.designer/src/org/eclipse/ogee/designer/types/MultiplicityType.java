/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.designer.types;

/**
 * ENUM for the different types Multiplicity
 */
public enum MultiplicityType {

	ONE,

	MANY,

	ZEROTOONE;

	public static final String ZEROTOONE_VALUE = "0..1"; //$NON-NLS-1$
	public static final String ONE_VALUE = "1"; //$NON-NLS-1$
	public static final String MANY_VALUE = "*"; //$NON-NLS-1$
}
