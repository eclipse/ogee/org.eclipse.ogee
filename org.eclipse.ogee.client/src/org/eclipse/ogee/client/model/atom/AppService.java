/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.model.generic.ServiceDocument;

/**
 * The root of a service document is the "app:service" element. A service
 * document enumerates the IRIs of a group of Collections and the capabilities
 * of those Collections supported by the server. It groups available Collections
 * into Workspaces. The "app:service" element is the container for service
 * information associated with one or more Workspaces.
 * 
 * The content of the service document can vary based on aspects of the client
 * request, including, but not limited to, authentication credentials.
 * 
 * 
 */
public class AppService extends BaseElement implements ServiceDocument {
	private String xmlBase;
	private Hashtable<String, String> namespaces;

	// Zero or more sub-elements atom:link pointing to related documents
	private AtomLink[] links;
	// One or more sub-elements app:workspace which describe groups of
	// Collections
	private AppWorkspace[] appWorkspaces;

	/**
	 * Default constructor
	 */
	public AppService() {
		super(new Hashtable<String, String>());
		appWorkspaces = new AppWorkspace[0];
		links = new AtomLink[0];
		namespaces = new Hashtable<String, String>();
	}

	/**
	 * @return the xmlBase
	 */
	public String getXmlBase() {
		return xmlBase;
	}

	/**
	 * @param xmlBase
	 *            the xmlBase to set
	 */
	public void setXmlBase(String xmlBase) {
		this.xmlBase = xmlBase;
		this.setAttribute(XML_BASE, xmlBase);
	}

	/**
	 * @return the namespaces
	 */
	public Hashtable<String, String> getNamespaces() {
		return namespaces;
	}

	/**
	 * @param namespaces
	 *            the namespaces to set
	 */
	public void setNamespaces(Hashtable<String, String> namespaces) {
		this.namespaces = namespaces;
		Enumeration<String> keys = namespaces.keys();// $JL-COLLECTION$
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			this.setAttribute(key, namespaces.get(key).toString());
		}
	}

	/**
	 * @return the links
	 */
	public AtomLink[] getLinks() {
		if (this.links != null) {
			return Arrays.copyOf(this.links, this.links.length);

		}
		return null;
	}

	/**
	 * @param links
	 *            the links to set
	 */
	public void setLinks(AtomLink[] links) {
		this.links = links;
	}

	/**
	 * @return the appWorkspaces
	 */
	public AppWorkspace[] getAppWorkspaces() {
		if (this.appWorkspaces != null) {
			return Arrays.copyOf(this.appWorkspaces, this.appWorkspaces.length);
		}
		return null;

	}

	/**
	 * @param appWorkspaces
	 *            the appWorkspaces to set
	 */
	public void setAppWorkspaces(AppWorkspace[] appWorkspaces) {
		this.appWorkspaces = appWorkspaces;
	}

	@Override
	public Object[] getCollections() {
		return getAppWorkspaces()[0].getCollections();
	}

	@Override
	public void setCollections(Object[] collections) {
		getAppWorkspaces()[0].setCollections((AppCollection[]) collections);

	}
}
