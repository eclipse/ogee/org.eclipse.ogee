/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.model.atom;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The "atom:category" element conveys information about a category associated
 * with an entry or feed.
 * 
 * 
 */
public class AtomCategory extends BaseElement {

	public AtomCategory() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @return the term
	 */
	public String getTerm() {
		return getAttribute(TERM);
	}

	/**
	 * @param term
	 *            the term to set
	 */
	public void setTerm(String term) {
		getAttributes().put(TERM, term);
	}

	/**
	 * @return the scheme
	 */
	public String getScheme() {
		return getAttribute(SCHEME);
	}

	/**
	 * @param scheme
	 *            the scheme to set
	 */
	public void setScheme(String scheme) {
		getAttributes().put(SCHEME, scheme);
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return getAttribute("label"); //$NON-NLS-1$
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		getAttributes().put("label", label); //$NON-NLS-1$
	}

}
