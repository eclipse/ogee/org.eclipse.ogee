/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The "atom:summary" element is a Text construct that conveys a short summary,
 * abstract, or excerpt of an entry.
 * 
 * 
 */
public class AtomSummary extends BaseElement {
	private String value;

	/**
	 * Default constructor
	 */
	public AtomSummary() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @return the contents
	 */
	public String getContents() {
		return value;
	}

	/**
	 * @param contents
	 *            the contents to set
	 */
	public void setContents(String contents) {
		this.value = contents;
	}

}
