/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.model.generic.IMediaResource;
import org.eclipse.ogee.client.model.generic.ODataEntry;
import org.eclipse.ogee.client.model.generic.ODataLink;
import org.eclipse.ogee.client.model.generic.ODataProperty;

/**
 * The "atom:entry" element represents an individual entry, acting as a
 * container for metadata and data associated with the entry. This element can
 * appear as a child of the atom:feed element, or it can appear as the document
 * (i.e., top-level) element of a stand-alone Atom Entry Document.
 * 
 * 
 */
public class AtomEntry extends BaseElement implements ODataEntry {
	private AtomContent content;
	private String atomId;
	private String atomTitle;
	private String atomUpdated;
	private AtomLink[] links;
	private AtomSummary atomSummary;
	private String xmlBase;
	private Hashtable<String, String> namespaces;
	private AtomAuthor atomAuthor;
	private AtomCategory atomCategory;
	private Mproperties mProperties;
	private IMediaResource mediaResource = null;

	/**
	 * 
	 * @param author
	 *            a Person construct that indicates the author of the entry.
	 * @param id
	 *            conveys a permanent, universally unique identifier for an
	 *            entry.
	 * @param title
	 *            conveys a human-readable title for an entry.
	 * @param updated
	 *            a Date construct indicating the most recent instant in time
	 *            when an entry was modified in a way the publisher considers
	 *            significant.
	 */
	public AtomEntry(AtomAuthor author, String id, String title, String updated) {
		super(new Hashtable<String, String>());
		links = new AtomLink[0];
		content = new AtomContent();
		mProperties = new Mproperties();
		atomSummary = new AtomSummary();
		namespaces = new Hashtable<String, String>();
		setAtomAuthor(author);
		setAtomId(id);
		setAtomTitle(title);
		setAtomUpdated(updated);
	}

	/**
	 * @return the atomCategory
	 */
	public AtomCategory getAtomCategory() {
		return atomCategory;
	}

	/**
	 * @param atomCategory
	 *            the atomCategory to set
	 */
	public void setAtomCategory(AtomCategory atomCategory) {
		this.atomCategory = atomCategory;
	}

	/**
	 * @return the mProperties
	 */
	public Mproperties getmProperties() {
		return mProperties;
	}

	/**
	 * @param mProperties
	 *            the mProperties to set
	 */
	public void setmProperties(Mproperties mProperties) {
		this.mProperties = mProperties;
	}

	/**
	 * @param mediaResource
	 *            the mediaResource to set
	 */
	public void setMediaResource(IMediaResource mediaResource) {
		this.mediaResource = mediaResource;
	}

	/**
	 * @return the atomSummary
	 */
	public AtomSummary getAtomSummary() {
		return atomSummary;
	}

	/**
	 * @param atomSummary
	 *            the atomSummary to set
	 */
	public void setAtomSummary(AtomSummary atomSummary) {
		this.atomSummary = atomSummary;
	}

	/**
	 * @return the content
	 */
	public AtomContent getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(AtomContent content) {
		this.content = content;
	}

	/**
	 * @return the atomId
	 */
	public String getAtomId() {
		return atomId;
	}

	/**
	 * @param atomId
	 *            the atomId to set
	 */
	public void setAtomId(String atomId) {
		if (null != atomId) {
			if (atomId.contains("\n") || atomId.contains("\r")) //$NON-NLS-1$ //$NON-NLS-2$
			{
				atomId = atomId.replace("\r", ""); //$NON-NLS-1$ //$NON-NLS-2$
				atomId = atomId.replace("\n", ""); //$NON-NLS-1$ //$NON-NLS-2$
			}
			this.atomId = atomId.trim();
		}
		this.atomId = atomId;
	}

	/**
	 * @return the atomTitle
	 */
	public String getAtomTitle() {
		return atomTitle;
	}

	/**
	 * @param atomTitle
	 *            the atomTitle to set
	 */
	public void setAtomTitle(String atomTitle) {
		this.atomTitle = atomTitle;
	}

	/**
	 * @return the atomUpdated
	 */
	public String getAtomUpdated() {
		return atomUpdated;
	}

	/**
	 * @param atomUpdated
	 *            the atomUpdated to set
	 */
	public void setAtomUpdated(String atomUpdated) {
		this.atomUpdated = atomUpdated;
	}

	/**
	 * @return the links
	 */
	public ODataLink[] getLinks() {
		if (this.links != null) {
			return Arrays.copyOf(this.links, this.links.length);
		}
		return null;

	}

	/**
	 * @param links
	 *            the links to set
	 */
	public void setLinks(AtomLink[] links) {
		this.links = links;
	}

	/**
	 * @param xmlBase
	 *            the xmlBase to set
	 */
	public void setXmlBase(String xmlBase) {
		this.xmlBase = xmlBase;
		this.setAttribute(XML_BASE, xmlBase);
	}

	/**
	 * @return the xmlBase
	 */
	public String getXmlBase() {
		return xmlBase;
	}

	/**
	 * @param namespaces
	 *            the namespaces to set
	 */
	public void setNamespaces(Hashtable<String, String> namespaces) {
		this.namespaces = namespaces;
		Enumeration<String> keys = namespaces.keys();// $JL-COLLECTION$
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			this.setAttribute(key, namespaces.get(key).toString());
		}
	}

	/**
	 * @return the namespaces
	 */
	public Hashtable<String, String> getNamespaces() {
		return namespaces;
	}

	/**
	 * @param atomAuthor
	 *            the atomAuthor to set
	 */
	public void setAtomAuthor(AtomAuthor atomAuthor) {
		this.atomAuthor = atomAuthor;
	}

	/**
	 * @return the atomAuthor
	 */
	public AtomAuthor getAtomAuthor() {
		return atomAuthor;
	}

	/**
	 * @return the properties
	 */
	public Mproperties getMproperties() {
		return this.mProperties;
	}

	/**
	 * @param mProperties
	 *            the properties to set
	 */
	public void setMproperties(Mproperties mProperties) {
		this.mProperties = mProperties;
	}

	@Override
	public ODataProperty[] getProperties() {
		if (isMediaLinkEntry()) {
			return getMproperties().getDataValues();
		}
		return getContent().getmProperties().getDataValues();
	}

	@Override
	public boolean isMediaLinkEntry() {
		return getContent().getSrc() != null;

	}

	@Override
	public IMediaResource getMediaResource() {
		if (isMediaLinkEntry() && mediaResource == null) {
			mediaResource = new IMediaResource() {
				@Override
				public String getURI() {
					return getContent().getSrc();
				}

				@Override
				public String getMIMEType() {
					return getContent().getType();
				}

			};
		}
		return mediaResource;

	}

	@Override
	public ODataProperty getProperty(String key) {
		if (isMediaLinkEntry()) {
			return getMproperties().getDataValue(key);
		}
		return getContent().getmProperties().getDataValue(key);
	}

	@Override
	public void putProperty(ODataProperty property) {
		if (isMediaLinkEntry()) {
			getMproperties().putDataValue((ODataPropertyImpl) property);
		} else {
			getContent().getmProperties().putDataValue(
					(ODataPropertyImpl) property);
		}
	}

	@Override
	public String getId() {
		String id = getAtomId();
		if (id != null) {
			return id.trim();
		}
		return id;
	}

	@Override
	public void putProperty(String key, String value) {
		if (isMediaLinkEntry()) {
			getMproperties().putStringDataValue(key, value);
		} else {
			getContent().getmProperties().putStringDataValue(key, value);
		}

	}

	@Override
	public String getType() {
		if (this.atomCategory != null) {
			return this.atomCategory.getAttribute("term"); //$NON-NLS-1$
		}

		return null;
	}
}
