/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.model.generic.ODataCollection;
import org.eclipse.ogee.client.model.generic.ODataEntry;

/**
 * The "atom:feed" element is the document (i.e., top-level) element of an Atom
 * Feed Document, acting as a container for metadata and data associated with
 * the feed. Its element children consist of metadata elements followed by zero
 * or more atom:entry child elements.
 * 
 * 
 */
public class AtomFeed extends BaseElement implements ODataCollection {
	private String xmlBase;
	private Hashtable<String, String> namespaces;
	private String atomId;
	private AtomLink[] links;
	private String atomTitle;
	private String atomUpdated;
	private AtomEntry[] entries;
	private AtomAuthor atomAuthor;

	/**
	 * 
	 * @param author
	 *            a Person construct that indicates the author of the feed.
	 * @param id
	 *            conveys a permanent, universally unique identifier for a feed.
	 * @param title
	 *            conveys a human-readable title for a feed.
	 * @param updated
	 *            a Date construct indicating the most recent instant in time
	 *            when a feed was modified in a way the publisher considers
	 *            significant.
	 */
	public AtomFeed(AtomAuthor author, String id, String title, String updated) {
		super(new Hashtable<String, String>());
		entries = new AtomEntry[0];
		links = new AtomLink[0];
		namespaces = new Hashtable<String, String>();
		setAtomAuthor(author);
		setAtomId(id);
		setAtomTitle(title);
		setAtomUpdated(updated);
	}

	/**
	 * @return the xmlBase
	 */
	public String getXmlBase() {
		return xmlBase;
	}

	/**
	 * @param xmlBase
	 *            the xmlBase to set
	 */
	public void setXmlBase(String xmlBase) {
		this.xmlBase = xmlBase;
		this.setAttribute(XML_BASE, xmlBase);
	}

	/**
	 * @return the namespaces
	 */
	public Hashtable<String, String> getNamespaces() {
		return namespaces;
	}

	/**
	 * @param namespaces
	 *            the namespaces to set
	 */
	public void setNamespaces(Hashtable<String, String> namespaces) {
		this.namespaces = namespaces;
		Enumeration<String> keys = namespaces.keys();// $JL-COLLECTION$
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			this.setAttribute(key, namespaces.get(key).toString());
		}
	}

	/**
	 * @return the atomId
	 */
	public String getAtomId() {
		return atomId;
	}

	/**
	 * @param atomId
	 *            the atomId to set
	 */
	public void setAtomId(String atomId) {
		this.atomId = atomId;
	}

	/**
	 * @return the atomTitle
	 */
	public String getAtomTitle() {
		return atomTitle;
	}

	/**
	 * @param atomTitle
	 *            the atomTitle to set
	 */
	public void setAtomTitle(String atomTitle) {
		this.atomTitle = atomTitle;
	}

	/**
	 * @return the atomUpdated
	 */
	public String getAtomUpdated() {
		return atomUpdated;
	}

	/**
	 * @param atomUpdated
	 *            the atomUpdated to set
	 */
	public void setAtomUpdated(String atomUpdated) {
		this.atomUpdated = atomUpdated;
	}

	/**
	 * @return the links
	 */
	public AtomLink[] getLinks() {
		if (this.links != null) {
			return Arrays.copyOf(this.links, this.links.length);
		}
		return null;

	}

	/**
	 * @param links
	 *            the links to set
	 */
	public void setLinks(AtomLink[] links) {
		this.links = links;
	}

	/**
	 * @return the entries
	 */
	public ODataEntry[] getEntries() {
		if (this.entries != null) {
			return Arrays.copyOf(this.entries, this.entries.length);
		}
		return null;

	}

	/**
	 * @param entries
	 *            the entries to set
	 */
	public void setEntries(AtomEntry[] entries) {
		this.entries = entries;
	}

	/**
	 * @param atomAuthor
	 *            the atomAuthor to set
	 */
	public void setAtomAuthor(AtomAuthor atomAuthor) {
		this.atomAuthor = atomAuthor;
	}

	/**
	 * @return the atomAuthor
	 */
	public AtomAuthor getAtomAuthor() {
		return atomAuthor;
	}

}
