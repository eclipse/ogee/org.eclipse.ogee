/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The "app:collection" element describes a Collection. Member resources are
 * grouped into collection resources. These resources are represented via
 * AtomSyn, with members being represented as entries, and collections
 * represented as feeds.
 * 
 */
public class AppCollection extends BaseElement {
	// the href attribute linking to the collection resource is relative to the
	// xml:base stated in the service element, which is compliant with the
	// AtomPub specification.
	private String href;
	// a human-readable title for the Collection.
	private String atomTitle;
	private AtomLink[] links;

	/**
	 * Default constructor
	 */
	public AppCollection() {
		super(new Hashtable<String, String>());
		links = new AtomLink[0];
	}

	/**
	 * @return the href the href attribute links to the collection resource and
	 *         is relative to the xml:base stated in the service element, which
	 *         is compliant with the AtomPub specification.
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param href
	 *            the href to set
	 */
	public void setHref(String href) {
		this.href = href;
		this.setAttribute(HREF, href);
	}

	/**
	 * @return the atomTitle
	 */
	public String getAtomTitle() {
		return atomTitle;
	}

	/**
	 * @param atomTitle
	 *            the atomTitle to set
	 */
	public void setAtomTitle(String atomTitle) {
		this.atomTitle = atomTitle;
	}

	/**
	 * @return the links
	 */
	public AtomLink[] getLinks() {
		if (this.links != null) {
			return Arrays.copyOf(this.links, this.links.length);

		}
		return null;
	}

	/**
	 * @param links
	 *            the links to set
	 */
	public void setLinks(AtomLink[] links) {
		this.links = links;
	}

}
