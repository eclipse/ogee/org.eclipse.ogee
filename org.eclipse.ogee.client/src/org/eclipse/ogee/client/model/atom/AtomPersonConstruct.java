/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

/**
 * A Person construct is an element that describes a person, corporation, or
 * similar entity (hereafter, 'person').
 * 
 * 
 */
public interface AtomPersonConstruct {
	// a human-readable name for the person.
	public String getAtomName();

	// an IRI associated with the person.
	public String getAtomUri();

	// an e-mail address associated with the person.
	public String getAtomEmail();

}
