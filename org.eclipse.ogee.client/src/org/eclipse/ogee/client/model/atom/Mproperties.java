/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The "m:properties" element contains properties of an entry.
 * 
 * 
 */
public class Mproperties extends BaseElement {
	private Hashtable<String, ODataPropertyImpl> dataValues;

	public Mproperties() {
		super(new Hashtable<String, String>());
		dataValues = new Hashtable<String, ODataPropertyImpl>();
	}

	/**
	 * @return ODataPropertyImpl by name
	 */
	public ODataPropertyImpl getDataValue(String name) {
		if (!name.startsWith(D)) {
			name = D + name;
		}
		return (ODataPropertyImpl) dataValues.get(name);
	}

	/**
	 * @param dataValue
	 *            the child dataValue
	 */
	public void putDataValue(ODataPropertyImpl dataValue) {
		this.dataValues.put(dataValue.getQualifiedName(), dataValue);
	}

	/**
	 * @return the mProperties
	 */
	public ODataPropertyImpl[] getDataValues() {
		ODataPropertyImpl[] resultDataValues = new ODataPropertyImpl[dataValues
				.size()];
		Enumeration<ODataPropertyImpl> enumeration = dataValues.elements();// $JL-COLLECTION$
		int i = 0;
		while (enumeration.hasMoreElements())
			resultDataValues[i++] = enumeration.nextElement();
		return resultDataValues;
	}

	public void putStringDataValue(String name, String value) {
		ODataPropertyImpl dataValue = new ODataPropertyImpl();
		dataValue.setName(name);
		dataValue.setValue(value);
		this.putDataValue(dataValue);
	}

	public String getStringDataValue(String name) {
		if (!name.startsWith(D)) {
			name = D + name;
		}
		ODataPropertyImpl dataValue = (ODataPropertyImpl) dataValues.get(name);
		if (dataValue != null)
			return dataValue.getValue();
		return null;
	}
}
