/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.model.generic.ODataCollection;
import org.eclipse.ogee.client.model.generic.ODataProperty;

public class ODataPropertyImpl extends BaseElement implements ODataProperty {
	private Hashtable<String, ODataPropertyImpl> childDataValues;
	private String name;
	private String value;
	private String type;

	public ODataPropertyImpl() {
		super(new Hashtable<String, String>());
		childDataValues = new Hashtable<String, ODataPropertyImpl>();
	}

	public String getName() {
		if (name == null) {
			return null;
		}

		int attrBeginIndex = name.indexOf(COLON_CHAR) + 1;
		return name.substring(attrBeginIndex);
	}

	public String getQualifiedName() {
		return name;
	}

	public void setName(String name) {
		if (!name.startsWith(D)) {
			name = D + name;
		}

		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
		this.setAttribute(M_TYPE, type);
	}

	/**
	 * @return ODataPropertyImpl by name
	 */
	public ODataPropertyImpl getChildDataValue(String name) {
		if (!name.startsWith(D)) {
			name = D + name;
		}
		return this.childDataValues.get(name);
	}

	/**
	 * @param property
	 *            the child dataValue
	 */
	public void putChildDataValue(ODataPropertyImpl property) {
		if (property != null)
			this.childDataValues.put(property.getQualifiedName(),
					(ODataPropertyImpl) property);
	}

	public ODataPropertyImpl[] getChildDataValues() {
		ODataPropertyImpl[] resultDataValues = new ODataPropertyImpl[childDataValues
				.size()];
		Enumeration<ODataPropertyImpl> enumeration = childDataValues.elements();// $JL-COLLECTION$
		int i = 0;
		while (enumeration.hasMoreElements())
			resultDataValues[i++] = enumeration.nextElement();
		return resultDataValues;
	}

	public boolean isComplexType() {
		return childDataValues.size() > 0;
	}

	@Override
	public boolean isComplexTypeProperty() {
		return isComplexType();
	}

	@Override
	public ODataProperty[] getChildProperties() {
		return getChildDataValues();
	}

	@Override
	public ODataProperty getChildProperty(String key) {
		return getChildDataValue(key);
	}

	@Override
	public void putChildProperty(ODataProperty property) {
		putChildDataValue((ODataPropertyImpl) property);
	}

	@Override
	public ODataCollection getCollectionValue() {
		return null;
	}

	@Override
	public boolean isCollectionTypeProperty() {
		return false;
	}

}
