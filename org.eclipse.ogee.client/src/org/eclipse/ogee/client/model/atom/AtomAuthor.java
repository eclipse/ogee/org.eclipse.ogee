/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The "atom:author" element is a Person construct that indicates the author of
 * the entry or feed.
 * 
 * 
 */
public class AtomAuthor extends BaseElement implements AtomPersonConstruct {
	private String atomName;
	private String atomUri;
	private String atomEmail;

	public AtomAuthor() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @return the atomName
	 */
	public String getAtomName() {
		return atomName;
	}

	/**
	 * @param atomName
	 *            the atomName to set
	 */
	public void setAtomName(String atomName) {
		this.atomName = atomName;
	}

	@Override
	public String getAtomUri() {
		return atomUri;
	}

	@Override
	public String getAtomEmail() {
		return atomEmail;
	}

	/**
	 * @param atomUri
	 *            the atomUri to set
	 */
	public void setAtomUri(String atomUri) {
		this.atomUri = atomUri;
	}

	/**
	 * @param atomEmail
	 *            the atomEmail to set
	 */
	public void setAtomEmail(String atomEmail) {
		this.atomEmail = atomEmail;
	}

}
