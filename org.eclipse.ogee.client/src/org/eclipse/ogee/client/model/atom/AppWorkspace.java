/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The "app:workspace" element contains zero or more "app:collection" elements
 * describing the Collections of Resources available for editing. Workspaces are
 * server-defined groups of Collections.
 * 
 * 
 */
public class AppWorkspace extends BaseElement {
	private String atomTitle;
	private AppCollection[] collections;

	/**
	 * Default constructor
	 */
	public AppWorkspace() {
		super(new Hashtable<String, String>());
		collections = new AppCollection[0];
	}

	/**
	 * @return the atomTitle
	 */
	public String getAtomTitle() {
		return atomTitle;
	}

	/**
	 * @param atomTitle
	 *            the atomTitle to set
	 */
	public void setAtomTitle(String atomTitle) {
		this.atomTitle = atomTitle;
	}

	/**
	 * @return the collections
	 */
	public AppCollection[] getCollections() {
		if (this.collections != null) {
			return Arrays.copyOf(this.collections, this.collections.length);

		}
		return null;

	}

	/**
	 * @param collections
	 *            the collections to set
	 */
	public void setCollections(AppCollection[] collections) {
		this.collections = collections;
	}

}
