/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The "atom:content" element either contains or links to the content of an
 * entry. The content of "atom:content" is Language-Sensitive.
 * 
 * 
 */
public class AtomContent extends BaseElement {
	private String type;
	private String src;
	private Mproperties mProperties;

	/**
	 * Default constructor
	 */
	public AtomContent() {
		super(new Hashtable<String, String>());
		setmProperties(new Mproperties());
	}

	/**
	 * @return the src
	 */
	public String getSrc() {
		return src;
	}

	/**
	 * @param src
	 *            the src to set
	 */
	public void setSrc(String src) {
		this.src = src;
		this.setAttribute(SRC, src);
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
		this.setAttribute(ATOM_CONTENT_TYPE, type);
	}

	/**
	 * @param mProperties
	 *            the mProperties to set
	 */
	public void setmProperties(Mproperties mProperties) {
		this.mProperties = mProperties;
	}

	/**
	 * @return the mProperties
	 */
	public Mproperties getmProperties() {
		return mProperties;
	}

}
