/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.model.generic.InlineData;
import org.eclipse.ogee.client.model.generic.ODataLink;

/**
 * The "atom:link" element defines a reference from an entry or feed to a Web
 * resource.
 * 
 * 
 */
public class AtomLink extends BaseElement implements ODataLink {
	private String href;
	private String rel;
	private String type;
	private String title;
	private Inline mInline;

	/**
	 * Default constructor
	 */
	public AtomLink() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @return the mInline
	 */
	public Inline getmInline() {
		return mInline;
	}

	/**
	 * @param mInline
	 *            the mInline to set
	 */
	public void setmInline(Inline mInline) {
		this.mInline = mInline;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
		this.setAttribute(ATOM_LINK_TYPE, type);
	}

	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param href
	 *            the href to set
	 */
	public void setHref(String href) {
		this.href = href;
		this.setAttribute(HREF, href);
	}

	/**
	 * @return the rel
	 */
	public String getRel() {
		return rel;
	}

	/**
	 * @param rel
	 *            the rel to set
	 */
	public void setRel(String rel) {
		this.rel = rel;
		this.setAttribute(REL, rel);
	}

	@Override
	public InlineData getInlineData() {
		return getmInline();
	}

	@Override
	public String toString() {
		return href;
	}

}
