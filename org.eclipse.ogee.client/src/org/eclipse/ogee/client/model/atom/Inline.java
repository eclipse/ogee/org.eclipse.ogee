/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.atom;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.model.generic.InlineData;
import org.eclipse.ogee.client.model.generic.ODataCollection;

/**
 * A related Entry or collection of Entries is represented as the child element
 * of an "m:inline" element as an "atom:feed" or "atom:entry" respectively.
 * 
 * 
 */
public class Inline extends BaseElement implements InlineData {
	private AtomFeed feed;
	private AtomEntry entry;

	private boolean isFeed;
	private boolean isEntry;

	/**
	 * Default constructor
	 */
	public Inline() {
		super(new Hashtable<String, String>());
		isFeed = false;
		isEntry = false;
	}

	/**
	 * @return the feed
	 */
	public AtomFeed getFeed() {
		return feed;
	}

	/**
	 * @param feed
	 *            the feed to set
	 */
	public void setFeed(AtomFeed feed) {
		this.feed = feed;
		isFeed = true;
		isEntry = false;
		this.entry = null;
	}

	/**
	 * @return the entry
	 */
	public AtomEntry getEntry() {
		return entry;
	}

	/**
	 * @param entry
	 *            the entry to set
	 */
	public void setEntry(AtomEntry entry) {
		this.entry = entry;
		isEntry = true;
		isFeed = false;
		this.feed = null;
	}

	/**
	 * @return the isFeed
	 */
	public boolean isFeed() {
		return isFeed;
	}

	/**
	 * @return the isEntry
	 */
	public boolean isEntry() {
		return isEntry;
	}

	@Override
	public boolean isCollection() {
		return isFeed;
	}

	@Override
	public ODataCollection getCollection() {
		return getFeed();
	}
}