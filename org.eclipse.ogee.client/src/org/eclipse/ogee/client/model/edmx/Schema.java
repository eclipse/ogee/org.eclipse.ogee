/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The Schema is the top-level CSDL construct that allows creation of namespace.
 * The contents of a namespace may be defined by one or more Schema instances.
 * 
 * 
 */
public class Schema extends BaseElement {
	private String namespace;
	private String alias;
	private Hashtable<String, String> namespaces;
	private EntityType[] entityTypes;
	private Association[] associations;
	private EntityContainer[] entityContainers;
	private ComplexType[] complexTypes;
	private Using[] usings;

	/**
	 * Default constructor
	 */
	public Schema() {
		super(new Hashtable<String, String>());
		namespaces = new Hashtable<String, String>();
		entityTypes = new EntityType[0];
		usings = new Using[0];
		associations = new Association[0];
		entityContainers = new EntityContainer[0];
		complexTypes = new ComplexType[0];
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace
	 *            the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
		this.setAttribute(NAMESPACE, namespace);
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @param alias
	 *            the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
		this.setAttribute(ALIAS, alias);
	}

	/**
	 * @return the namespaces
	 */
	public Hashtable<String, String> getNamespaces() {
		return namespaces;
	}

	/**
	 * @param namespaces
	 *            the namespaces to set
	 */
	public void setNamespaces(Hashtable<String, String> namespaces) {
		this.namespaces = namespaces;
		Enumeration<String> keys = namespaces.keys();// $JL-COLLECTION$
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			this.setAttribute(key, namespaces.get(key));
		}
	}

	/**
	 * @return the entityTypes
	 */
	public EntityType[] getEntityTypes() {
		if (this.entityTypes != null) {
			return Arrays.copyOf(this.entityTypes, this.entityTypes.length);
		}
		return null;
	}

	/**
	 * @param entityTypes
	 *            the entityTypes to set
	 */
	public void setEntityTypes(EntityType[] entityTypes) {
		this.entityTypes = entityTypes;
	}

	/**
	 * @return the usings
	 */
	public Using[] getUsings() {
		if (this.usings != null) {
			return Arrays.copyOf(this.usings, this.usings.length);
		}
		return null;
	}

	/**
	 * @param usings
	 *            the usings to set
	 */
	public void setUsings(Using[] usings) {
		this.usings = usings;
	}

	/**
	 * @return the associations
	 */
	public Association[] getAssociations() {
		if (this.associations != null) {
			return Arrays.copyOf(this.associations, this.associations.length);
		}
		return null;

	}

	/**
	 * @param associations
	 *            the associations to set
	 */
	public void setAssociations(Association[] associations) {
		this.associations = associations;
	}

	/**
	 * @return the entityContainers
	 */
	public EntityContainer[] getEntityContainers() {
		if (this.entityContainers != null) {
			return Arrays.copyOf(this.entityContainers,
					this.entityContainers.length);
		}
		return null;

	}

	/**
	 * @param entityContainers
	 *            the entityContainers to set
	 */
	public void setEntityContainers(EntityContainer[] entityContainers) {
		this.entityContainers = entityContainers;
	}

	/**
	 * @return the complexTypes
	 */
	public ComplexType[] getComplexTypes() {
		if (this.complexTypes != null) {
			return Arrays.copyOf(this.complexTypes, this.complexTypes.length);
		}
		return null;

	}

	/**
	 * @param complexTypes
	 *            the complexTypes to set
	 */
	public void setComplexTypes(ComplexType[] complexTypes) {
		this.complexTypes = complexTypes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + Arrays.hashCode(associations);
		result = prime * result + Arrays.hashCode(complexTypes);
		result = prime * result + Arrays.hashCode(entityContainers);
		result = prime * result + Arrays.hashCode(entityTypes);
		result = prime * result
				+ ((namespace == null) ? 0 : namespace.hashCode());
		result = prime * result
				+ ((namespaces == null) ? 0 : namespaces.hashCode());
		result = prime * result + Arrays.hashCode(usings);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Schema other = (Schema) obj;
		if (alias == null) {
			if (other.alias != null) {
				return false;
			}
		} else if (!alias.equals(other.alias)) {
			return false;
		}
		if (!Arrays.equals(associations, other.associations)) {
			return false;
		}
		if (!Arrays.equals(complexTypes, other.complexTypes)) {
			return false;
		}
		if (!Arrays.equals(entityContainers, other.entityContainers)) {
			return false;
		}
		if (!Arrays.equals(entityTypes, other.entityTypes)) {
			return false;
		}
		if (namespace == null) {
			if (other.namespace != null) {
				return false;
			}
		} else if (!namespace.equals(other.namespace)) {
			return false;
		}
		if (namespaces == null) {
			if (other.namespaces != null) {
				return false;
			}
		} else if (!namespaces.equals(other.namespaces)) {
			return false;
		}
		if (!Arrays.equals(usings, other.usings)) {
			return false;
		}
		return true;
	}

}
