/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.model.edmx.v3;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.parser.impl.EdmTypes;

/**
 * An EnumType element is used in CSDL 3.0 to declare an enumeration type.
 * Enumeration types are scalar types. An enumeration type has a Name attribute,
 * an optional UnderlyingType attribute, an optional IsFlags attribute, and a
 * payload that consists of zero or more declared Member elements.
 * 
 * Enumeration types are equal-comparable, order-comparable, and can participate
 * in entity Key elements that is, they can be the Key or can be a part of the
 * Key.
 */
public class EnumType extends BaseElement {
	// elements MUST specify a Name attribute. Name attribute is of type
	// SimpleIdentifier.
	private String name;

	// elements can specify an UnderlyingType attribute which MUST be an
	// integral EDMSimpleType, such as SByte, Int16, Int32, Int64, or Byte.
	// Edm.Int32 MUST be assumed if it is not specified in the declaration.
	private String underlyingType = EdmTypes.Int32.toString(); // "Edm.Int32";

	// elements can specify an IsFlags Boolean attribute, which MUST be assumed
	// to be false if it is not specified in the declaration.
	// If the enumeration type can be treated as a bit field, IsFlags MUST be
	// set to "true".
	private boolean isFlags = false;

	// elements can contain a list of zero or more Member child elements that
	// are referred to as declared enumeration members.
	private Member[] members;

	private ValueAnnotation[] valueAnnotations;

	private TypeAnnotation[] typeAnnotations;

	/**
	 * Constructs a new Enum Type.
	 */
	public EnumType() {
		super(new Hashtable<String, String>());
		members = new Member[0];
		typeAnnotations = new TypeAnnotation[0];
		valueAnnotations = new ValueAnnotation[0];
	}

	/**
	 * Returns the name of this enum type.
	 * 
	 * @return the name of this enum type.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of this enum type.
	 * 
	 * @param name
	 *            - the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the underlying type of this enum type.
	 * 
	 * @return the underlying type of this enum type.
	 */
	public String getUnderlyingType() {
		return underlyingType;
	}

	/**
	 * Sets the underlying type of this enum type.
	 * 
	 * @param underlyingType
	 *            - the underlying type to set
	 */
	public void setUnderlyingType(String underlyingType) {
		this.underlyingType = underlyingType;
	}

	/**
	 * Returns whether this enum type can be treated as a bit field.
	 * 
	 * @return true if this enum type can be treated as a bit field, and false
	 *         otherwise.
	 */
	public boolean isFlags() {
		return isFlags;
	}

	/**
	 * Sets whether this enum type can be treated as a bit field.
	 * 
	 * @param isFlags
	 *            - the boolean to set
	 */
	public void setFlags(boolean isFlags) {
		this.isFlags = isFlags;
	}

	/**
	 * Returns the members of this enum type.
	 * 
	 * @return the members of this enum type.
	 */
	public Member[] getMembers() {
		if (this.members != null) {
			return Arrays.copyOf(this.members, this.members.length);
		}
		return null;
	}

	/**
	 * Sets the members of this enum type.
	 * 
	 * @param members
	 *            - the members to set
	 */
	public void setMembers(Member[] members) {
		this.members = members;
	}

	/**
	 * Returns the value annotations of this enum type.
	 * 
	 * @return the value annotations of this enum type.
	 */
	public ValueAnnotation[] getValueAnnotations() {
		if (this.valueAnnotations != null) {
			return Arrays.copyOf(this.valueAnnotations,
					this.valueAnnotations.length);
		}
		return null;
	}

	/**
	 * Sets the value annotations of this enum type.
	 * 
	 * @param valueAnnotations
	 *            - the value annotations to set
	 */
	public void setValueAnnotations(ValueAnnotation[] valueAnnotations) {
		this.valueAnnotations = valueAnnotations;
	}

	/**
	 * Returns the type annotations of this enum type.
	 * 
	 * @return the type annotations of this enum type.
	 */
	public TypeAnnotation[] getTypeAnnotations() {
		if (this.typeAnnotations != null) {
			return Arrays.copyOf(this.typeAnnotations,
					this.typeAnnotations.length);
		}
		return null;
	}

	/**
	 * Sets the type annotaions of this enum type.
	 * 
	 * @param typeAnnotations
	 *            - the type annotations to set
	 */
	public void setTypeAnnotations(TypeAnnotation[] typeAnnotations) {
		this.typeAnnotations = typeAnnotations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (isFlags ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(members);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Arrays.hashCode(typeAnnotations);
		result = prime * result
				+ ((underlyingType == null) ? 0 : underlyingType.hashCode());
		result = prime * result + Arrays.hashCode(valueAnnotations);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EnumType other = (EnumType) obj;
		if (isFlags != other.isFlags) {
			return false;
		}
		if (!Arrays.equals(members, other.members)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (!Arrays.equals(typeAnnotations, other.typeAnnotations)) {
			return false;
		}
		if (underlyingType == null) {
			if (other.underlyingType != null) {
				return false;
			}
		} else if (!underlyingType.equals(other.underlyingType)) {
			return false;
		}
		if (!Arrays.equals(valueAnnotations, other.valueAnnotations)) {
			return false;
		}
		return true;
	}

}
