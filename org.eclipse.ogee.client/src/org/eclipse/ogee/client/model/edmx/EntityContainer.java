/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.model.generic.StringUtilities;

/**
 * EntityContainer is conceptually similar to a database or data source. It
 * groups EntitySet, AssociationSet and FunctionImport sub-elements that
 * represent a data source.
 * 
 * 
 */
public class EntityContainer extends BaseElement {
	private String name;
	private EntitySet[] entitySets;
	private AssociationSet[] associationSets;
	private FunctionImport[] functionImports;
	private boolean isDefault;
	private Documentation documentation;

	/**
	 * Default constructor
	 */
	public EntityContainer() {
		super(new Hashtable<String, String>());
		entitySets = new EntitySet[0];
		associationSets = new AssociationSet[0];
		functionImports = new FunctionImport[0];
		isDefault = false;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
		this.setAttribute(NAME, name);
	}

	/**
	 * @return is default
	 */
	public boolean isDefault() {
		return this.isDefault;
	}

	/**
	 * @param isDefault
	 */
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
		this.setAttribute(M_IS_DEFAULT_ENTITY_CONTAINER,
				StringUtilities.stringValueOf(isDefault));
	}

	/**
	 * @return the entitySets
	 */
	public EntitySet[] getEntitySets() {
		return Arrays.copyOf(this.entitySets, this.entitySets.length);
	}

	/**
	 * @param entitySets
	 *            the entitySets to set
	 */
	public void setEntitySets(EntitySet[] entitySets) {
		this.entitySets = entitySets;
	}

	/**
	 * @return the associationSets
	 */
	public AssociationSet[] getAssociationSets() {
		if (this.associationSets != null) {
			return Arrays.copyOf(this.associationSets,
					this.associationSets.length);
		}
		return null;

	}

	/**
	 * @param associationSets
	 *            the associationSets to set
	 */
	public void setAssociationSets(AssociationSet[] associationSets) {
		this.associationSets = associationSets;
	}

	/**
	 * @return the functionImports
	 */
	public FunctionImport[] getFunctionImports() {
		if (this.functionImports != null) {
			return Arrays.copyOf(this.functionImports,
					this.functionImports.length);
		}
		return null;

	}

	/**
	 * @param functionImports
	 *            the functionImports to set
	 */
	public void setFunctionImports(FunctionImport[] functionImports) {
		this.functionImports = functionImports;
	}

	/**
	 * @return the documentation
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * @param documentation
	 *            the documentation to set
	 */
	public void setDocumentation(Documentation documentation) {
		this.documentation = documentation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(associationSets);
		result = prime * result
				+ ((documentation == null) ? 0 : documentation.hashCode());
		result = prime * result + Arrays.hashCode(entitySets);
		result = prime * result + Arrays.hashCode(functionImports);
		result = prime * result + (isDefault ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EntityContainer other = (EntityContainer) obj;
		if (!Arrays.equals(associationSets, other.associationSets)) {
			return false;
		}
		if (documentation == null) {
			if (other.documentation != null) {
				return false;
			}
		} else if (!documentation.equals(other.documentation)) {
			return false;
		}
		if (!Arrays.equals(entitySets, other.entitySets)) {
			return false;
		}
		if (!Arrays.equals(functionImports, other.functionImports)) {
			return false;
		}
		if (isDefault != other.isDefault) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
