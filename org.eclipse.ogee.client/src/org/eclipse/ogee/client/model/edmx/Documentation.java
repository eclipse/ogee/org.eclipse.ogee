/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The Documentation element is used to provide documentation of comments on the
 * contents of the CSDL file.
 * 
 * 
 */
public class Documentation extends BaseElement {
	private LongDescription longDescription;
	private Summary summary;

	/**
	 * Default constructor
	 */
	public Documentation() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @return the longDescription
	 */
	public LongDescription getLongDescription() {
		return longDescription;
	}

	/**
	 * @param longDescription
	 *            the longDescription to set
	 */
	public void setLongDescription(LongDescription longDescription) {
		this.longDescription = longDescription;
	}

	/**
	 * @return the summary
	 */
	public Summary getSummary() {
		return summary;
	}

	/**
	 * @param summary
	 *            the summary to set
	 */
	public void setSummary(Summary summary) {
		this.summary = summary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((longDescription == null) ? 0 : longDescription.hashCode());
		result = prime * result + ((summary == null) ? 0 : summary.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!super.equals(obj)) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		Documentation other = (Documentation) obj;
		if (longDescription == null) {
			if (other.longDescription != null) {
				return false;
			}
		} else if (!longDescription.equals(other.longDescription)) {
			return false;
		}

		if (summary == null) {
			if (other.summary != null) {
				return false;
			}
		} else if (!summary.equals(other.summary)) {
			return false;
		}

		return true;
	}
}
