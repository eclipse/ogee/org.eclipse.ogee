/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v3.expressions.primitive;

/**
 * Decimal allows content in the following form: [0-9]+.[0-9]+M|m.
 * 
 * 
 */
public class Decimal extends PrimitiveExpression {
	public static final java.lang.String DECIMAL = "Decimal"; //$NON-NLS-1$

}
