/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v3;

import java.util.Arrays;

import org.eclipse.ogee.client.model.edmx.EntitySet;

/**
 * Represents an Entity Set for OData V3.
 */
public class EntitySetV3 extends EntitySet {
	// In CSDL 3.0, EntitySet can contain any number of TypeAnnotation elements.
	private TypeAnnotation[] typeAnnotations;
	// In CSDL 3.0, EntitySet can contain any number of ValueAnnotation
	// elements.
	private ValueAnnotation[] valueAnnotations;

	/**
	 * Constructs a new entity set.
	 */
	public EntitySetV3() {
		super();

		this.typeAnnotations = new TypeAnnotation[0];
		this.valueAnnotations = new ValueAnnotation[0];
	}

	/**
	 * Returns the type annotations of this entity set.
	 * 
	 * @return the type annotations of this entity set.
	 */
	public TypeAnnotation[] getTypeAnnotations() {
		if (this.typeAnnotations != null) {
			return Arrays.copyOf(this.typeAnnotations,
					this.typeAnnotations.length);
		}
		return null;
	}

	/**
	 * Sets the type annotations of this entity set.
	 * 
	 * @param typeAnnotations
	 *            - the type annotations to set
	 */
	public void setTypeAnnotations(TypeAnnotation[] typeAnnotations) {
		this.typeAnnotations = typeAnnotations;
	}

	/**
	 * Returns the value annotations of this entity set.
	 * 
	 * @return the value annotations of this entity set.
	 */
	public ValueAnnotation[] getValueAnnotations() {
		if (this.valueAnnotations != null) {
			return Arrays.copyOf(this.valueAnnotations,
					this.valueAnnotations.length);
		}
		return null;
	}

	/**
	 * Sets the value annotations of this entity set.
	 * 
	 * @param valueAnnotations
	 *            - the value annotations to set
	 */
	public void setValueAnnotations(ValueAnnotation[] valueAnnotations) {
		this.valueAnnotations = valueAnnotations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(typeAnnotations);
		result = prime * result + Arrays.hashCode(valueAnnotations);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EntitySetV3 other = (EntitySetV3) obj;
		if (!Arrays.equals(typeAnnotations, other.typeAnnotations)) {
			return false;
		}
		if (!Arrays.equals(valueAnnotations, other.valueAnnotations)) {
			return false;
		}
		return true;
	}
}
