/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * An Entity Key is formed from a subset of properties of the Entity Type. The
 * Entity Key is a fundamental concept for uniquely identifying instances of
 * Entity Types and allowing Entity Type instances to participate in
 * relationships.
 * 
 * 
 */
public class Key extends BaseElement {
	private PropertyRef[] propertyRefs;

	/**
	 * Default constructor
	 */
	public Key() {
		super(new Hashtable<String, String>());
		propertyRefs = new PropertyRef[0];
	}

	/**
	 * @return the propertyRefs
	 */
	public PropertyRef[] getPropertyRefs() {
		if (this.propertyRefs != null) {
			return Arrays.copyOf(this.propertyRefs, this.propertyRefs.length);
		}
		return null;

	}

	/**
	 * @param propertyRefs
	 *            the propertyRefs to set
	 */
	public void setPropertyRefs(PropertyRef[] propertyRefs) {
		this.propertyRefs = propertyRefs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(propertyRefs);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Key other = (Key) obj;
		if (!Arrays.equals(propertyRefs, other.propertyRefs)) {
			return false;
		}
		return true;
	}

}
