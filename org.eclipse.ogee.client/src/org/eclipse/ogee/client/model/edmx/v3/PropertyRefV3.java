/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v3;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.PropertyRef;

/**
 * PropertyRefV3 element refers to a declared property of an EntityType.
 * Example: PropertyRef Name="CustomerId"
 * 
 * 
 */
public class PropertyRefV3 extends PropertyRef {
	private Documentation documentation;

	/**
	 * Constructs a new property reference.
	 */
	public PropertyRefV3() {
		super();
	}

	/**
	 * Returns the documentation of this property reference.
	 * 
	 * @return the documentation of this property reference.
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * Sets the documentation of this property reference.
	 * 
	 * @param documentation
	 *            - the documentation to set
	 */
	public void setDocumentation(Documentation documentation) {
		this.documentation = documentation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((documentation == null) ? 0 : documentation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PropertyRefV3 other = (PropertyRefV3) obj;
		if (documentation == null) {
			if (other.documentation != null) {
				return false;
			}
		} else if (!documentation.equals(other.documentation)) {
			return false;
		}
		return true;
	}
}
