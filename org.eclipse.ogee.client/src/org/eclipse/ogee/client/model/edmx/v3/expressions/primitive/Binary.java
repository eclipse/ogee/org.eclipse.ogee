/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.model.edmx.v3.expressions.primitive;

/**
 * Binary allows content in the following form: [A-Fa-f0-9][A-Fa-f0-9]*.
 * 
 * 
 */
public class Binary extends PrimitiveExpression {
	public static final java.lang.String BINARY = "Binary"; //$NON-NLS-1$

}
