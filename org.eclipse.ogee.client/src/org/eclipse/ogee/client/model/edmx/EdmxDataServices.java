/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The "edmx:DataServices" element contains the service metadata of a Data
 * Service. This service metadata contains zero or more EDM conceptual schemas
 * (as specified in [MC-CSDL]).
 * 
 * 
 */
public class EdmxDataServices extends BaseElement {
	private String mDataServiceVersion;
	private Hashtable<String, String> namespaces;
	private Schema[] schemas;

	/**
	 * Default constructor
	 */
	public EdmxDataServices() {
		super(new Hashtable<String, String>());
		namespaces = new Hashtable<String, String>();
		schemas = new Schema[0];
	}

	/**
	 * @return the mDataServiceVersion
	 */
	public String getmDataServiceVersion() {
		return mDataServiceVersion;
	}

	/**
	 * @param mDataServiceVersion
	 *            the mDataServiceVersion to set
	 */
	public void setmDataServiceVersion(String mDataServiceVersion) {
		this.mDataServiceVersion = mDataServiceVersion;
		this.setAttribute(M_DATA_SERVICE_VERSION, mDataServiceVersion);
	}

	/**
	 * @return the namespaces
	 */
	public Hashtable<String, String> getNamespaces() {
		return namespaces;
	}

	/**
	 * @param namespaces
	 *            the namespaces to set
	 */
	public void setNamespaces(Hashtable<String, String> namespaces) {
		this.namespaces = namespaces;
		Enumeration<String> keys = namespaces.keys();// $JL-COLLECTION$
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			this.setAttribute(key, namespaces.get(key));
		}
	}

	/**
	 * @return the schemas
	 */
	public Schema[] getSchemas() {
		if (this.schemas != null) {
			return Arrays.copyOf(this.schemas, this.schemas.length);
		}
		return null;

	}

	/**
	 * @param schemas
	 *            the schemas to set
	 */
	public void setSchemas(Schema[] schemas) {
		this.schemas = schemas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((mDataServiceVersion == null) ? 0 : mDataServiceVersion
						.hashCode());
		result = prime * result
				+ ((namespaces == null) ? 0 : namespaces.hashCode());
		result = prime * result + Arrays.hashCode(schemas);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!super.equals(obj)) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		EdmxDataServices other = (EdmxDataServices) obj;
		if (mDataServiceVersion == null) {
			if (other.mDataServiceVersion != null) {
				return false;
			}
		} else if (!mDataServiceVersion.equals(other.mDataServiceVersion)) {
			return false;
		}

		if (namespaces == null) {
			if (other.namespaces != null) {
				return false;
			}
		} else if (!namespaces.equals(other.namespaces)) {
			return false;
		}

		if (!Arrays.equals(schemas, other.schemas)) {
			return false;
		}

		return true;
	}
}
