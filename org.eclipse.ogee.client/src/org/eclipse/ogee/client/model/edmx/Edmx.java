/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * EDMX stands for Entity Data Model Extensions. Every EDMX document must have a
 * single "edmx:Edmx" element. When used to package the service metadata for a
 * Data Service the "edmx:Edmx" element must contain exactly one
 * "edmx:DataServices" element.
 * 
 * 
 */
public class Edmx extends BaseElement {
	private Hashtable<String, String> namespaces;
	private String version;
	private EdmxDataServices edmxDataServices;

	private Set<String> notSupported;

	/**
	 * Default constructor
	 */
	public Edmx() {
		super(new Hashtable<String, String>());
		namespaces = new Hashtable<String, String>();
		edmxDataServices = new EdmxDataServices();
		notSupported = new HashSet<String>();
	}

	public void addNotSupported(String tagName) {
		notSupported.add(tagName);
	}

	/**
	 * @return the namespaces
	 */
	public Hashtable<String, String> getNamespaces() {
		return namespaces;
	}

	/**
	 * @param namespaces
	 *            the namespaces to set
	 */
	public void setNamespaces(Hashtable<String, String> namespaces) {
		this.namespaces = namespaces;
		Enumeration<String> keys = namespaces.keys();// $JL-COLLECTION$
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			this.setAttribute(key, namespaces.get(key).toString());
		}
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
		this.setAttribute(VERSION, version);
	}

	/**
	 * @return the edmxDataServices
	 */
	public EdmxDataServices getEdmxDataServices() {
		return edmxDataServices;
	}

	/**
	 * @param edmxDataServices
	 *            the edmxDataServices to set
	 */
	public void setEdmxDataServices(EdmxDataServices edmxDataServices) {
		this.edmxDataServices = edmxDataServices;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((edmxDataServices == null) ? 0 : edmxDataServices.hashCode());
		result = prime * result
				+ ((namespaces == null) ? 0 : namespaces.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Edmx other = (Edmx) obj;
		if (edmxDataServices == null) {
			if (other.edmxDataServices != null) {
				return false;
			}
		} else if (!edmxDataServices.equals(other.edmxDataServices)) {
			return false;
		}
		if (namespaces == null) {
			if (other.namespaces != null) {
				return false;
			}
		} else if (!namespaces.equals(other.namespaces)) {
			return false;
		}
		if (version == null) {
			if (other.version != null) {
				return false;
			}
		} else if (!version.equals(other.version)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the notSupported
	 */
	public Set<String> getNotSupported() {
		return notSupported;
	}

	/**
	 * @param notSupported
	 *            the notSupported to set
	 */
	public void setNotSupported(Set<String> notSupported) {
		this.notSupported = notSupported;
	}

}
