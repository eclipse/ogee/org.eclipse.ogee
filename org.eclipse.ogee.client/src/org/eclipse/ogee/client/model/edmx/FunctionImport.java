/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * Actions are translated into OData as function imports with a name consisting
 * of the associated entity type plus the name of the action, and the key
 * properties of entity type plus the parameters of the actions as function
 * parameters.
 * 
 * The FunctionImport element is used to import Stored Procedures or Functions
 * defined in Store Schema Model into Entity Data Model.
 * 
 * 
 */
public class FunctionImport extends BaseElement {
	private String name;
	private String returnType;
	private String mHttpMethod;
	private Parameter[] parameters;
	private String entitySet;
	private Documentation documentation;

	/**
	 * Default constructor
	 */
	public FunctionImport() {
		super(new Hashtable<String, String>());
		parameters = new Parameter[0];
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
		this.setAttribute(NAME, name);
	}

	/**
	 * @return the entity set
	 */
	public String getEntitySet() {
		return entitySet;
	}

	/**
	 * @param entitySet
	 *            the entity set to set
	 */
	public void setEntitySet(String entitySet) {
		this.entitySet = entitySet;
		this.setAttribute(ENTITY_SET, entitySet);
	}

	/**
	 * @return the returnType
	 */
	public String getReturnType() {
		return returnType;
	}

	/**
	 * @param returnType
	 *            the returnType to set
	 */
	public void setReturnType(String returnType) {
		this.returnType = returnType;
		this.setAttribute(RETURN_TYPE, returnType);
	}

	/**
	 * @return the mHttpMethod
	 */
	public String getmHttpMethod() {
		return mHttpMethod;
	}

	/**
	 * @param mHttpMethod
	 *            the mHttpMethod to set
	 */
	public void setmHttpMethod(String mHttpMethod) {
		this.mHttpMethod = mHttpMethod;
		this.setAttribute(M_HTTP_METHOD, mHttpMethod);
	}

	/**
	 * @return the parameters
	 */
	public Parameter[] getParameters() {
		if (this.parameters != null) {
			return Arrays.copyOf(this.parameters, this.parameters.length);
		}
		return null;
	}

	/**
	 * @param parameters
	 *            the parameters to set
	 */
	public void setParameters(Parameter[] parameters) {
		this.parameters = parameters;
	}

	/**
	 * @return the documentation
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * @param documentation
	 *            the documentation to set
	 */
	public void setDocumentation(Documentation documentation) {
		this.documentation = documentation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((documentation == null) ? 0 : documentation.hashCode());
		result = prime * result
				+ ((entitySet == null) ? 0 : entitySet.hashCode());
		result = prime * result
				+ ((mHttpMethod == null) ? 0 : mHttpMethod.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Arrays.hashCode(parameters);
		result = prime * result
				+ ((returnType == null) ? 0 : returnType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FunctionImport other = (FunctionImport) obj;
		if (documentation == null) {
			if (other.documentation != null) {
				return false;
			}
		} else if (!documentation.equals(other.documentation)) {
			return false;
		}
		if (entitySet == null) {
			if (other.entitySet != null) {
				return false;
			}
		} else if (!entitySet.equals(other.entitySet)) {
			return false;
		}
		if (mHttpMethod == null) {
			if (other.mHttpMethod != null) {
				return false;
			}
		} else if (!mHttpMethod.equals(other.mHttpMethod)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (!Arrays.equals(parameters, other.parameters)) {
			return false;
		}
		if (returnType == null) {
			if (other.returnType != null) {
				return false;
			}
		} else if (!returnType.equals(other.returnType)) {
			return false;
		}
		return true;
	}
}
