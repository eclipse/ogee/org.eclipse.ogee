/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v4;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The edmx:Reference element contains zero or more edmx:IncludeAnnotations
 * elements that specify the annotations to include from the target document.
 * 
 */
public class EdmxIncludeAnnotations extends BaseElement {
	// A term namespace is a string that disambiguates terms with the same name.
	private String termNamespace;
	// A qualifier is used to apply an annotation to a subset of consumers.
	// For instance, a service author might want to supply a different set of
	// annotations for various device form factors.
	private String qualifier;

	/**
	 * Default constructor
	 */
	public EdmxIncludeAnnotations() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @return the termNamespace
	 */
	public String getTermNamespace() {
		return termNamespace;
	}

	/**
	 * @param termNamespace
	 *            the termNamespace to set
	 */
	public void setTermNamespace(String termNamespace) {
		this.termNamespace = termNamespace;
	}

	/**
	 * @return the qualifier
	 */
	public String getQualifier() {
		return qualifier;
	}

	/**
	 * @param qualifier
	 *            the qualifier to set
	 */
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

}
