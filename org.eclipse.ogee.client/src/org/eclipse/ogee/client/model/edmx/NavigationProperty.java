/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * Navigation properties are the anchor points for associations, describing
 * possible links to related data. Navigation Properties are special properties
 * on Entity Types which are bound to a specific association and can be used to
 * refer to associations of an entity.
 * 
 * 
 */
public class NavigationProperty extends BaseElement {
	private String name;
	private String relationship;
	private String fromRole;
	private String toRole;
	private Documentation documentation;

	/**
	 * Default constructor
	 */
	public NavigationProperty() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
		this.setAttribute(NAME, name);
	}

	/**
	 * @return the relationship
	 */
	public String getRelationship() {
		return relationship;
	}

	/**
	 * @param relationship
	 *            the relationship to set
	 */
	public void setRelationship(String relationship) {
		this.relationship = relationship;
		this.setAttribute(RELATIONSHIP, relationship);
	}

	/**
	 * @return the fromRole
	 */
	public String getFromRole() {
		return fromRole;
	}

	/**
	 * @param fromRole
	 *            the fromRole to set
	 */
	public void setFromRole(String fromRole) {
		this.fromRole = fromRole;
		this.setAttribute(FROM_ROLE, fromRole);
	}

	/**
	 * @return the toRole
	 */
	public String getToRole() {
		return toRole;
	}

	/**
	 * @param toRole
	 *            the toRole to set
	 */
	public void setToRole(String toRole) {
		this.toRole = toRole;
		this.setAttribute(TO_ROLE, toRole);
	}

	/**
	 * @return the documentation
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * @param documentation
	 *            the documentation to set
	 */
	public void setDocumentation(Documentation documentation) {
		this.documentation = documentation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((documentation == null) ? 0 : documentation.hashCode());
		result = prime * result
				+ ((fromRole == null) ? 0 : fromRole.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((relationship == null) ? 0 : relationship.hashCode());
		result = prime * result + ((toRole == null) ? 0 : toRole.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NavigationProperty other = (NavigationProperty) obj;
		if (documentation == null) {
			if (other.documentation != null) {
				return false;
			}
		} else if (!documentation.equals(other.documentation)) {
			return false;
		}
		if (fromRole == null) {
			if (other.fromRole != null) {
				return false;
			}
		} else if (!fromRole.equals(other.fromRole)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (relationship == null) {
			if (other.relationship != null) {
				return false;
			}
		} else if (!relationship.equals(other.relationship)) {
			return false;
		}
		if (toRole == null) {
			if (other.toRole != null) {
				return false;
			}
		} else if (!toRole.equals(other.toRole)) {
			return false;
		}
		return true;
	}
}
