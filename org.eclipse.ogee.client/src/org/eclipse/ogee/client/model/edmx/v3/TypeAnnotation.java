/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.model.edmx.v3;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * A TypeAnnotation element is used to annotate a model element with a term and
 * provide zero or more values for the properties of the term.
 * 
 * 
 */
public class TypeAnnotation extends BaseElement {
	// Term MUST be a namespace qualified name, an alias qualified name, or a
	// SimpleIdentifier.
	private String term;
	// A TypeAnnotation can contain any number of PropertyValue elements.
	private PropertyValue[] propertyValues;

	/**
	 * Default constructor
	 */
	public TypeAnnotation() {
		super(new Hashtable<String, String>());
		setPropertyValues(new PropertyValue[0]);
	}

	/**
	 * @return the term
	 */
	public String getTerm() {
		return term;
	}

	/**
	 * @param term
	 *            the term to set
	 */
	public void setTerm(String term) {
		this.term = term;
	}

	/**
	 * @return the propertyValues
	 */
	public PropertyValue[] getPropertyValues() {
		if (this.propertyValues != null) {
			return Arrays.copyOf(this.propertyValues,
					this.propertyValues.length);
		}
		return null;
	}

	/**
	 * @param propertyValues
	 *            the propertyValues to set
	 */
	public void setPropertyValues(PropertyValue[] propertyValues) {
		this.propertyValues = propertyValues;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(propertyValues);
		result = prime * result + ((term == null) ? 0 : term.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TypeAnnotation other = (TypeAnnotation) obj;
		if (!Arrays.equals(propertyValues, other.propertyValues)) {
			return false;
		}
		if (term == null) {
			if (other.term != null) {
				return false;
			}
		} else if (!term.equals(other.term)) {
			return false;
		}
		return true;
	}

}
