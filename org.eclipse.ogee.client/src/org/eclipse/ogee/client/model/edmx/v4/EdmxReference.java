/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v4;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.eclipse.ogee.client.model.edm.v4.Annotation;
import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The edmx:Reference element specifies external CSDL documents referenced by
 * the referencing document.
 * 
 */
public class EdmxReference extends BaseElement {
	private EdmxInclude[] edmxIncludes;
	private EdmxIncludeAnnotations[] edmxIncludeAnnotations;
	private String uri;
	private List<Annotation> annotation;

	/**
	 * Default constructor
	 */
	public EdmxReference() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @return the edmxIncludes
	 */
	public EdmxInclude[] getEdmxIncludes() {
		if (edmxIncludes == null) {
			edmxIncludes = new EdmxInclude[0];
		}
		return edmxIncludes;
	}

	/**
	 * @param edmxIncludes
	 *            the edmxIncludes to set
	 */
	public void setEdmxIncludes(EdmxInclude[] edmxIncludes) {
		this.edmxIncludes = edmxIncludes;
	}

	/**
	 * @return the edmxIncludeAnnotations
	 */
	public EdmxIncludeAnnotations[] getEdmxIncludeAnnotations() {
		if (edmxIncludeAnnotations == null) {
			edmxIncludeAnnotations = new EdmxIncludeAnnotations[0];
		}
		return edmxIncludeAnnotations;
	}

	/**
	 * @param edmxIncludeAnnotations
	 *            the edmxIncludeAnnotations to set
	 */
	public void setEdmxIncludeAnnotations(
			EdmxIncludeAnnotations[] edmxIncludeAnnotations) {
		this.edmxIncludeAnnotations = edmxIncludeAnnotations;
	}

	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * @param uri
	 *            the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @return the edmAnnotation
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return this.annotation;
	}

	/**
	 * @param edmAnnotation
	 *            the edmAnnotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

}
