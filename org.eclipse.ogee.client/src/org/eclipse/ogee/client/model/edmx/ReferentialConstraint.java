/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * A referential constraint tells which properties on the dependent entity
 * correspond to which parts of the primary key of the leading entity.
 * 
 * 
 */
public class ReferentialConstraint extends BaseElement {
	private Principal principal;
	private Dependent dependent;
	private Documentation documentation;

	/**
	 * @param principal
	 *            the leading entity
	 * @param dependent
	 *            the dependent entity
	 */
	public ReferentialConstraint(Principal principal, Dependent dependent) {
		super(new Hashtable<String, String>());
		this.principal = principal;
		this.dependent = dependent;
	}

	public ReferentialConstraint() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @return the principal
	 */
	public Principal getPrincipal() {
		return principal;
	}

	/**
	 * @param principal
	 *            the principal to set
	 */
	public void setPrincipal(Principal principal) {
		this.principal = principal;
	}

	/**
	 * @return the dependent
	 */
	public Dependent getDependent() {
		return dependent;
	}

	/**
	 * @param dependent
	 *            the dependent to set
	 */
	public void setDependent(Dependent dependent) {
		this.dependent = dependent;
	}

	/**
	 * @return the documentation
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * @param documentation
	 *            the documentation to set
	 */
	public void setDocumentation(Documentation documentation) {
		this.documentation = documentation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((dependent == null) ? 0 : dependent.hashCode());
		result = prime * result
				+ ((documentation == null) ? 0 : documentation.hashCode());
		result = prime * result
				+ ((principal == null) ? 0 : principal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ReferentialConstraint other = (ReferentialConstraint) obj;
		if (dependent == null) {
			if (other.dependent != null) {
				return false;
			}
		} else if (!dependent.equals(other.dependent)) {
			return false;
		}
		if (documentation == null) {
			if (other.documentation != null) {
				return false;
			}
		} else if (!documentation.equals(other.documentation)) {
			return false;
		}
		if (principal == null) {
			if (other.principal != null) {
				return false;
			}
		} else if (!principal.equals(other.principal)) {
			return false;
		}
		return true;
	}
}
