/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.model.edmx.v3.expressions;

import java.util.Arrays;

/**
 * The edm:Collection expression enables a value to be obtained from zero or
 * more child expressions. The value calculated by the collection expression is
 * the collection of the values calculated by each of the child expressions. A
 * collection expression MUST contain zero or more child expressions. The values
 * of the child expressions MUST all be type compatible. Each child expression
 * MUST be a constant expression or an edm:Record expression.
 * 
 * 
 */
public class Collection {
	private Object[] childExpressions;

	/**
	 * Default constructor
	 */
	public Collection() {
		setChildExpressions(new Object[0]);
	}

	/**
	 * @return the childExpressions
	 */
	public Object[] getChildExpressions() {
		if (this.childExpressions != null) {
			return Arrays.copyOf(this.childExpressions,
					this.childExpressions.length);
		}
		return null;
	}

	/**
	 * @param childExpressions
	 *            the childExpressions to set
	 */
	public void setChildExpressions(Object[] childExpressions) {
		this.childExpressions = childExpressions;
	}
}
