/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.model.edmx.v3.expressions;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.edmx.v3.PropertyValue;
import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The edm:Record expression enables a new entity type or complex type instance
 * to be constructed. A record expression can specify a SimpleIdentifier or
 * QualifiedName value for the Type attribute. If no value is specified for the
 * type attribute, the type is derived from the expression context. If a value
 * is specified, the value MUST resolve to an entity type or complex type in the
 * entity model. A record expression MUST contain zero or more edm:PropertyValue
 * elements. For each non-nullable property of the record construct type an
 * <edm:PropertyValue> child element MUST be provided. For derived types this
 * rule applies only to properties directly defined by the derived type.
 * 
 * 
 */
public class Record extends BaseElement {
	private PropertyValue[] propertyValues;

	/**
	 * Default constructor
	 */
	public Record() {
		super(new Hashtable<java.lang.String, java.lang.String>());
		setPropertyValues(new PropertyValue[0]);
	}

	/**
	 * @return the propertyValues
	 */
	public PropertyValue[] getPropertyValues() {
		if (this.propertyValues != null) {
			return Arrays.copyOf(this.propertyValues,
					this.propertyValues.length);
		}
		return null;
	}

	/**
	 * @param propertyValues
	 *            the propertyValues to set
	 */
	public void setPropertyValues(PropertyValue[] propertyValues) {
		this.propertyValues = propertyValues;
	}

}
