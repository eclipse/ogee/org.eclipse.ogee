/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The End element defines the two sides of an Association and AssociationSet.
 * This association is defined between the two EntitySets declared in an
 * EntitySet attribute.
 * 
 * 
 */
public class End extends BaseElement {
	private String role;
	private String type;
	private String multiplicity;
	private String entitySet;
	private Documentation documentation;

	/**
	 * Default constructor
	 */
	public End() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(String role) {
		this.role = role;
		this.setAttribute(ROLE, role);
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
		this.setAttribute(TYPE, type);
	}

	/**
	 * @return the multiplicity
	 */
	public String getMultiplicity() {
		return multiplicity;
	}

	/**
	 * @param multiplicity
	 *            the multiplicity to set
	 */
	public void setMultiplicity(String multiplicity) {
		this.multiplicity = multiplicity;
		this.setAttribute(MULTIPLICITY, multiplicity);
	}

	/**
	 * @return the entitySet
	 */
	public String getEntitySet() {
		return entitySet;
	}

	/**
	 * @param entitySet
	 *            the entitySet to set
	 */
	public void setEntitySet(String entitySet) {
		this.entitySet = entitySet;
		this.setAttribute(ENTITY_SET, entitySet);
	}

	/**
	 * @return the documentation
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * @param documentation
	 *            the documentation to set
	 */
	public void setDocumentation(Documentation documentation) {
		this.documentation = documentation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((documentation == null) ? 0 : documentation.hashCode());
		result = prime * result
				+ ((entitySet == null) ? 0 : entitySet.hashCode());
		result = prime * result
				+ ((multiplicity == null) ? 0 : multiplicity.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		End other = (End) obj;
		if (documentation == null) {
			if (other.documentation != null) {
				return false;
			}
		} else if (!documentation.equals(other.documentation)) {
			return false;
		}
		if (entitySet == null) {
			if (other.entitySet != null) {
				return false;
			}
		} else if (!entitySet.equals(other.entitySet)) {
			return false;
		}
		if (multiplicity == null) {
			if (other.multiplicity != null) {
				return false;
			}
		} else if (!multiplicity.equals(other.multiplicity)) {
			return false;
		}
		if (role == null) {
			if (other.role != null) {
				return false;
			}
		} else if (!role.equals(other.role)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}
}
