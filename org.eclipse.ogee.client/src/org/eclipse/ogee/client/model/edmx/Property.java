/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.model.generic.StringUtilities;

/**
 * The declared properties of an EntityType or ComplexType are defined using the
 * Property element. EntityType and ComplexType may have one or more Property
 * elements. Property can be an EDMSimpleType or ComplexType. A declared
 * property description consists of its name, type and a set of facets, such as
 * Nullable or Default.
 * 
 * 
 */
public class Property extends BaseElement {
	private String name;
	private String type;
	private boolean nullable;
	private String maxLength;
	private String fixedLength;
	private String unicode;
	private Documentation documentation;
	private String mFC_TargetPath;
	private boolean mFC_KeepInContent;

	/**
	 * Default constructor
	 */
	public Property() {
		super(new Hashtable<String, String>());
		this.nullable = true;
	}

	/**
	 * @return the mFC_TargetPath
	 */
	public String getmFC_TargetPath() {
		return mFC_TargetPath;
	}

	/**
	 * @param mFCTargetPath
	 *            the mFC_TargetPath to set
	 */
	public void setmFC_TargetPath(String mFCTargetPath) {
		mFC_TargetPath = mFCTargetPath;
		this.setAttribute(M_FC_TARGET_PATH, mFCTargetPath);
	}

	/**
	 * @return the mFC_KeepInContent
	 */
	public boolean ismFC_KeepInContent() {
		return mFC_KeepInContent;
	}

	/**
	 * @param mFCKeepInContent
	 *            the mFC_KeepInContent to set
	 */
	public void setmFC_KeepInContent(boolean mFCKeepInContent) {
		mFC_KeepInContent = mFCKeepInContent;
		this.setAttribute(M_FC_KEEP_IN_CONTENT,
				StringUtilities.stringValueOf(mFCKeepInContent));
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
		this.setAttribute(NAME, name);
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
		this.setAttribute(TYPE, type);
	}

	/**
	 * @return the nullable
	 */
	public boolean isNullable() {
		return nullable;
	}

	/**
	 * @param nullable
	 *            the nullable to set
	 */
	public void setNullable(boolean nullable) {
		this.nullable = nullable;
		this.setAttribute(NULLABLE, StringUtilities.stringValueOf(nullable));
	}

	/**
	 * @return the maxLength
	 */
	public String getMaxLength() {
		return maxLength;
	}

	/**
	 * @param maxLength
	 *            the maxLength to set
	 */
	public void setMaxLength(String maxLength) {
		this.maxLength = maxLength;
		this.setAttribute(MAX_LENGTH, maxLength);
	}

	/**
	 * @return the fixedLength
	 */
	public String getFixedLength() {
		return fixedLength;
	}

	/**
	 * @param fixedLength
	 *            the fixedLength to set
	 */
	public void setFixedLength(String fixedLength) {
		this.fixedLength = fixedLength;
		this.setAttribute(FIXED_LENGTH, fixedLength);
	}

	/**
	 * @return the unicode
	 */
	public String getUnicode() {
		return unicode;
	}

	/**
	 * @param unicode
	 *            the unicode to set
	 */
	public void setUnicode(String unicode) {
		this.unicode = unicode;
		this.setAttribute(UNICODE, unicode);
	}

	/**
	 * @return the documentation
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * @param documentation
	 *            the documentation to set
	 */
	public void setDocumentation(Documentation documentation) {
		this.documentation = documentation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((documentation == null) ? 0 : documentation.hashCode());
		result = prime * result
				+ ((fixedLength == null) ? 0 : fixedLength.hashCode());		
		result = prime * result + (mFC_KeepInContent ? 1231 : 1237);
		result = prime * result
				+ ((mFC_TargetPath == null) ? 0 : mFC_TargetPath.hashCode());
		result = prime * result
				+ ((maxLength == null) ? 0 : maxLength.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (nullable ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((unicode == null) ? 0 : unicode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Property other = (Property) obj;
		if (documentation == null) {
			if (other.documentation != null) {
				return false;
			}
		} else if (!documentation.equals(other.documentation)) {
			return false;
		}
		if (fixedLength == null) {
			if (other.fixedLength != null) {
				return false;
			}
		} else if (!fixedLength.equals(other.fixedLength)) {
			return false;
		}
		if (mFC_KeepInContent != other.mFC_KeepInContent) {
			return false;
		}
		if (mFC_TargetPath == null) {
			if (other.mFC_TargetPath != null) {
				return false;
			}
		} else if (!mFC_TargetPath.equals(other.mFC_TargetPath)) {
			return false;
		}
		if (maxLength == null) {
			if (other.maxLength != null) {
				return false;
			}
		} else if (!maxLength.equals(other.maxLength)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (nullable != other.nullable) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		if (unicode == null) {
			if (other.unicode != null) {
				return false;
			}
		} else if (!unicode.equals(other.unicode)) {
			return false;
		}
		return true;
	}
}
