/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v3;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.edmx.v3.expressions.Collection;
import org.eclipse.ogee.client.model.edmx.v3.expressions.Path;
import org.eclipse.ogee.client.model.edmx.v3.expressions.Record;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Binary;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTime;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTimeOffset;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Guid;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String;
import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * Represents an expression element.
 */
public class BaseExpressionElement extends BaseElement {
	private String String;
	private Int Int;
	private Float Float;
	private Decimal Decimal;
	private Bool Bool;
	private DateTime DateTime;
	private DateTimeOffset DateTimeOffset;
	private Binary Binary;
	private Guid Guid;
	private org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte Byte;
	private Path Path;
	private Record record;
	private Collection collection;

	/**
	 * Constructs a new base expression element.
	 */
	public BaseExpressionElement() {
		super(new Hashtable<java.lang.String, java.lang.String>());
	}

	/**
	 * Returns the path of the element.
	 * 
	 * @return the path
	 */
	public Path getPath() {
		if (this.Path == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.Path.PATH);
			if (attribute != null) {
				this.Path = new Path();
				this.Path.setValue(attribute);
			}
		}
		return this.Path;
	}

	/**
	 * Sets the path of the element.
	 * 
	 * @param path
	 *            the path to set
	 */
	public void setPath(Path path) {
		Path = path;
	}

	/**
	 * Returns the string of the element.
	 * 
	 * @return the string
	 */
	public String getString() {
		if (this.String == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String.STRING);
			if (attribute != null) {
				this.String = new String();
				this.String.setValue(attribute);
			}
		}
		return this.String;
	}

	/**
	 * Sets the string of the element.
	 * 
	 * @param string
	 *            the string to set
	 */
	public void setString(String string) {
		String = string;
	}

	/**
	 * Returns the int of the element.
	 * 
	 * @return the int of the element.
	 */
	public Int getInt() {
		if (this.Int == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int.INT);
			if (attribute != null) {
				this.Int = new Int();
				this.Int.setValue(attribute);
			}
		}
		return this.Int;
	}

	/**
	 * Sets the int of the element.
	 * 
	 * @param i
	 *            - the int to set
	 */
	public void setInt(Int i) {
		Int = i;
	}

	/**
	 * Returns the float of the element.
	 * 
	 * @return the float
	 */
	public Float getFloat() {
		if (this.Float == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float.FLOAT);
			if (attribute != null) {
				this.Float = new Float();
				this.Float.setValue(attribute);
			}
		}
		return this.Float;
	}

	/**
	 * Sets the float of the element.
	 * 
	 * @param f
	 *            - the float to set
	 */
	public void setFloat(Float f) {
		Float = f;
	}

	/**
	 * Returns the decimal of the element.
	 * 
	 * @return the decimal
	 */
	public Decimal getDecimal() {
		if (this.Decimal == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal.DECIMAL);
			if (attribute != null) {
				this.Decimal = new Decimal();
				this.Decimal.setValue(attribute);
			}
		}
		return this.Decimal;
	}

	/**
	 * Sets the decimal of the element.
	 * 
	 * @param decimal
	 *            the decimal to set
	 */
	public void setDecimal(Decimal decimal) {
		Decimal = decimal;
	}

	/**
	 * Returns the bool of the element.
	 * 
	 * @return the bool
	 */
	public Bool getBool() {
		if (this.Bool == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool.BOOL);
			if (attribute != null) {
				this.Bool = new Bool();
				this.Bool.setValue(attribute);
			} else {
				attribute = this
						.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool.BOOLEAN);
				if (attribute != null) {
					this.Bool = new Bool();
					this.Bool.setValue(attribute);
				}
			}
		}
		return this.Bool;
	}

	/**
	 * Sets the bool of the element.
	 * 
	 * @param bool
	 *            the bool to set
	 */
	public void setBool(Bool bool) {
		Bool = bool;
	}

	/**
	 * Returns the date time of the element.
	 * 
	 * @return the dateTime
	 */
	public DateTime getDateTime() {
		if (this.DateTime == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTime.DATETIME);
			if (attribute != null) {
				this.DateTime = new DateTime();
				this.DateTime.setValue(attribute);
			}
		}
		return this.DateTime;
	}

	/**
	 * Sets the date time of the element.
	 * 
	 * @param dateTime
	 *            the dateTime to set
	 */
	public void setDateTime(DateTime dateTime) {
		DateTime = dateTime;
	}

	/**
	 * Returns the record of the element.
	 * 
	 * @return the record
	 */
	public Record getRecord() {
		return record;
	}

	/**
	 * Sets the record of the element.
	 * 
	 * @param record
	 *            the record to set
	 */
	public void setRecord(Record record) {
		this.record = record;
	}

	/**
	 * Returns the collection of the element.
	 * 
	 * @return the collection
	 */
	public Collection getCollection() {
		return collection;
	}

	/**
	 * Sets the collection of the element.
	 * 
	 * @param collection
	 *            the collection to set
	 */
	public void setCollection(Collection collection) {
		this.collection = collection;
	}

	/**
	 * Returns the date time offset of the element.
	 * 
	 * @return the dateTimeOffset
	 */
	public DateTimeOffset getDateTimeOffset() {
		if (this.DateTimeOffset == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTimeOffset.DATETIMEOFFSET);
			if (attribute != null) {
				this.DateTimeOffset = new DateTimeOffset();
				this.DateTimeOffset.setValue(attribute);
			}
		}
		return this.DateTimeOffset;
	}

	/**
	 * Sets the date time offset of the element.
	 * 
	 * @param dateTimeOffset
	 *            the dateTimeOffset to set
	 */
	public void setDateTimeOffset(DateTimeOffset dateTimeOffset) {
		DateTimeOffset = dateTimeOffset;
	}

	/**
	 * Returns the binary of the element.
	 * 
	 * @return the binary
	 */
	public Binary getBinary() {
		if (this.Binary == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Binary.BINARY);
			if (attribute != null) {
				this.Binary = new Binary();
				this.Binary.setValue(attribute);
			}
		}
		return this.Binary;
	}

	/**
	 * Sets the binary of the element.
	 * 
	 * @param binary
	 *            the binary to set
	 */
	public void setBinary(Binary binary) {
		Binary = binary;
	}

	/**
	 * Returns the giud of the element.
	 * 
	 * @return the guid
	 */
	public Guid getGuid() {
		if (this.Guid == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Guid.GUID);
			if (attribute != null) {
				this.Guid = new Guid();
				this.Guid.setValue(attribute);
			}
		}
		return this.Guid;
	}

	/**
	 * Sets the guid of the element.
	 * 
	 * @param guid
	 *            the guid to set
	 */
	public void setGuid(Guid guid) {
		Guid = guid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((Binary == null) ? 0 : Binary.hashCode());
		result = prime * result + ((Bool == null) ? 0 : Bool.hashCode());
		result = prime * result
				+ ((DateTime == null) ? 0 : DateTime.hashCode());
		result = prime * result
				+ ((DateTimeOffset == null) ? 0 : DateTimeOffset.hashCode());
		result = prime * result + ((Decimal == null) ? 0 : Decimal.hashCode());
		result = prime * result + ((Float == null) ? 0 : Float.hashCode());
		result = prime * result + ((Guid == null) ? 0 : Guid.hashCode());
		result = prime * result + ((Int == null) ? 0 : Int.hashCode());
		result = prime * result + ((Path == null) ? 0 : Path.hashCode());
		result = prime * result + ((String == null) ? 0 : String.hashCode());
		result = prime * result
				+ ((collection == null) ? 0 : collection.hashCode());
		result = prime * result + ((record == null) ? 0 : record.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BaseExpressionElement other = (BaseExpressionElement) obj;
		if (Binary == null) {
			if (other.Binary != null) {
				return false;
			}
		} else if (!Binary.equals(other.Binary)) {
			return false;
		}
		if (Bool == null) {
			if (other.Bool != null) {
				return false;
			}
		} else if (!Bool.equals(other.Bool)) {
			return false;
		}
		if (DateTime == null) {
			if (other.DateTime != null) {
				return false;
			}
		} else if (!DateTime.equals(other.DateTime)) {
			return false;
		}
		if (DateTimeOffset == null) {
			if (other.DateTimeOffset != null) {
				return false;
			}
		} else if (!DateTimeOffset.equals(other.DateTimeOffset)) {
			return false;
		}
		if (Decimal == null) {
			if (other.Decimal != null) {
				return false;
			}
		} else if (!Decimal.equals(other.Decimal)) {
			return false;
		}
		if (Float == null) {
			if (other.Float != null) {
				return false;
			}
		} else if (!Float.equals(other.Float)) {
			return false;
		}
		if (Guid == null) {
			if (other.Guid != null) {
				return false;
			}
		} else if (!Guid.equals(other.Guid)) {
			return false;
		}
		if (Int == null) {
			if (other.Int != null) {
				return false;
			}
		} else if (!Int.equals(other.Int)) {
			return false;
		}
		if (Path == null) {
			if (other.Path != null) {
				return false;
			}
		} else if (!Path.equals(other.Path)) {
			return false;
		}
		if (String == null) {
			if (other.String != null) {
				return false;
			}
		} else if (!String.equals(other.String)) {
			return false;
		}
		if (collection == null) {
			if (other.collection != null) {
				return false;
			}
		} else if (!collection.equals(other.collection)) {
			return false;
		}
		if (record == null) {
			if (other.record != null) {
				return false;
			}
		} else if (!record.equals(other.record)) {
			return false;
		}
		return true;
	}

	/**
	 * Return the byte of the element.
	 * 
	 * @return the byte
	 */
	public org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte getByte() {
		if (this.Byte == null) {
			java.lang.String attribute = this
					.getAttribute(org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte.BYTE);
			if (attribute != null) {
				this.Byte = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte();
				this.Byte.setValue(attribute);
			}
		}
		return this.Byte;
	}

	/**
	 * Sets the byte of the element.
	 * 
	 * @param _byte
	 *            the byte to set
	 */
	public void setByte(
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte _byte) {
		Byte = _byte;
	}
}
