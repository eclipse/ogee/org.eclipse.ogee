/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v3;

import java.util.Arrays;

import org.eclipse.ogee.client.model.edmx.FunctionImport;

/**
 * Actions are translated into OData as function imports with a name consisting
 * of the associated entity type plus the name of the action, and the key
 * properties of entity type plus the parameters of the actions as function
 * parameters.
 * 
 * The FunctionImport element is used to import Stored Procedures or Functions
 * defined in Store Schema Model into Entity Data Model.
 * 
 * 
 */
public class FunctionImportV3 extends FunctionImport {
	// A FI that may have side effects represents an Action.
	// default value: "true".
	private boolean IsSideEffecting;

	// represents the fact that this FI can be bound to an Entity Type
	// or collection of Entity Types in OData V3.
	// If this attribute is set to true,
	// the FI MUST contain at least one edm:Parameter element,
	// and the first such parameter must represent an entity or collection of
	// entities.
	// default value: "false".
	private boolean isBindable;

	// In CSDL 3.0, FunctionImport can contain any number of TypeAnnotation
	// elements.
	private TypeAnnotation[] typeAnnotations;
	// In CSDL 3.0, FunctionImport can contain any number of ValueAnnotation
	// elements.
	private ValueAnnotation[] valueAnnotations;

	private FunctionImportReturnType returnTypeElement;

	/**
	 * Constructs a new function import.
	 */
	public FunctionImportV3() {
		super();

		this.typeAnnotations = new TypeAnnotation[0];
		this.valueAnnotations = new ValueAnnotation[0];
	}

	/**
	 * Returns whether this function import has side effecting.
	 * 
	 * @return true whether this function import has side effecting, and false
	 *         otherwise.
	 */
	public boolean isIsSideEffecting() {
		return IsSideEffecting;
	}

	/**
	 * Sets whether this function import has side effecting.
	 * 
	 * @param isSideEffecting
	 *            - the boolean value to set.
	 */
	public void setIsSideEffecting(boolean isSideEffecting) {
		IsSideEffecting = isSideEffecting;
	}

	/**
	 * Returns whether this function import is bound to an entity type.
	 * 
	 * @return true whether this function import is bound to an entity type, and
	 *         false otherwise.
	 */
	public boolean isBindable() {
		return isBindable;
	}

	/**
	 * Sets whether this function import is bound to an entity type.
	 * 
	 * @param isBindable
	 *            - the boolean value to set.
	 */
	public void setBindable(boolean isBindable) {
		this.isBindable = isBindable;
	}

	/**
	 * Returns the type annotations of this function import.
	 * 
	 * @return the type annotations of this function import.
	 */
	public TypeAnnotation[] getTypeAnnotations() {
		if (this.typeAnnotations != null) {
			return Arrays.copyOf(this.typeAnnotations,
					this.typeAnnotations.length);
		}
		return null;
	}

	/**
	 * Sets the type annotations of this function import.
	 * 
	 * @param typeAnnotations
	 *            - the type annotations to set
	 */
	public void setTypeAnnotations(TypeAnnotation[] typeAnnotations) {
		this.typeAnnotations = typeAnnotations;
	}

	/**
	 * Returns the value annotations of this function import.
	 * 
	 * @return the value annotations of this function import.
	 */
	public ValueAnnotation[] getValueAnnotations() {
		if (this.valueAnnotations != null) {
			return Arrays.copyOf(this.valueAnnotations,
					this.valueAnnotations.length);
		}
		return null;
	}

	/**
	 * Sets the value annotations of this function import.
	 * 
	 * @param valueAnnotations
	 *            - the value annotations to set
	 */
	public void setValueAnnotations(ValueAnnotation[] valueAnnotations) {
		this.valueAnnotations = valueAnnotations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (IsSideEffecting ? 1231 : 1237);
		result = prime * result + (isBindable ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(typeAnnotations);
		result = prime * result + Arrays.hashCode(valueAnnotations);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FunctionImportV3 other = (FunctionImportV3) obj;
		if (IsSideEffecting != other.IsSideEffecting) {
			return false;
		}
		if (isBindable != other.isBindable) {
			return false;
		}
		if (!Arrays.equals(typeAnnotations, other.typeAnnotations)) {
			return false;
		}
		if (!Arrays.equals(valueAnnotations, other.valueAnnotations)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the returnTypeElement
	 */
	public FunctionImportReturnType getReturnTypeElement() {
		return returnTypeElement;
	}

	/**
	 * @param returnTypeElement
	 *            the returnTypeElement to set
	 */
	public void setReturnTypeElement(FunctionImportReturnType returnTypeElement) {
		this.returnTypeElement = returnTypeElement;
	}
}
