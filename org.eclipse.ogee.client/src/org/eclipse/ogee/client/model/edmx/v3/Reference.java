/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.model.edmx.v3;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.model.generic.v3.ODataConstantsV3;

/**
 * 
 * The edmx:Reference element is used to reference another EDMX document or an
 * Entity Data Model (EDM) conceptual schema.
 * 
 * 
 */
public class Reference extends BaseElement {
	String url;

	public Reference() {
		super(new Hashtable<String, String>());
	}

	/**
	 * @param url
	 *            the url to set. Url specifies a URI that resolves to the
	 *            referenced EDMX document or to the EDM conceptual schema. Url
	 *            MUST be an absolute URL.
	 */
	public void setUrl(String url) {
		this.url = url;
		this.setAttribute(ODataConstantsV3.URL, url);
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Reference other = (Reference) obj;
		if (url == null) {
			if (other.url != null) {
				return false;
			}
		} else if (!url.equals(other.url)) {
			return false;
		}
		return true;
	}

}
