/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * A ReferentialConstraint must specify which of the ends is the Principal Role
 * and which of the ends is the Dependent Role for the ReferentialConstraint.
 * The two entity types are in a Principal-to-Dependent relationship, which can
 * also be thought of as a type of parent-child relationship. When entities are
 * related by an Association that specifies a ReferentialConstraint between the
 * keys of the two entities then the dependent entity object cannot exist
 * without a valid relationship to a parent entity object.
 * 
 * 
 */
public class Principal extends BaseElement {
	private String role;
	private PropertyRef[] propertyRefs;

	/**
	 * Default constructor
	 */
	public Principal() {
		super(new Hashtable<String, String>());
		setPropertyRefs(new PropertyRef[0]);
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(String role) {
		this.role = role;
		this.setAttribute(ROLE, role);

	}

	public void setPropertyRefs(PropertyRef[] propertyRefs) {
		this.propertyRefs = propertyRefs;
	}

	public PropertyRef[] getPropertyRefs() {
		if (this.propertyRefs == null) {
			return new PropertyRef[0];
		}
		return Arrays.copyOf(this.propertyRefs, this.propertyRefs.length);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(propertyRefs);
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Principal other = (Principal) obj;
		if (!Arrays.equals(propertyRefs, other.propertyRefs)) {
			return false;
		}
		if (role == null) {
			if (other.role != null) {
				return false;
			}
		} else if (!role.equals(other.role)) {
			return false;
		}
		return true;
	}
}
