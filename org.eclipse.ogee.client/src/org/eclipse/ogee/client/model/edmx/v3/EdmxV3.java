/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v3;

import java.util.Arrays;

import org.eclipse.ogee.client.model.edmx.Edmx;

/**
 * EDMX stands for Entity Data Model Extensions. Every EDMX document must have a
 * single "edmx:EdmxV3" element. When used to package the service metadata for a
 * Data Service the "edmx:EdmxV3" element must contain exactly one
 * "edmx:DataServices" element.
 * 
 * 
 */
public class EdmxV3 extends Edmx {
	// The edmx:Edmx element MAY contain one or more edmx:Reference subelements
	private Reference[] references;

	/**
	 * Constructs a new Edmx.
	 */
	public EdmxV3() {
		super();
		this.references = new Reference[0];
	}

	/**
	 * Returns the references of this edmx.
	 * 
	 * @return the references of this edmx.
	 */
	public Reference[] getReferences() {
		if (this.references != null) {
			return Arrays.copyOf(this.references, this.references.length);
		}
		return null;
	}

	/**
	 * Sets the references of this edmx.
	 * 
	 * @param references
	 *            - the references to set
	 */
	public void setReferences(Reference[] references) {
		this.references = references;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(references);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EdmxV3 other = (EdmxV3) obj;
		if (!Arrays.equals(references, other.references)) {
			return false;
		}
		return true;
	}
}
