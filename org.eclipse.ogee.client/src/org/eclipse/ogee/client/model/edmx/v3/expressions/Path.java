/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.model.edmx.v3.expressions;

/**
 * The Path expression element is used to refer to model elements. A Path
 * expression may resolve to the following: A property of an object An enum
 * constant An entity set A navigation property
 * 
 * A Path expression element may refer to any number of navigation properties
 * that represent an arbitrary depth. Furthermore, a Path expression element
 * that refers to a navigation property with a cardinality greater than 1 refers
 * to a collection.
 * 
 */
public class Path {
	public static final java.lang.String PATH = "Path"; //$NON-NLS-1$

	private String value;

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
