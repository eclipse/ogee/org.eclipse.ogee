/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v3;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * A Member element is used inside an EnumType element to declare a member of an
 * enumeration type.
 */
public class Member extends BaseElement {
	// elements MUST specify a Name attribute. The Name must be unique within
	// the EnumType declaration.
	private String name;

	// elements can specify the Value attribute. Value MUST be a valid value of
	// underlyingType.
	private long value;

	/**
	 * Constructs a new member.
	 */
	public Member() {
		super(new Hashtable<String, String>());
	}

	/**
	 * Returns the name of this member.
	 * 
	 * @return the name of this member.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of this member.
	 * 
	 * @param name
	 *            - the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the value of this member.
	 * 
	 * @return the value of this member.
	 */
	public long getValue() {
		return value;
	}

	/**
	 * Sets the value of this member.
	 * 
	 * @param value
	 *            - the value to set
	 */
	public void setValue(long value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (int) (value ^ (value >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Member other = (Member) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (value != other.value) {
			return false;
		}
		return true;
	}
}
