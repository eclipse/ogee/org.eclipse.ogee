/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v4;

import org.eclipse.ogee.client.model.edmx.Edmx;

public class EdmxV4 extends Edmx {
	// The Edmx element contains zero or more edmx:Reference elements.
	// Reference elements specify the location of schemas used by the OData
	// service.
	private EdmxReference[] edmxReferences;

	/**
	 * @return the edmxReferences
	 */
	public EdmxReference[] getEdmxReferences() {
		if (edmxReferences == null) {
			edmxReferences = new EdmxReference[0];
		}
		return edmxReferences;
	}

	/**
	 * @param edmxReferences
	 *            the edmxReferences to set
	 */
	public void setEdmxReferences(EdmxReference[] edmxReferences) {
		this.edmxReferences = edmxReferences;
	}

}
