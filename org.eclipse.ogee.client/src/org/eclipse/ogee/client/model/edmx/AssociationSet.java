/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * An AssociationSet contains relationship instances of the specified
 * AssociationType. The AssociationType specifies the EntityTypes of the two end
 * points while AssociationSet specifies the EntitySet that corresponds to
 * either these EntityTypes directly, or to derived EntityTypes. The
 * association-instances contained in the AssociationSet relate entity instances
 * belonging to these EntityTypes.
 * 
 * 
 */
public class AssociationSet extends BaseElement {
	private String name;
	private String association;
	private End[] ends; // tuple ends;
	private Documentation documentation;

	/**
	 * Default constructor
	 */
	public AssociationSet() {
		super(new Hashtable<String, String>());
		ends = new End[0];
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
		this.setAttribute(NAME, name);
	}

	/**
	 * @return the association
	 */
	public String getAssociation() {
		return association;
	}

	/**
	 * @param association
	 *            the association to set
	 */
	public void setAssociation(String association) {
		this.association = association;
		this.setAttribute(ASSOCIATION, association);
	}

	/**
	 * @return the ends
	 */
	public End[] getEnds() {
		if (this.ends != null) {
			return Arrays.copyOf(this.ends, this.ends.length);
		}
		return null;

	}

	/**
	 * @param ends
	 *            the ends to set
	 */
	public void setEnds(End[] ends) {
		this.ends = ends;
	}

	/**
	 * @return the documentation
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * @param documentation
	 *            the documentation to set
	 */
	public void setDocumentation(Documentation documentation) {
		this.documentation = documentation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((association == null) ? 0 : association.hashCode());
		result = prime * result
				+ ((documentation == null) ? 0 : documentation.hashCode());
		result = prime * result + Arrays.hashCode(ends);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AssociationSet other = (AssociationSet) obj;
		if (association == null) {
			if (other.association != null) {
				return false;
			}
		} else if (!association.equals(other.association)) {
			return false;
		}
		if (documentation == null) {
			if (other.documentation != null) {
				return false;
			}
		} else if (!documentation.equals(other.documentation)) {
			return false;
		}
		if (!Arrays.equals(ends, other.ends)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
