/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx.v3;

import java.util.Arrays;

import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.Schema;

/**
 * The Schema is the top-level CSDL construct that allows creation of namespace.
 * The contents of a namespace may be defined by one or more Schema instances.
 * 
 * 
 */
public class SchemaV3 extends Schema {
	private Documentation documentation;
	private EnumType[] enumTypes;
	// In CSDL 3.0, Schema can contain any number of ValueTerm elements.
	private ValueTerm[] valueTerms;
	// In CSDL 3.0, Schema can contain any number of Annotations elements.
	private Annotations[] annotations;

	/**
	 * Default constructor
	 */
	public SchemaV3() {
		super();
		enumTypes = new EnumType[0];
		setValueTerms(new ValueTerm[0]);
		annotations = new Annotations[0];
	}

	/**
	 * @return the documentation
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * @param documentation
	 *            the documentation to set
	 */
	public void setDocumentation(Documentation documentation) {
		this.documentation = documentation;
	}

	/**
	 * @return the enumTypes
	 */
	public EnumType[] getEnumTypes() {
		if (this.enumTypes != null) {
			return Arrays.copyOf(this.enumTypes, this.enumTypes.length);
		}
		return null;
	}

	/**
	 * @param enumTypes
	 *            the enumTypes to set
	 */
	public void setEnumTypes(EnumType[] enumTypes) {
		this.enumTypes = enumTypes;
	}

	/**
	 * @return the valueTerms
	 */
	public ValueTerm[] getValueTerms() {
		if (this.valueTerms != null) {
			return Arrays.copyOf(this.valueTerms, this.valueTerms.length);
		}
		return null;
	}

	/**
	 * @param valueTerms
	 *            the valueTerms to set
	 */
	public void setValueTerms(ValueTerm[] valueTerms) {
		this.valueTerms = valueTerms;
	}

	/**
	 * @return the annotations
	 */
	public Annotations[] getAnnotations() {
		if (this.annotations != null) {
			return Arrays.copyOf(this.annotations, this.annotations.length);
		}
		return null;
	}

	/**
	 * @param annotations
	 *            the annotations to set
	 */
	public void setAnnotations(Annotations[] annotations) {
		this.annotations = annotations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(annotations);
		result = prime * result
				+ ((documentation == null) ? 0 : documentation.hashCode());
		result = prime * result + Arrays.hashCode(enumTypes);
		result = prime * result + Arrays.hashCode(valueTerms);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SchemaV3 other = (SchemaV3) obj;
		if (!Arrays.equals(annotations, other.annotations)) {
			return false;
		}
		if (documentation == null) {
			if (other.documentation != null) {
				return false;
			}
		} else if (!documentation.equals(other.documentation)) {
			return false;
		}
		if (!Arrays.equals(enumTypes, other.enumTypes)) {
			return false;
		}
		if (!Arrays.equals(valueTerms, other.valueTerms)) {
			return false;
		}
		return true;
	}
}
