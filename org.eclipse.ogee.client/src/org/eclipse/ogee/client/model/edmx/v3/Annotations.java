/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.model.edmx.v3;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The Annotations element is used to group one or more TypeAnnotation or
 * ValueAnnotation elements that target the same model element.
 * 
 * 
 */
public class Annotations extends BaseElement {
	// element MUST have a Target attribute defined.
	private String target;
	// Annotations MUST contain one or more TypeAnnotation or ValueAnnotation
	// elements.
	private ValueAnnotation[] valueAnnotations;
	private TypeAnnotation[] typeAnnotations;

	/**
	 * Default Constructor
	 */
	public Annotations() {
		super(new Hashtable<String, String>());
		valueAnnotations = new ValueAnnotation[0];
		typeAnnotations = new TypeAnnotation[0];
	}

	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @param target
	 *            the target to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * @return the valueAnnotations
	 */
	public ValueAnnotation[] getValueAnnotations() {
		if (this.valueAnnotations != null) {
			return Arrays.copyOf(this.valueAnnotations,
					this.valueAnnotations.length);
		}
		return null;
	}

	/**
	 * @param valueAnnotations
	 *            the valueAnnotations to set
	 */
	public void setValueAnnotations(ValueAnnotation[] valueAnnotations) {
		this.valueAnnotations = valueAnnotations;
	}

	/**
	 * @return the typeAnnotations
	 */
	public TypeAnnotation[] getTypeAnnotations() {
		if (this.typeAnnotations != null) {
			return Arrays.copyOf(this.typeAnnotations,
					this.typeAnnotations.length);
		}
		return null;
	}

	/**
	 * @param typeAnnotations
	 *            the typeAnnotations to set
	 */
	public void setTypeAnnotations(TypeAnnotation[] typeAnnotations) {
		this.typeAnnotations = typeAnnotations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		result = prime * result + Arrays.hashCode(typeAnnotations);
		result = prime * result + Arrays.hashCode(valueAnnotations);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Annotations other = (Annotations) obj;
		if (target == null) {
			if (other.target != null) {
				return false;
			}
		} else if (!target.equals(other.target)) {
			return false;
		}
		if (!Arrays.equals(typeAnnotations, other.typeAnnotations)) {
			return false;
		}
		if (!Arrays.equals(valueAnnotations, other.valueAnnotations)) {
			return false;
		}
		return true;
	}

}
