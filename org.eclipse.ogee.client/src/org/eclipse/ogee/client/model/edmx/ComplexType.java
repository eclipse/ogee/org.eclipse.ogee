/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edmx;

import java.util.Arrays;
import java.util.Hashtable;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.model.generic.StringUtilities;

/**
 * Complex Types are structured types also consisting of a list of properties
 * but with no key, and thus can only exist as a property of a containing entity
 * or as a temporary value.
 * 
 * 
 */
public class ComplexType extends BaseElement {
	private String name;
	private Property[] properties;
	private String baseType;
	private boolean isAbstract;
	private Documentation documentation;

	/**
	 * Default constructor
	 */
	public ComplexType() {
		super(new Hashtable<String, String>());
		properties = new Property[0];
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
		this.setAttribute(NAME, name);
	}

	/**
	 * @return the properties
	 */
	public Property[] getProperties() {
		if (this.properties != null) {
			return Arrays.copyOf(this.properties, this.properties.length);
		}
		return null;

	}

	/**
	 * @return the property by name
	 */
	public Property getProperty(String name) {
		for (Property property : properties) {
			if (property.getName().equals(name)) {
				return property;
			}
		}
		return null;
	}

	/**
	 * @param properties
	 *            the properties to set
	 */
	public void setProperties(Property[] properties) {
		this.properties = properties;
	}

	/**
	 * @return the base type
	 */
	public String getBaseType() {
		return baseType;
	}

	/**
	 * @param baseType
	 *            the base type to set
	 */
	public void setBaseType(String baseType) {
		this.baseType = baseType;
		this.setAttribute(BASE_TYPE, baseType);
	}

	/**
	 * @return is abstract
	 */
	public boolean isAbstract() {
		return this.isAbstract;
	}

	/**
	 * @param isAbstract
	 *            is abstract to set
	 */
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract;
		this.setAttribute(ABSTRACT, StringUtilities.stringValueOf(isAbstract));
	}

	/**
	 * @return the documentation
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * @param documentation
	 *            the documentation to set
	 */
	public void setDocumentation(Documentation documentation) {
		this.documentation = documentation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((baseType == null) ? 0 : baseType.hashCode());
		result = prime * result
				+ ((documentation == null) ? 0 : documentation.hashCode());
		result = prime * result + (isAbstract ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Arrays.hashCode(properties);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ComplexType other = (ComplexType) obj;
		if (baseType == null) {
			if (other.baseType != null) {
				return false;
			}
		} else if (!baseType.equals(other.baseType)) {
			return false;
		}
		if (documentation == null) {
			if (other.documentation != null) {
				return false;
			}
		} else if (!documentation.equals(other.documentation)) {
			return false;
		}
		if (isAbstract != other.isAbstract) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (!Arrays.equals(properties, other.properties)) {
			return false;
		}
		return true;
	}
}
