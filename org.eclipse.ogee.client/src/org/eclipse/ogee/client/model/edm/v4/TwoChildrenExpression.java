/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TwoChildrenExpression complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TwoChildrenExpression">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://docs.oasis-open.org/odata/ns/edm}Annotation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;group ref="{http://docs.oasis-open.org/odata/ns/edm}GExpression" maxOccurs="2" minOccurs="2"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TwoChildrenExpression", propOrder = { "annotation",
		"gExpression" })
public class TwoChildrenExpression {

	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;
	@XmlElementRefs({
			@XmlElementRef(name = "Lt", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Not", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Decimal", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "LabeledElement", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Collection", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Le", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "LabeledElementReference", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Eq", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Cast", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Guid", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "String", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "AnnotationPath", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Path", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Binary", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "And", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "NavigationPropertyPath", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "EnumMember", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "TimeOfDay", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Bool", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "If", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "IsOf", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Int", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Float", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "UrlRef", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Ge", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Or", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Ne", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Duration", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Apply", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Gt", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Date", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "PropertyPath", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Record", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "DateTimeOffset", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class),
			@XmlElementRef(name = "Null", namespace = "http://docs.oasis-open.org/odata/ns/edm", type = JAXBElement.class) })
	protected List<JAXBElement<?>> gExpression;

	/**
	 * Gets the value of the annotation property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the annotation property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAnnotation().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link Annotation }
	 * 
	 * 
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return this.annotation;
	}

	/**
	 * Gets the value of the gExpression property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the gExpression property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGExpression().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link JAXBElement }{@code <}{@link TwoChildrenExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link OneChildExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link CollectionExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link LabeledElementExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link DecimalConstantExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link TwoChildrenExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link LabeledElementReferenceExpression }
	 * {@code >} {@link JAXBElement }{@code <}{@link TwoChildrenExpression }
	 * {@code >} {@link JAXBElement }{@code <}{@link CastOrIsOfExpression }
	 * {@code >} {@link JAXBElement }{@code <}{@link StringConstantExpression }
	 * {@code >} {@link JAXBElement }{@code <}{@link GuidConstantExpression }
	 * {@code >} {@link JAXBElement }{@code <}{@link PathExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link PathExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link BinaryConstantExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link TwoChildrenExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}
	 * {@code >} {@link JAXBElement }{@code <}{@link PathExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link TimeOfDayConstantExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link BoolConstantExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link IfExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link CastOrIsOfExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link FloatConstantExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link IntConstantExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link OneChildExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link TwoChildrenExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link TwoChildrenExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link TwoChildrenExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link DurationConstantExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link ApplyExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link TwoChildrenExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link RecordExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link PathExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link DateConstantExpression }{@code >}
	 * {@link JAXBElement }{@code <}{@link DateTimeOffsetConstantExpression }
	 * {@code >} {@link JAXBElement }{@code <}{@link NullExpression }{@code >}
	 * 
	 * 
	 */
	public List<JAXBElement<?>> getGExpression() {
		if (gExpression == null) {
			gExpression = new ArrayList<JAXBElement<?>>();
		}
		return this.gExpression;
	}

}
