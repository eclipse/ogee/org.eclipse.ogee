/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The edm:Action element represents an action in an entity model. Actions MAY
 * have observable side effects and MAY return a single instance or a collection
 * of instances of any type. Actions are not composable. The action MAY specify
 * a return type using the edm:ReturnType element. The return type must be a
 * scalar, entity or complex type, or a collection of scalar, entity or complex
 * types. The action may also define zero or more edm:Parameter elements to be
 * used during the execution of the action.
 */
public class Action extends BaseElement {
	protected List<ActionFunctionParameter> parameters;
	protected List<Annotation> annotation;
	@XmlElement(name = "ReturnType")
	protected ActionFunctionReturnType returnType;
	@XmlAttribute(name = "Name", required = true)
	protected String name;
	@XmlAttribute(name = "ReturnType")
	protected String returnTypeAttribute;
	@XmlAttribute(name = "EntitySetPath")
	protected String entitySetPath;
	@XmlAttribute(name = "IsBound")
	protected Boolean isBound;

	/**
	 * @return the parameters
	 */
	public List<ActionFunctionParameter> getParameters() {
		if (parameters == null) {
			parameters = new ArrayList<ActionFunctionParameter>();
		}
		return parameters;
	}

	/**
	 * @param parameters
	 *            the parameters to set
	 */
	public void setParameters(List<ActionFunctionParameter> parameters) {
		this.parameters = parameters;
	}

	/**
	 * @return the annotation
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return this.annotation;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

	/**
	 * @return the returnType
	 */
	public ActionFunctionReturnType getReturnType() {
		return returnType;
	}

	/**
	 * @param returnType
	 *            the returnType to set
	 */
	public void setReturnType(ActionFunctionReturnType returnType) {
		this.returnType = returnType;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the returnTypeAttribute
	 */
	public String getReturnTypeAttribute() {
		return returnTypeAttribute;
	}

	/**
	 * @param returnTypeAttribute
	 *            the returnTypeAttribute to set
	 */
	public void setReturnTypeAttribute(String returnTypeAttribute) {
		this.returnTypeAttribute = returnTypeAttribute;
	}

	/**
	 * @return the entitySetPath
	 */
	public String getEntitySetPath() {
		return entitySetPath;
	}

	/**
	 * @param entitySetPath
	 *            the entitySetPath to set
	 */
	public void setEntitySetPath(String entitySetPath) {
		this.entitySetPath = entitySetPath;
	}

	/**
	 * @return the isBound
	 */
	public Boolean getIsBound() {
		return isBound;
	}

	/**
	 * @param isBound
	 *            the isBound to set
	 */
	public void setIsBound(Boolean isBound) {
		this.isBound = isBound;
	}

}
