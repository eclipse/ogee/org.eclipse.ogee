/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;

import org.eclipse.ogee.client.model.edmx.EntitySet;

public class EntitySetV4 extends EntitySet {
	protected List<NavigationPropertyBinding> navigationPropertyBindings;
	protected List<Annotation> annotation;
	@XmlAttribute(name = "IncludeInServiceDocument")
	protected Boolean includeInServiceDocument = true;

	/**
	 * @return the navigationPropertyBindings
	 */
	public List<NavigationPropertyBinding> getNavigationPropertyBindings() {
		if (navigationPropertyBindings == null) {
			this.navigationPropertyBindings = new ArrayList<NavigationPropertyBinding>();
		}
		return navigationPropertyBindings;
	}

	/**
	 * @param navigationPropertyBindings
	 *            the navigationPropertyBindings to set
	 */
	public void setNavigationPropertyBindings(
			List<NavigationPropertyBinding> navigationPropertyBindings) {
		this.navigationPropertyBindings = navigationPropertyBindings;
	}

	/**
	 * @return the annotation
	 */
	public List<Annotation> getAnnotation() {
		if (this.annotation == null) {
			this.annotation = new ArrayList<Annotation>();
		}
		return annotation;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

	/**
	 * @return the includeInServiceDocument
	 */
	public Boolean getIncludeInServiceDocument() {
		return includeInServiceDocument;
	}

	/**
	 * @param includeInServiceDocument
	 *            the includeInServiceDocument to set
	 */
	public void setIncludeInServiceDocument(Boolean includeInServiceDocument) {
		this.includeInServiceDocument = includeInServiceDocument;
		this.setAttribute("IncludeInServiceDocument",
				String.valueOf(includeInServiceDocument));
	}

}
