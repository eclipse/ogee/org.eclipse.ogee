/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.ogee.client.model.edmx.Parameter;

/**
 * The edm:Parameter element allows one or more parameters to be passed to a
 * function or action.
 * 
 */
public class ActionFunctionParameter extends Parameter {

	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;
	@XmlAttribute(name = "Nullable")
	protected Boolean nullable;
	@XmlAttribute(name = "DefaultValue")
	protected String defaultValue;
	@XmlAttribute(name = "Precision")
	protected String precision;
	@XmlAttribute(name = "Scale")
	protected String scale;
	@XmlAttribute(name = "SRID")
	protected String srid;

	/**
	 * @return the annotation
	 */
	public List<Annotation> getAnnotation() {
		if (this.annotation == null) {
			this.annotation = new ArrayList<Annotation>();
		}
		return annotation;
	}

	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @param defaultValue
	 *            the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

	/**
	 * @return the nullable
	 */
	public Boolean getNullable() {
		return nullable;
	}

	/**
	 * @param nullable
	 *            the nullable to set
	 */
	public void setNullable(Boolean nullable) {
		this.nullable = nullable;
		this.setAttribute("Nullable", String.valueOf(nullable));
	}

	/**
	 * @return the precision
	 */
	public String getPrecision() {
		return precision;
	}

	/**
	 * @param precision
	 *            the precision to set
	 */
	public void setPrecision(String precision) {
		this.precision = precision;
		this.setAttribute("Precision", String.valueOf(precision));

	}

	/**
	 * @return the scale
	 */
	public String getScale() {
		return scale;
	}

	/**
	 * @param scale
	 *            the scale to set
	 */
	public void setScale(String scale) {
		this.scale = scale;
		this.setAttribute("Scale", String.valueOf(scale));

	}

	/**
	 * @return the srid
	 */
	public String getSRID() {
		return srid;
	}

	/**
	 * @param srid
	 *            the srid to set
	 */
	public void setSRID(String srid) {
		this.srid = srid;
		this.setAttribute("SRID", String.valueOf(srid));

	}

}
