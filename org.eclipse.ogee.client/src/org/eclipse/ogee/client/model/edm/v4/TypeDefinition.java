/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * A type definition defines a specialization of one of the primitive types.
 * 
 */
public class TypeDefinition extends BaseElement {

	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;
	@XmlAttribute(name = "Name", required = true)
	protected String name;
	@XmlAttribute(name = "UnderlyingType", required = true)
	protected String underlyingType;
	@XmlAttribute(name = "MaxLength")
	protected String maxLength;
	@XmlAttribute(name = "Precision")
	protected String precision;
	@XmlAttribute(name = "Scale")
	protected String scale;
	@XmlAttribute(name = "SRID")
	protected String srid;
	@XmlAttribute(name = "Unicode")
	protected Boolean unicode;

	/**
	 * Gets the value of the annotation property.
	 * 
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return this.annotation;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the underlyingType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUnderlyingType() {
		return underlyingType;
	}

	/**
	 * Sets the value of the underlyingType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUnderlyingType(String value) {
		this.underlyingType = value;
	}

	/**
	 * Gets the value of the maxLength property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMaxLength() {
		return maxLength;
	}

	/**
	 * Sets the value of the maxLength property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMaxLength(String value) {
		this.maxLength = value;
	}

	/**
	 * Gets the value of the precision property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrecision() {
		return precision;
	}

	/**
	 * Sets the value of the precision property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrecision(String value) {
		this.precision = value;
	}

	/**
	 * Gets the value of the scale property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScale() {
		return scale;
	}

	/**
	 * Sets the value of the scale property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScale(String value) {
		this.scale = value;
	}

	/**
	 * Gets the value of the srid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSRID() {
		return srid;
	}

	/**
	 * Sets the value of the srid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSRID(String value) {
		this.srid = value;
	}

	/**
	 * Gets the value of the unicode property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isUnicode() {
		return unicode;
	}

	/**
	 * Sets the value of the unicode property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setUnicode(Boolean value) {
		this.unicode = value;
	}

}
