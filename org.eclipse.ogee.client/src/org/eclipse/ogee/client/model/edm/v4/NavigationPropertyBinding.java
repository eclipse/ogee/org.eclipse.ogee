/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import javax.xml.bind.annotation.XmlAttribute;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * An entity set or a singleton SHOULD contain an edm:NavigationPropertyBinding
 * element for each navigation property of its entity type, including navigation
 * properties defined on complex typed properties. If omitted, clients MUST
 * assume that the target entity set or singleton can vary per related entity.
 */
public class NavigationPropertyBinding extends BaseElement {

	@XmlAttribute(name = "Path", required = true)
	protected String path;
	@XmlAttribute(name = "Target", required = true)
	protected String target;
	@XmlAttribute(name = "EntitySet", required = true)
	protected String entitySet;

	/**
	 * Gets the value of the path property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @return the entitySet
	 */
	public String getEntitySet() {
		return entitySet;
	}

	/**
	 * @param entitySet
	 *            the entitySet to set
	 */
	public void setEntitySet(String entitySet) {
		this.entitySet = entitySet;
	}

	/**
	 * Sets the value of the path property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPath(String value) {
		this.path = value;
	}

	/**
	 * Gets the value of the target property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * Sets the value of the target property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTarget(String value) {
		this.target = value;
	}

}
