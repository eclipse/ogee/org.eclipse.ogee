/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The edm:Apply expression enables a value to be obtained by applying a
 * client-side function. The Apply expression MUST contain at least one
 * expression. The expressions contained within the Apply expression are used as
 * parameters to the function. The edm:Apply expression MUST be written with
 * element notation.
 * 
 */
public class ApplyExpression extends BaseElement {

	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;
	@XmlAttribute(name = "Function")
	protected String function;

	private Object[] childExpressions;

	/**
	 * Gets the value of the annotation property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the annotation property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAnnotation().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link Annotation }
	 * 
	 * 
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return this.annotation;
	}

	/**
	 * Gets the value of the function property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFunction() {
		return function;
	}

	/**
	 * Sets the value of the function property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFunction(String value) {
		this.function = value;
	}

	/**
	 * @return the childExpressions
	 */
	public Object[] getChildExpressions() {
		if (this.childExpressions == null) {
			return new Object[0];
		}
		return Arrays.copyOf(this.childExpressions,
				this.childExpressions.length);
	}

	/**
	 * @param childExpressions
	 *            the childExpressions to set
	 */
	public void setChildExpressions(Object[] childExpressions) {
		this.childExpressions = childExpressions;
	}

}
