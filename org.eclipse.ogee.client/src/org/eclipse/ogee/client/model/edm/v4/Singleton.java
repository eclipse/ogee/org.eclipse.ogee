/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import javax.xml.bind.annotation.XmlAttribute;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The edm:Singleton element represents a single entity in an entity model,
 * called a singleton.
 * 
 */
public class Singleton extends BaseElement {

	protected NavigationPropertyBinding navigationPropertyBinding;
	protected Annotation annotation;
	@XmlAttribute(name = "Name", required = true)
	protected String name;
	@XmlAttribute(name = "Type", required = true)
	protected String type;

	/**
	 * @return the navigationPropertyBinding
	 */
	public NavigationPropertyBinding getNavigationPropertyBinding() {
		return navigationPropertyBinding;
	}

	/**
	 * @param navigationPropertyBinding
	 *            the navigationPropertyBinding to set
	 */
	public void setNavigationPropertyBinding(
			NavigationPropertyBinding navigationPropertyBinding) {
		this.navigationPropertyBinding = navigationPropertyBinding;
	}

	/**
	 * @return the annotation
	 */
	public Annotation getAnnotation() {
		return annotation;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(Annotation annotation) {
		this.annotation = annotation;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
