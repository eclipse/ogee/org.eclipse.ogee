/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The edm:Annotations element is used to apply a group of annotations to a
 * single model element. It MUST contain at least one edm:Annotation element.
 */
public class AnnotationsV4 extends BaseElement {

	@XmlElement(name = "Annotation", required = true)
	protected List<Annotation> annotations;
	@XmlAttribute(name = "Target", required = true)
	protected String target;
	@XmlAttribute(name = "Qualifier")
	protected String qualifier;

	/**
	 * @return the annotations
	 */
	public List<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new ArrayList<Annotation>();
		}
		return annotations;
	}

	/**
	 * @param annotations
	 *            the annotations to set
	 */
	public void setAnnotations(List<Annotation> annotations) {
		this.annotations = annotations;
	}

	/**
	 * Gets the value of the target property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * Sets the value of the target property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTarget(String value) {
		this.target = value;
	}

	/**
	 * Gets the value of the qualifier property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getQualifier() {
		return qualifier;
	}

	/**
	 * Sets the value of the qualifier property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setQualifier(String value) {
		this.qualifier = value;
	}

}
