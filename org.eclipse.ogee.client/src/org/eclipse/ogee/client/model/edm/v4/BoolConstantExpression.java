/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import javax.xml.bind.annotation.XmlValue;

/**
 * The edm:Bool expression evaluates to a primitive Boolean value. A Boolean
 * expression MUST be assigned a Boolean value. The Boolean expression MAY be
 * provided using element notation or attribute notation.
 */
public class BoolConstantExpression {

	@XmlValue
	protected boolean value;

	/**
	 * Gets the value of the value property.
	 * 
	 */
	public boolean isValue() {
		return value;
	}

	/**
	 * Sets the value of the value property.
	 * 
	 */
	public void setValue(boolean value) {
		this.value = value;
	}

}
