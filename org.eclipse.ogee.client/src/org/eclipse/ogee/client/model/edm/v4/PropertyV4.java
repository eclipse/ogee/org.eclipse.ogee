/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.ogee.client.model.edmx.Property;

public class PropertyV4 extends Property {

	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;
	@XmlAttribute(name = "DefaultValue")
	protected String defaultValue;
	@XmlAttribute(name = "Precision")
	protected String precision;
	@XmlAttribute(name = "Scale")
	protected String scale;
	@XmlAttribute(name = "SRID")
	protected String srid;

	/**
	 * Gets the value of the annotation property.
	 * 
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return this.annotation;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

	/**
	 * Gets the value of the defaultValue property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Sets the value of the defaultValue property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDefaultValue(String value) {
		this.defaultValue = value;
		this.setAttribute("DefaultValue", value);
	}

	/**
	 * Gets the value of the precision property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public String getPrecision() {
		return precision;
	}

	/**
	 * Sets the value of the precision property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setPrecision(String value) {
		this.precision = value;
		this.setAttribute("Precision", value);
	}

	/**
	 * Gets the value of the scale property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScale() {
		return scale;
	}

	/**
	 * Sets the value of the scale property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScale(String value) {
		this.scale = value;
		this.setAttribute("Scale", value);

	}

	/**
	 * Gets the value of the srid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSRID() {
		return srid;
	}

	/**
	 * Sets the value of the srid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSRID(String value) {
		this.srid = value;
		this.setAttribute("SRID", value);

	}

}
