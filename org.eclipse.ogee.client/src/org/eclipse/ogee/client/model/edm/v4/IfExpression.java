/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * The edm:If expression enables a value to be obtained by evaluating a
 * conditional expression.
 * 
 */
public class IfExpression {

	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;

	/**
	 * Gets the value of the annotation property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the annotation property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAnnotation().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link Annotation }
	 * 
	 * 
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return this.annotation;
	}

	private Object[] childExpressions;

	/**
	 * Default constructor
	 */
	public IfExpression() {
		super();
		setChildExpressions(new Object[0]);
	}

	/**
	 * @return the childExpressions
	 */
	public Object[] getChildExpressions() {
		if (this.childExpressions != null) {
			return Arrays.copyOf(this.childExpressions,
					this.childExpressions.length);
		}
		return null;
	}

	/**
	 * @param childExpressions
	 *            the childExpressions to set
	 */
	public void setChildExpressions(Object[] childExpressions) {
		this.childExpressions = childExpressions;
	}

}
