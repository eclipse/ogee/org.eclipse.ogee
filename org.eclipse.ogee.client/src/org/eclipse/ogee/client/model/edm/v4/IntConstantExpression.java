/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlValue;

/**
 * The edm:Int expression evaluates to a primitive integer value. An integer
 * MUST be assigned a value of the type xs:integer.
 * 
 */
public class IntConstantExpression {

	@XmlValue
	protected String value;

	/**
	 * Gets the value of the value property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value of the value property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
