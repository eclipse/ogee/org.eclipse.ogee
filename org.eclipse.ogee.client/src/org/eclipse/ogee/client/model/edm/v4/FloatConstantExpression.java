/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import javax.xml.bind.annotation.XmlValue;

/**
 * The edm:Float expression evaluates to a primitive floating point (or double)
 * value. A float expression MUST be assigned a value of the type xs:double
 * 
 */
public class FloatConstantExpression {

	@XmlValue
	protected String value;

	/**
	 * Gets the value of the value property.
	 * 
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value of the value property.
	 * 
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
