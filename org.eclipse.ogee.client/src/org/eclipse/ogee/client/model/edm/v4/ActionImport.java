/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The edm:ActionImport element allows exposing an unbound action as a top-level
 * element in an entity container. Action imports are never advertised in the
 * service document.
 * 
 */
public class ActionImport extends BaseElement {

	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;
	@XmlAttribute(name = "Action", required = true)
	protected String action;
	@XmlAttribute(name = "Name", required = true)
	protected String name;
	@XmlAttribute(name = "EntitySet")
	protected String entitySet;
	@XmlAttribute(name = "IncludeInServiceDocument")
	protected Boolean includeInServiceDocument;

	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return this.annotation;
	}

	/**
	 * @return the includeInServiceDocument
	 */
	public Boolean getIncludeInServiceDocument() {
		return includeInServiceDocument;
	}

	/**
	 * Gets the value of the action property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the value of the action property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAction(String value) {
		this.action = value;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the entitySet property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEntitySet() {
		return entitySet;
	}

	/**
	 * Sets the value of the entitySet property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEntitySet(String value) {
		this.entitySet = value;
	}

	/**
	 * Gets the value of the includeInServiceDocument property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public boolean isIncludeInServiceDocument() {
		if (includeInServiceDocument == null) {
			return false;
		} else {
			return includeInServiceDocument;
		}
	}

	/**
	 * Sets the value of the includeInServiceDocument property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setIncludeInServiceDocument(Boolean value) {
		this.includeInServiceDocument = value;
	}

}
