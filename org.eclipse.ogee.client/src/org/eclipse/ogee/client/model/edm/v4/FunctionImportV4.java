/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.ogee.client.model.edmx.FunctionImport;

/**
 * The edm:FunctionImport element allows exposing an unbound function as a
 * top-level element in an entity container.
 * 
 */
public class FunctionImportV4 extends FunctionImport {

	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;
	@XmlAttribute(name = "Function", required = true)
	protected String function;
	@XmlAttribute(name = "IncludeInServiceDocument")
	protected Boolean includeInServiceDocument;

	/**
	 * Gets the value of the function property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFunction() {
		return function;
	}

	/**
	 * Sets the value of the function property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFunction(String value) {
		this.function = value;
		this.setAttribute("Function", value);
	}

	/**
	 * Gets the value of the includeInServiceDocument property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public boolean isIncludeInServiceDocument() {
		if (includeInServiceDocument == null) {
			return false;
		} else {
			return includeInServiceDocument;
		}
	}

	/**
	 * Sets the value of the includeInServiceDocument property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setIncludeInServiceDocument(Boolean value) {
		this.includeInServiceDocument = value;
		this.setAttribute("IncludeInServiceDocument", String.valueOf(value));
	}

	/**
	 * @return the annotation
	 */
	public List<Annotation> getAnnotation() {
		if (this.annotation == null) {
			this.annotation = new ArrayList<Annotation>();
		}
		return annotation;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

	/**
	 * @return the includeInServiceDocument
	 */
	public Boolean getIncludeInServiceDocument() {
		return includeInServiceDocument;
	}

}
