/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ogee.client.model.edmx.Schema;

public class SchemaV4 extends Schema {
	protected List<TypeDefinition> typeDefinitions;
	protected List<Action> actions;
	protected List<AnnotationsV4> annotations;
	protected List<EnumTypeV4> enumTypes;
	protected List<Function> functions;
	protected List<Term> terms;

	/**
	 * @return the typeDefinitions
	 */
	public List<TypeDefinition> getTypeDefinitions() {
		if (typeDefinitions == null) {
			typeDefinitions = new ArrayList<TypeDefinition>();
		}
		return typeDefinitions;
	}

	/**
	 * @param typeDefinitions
	 *            the typeDefinitions to set
	 */
	public void setTypeDefinitions(List<TypeDefinition> typeDefinitions) {
		this.typeDefinitions = typeDefinitions;
	}

	/**
	 * @return the actions
	 */
	public List<Action> getActions() {
		if (actions == null) {
			actions = new ArrayList<Action>();
		}
		return actions;
	}

	/**
	 * @param actions
	 *            the actions to set
	 */
	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	/**
	 * @return the annotations
	 */
	public List<AnnotationsV4> getAnnotations() {
		if (annotations == null) {
			annotations = new ArrayList<AnnotationsV4>();
		}
		return annotations;
	}

	/**
	 * @param annotations
	 *            the annotations to set
	 */
	public void setAnnotations(List<AnnotationsV4> annotations) {
		this.annotations = annotations;
	}

	/**
	 * @return the enumTypes
	 */
	public List<EnumTypeV4> getEnumTypes() {
		if (enumTypes == null) {
			enumTypes = new ArrayList<EnumTypeV4>();
		}
		return enumTypes;
	}

	/**
	 * @param enumTypes
	 *            the enumTypes to set
	 */
	public void setEnumTypes(List<EnumTypeV4> enumTypes) {
		this.enumTypes = enumTypes;
	}

	/**
	 * @return the functions
	 */
	public List<Function> getFunctions() {
		if (functions == null) {
			functions = new ArrayList<Function>();
		}
		return functions;
	}

	/**
	 * @param functions
	 *            the functions to set
	 */
	public void setFunctions(List<Function> functions) {
		this.functions = functions;
	}

	/**
	 * @return the terms
	 */
	public List<Term> getTerms() {
		if (terms == null) {
			terms = new ArrayList<Term>();
		}
		return terms;
	}

	/**
	 * @param terms
	 *            the terms to set
	 */
	public void setTerms(List<Term> terms) {
		this.terms = terms;
	}

}
