/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.ogee.client.model.edmx.EntityContainer;

public class EntityContainerV4 extends EntityContainer {

	protected List<ActionImport> actionImports;
	protected List<Singleton> entities;
	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;

	@XmlAttribute(name = "Extends")
	protected String _extends;

	/**
	 * @return the actionImports
	 */
	public List<ActionImport> getActionImports() {
		return actionImports;
	}

	/**
	 * @param actionImports
	 *            the actionImports to set
	 */
	public void setActionImports(List<ActionImport> actionImports) {
		this.actionImports = actionImports;
	}

	/**
	 * @return the entities
	 */
	public List<Singleton> getEntities() {
		if (entities == null) {
			entities = new ArrayList<Singleton>();
		}
		return entities;
	}

	/**
	 * @param entities
	 *            the entities to set
	 */
	public void setEntities(List<Singleton> entities) {
		this.entities = entities;
	}

	/**
	 * @return the annotation
	 */
	public List<Annotation> getAnnotation() {
		if (this.annotation == null) {
			this.annotation = new ArrayList<Annotation>();
		}
		return annotation;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

	/**
	 * @return the _extends
	 */
	public String getExtends() {
		return _extends;
	}

	/**
	 * @param _extends
	 *            the _extends to set
	 */
	public void setExtends(String _extends) {
		this._extends = _extends;
	}

}
