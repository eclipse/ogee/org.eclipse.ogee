/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * The edm:Record expression enables a new entity type or complex type instance
 * to be constructed. A record expression contains zero or more
 * edm:PropertyValue elements.
 * 
 */
public class RecordExpression extends BaseElement {
	protected List<PropertyValue> propertyValues;
	protected List<Annotation> annotation;
	@XmlAttribute(name = "Type")
	protected String type;

	/**
	 * Gets the property values
	 * 
	 * @return property values list
	 */
	public List<PropertyValue> getPropertyValues() {
		if (propertyValues == null) {
			propertyValues = new ArrayList<PropertyValue>();
		}
		return this.propertyValues;
	}

	/**
	 * @param propertyValues
	 *            the propertyValues to set
	 */
	public void setPropertyValues(List<PropertyValue> propertyValues) {
		this.propertyValues = propertyValues;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

	/**
	 * Gets the annotation list
	 * 
	 * @return annotation list
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return this.annotation;
	}

	/**
	 * Gets the value of the type property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setType(String value) {
		this.type = value;
	}

}
