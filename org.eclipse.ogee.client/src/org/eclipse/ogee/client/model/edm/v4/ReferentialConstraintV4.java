/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.ogee.client.model.edmx.ReferentialConstraint;

public class ReferentialConstraintV4 extends ReferentialConstraint {

	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;
	@XmlAttribute(name = "Property", required = true)
	protected String property;
	@XmlAttribute(name = "ReferencedProperty", required = true)
	protected String referencedProperty;

	/**
	 * @return the annotation
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return annotation;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

	/**
	 * Gets the value of the property property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * Sets the value of the property property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProperty(String value) {
		this.property = value;
		this.setAttribute("Property", value);
	}

	/**
	 * Gets the value of the referencedProperty property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReferencedProperty() {
		return referencedProperty;
	}

	/**
	 * Sets the value of the referencedProperty property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setReferencedProperty(String value) {
		this.referencedProperty = value;
		this.setAttribute("ReferencedProperty", value);
	}

}
