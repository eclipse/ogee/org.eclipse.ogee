/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;

import org.eclipse.ogee.client.model.edmx.NavigationProperty;

/**
 * A navigation property allows navigation to related entities.
 * 
 */
public class NavigationPropertyV4 extends NavigationProperty {
	protected List<ReferentialConstraintV4> referentialConstraint;
	protected OnDelete OnDelete;
	protected List<Annotation> annotation;
	@XmlAttribute(name = "Type", required = true)
	protected String type;
	@XmlAttribute(name = "Nullable")
	protected Boolean nullable;
	@XmlAttribute(name = "Partner")
	protected String partner;
	@XmlAttribute(name = "ContainsTarget")
	protected Boolean containsTarget;

	/**
	 * @return the referentialConstraint
	 */
	public List<ReferentialConstraintV4> getReferentialConstraint() {
		if (referentialConstraint == null) {
			referentialConstraint = new ArrayList<ReferentialConstraintV4>();
		}
		return referentialConstraint;
	}

	/**
	 * @param referentialConstraint
	 *            the referentialConstraint to set
	 */
	public void setReferentialConstraint(
			List<ReferentialConstraintV4> referentialConstraint) {
		this.referentialConstraint = referentialConstraint;
	}

	/**
	 * @return the onDelete
	 */
	public OnDelete getOnDelete() {
		return OnDelete;
	}

	/**
	 * @param onDelete
	 *            the onDelete to set
	 */
	public void setOnDelete(OnDelete onDelete) {
		OnDelete = onDelete;
	}

	/**
	 * @return the annotation
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return annotation;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
		this.setAttribute("Type", type);
	}

	/**
	 * @return the nullable
	 */
	public Boolean getNullable() {
		return nullable;
	}

	/**
	 * @param nullable
	 *            the nullable to set
	 */
	public void setNullable(Boolean nullable) {
		this.nullable = nullable;
		this.setAttribute("Nullable", String.valueOf(nullable));

	}

	/**
	 * @return the partner
	 */
	public String getPartner() {
		return partner;
	}

	/**
	 * @param partner
	 *            the partner to set
	 */
	public void setPartner(String partner) {
		this.partner = partner;
		this.setAttribute("Partner", partner);
	}

	/**
	 * @return the containsTarget
	 */
	public Boolean getContainsTarget() {
		return containsTarget;
	}

	/**
	 * @param containsTarget
	 *            the containsTarget to set
	 */
	public void setContainsTarget(Boolean containsTarget) {
		this.containsTarget = containsTarget;
		this.setAttribute("ContainsTarget", String.valueOf(containsTarget));
	}

}
