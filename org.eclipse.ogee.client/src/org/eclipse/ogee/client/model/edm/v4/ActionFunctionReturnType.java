/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAttribute;

import org.eclipse.ogee.client.model.generic.BaseElement;

/**
 * If an edm:Action or edm:Function element does not include the ReturnType
 * attribute, it MUST contain a single edm:ReturnType element. The attributes
 * MaxLength, Precision, Scale, and SRID can be used to specify the facets of
 * the return type, as appropriate. If the facet attributes are not specified,
 * their values are considered unspecified.
 */
public class ActionFunctionReturnType extends BaseElement {

	@XmlAttribute(name = "Type")
	protected String type;
	@XmlAttribute(name = "Nullable")
	protected Boolean nullable;
	@XmlAttribute(name = "EntitySetPath")
	protected String entitySetPath;
	@XmlAttribute(name = "MaxLength")
	protected String maxLength;
	@XmlAttribute(name = "Precision")
	protected String precision;
	@XmlAttribute(name = "Scale")
	protected String scale;
	@XmlAttribute(name = "SRID")
	protected String srid;

	/**
	 * Gets the value of the type property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the type property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getType().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link String }
	 * 
	 * 
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the value of the nullable property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isNullable() {
		return nullable;
	}

	/**
	 * Sets the value of the nullable property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setNullable(Boolean value) {
		this.nullable = value;
	}

	/**
	 * Gets the value of the entitySetPath property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEntitySetPath() {
		return entitySetPath;
	}

	/**
	 * Sets the value of the entitySetPath property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEntitySetPath(String value) {
		this.entitySetPath = value;
	}

	/**
	 * Gets the value of the maxLength property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMaxLength() {
		return maxLength;
	}

	/**
	 * Sets the value of the maxLength property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMaxLength(String value) {
		this.maxLength = value;
	}

	/**
	 * Gets the value of the precision property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public String getPrecision() {
		return precision;
	}

	/**
	 * Sets the value of the precision property.
	 * 
	 * @param attrValue
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setPrecision(String attrValue) {
		this.precision = attrValue;
	}

	/**
	 * Gets the value of the scale property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScale() {
		return scale;
	}

	/**
	 * Sets the value of the scale property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScale(String value) {
		this.scale = value;
	}

	/**
	 * Gets the value of the srid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSRID() {
		return srid;
	}

	/**
	 * Sets the value of the srid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSRID(String value) {
		this.srid = value;
	}

}
