/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OneChildExpression complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OneChildExpression">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://docs.oasis-open.org/odata/ns/edm}Annotation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;group ref="{http://docs.oasis-open.org/odata/ns/edm}GExpression"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OneChildExpression", propOrder = { "annotation", "binary",
		"bool", "date", "dateTimeOffset", "decimal", "duration", "enumMember",
		"_float", "guid", "_int", "string", "timeOfDay", "annotationPath",
		"apply", "cast", "collection", "_if", "eq", "ne", "ge", "gt", "le",
		"lt", "and", "or", "not", "isOf", "labeledElement",
		"labeledElementReference", "_null", "navigationPropertyPath", "path",
		"propertyPath", "record", "urlRef" })
public class OneChildExpression {

	@XmlElement(name = "Annotation")
	protected List<Annotation> annotation;
	@XmlElement(name = "Binary")
	protected BinaryConstantExpression binary;
	@XmlElement(name = "Bool")
	protected BoolConstantExpression bool;
	@XmlElement(name = "Date")
	protected DateConstantExpression date;
	@XmlElement(name = "DateTimeOffset")
	protected DateTimeOffsetConstantExpression dateTimeOffset;
	@XmlElement(name = "Decimal")
	protected DecimalConstantExpression decimal;
	@XmlElement(name = "Duration")
	protected DurationConstantExpression duration;
	@XmlList
	@XmlElement(name = "EnumMember")
	protected List<String> enumMember;
	@XmlElement(name = "Float")
	protected FloatConstantExpression _float;
	@XmlElement(name = "Guid")
	protected GuidConstantExpression guid;
	@XmlElement(name = "Int")
	protected IntConstantExpression _int;
	@XmlElement(name = "String")
	protected StringConstantExpression string;
	@XmlElement(name = "TimeOfDay")
	protected TimeOfDayConstantExpression timeOfDay;
	@XmlElement(name = "AnnotationPath")
	protected PathExpression annotationPath;
	@XmlElement(name = "Apply")
	protected ApplyExpression apply;
	@XmlElement(name = "Cast")
	protected CastOrIsOfExpression cast;
	@XmlElement(name = "Collection")
	protected CollectionExpression collection;
	@XmlElement(name = "If")
	protected IfExpression _if;
	@XmlElement(name = "Eq")
	protected TwoChildrenExpression eq;
	@XmlElement(name = "Ne")
	protected TwoChildrenExpression ne;
	@XmlElement(name = "Ge")
	protected TwoChildrenExpression ge;
	@XmlElement(name = "Gt")
	protected TwoChildrenExpression gt;
	@XmlElement(name = "Le")
	protected TwoChildrenExpression le;
	@XmlElement(name = "Lt")
	protected TwoChildrenExpression lt;
	@XmlElement(name = "And")
	protected TwoChildrenExpression and;
	@XmlElement(name = "Or")
	protected TwoChildrenExpression or;
	@XmlElement(name = "Not")
	protected OneChildExpression not;
	@XmlElement(name = "IsOf")
	protected CastOrIsOfExpression isOf;
	@XmlElement(name = "LabeledElement")
	protected LabeledElementExpression labeledElement;
	@XmlElement(name = "LabeledElementReference")
	protected LabeledElementReferenceExpression labeledElementReference;
	@XmlElement(name = "Null")
	protected NullExpression _null;
	@XmlElement(name = "NavigationPropertyPath")
	protected PathExpression navigationPropertyPath;
	@XmlElement(name = "Path")
	protected PathExpression path;
	@XmlElement(name = "PropertyPath")
	protected PathExpression propertyPath;
	@XmlElement(name = "Record")
	protected RecordExpression record;
	@XmlElement(name = "UrlRef")
	protected OneChildExpression urlRef;

	/**
	 * Gets the value of the annotation property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the annotation property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAnnotation().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link Annotation }
	 * 
	 * 
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return this.annotation;
	}

	/**
	 * Gets the value of the binary property.
	 * 
	 * @return possible object is {@link BinaryConstantExpression }
	 * 
	 */
	public BinaryConstantExpression getBinary() {
		return binary;
	}

	/**
	 * Sets the value of the binary property.
	 * 
	 * @param value
	 *            allowed object is {@link BinaryConstantExpression }
	 * 
	 */
	public void setBinary(BinaryConstantExpression value) {
		this.binary = value;
	}

	/**
	 * Gets the value of the bool property.
	 * 
	 * @return possible object is {@link BoolConstantExpression }
	 * 
	 */
	public BoolConstantExpression getBool() {
		return bool;
	}

	/**
	 * Sets the value of the bool property.
	 * 
	 * @param value
	 *            allowed object is {@link BoolConstantExpression }
	 * 
	 */
	public void setBool(BoolConstantExpression value) {
		this.bool = value;
	}

	/**
	 * Gets the value of the date property.
	 * 
	 * @return possible object is {@link DateConstantExpression }
	 * 
	 */
	public DateConstantExpression getDate() {
		return date;
	}

	/**
	 * Sets the value of the date property.
	 * 
	 * @param value
	 *            allowed object is {@link DateConstantExpression }
	 * 
	 */
	public void setDate(DateConstantExpression value) {
		this.date = value;
	}

	/**
	 * Gets the value of the dateTimeOffset property.
	 * 
	 * @return possible object is {@link DateTimeOffsetConstantExpression }
	 * 
	 */
	public DateTimeOffsetConstantExpression getDateTimeOffset() {
		return dateTimeOffset;
	}

	/**
	 * Sets the value of the dateTimeOffset property.
	 * 
	 * @param value
	 *            allowed object is {@link DateTimeOffsetConstantExpression }
	 * 
	 */
	public void setDateTimeOffset(DateTimeOffsetConstantExpression value) {
		this.dateTimeOffset = value;
	}

	/**
	 * Gets the value of the decimal property.
	 * 
	 * @return possible object is {@link DecimalConstantExpression }
	 * 
	 */
	public DecimalConstantExpression getDecimal() {
		return decimal;
	}

	/**
	 * Sets the value of the decimal property.
	 * 
	 * @param value
	 *            allowed object is {@link DecimalConstantExpression }
	 * 
	 */
	public void setDecimal(DecimalConstantExpression value) {
		this.decimal = value;
	}

	/**
	 * Gets the value of the duration property.
	 * 
	 * @return possible object is {@link DurationConstantExpression }
	 * 
	 */
	public DurationConstantExpression getDuration() {
		return duration;
	}

	/**
	 * Sets the value of the duration property.
	 * 
	 * @param value
	 *            allowed object is {@link DurationConstantExpression }
	 * 
	 */
	public void setDuration(DurationConstantExpression value) {
		this.duration = value;
	}

	/**
	 * Gets the value of the enumMember property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the enumMember property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getEnumMember().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link String }
	 * 
	 * 
	 */
	public List<String> getEnumMember() {
		if (enumMember == null) {
			enumMember = new ArrayList<String>();
		}
		return this.enumMember;
	}

	/**
	 * Gets the value of the float property.
	 * 
	 * @return possible object is {@link FloatConstantExpression }
	 * 
	 */
	public FloatConstantExpression getFloat() {
		return _float;
	}

	/**
	 * Sets the value of the float property.
	 * 
	 * @param value
	 *            allowed object is {@link FloatConstantExpression }
	 * 
	 */
	public void setFloat(FloatConstantExpression value) {
		this._float = value;
	}

	/**
	 * Gets the value of the guid property.
	 * 
	 * @return possible object is {@link GuidConstantExpression }
	 * 
	 */
	public GuidConstantExpression getGuid() {
		return guid;
	}

	/**
	 * Sets the value of the guid property.
	 * 
	 * @param value
	 *            allowed object is {@link GuidConstantExpression }
	 * 
	 */
	public void setGuid(GuidConstantExpression value) {
		this.guid = value;
	}

	/**
	 * Gets the value of the int property.
	 * 
	 * @return possible object is {@link IntConstantExpression }
	 * 
	 */
	public IntConstantExpression getInt() {
		return _int;
	}

	/**
	 * Sets the value of the int property.
	 * 
	 * @param value
	 *            allowed object is {@link IntConstantExpression }
	 * 
	 */
	public void setInt(IntConstantExpression value) {
		this._int = value;
	}

	/**
	 * Gets the value of the string property.
	 * 
	 * @return possible object is {@link StringConstantExpression }
	 * 
	 */
	public StringConstantExpression getString() {
		return string;
	}

	/**
	 * Sets the value of the string property.
	 * 
	 * @param value
	 *            allowed object is {@link StringConstantExpression }
	 * 
	 */
	public void setString(StringConstantExpression value) {
		this.string = value;
	}

	/**
	 * Gets the value of the timeOfDay property.
	 * 
	 * @return possible object is {@link TimeOfDayConstantExpression }
	 * 
	 */
	public TimeOfDayConstantExpression getTimeOfDay() {
		return timeOfDay;
	}

	/**
	 * Sets the value of the timeOfDay property.
	 * 
	 * @param value
	 *            allowed object is {@link TimeOfDayConstantExpression }
	 * 
	 */
	public void setTimeOfDay(TimeOfDayConstantExpression value) {
		this.timeOfDay = value;
	}

	/**
	 * Gets the value of the annotationPath property.
	 * 
	 * @return possible object is {@link PathExpression }
	 * 
	 */
	public PathExpression getAnnotationPath() {
		return annotationPath;
	}

	/**
	 * Sets the value of the annotationPath property.
	 * 
	 * @param value
	 *            allowed object is {@link PathExpression }
	 * 
	 */
	public void setAnnotationPath(PathExpression value) {
		this.annotationPath = value;
	}

	/**
	 * Gets the value of the apply property.
	 * 
	 * @return possible object is {@link ApplyExpression }
	 * 
	 */
	public ApplyExpression getApply() {
		return apply;
	}

	/**
	 * Sets the value of the apply property.
	 * 
	 * @param value
	 *            allowed object is {@link ApplyExpression }
	 * 
	 */
	public void setApply(ApplyExpression value) {
		this.apply = value;
	}

	/**
	 * Gets the value of the cast property.
	 * 
	 * @return possible object is {@link CastOrIsOfExpression }
	 * 
	 */
	public CastOrIsOfExpression getCast() {
		return cast;
	}

	/**
	 * Sets the value of the cast property.
	 * 
	 * @param value
	 *            allowed object is {@link CastOrIsOfExpression }
	 * 
	 */
	public void setCast(CastOrIsOfExpression value) {
		this.cast = value;
	}

	/**
	 * Gets the value of the collection property.
	 * 
	 * @return possible object is {@link CollectionExpression }
	 * 
	 */
	public CollectionExpression getCollection() {
		return collection;
	}

	/**
	 * Sets the value of the collection property.
	 * 
	 * @param value
	 *            allowed object is {@link CollectionExpression }
	 * 
	 */
	public void setCollection(CollectionExpression value) {
		this.collection = value;
	}

	/**
	 * Gets the value of the if property.
	 * 
	 * @return possible object is {@link IfExpression }
	 * 
	 */
	public IfExpression getIf() {
		return _if;
	}

	/**
	 * Sets the value of the if property.
	 * 
	 * @param value
	 *            allowed object is {@link IfExpression }
	 * 
	 */
	public void setIf(IfExpression value) {
		this._if = value;
	}

	/**
	 * Gets the value of the eq property.
	 * 
	 * @return possible object is {@link TwoChildrenExpression }
	 * 
	 */
	public TwoChildrenExpression getEq() {
		return eq;
	}

	/**
	 * Sets the value of the eq property.
	 * 
	 * @param value
	 *            allowed object is {@link TwoChildrenExpression }
	 * 
	 */
	public void setEq(TwoChildrenExpression value) {
		this.eq = value;
	}

	/**
	 * Gets the value of the ne property.
	 * 
	 * @return possible object is {@link TwoChildrenExpression }
	 * 
	 */
	public TwoChildrenExpression getNe() {
		return ne;
	}

	/**
	 * Sets the value of the ne property.
	 * 
	 * @param value
	 *            allowed object is {@link TwoChildrenExpression }
	 * 
	 */
	public void setNe(TwoChildrenExpression value) {
		this.ne = value;
	}

	/**
	 * Gets the value of the ge property.
	 * 
	 * @return possible object is {@link TwoChildrenExpression }
	 * 
	 */
	public TwoChildrenExpression getGe() {
		return ge;
	}

	/**
	 * Sets the value of the ge property.
	 * 
	 * @param value
	 *            allowed object is {@link TwoChildrenExpression }
	 * 
	 */
	public void setGe(TwoChildrenExpression value) {
		this.ge = value;
	}

	/**
	 * Gets the value of the gt property.
	 * 
	 * @return possible object is {@link TwoChildrenExpression }
	 * 
	 */
	public TwoChildrenExpression getGt() {
		return gt;
	}

	/**
	 * Sets the value of the gt property.
	 * 
	 * @param value
	 *            allowed object is {@link TwoChildrenExpression }
	 * 
	 */
	public void setGt(TwoChildrenExpression value) {
		this.gt = value;
	}

	/**
	 * Gets the value of the le property.
	 * 
	 * @return possible object is {@link TwoChildrenExpression }
	 * 
	 */
	public TwoChildrenExpression getLe() {
		return le;
	}

	/**
	 * Sets the value of the le property.
	 * 
	 * @param value
	 *            allowed object is {@link TwoChildrenExpression }
	 * 
	 */
	public void setLe(TwoChildrenExpression value) {
		this.le = value;
	}

	/**
	 * Gets the value of the lt property.
	 * 
	 * @return possible object is {@link TwoChildrenExpression }
	 * 
	 */
	public TwoChildrenExpression getLt() {
		return lt;
	}

	/**
	 * Sets the value of the lt property.
	 * 
	 * @param value
	 *            allowed object is {@link TwoChildrenExpression }
	 * 
	 */
	public void setLt(TwoChildrenExpression value) {
		this.lt = value;
	}

	/**
	 * Gets the value of the and property.
	 * 
	 * @return possible object is {@link TwoChildrenExpression }
	 * 
	 */
	public TwoChildrenExpression getAnd() {
		return and;
	}

	/**
	 * Sets the value of the and property.
	 * 
	 * @param value
	 *            allowed object is {@link TwoChildrenExpression }
	 * 
	 */
	public void setAnd(TwoChildrenExpression value) {
		this.and = value;
	}

	/**
	 * Gets the value of the or property.
	 * 
	 * @return possible object is {@link TwoChildrenExpression }
	 * 
	 */
	public TwoChildrenExpression getOr() {
		return or;
	}

	/**
	 * Sets the value of the or property.
	 * 
	 * @param value
	 *            allowed object is {@link TwoChildrenExpression }
	 * 
	 */
	public void setOr(TwoChildrenExpression value) {
		this.or = value;
	}

	/**
	 * Gets the value of the not property.
	 * 
	 * @return possible object is {@link OneChildExpression }
	 * 
	 */
	public OneChildExpression getNot() {
		return not;
	}

	/**
	 * Sets the value of the not property.
	 * 
	 * @param value
	 *            allowed object is {@link OneChildExpression }
	 * 
	 */
	public void setNot(OneChildExpression value) {
		this.not = value;
	}

	/**
	 * Gets the value of the isOf property.
	 * 
	 * @return possible object is {@link CastOrIsOfExpression }
	 * 
	 */
	public CastOrIsOfExpression getIsOf() {
		return isOf;
	}

	/**
	 * Sets the value of the isOf property.
	 * 
	 * @param value
	 *            allowed object is {@link CastOrIsOfExpression }
	 * 
	 */
	public void setIsOf(CastOrIsOfExpression value) {
		this.isOf = value;
	}

	/**
	 * Gets the value of the labeledElement property.
	 * 
	 * @return possible object is {@link LabeledElementExpression }
	 * 
	 */
	public LabeledElementExpression getLabeledElement() {
		return labeledElement;
	}

	/**
	 * Sets the value of the labeledElement property.
	 * 
	 * @param value
	 *            allowed object is {@link LabeledElementExpression }
	 * 
	 */
	public void setLabeledElement(LabeledElementExpression value) {
		this.labeledElement = value;
	}

	/**
	 * Gets the value of the labeledElementReference property.
	 * 
	 * @return possible object is {@link LabeledElementReferenceExpression }
	 * 
	 */
	public LabeledElementReferenceExpression getLabeledElementReference() {
		return labeledElementReference;
	}

	/**
	 * Sets the value of the labeledElementReference property.
	 * 
	 * @param value
	 *            allowed object is {@link LabeledElementReferenceExpression }
	 * 
	 */
	public void setLabeledElementReference(
			LabeledElementReferenceExpression value) {
		this.labeledElementReference = value;
	}

	/**
	 * Gets the value of the null property.
	 * 
	 * @return possible object is {@link NullExpression }
	 * 
	 */
	public NullExpression getNull() {
		return _null;
	}

	/**
	 * Sets the value of the null property.
	 * 
	 * @param value
	 *            allowed object is {@link NullExpression }
	 * 
	 */
	public void setNull(NullExpression value) {
		this._null = value;
	}

	/**
	 * Gets the value of the navigationPropertyPath property.
	 * 
	 * @return possible object is {@link PathExpression }
	 * 
	 */
	public PathExpression getNavigationPropertyPath() {
		return navigationPropertyPath;
	}

	/**
	 * Sets the value of the navigationPropertyPath property.
	 * 
	 * @param value
	 *            allowed object is {@link PathExpression }
	 * 
	 */
	public void setNavigationPropertyPath(PathExpression value) {
		this.navigationPropertyPath = value;
	}

	/**
	 * Gets the value of the path property.
	 * 
	 * @return possible object is {@link PathExpression }
	 * 
	 */
	public PathExpression getPath() {
		return path;
	}

	/**
	 * Sets the value of the path property.
	 * 
	 * @param value
	 *            allowed object is {@link PathExpression }
	 * 
	 */
	public void setPath(PathExpression value) {
		this.path = value;
	}

	/**
	 * Gets the value of the propertyPath property.
	 * 
	 * @return possible object is {@link PathExpression }
	 * 
	 */
	public PathExpression getPropertyPath() {
		return propertyPath;
	}

	/**
	 * Sets the value of the propertyPath property.
	 * 
	 * @param value
	 *            allowed object is {@link PathExpression }
	 * 
	 */
	public void setPropertyPath(PathExpression value) {
		this.propertyPath = value;
	}

	/**
	 * Gets the value of the record property.
	 * 
	 * @return possible object is {@link RecordExpression }
	 * 
	 */
	public RecordExpression getRecord() {
		return record;
	}

	/**
	 * Sets the value of the record property.
	 * 
	 * @param value
	 *            allowed object is {@link RecordExpression }
	 * 
	 */
	public void setRecord(RecordExpression value) {
		this.record = value;
	}

	/**
	 * Gets the value of the urlRef property.
	 * 
	 * @return possible object is {@link OneChildExpression }
	 * 
	 */
	public OneChildExpression getUrlRef() {
		return urlRef;
	}

	/**
	 * Sets the value of the urlRef property.
	 * 
	 * @param value
	 *            allowed object is {@link OneChildExpression }
	 * 
	 */
	public void setUrlRef(OneChildExpression value) {
		this.urlRef = value;
	}

}
