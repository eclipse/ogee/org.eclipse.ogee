/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;

import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.generic.StringUtilities;

/**
 * Entity types are nominal structured types with a key that consists of one or
 * more references to structural properties. An entity type is the template for
 * an entity: any uniquely identifiable record such as a customer or order.
 * 
 */
public class EntityTypeV4 extends EntityType {

	protected List<Annotation> annotation;
	@XmlAttribute(name = "OpenType")
	protected Boolean openType;

	/**
	 * Gets the value of the openType property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public boolean isOpenType() {
		if (openType == null) {
			return false;
		} else {
			return openType;
		}
	}

	/**
	 * Sets the value of the openType property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setOpenType(Boolean value) {
		this.openType = value;
		this.setAttribute("OpenType", StringUtilities.stringValueOf(value));
	}

	/**
	 * @return the annotation
	 */
	public List<Annotation> getAnnotation() {
		if (this.annotation == null) {
			this.annotation = new ArrayList<Annotation>();
		}
		return annotation;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

}
