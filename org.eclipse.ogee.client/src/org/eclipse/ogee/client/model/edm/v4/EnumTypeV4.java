/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;

import org.eclipse.ogee.client.model.generic.BaseElement;
import org.eclipse.ogee.client.parser.impl.EdmTypes;

/**
 * Enumeration types are nominal scalar types that represent a series of related
 * values. Enumeration types expose these related values as members of the
 * enumeration.
 * 
 * Enumeration types typically allow the selection of a single member. The
 * IsFlags attribute allows entity model authors to indicate that more than one
 * value can be selected.
 * 
 */
public class EnumTypeV4 extends BaseElement {
	protected List<EnumTypeMember> members;
	protected List<Annotation> annotation;
	@XmlAttribute(name = "IsFlags")
	protected Boolean isFlags;
	@XmlAttribute(name = "UnderlyingType")
	protected String underlyingType = EdmTypes.Int32.toString();
	@XmlAttribute(name = "Name", required = true)
	protected String name;

	/**
	 * @return the members
	 */
	public List<EnumTypeMember> getMembers() {
		if (members == null) {
			members = new ArrayList<EnumTypeMember>();
		}
		return members;
	}

	/**
	 * @param members
	 *            the members to set
	 */
	public void setMembers(List<EnumTypeMember> members) {
		this.members = members;
	}

	/**
	 * Gets the value of the isFlags property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isIsFlags() {
		return isFlags;
	}

	/**
	 * Sets the value of the isFlags property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setIsFlags(Boolean value) {
		this.isFlags = value;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * @return the annotation
	 */
	public List<Annotation> getAnnotation() {
		if (annotation == null) {
			annotation = new ArrayList<Annotation>();
		}
		return annotation;
	}

	/**
	 * @param annotation
	 *            the annotation to set
	 */
	public void setAnnotation(List<Annotation> annotation) {
		this.annotation = annotation;
	}

	/**
	 * @return the underlyingType
	 */
	public String getUnderlyingType() {
		return underlyingType;
	}

	/**
	 * @param underlyingType
	 *            the underlyingType to set
	 */
	public void setUnderlyingType(String underlyingType) {
		this.underlyingType = underlyingType;
	}

}
