/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.edm.v4;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OnDeleteAction.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="OnDeleteAction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Cascade"/>
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="SetDefault"/>
 *     &lt;enumeration value="SetNull"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OnDeleteAction")
@XmlEnum
public enum OnDeleteAction {

	@XmlEnumValue("Cascade")
	CASCADE("Cascade"), @XmlEnumValue("None")
	NONE("None"), @XmlEnumValue("SetDefault")
	SET_DEFAULT("SetDefault"), @XmlEnumValue("SetNull")
	SET_NULL("SetNull");
	private final String value;

	OnDeleteAction(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static OnDeleteAction fromValue(String v) {
		for (OnDeleteAction c : OnDeleteAction.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
