/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

import java.util.Hashtable;

/**
 * Represents a return type of a function import that its response is a
 * collection of complex type, i.e. Collection(ComplexType)
 */
public class ComplexTypeCollection extends BaseElement {
	private ODataProperty[] elements;
	private String name;

	/**
	 * Constructs a new complex type collection, by the given service operation
	 * parameter.
	 * 
	 * @param serviceOperation
	 *            - name of the service operation (function import name).
	 */
	public ComplexTypeCollection(String serviceOperation) {
		super(new Hashtable<String, String>());
		elements = new ODataProperty[0];
		this.name = serviceOperation;
	}

	/**
	 * Returns the properties of the collection.
	 * 
	 * @return the elements
	 */
	public ODataProperty[] getElements() {
		return elements;
	}

	/**
	 * Sets the properties of the collection.
	 * 
	 * @param elements
	 *            the elements to set
	 */
	public void setElements(ODataProperty[] elements) {
		this.elements = elements;
	}

	/**
	 * Returns the qualified name of the collection.
	 * 
	 * @return the qualified name of the collection.
	 */
	public String getQualifiedName() {
		if (!name.startsWith(D)) {
			name = D + name;
		}

		return name;
	}

	/**
	 * Returns the name of the collection.
	 * 
	 * @return the name of the collection.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the collection.
	 * 
	 * @param name
	 *            - the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
