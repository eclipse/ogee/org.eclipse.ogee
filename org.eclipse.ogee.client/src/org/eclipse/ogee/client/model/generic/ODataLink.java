/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

/**
 * Describes an association between two Entries. <br>
 * More specifically, the term refers to a unidirectional association or to one
 * direction of a bidirectional association.
 * 
 * 
 */
public interface ODataLink {

	/**
	 * @return the title
	 */
	public String getTitle();

	/**
	 * @return the inline entry or collection of entries
	 */
	public InlineData getInlineData();

}
