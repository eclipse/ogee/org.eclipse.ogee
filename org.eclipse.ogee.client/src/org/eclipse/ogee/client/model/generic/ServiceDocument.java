/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

/**
 * To aid discovery of Collections by clients it is useful for the service to
 * expose a Service Document that lists the available Collections.
 */
public interface ServiceDocument {
	/**
	 * Returns the collections of this service document.
	 * 
	 * @return array of Collections
	 */
	public Object[] getCollections();

	/**
	 * Sets the collections of this service document.
	 * 
	 * @param collections
	 */
	public void setCollections(Object[] collections);
}
