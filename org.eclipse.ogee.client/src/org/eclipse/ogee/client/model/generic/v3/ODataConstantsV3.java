/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic.v3;

import org.eclipse.ogee.client.model.generic.ODataConstants;

/**
 * A class the represents constants.
 */
public interface ODataConstantsV3 extends ODataConstants {
	public static final String URL = "Url"; //$NON-NLS-1$
	public static final String VALUE = "Value"; //$NON-NLS-1$
	public static final String UNDERLYING_TYPE = "UnderlyingType"; //$NON-NLS-1$
	public static final String ENUM_TYPE = "EnumType"; //$NON-NLS-1$
	public static final String MEMBER = "Member"; //$NON-NLS-1$
	public static final String EDMX_REFERENCE = "Reference"; //$NON-NLS-1$
	public static final String IS_SIDE_EFFECTING = "IsSideEffecting"; //$NON-NLS-1$
	public static final String IS_BINDABLE = "isBindable"; //$NON-NLS-1$
	public static final String VALUE_TERM = "ValueTerm"; //$NON-NLS-1$
	public static final String ANNOTATIONS = "Annotations"; //$NON-NLS-1$
	public static final String TARGET = "Target"; //$NON-NLS-1$
	public static final String VALUE_ANNOTAION = "ValueAnnotation"; //$NON-NLS-1$
	public static final String TYPE_ANNOTATION = "TypeAnnotation"; //$NON-NLS-1$
	public static final String TERM = "Term"; //$NON-NLS-1$
	public static final String PROPERTY_VALUE = "PropertyValue"; //$NON-NLS-1$
	public static final String RECORD_EXPRESSION = "Record"; //$NON-NLS-1$
	public static final String COLLECTION_EXPRESSION = "Collection"; //$NON-NLS-1$
	public static final String PATH_EXPRESSION = "Path"; //$NON-NLS-1$
	public static final String STRING_EXPRESSION = "String"; //$NON-NLS-1$
	public static final String INT_EXPRESSION = "Int"; //$NON-NLS-1$
	public static final String FLOAT_EXPRESSION = "Float"; //$NON-NLS-1$
	public static final String DECIMAL_EXPRESSION = "Decimal"; //$NON-NLS-1$
	public static final String BOOL_EXPRESSION = "Bool"; //$NON-NLS-1$
	public static final String DATETIME_EXPRESSION = "DateTime"; //$NON-NLS-1$
	public static final String DATETIMEOFFSET_EXPRESSION = "DateTimeOffset"; //$NON-NLS-1$
	public static final String GUID_EXPRESSION = "Guid"; //$NON-NLS-1$
	public static final String BINARY_EXPRESSION = "Binary"; //$NON-NLS-1$
	public static final String BYTE_EXPRESSION = "Byte"; //$NON-NLS-1$
}
