/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

import org.eclipse.ogee.client.model.atom.AppCollection;
import org.eclipse.ogee.client.model.atom.AppService;
import org.eclipse.ogee.client.model.atom.AppWorkspace;
import org.eclipse.ogee.client.model.atom.AtomEntry;
import org.eclipse.ogee.client.model.atom.AtomFeed;
import org.eclipse.ogee.client.model.atom.AtomLink;
import org.eclipse.ogee.client.model.atom.ODataPropertyImpl;
import org.eclipse.ogee.client.model.edm.v4.ApplyExpression;
import org.eclipse.ogee.client.model.edm.v4.BinaryConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.BoolConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.CollectionExpression;
import org.eclipse.ogee.client.model.edm.v4.DateConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.DateTimeOffsetConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.DecimalConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.FloatConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.GuidConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.IfExpression;
import org.eclipse.ogee.client.model.edm.v4.IntConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.LabeledElementExpression;
import org.eclipse.ogee.client.model.edm.v4.NavigationPropertyPathExpression;
import org.eclipse.ogee.client.model.edm.v4.PathExpression;
import org.eclipse.ogee.client.model.edm.v4.PropertyPathExpression;
import org.eclipse.ogee.client.model.edm.v4.RecordExpression;
import org.eclipse.ogee.client.model.edm.v4.StringConstantExpression;
import org.eclipse.ogee.client.model.edmx.Association;
import org.eclipse.ogee.client.model.edmx.AssociationSet;
import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.Dependent;
import org.eclipse.ogee.client.model.edmx.EdmxDataServices;
import org.eclipse.ogee.client.model.edmx.End;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.Key;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.client.model.edmx.Parameter;
import org.eclipse.ogee.client.model.edmx.Principal;
import org.eclipse.ogee.client.model.edmx.Property;
import org.eclipse.ogee.client.model.edmx.PropertyRef;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.model.edmx.Using;
import org.eclipse.ogee.client.model.edmx.v3.Annotations;
import org.eclipse.ogee.client.model.edmx.v3.AssociationSetV3;
import org.eclipse.ogee.client.model.edmx.v3.AssociationV3;
import org.eclipse.ogee.client.model.edmx.v3.ComplexTypeV3;
import org.eclipse.ogee.client.model.edmx.v3.EdmxV3;
import org.eclipse.ogee.client.model.edmx.v3.EntityContainerV3;
import org.eclipse.ogee.client.model.edmx.v3.EntitySetV3;
import org.eclipse.ogee.client.model.edmx.v3.EntityTypeV3;
import org.eclipse.ogee.client.model.edmx.v3.EnumType;
import org.eclipse.ogee.client.model.edmx.v3.FunctionImportV3;
import org.eclipse.ogee.client.model.edmx.v3.Member;
import org.eclipse.ogee.client.model.edmx.v3.NavigationPropertyV3;
import org.eclipse.ogee.client.model.edmx.v3.ParameterV3;
import org.eclipse.ogee.client.model.edmx.v3.PropertyV3;
import org.eclipse.ogee.client.model.edmx.v3.PropertyValue;
import org.eclipse.ogee.client.model.edmx.v3.Reference;
import org.eclipse.ogee.client.model.edmx.v3.SchemaV3;
import org.eclipse.ogee.client.model.edmx.v3.TypeAnnotation;
import org.eclipse.ogee.client.model.edmx.v3.ValueAnnotation;
import org.eclipse.ogee.client.model.edmx.v3.ValueTerm;
import org.eclipse.ogee.client.model.edmx.v3.expressions.Collection;
import org.eclipse.ogee.client.model.edmx.v3.expressions.Record;
import org.eclipse.ogee.client.model.edmx.v4.EdmxInclude;
import org.eclipse.ogee.client.model.edmx.v4.EdmxIncludeAnnotations;
import org.eclipse.ogee.client.model.edmx.v4.EdmxReference;
import org.eclipse.ogee.client.model.edmx.v4.EdmxV4;

public class Arrays {
	private static boolean isValidParameters(Object container, Object[] items,
			Object item) {
		if (container == null || items == null || item == null) {
			return false;
		}

		return true;
	}

	private static Object[] createNewArray(Object[] items, Object item) {
		int length = items.length;

		Object[] newItems = new Object[length + 1];
		System.arraycopy(items, 0, newItems, 0, length);
		newItems[length] = item;

		return newItems;
	}

	/**
	 * Add App Workspace to App Service.
	 * 
	 * @param appService
	 * @param workspaces
	 * @param workspace
	 */
	public static void add(AppService appService, AppWorkspace[] workspaces,
			AppWorkspace workspace) {
		if (isValidParameters(appService, workspaces, workspace)) {
			Object[] newObjs = createNewArray(workspaces, workspace);

			AppWorkspace[] newWorkspaces = new AppWorkspace[newObjs.length];
			System.arraycopy(newObjs, 0, newWorkspaces, 0, newObjs.length);
			appService.setAppWorkspaces(newWorkspaces);
		}
	}

	public static void add(EdmxReference edmxReference,
			EdmxInclude[] edmxIncludes, EdmxInclude edmxInclude) {
		if (isValidParameters(edmxReference, edmxIncludes, edmxInclude)) {
			Object[] newObjs = createNewArray(edmxIncludes, edmxInclude);

			EdmxInclude[] newIncludes = new EdmxInclude[newObjs.length];
			System.arraycopy(newObjs, 0, newIncludes, 0, newObjs.length);
			edmxReference.setEdmxIncludes(newIncludes);
		}
	}

	public static void add(EdmxReference edmxReference,
			EdmxIncludeAnnotations[] edmxIncludes,
			EdmxIncludeAnnotations edmxInclude) {
		if (isValidParameters(edmxReference, edmxIncludes, edmxInclude)) {
			Object[] newObjs = createNewArray(edmxIncludes, edmxInclude);

			EdmxIncludeAnnotations[] newIncludes = new EdmxIncludeAnnotations[newObjs.length];
			System.arraycopy(newObjs, 0, newIncludes, 0, newObjs.length);
			edmxReference.setEdmxIncludeAnnotations(newIncludes);
		}
	}

	/**
	 * Add App Collection to App Workspace.
	 * 
	 * @param appWorkspace
	 * @param collections
	 * @param collection
	 */
	public static void add(AppWorkspace appWorkspace,
			AppCollection[] collections, AppCollection collection) {
		if (isValidParameters(appWorkspace, collections, collection)) {
			Object[] newObjs = createNewArray(collections, collection);

			AppCollection[] newCollections = new AppCollection[newObjs.length];
			System.arraycopy(newObjs, 0, newCollections, 0, newObjs.length);
			appWorkspace.setCollections(newCollections);
		}
	}

	/**
	 * Add Atom Entry to Atom Feed.
	 * 
	 * @param atomFeed
	 * @param entries
	 * @param entry
	 */
	public static void add(AtomFeed atomFeed, AtomEntry[] entries,
			AtomEntry entry) {
		if (isValidParameters(atomFeed, entries, entry)) {
			Object[] newObjs = createNewArray(entries, entry);

			AtomEntry[] newEntries = new AtomEntry[newObjs.length];
			System.arraycopy(newObjs, 0, newEntries, 0, newObjs.length);
			atomFeed.setEntries(newEntries);
		}
	}

	/**
	 * Add Atom Link to App Collection.
	 * 
	 * @param appCollection
	 * @param links
	 * @param link
	 */
	public static void add(AppCollection appCollection, AtomLink[] links,
			AtomLink link) {
		if (isValidParameters(appCollection, links, link)) {
			Object[] newObjs = createNewArray(links, link);

			AtomLink[] newLinks = new AtomLink[newObjs.length];
			System.arraycopy(newObjs, 0, newLinks, 0, newObjs.length);
			appCollection.setLinks(newLinks);
		}
	}

	/**
	 * Add Atom Link to Atom Entry.
	 * 
	 * @param atomEntry
	 * @param links
	 * @param link
	 */
	public static void add(AtomEntry atomEntry, AtomLink[] links, AtomLink link) {
		if (isValidParameters(atomEntry, links, link)) {
			Object[] newObjs = createNewArray(links, link);

			AtomLink[] newLinks = new AtomLink[newObjs.length];
			System.arraycopy(newObjs, 0, newLinks, 0, newObjs.length);
			atomEntry.setLinks(newLinks);
		}
	}

	/**
	 * Add Atom Link to Atom Feed.
	 * 
	 * @param atomFeed
	 * @param links
	 * @param link
	 */
	public static void add(AtomFeed atomFeed, AtomLink[] links, AtomLink link) {
		if (isValidParameters(atomFeed, links, link)) {
			Object[] newObjs = createNewArray(links, link);

			AtomLink[] newLinks = new AtomLink[newObjs.length];
			System.arraycopy(newObjs, 0, newLinks, 0, newObjs.length);
			atomFeed.setLinks(newLinks);
		}
	}

	/**
	 * Add Schema to Edmx Data Services.
	 * 
	 * @param edmxDataServices
	 * @param schemas
	 * @param schema
	 */
	public static void add(EdmxDataServices edmxDataServices, Schema[] schemas,
			Schema schema) {
		if (isValidParameters(edmxDataServices, schemas, schema)) {
			Object[] newObjs = createNewArray(schemas, schema);

			Schema[] newSchemas = new Schema[newObjs.length];
			System.arraycopy(newObjs, 0, newSchemas, 0, newObjs.length);
			edmxDataServices.setSchemas(newSchemas);
		}
	}

	/**
	 * Add References to Edmx.
	 * 
	 * @param edmx
	 * @param references
	 * @param reference
	 */
	public static void add(EdmxV3 edmx, Reference[] references,
			Reference reference) {
		if (isValidParameters(edmx, references, reference)) {
			Object[] newObjs = createNewArray(references, reference);

			Reference[] newReferences = new Reference[newObjs.length];
			System.arraycopy(newObjs, 0, newReferences, 0, newObjs.length);
			edmx.setReferences(newReferences);
		}
	}

	/**
	 * Add References to Edmx.
	 * 
	 * @param edmx
	 * @param references
	 * @param reference
	 */
	public static void add(EdmxV4 edmx, EdmxReference[] references,
			EdmxReference reference) {
		if (isValidParameters(edmx, references, reference)) {
			Object[] newObjs = createNewArray(references, reference);

			EdmxReference[] newReferences = new EdmxReference[newObjs.length];
			System.arraycopy(newObjs, 0, newReferences, 0, newObjs.length);
			edmx.setEdmxReferences(newReferences);
		}
	}

	/**
	 * Add Entity Types to Schema.
	 * 
	 * @param schema
	 * @param entityTypes
	 * @param entityType
	 */
	public static void add(Schema schema, EntityType[] entityTypes,
			EntityType entityType) {
		if (isValidParameters(schema, entityTypes, entityType)) {
			Object[] newObjs = createNewArray(entityTypes, entityType);

			EntityType[] newEntityTypes = new EntityType[newObjs.length];
			System.arraycopy(newObjs, 0, newEntityTypes, 0, newObjs.length);
			schema.setEntityTypes(newEntityTypes);
		}
	}

	/**
	 * Add Usings to Schema.
	 * 
	 * @param schema
	 * @param usings
	 * @param using
	 */
	public static void add(Schema schema, Using[] usings, Using using) {
		if (isValidParameters(schema, usings, using)) {
			Object[] newObjs = createNewArray(usings, using);

			Using[] newUsings = new Using[newObjs.length];
			System.arraycopy(newObjs, 0, newUsings, 0, newObjs.length);
			schema.setUsings(newUsings);
		}
	}

	/**
	 * Add Complex Types to Schema.
	 * 
	 * @param schema
	 * @param complexTypes
	 * @param complexType
	 */
	public static void add(Schema schema, ComplexType[] complexTypes,
			ComplexType complexType) {
		if (isValidParameters(schema, complexTypes, complexType)) {
			Object[] newObjs = createNewArray(complexTypes, complexType);

			ComplexType[] newComplexTypes = new ComplexType[newObjs.length];
			System.arraycopy(newObjs, 0, newComplexTypes, 0, newObjs.length);
			schema.setComplexTypes(newComplexTypes);
		}
	}

	/**
	 * Add Enum Types to Schema.
	 * 
	 * @param schema
	 * @param enumTypes
	 * @param enumType
	 */
	public static void add(SchemaV3 schema, EnumType[] enumTypes,
			EnumType enumType) {
		if (isValidParameters(schema, enumTypes, enumType)) {
			Object[] newObjs = createNewArray(enumTypes, enumType);

			EnumType[] newEnumTypes = new EnumType[newObjs.length];
			System.arraycopy(newObjs, 0, newEnumTypes, 0, newObjs.length);
			schema.setEnumTypes(newEnumTypes);
		}
	}

	/**
	 * Add Members to Enum Type.
	 * 
	 * @param enumType
	 * @param members
	 * @param member
	 */
	public static void add(EnumType enumType, Member[] members, Member member) {
		if (isValidParameters(enumType, members, member)) {
			Object[] newObjs = createNewArray(members, member);

			Member[] newMembers = new Member[newObjs.length];
			System.arraycopy(newObjs, 0, newMembers, 0, newObjs.length);
			enumType.setMembers(newMembers);
		}
	}

	/**
	 * Add Associations to Schema.
	 * 
	 * @param schema
	 * @param associations
	 * @param association
	 */
	public static void add(Schema schema, Association[] associations,
			Association association) {
		if (isValidParameters(schema, associations, association)) {
			Object[] newObjs = createNewArray(associations, association);

			Association[] newAssociations = new Association[newObjs.length];
			System.arraycopy(newObjs, 0, newAssociations, 0, newObjs.length);
			schema.setAssociations(newAssociations);
		}
	}

	/**
	 * Add Entity Containers to Schema.
	 * 
	 * @param schema
	 * @param entityContainers
	 * @param entityContainer
	 */
	public static void add(Schema schema, EntityContainer[] entityContainers,
			EntityContainer entityContainer) {
		if (isValidParameters(schema, entityContainers, entityContainer)) {
			Object[] newObjs = createNewArray(entityContainers, entityContainer);

			EntityContainer[] newEntityContainers = new EntityContainer[newObjs.length];
			System.arraycopy(newObjs, 0, newEntityContainers, 0, newObjs.length);
			schema.setEntityContainers(newEntityContainers);
		}
	}

	/**
	 * Add Properties to Entity Type.
	 * 
	 * @param entityType
	 * @param properties
	 * @param property
	 */
	public static void add(EntityType entityType, Property[] properties,
			Property property) {
		if (isValidParameters(entityType, properties, property)) {
			Object[] newObjs = createNewArray(properties, property);

			Property[] newPropertys = new Property[newObjs.length];
			System.arraycopy(newObjs, 0, newPropertys, 0, newObjs.length);
			entityType.setProperties(newPropertys);
		}
	}

	/**
	 * Add Navigation Properties to Entity Type.
	 * 
	 * @param entityType
	 * @param navigationProperties
	 * @param navigationProperty
	 */
	public static void add(EntityType entityType,
			NavigationProperty[] navigationProperties,
			NavigationProperty navigationProperty) {
		if (isValidParameters(entityType, navigationProperties,
				navigationProperty)) {
			Object[] newObjs = createNewArray(navigationProperties,
					navigationProperty);

			NavigationProperty[] newNavigationProperties = new NavigationProperty[newObjs.length];
			System.arraycopy(newObjs, 0, newNavigationProperties, 0,
					newObjs.length);
			entityType.setNavigationProperties(newNavigationProperties);
		}
	}

	/**
	 * Add Properties to Complex Type.
	 * 
	 * @param complexType
	 * @param properties
	 * @param property
	 */
	public static void add(ComplexType complexType, Property[] properties,
			Property property) {
		if (isValidParameters(complexType, properties, property)) {
			Object[] newObjs = createNewArray(properties, property);

			Property[] newProperties = new Property[newObjs.length];
			System.arraycopy(newObjs, 0, newProperties, 0, newObjs.length);
			complexType.setProperties(newProperties);
		}
	}

	/**
	 * Add Ends to Association.
	 * 
	 * @param association
	 * @param ends
	 * @param end
	 */
	public static void add(Association association, End[] ends, End end) {
		if (isValidParameters(association, ends, end)) {
			Object[] newObjs = createNewArray(ends, end);

			End[] newEnds = new End[newObjs.length];
			System.arraycopy(newObjs, 0, newEnds, 0, newObjs.length);
			association.setEnds(newEnds);
		}
	}

	/**
	 * Add Entity Sets to Entity Container.
	 * 
	 * @param entityContainer
	 * @param entitySets
	 * @param entitySet
	 */
	public static void add(EntityContainer entityContainer,
			EntitySet[] entitySets, EntitySet entitySet) {
		if (isValidParameters(entityContainer, entitySets, entitySet)) {
			Object[] newObjs = createNewArray(entitySets, entitySet);

			EntitySet[] newEntitySets = new EntitySet[newObjs.length];
			System.arraycopy(newObjs, 0, newEntitySets, 0, newObjs.length);
			entityContainer.setEntitySets(newEntitySets);
		}
	}

	/**
	 * Add Association Sets to Entity Container.
	 * 
	 * @param entityContainer
	 * @param associationSets
	 * @param associationSet
	 */
	public static void add(EntityContainer entityContainer,
			AssociationSet[] associationSets, AssociationSet associationSet) {
		if (isValidParameters(entityContainer, associationSets, associationSet)) {
			Object[] newObjs = createNewArray(associationSets, associationSet);

			AssociationSet[] newAssociationSets = new AssociationSet[newObjs.length];
			System.arraycopy(newObjs, 0, newAssociationSets, 0, newObjs.length);
			entityContainer.setAssociationSets(newAssociationSets);
		}
	}

	/**
	 * Add Function Imports to Entity Container.
	 * 
	 * @param entityContainer
	 * @param functionImports
	 * @param functionImport
	 */
	public static void add(EntityContainer entityContainer,
			FunctionImport[] functionImports, FunctionImport functionImport) {
		if (isValidParameters(entityContainer, functionImports, functionImport)) {
			Object[] newObjs = createNewArray(functionImports, functionImport);

			FunctionImport[] newFunctionImports = new FunctionImport[newObjs.length];
			System.arraycopy(newObjs, 0, newFunctionImports, 0, newObjs.length);
			entityContainer.setFunctionImports(newFunctionImports);
		}
	}

	/**
	 * Add Property Refs to Key.
	 * 
	 * @param key
	 * @param propertyRefs
	 * @param propertyRef
	 */
	public static void add(Key key, PropertyRef[] propertyRefs,
			PropertyRef propertyRef) {
		if (isValidParameters(key, propertyRefs, propertyRef)) {
			Object[] newObjs = createNewArray(propertyRefs, propertyRef);

			PropertyRef[] newPropertyRefs = new PropertyRef[newObjs.length];
			System.arraycopy(newObjs, 0, newPropertyRefs, 0, newObjs.length);
			key.setPropertyRefs(newPropertyRefs);
		}
	}

	/**
	 * Add Property Refs to Principal.
	 * 
	 * @param principal
	 * @param propertyRefs
	 * @param propertyRef
	 */
	public static void add(Principal principal, PropertyRef[] propertyRefs,
			PropertyRef propertyRef) {
		if (isValidParameters(principal, propertyRefs, propertyRef)) {
			Object[] newObjs = createNewArray(propertyRefs, propertyRef);

			PropertyRef[] newPropertyRefs = new PropertyRef[newObjs.length];
			System.arraycopy(newObjs, 0, newPropertyRefs, 0, newObjs.length);
			principal.setPropertyRefs(newPropertyRefs);
		}
	}

	/**
	 * Add Property Refs to Dependent.
	 * 
	 * @param dependent
	 * @param propertyRefs
	 * @param propertyRef
	 */
	public static void add(Dependent dependent, PropertyRef[] propertyRefs,
			PropertyRef propertyRef) {
		if (isValidParameters(dependent, propertyRefs, propertyRef)) {
			Object[] newObjs = createNewArray(propertyRefs, propertyRef);

			PropertyRef[] newPropertyRefs = new PropertyRef[newObjs.length];
			System.arraycopy(newObjs, 0, newPropertyRefs, 0, newObjs.length);
			dependent.setPropertyRefs(newPropertyRefs);
		}
	}

	/**
	 * Add Ends to Association Set.
	 * 
	 * @param associationSet
	 * @param ends
	 * @param end
	 */
	public static void add(AssociationSet associationSet, End[] ends, End end) {
		if (isValidParameters(associationSet, ends, end)) {
			Object[] newObjs = createNewArray(ends, end);

			End[] newEnds = new End[newObjs.length];
			System.arraycopy(newObjs, 0, newEnds, 0, newObjs.length);
			associationSet.setEnds(newEnds);
		}
	}

	/**
	 * Add Parameters to Function Import.
	 * 
	 * @param functionImport
	 * @param parameters
	 * @param parameter
	 */
	public static void add(FunctionImport functionImport,
			Parameter[] parameters, Parameter parameter) {
		if (isValidParameters(functionImport, parameters, parameter)) {
			Object[] newObjs = createNewArray(parameters, parameter);

			Parameter[] newParameters = new Parameter[newObjs.length];
			System.arraycopy(newObjs, 0, newParameters, 0, newObjs.length);
			functionImport.setParameters(newParameters);
		}
	}

	/**
	 * Add Value Terms to Schema.
	 * 
	 * @param schema
	 * @param valueTerms
	 * @param valueTerm
	 */
	public static void add(SchemaV3 schema, ValueTerm[] valueTerms,
			ValueTerm valueTerm) {
		if (isValidParameters(schema, valueTerms, valueTerm)) {
			Object[] newObjs = createNewArray(valueTerms, valueTerm);

			ValueTerm[] newValueTerms = new ValueTerm[newObjs.length];
			System.arraycopy(newObjs, 0, newValueTerms, 0, newObjs.length);
			schema.setValueTerms(newValueTerms);
		}
	}

	/**
	 * Add Value Annotations to Annotations.
	 * 
	 * @param annotations
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(Annotations annotations,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(annotations, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotations = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotations, 0, newObjs.length);
			annotations.setValueAnnotations(newValueAnnotations);
		}
	}

	/**
	 * Add annotations to Schema.
	 * 
	 * @param schema
	 * @param annotationsArray
	 * @param annotations
	 */
	public static void add(SchemaV3 schema, Annotations[] annotationsArray,
			Annotations annotations) {
		if (isValidParameters(schema, annotationsArray, annotations)) {
			Object[] newObjs = createNewArray(annotationsArray, annotations);

			Annotations[] newAnnotationsArray = new Annotations[newObjs.length];
			System.arraycopy(newObjs, 0, newAnnotationsArray, 0, newObjs.length);
			schema.setAnnotations(newAnnotationsArray);
		}
	}

	/**
	 * Add Type Annotations to Annotation.
	 * 
	 * @param annotations
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(Annotations annotations,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(annotations, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			annotations.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Property Values to Type Annotation.
	 * 
	 * @param typeAnnotation
	 * @param propertyValues
	 * @param propertyValue
	 */
	public static void add(TypeAnnotation typeAnnotation,
			PropertyValue[] propertyValues, PropertyValue propertyValue) {
		if (isValidParameters(typeAnnotation, propertyValues, propertyValue)) {
			Object[] newObjs = createNewArray(propertyValues, propertyValue);

			PropertyValue[] newPropertyValuesArray = new PropertyValue[newObjs.length];
			System.arraycopy(newObjs, 0, newPropertyValuesArray, 0,
					newObjs.length);
			typeAnnotation.setPropertyValues(newPropertyValuesArray);
		}
	}

	/**
	 * Add Property Values to Record.
	 * 
	 * @param record
	 * @param propertyValues
	 * @param propertyValue
	 */
	public static void add(Record record, PropertyValue[] propertyValues,
			PropertyValue propertyValue) {
		if (isValidParameters(record, propertyValues, propertyValue)) {
			Object[] newObjs = createNewArray(propertyValues, propertyValue);

			PropertyValue[] newPropertyValuesArray = new PropertyValue[newObjs.length];
			System.arraycopy(newObjs, 0, newPropertyValuesArray, 0,
					newObjs.length);
			record.setPropertyValues(newPropertyValuesArray);
		}
	}

	public static void add(Collection collection, Object[] childExpressions,
			Record record) {
		if (isValidParameters(collection, childExpressions, record)) {
			Object[] newObjs = createNewArray(childExpressions, record);

			Object[] newChildExpressionsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newChildExpressionsArray, 0,
					newObjs.length);
			collection.setChildExpressions(newChildExpressionsArray);
		}
	}

	public static void add(CollectionExpression collection,
			Object[] childExpressions, Object record) {
		if (isValidParameters(collection, childExpressions, record)) {
			Object[] newObjs = createNewArray(childExpressions, record);

			Object[] newChildExpressionsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newChildExpressionsArray, 0,
					newObjs.length);
			collection.setChildExpressions(newChildExpressionsArray);
		}
	}

	/**
	 * Add Type Annotations to Entity Type.
	 * 
	 * @param entityType
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(EntityTypeV3 entityType,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(entityType, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			entityType.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Value Annotations to Entity Type.
	 * 
	 * @param entityType
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(EntityTypeV3 entityType,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(entityType, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			entityType.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	/**
	 * Add Type Annotations to Complex Type.
	 * 
	 * @param complexType
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(ComplexTypeV3 complexType,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(complexType, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			complexType.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Value Annotations to Complex Type.
	 * 
	 * @param complexType
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(ComplexTypeV3 complexType,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(complexType, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			complexType.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	/**
	 * Add Type Annotations to Association.
	 * 
	 * @param association
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(AssociationV3 association,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(association, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			association.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Value Annotations to Association.
	 * 
	 * @param association
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(AssociationV3 association,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(association, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			association.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	/**
	 * Add Type Annotations to Navigation Property.
	 * 
	 * @param navigationProperty
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(NavigationPropertyV3 navigationProperty,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(navigationProperty, typeAnnotations,
				typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			navigationProperty.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Value Annotations to Navigation Property.
	 * 
	 * @param navigationProperty
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(NavigationPropertyV3 navigationProperty,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(navigationProperty, valueAnnotations,
				valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			navigationProperty.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	/**
	 * Add Type Annotations to Entity Container.
	 * 
	 * @param container
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(EntityContainerV3 container,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(container, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			container.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Value Annotations to Entity Container.
	 * 
	 * @param container
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(EntityContainerV3 container,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(container, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			container.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	/**
	 * Add Type Annotations to Association Set.
	 * 
	 * @param associationSet
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(AssociationSetV3 associationSet,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(associationSet, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			associationSet.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Value Annotations to Association Set.
	 * 
	 * @param associationSet
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(AssociationSetV3 associationSet,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(associationSet, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			associationSet.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	/**
	 * Add Type Annotations to Entity Set.
	 * 
	 * @param entitySet
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(EntitySetV3 entitySet,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(entitySet, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			entitySet.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Value Annotations to Entity Set.
	 * 
	 * @param entitySet
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(EntitySetV3 entitySet,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(entitySet, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			entitySet.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	/**
	 * Add Type Annotations to Function Import.
	 * 
	 * @param functionImport
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(FunctionImportV3 functionImport,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(functionImport, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			functionImport.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Value Annotations to Function Import.
	 * 
	 * @param functionImport
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(FunctionImportV3 functionImport,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(functionImport, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			functionImport.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	/**
	 * Add Type Annotations to Parameter.
	 * 
	 * @param parameter
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(ParameterV3 parameter,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(parameter, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			parameter.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Value Annotations to Parameter.
	 * 
	 * @param parameter
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(ParameterV3 parameter,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(parameter, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			parameter.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	/**
	 * Add Type Annotations to Property.
	 * 
	 * @param property
	 * @param typeAnnotations
	 * @param typeAnnotation
	 */
	public static void add(PropertyV3 property,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(property, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			property.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	/**
	 * Add Value Annotations to Property.
	 * 
	 * @param property
	 * @param valueAnnotations
	 * @param valueAnnotation
	 */
	public static void add(PropertyV3 property,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(property, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			property.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	public static void add(Collection collection, Object[] childExpressions,
			Object childExpression) {
		if (isValidParameters(collection, childExpressions, childExpression)) {
			Object[] newObjs = createNewArray(childExpressions, childExpression);

			Object[] newchildExpressionsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newchildExpressionsArray, 0,
					newObjs.length);
			collection.setChildExpressions(newchildExpressionsArray);
		}
	}

	public static void add(ValueTerm valueTerm,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(valueTerm, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			valueTerm.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	public static void add(ValueTerm valueTerm,
			TypeAnnotation[] typeAnnotations, TypeAnnotation typeAnnotation) {
		if (isValidParameters(valueTerm, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			valueTerm.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	public static void add(EnumType enumType,
			ValueAnnotation[] valueAnnotations, ValueAnnotation valueAnnotation) {
		if (isValidParameters(enumType, valueAnnotations, valueAnnotation)) {
			Object[] newObjs = createNewArray(valueAnnotations, valueAnnotation);

			ValueAnnotation[] newValueAnnotationsArray = new ValueAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newValueAnnotationsArray, 0,
					newObjs.length);
			enumType.setValueAnnotations(newValueAnnotationsArray);
		}
	}

	public static void add(EnumType enumType, TypeAnnotation[] typeAnnotations,
			TypeAnnotation typeAnnotation) {
		if (isValidParameters(enumType, typeAnnotations, typeAnnotation)) {
			Object[] newObjs = createNewArray(typeAnnotations, typeAnnotation);

			TypeAnnotation[] newTypeAnnotationsArray = new TypeAnnotation[newObjs.length];
			System.arraycopy(newObjs, 0, newTypeAnnotationsArray, 0,
					newObjs.length);
			enumType.setTypeAnnotations(newTypeAnnotationsArray);
		}
	}

	public static void add(ComplexTypeCollection root,
			ODataProperty[] elements, ODataPropertyImpl element) {
		if (isValidParameters(root, elements, element)) {
			Object[] newObjs = createNewArray(elements, element);

			ODataPropertyImpl[] newElementsArray = new ODataPropertyImpl[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			root.setElements(newElementsArray);
		}

	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, RecordExpression record) {
		if (isValidParameters(applyExpression, childExpressions, record)) {
			Object[] newObjs = createNewArray(childExpressions, record);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, PathExpression path) {
		if (isValidParameters(applyExpression, childExpressions, path)) {
			Object[] newObjs = createNewArray(childExpressions, path);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, StringConstantExpression primitiveExpr) {
		if (isValidParameters(applyExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, IntConstantExpression primitiveExpr) {
		if (isValidParameters(applyExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, FloatConstantExpression primitiveExpr) {
		if (isValidParameters(applyExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, DecimalConstantExpression primitiveExpr) {
		if (isValidParameters(applyExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, BoolConstantExpression primitiveExpr) {
		if (isValidParameters(applyExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, DateConstantExpression primitiveExpr) {
		if (isValidParameters(applyExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions,
			DateTimeOffsetConstantExpression primitiveExpr) {
		if (isValidParameters(applyExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, GuidConstantExpression primitiveExpr) {
		if (isValidParameters(applyExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, BinaryConstantExpression primitiveExpr) {
		if (isValidParameters(applyExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, PropertyPathExpression path) {
		if (isValidParameters(applyExpression, childExpressions, path)) {
			Object[] newObjs = createNewArray(childExpressions, path);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(ApplyExpression applyExpression,
			Object[] childExpressions, NavigationPropertyPathExpression path) {
		if (isValidParameters(applyExpression, childExpressions, path)) {
			Object[] newObjs = createNewArray(childExpressions, path);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			applyExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, RecordExpression expression) {
		if (isValidParameters(ifExpression, childExpressions, expression)) {
			Object[] newObjs = createNewArray(childExpressions, expression);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, StringConstantExpression primitiveExpr) {
		if (isValidParameters(ifExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, IntConstantExpression primitiveExpr) {
		if (isValidParameters(ifExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, FloatConstantExpression primitiveExpr) {
		if (isValidParameters(ifExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, DecimalConstantExpression primitiveExpr) {
		if (isValidParameters(ifExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, BoolConstantExpression primitiveExpr) {
		if (isValidParameters(ifExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, DateConstantExpression primitiveExpr) {
		if (isValidParameters(ifExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions,
			DateTimeOffsetConstantExpression primitiveExpr) {
		if (isValidParameters(ifExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, GuidConstantExpression primitiveExpr) {
		if (isValidParameters(ifExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, BinaryConstantExpression primitiveExpr) {
		if (isValidParameters(ifExpression, childExpressions, primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, PathExpression path) {
		if (isValidParameters(ifExpression, childExpressions, path)) {
			Object[] newObjs = createNewArray(childExpressions, path);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, PropertyPathExpression path) {
		if (isValidParameters(ifExpression, childExpressions, path)) {
			Object[] newObjs = createNewArray(childExpressions, path);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(IfExpression ifExpression,
			Object[] childExpressions, NavigationPropertyPathExpression path) {
		if (isValidParameters(ifExpression, childExpressions, path)) {
			Object[] newObjs = createNewArray(childExpressions, path);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			ifExpression.setChildExpressions(newElementsArray);
		}
	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, RecordExpression record) {
		if (isValidParameters(labeledElementExpression, childExpressions,
				record)) {
			Object[] newObjs = createNewArray(childExpressions, record);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, StringConstantExpression primitiveExpr) {
		if (isValidParameters(labeledElementExpression, childExpressions,
				primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, IntConstantExpression primitiveExpr) {
		if (isValidParameters(labeledElementExpression, childExpressions,
				primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, FloatConstantExpression primitiveExpr) {
		if (isValidParameters(labeledElementExpression, childExpressions,
				primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, DecimalConstantExpression primitiveExpr) {
		if (isValidParameters(labeledElementExpression, childExpressions,
				primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, BoolConstantExpression primitiveExpr) {
		if (isValidParameters(labeledElementExpression, childExpressions,
				primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, DateConstantExpression primitiveExpr) {
		if (isValidParameters(labeledElementExpression, childExpressions,
				primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions,
			DateTimeOffsetConstantExpression primitiveExpr) {
		if (isValidParameters(labeledElementExpression, childExpressions,
				primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, GuidConstantExpression primitiveExpr) {
		if (isValidParameters(labeledElementExpression, childExpressions,
				primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, BinaryConstantExpression primitiveExpr) {
		if (isValidParameters(labeledElementExpression, childExpressions,
				primitiveExpr)) {
			Object[] newObjs = createNewArray(childExpressions, primitiveExpr);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, PathExpression path) {
		if (isValidParameters(labeledElementExpression, childExpressions, path)) {
			Object[] newObjs = createNewArray(childExpressions, path);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, PropertyPathExpression path) {
		if (isValidParameters(labeledElementExpression, childExpressions, path)) {
			Object[] newObjs = createNewArray(childExpressions, path);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}

	public static void add(LabeledElementExpression labeledElementExpression,
			Object[] childExpressions, NavigationPropertyPathExpression path) {
		if (isValidParameters(labeledElementExpression, childExpressions, path)) {
			Object[] newObjs = createNewArray(childExpressions, path);

			Object[] newElementsArray = new Object[newObjs.length];
			System.arraycopy(newObjs, 0, newElementsArray, 0, newObjs.length);
			labeledElementExpression.setChildExpressions(newElementsArray);
		}

	}
}
