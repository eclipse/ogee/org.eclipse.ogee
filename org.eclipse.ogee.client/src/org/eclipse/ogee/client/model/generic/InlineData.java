/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

/**
 * A related Entry or collection of Entries is represented as the child element
 * of an "m:inline" element.
 * 
 * 
 */
public interface InlineData {
	/**
	 * Returns whether the element is a collection.
	 * 
	 * @return - true whether the element is a collection, false otherwise.
	 */
	public boolean isCollection();

	/**
	 * Returns the entry.
	 * 
	 * @return - inline entry
	 */
	public ODataEntry getEntry();

	/**
	 * Returns the entry's collection.
	 * 
	 * @return - entry's collection
	 */
	public ODataCollection getCollection();
}