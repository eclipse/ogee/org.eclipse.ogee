/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

/**
 * ODataCollection - A Resource that contains a set of Member Resources. <br>
 * In OData, a Collection is represented as an Atom Feed or an array of JSON
 * objects. <br>
 * Can be retrieved in whole or in part.
 * 
 * 
 */
public interface ODataCollection {
	/**
	 * @return Members of the ODataCollection
	 */
	ODataEntry[] getEntries();
}
