/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Base object to describe a xml tag/element. Every element has attributes.
 * 
 */
public class BaseElement implements ODataConstants {

	private Hashtable<String, String> attributes;

	/**
	 * Default constructor
	 */
	public BaseElement() {
		this.attributes = new Hashtable<String, String>();
	}

	/**
	 * @param attributes
	 */
	public BaseElement(Hashtable<String, String> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @param attributes
	 *            Hashtable with attributes to set
	 */
	public void setAttributes(Hashtable<String, String> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return copy of the attributes as a Hashtable
	 */
	public Hashtable<String, String> getAttributes() {
		Hashtable<String, String> copyOfAttributes = new Hashtable<String, String>();
		Enumeration<String> keys = attributes.keys();// $JL-COLLECTION$
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			copyOfAttributes.put(key, attributes.get(key));
		}
		return copyOfAttributes;
	}

	/**
	 * @param key
	 *            attribute key
	 * @param value
	 *            attribute value
	 */
	public void setAttribute(String key, String value) {
		attributes.put(key, value);
	}

	/**
	 * @param key
	 *            attribute key
	 * @return value of the attribute
	 */
	public String getAttribute(String key) {
		return attributes.get(key);
	}

	/**
	 * @param key
	 *            attribute key
	 * @return value of the removed attribute
	 */
	public String removeAttribute(String key) {
		return attributes.remove(key);
	}

	/**
	 * @return array with all the attributes keys
	 */
	public String[] getAttributesKeys() {
		String[] returnValue = new String[attributes.size()];
		Enumeration<String> keys = attributes.keys();// $JL-COLLECTION$
		for (int i = 0; keys.hasMoreElements(); i++) {
			String key = keys.nextElement();
			returnValue[i] = key;
		}
		return returnValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attributes == null) ? 0 : attributes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BaseElement other = (BaseElement) obj;
		if (attributes == null) {
			if (other.attributes != null) {
				return false;
			}
		} else if (!attributes.equals(other.attributes)) {
			return false;
		}
		return true;
	}

}
