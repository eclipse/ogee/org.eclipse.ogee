/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

/**
 * Utility class for working with booleans and strings.
 * 
 * 
 */
public class StringUtilities {
	/**
	 * 
	 * @param b
	 *            boolean object
	 * @return string value corresponding to the boolean object
	 */
	public static String stringValueOf(boolean b) {
		if (b)
			return Boolean.TRUE.toString();
		else
			return Boolean.FALSE.toString();
	}

	/**
	 * 
	 * @param str
	 *            string representation corresponding to a boolean object
	 * @return boolean object
	 */
	public static boolean stringToBoolean(String str) {
		if (str.equalsIgnoreCase(Boolean.TRUE.toString()))
			return true;
		else
			return false;
	}
}
