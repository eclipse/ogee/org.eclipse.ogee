/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

/**
 * ODataEntry (ODataEntry Resource) - Members of an ODataCollection, as defined
 * by [<a href="http://tools.ietf.org/html/rfc5023">RFC 5023</a>]. <br>
 * In OData, Entries are represented as Atom ODataEntry Documents, as described
 * in [<a href="http://www.odata.org/developers/protocols/atom-format">OData:
 * Atom</a>] or JSON objects, as described in [<a
 * href="http://www.odata.org/developers/protocols/JSON-format">OData:
 * JSON</a>].
 * 
 * 
 */
public interface ODataEntry {
	/**
	 * Returns the properties of the entry.
	 * 
	 * @return properties of the ODataEntry
	 */
	public ODataProperty[] getProperties();

	/**
	 * Returns the property of the entry by the given key.
	 * 
	 * @param key
	 *            name of property
	 * @return ODataProperty by key
	 */
	public ODataProperty getProperty(String key);

	/**
	 * Puts the given property in the entry.
	 * 
	 * @param property
	 */
	public void putProperty(ODataProperty property);

	/**
	 * Puts the given key, value pair as a property in the entry.
	 * 
	 * @param key
	 *            name of the property
	 * @param value
	 *            the string value of the property
	 */
	public void putProperty(String key, String value);

	/**
	 * Returns the links of the entry.
	 * 
	 * @return array of the entry links
	 */
	public ODataLink[] getLinks();

	/**
	 * Returns whether this entry is a media link. Media ODataLink ODataEntry -
	 * A special kind of ODataEntry Resource that contains metadata about a
	 * Media Resource, as defined in [RFC 5023].
	 * 
	 * @return true if entry id a media link entry, false otherwise.
	 */
	public boolean isMediaLinkEntry();

	/**
	 * Returns the media resource of the entry.
	 * 
	 * @return the media resource object
	 */
	public IMediaResource getMediaResource();

	/**
	 * Returns the id of the entry.
	 * 
	 * @return the entry identifier
	 */
	public String getId();

	/**
	 * Returns the type of the entry.
	 * 
	 * @return - the type of the entry.
	 */
	public String getType();
}
