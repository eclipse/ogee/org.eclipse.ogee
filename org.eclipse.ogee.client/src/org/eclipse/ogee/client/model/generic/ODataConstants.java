/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

/**
 * OData protocol constants.
 * 
 * 
 */
public interface ODataConstants {
	public static final char COLON_CHAR = ':';
	public static final String URI = "uri"; //$NON-NLS-1$
	public static final String HEIGHT = "height"; //$NON-NLS-1$
	public static final String D = "d:"; //$NON-NLS-1$
	public static final String M = "m:"; //$NON-NLS-1$
	public static final String SRC = "src"; //$NON-NLS-1$
	public static final String NAME = "Name"; //$NON-NLS-1$
	public static final String BASE_TYPE = "BaseType"; //$NON-NLS-1$
	public static final String ABSTRACT = "Abstract"; //$NON-NLS-1$
	public static final String TITLE = "title"; //$NON-NLS-1$
	public static final String TYPE = "Type"; //$NON-NLS-1$
	public static final String ATOM_LINK_TYPE = "type"; //$NON-NLS-1$
	public static final String ATOM_CONTENT_TYPE = "type"; //$NON-NLS-1$
	public static final String REL = "rel"; //$NON-NLS-1$
	public static final String HREF = "href"; //$NON-NLS-1$
	public static final String XMLNS = "xmlns:"; //$NON-NLS-1$
	public static final String XMLNS_D = "xmlns:d"; //$NON-NLS-1$
	public static final String XMLNS_M = "xmlns:m"; //$NON-NLS-1$
	public static final String M_INLINE = "m:inline"; //$NON-NLS-1$
	public static final String M_PROPERTIES = "m:properties"; //$NON-NLS-1$
	public static final String XML_BASE = "xml:base"; //$NON-NLS-1$
	public static final String ATOM = "atom:"; //$NON-NLS-1$
	public static final String ATOM_LINK = "atom:link"; //$NON-NLS-1$
	public static final String ATOM_CONTENT = "atom:content"; //$NON-NLS-1$
	public static final String ATOM_ENTRY = "atom:entry"; //$NON-NLS-1$
	public static final String ATOM_UPDATED = "atom:updated"; //$NON-NLS-1$
	public static final String ATOM_TITLE = "atom:title"; //$NON-NLS-1$
	public static final String ATOM_ID = "atom:id"; //$NON-NLS-1$
	public static final String ATOM_FEED = "atom:feed"; //$NON-NLS-1$
	public static final String ATOM_AUTHOR = "atom:author"; //$NON-NLS-1$
	public static final String ATOM_CATEGORY = "atom:category"; //$NON-NLS-1$
	public static final String ATOM_NAME = "atom:name"; //$NON-NLS-1$
	public static final String ATOM_URI = "atom:uri"; //$NON-NLS-1$
	public static final String ATOM_EMAIL = "atom:email"; //$NON-NLS-1$
	public static final String ATOM_SUMMARY = "atom:summary"; //$NON-NLS-1$
	public static final String LINK = "link"; //$NON-NLS-1$
	public static final String CONTENT = "content"; //$NON-NLS-1$
	public static final String ENTRY = "entry"; //$NON-NLS-1$
	public static final String UPDATED = "updated"; //$NON-NLS-1$
	public static final String TITLE_TAG = "title"; //$NON-NLS-1$
	public static final String ID = "id"; //$NON-NLS-1$
	public static final String FEED = "feed"; //$NON-NLS-1$
	public static final String AUTHOR = "author"; //$NON-NLS-1$
	public static final String CATEGORY = "category"; //$NON-NLS-1$
	public static final String TERM = "term"; //$NON-NLS-1$
	public static final String SCHEME = "scheme"; //$NON-NLS-1$
	public static final String LABEL = "label"; //$NON-NLS-1$
	public static final String NAME_TAG = "name"; //$NON-NLS-1$
	public static final String URI_TAG = "uri"; //$NON-NLS-1$
	public static final String EMAIL = "email"; //$NON-NLS-1$
	public static final String SUMMARY = "summary"; //$NON-NLS-1$
	public static final String M_HAS_STREAM = "m:HasStream"; //$NON-NLS-1$
	public static final String M_IS_DEFAULT_ENTITY_CONTAINER = "m:IsDefaultEntityContainer"; //$NON-NLS-1$
	public static final String ROLE = "Role"; //$NON-NLS-1$
	public static final String VERSION = "Version"; //$NON-NLS-1$
	public static final String M_DATA_SERVICE_VERSION = "m:DataServiceVersion"; //$NON-NLS-1$
	public static final String ASSOCIATION = "Association"; //$NON-NLS-1$
	public static final String MULTIPLICITY = "Multiplicity"; //$NON-NLS-1$
	public static final String ENTITY_TYPE = "EntityType"; //$NON-NLS-1$
	public static final String ENTITY_SET = "EntitySet"; //$NON-NLS-1$
	public static final String RETURN_TYPE = "ReturnType"; //$NON-NLS-1$
	public static final String M_HTTP_METHOD = "m:HttpMethod"; //$NON-NLS-1$
	public static final String TOP_LEVEL = "top-level"; //$NON-NLS-1$
	public static final String DISPLAY_ORDER = "display-order"; //$NON-NLS-1$
	public static final String RELATIONSHIP = "Relationship"; //$NON-NLS-1$
	public static final String FROM_ROLE = "FromRole"; //$NON-NLS-1$
	public static final String TO_ROLE = "ToRole"; //$NON-NLS-1$
	public static final String MODE = "Mode"; //$NON-NLS-1$
	public static final String MAX_LENGTH = "MaxLength"; //$NON-NLS-1$
	public static final String M_FC_TARGET_PATH = "m:FC_TargetPath"; //$NON-NLS-1$
	public static final String M_FC_KEEP_IN_CONTENT = "m:FC_KeepInContent"; //$NON-NLS-1$
	public static final String NULLABLE = "Nullable"; //$NON-NLS-1$
	public static final String FIXED_LENGTH = "FixedLength"; //$NON-NLS-1$
	public static final String UNICODE = "Unicode"; //$NON-NLS-1$
	public static final String NAMESPACE = "Namespace"; //$NON-NLS-1$
	public static final String ALIAS = "Alias"; //$NON-NLS-1$
	public static final String M_TYPE = "m:Type"; //$NON-NLS-1$
	public static final String APP = "app:"; //$NON-NLS-1$
	public static final String APP_COLLECTION = "app:collection"; //$NON-NLS-1$
	public static final String COLLECTION = "collection"; //$NON-NLS-1$
	public static final String APP_WORKSPACE = "app:workspace"; //$NON-NLS-1$
	public static final String WORKSPACE = "workspace"; //$NON-NLS-1$
	public static final String APP_SERVICE = "app:service"; //$NON-NLS-1$
	public static final String SERVICE = "service"; //$NON-NLS-1$
	public static final String EDMX_EDMX = "Edmx"; //$NON-NLS-1$
	public static final String EDMX_DATASERVICES = "DataServices"; //$NON-NLS-1$
	public static final String SCHEMA = "Schema"; //$NON-NLS-1$
	public static final String COMPLEX_TYPE = "ComplexType"; //$NON-NLS-1$
	public static final String USING = "Using"; //$NON-NLS-1$
	public static final String ENTITY_CONTAINER = "EntityContainer"; //$NON-NLS-1$
	public static final String KEY = "Key"; //$NON-NLS-1$
	public static final String PROPERTY = "Property"; //$NON-NLS-1$
	public static final String PARAMETER = "Parameter"; //$NON-NLS-1$
	public static final String DOCUMENTATION = "Documentation"; //$NON-NLS-1$
	public static final String LONG_DESCRIPTION = "LongDescription"; //$NON-NLS-1$
	public static final String DOCUMENTATION_SUMMARY = "Summary"; //$NON-NLS-1$
	public static final String END = "End"; //$NON-NLS-1$
	public static final String ASSOCIATION_SET = "AssociationSet"; //$NON-NLS-1$
	public static final String PROPERTY_REF = "PropertyRef"; //$NON-NLS-1$
	public static final String NAVIGATION_PROPERTY = "NavigationProperty"; //$NON-NLS-1$
	public static final String REFERENTIAL_CONSTRAINT = "ReferentialConstraint"; //$NON-NLS-1$
	public static final String PRINCIPAL = "Principal"; //$NON-NLS-1$
	public static final String DEPENDENT = "Dependent"; //$NON-NLS-1$
	public static final String FUNCTION_IMPORT = "FunctionImport"; //$NON-NLS-1$
	// constants of the ServicedocumentCollection properties (metadata
	// processing service):
	public static final String SERVICE_NAME_PROP_1_0 = "d:service_name"; //$NON-NLS-1$
	public static final String NAMESPACE_PROP_1_0 = "d:namespace"; //$NON-NLS-1$
	public static final String CHANGED_TIMESTAMP_PROP_1_0 = "d:changed_timestmp"; //$NON-NLS-1$
	public static final String CHANGED_BY_PROP_1_0 = "d:changed_by"; //$NON-NLS-1$
	public static final String OBJECT_NAME_PROP_1_0 = "d:object_name"; //$NON-NLS-1$
	public static final String CREATED_TIMESTAMP_PROP_1_0 = "d:created_timestmp"; //$NON-NLS-1$
	public static final String CREATED_BY_PROP_1_0 = "d:created_by"; //$NON-NLS-1$
	public static final String SRV_IDENTIFIER_PROP_1_0 = "d:srv_identifier"; //$NON-NLS-1$
	public static final String DESCRIPTION_PROP_1_0 = "d:description"; //$NON-NLS-1$
	public static final String VALUE_PROP_1_0 = "d:value"; //$NON-NLS-1$
	public static final String SCHEMA_AGENCY_ID_PROP_1_0 = "d:scheme_agency_id"; //$NON-NLS-1$
	public static final String SCHEMA_ID_PROP_1_0 = "d:scheme_id"; //$NON-NLS-1$
	public static final String SERVICE_VERSION_PROP_1_0 = "d:service_version"; //$NON-NLS-1$
	// constants of the ServiceCollection properties (service catalog service):
	public static final String SERVICE_NAME_PROP_2_0 = "d:Title"; //$NON-NLS-1$
	public static final String DESCRIPTION_PROP_2_0 = "d:Description"; //$NON-NLS-1$
	public static final String SERVICE_ID_PROP_2_0 = "d:ID"; //$NON-NLS-1$
	public static final String AUTHOR_PROP_2_0 = "d:Author"; //$NON-NLS-1$
	public static final String UPDATE_DATE_PROP_2_0 = "d:UpdatedDate"; //$NON-NLS-1$
	public static final String SERVICE_URL_PROP_2_0 = "d:ServiceUrl"; //$NON-NLS-1$
	public static final String METADATA_URL_PROP_2_0 = "d:MetadataUrl"; //$NON-NLS-1$
	public static final String IMAGE_URL_PROP_2_0 = "d:ImageUrl"; //$NON-NLS-1$
}
