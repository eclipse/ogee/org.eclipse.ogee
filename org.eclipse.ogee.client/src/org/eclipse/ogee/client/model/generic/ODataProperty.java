/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

/**
 * ODataProperty - A generic term to represent a declared or dynamic property of
 * an ODataEntry.
 * 
 * 
 */
public interface ODataProperty {
	/**
	 * @return The name of the property
	 */
	public String getName();

	/**
	 * @return The fully qualified name of the property (including namespace),
	 *         also referred as the property key
	 */
	public String getQualifiedName();

	/**
	 * @return The value of the property
	 */
	public String getValue();

	/**
	 * @return The collection value of the property
	 */
	public ODataCollection getCollectionValue();

	/**
	 * @return True if the property is a collection
	 */
	public boolean isCollectionTypeProperty();

	/**
	 * @return The type of the property
	 */
	public String getType();

	/**
	 * @return The sub properties of a complex type (non primitive) property
	 */
	public ODataProperty[] getChildProperties();

	/**
	 * @return True if the property is a complex type property
	 */
	public boolean isComplexTypeProperty();

	/**
	 * @param key
	 *            Name of property
	 * @return The sub property by key
	 */
	public ODataProperty getChildProperty(String key);

	/**
	 * Adds the given property as a sub property
	 * 
	 * @param property
	 */
	public void putChildProperty(ODataProperty property);

	/**
	 * Set The value of the property
	 */
	public void setValue(String convertToString);
}
