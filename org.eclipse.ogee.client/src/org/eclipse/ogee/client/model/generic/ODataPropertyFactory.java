/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

import org.eclipse.ogee.client.model.atom.ODataPropertyImpl;
import org.eclipse.ogee.client.model.json.ODataPropertyJson;
import org.eclipse.ogee.client.parser.Representation;
import org.eclipse.ogee.client.parser.impl.EdmTypes;

/**
 * Factory class to create ODataProperty
 * 
 * 
 */
public class ODataPropertyFactory {
	/**
	 * Creates an OData property by the given representation, name and type.
	 * 
	 * @param representation
	 *            - Enum representation of the OData service, e.g. ATOM or JSON
	 * @param name
	 * @param type
	 * @return ODataProperty implementation
	 */
	public static ODataProperty createProperty(Representation representation,
			String name, String type) {
		if ((representation == null) || (name == null) || (type == null)) {
			return null;
		}

		switch (representation) {
		case ATOM:
			ODataPropertyImpl property = new ODataPropertyImpl();
			property.setName(name);
			property.setType(type);
			return property;

		case JSON:
			ODataPropertyJson oDataPropertyJson = new ODataPropertyJson();
			oDataPropertyJson.setName(name);
			oDataPropertyJson.setType(EdmTypes.toEdmTypes(type));
			return oDataPropertyJson;

		default:
			return null;
		}
	}
}
