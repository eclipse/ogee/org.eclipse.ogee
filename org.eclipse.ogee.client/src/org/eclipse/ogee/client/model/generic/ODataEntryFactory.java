/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.model.generic;

import java.util.Hashtable;

import org.eclipse.ogee.client.model.atom.AtomContent;
import org.eclipse.ogee.client.model.atom.AtomEntry;
import org.eclipse.ogee.client.model.atom.Mproperties;
import org.eclipse.ogee.client.model.json.ODataEntryJson;
import org.eclipse.ogee.client.parser.Representation;

/**
 * Factory class to create ODataEntry
 * 
 * 
 */
public class ODataEntryFactory {
	/**
	 * Creates an OData entry by the given representation, title and base URL.
	 * 
	 * @param representation
	 *            Enum representation of the OData service, e.g. ATOM or JSON
	 * @param title
	 * @param baseUrl
	 * @return ODataEntry object.
	 */
	public static ODataEntry createEntry(Representation representation,
			String title, String baseUrl) {
		switch (representation) {
		case ATOM:
			AtomEntry atomEntry = new AtomEntry(null, null, title, null);
			atomEntry.setXmlBase(baseUrl);
			Hashtable<String, String> attributes = new Hashtable<String, String>(
					4);
			attributes.put("xmlns", "http://www.w3.org/2005/Atom"); //$NON-NLS-1$ //$NON-NLS-2$
			attributes
					.put("xmlns:d", "http://schemas.microsoft.com/ado/2007/08/dataservices"); //$NON-NLS-1$ //$NON-NLS-2$
			attributes
					.put("xmlns:m", "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"); //$NON-NLS-1$ //$NON-NLS-2$
			atomEntry.setAttributes(attributes);
			AtomContent content = new AtomContent();
			content.setType("application/xml"); //$NON-NLS-1$
			Mproperties properties = new Mproperties();
			content.setmProperties(properties);
			atomEntry.setContent(content);
			return atomEntry;

		case JSON:
			return new ODataEntryJson();

		default:
			return null;
		}
	}
}
