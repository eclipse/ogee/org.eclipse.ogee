/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.generic;

/**
 * Interface for Media Resource
 * 
 * 
 */
public interface IMediaResource {
	/**
	 * Returns the URI of the media resource.
	 * 
	 * @return - the URI of the media resource.
	 */
	public String getURI();

	/**
	 * Returns the MIME type of the media resource.
	 * 
	 * @return - the MIME type of the media resource.
	 */
	public String getMIMEType();
}
