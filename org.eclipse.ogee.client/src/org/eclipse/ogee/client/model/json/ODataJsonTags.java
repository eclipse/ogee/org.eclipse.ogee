/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.json;

/**
 * OData JSON Tags constants.
 */
public interface ODataJsonTags {
	public static final String CONTENT_TYPE_TAG = "content_type"; //$NON-NLS-1$
	public static final String MEDIA_SRC_TAG = "media_src"; //$NON-NLS-1$
	public static final String METADATA_TAG = "__metadata"; //$NON-NLS-1$
	public static final String URI_TAG = "uri"; //$NON-NLS-1$
	public static final String DEFERRED_TAG = "__deferred"; //$NON-NLS-1$
	public static final String CLOSE_BRACES = "}"; //$NON-NLS-1$
	public static final String OPEN_BRACES = "{"; //$NON-NLS-1$
	public static final String D_PREFIX = OPEN_BRACES + "\"d\":"; //$NON-NLS-1$
	public static final String TYPE_TAG = "type"; //$NON-NLS-1$
	public static final String RESULTS_TAG = "results"; //$NON-NLS-1$
	public static final String D_TAG = "d"; //$NON-NLS-1$
}
