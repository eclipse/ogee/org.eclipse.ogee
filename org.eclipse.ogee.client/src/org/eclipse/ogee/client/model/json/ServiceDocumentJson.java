/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.json;

import java.util.Arrays;

import org.eclipse.ogee.client.model.generic.ServiceDocument;

/**
 * Service Documents are represented in JSON by an object with a single
 * name/value pair with the name equal to "EntitySets" and the value being an
 * array of Collection names.
 */
public class ServiceDocumentJson implements ServiceDocument {
	private Object[] collections;

	/**
	 * Constructor
	 */
	public ServiceDocumentJson() {
		super();
		collections = new String[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.model.generic.ServiceDocument#
	 * getCollections()
	 */
	@Override
	public Object[] getCollections() {
		if (this.collections != null) {
			return Arrays.copyOf(this.collections, this.collections.length);
		}

		return null;
	}

	@Override
	public void setCollections(Object[] collections) {
		this.collections = collections;
	}
}
