/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.json;

import java.util.Arrays;

import org.eclipse.ogee.client.model.generic.ODataCollection;
import org.eclipse.ogee.client.model.generic.ODataEntry;

/**
 * Represents an OData collection for JSON.
 */
public class ODataCollectionJson implements ODataCollection {
	private ODataEntry[] entries;

	@Override
	public ODataEntry[] getEntries() {
		if (this.entries != null) {
			return Arrays.copyOf(this.entries, this.entries.length);
		}
		return null;
	}

	/**
	 * Sets the entries of this collection.
	 * 
	 * @param entries
	 */
	public void setEntries(ODataEntry[] entries) {
		this.entries = entries;
	}
}
