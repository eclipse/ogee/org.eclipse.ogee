/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.json;

import org.eclipse.ogee.client.model.generic.InlineData;
import org.eclipse.ogee.client.model.generic.ODataLink;

/**
 * Represents an ODataLink for JSON.
 */
public class ODataLinkJson implements ODataLink {
	private String uri;
	private String title;

	/**
	 * Returns the URI.
	 * 
	 * @return - the URI.
	 */
	public String getUri() {
		return this.uri;
	}

	/**
	 * Sets the URI.
	 * 
	 * @param URI
	 *            - URI
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public String getTitle() {
		return this.title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public InlineData getInlineData() {
		// no inline in JSON format
		return null;
	}
}
