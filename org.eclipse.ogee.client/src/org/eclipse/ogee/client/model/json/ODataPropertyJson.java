/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.json;

import java.util.HashMap;

import org.eclipse.ogee.client.model.generic.ODataCollection;
import org.eclipse.ogee.client.model.generic.ODataProperty;
import org.eclipse.ogee.client.parser.impl.EdmTypes;

/**
 * Implementation of ODataProperty in the JSON format. Refers to both primitive
 * properties, as well as to Complex Types Properties
 */
public class ODataPropertyJson implements ODataProperty {
	private HashMap<String, ODataPropertyJson> childDataValues;
	private HashMap<String, String> metadata;
	private String name;
	private String value;
	private String metadataType;
	private EdmTypes type;
	private ODataCollectionJson collection;

	/**
	 * Constructor
	 */
	public ODataPropertyJson() {
		childDataValues = new HashMap<String, ODataPropertyJson>();
		metadata = new HashMap<String, String>();
	}

	/**
	 * Returns the name of the property.
	 * 
	 * @return - the name of the property.
	 */
	public String getName() {
		if (name == null) {
			return null;
		}

		int attrBeginIndex = name.indexOf(".") + 1; //$NON-NLS-1$
		return name.substring(attrBeginIndex);
	}

	/**
	 * Returns the qualified name of the property.
	 * 
	 * @return - the qualified name of the property.
	 */
	public String getQualifiedName() {
		return name;
	}

	/**
	 * Sets the name of the property.
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the value of the property.
	 * 
	 * @return - the value of the property.
	 */
	public String getValue() {
		if (value != null && value.startsWith("[")) //$NON-NLS-1$
		{
			value = "{\"results\":" + value + "}"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		return value;
	}

	/**
	 * Sets the value of the property.
	 * 
	 * @param value
	 *            - the value to be set.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Returns the metadata type of the property.
	 * 
	 * @return - the metadata type of the property.
	 */
	public String getMetadataType() {
		return metadataType;
	}

	/**
	 * Sets the metadata type of the property.
	 * 
	 * @param type
	 */
	public void setMetadataType(String type) {
		this.metadataType = type;
		this.metadata.put("type", type); //$NON-NLS-1$
	}

	/**
	 * Returns the child data value of the property by the given name.
	 * 
	 * @param name
	 * @return ODataPropertyJson by name
	 */
	public ODataPropertyJson getChildDataValue(String name) {
		return this.childDataValues.get(name);
	}

	/**
	 * Put the given property as child data value.
	 * 
	 * @param property
	 *            the child dataValue
	 */
	public void putChildDataValue(ODataProperty property) {
		if (property != null) {
			this.childDataValues.put(property.getQualifiedName(),
					(ODataPropertyJson) property);
		}
	}

	/**
	 * Returns the child data values of the property.
	 * 
	 * @return - the child data values of the property.
	 */
	public ODataPropertyJson[] getChildDataValues() {
		ODataPropertyJson[] resultDataValues = new ODataPropertyJson[childDataValues
				.size()];
		return childDataValues.values().toArray(resultDataValues);
	}

	/**
	 * Returns true whether this property is complex type, false otherwise.
	 * 
	 * @return - true whether this property is complex type, false otherwise.
	 */
	public boolean isComplexType() {
		return childDataValues.size() > 0;
	}

	@Override
	public boolean isComplexTypeProperty() {
		return isComplexType();
	}

	@Override
	public ODataProperty[] getChildProperties() {
		return getChildDataValues();
	}

	@Override
	public ODataProperty getChildProperty(String key) {
		return getChildDataValue(key);
	}

	@Override
	public void putChildProperty(ODataProperty property) {
		putChildDataValue((ODataPropertyJson) property);
	}

	/**
	 * Returns the metadata.
	 * 
	 * @return the metadata
	 */
	public HashMap<String, String> getMetadata() {
		return metadata;
	}

	/**
	 * Sets the metadata.
	 * 
	 * @param metadata
	 *            the metadata to set
	 */
	public void setMetadata(HashMap<String, String> metadata) {
		this.metadata = metadata;
	}

	@Override
	public String getType() {
		if (type != null) {
			return this.type.name();
		}

		return null;
	}

	/**
	 * Sets the type of this property.
	 * 
	 * @param type
	 */
	public void setType(EdmTypes type) {
		this.type = type;
	}

	@Override
	public ODataCollection getCollectionValue() {
		return collection;
	}

	/**
	 * Sets the given collection value.
	 * 
	 * @param value
	 */
	public void setCollectionValue(ODataCollectionJson value) {
		this.collection = value;
	}

	@Override
	public boolean isCollectionTypeProperty() {
		return collection != null;
	}
}
