/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.model.json;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.ogee.client.exceptions.TypeConversionException;
import org.eclipse.ogee.client.jsonsimple.JSONAware;
import org.eclipse.ogee.client.jsonsimple.JSONObject;
import org.eclipse.ogee.client.jsonsimple.JSONValue;
import org.eclipse.ogee.client.model.generic.IMediaResource;
import org.eclipse.ogee.client.model.generic.ODataEntry;
import org.eclipse.ogee.client.model.generic.ODataLink;
import org.eclipse.ogee.client.model.generic.ODataProperty;
import org.eclipse.ogee.client.parser.impl.TypeConverter;

/**
 * Represents an OData entry, in JSON format.
 */
public class ODataEntryJson implements ODataEntry, JSONAware {
	private HashMap<String, String> metadata;
	private HashMap<String, ODataPropertyJson> properties;
	private LinkedList<ODataLinkJson> links;
	private String id;
	private String type;

	private static Logger log = Logger.getAnonymousLogger();
	private IMediaResource mediaResource = null;

	/**
	 * Constructs a new OData entry JSON
	 */
	public ODataEntryJson() {
		this.properties = new HashMap<String, ODataPropertyJson>();
		this.metadata = new HashMap<String, String>();
		links = new LinkedList<ODataLinkJson>();
	}

	/**
	 * Returns the metadata of this OData JSON entry.
	 * 
	 * @return - the metadata of this OData JSON entry.
	 */
	public HashMap<String, String> getMetadata() {
		return this.metadata;
	}

	/**
	 * Sets the metadata of this OData JSON entry.
	 * 
	 * @param metadata
	 */
	public void setMetadata(HashMap<String, String> metadata) {
		this.metadata = metadata;
	}

	@Override
	public ODataProperty[] getProperties() {
		return properties.values().toArray(
				new ODataProperty[properties.values().size()]);
	}

	@Override
	public ODataProperty getProperty(String key) {
		return properties.get(key);
	}

	@Override
	public void putProperty(ODataProperty property) {
		properties.put(property.getQualifiedName(),
				(ODataPropertyJson) property);
	}

	@Override
	public void putProperty(String key, String value) {
		ODataPropertyJson dataValue = new ODataPropertyJson();
		dataValue.setName(key);
		dataValue.setValue(value);
		this.putProperty(dataValue);
	}

	/**
	 * @return - the links of this OData JSON entry.
	 */
	public ODataLink[] getLinks() {
		ODataLink[] links = new ODataLink[this.links.size()];
		return this.links.toArray(links);
	}

	/**
	 * Sets the links of this OData JSON entry.
	 * 
	 * @param links
	 */
	public void setLinks(ODataLink[] links) {
		LinkedList<ODataLinkJson> linksList = new LinkedList<ODataLinkJson>();
		for (ODataLink oDataLink : links) {
			linksList.add((ODataLinkJson) oDataLink);
		}
		this.links = linksList;
	}

	/**
	 * Sets the links of this OData JSON entry.
	 * 
	 * @param links
	 */
	public void addLink(ODataLink link) {
		this.links.add((ODataLinkJson) link);
	}

	@Override
	public boolean isMediaLinkEntry() {
		if (this.metadata.get(ODataJsonTags.CONTENT_TYPE_TAG) != null
				&& this.metadata.get(ODataJsonTags.MEDIA_SRC_TAG) != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public IMediaResource getMediaResource() {
		if (isMediaLinkEntry() && mediaResource == null) {
			mediaResource = new IMediaResource() {
				@Override
				public String getURI() {
					return getMetadata().get(ODataJsonTags.MEDIA_SRC_TAG);
				}

				@Override
				public String getMIMEType() {
					return getMetadata().get(ODataJsonTags.CONTENT_TYPE_TAG);
				}

			};
		}
		return mediaResource;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public String getType() {
		return this.type;
	}

	/**
	 * Sets the id of the entry.
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Sets the type of this entry.
	 * 
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toJSONString() {
		return formatEntry(this);
	}

	/**
	 * Converts an ODataEntry object to JSON format string.
	 * 
	 * @param entry
	 *            - the given OData entry to be formatted.
	 * @return - the JSON string representation of the given OData entry.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String formatEntry(ODataEntry entry) {
		// using LinkedHashMap so it will keep the order
		Map parent = new LinkedHashMap();

		// put the metadata under the parent
		parent.put(ODataJsonTags.METADATA_TAG,
				((ODataEntryJson) entry).getMetadata());

		// get the entry's properties and insert them to the parent
		parent = handleProperties(entry, parent);

		parent = handleLinks(entry, parent);

		// convert the json object to a string
		String jsonText = JSONValue.toJSONString(parent);
		return jsonText;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map handleLinks(ODataEntry entry, Map parent) {
		// get the links of this entry
		ODataLink[] links = entry.getLinks();

		// go over each link in this entry and add it
		for (int i = 0; i < links.length; i++) {
			// get the link
			ODataLinkJson oDataLinkJson = (ODataLinkJson) links[i];
			JSONObject linkJsonObject = new JSONObject();
			JSONObject uri = new JSONObject();

			// put the uri
			uri.put(ODataJsonTags.URI_TAG, oDataLinkJson.getUri());
			// put the "__deferred" string
			linkJsonObject.put(ODataJsonTags.DEFERRED_TAG, uri);
			// put the link in 'd'
			parent.put(oDataLinkJson.getTitle(), linkJsonObject);
		}

		return parent;
	}

	@SuppressWarnings("unchecked")
	private Map handleProperties(ODataEntry entry, Map parent) {
		TypeConverter converter = TypeConverter.JSON;
		ODataProperty[] odataProperties = entry.getProperties();
		for (ODataProperty property : odataProperties) {
			if ((property != null) && (property.getName() != null)) {
				if (property.isComplexTypeProperty()) {
					Map complexType = new LinkedHashMap<String, String>();

					complexType.put(ODataJsonTags.METADATA_TAG,
							((ODataPropertyJson) property).getMetadata());

					ODataProperty[] childProperties = property
							.getChildProperties();
					for (int i = 0; i < childProperties.length; i++) {
						// move the children properties from the array to the
						// map so they can be added to 'd'

						// get the value of this property with its real Edm type
						Object valueByType = null;
						try {
							valueByType = converter.convertToConcreteObject(
									childProperties[i],
									childProperties[i].getType());
						} catch (TypeConversionException e) {
							log.logp(Level.SEVERE, this.getClass().toString(),
									"handleProperties", //$NON-NLS-1$
									e.getLocalizedMessage(), e);
						}
						complexType.put(childProperties[i].getName(),
								valueByType);
					}

					parent.put(property.getName(), complexType);
				} else {
					// get the value of this property with its real Edm type
					parent.put(property.getName(), property.getValue());
				}
			}
		}

		return parent;
	}
}
