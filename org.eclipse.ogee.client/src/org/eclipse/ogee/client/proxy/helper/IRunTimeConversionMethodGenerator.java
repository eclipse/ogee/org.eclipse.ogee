/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.proxy.helper;

import org.eclipse.ogee.client.exceptions.TypeConversionException;

public interface IRunTimeConversionMethodGenerator {

	/**
	 * Generates string with a method name that converts from EDM or Complex
	 * type to java type
	 * 
	 * @param type
	 * @param propertyName
	 * @param isComplexType
	 * @return string with a method name that converts from EDM or Complex type
	 *         to java type
	 * @throws TypeConversionException
	 */
	public abstract String getConversionMethodName(String type,
			String propertyName, boolean isComplexType)
			throws TypeConversionException;

	public abstract String removePrefix(String fullName);

	public abstract String getPropertyStringByType(boolean isComplexType);

	public abstract String getFunctionImportComplexTypeMethodName(String type,
			String propertyName) throws TypeConversionException;

}
