/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.proxy.helper;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;
import java.util.regex.Matcher;

import org.eclipse.ogee.client.exceptions.ProxyException;
import org.eclipse.ogee.client.exceptions.TypeConversionException;
import org.eclipse.ogee.client.model.edmx.Association;
import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.EdmxDataServices;
import org.eclipse.ogee.client.model.edmx.End;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.Key;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.client.model.edmx.Parameter;
import org.eclipse.ogee.client.model.edmx.Property;
import org.eclipse.ogee.client.model.edmx.PropertyRef;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.model.generic.ODataEntry;
import org.eclipse.ogee.client.model.generic.ODataProperty;
import org.eclipse.ogee.client.nls.messages.Messages;
import org.eclipse.ogee.client.parser.impl.EdmTypes;
import org.eclipse.ogee.client.parser.impl.TypeConverter;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.client.parser.impl.util.EntitySetExt;
import org.eclipse.ogee.client.parser.util.StringParsingUtils;

/**
 * Helper class for querying an Edmx object.
 */
public class ODataProxyHelper implements IODataProxyHelper {
	private static final String VOID = "void"; //$NON-NLS-1$	
	private static final String HTTP_POST = "POST"; //$NON-NLS-1$
	private static final String HTTP_PUT = "PUT"; //$NON-NLS-1$
	private static final String ENTITY_SET_ATTRIBUTE = "EntitySet"; //$NON-NLS-1$
	private static final String HTTP_GET = "GET"; //$NON-NLS-1$
	public static final String ENTITY_SET_NAME_ATTRIBUTE = "Name"; //$NON-NLS-1$	
	private Edmx edmxObject = null;
	private static boolean isOdataVersion1_0 = false;

	private static final String EMPTY = ""; //$NON-NLS-1$

	private String serviceUrl;
	private String serviceName;
	private static final String DOT = "."; //$NON-NLS-1$
	private static final String SERVICE_SUFFIX = "Service"; //$NON-NLS-1$
	private IRunTimeConversionMethodGenerator runtimeConversionMethodGen;
	private ODataEntry serviceEntry;
	private TypeConverter typeConverter;

	private final static Logger LOGGER = Logger
			.getLogger(ODataProxyHelper.class.getName());

	/**
	 * Constructor.
	 * 
	 * @param serviceName
	 * @param serviceUrl
	 * @param edmx
	 * @param serviceEntry
	 */
	public ODataProxyHelper(String serviceName, String serviceUrl, Edmx edmx,
			ODataEntry serviceEntry) {
		this.serviceEntry = serviceEntry;
		this.edmxObject = edmx;
		this.serviceUrl = serviceUrl;
		setCompatibilityLevel();
		this.runtimeConversionMethodGen = new RunTimeConversionMethodGenerator();
		this.serviceName = serviceName;
		this.typeConverter = TypeConverter.ATOM;
	}

	private void setCompatibilityLevel() {
		if (StringParsingUtils.getCompatabilityLevel(serviceUrl).equals("0.2")) //$NON-NLS-1$
		{
			isOdataVersion1_0 = false;
		} else {
			isOdataVersion1_0 = true;
		}
	}

	/**
	 * @return the isOdataVersion1_0
	 */
	public boolean isOdataVersion1_0() {
		return isOdataVersion1_0;
	}

	/**
	 * @return the isOdataVersion0_2
	 */
	public boolean isOdataVersion0_2() {
		return !isOdataVersion1_0;
	}

	@Override
	public String getServiceDescription() {
		String description = EMPTY;

		if (this.serviceEntry == null) {
			return EMPTY;
		}

		ODataProperty descriptionProperty = this.serviceEntry
				.getProperty("Description"); //$NON-NLS-1$
		if (descriptionProperty == null) {
			return description;
		}

		description = descriptionProperty.getValue();

		return description;
	}

	@Override
	public Edmx getEdmxObject() {
		return edmxObject;
	}

	@Override
	public String getKeyPropertiesString(String entityTypeName) {
		String result = EMPTY; //$NON-NLS-1$
		List<EntityType> allEntityTypes = getAllEntityTypes();
		for (EntityType entityType : allEntityTypes) {
			if (entityType.getName().equalsIgnoreCase(entityTypeName)) {
				Key key = entityType.getKey();
				if (key != null) {
					PropertyRef[] propertyRefsArray = key.getPropertyRefs();
					for (int i = 0; i < propertyRefsArray.length; i++) {
						if (i == propertyRefsArray.length - 1) {
							result = result + propertyRefsArray[i].getName();// $JL-STR_CONCAT$
						} else {
							result = result + propertyRefsArray[i].getName()
									+ ", ";//$JL-STR_CONCAT$ //$NON-NLS-1$
						}
					}
				}
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getKeyPropertiesList(java.lang.String)
	 */
	@Override
	public List<PropertyRef> getKeyPropertiesList(String entityTypeName) {
		List<EntityType> allEntityTypes = getAllEntityTypes();
		for (EntityType entityType : allEntityTypes) {
			if (entityType.getName().equalsIgnoreCase(entityTypeName)) {
				Key key = entityType.getKey();
				if (key != null) {
					PropertyRef[] propertyRefsArray = key.getPropertyRefs();

					return Arrays.asList(propertyRefsArray);
				}
			}
		}
		return null;
	}

	@Override
	public Collection<Property> getNonKeyPropertiesList(String entityTypeName) {
		HashMap<String, Property> propertysHash = new HashMap<String, Property>();

		List<EntityType> allEntityTypes = getAllEntityTypes();
		for (EntityType entityType : allEntityTypes) {
			if (entityType.getName().equalsIgnoreCase(entityTypeName)) {
				Property[] propertysArray = entityType.getProperties();
				for (Property property : propertysArray) {
					propertysHash.put(property.getName(), property);
				}
			}
		}

		List<PropertyRef> propertyRefsList = getKeyPropertiesList(entityTypeName);
		for (PropertyRef propertyRef : propertyRefsList) {
			propertysHash.remove(propertyRef.getName());
		}

		return propertysHash.values();
	}

	@Override
	public boolean isEdmType(String type) {
		return EdmTypes.toEdmTypes(type) != EdmTypes.NOVALUE;
	}

	@Override
	public boolean isPostHttpMethod(String functionImportName) {

		List<FunctionImport> allFunctionImports = getAllFunctionImports();
		for (FunctionImport functionImport : allFunctionImports) {
			if (functionImport.getmHttpMethod() != null) {
				if (functionImport.getName().equalsIgnoreCase(
						functionImportName)) {
					return functionImport.getmHttpMethod().equalsIgnoreCase(
							HTTP_POST);
				}
			}
		}

		return false;
	}

	@Override
	public boolean isPutHttpMethod(String functionImportName) {

		List<FunctionImport> allFunctionImports = getAllFunctionImports();
		for (FunctionImport functionImport : allFunctionImports) {
			if (functionImport.getmHttpMethod() != null) {
				if (functionImport.getName().equalsIgnoreCase(
						functionImportName)) {
					return functionImport.getmHttpMethod().equalsIgnoreCase(
							HTTP_PUT);
				}
			}
		}

		return false;
	}

	@Override
	public String getServiceUrl() {
		return serviceUrl;
	}

	@Override
	public String getShortServiceUrl() {
		String relativURL = EMPTY; //$NON-NLS-1$
		try {
			URL shortUrl = new URL(
					StringParsingUtils.removeURLParameters(serviceUrl));
			relativURL = shortUrl.getFile();

		} catch (MalformedURLException e) {
			LOGGER.severe(Messages.getString("ODataProxyHelper.5")); //$NON-NLS-1$
			LOGGER.severe(Messages.getString("ODataProxyHelper.6")); //$NON-NLS-1$
		}

		relativURL = stripSlashSaffix(relativURL);

		return relativURL;
	}

	private String stripSlashSaffix(String relativURL) {
		// remove the "/" suffix
		if (relativURL.endsWith("/")) //$NON-NLS-1$
		{
			relativURL = relativURL.substring(0, relativURL.length() - 1);
		}
		return relativURL;
	}

	@SuppressWarnings("unused")
	private String stripUrlParameters(String baseUrl) {
		java.util.regex.Pattern pattern = java.util.regex.Pattern
				.compile("(.*)(\\?.*)"); //$NON-NLS-1$
		Matcher matcher = pattern.matcher(baseUrl);
		if (matcher.find()) {
			baseUrl = matcher.group(1);
		}
		return baseUrl;
	}

	public String getUrlParameters() {
		int index = serviceUrl.indexOf("?"); //$NON-NLS-1$
		if (index > 0) {
			return serviceUrl.substring(index);
		}
		return "";
	}

	@Override
	public List<FunctionImport> getAllGetFunctionImports() {
		LinkedList<FunctionImport> functionImports = new LinkedList<FunctionImport>();

		List<FunctionImport> tmpList = EdmxUtilities
				.getAllFunctionImports(this.edmxObject);

		for (FunctionImport eSet : tmpList) {
			if (null != eSet.getmHttpMethod()) {
				if (eSet.getmHttpMethod().equals(HTTP_GET)) {
					functionImports.add(eSet);
				}

			} else {
				functionImports.add(eSet);
			}
		}

		return functionImports;
	}

	@Override
	public List<FunctionImport> getAllFunctionImports() {
		return EdmxUtilities.getAllFunctionImports(this.edmxObject);
	}

	@Override
	public String getFunctionImportsReturnType(String name) {
		List<FunctionImport> tmpList = EdmxUtilities
				.getAllFunctionImports(this.edmxObject);

		for (FunctionImport fi : tmpList) {
			if (fi.getName().equals(name)) {
				if (fi.getReturnType() == null) {
					return VOID;
				} else {
					return removePrefix(fi.getReturnType());
				}
			}
		}

		return null;
	}

	@Override
	public boolean isFunctionImportReturnsList(String name) {
		List<FunctionImport> tmpList = EdmxUtilities
				.getAllFunctionImports(this.edmxObject);

		for (FunctionImport fi : tmpList) {
			if (fi.getName().equals(name)) {
				if (fi.getReturnType() == null) {
					return false;
				}

				if (fi.getReturnType().contains("Collection")) //$NON-NLS-1$
				{
					return true;
				}
				return false;
			}
		}

		return false;
	}

	/**
	 * @param functionImportName
	 *            name of function import
	 * @return true if return type is a collection of a complex type
	 */
	public boolean isFunctionImportReturnsComplexTypeCollection(
			String functionImportName) {
		List<FunctionImport> tmpList = EdmxUtilities
				.getAllFunctionImports(this.edmxObject);

		for (FunctionImport fi : tmpList) {
			if (fi.getName().equals(functionImportName)) {
				if (fi.getReturnType() == null) {
					return false;
				}

				String returnType = fi.getReturnType();
				if (returnType.startsWith("Collection")) //$NON-NLS-1$
				{
					String innerType = returnType.substring(
							"Collection(".length(), returnType.length() - 1);
					if (EdmxUtilities.isComplexType(innerType, this.edmxObject)) {
						return true;
					}
				}
				return false;
			}
		}
		return false;
	}

	public boolean isComplexType(String name) {
		return EdmxUtilities.isComplexType(name, this.edmxObject);
	}

	@Override
	public boolean isFunctionImportReturnsAtomEntry(String name) {
		List<FunctionImport> tmpList = EdmxUtilities
				.getAllFunctionImports(this.edmxObject);

		for (FunctionImport fi : tmpList) {
			if (fi.getName().equals(name)) {
				if (null == fi.getAttribute(ENTITY_SET_ATTRIBUTE)) {
					return false;
				}
				return true;
			}
		}

		return false;
	}

	@Override
	public List<EntitySet> getAllAddressableCollections() {
		LinkedList<EntitySet> addressableCollections = new LinkedList<EntitySet>();

		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);

		for (EntitySet eSet : tmpList) {
			if (EdmxUtilities.isEntitySetAddressable(eSet, this.edmxObject)) {
				addressableCollections.add(eSet);
			}
		}

		return addressableCollections;
	}

	@Override
	public List<EntitySet> getAllCreatableCollections() {
		LinkedList<EntitySet> creatableCollections = new LinkedList<EntitySet>();

		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);

		for (EntitySet entitySet : tmpList) {
			if (EdmxUtilities.isEntitySetCreatable(entitySet, this.edmxObject)) {
				creatableCollections.add(entitySet);
			}
		}

		return creatableCollections;
	}

	@Override
	public List<EntitySet> getAllUpdatableCollections() {
		LinkedList<EntitySet> updatableCollections = new LinkedList<EntitySet>();

		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);

		for (EntitySet entitySet : tmpList) {
			if (EdmxUtilities.isEntitySetUpdatable(entitySet, this.edmxObject)) {
				updatableCollections.add(entitySet);
			}
		}

		return updatableCollections;
	}

	@Override
	public List<EntitySet> getAllDeletableCollections() {
		LinkedList<EntitySet> deletableCollections = new LinkedList<EntitySet>();

		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);

		for (EntitySet entitySet : tmpList) {
			if (EdmxUtilities.isEntitySetDeletable(entitySet, this.edmxObject)) {
				deletableCollections.add(entitySet);
			}
		}

		return deletableCollections;
	}

	@Override
	public String getEntityTypeNameForEntitySet(String entitySetName) {
		List<EntitySet> entitySets = EdmxUtilities
				.getAllEntitySets(this.edmxObject);
		for (EntitySet entitySet : entitySets) {
			if (entitySet.getName().equalsIgnoreCase(entitySetName)) {
				return removePrefix(entitySet.getEntityType());
			}
		}

		return null;
	}

	@Override
	public boolean isAddressable(String entityTypeName) {
		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);

		for (EntitySet entitySet : tmpList) {
			if (removePrefix(entitySet.getEntityType()).equalsIgnoreCase(
					entityTypeName)) {
				return EdmxUtilities.isEntitySetAddressable(entitySet,
						this.edmxObject);
			}
		}

		return false;
	}

	@Override
	public boolean isUpdatable(String entityTypeName) {
		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);

		for (EntitySet entitySet : tmpList) {
			if (removePrefix(entitySet.getEntityType()).equalsIgnoreCase(
					entityTypeName)) {
				return EdmxUtilities.isEntitySetUpdatable(entitySet,
						this.edmxObject);
			}
		}

		return false;
	}

	@Override
	public boolean isDeletable(String entityTypeName) {
		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);

		for (EntitySet entitySet : tmpList) {
			if (removePrefix(entitySet.getEntityType()).equalsIgnoreCase(
					entityTypeName)) {
				return EdmxUtilities.isEntitySetDeletable(entitySet,
						this.edmxObject);
			}
		}

		return false;
	}

	@Override
	public boolean isMediaLinkEntry(String entityTypeName) {
		Schema[] schemas = edmxObject.getEdmxDataServices().getSchemas();
		List<Schema> asList = Arrays.asList(schemas);
		for (Schema schema : asList) {
			EntityType[] entityTypes = schema.getEntityTypes();
			for (EntityType entityType : entityTypes) {
				if (removePrefix(entityType.getName()).equalsIgnoreCase(
						entityTypeName)) {
					return entityType.getMHasStream();
				}
			}
		}

		return false;
	}

	@Override
	public String getAddressableCollectionsReturnType(String name) {
		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);

		for (EntitySet eSet : tmpList) {
			if (eSet.getAttribute(ENTITY_SET_NAME_ATTRIBUTE).equals(name)) {
				return removePrefix(eSet.getEntityType());
			}
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getMethodHeaderFromKeyProperties(java.lang.String)
	 */
	@Override
	public String getMethodHeaderFromKeyProperties(String entitySetName)
			throws TypeConversionException {
		StringBuffer header = new StringBuffer("( "); //$NON-NLS-1$
		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);
		EntityType baseEntityType = null;

		for (EntitySet eSet : tmpList) {
			if (eSet.getAttribute(ENTITY_SET_NAME_ATTRIBUTE).equals(
					entitySetName)) {
				EntityType entityType = EdmxUtilities.getEntityTypeByName(eSet
						.getEntityType(), this.edmxObject.getEdmxDataServices()
						.getSchemas());
				Key key = entityType.getKey();

				if (key == null) { // the key is derived from the base entity
									// type.
					String baseType = entityType.getBaseType();

					baseEntityType = EdmxUtilities.getEntityTypeByName(
							baseType, this.getEdmxObject()
									.getEdmxDataServices().getSchemas());
					key = baseEntityType.getKey();
				}

				if (key != null) {
					PropertyRef[] propertyRefs = key.getPropertyRefs();
					for (PropertyRef propertyRef : propertyRefs) {
						Property property = entityType.getProperty(propertyRef
								.getName());
						if (property == null) { // the property is derived from
												// the base entity type.
							property = baseEntityType.getProperty(propertyRef
									.getName());
						}

						if (property == null) {
							throw new TypeConversionException(
									"property element is null"); //$NON-NLS-1$
						}
						String edmType = property.getType();
						String typeConverted = this.typeConvert(edmType);
						String type = removePrefix(typeConverted);
						header.append(type + " " + propertyRef.getName() + ", "); //$NON-NLS-1$ //$NON-NLS-2$
					}
				}
			}
		}

		int index = header.lastIndexOf(","); //$NON-NLS-1$

		if (index == (-1)) {
			return header.toString();
		} else {
			header.replace(index, index + 1, ")"); //$NON-NLS-1$
		}

		return header.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getConstructorHeaderFromKeyProperties(java.lang.String)
	 */
	@Override
	public String getConstructorHeaderFromKeyProperties(String entityName)
			throws TypeConversionException {
		StringBuffer header = new StringBuffer("( "); //$NON-NLS-1$

		EntityType entityType = EdmxUtilities.getEntityTypeByName(entityName,
				this.edmxObject.getEdmxDataServices().getSchemas());
		Key key = entityType.getKey();
		if (key != null) {
			PropertyRef[] propertyRefs = key.getPropertyRefs();
			for (PropertyRef propertyRef : propertyRefs) {
				String type = removePrefix(typeConverter.typeConvert(entityType
						.getProperty(propertyRef.getName()).getType()));
				header.append(type + " " + propertyRef.getName() + ", "); //$NON-NLS-1$ //$NON-NLS-2$
			}

			return header.toString();
		}
		throw new TypeConversionException("Key element is null"); //$NON-NLS-1$

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getFunctionImportsMethodHeader(java.lang.String)
	 */
	@Override
	public String getFunctionImportsMethodHeader(String name)
			throws TypeConversionException {
		StringBuffer header = new StringBuffer("("); //$NON-NLS-1$

		List<FunctionImport> tmpList = EdmxUtilities
				.getAllFunctionImports(this.edmxObject);

		for (FunctionImport fi : tmpList) {
			if (fi.getName().equals(name)) {
				Parameter[] params = fi.getParameters();
				for (Parameter param : params) {
					header.append(removePrefix(typeConverter.typeConvert(param
							.getType())) + " " + param.getName() //$NON-NLS-1$
							+ ", "); //$NON-NLS-1$
				}
			}
		}

		int index = header.lastIndexOf(","); //$NON-NLS-1$

		if (index == (-1)) {
			header.append(")"); //$NON-NLS-1$
			return header.toString();
		} else {
			header.replace(index, index + 1, ")"); //$NON-NLS-1$
		}

		return header.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getFunctionImportParameters(java.lang.String)
	 */
	@Override
	public List<Parameter> getFunctionImportParameters(String functionImportName) {
		List<FunctionImport> tmpList = EdmxUtilities
				.getAllFunctionImports(this.edmxObject);

		for (FunctionImport fi : tmpList) {
			if (fi.getName().equals(functionImportName)) {
				Parameter[] params = fi.getParameters();
				return Arrays.asList(params);
			}
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * hasParameters(java.lang.String)
	 */
	@Override
	public boolean hasParameters(String functionImportName) {
		List<FunctionImport> tmpList = EdmxUtilities
				.getAllFunctionImports(this.edmxObject);

		for (FunctionImport fi : tmpList) {
			if (fi.getName().equals(functionImportName)) {
				Parameter[] params = fi.getParameters();
				return (params.length != 0);
			}
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getParamterValue(java.lang.String, java.lang.String)
	 */
	@Override
	public String getParamterValue(String functionImportName, String paramerName) {
		List<FunctionImport> tmpList = EdmxUtilities
				.getAllFunctionImports(this.edmxObject);

		for (FunctionImport fi : tmpList) {
			if (fi.getName().equals(functionImportName)) {
				Parameter[] params = fi.getParameters();
				for (Parameter parameter : params) {
					if (parameter.getName().equalsIgnoreCase(paramerName)) {
						if (isOdataVersion1_0) {
							if (removePrefix(parameter.getType()).equals(
									EdmTypes.String.name())) //$NON-NLS-1$
							{
								return "\"'\"+" + parameter.getName() + "+\"'\""; //$NON-NLS-1$ //$NON-NLS-2$
							}

							if (removePrefix(parameter.getType()).equals(
									EdmTypes.DateTime.name())) //$NON-NLS-1$
							{
								return "\"" + EdmTypes.DateTime.name().toLowerCase(Locale.ENGLISH) + "'\" + " //$NON-NLS-1$ //$NON-NLS-2$
										+ "parser.getTypeConverter().convertToString(\"" + EdmTypes.DateTime.name() + "\"," //$NON-NLS-1$ //$NON-NLS-2$
										+ parameter.getName() + ") + \"'\""; //$NON-NLS-1$
							}

							if (removePrefix(parameter.getType()).equals(
									EdmTypes.Guid.name())) //$NON-NLS-1$
							{
								return "\"" + EdmTypes.Guid.name().toLowerCase(Locale.ENGLISH) + "'\" + " //$NON-NLS-1$ //$NON-NLS-2$
										+ "parser.getTypeConverter().convertToString(\"" + EdmTypes.Guid.name() + "\"," //$NON-NLS-1$ //$NON-NLS-2$
										+ parameter.getName() + ") + \"'\""; //$NON-NLS-1$
							}

							if (removePrefix(parameter.getType()).equals(
									EdmTypes.Binary.name())) //$NON-NLS-1$
							{
								return "\"" + EdmTypes.Binary.name().toLowerCase(Locale.ENGLISH) + "'\" + " //$NON-NLS-1$ //$NON-NLS-2$
										+ "parser.getTypeConverter().convertToString(\"" + EdmTypes.Binary.name() + "\"," //$NON-NLS-1$ //$NON-NLS-2$
										+ parameter.getName() + ") + \"'\""; //$NON-NLS-1$
							}

							if (removePrefix(parameter.getType()).equals(
									EdmTypes.Time.name())) //$NON-NLS-1$
							{
								return "\"" + EdmTypes.Time.name().toLowerCase(Locale.ENGLISH) + "'\" + " //$NON-NLS-1$ //$NON-NLS-2$
										+ "parser.getTypeConverter().convertToString(\"" + EdmTypes.Time.name() + "\"," //$NON-NLS-1$ //$NON-NLS-2$
										+ parameter.getName() + ") + \"'\""; //$NON-NLS-1$
							}

							if (removePrefix(parameter.getType()).equals(
									EdmTypes.DateTimeOffset.name())) //$NON-NLS-1$
							{
								return "\"" + EdmTypes.DateTimeOffset.name().toLowerCase(Locale.ENGLISH) + "'\" + " //$NON-NLS-1$ //$NON-NLS-2$
										+ "parser.getTypeConverter().convertToString(\"" + EdmTypes.DateTimeOffset.name() + "\"," //$NON-NLS-1$ //$NON-NLS-2$
										+ parameter.getName() + ") + \"'\""; //$NON-NLS-1$
							}

							if (removePrefix(parameter.getType()).equals(
									EdmTypes.Date.name())) //$NON-NLS-1$
							{
								return "\"" + EdmTypes.DateTime.name().toLowerCase(Locale.ENGLISH) + "'\" + " //$NON-NLS-1$ //$NON-NLS-2$
										+ "parser.getTypeConverter().convertToString(\"" + EdmTypes.DateTime.name() + "\"," //$NON-NLS-1$ //$NON-NLS-2$
										+ parameter.getName() + ") + \"'\""; //$NON-NLS-1$
							}
						} else {
							if (removePrefix(parameter.getType()).equals(
									EdmTypes.DateTime.name())) //$NON-NLS-1$
							{
								return "dateFormat.format(" + parameter.getName() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
							}

							if (removePrefix(parameter.getType()).equals(
									EdmTypes.Date.name())) //$NON-NLS-1$
							{
								return "dateFormat.format(" + parameter.getName() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
							}
						}
					}
				}
			}
		}

		return paramerName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getMethodArgsFromKeyProperties(java.lang.String)
	 */
	@Override
	public String getMethodArgsFromKeyProperties(String name)
			throws TypeConversionException, ProxyException {
		StringBuffer header = new StringBuffer("(\"+" + "\""); //$NON-NLS-1$ //$NON-NLS-2$
		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);

		for (EntitySet eSet : tmpList) {
			if (eSet.getAttribute(ENTITY_SET_NAME_ATTRIBUTE).equals(name)) {
				EntityType entityType = EdmxUtilities.getEntityTypeByName(eSet
						.getEntityType(), this.edmxObject.getEdmxDataServices()
						.getSchemas());
				PropertyRef[] propertyRefs = entityType.getKey()
						.getPropertyRefs();
				for (PropertyRef propertyRef : propertyRefs) {
					if (isOdataVersion1_0) {
						String type = getPropertyByPropertyRef(propertyRef,
								entityType);

						if (type.equals(EdmTypes.Guid.name())) //$NON-NLS-1$
						{
							header.append(propertyRef.getName()
									+ "=" + EdmTypes.Guid.name().toLowerCase(Locale.ENGLISH) + "'\"+" + "parser.getTypeConverter().convertToString(\"" + EdmTypes.Guid.name() + "\"," + propertyRef.getName() + ")" + "+\"',"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
						} else if (type.equals(EdmTypes.Binary.name())) //$NON-NLS-1$
						{
							header.append(propertyRef.getName()
									+ "=" + EdmTypes.Binary.name().toLowerCase(Locale.ENGLISH) + "'\"+" + "parser.getTypeConverter().convertToString(\"" + EdmTypes.Binary.name() + "\"," + propertyRef.getName() + ")" + "+\"',"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
						} else if (type.equals(EdmTypes.Time.name())) //$NON-NLS-1$
						{
							header.append(propertyRef.getName()
									+ "=" + EdmTypes.Time.name().toLowerCase(Locale.ENGLISH) + "'\"+" + "parser.getTypeConverter().convertToString(\"" + EdmTypes.Time.name() + "\"," + propertyRef.getName() + ",Representation.ATOM)" + "+\"',"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
						} else if (type.equals(EdmTypes.DateTimeOffset.name())) //$NON-NLS-1$
						{
							header.append(propertyRef.getName()
									+ "=" + EdmTypes.DateTimeOffset.name().toLowerCase(Locale.ENGLISH) + "'\"+" + "parser.getTypeConverter().convertToString(\"" + EdmTypes.DateTimeOffset.name() + "\"," + propertyRef.getName() + ",Representation.ATOM)" + "+\"',"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
						} else if (type.equals(EdmTypes.Date.name())) //$NON-NLS-1$
						{
							header.append(propertyRef.getName()
									+ "=" + EdmTypes.DateTime.name().toLowerCase(Locale.ENGLISH) + "'\"+" + "parser.getTypeConverter().convertToString(\"" + EdmTypes.DateTime.name() + "\"," + propertyRef.getName() + ",Representation.ATOM)" + "+\"',"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
						} else if (type.equals(EdmTypes.DateTime.name())) //$NON-NLS-1$
						{
							header.append(propertyRef.getName()
									+ "=" + EdmTypes.DateTime.name().toLowerCase(Locale.ENGLISH) + "'\"+" + "parser.getTypeConverter().convertToString(\"" + EdmTypes.DateTime.name() + "\"," + propertyRef.getName() + ",Representation.ATOM)" + "+\"',"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
						} else {
							header.append(propertyRef.getName()
									+ "='\"+" + propertyRef.getName() + "+\"',"); //$NON-NLS-1$ //$NON-NLS-2$
						}
					} else {
						// isOdataVersion 0_2
						String type = getPropertyByPropertyRef(propertyRef,
								entityType);
						if (type.equals(EdmTypes.DateTime.name())) //$NON-NLS-1$
						{
							header.append(propertyRef.getName()
									+ "='\"+" + "dateFormat.format(" + propertyRef.getName() + ")" + "+\"',"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						} else {
							header.append(propertyRef.getName()
									+ "='\"+" + propertyRef.getName() + "+\"',"); //$NON-NLS-1$ //$NON-NLS-2$
						}
					}
				}
			}
		}

		int index = header.lastIndexOf(","); //$NON-NLS-1$

		if (index == (-1)) {
			return header.toString();
		} else {
			header.replace(index, index + 1, ")"); //$NON-NLS-1$
			header.append("\""); //$NON-NLS-1$
		}

		return header.toString();
	}

	public String getKeyPropertiesForUrl(String name) throws ProxyException {
		StringBuffer header = new StringBuffer(); //$NON-NLS-1$ //$NON-NLS-2$

		List<EntitySet> tmpList = EdmxUtilities
				.getAllEntitySets(this.edmxObject);

		for (EntitySet eSet : tmpList) {
			if (eSet.getAttribute(ENTITY_SET_NAME_ATTRIBUTE).equals(name)) {
				EntityType entityType = EdmxUtilities.getEntityTypeByName(eSet
						.getEntityType(), this.edmxObject.getEdmxDataServices()
						.getSchemas());

				Key key = entityType.getKey();
				if (key == null) // check if this entity type has a base type,
									// and therefore doesn't need a key
				{
					String baseType = entityType.getBaseType();
					if (baseType != null && baseType.length() != 0) {
						throw new ProxyException(
								"Service contains derived types of a BaseType which is not supported: "
										+ entityType.getName());
					} else {
						// Illegal entity type, malformed service
						throw new ProxyException(entityType.getName()
								+ " Entity Type has no key.");
					}
				}

				PropertyRef[] propertyRefs = key.getPropertyRefs();
				header.append("buffer.append(\"(\");");
				header.append("\n\t\t\t");
				for (PropertyRef propertyRef : propertyRefs) {

					header.append("buffer.append(\"" + propertyRef.getName()
							+ "=\");");
					header.append("\n\t\t\t");

					String type = getPropertyByPropertyRef(propertyRef,
							entityType);

					if (type == null) {
						// Illegal entity type, malformed service
						throw new ProxyException(" Can't get property of "
								+ entityType.getName() + " by "
								+ propertyRef.getName() + " property ref.");
					}

					if (type.equals(EdmTypes.Guid.name())) //$NON-NLS-1$
					{
						header.append("buffer.append(");
						header.append("\""
								+ EdmTypes.Guid.name().toLowerCase(
										Locale.ENGLISH));
						header.append("'\" + " + "parser.getTypeConverter().convertToString(\"" + EdmTypes.Guid.name() + "\", entry.get" + propertyRef.getName() + "(), Representation.ATOM));"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					} else if (type.equals(EdmTypes.Binary.name())) //$NON-NLS-1$
					{
						header.append("buffer.append(");
						header.append("\""
								+ EdmTypes.Binary.name().toLowerCase(
										Locale.ENGLISH));
						header.append("'\" + " + "parser.getTypeConverter().convertToString(\"" + EdmTypes.Binary.name() + "\", entry.get" + propertyRef.getName() + "(), Representation.ATOM));"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					} else if (type.equals(EdmTypes.Time.name())) //$NON-NLS-1$
					{
						header.append("buffer.append(");
						header.append("\""
								+ EdmTypes.Time.name().toLowerCase(
										Locale.ENGLISH));
						header.append("'\" + " + "parser.getTypeConverter().convertToString(\"" + EdmTypes.Time.name() + "\", entry.get" + propertyRef.getName() + "(), Representation.ATOM));"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					} else if (type.equals(EdmTypes.DateTimeOffset.name())) //$NON-NLS-1$
					{
						header.append("buffer.append(");
						header.append("\""
								+ EdmTypes.DateTimeOffset.name().toLowerCase(
										Locale.ENGLISH));
						header.append("'\" + " + "parser.getTypeConverter().convertToString(\"" + EdmTypes.DateTimeOffset.name() + "\", entry.get" + propertyRef.getName() + "(), Representation.ATOM));"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					} else if (type.equals(EdmTypes.Date.name())
							|| type.equals(EdmTypes.DateTime.name())) //$NON-NLS-1$
					{
						header.append("buffer.append(");
						header.append("\""
								+ EdmTypes.DateTime.name().toLowerCase(
										Locale.ENGLISH));
						header.append("'\" + " + "parser.getTypeConverter().convertToString(\"" + EdmTypes.DateTime.name() + "\", entry.get" + propertyRef.getName() + "(), Representation.ATOM));"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					} else {
						header.append("buffer.append(\"'\");");
						header.append("\n\t\t\t");
						header.append("buffer.append(String.valueOf(entry.get"
								+ propertyRef.getName() + "()));");
					}

					header.append("\n\t\t\t");
					header.append("buffer.append(\"',\");");
					header.append("\n\t\t\t");
				}

				break;
			}
		}

		int index = header.lastIndexOf(","); //$NON-NLS-1$

		if (index == (-1)) {
			return header.toString();
		} else {
			header.replace(index, index + 1, ")"); //$NON-NLS-1$
		}

		return header.toString();
	}

	/**
	 * Returns the property of the given entity type by the given property ref.
	 * 
	 * @throws ProxyException
	 */
	public String getPropertyByPropertyRef(PropertyRef propertyRef,
			EntityType entityType) throws ProxyException {
		Property property = entityType.getProperty(propertyRef.getName());
		if (property == null) {
			// check for base type properties
			String baseType = entityType.getBaseType();
			if ((baseType != null) && (baseType.length() != 0)) {
				// base type exists!
				throw new ProxyException(
						"Service contains derived types of a BaseType which is not supported: "
								+ entityType.getName());
			} else {
				return null;
			}
		}

		return removePrefix(property.getType());
	}

	/**
	 * returns the java type equivalent to edmx type as string.
	 * 
	 * to rewrite that will return the type string if unknown
	 * 
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public String typeConvert(String type) throws TypeConversionException {
		return removePrefix(typeConverter.typeConvert(type));
	}

	/**
	 * returns the Conversion Method Name to java type as string.
	 * 
	 * to rewrite that will return the type string if unknown
	 * 
	 * @param type
	 * @return
	 * @throws TypeConversionException
	 * @throws Exception
	 */
	public <T> String getConversionMethodName(String type, String propertyName,
			boolean isComplexType) throws TypeConversionException {
		return runtimeConversionMethodGen.getConversionMethodName(type,
				propertyName, isComplexType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#getServices ()
	 */
	@Override
	public List<EntitySetExt> getServices() {
		List<EntitySetExt> sevices = EdmxUtilities
				.getAllEntitySetsExt(this.edmxObject);

		return sevices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getAssociation(java.lang.String)
	 */
	@Override
	public Association getAssociation(String name) {
		Association association = EdmxUtilities.getAssociation(name,
				this.edmxObject);

		return association;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getNavigationPropertyReturnType(java.lang.String, java.lang.String)
	 */
	@Override
	public String getNavigationPropertyReturnType(String nPropertyName,
			String eTypeName) {
		String rType = EMPTY;
		NavigationProperty navigationProperty = null;

		List<NavigationProperty> list = getNavigationProperties(eTypeName);

		for (NavigationProperty nProperty : list) {
			if (nProperty.getName().equals(nPropertyName)) {
				navigationProperty = nProperty;
			}
		}

		if (null != navigationProperty) {
			String relationship = navigationProperty.getRelationship();
			String toRole = navigationProperty.getToRole();
			Association association = getAssociation(removePrefix(relationship));

			End returnTypeEnd = null;
			End[] ends = association.getEnds();
			for (End end : ends) {
				if (end.getRole().equalsIgnoreCase(toRole)) {
					returnTypeEnd = end;
				}
			}
			if (returnTypeEnd == null) {
				returnTypeEnd = ends[1];
			}

			// if return type is many
			if (returnTypeEnd.getMultiplicity().equals("*")) //$NON-NLS-1$
			{
				String type = returnTypeEnd.getType();
				rType = "List<" + removePrefix(type) + ">"; //$NON-NLS-1$ //$NON-NLS-2$
			}// if return type is one
			else {
				String type = returnTypeEnd.getType();
				rType = removePrefix(type);
			}
		}

		return rType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getNavigationPropertyReturnTypeClass(java.lang.String, java.lang.String)
	 */
	@Override
	public String getNavigationPropertyReturnTypeClass(String nPropertyName,
			String eTypeName) {
		String rType = EMPTY;
		NavigationProperty nProperty = null;

		List<NavigationProperty> list = getNavigationProperties(eTypeName);

		for (NavigationProperty navigationProperty : list) {
			if (navigationProperty.getName().equals(nPropertyName)) {
				nProperty = navigationProperty;
			}
		}

		if (null != nProperty) {
			String relationship = nProperty.getRelationship();
			String toRole = nProperty.getToRole();
			Association association = getAssociation(removePrefix(relationship));

			End returnTypeEnd = null;
			End[] ends = association.getEnds();
			for (End end : ends) {
				if (end.getRole().equalsIgnoreCase(toRole)) {
					returnTypeEnd = end;
				}
			}

			if (returnTypeEnd == null) {
				returnTypeEnd = ends[1];
			}

			String type = returnTypeEnd.getType();
			rType = removePrefix(type);
		}

		return rType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * isNavigationPropertyReturnTypeIsList(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean isNavigationPropertyReturnTypeIsList(String nPropertyName,
			String eTypeName) {
		NavigationProperty nProperty = null;

		List<NavigationProperty> list = getNavigationProperties(eTypeName);

		for (NavigationProperty navigationProperty : list) {
			if (navigationProperty.getName().equals(nPropertyName)) {
				nProperty = navigationProperty;
			}
		}

		if (null != nProperty) {
			String relationship = nProperty.getRelationship();

			String toRole = nProperty.getToRole();
			Association association = getAssociation(removePrefix(relationship));

			End returnTypeEnd = null;
			End[] ends = association.getEnds();

			for (End end : ends) {
				if (end.getRole().equalsIgnoreCase(toRole)) {
					returnTypeEnd = end;
				}
			}

			if (returnTypeEnd == null) {
				returnTypeEnd = ends[1];
			}

			// if return type is many
			if (returnTypeEnd.getMultiplicity().equals("*")) //$NON-NLS-1$
			{
				return true;
			}
			// if return type is one
			else {
				return false;
			}
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getNavigationProperties(java.lang.String)
	 */
	@Override
	public List<NavigationProperty> getNavigationProperties(String eTypeName) {
		EntityType eType = EdmxUtilities.getEntityTypeByName(eTypeName,
				this.edmxObject.getEdmxDataServices().getSchemas());
		if (eType == null) {
			return new ArrayList<NavigationProperty>();
		}

		List<NavigationProperty> propertys = Arrays.asList(eType
				.getNavigationProperties());

		return propertys;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getAllEntitySets()
	 */
	@Override
	public List<EntitySetExt> getAllEntitySets() {
		List<EntitySetExt> allEntitySets = EdmxUtilities
				.getAllEntitySetsExt(this.edmxObject);

		return allEntitySets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getProperties(java.lang.String)
	 */
	@Override
	public List<Property> getProperties(String entityTypeName) {
		if (this.edmxObject != null) {
			return EdmxUtilities.getProperties(entityTypeName, this.edmxObject);
		}

		return new ArrayList<Property>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getComplextypeProperties(java.lang.String)
	 */
	@Override
	public List<Property> getComplextypeProperties(String cTypeName) {
		if (this.edmxObject != null) {
			return EdmxUtilities.getCtypeProperties(cTypeName, this.edmxObject);
		}

		return new ArrayList<Property>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getComplextypePropertiesCount(java.lang.String)
	 */
	@Override
	public int getComplextypePropertiesCount(String cTypeName) {
		if (this.edmxObject != null) {
			return EdmxUtilities.getCtypeProperties(cTypeName, this.edmxObject)
					.size();
		}

		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getAllEntityTypes()
	 */
	@Override
	public List<EntityType> getAllEntityTypes() {
		List<EntityType> entityTypesList = new LinkedList<EntityType>();

		Schema[] schemas = edmxObject.getEdmxDataServices().getSchemas();
		List<Schema> asList = Arrays.asList(schemas);

		for (Schema schema : asList) {
			EntityType[] entityTypes = schema.getEntityTypes();
			entityTypesList.addAll(Arrays.asList(entityTypes));
		}

		return entityTypesList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#getService ()
	 */
	@Override
	public EdmxDataServices getService() {
		return edmxObject.getEdmxDataServices();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getAllComplexTypes()
	 */
	@Override
	public List<ComplexType> getAllComplexTypes() {
		List<ComplexType> complexTypeList = new LinkedList<ComplexType>();

		Schema[] schemas = edmxObject.getEdmxDataServices().getSchemas();
		List<Schema> asList = Arrays.asList(schemas);

		for (Schema schema : asList) {
			ComplexType[] entityTypes = schema.getComplexTypes();
			complexTypeList.addAll(Arrays.asList(entityTypes));
		}

		return complexTypeList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getServiceClassName()
	 */
	@Override
	public String getServiceClassName() {
		return serviceName.trim().replaceAll(" ", "") + SERVICE_SUFFIX;//$NON-NLS-1$ //$NON-NLS-2$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getServiceName()
	 */
	@Override
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * This function will remove the package prefix of a Java type if its passed
	 * as a parameter or it will remove the nameSpace prefix of a EntityType if
	 * its passed as a parameter
	 * 
	 * @param fullName
	 * @return
	 */
	public String removePrefix(String fullName) {
		if (fullName.contains(DOT)) {
			fullName = fullName.substring(fullName.lastIndexOf(DOT) + 1);
			fullName = fullName.replaceAll("\\(", EMPTY); //$NON-NLS-1$ //$NON-NLS-2$
			fullName = fullName.replaceAll("\\)", EMPTY); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return fullName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#
	 * getGatewayServiceMetaDataXml()
	 */
	@Override
	public Object getGatewayServiceMetaDataXml() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.parser.impl.IOdataProxyHelper#isCreatable
	 * (java.lang.String)
	 */
	@Override
	public boolean isCreatable(String entityTypeName) {
		List<EntitySet> allCreatableCollections = getAllCreatableCollections();
		for (EntitySet entitySet : allCreatableCollections) {
			if (entitySet.getEntityType().contains(entityTypeName)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public List<EntitySet> getAllCollections() {
		return EdmxUtilities.getAllEntitySets(this.edmxObject);
	}

	@Override
	public String getMethodHeaderFromKeyPropertiesEntityType(
			String entityTypeName) throws TypeConversionException {
		return null;
	}
}
