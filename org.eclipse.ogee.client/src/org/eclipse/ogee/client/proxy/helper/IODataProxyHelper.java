/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.proxy.helper;

import java.util.Collection;
import java.util.List;

import org.eclipse.ogee.client.exceptions.ProxyException;
import org.eclipse.ogee.client.exceptions.TypeConversionException;
import org.eclipse.ogee.client.model.edmx.Association;
import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.EdmxDataServices;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.client.model.edmx.Parameter;
import org.eclipse.ogee.client.model.edmx.Property;
import org.eclipse.ogee.client.model.edmx.PropertyRef;
import org.eclipse.ogee.client.parser.impl.util.EntitySetExt;

public interface IODataProxyHelper {
	public abstract <T> String getConversionMethodName(String type,
			String propertyName, boolean isComplexType)
			throws TypeConversionException;

	public abstract String typeConvert(String type)
			throws TypeConversionException;

	public abstract boolean isOdataVersion0_2();

	public abstract boolean isOdataVersion1_0();

	public abstract String removePrefix(String fullName);

	public abstract String getServiceDescription();

	public abstract Edmx getEdmxObject();

	public abstract String getKeyPropertiesString(String entityTypeName);

	public abstract List<PropertyRef> getKeyPropertiesList(String entityTypeName);

	public abstract Collection<Property> getNonKeyPropertiesList(
			String entityTypeName);

	/**
	 * Returns true if the given property type is a known edm type according to
	 * the OData protocol.
	 * 
	 * @param type
	 *            The property type as found in the service metadata (including
	 *            the "Edm." prefix or other namespace).
	 */
	public abstract boolean isEdmType(String type);

	public abstract boolean isPostHttpMethod(String functionImportName);

	/**
	 * 
	 * @return Service Url string
	 */
	public abstract String getServiceUrl();

	/**
	 * 
	 * @return Service Url string
	 */
	public abstract String getShortServiceUrl();

	/**
	 * 
	 * @return All FunctionImports
	 */
	public abstract List<FunctionImport> getAllGetFunctionImports();

	/**
	 * 
	 * @return All FunctionImports
	 */
	public abstract List<FunctionImport> getAllFunctionImports();

	/**
	 * get the FunctionImports ReturnType as String
	 * 
	 * @param name
	 * @return
	 */
	public abstract String getFunctionImportsReturnType(String name);

	/**
	 * checks either the functionimport returns a List or Not.
	 * 
	 * @param name
	 * @returns a true / false value or either the function import returns a
	 *          EntitySet or EntityType.
	 */
	public abstract boolean isFunctionImportReturnsList(String name);

	/**
	 * @param name
	 * @returns a true / false value or either the function import returns a
	 *          complex type or an entity Type.
	 */
	public abstract boolean isFunctionImportReturnsAtomEntry(String name);

	/**
	 * 
	 * @return All Addressable collections
	 */
	public abstract List<EntitySet> getAllAddressableCollections();

	/**
	 * 
	 * @return All Addressable collections
	 */
	public abstract List<EntitySet> getAllCreatableCollections();

	/**
	 * 
	 * @return All updatable collections
	 */
	public abstract List<EntitySet> getAllUpdatableCollections();

	/**
	 * 
	 * @return All deletable collections
	 */
	public abstract List<EntitySet> getAllDeletableCollections();

	public abstract String getEntityTypeNameForEntitySet(String entitySetName);

	public abstract boolean isAddressable(String entityTypeName);

	public abstract boolean isUpdatable(String entityTypeName);

	public abstract boolean isDeletable(String entityTypeName);

	public abstract boolean isCreatable(String entityTypeName);

	public abstract boolean isMediaLinkEntry(String entityTypeName);

	/**
	 * 
	 * @return collections return type as String, by name
	 */
	public abstract String getAddressableCollectionsReturnType(String name);

	/**
	 * 
	 * @return collections method header (i.e. (String s, String b) )
	 * @throws Exception
	 */
	public abstract String getMethodHeaderFromKeyProperties(String entitySetName)
			throws TypeConversionException;

	/**
	 * 
	 * @return Entity constructor method header (i.e. (String s, String b) )
	 * @throws Exception
	 */
	public abstract String getConstructorHeaderFromKeyProperties(
			String entityName) throws TypeConversionException;

	/**
	 * 
	 * @return FunctionImport method header (i.e. (String s, String b) )
	 * @throws Exception
	 */
	public abstract String getFunctionImportsMethodHeader(String name)
			throws TypeConversionException;

	public abstract List<Parameter> getFunctionImportParameters(
			String functionImportName);

	public abstract boolean hasParameters(String functionImportName);

	public abstract String getParamterValue(String functionImportName,
			String paramerName);

	/**
	 * @return FunctionImport method args (i.e. (s,b) ) by name
	 * @throws TypeConversionException
	 * @throws ProxyException
	 */
	public abstract String getMethodArgsFromKeyProperties(String name)
			throws TypeConversionException, ProxyException;

	/**
	 * Returns all entity set ext for the service.
	 * 
	 * @return all entity set ext for the service
	 */
	public abstract List<EntitySetExt> getServices();

	/**
	 * Returns Association.
	 * 
	 * @return Returns Association.
	 */
	public abstract Association getAssociation(String name);

	/**
	 * Returns NavigationProperty's return type, by EntityType name and
	 * NavigationProperty's name
	 * 
	 * @return Returns NavigationProperty return type as string
	 */
	public abstract String getNavigationPropertyReturnType(
			String nPropertyName, String eTypeName);

	/**
	 * Returns NavigationProperty's return type class, by EntityType name and
	 * NavigationProperty's name
	 * 
	 * @return Returns NavigationProperty return type as string
	 */
	public abstract String getNavigationPropertyReturnTypeClass(
			String nPropertyName, String eTypeName);

	/**
	 * Returns is NavigationProperty's return type is a list or an single
	 * instance of entityType, by EntityType name and NavigationProperty's name
	 * 
	 * @return Returns NavigationProperty return type as string
	 */
	public abstract boolean isNavigationPropertyReturnTypeIsList(
			String nPropertyName, String eTypeName);

	/**
	 * Returns NavigationPropertys of a given EntityType .
	 * 
	 * @return Returns NavigationPropertys of a given EntityType .
	 */
	public abstract List<NavigationProperty> getNavigationProperties(
			String eTypeName);

	/**
	 * Returns all entity set ext for the service.
	 * 
	 * @return all entity set ext for the service
	 */
	public abstract List<EntitySetExt> getAllEntitySets();

	/**
	 * 
	 * Returns the entity type Properties by the given EntityType object name
	 * 
	 * @param entitySetExt
	 * @return List Property
	 */
	public abstract List<Property> getProperties(String entityTypeName);

	/**
	 * 
	 * Returns the entity type Properties by the given EntityType object name
	 * 
	 * @param entitySetExt
	 * @return List Property
	 */
	public abstract List<Property> getComplextypeProperties(String cTypeName);

	/**
	 * 
	 * Returns the count of entity type Properties by the given EntityType
	 * object name
	 * 
	 * @param entitySetExt
	 * @return List count
	 */
	public abstract int getComplextypePropertiesCount(String cTypeName);

	/**
	 * 
	 * 
	 * @return a list of all Services EntityTypes.
	 */
	public abstract List<EntityType> getAllEntityTypes();

	/**
	 * 
	 * 
	 * @return a list of all Services .
	 */
	public abstract EdmxDataServices getService();

	/**
	 * 
	 * 
	 * @return a list of all Services ComplexTypes.
	 */
	public abstract List<ComplexType> getAllComplexTypes();

	/**
	 * returns the Service Singeltone class name
	 * 
	 * @return
	 */
	public abstract String getServiceClassName();

	public abstract String getServiceName();

	public abstract Object getGatewayServiceMetaDataXml();

	public List<EntitySet> getAllCollections();

	public String getPropertyByPropertyRef(PropertyRef propertyRef,
			EntityType entityType) throws TypeConversionException,
			ProxyException;

	boolean isPutHttpMethod(String functionImportName);

	String getMethodHeaderFromKeyPropertiesEntityType(String entityTypeName)
			throws TypeConversionException;

	public boolean isComplexType(String name);
}
