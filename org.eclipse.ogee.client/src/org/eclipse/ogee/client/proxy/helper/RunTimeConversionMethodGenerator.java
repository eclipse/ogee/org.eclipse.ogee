/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.proxy.helper;

import org.eclipse.ogee.client.exceptions.TypeConversionException;
import org.eclipse.ogee.client.parser.impl.JavaVariableTypeConstants;
import org.eclipse.ogee.client.parser.impl.TypeConverter;

public class RunTimeConversionMethodGenerator implements
		IRunTimeConversionMethodGenerator {
	private static final String DOT = "."; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ogee.client.parser.impl.IRunTimeConversionMethodGenerator
	 * #getConversionMethodName(java.lang.String, java.lang.String, boolean)
	 */
	@Override
	public String getConversionMethodName(String type, String propertyName,
			boolean isComplexType) throws TypeConversionException {
		String res = null;
		TypeConverter converter = TypeConverter.ATOM;
		String javaType = converter.typeConvert(type);

		// convert to java type the input value
		if (javaType.equals(JavaVariableTypeConstants.INT)) {
			res = "converter.getAsInt(" + getPropertyStringByType(isComplexType) + "(\"" + propertyName + "\"))"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return res;
		}

		if (javaType.equals(JavaVariableTypeConstants.INTEGER)) {
			res = "converter.getAsInt(" + getPropertyStringByType(isComplexType) + "(\"" + propertyName + "\"))"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return res;
		}

		if (javaType.equals(JavaVariableTypeConstants.BOOLEAN)) {
			res = "converter.getAsBoolean(" + getPropertyStringByType(isComplexType) + "(\"" + propertyName //$NON-NLS-1$ //$NON-NLS-2$
					+ "\"))"; //$NON-NLS-1$
			return res;
		}

		if (javaType.equals(JavaVariableTypeConstants.DOUBLE)) {
			res = "converter.getAsDouble(" + getPropertyStringByType(isComplexType) + "(\"" + propertyName + "\"))"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return res;
		}

		if (javaType.equals(JavaVariableTypeConstants.DATE)) {
			res = "converter.getAsDate(" + getPropertyStringByType(isComplexType) + "(\"" + propertyName + "\"))"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return res;
		}

		if (javaType.equals(JavaVariableTypeConstants.STRING)) {
			res = "converter.getAsString(" + getPropertyStringByType(isComplexType) + "(\"" + propertyName + "\"))"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return res;
		}

		if (javaType.equals(JavaVariableTypeConstants.FLOAT)) {
			res = "converter.getAsFloat(" + getPropertyStringByType(isComplexType) + "(\"" + propertyName + "\"))"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return res;
		}

		if (javaType.equals(JavaVariableTypeConstants.UUID)) {
			res = "converter.getAsUUID(" + getPropertyStringByType(isComplexType) + "(\"" + propertyName + "\"))"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return res;
		}

		if (javaType.equals(JavaVariableTypeConstants.LONG)) {
			res = "converter.getAsLong(" + getPropertyStringByType(isComplexType) + "(\"" + propertyName + "\"))"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return res;
		}

		/*
		 * This must be a complex type since it didn't match any known java
		 * types.
		 * 
		 * At runtime this will throw an Exception if metaData was incorrect and
		 * Didn't describe this type's structure.
		 */
		res = "new " + removePrefix(type) + "(entry.getProperty(\"" + propertyName + "\"),service);"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ogee.client.parser.impl.IRunTimeConversionMethodGenerator
	 * #removePrefix(java.lang.String)
	 */
	@Override
	public String removePrefix(String fullName) {
		if (fullName.contains(DOT)) {
			fullName = fullName.substring(fullName.lastIndexOf(DOT) + 1);

			fullName = fullName.replaceAll("\\(", ""); //$NON-NLS-1$ //$NON-NLS-2$
			fullName = fullName.replaceAll("\\)", ""); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return fullName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ogee.client.parser.impl.IRunTimeConversionMethodGenerator
	 * #getPropertyStringByType(boolean)
	 */
	@Override
	public String getPropertyStringByType(boolean isComplexType) {
		return (isComplexType ? "property.getChildProperty" : "entry.getProperty"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public String getFunctionImportComplexTypeMethodName(String type,
			String propertyName) throws TypeConversionException {
		String res = null;
		TypeConverter converter = TypeConverter.ATOM;
		String javaType = converter.typeConvert(type);

		if (javaType.equals(JavaVariableTypeConstants.STRING)) {
			res = "TypeConverter.getAsString(entry.getFunctionImportComplexPropertyValue(\"" + propertyName + "\")" + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return res;
		}
		if (javaType.equals(JavaVariableTypeConstants.BOOLEAN)) {
			res = "TypeConverter.getAsBoolean(entry.getFunctionImportComplexPropertyValue(\"" + propertyName + "\")" + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return res;
		}

		// TODO: add here more java types!!!

		return res;
	}
}
