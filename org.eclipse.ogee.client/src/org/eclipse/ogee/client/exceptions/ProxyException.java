/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.exceptions;

/**
 * Thrown when an error occurred in the proxy.
 * 
 * 
 */
public class ProxyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2543266976590564130L;

	public ProxyException() {
		super();
	}

	public ProxyException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProxyException(String message) {
		super(message);
	}

	public ProxyException(Throwable cause) {
		super(cause);
	}

}
