/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.exceptions;

/**
 * Thrown when an error occurred while parsing.
 * 
 */
public class ParserException extends Exception {

	private static final long serialVersionUID = -2395591646741741045L;

	private String xml = null;

	public ParserException(String message, String xml) {
		super(message);
		this.xml = xml;
	}

	public ParserException(String message, Throwable cause, String xml) {
		super(message, cause);
		this.xml = xml;
	}

	public ParserException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParserException(Throwable cause) {
		super(cause);
	}

	public ParserException(String message) {
		super(message);
	}

	public ParserException() {
	}

	public String getXml() {
		return xml;
	}
}
