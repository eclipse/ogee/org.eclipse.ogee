/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.exceptions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.ogee.client.util.Util;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

/**
 * Java class for OData error response received from the server. Used by
 * RestClientException.
 * 
 * The spec for the message can be found here:
 * http://www.odata.org/media/6655/%5Bmc-apdsu%5D%5B1%5D.htm#_Toc246716641
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="message">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="innererror" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * For example:
 * 
 * <pre>
 *  &lt;error xmlns="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata">
 * 	&lt;code>/IWFND/CM_CONSUMER/102&lt;/code>
 * 		&lt;message>
 * 		Service Document '/IWFND/ERROR' not found. Check spelling and capitalization.
 * 		&lt;/message>
 * 	&lt;innererror>
 * 		&lt;transactionid>BE143DE181CAF1BD94DE005056B43CF0&lt;/transactionid>
 * 	&lt;/innererror>
 * &lt;/error>
 * </pre>
 * 
 * Note that the tag transactionid is optional and not part of the schema. In
 * case it exists, it's content will be part of the innererror field.
 * 
 * In the example above (message contains the above text): <code>
 * ODataError error = ODataErrorcreateErrorFromMessage(message);
 * 
 * </code>
 * 
 * will print this to the console:
 * 
 * <pre>
 *  &lt;innererror>
 * 		&lt;transactionid>BE143DE181CAF1BD94DE005056B43CF0&lt;/transactionid>
 * 	&lt;/innererror>
 * </pre>
 * 
 */
public class ODataError {
	private final static Logger LOGGER = Logger.getLogger(ODataError.class
			.getName());
	protected String code;
	protected ODataError.Message message;
	protected String innererror;

	/**
	 * Default constructor
	 */
	public ODataError() {

	}

	/**
	 * Gets the value of the code property.
	 * 
	 * @return code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the value of the code property.
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the value of the message property.
	 * 
	 * @return possible object is {@link Error.Message }
	 */
	public ODataError.Message getMessage() {
		return message;
	}

	/**
	 * Sets the value of the message property.
	 * 
	 * @param value
	 *            allowed object is {@link Error.Message }
	 */
	public void setMessage(ODataError.Message value) {
		this.message = value;
	}

	/**
	 * Gets the value of the innererror property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getInnererror() {
		return innererror;
	}

	/**
	 * Sets the value of the innererror property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInnererror(String value) {
		this.innererror = value;
	}

	/**
	 * Error message as received from the server OData error response.
	 * 
	 */
	public static class Message {
		protected String content;
		protected String lang;

		/**
		 * Gets the value of the content property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getContent() {
			return content;
		}

		/**
		 * Sets the value of the content property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setContent(String value) {
			this.content = value;
		}

		/**
		 * Gets the value of the lang property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getLang() {
			return lang;
		}

		/**
		 * Sets the value of the lang property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setLang(String value) {
			this.lang = value;
		}

		@Override
		public String toString() {
			return content;
		}
	}

	@Override
	public String toString() {
		return message.toString();
	}

	/**
	 * Set the internal Message object content Can also be accessed through
	 * ODataError.Message.setContent
	 * 
	 * @param messageContent
	 */
	public void setMessageContent(String messageContent) {
		this.message = new Message();
		if (messageContent != null)
			message.setContent(messageContent.trim());
	}

	/**
	 * Get the internal Message object content Can also be accessed through
	 * ODataError.Message.setContent
	 * 
	 * @return the internal message content
	 */
	public String getMessageContent() {
		return message.getContent();
	}

	/**
	 * Parse the odata xml response from the server and build a ODataError
	 * object
	 * 
	 * @param message
	 *            odata xml
	 * @return ODataError created object
	 * @throws ParserException
	 */
	public static ODataError createErrorFromXmlMessage(String message)
			throws ParserException {
		ODataError error = new ODataError();
		StringBuffer sb = new StringBuffer();
		ByteArrayInputStream bais = null;

		try {
			bais = new ByteArrayInputStream(message.getBytes(Util.UTF_8));
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			factory.setFeature(
					"http://xml.org/sax/features/external-general-entities", false); //$NON-NLS-1$
			factory.setFeature(
					"http://xml.org/sax/features/external-parameter-entities", false); //$NON-NLS-1$
			factory.setFeature(
					"http://apache.org/xml/features/disallow-doctype-decl", true); //$NON-NLS-1$
			factory.setFeature("http://xml.org/sax/features/validation", true); //$NON-NLS-1$
			factory.setFeature(
					"http://apache.org/xml/features/validation/schema", true); //$NON-NLS-1$
			factory.setFeature(
					"http://apache.org/xml/features/validation/schema-full-checking", true); //$NON-NLS-1$
			factory.setValidating(true);

			Document doc = factory.newDocumentBuilder().parse(bais);

			Element root = doc.getDocumentElement();

			NodeList childNodes = root.getChildNodes();
			int length = childNodes.getLength();

			for (int i = 0; i < length; i++) {
				Node item = childNodes.item(i);

				String name = item.getNodeName();
				Node firstChild = item.getFirstChild();
				if (firstChild != null) {
					String value = firstChild.getNodeValue();
					sb.append(name).append(":").append(value).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
					if (name.equalsIgnoreCase("message")) //$NON-NLS-1$
					{
						error.setMessageContent(value);
					} else if (name.equalsIgnoreCase("code")) //$NON-NLS-1$
					{
						error.setCode(value);
					}
					if (name.equalsIgnoreCase("innererror")) //$NON-NLS-1$
					{
						error.setInnererror(innerXml(item));
					}
				}
			}

			return error;
		} catch (UnsupportedEncodingException e) {
			throw new ParserException(e);
		} catch (SAXException e) {
			throw new ParserException(e);
		} catch (IOException e) {
			throw new ParserException(e);
		} catch (ParserConfigurationException e) {
			throw new ParserException(e);
		} finally {
			if (bais != null) {
				try {
					bais.close();
				} catch (IOException e) {
					LOGGER.severe(e.getMessage());
				}
			}
		}
	}

	/**
	 * Parse the odata xml response from the server and build a ODataError
	 * object
	 * 
	 * @param message
	 *            odata xml
	 * @return ODataError created object
	 * @throws ParserException
	 */
	public static ODataError createErrorFromHtmlMessage(String html)
			throws ParserException {
		ODataError error = new ODataError();

		ODataError.Message message = new Message();
		message.setContent(html);

		error.setMessage(message);

		return error;
	}

	private static String innerXml(Node node) {
		DOMImplementationLS lsImpl = (DOMImplementationLS) node
				.getOwnerDocument().getImplementation().getFeature("LS", "3.0"); //$NON-NLS-1$ //$NON-NLS-2$
		LSSerializer lsSerializer = lsImpl.createLSSerializer();
		lsSerializer.getDomConfig().setParameter("xml-declaration", false); //$NON-NLS-1$
		NodeList childNodes = node.getChildNodes();
		StringBuilder sb = new StringBuilder();
		int length = childNodes.getLength();
		for (int i = 0; i < length; i++) {
			sb.append(lsSerializer.writeToString(childNodes.item(i)));
		}
		return sb.toString().trim();
	}
}
