/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.exceptions;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import org.eclipse.ogee.client.connectivity.api.IRestResponse;
import org.eclipse.ogee.client.connectivity.impl.Headers;

/**
 * Thrown when an error occurred in IRestClient execute or send method. This can
 * be caused when a REST request was sent to the server and an error was
 * returned by the server. The exception implements {@link IRestResponse}
 * interface. Through this exception it's possible to get information about the
 * HTTP status code that was returned by the server. In case the error was not
 * received from the server, the status code will be 0. In addition to this an
 * error body and the parsed server error is available through getODataError()
 * method.
 * 
 * @see ODataError
 * @see IRestResponse
 */
public class RestClientException extends Exception implements IRestResponse {
	private static final String HTML = "<html"; //$NON-NLS-1$

	private static final String XML = "<xml"; //$NON-NLS-1$

	private static final String UTF_8 = "UTF-8"; //$NON-NLS-1$

	private final static Logger LOGGER = Logger
			.getLogger(RestClientException.class.getName());

	private static final long serialVersionUID = -6091570883387796735L;

	private int statusCode;
	private String body;
	private Headers headers = new Headers();
	private ODataError odataError;

	private byte[] bodyBytes;

	/**
	 * Default constructor
	 */
	public RestClientException() {

	}

	public RestClientException(IRestResponse restResponse) {
		super();
		setStatusCode(restResponse.getStatusCode());
		setBody(restResponse.getBody());
		setHeaders(restResponse.getHeaders());
	}

	public RestClientException(String message) {
		super(message);
	}

	public RestClientException(String message, Throwable t) {
		super(message, t);
	}

	public RestClientException(String message, IRestResponse restResponse,
			Throwable t) {
		super(message, t);
		setStatusCode(restResponse.getStatusCode());
		setBody(restResponse.getBody());
		setHeaders(restResponse.getHeaders());
	}

	@Override
	public int getStatusCode() {
		return this.statusCode;
	}

	@Override
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public String getBody() {
		try {
			if (this.body == null) {
				this.body = new String(getBodyBytes(), UTF_8);
			}
		} catch (UnsupportedEncodingException e) {
			LOGGER.severe(e.getMessage());
		}

		return this.body;
	}

	@Override
	public void setBody(String body) {
		if (body == null) {
			return;
		}

		try {
			this.bodyBytes = body.getBytes(UTF_8);
			this.body = body;
		} catch (UnsupportedEncodingException e) {
			LOGGER.severe(e.getMessage());
		}
	}

	@Override
	public String getHeader(String header) {
		return this.headers.getFirst(header);
	}

	@Override
	public List<String> getHeaders(String header) {
		return this.headers.get(header);
	}

	@Override
	public void setHeader(String header, String value) {
		this.headers.put(header, value);
	}

	@Override
	public void setHeaders(Headers headers) {
		this.headers = headers;

	}

	@Override
	public Headers getHeaders() {
		return this.headers;
	}

	/**
	 * Returns the internal ODataError as received and parsed from the server.
	 * Returns null in case the error was not received from the server.
	 * 
	 * @see ODataError
	 * @return the internal ODataError as received and parsed from the server
	 */
	public ODataError getODataError() {
		try {
			if (this.body == null) {
				this.body = new String(getBodyBytes(), UTF_8);
			}

			if (body.length() >= 5) {
				String bodyStartString = body.substring(0, 5).toLowerCase(
						Locale.ENGLISH);
				if (bodyStartString.startsWith(XML)) //$NON-NLS-1$
					this.odataError = ODataError
							.createErrorFromXmlMessage(this.body);
				else if (bodyStartString.startsWith(HTML)) //$NON-NLS-1$
					this.odataError = ODataError
							.createErrorFromHtmlMessage(this.body);
			} else {
				this.odataError = new ODataError();
				this.odataError.setMessageContent(this.body);
			}
		} catch (UnsupportedEncodingException e) {
			LOGGER.severe(e.getMessage());
		} catch (ParserException e) {
			LOGGER.severe(e.getMessage());
		}

		return this.odataError;
	}

	/**
	 * Setter method for the internal ODataError
	 * 
	 * @param odataError
	 */
	public void setODataError(ODataError odataError) {
		this.odataError = odataError;
	}

	@Override
	public void setBody(byte[] byteArray) {
		this.bodyBytes = byteArray;
		this.body = null;
	}

	@Override
	public byte[] getBodyBytes() {
		if (this.bodyBytes == null) {
			return new byte[0];
		}

		return Arrays.copyOf(this.bodyBytes, this.bodyBytes.length);
	}
}
