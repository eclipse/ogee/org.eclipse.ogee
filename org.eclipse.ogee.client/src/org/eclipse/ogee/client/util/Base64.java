/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.util;

import java.io.UnsupportedEncodingException;

/**
 * Helper class used for base 64 encoding only.
 * 
 * 
 */
public class Base64 {
	private static final String CHARACTER_ENCODING = "UTF-8"; //$NON-NLS-1$
	private static final String base64code = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz" + "0123456789" + "+/"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	public static byte[] zeroPad(int length, byte[] bytes) {
		byte[] padded = new byte[length]; // initialized to zero by JVM
		System.arraycopy(bytes, 0, padded, 0, bytes.length);
		return padded;
	}

	/**
	 * Returns a base64 representation of the given string.
	 * 
	 * @param string
	 * @return base64 representation of the given string
	 */
	public static String encode(String string) {
		byte[] stringArray = null;

		try {
			stringArray = string.getBytes(CHARACTER_ENCODING);
		} catch (UnsupportedEncodingException e) {
			// TODO:Add Logging @Slavik
			return null;
		}
		// determine how many padding bytes to add to the output
		int paddingCount = (3 - (stringArray.length % 3)) % 3;
		// add any necessary padding to the input
		stringArray = zeroPad(stringArray.length + paddingCount, stringArray);
		// process 3 bytes at a time, churning out 4 output bytes
		// worry about CRLF insertions later
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < stringArray.length; i += 3) {
			int j = ((stringArray[i] & 0xff) << 16)
					+ ((stringArray[i + 1] & 0xff) << 8)
					+ (stringArray[i + 2] & 0xff);
			sb.append(base64code.charAt((j >> 18) & 0x3f))
					.append(base64code.charAt((j >> 12) & 0x3f))
					.append(base64code.charAt((j >> 6) & 0x3f))
					.append(base64code.charAt(j & 0x3f));
		}

		String encoded = sb.toString();
		// replace encoded padding nulls with "="
		return (encoded.substring(0, encoded.length() - paddingCount) + "==".substring(0, paddingCount)); //$NON-NLS-1$
	}
}
