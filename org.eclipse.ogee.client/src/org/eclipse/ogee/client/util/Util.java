/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.util.UUID;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.ogee.client.exceptions.RestClientException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Utility class.
 */
public class Util {
	private final static Logger LOGGER = Logger.getLogger(Util.class.getName());

	public static final String UTF_8 = "UTF-8"; //$NON-NLS-1$

	/**
	 * Utility method for converting input stream to string
	 * 
	 * @param inputStream
	 *            input stream
	 * @return string
	 * @throws IOException
	 */
	public static String convertStreamToString(InputStream inputStream)
			throws IOException {
		/*
		 * To convert the InputStream to String we use the Reader.read(char[]
		 * buffer) method. We iterate until the Reader return -1 which means
		 * there's no more data to read. We use the StringWriter class to
		 * produce the string.
		 */
		if (inputStream == null) {
			return ""; //$NON-NLS-1$
		}

		Writer writer = null;
		Reader reader = null;

		try {
			writer = new StringWriter();

			char[] buffer = new char[8192];
			reader = new BufferedReader(new InputStreamReader(inputStream,
					UTF_8));
			int n;
			while ((n = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, n);
			}

			return writer.toString();
		} catch (UnsupportedEncodingException e) {
			LOGGER.severe(e.getMessage());
		} finally {
			inputStream.close();

			if (reader != null) {
				reader.close();
			}

			if (writer != null) {
				writer.close();
			}
		}

		return null;
	}

	/**
	 * Utility method for converting input stream to byte array
	 * 
	 * @param inputStream
	 *            input stream
	 * @return byte[] byte array
	 * @throws IOException
	 */
	public static byte[] convertStreamToByteArray(InputStream inputStream)
			throws IOException {
		if (inputStream == null) {
			return new byte[0];
		}

		ByteArrayOutputStream baos = null;

		try {
			baos = new ByteArrayOutputStream();
			int next = inputStream.read();
			while (next > -1) {
				baos.write(next);
				next = inputStream.read();
			}
			baos.flush();
			byte[] result = baos.toByteArray();

			return result;
		} finally {
			if (baos != null) {
				baos.close();
			}
		}
	}

	/**
	 * Parses error xml to extract the error message
	 * 
	 * @param message
	 *            error xml
	 * @return message string
	 */
	public static String getErrorMessage(String message) {
		StringBuffer sb = new StringBuffer();
		ByteArrayInputStream bais = null;

		try {
			bais = new ByteArrayInputStream(message.getBytes(UTF_8));
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			factory.setValidating(true);
			factory.setFeature(
					"http://apache.org/xml/features/validation/schema", true); //$NON-NLS-1$ 
			factory.setFeature(
					"http://apache.org/xml/features/validation/schema-full-checking", true); //$NON-NLS-1$
			factory.setFeature(
					"http://xml.org/sax/features/external-general-entities", false); //$NON-NLS-1$
			factory.setFeature(
					"http://xml.org/sax/features/external-parameter-entities", false); //$NON-NLS-1$
			factory.setFeature(
					"http://apache.org/xml/features/disallow-doctype-decl", true); //$NON-NLS-1$

			Document doc = factory.newDocumentBuilder().parse(bais);

			Element root = doc.getDocumentElement();

			NodeList childNodes = root.getChildNodes();
			int length = childNodes.getLength();

			for (int i = 0; i < length; i++) {
				Node item = childNodes.item(i);

				String name = item.getNodeName();
				if (item.getFirstChild() != null) {
					String value = item.getFirstChild().getNodeValue();
					sb.append(name).append(":").append(value).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
					if (name.equalsIgnoreCase("message")) //$NON-NLS-1$
						return value;
				}

			}

			return sb.toString();
		} catch (UnsupportedEncodingException e) {
			LOGGER.severe(e.getMessage());
			return message;
		} catch (SAXException e) {
			LOGGER.severe(e.getMessage());
			return message;
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
			return message;
		} catch (ParserConfigurationException e) {
			LOGGER.severe(e.getMessage());
			return message;
		} finally {
			if (bais != null) {
				try {
					bais.close();
				} catch (IOException e) {
					LOGGER.severe(e.getMessage());
				}
			}
		}
	}

	/**
	 * Adds a url parameter to the given urlString
	 * 
	 * @param urlString
	 *            the string of the url
	 * @param name
	 *            the name of the url parameter
	 * @param value
	 *            the value of the url parameter
	 * @return new url with the parameter
	 * @throws RestClientException
	 */
	public static String addParameter(String urlString, String name,
			String value) throws RestClientException {
		int qpos = urlString.indexOf('?');
		int hpos = urlString.indexOf('#');
		char sep = qpos == -1 ? '?' : '&';
		StringBuilder builder = new StringBuilder();
		builder.append(sep).append(encodeForUrl(name));

		builder.append('=').append(encodeForUrl(value.toString()));

		String seg = builder.toString();
		return hpos == -1 ? urlString + seg : urlString.substring(0, hpos)
				+ seg + urlString.substring(hpos);
	}

	/**
	 * Adds a url parameter to the given urlString
	 * 
	 * @param urlString
	 *            the string of the url
	 * @param name
	 *            the name of the url parameter
	 * @param value
	 *            the value of the url parameter
	 * @return new url with the parameter
	 * @throws RestClientException
	 */
	public static String addParameter(String urlString, String name, int value)
			throws RestClientException {
		int qpos = urlString.indexOf('?');
		int hpos = urlString.indexOf('#');
		char sep = qpos == -1 ? '?' : '&';
		StringBuilder builder = new StringBuilder();
		builder.append(sep).append(encodeForUrl(name));

		builder.append('=').append(encodeForUrl(String.valueOf(value)));

		String seg = builder.toString();
		return hpos == -1 ? urlString + seg : urlString.substring(0, hpos)
				+ seg + urlString.substring(hpos);
	}

	/**
	 * Removes the parameters from the given url
	 * 
	 * @param inputUri
	 * @return - the url without the parameters.
	 */
	public static String removeURLParameters(String inputUri) {
		if (inputUri.contains("$")) {
			inputUri = inputUri.substring(0, inputUri.indexOf('$') - 1);
		}

		if (inputUri.contains("?")) {
			inputUri = inputUri.substring(0, inputUri.indexOf('?'));
		}

		if (!inputUri.endsWith("/")) {
			inputUri = inputUri + "/";
		}

		return inputUri;
	}

	/**
	 * UTF-8 URL encoding Encodes string to be used in a URL
	 * 
	 * @param string
	 *            URL to be encoded
	 * @return String encoded URL
	 */
	public static String encodeForUrl(String string) {
		try {
			return URLEncoder.encode(string, UTF_8);
		} catch (UnsupportedEncodingException e) {
			LOGGER.warning(e.getMessage());
			return string;
		}
	}

	/**
	 * Utility method for getting a ByteBuffer from an input stream.
	 * 
	 * @param inputStream
	 * @return - ByteBuffer from an input stream.
	 * @throws IOException
	 */
	public static ByteBuffer getBytes(InputStream inputStream)
			throws IOException {
		ByteArrayOutputStream baos = null;

		try {
			baos = new ByteArrayOutputStream();

			byte[] buf = new byte[512];
			while (true) {
				int len = inputStream.read(buf);
				if (len == -1) {
					break;
				}

				baos.write(buf, 0, len);
			}

			byte[] array = baos.toByteArray();
			return ByteBuffer.wrap(array);
		} finally {
			if (baos != null) {
				baos.close();
			}
		}
	}

	/**
	 * Returns a boundary specification for a batch request.
	 * 
	 * @return a boundary specification for a batch request.
	 */
	public static String generateBoundary() {
		return UUID.randomUUID().toString();
	}
}
