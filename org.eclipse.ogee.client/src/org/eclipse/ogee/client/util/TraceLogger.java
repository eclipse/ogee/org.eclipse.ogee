/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.util;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.ogee.client.activator.Activator;
import org.eclipse.ogee.client.nls.messages.Messages;

/**
 * Logging API
 * 
 * @author SAP SE
 */
public class TraceLogger {
	private static final String ORG_ECLIPSE_OGEE_CLIENT = "org.eclipse.ogee.client"; //$NON-NLS-1$

	private static Map<String, TraceLogger> loggers = new HashMap<String, TraceLogger>();
	private static PrintStream tracingOut = System.out;
	private static DateFormat dateFormatter = DateFormat.getDateTimeInstance(
			DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.ENGLISH);

	private String id;

	private Boolean tracing;

	protected TraceLogger(String id) {
		if (id == null || id.isEmpty())
			id = ORG_ECLIPSE_OGEE_CLIENT;
		this.id = id;
	}

	public void log(String message) {
		log(IStatus.INFO, IStatus.OK, message, null);
	}

	public void logError(Throwable exception) {
		logError(Messages.Logger_0, exception);
	}

	public void logError(String message, Throwable exception) {
		log(IStatus.ERROR, IStatus.OK, message, exception);
	}

	public void logError(String message) {
		log(IStatus.ERROR, IStatus.OK, message, null);
	}

	public void logWarning(String message) {
		log(IStatus.WARNING, IStatus.OK, message, null);
	}

	public void logWarning(Throwable exception) {
		logError(Messages.Logger_1, exception);
	}

	public void logWarning(String message, Throwable exception) {
		log(IStatus.WARNING, IStatus.OK, message, exception);
	}

	public void log(int severity, int code, String message, Throwable exception) {
		log(createStatus(severity, code, message, exception));
	}

	public IStatus createStatus(int severity, int code, String message,
			Throwable exception) {
		return new Status(severity, id, code, message, exception);
	}

	public void log(IStatus status) {
		Activator.getDefault().getLog().log(status);
	}

	/**
	 * Check if Eclipse is started in debug mode and tracing is enabled for the
	 * plug-in with the Logger id The tracing setting format is
	 * "<plug-in id>/debug". <br>
	 * In case that Eclipse is running in debug mode, but tracing is not
	 * activated for the plug-in, a message will be printed to the trace with
	 * the plug-in id: "plug-in <id> is not traced!"
	 * 
	 * Tracing is possible only if the logger id is the same as plug-in id.
	 */
	public boolean isTracing() {
		if (!Platform.inDebugMode()) {
			return false;
		}

		String value = Platform.getDebugOption(id + "/debug"); //$NON-NLS-1$		
		boolean tracingFlag = Boolean.TRUE.toString().equalsIgnoreCase(value);

		if (this.tracing == null && !tracingFlag) {
			// this message printed to the trace only once
			tracingOut.println("Plug-in " + id + " is not traced!"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		this.tracing = tracingFlag;

		return this.tracing;
	}

	/**
	 * Trace a formatted string using the specified format string and arguments. <br>
	 * The arguments list is optional and can be empty. The format string should
	 * be in Java Format string syntax: {@link http
	 * ://docs.oracle.com/javase/6/docs/api/java/util/Formatter.html#syntax} <br>
	 * Tracing is possible only if the logger id is the same as plug-in id. <br>
	 * Tracing will occur only if isTracing() function returns true. <br>
	 * Note that there is no need to call isTracing() before the call to trace
	 * function. It will be called automatically by the trace function.
	 * 
	 * @param message
	 *            A format string as described in Java Format string syntax
	 * @param args
	 *            Arguments referenced by the format specifiers in the format
	 *            string. If there are more arguments than format specifiers,
	 *            the extra arguments are ignored. The number of arguments is
	 *            variable and may be zero.
	 * 
	 * @see org.eclipse.ogee.utils.logger.Logger#isTracing isTracing()
	 */
	public void trace(String message, Object... args) {
		if (!isTracing())
			return;

		// current time
		Calendar cal = Calendar.getInstance();
		String time = dateFormatter.format(cal.getTime());
		tracingOut.println(time + " " + id); //$NON-NLS-1$

		// get the caller details and format it to string
		StackTraceElement myCaller = Thread.currentThread().getStackTrace()[2];
		String callerString = myCaller.getClassName()
				+ "." + myCaller.getMethodName() + " (" + myCaller.getFileName() //$NON-NLS-1$ //$NON-NLS-2$
				+ ":" + myCaller.getLineNumber() + ")"; //$NON-NLS-1$ //$NON-NLS-2$

		tracingOut.println(callerString);

		tracingOut.print("TRACE: "); //$NON-NLS-1$
		tracingOut.printf(message, args);
		tracingOut.println();
	}

	/**
	 * Retrieve a logger according to the value of the id parameter. If the
	 * logger already exists, then the existing instance will be returned.
	 * Otherwise, a new instance is created.
	 * 
	 * The id should match the plug-in id and will be used in the Plug-in column
	 * in the Error Log
	 * 
	 * @param id
	 * @return Logger
	 */
	public static TraceLogger getLogger(String id) {
		TraceLogger logger = loggers.get(id);
		if (logger == null)
			logger = createLogger(id);
		return logger;
	}

	/**
	 * Retrieve a logger according to the value of the package of the class
	 * parameter. If the logger already exists, then the existing instance will
	 * be returned. Otherwise, a new instance is created.
	 * 
	 * The package of the class should match the plug-in id or extend it and
	 * will be used in the Plug-in column in the Error Log
	 * 
	 * @param c
	 * @return TraceLogger instance
	 */
	public static TraceLogger getLogger(Class<?> c) {
		String id = c.getPackage().getName();
		TraceLogger logger = loggers.get(id);
		if (logger == null)
			logger = createLogger(id);
		return logger;
	}

	private static TraceLogger createLogger(String id) {
		TraceLogger l = new TraceLogger(id);
		loggers.put(id, l);
		return l;
	}

	/**
	 * Retrieve the default utils logger The utils logger id is
	 * "org.eclipse.ogee.utils"
	 * 
	 * @return Logger
	 */
	public static TraceLogger getUtilsLogger() {
		return getLogger(Activator.PLUGIN_ID);
	}

	/**
	 * Retrieve the default framework logger
	 * 
	 * @return Logger
	 */
	public static TraceLogger getFrameworkLogger() {
		return getLogger(ORG_ECLIPSE_OGEE_CLIENT);
	}
}
