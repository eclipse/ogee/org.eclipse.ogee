/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser;

/**
 * OData supports two formats for representing the resources (Collections,
 * Entries, Links, etc) it exposes: the XML-based AtomPub format and the JSON
 * format.
 * 
 */
public enum Representation {
	JSON, ATOM

}
