/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser;

import org.eclipse.ogee.client.exceptions.MarshalerException;
import org.eclipse.ogee.client.exceptions.ParserException;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.generic.ODataCollection;
import org.eclipse.ogee.client.model.generic.ODataEntry;
import org.eclipse.ogee.client.model.generic.ODataProperty;
import org.eclipse.ogee.client.model.generic.ServiceDocument;
import org.eclipse.ogee.client.parser.impl.TypeConverter;

/**
 * Interface of an OData Parser
 */
public interface IODataParser {
	/**
	 * Converts Atom / JSON format to an ODataEntry object
	 * 
	 * @param data
	 * @return ODataEntry
	 * @throws ParserException
	 */
	public ODataEntry parseODataEntry(String data) throws ParserException;
	
	/**
	 * Converts Atom / JSON format to ODataCollection object
	 * 
	 * @param data
	 * @return ODataCollection
	 * @throws ParserException
	 */
	public ODataCollection parseODataCollection(String data)
			throws ParserException;

	/**
	 * Converts Atom / JSON format to ODataProperty object
	 * 
	 * @param data
	 * @return Property
	 * @throws ParserException
	 */
	public ODataProperty parseODataProperty(String data) throws ParserException;

	/**
	 * Converts Atom / JSON format to a ServiceDocument object
	 * 
	 * @param data
	 * @return ServiceDocument service document that lists all the top-level
	 *         collections.
	 * @throws ParserException
	 */
	public ServiceDocument parseServiceDocument(String data)
			throws ParserException;

	/**
	 * Converts ODataCollection object to Atom / JSON format string
	 * 
	 * @param collection
	 *            ODataCollection
	 * @return String
	 * @throws MarshalerException
	 */
	public String format(ODataCollection collection) throws MarshalerException;

	/**
	 * Converts ODataEntry object to Atom / JSON format string
	 * 
	 * @param entry
	 *            ODataEntry
	 * @return String
	 * @throws MarshalerException
	 */
	public String format(ODataEntry entry) throws MarshalerException;

	/**
	 * Returns the type converter.
	 * 
	 * @return - the type converter.
	 */
	public TypeConverter getTypeConverter();

	/**
	 * @return The parser representation type: Atom / JSON
	 */
	public Representation getRepresentation();

	/**
	 * Converts Edmx object to xml string
	 * 
	 * @param edmx
	 *            Edmx
	 * @return xml string
	 * @throws MarshalerException
	 */
	public String format(Edmx edmx) throws MarshalerException;

}
