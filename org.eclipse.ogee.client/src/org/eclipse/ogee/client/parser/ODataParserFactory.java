/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser;

public class ODataParserFactory {

	/**
	 * Creates an IODataParser instance based on the given representation.
	 * Supports Atom and JSON representations.
	 * 
	 * @param representation
	 *            Representation enum. Possible values: Atom, JSON
	 * @return IODataParser IODataParser instance
	 */
	public static IODataParser createInstance(Representation representation) {
		switch (representation) {
		case JSON:
			return new ODataJSONParser();

		case ATOM:
			return new ODataAtomParser();

		default:
			return null;
		}
	}
}
