/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl;

import java.util.List;

import org.eclipse.ogee.client.model.edm.v4.Action;
import org.eclipse.ogee.client.model.edm.v4.ActionFunctionParameter;
import org.eclipse.ogee.client.model.edm.v4.ActionFunctionReturnType;
import org.eclipse.ogee.client.model.edm.v4.ActionImport;
import org.eclipse.ogee.client.model.edm.v4.Annotation;
import org.eclipse.ogee.client.model.edm.v4.AnnotationsV4;
import org.eclipse.ogee.client.model.edm.v4.ApplyExpression;
import org.eclipse.ogee.client.model.edm.v4.BinaryConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.BoolConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.CastOrIsOfExpression;
import org.eclipse.ogee.client.model.edm.v4.CollectionExpression;
import org.eclipse.ogee.client.model.edm.v4.ComplexTypeV4;
import org.eclipse.ogee.client.model.edm.v4.DateConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.DateTimeOffsetConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.DecimalConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.DurationConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.EntityContainerV4;
import org.eclipse.ogee.client.model.edm.v4.EntitySetV4;
import org.eclipse.ogee.client.model.edm.v4.EntityTypeV4;
import org.eclipse.ogee.client.model.edm.v4.EnumTypeMember;
import org.eclipse.ogee.client.model.edm.v4.EnumTypeV4;
import org.eclipse.ogee.client.model.edm.v4.FloatConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.Function;
import org.eclipse.ogee.client.model.edm.v4.FunctionImportV4;
import org.eclipse.ogee.client.model.edm.v4.GuidConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.IfExpression;
import org.eclipse.ogee.client.model.edm.v4.IntConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.LabeledElementExpression;
import org.eclipse.ogee.client.model.edm.v4.LabeledElementReferenceExpression;
import org.eclipse.ogee.client.model.edm.v4.NavigationPropertyBinding;
import org.eclipse.ogee.client.model.edm.v4.NavigationPropertyPathExpression;
import org.eclipse.ogee.client.model.edm.v4.NavigationPropertyV4;
import org.eclipse.ogee.client.model.edm.v4.NullExpression;
import org.eclipse.ogee.client.model.edm.v4.OnDelete;
import org.eclipse.ogee.client.model.edm.v4.OnDeleteAction;
import org.eclipse.ogee.client.model.edm.v4.PathExpression;
import org.eclipse.ogee.client.model.edm.v4.PropertyPathExpression;
import org.eclipse.ogee.client.model.edm.v4.PropertyRefV4;
import org.eclipse.ogee.client.model.edm.v4.PropertyV4;
import org.eclipse.ogee.client.model.edm.v4.PropertyValue;
import org.eclipse.ogee.client.model.edm.v4.RecordExpression;
import org.eclipse.ogee.client.model.edm.v4.ReferentialConstraintV4;
import org.eclipse.ogee.client.model.edm.v4.SchemaV4;
import org.eclipse.ogee.client.model.edm.v4.Singleton;
import org.eclipse.ogee.client.model.edm.v4.StringConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.Term;
import org.eclipse.ogee.client.model.edm.v4.TimeOfDayConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.TypeDefinition;
import org.eclipse.ogee.client.model.edmx.Association;
import org.eclipse.ogee.client.model.edmx.AssociationSet;
import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.Dependent;
import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.EdmxDataServices;
import org.eclipse.ogee.client.model.edmx.End;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.Key;
import org.eclipse.ogee.client.model.edmx.LongDescription;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.client.model.edmx.Parameter;
import org.eclipse.ogee.client.model.edmx.Principal;
import org.eclipse.ogee.client.model.edmx.Property;
import org.eclipse.ogee.client.model.edmx.PropertyRef;
import org.eclipse.ogee.client.model.edmx.ReferentialConstraint;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.model.edmx.Summary;
import org.eclipse.ogee.client.model.edmx.Using;
import org.eclipse.ogee.client.model.edmx.v3.AssociationSetV3;
import org.eclipse.ogee.client.model.edmx.v3.AssociationV3;
import org.eclipse.ogee.client.model.edmx.v3.TypeAnnotation;
import org.eclipse.ogee.client.model.edmx.v3.ValueAnnotation;
import org.eclipse.ogee.client.model.edmx.v4.EdmxInclude;
import org.eclipse.ogee.client.model.edmx.v4.EdmxIncludeAnnotations;
import org.eclipse.ogee.client.model.edmx.v4.EdmxReference;
import org.eclipse.ogee.client.model.edmx.v4.EdmxV4;
import org.eclipse.ogee.client.model.generic.Arrays;
import org.eclipse.ogee.client.model.generic.StringUtilities;
import org.eclipse.ogee.client.model.generic.v3.ODataConstantsV3;
import org.xml.sax.Attributes;

public class EdmxTagFactoryV4 implements TagFactory {
	private static final String ENUM_MEMBER = "EnumMember";
	private static final String NULL = "Null";
	private static final String LABELED_ELEMENT_REFERENCE = "LabeledElementReference";
	private static final String LABELED_ELEMENT = "LabeledElement";
	private static final String IS_OF = "IsOf";
	private static final String CAST = "Cast";
	private static final String IF = "If";
	private static final String APPLY = "Apply";
	private static final String NAVIGATION_PROPERTY_PATH = "NavigationPropertyPath";
	private static final String PROPERTY_PATH = "PropertyPath";
	private static final String TIME_OF_DAY = "TimeOfDay";
	private static final String DURATION = "Duration";
	private static final String DATE = "Date";
	private static final String ENTITY = "Entity";
	private static final String ACTION_IMPORT = "ActionImport";
	private static final String INCLUDE_IN_SERVICE_DOCUMENT = "IncludeInServiceDocument";
	private static final String EXTENDS = "Extends";
	private static final String IS_DEFAULT_ENTITY_CONTAINER = "IsDefaultEntityContainer";
	private static final String OPEN_TYPE = "OpenType";
	private static final String REFERENCED_PROPERTY = "ReferencedProperty";
	private static final String TARGET = "Target";
	private static final String NAVIGATION_PROPERTY_BINDING = "NavigationPropertyBinding";
	private static final String CONTAINS_TARGET = "ContainsTarget";
	private static final String PARTNER = "Partner";
	private static final String HAS_STREAM = "HasStream";
	private static final String APPLIES_TO = "AppliesTo";
	private static final String DEFAULT_VALUE = "DefaultValue";
	private static final String IS_COMPOSABLE = "IsComposable";
	private static final String FUNCTION = "Function";
	private static final String IS_FLAGS = "IsFlags";
	private static final String ENUM_TYPE = "EnumType";
	private static final String ON_DELETE = "OnDelete";
	private static final String PATH = "Path";
	private static final String TERM_ATTRIBUTE = "Term";
	private static final String ANNOTATION = "Annotation";
	private static final String ENTITY_SET_PATH = "EntitySetPath";
	private static final String IS_BOUND = "IsBound";
	private static final String ACTION = "Action";
	private static final String SRID = "SRID";
	private static final String SCALE = "Scale";
	private static final String PRECISION = "Precision";
	private static final String UNDERLYING_TYPE = "UnderlyingType";
	private static final String TYPE_DEFINITION = "TypeDefinition";
	private static final String XMLNS_ATTRIBUTE = "xmlns";
	private static final String QUALIFIER = "Qualifier";
	private static final String TERM_NAMESPACE = "TermNamespace";
	private static final String EDMX_INCLUDE_ANNOTATIONS = "IncludeAnnotations";
	private static final String EDMX_INCLUDE = "Include";
	private static final String URI_ATTRIBUTE = "Uri";

	private EdmxV4 edmx;

	public TagObject createTagObject(String tagName, Attributes attr) {
		TagObject result = new TagObject();
		result.setTag(tagName);

		int attrCount = attr.getLength();

		if (tagName.equalsIgnoreCase(ODataConstantsV3.EDMX_EDMX)) {
			edmx = new EdmxV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(VERSION))
						edmx.setVersion(attr.getValue(i));
					else if (attrName.startsWith(XMLNS))
						edmx.getNamespaces().put(attrName, attrValue);
					else
						edmx.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(edmx);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.EDMX_REFERENCE)) {
			EdmxReference reference = new EdmxReference();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(URI_ATTRIBUTE))
						reference.setUri(attr.getValue(i));
					else
						reference.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(reference);
		} else if (tagName.equalsIgnoreCase(EDMX_INCLUDE)) {
			EdmxInclude include = new EdmxInclude();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					include.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(NAMESPACE))
						include.setNamespace(attr.getValue(i));
					else if (attrName.equalsIgnoreCase(ALIAS))
						include.setAlias(attr.getValue(i));
				}
			}
			result.setObject(include);
		} else if (tagName.equalsIgnoreCase(EDMX_INCLUDE_ANNOTATIONS)) {
			EdmxIncludeAnnotations includeAnnotations = new EdmxIncludeAnnotations();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					includeAnnotations.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(TERM_NAMESPACE))
						includeAnnotations.setTermNamespace(attr.getValue(i));
					else if (attrName.equalsIgnoreCase(QUALIFIER))
						includeAnnotations.setQualifier(attr.getValue(i));
				}
			}
			result.setObject(includeAnnotations);
		} else if (tagName.equalsIgnoreCase(EDMX_DATASERVICES)) {
			EdmxDataServices edmxDataServices = new EdmxDataServices();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(M_DATA_SERVICE_VERSION))
						edmxDataServices.setmDataServiceVersion(attr
								.getValue(i));
					else if (attrName.startsWith(XMLNS))
						edmxDataServices.getNamespaces().put(attrName,
								attrValue);
					else
						edmxDataServices.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(edmxDataServices);
		} else if (tagName.equalsIgnoreCase(SCHEMA)) {
			SchemaV4 schema = new SchemaV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAMESPACE))
						schema.setNamespace(attrValue);
					else if (attrName.equalsIgnoreCase(ALIAS))
						schema.setAlias(attrValue);
					else if (attrName.startsWith(XMLNS_ATTRIBUTE))
						schema.getNamespaces().put(attrName, attrValue);
					else
						schema.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(schema);
		} else if (tagName.equalsIgnoreCase(USING)) {
			Using using = new Using();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAMESPACE))
						using.setNamespace(attrValue);
					else if (attrName.equalsIgnoreCase(ALIAS))
						using.setAlias(attrValue);
					else
						using.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(using);
		} else if (tagName.equalsIgnoreCase(TYPE_DEFINITION)) {
			TypeDefinition typeDefinition = new TypeDefinition();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					typeDefinition.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(NAME))
						typeDefinition.setName(attrValue);
					else if (attrName.equalsIgnoreCase(UNDERLYING_TYPE))
						typeDefinition.setUnderlyingType(attrValue);
					else if (attrName.equalsIgnoreCase(MAX_LENGTH))
						typeDefinition.setMaxLength(attrValue);
					else if (attrName.equalsIgnoreCase(PRECISION))
						typeDefinition.setPrecision(attrValue);
					else if (attrName.equalsIgnoreCase(SCALE))
						typeDefinition.setScale(attrValue);
					else if (attrName.equalsIgnoreCase(SRID))
						typeDefinition.setSRID(attrValue);
					else if (attrName.equalsIgnoreCase(UNICODE))
						typeDefinition.setUnicode(StringUtilities
								.stringToBoolean(attrValue));
				}
			}
			result.setObject(typeDefinition);
		} else if (tagName.equalsIgnoreCase(ACTION)) {
			Action action = new Action();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					action.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(NAME))
						action.setName(attrValue);
					else if (attrName.equalsIgnoreCase(IS_BOUND))
						action.setIsBound(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(ENTITY_SET_PATH))
						action.setEntitySetPath(attrValue);
					else if (attrName.equalsIgnoreCase(RETURN_TYPE))
						action.setReturnTypeAttribute(attrValue);
				}
			}
			result.setObject(action);
		} else if (tagName.equalsIgnoreCase(ANNOTATION)) {
			Annotation annotation = new Annotation();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					annotation.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(TERM_ATTRIBUTE))
						annotation.setTerm(attrValue);
					else if (attrName.equalsIgnoreCase(PATH)) {
						PathExpression pathExpression = new PathExpression();
						pathExpression.setValue(attrValue);
						annotation.setPath(pathExpression);
					}
				}
			}
			result.setObject(annotation);
		} else if (tagName.equalsIgnoreCase(ON_DELETE)) {
			OnDelete onDelete = new OnDelete();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					onDelete.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(ACTION))
						onDelete.setAction(OnDeleteAction.fromValue(attrValue));
				}
			}
			result.setObject(onDelete);
		} else if (tagName.equalsIgnoreCase(ENUM_TYPE)) {
			EnumTypeV4 enumType = new EnumTypeV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					enumType.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(NAME))
						enumType.setName(attrValue);
					else if (attrName.equalsIgnoreCase(UNDERLYING_TYPE))
						enumType.setUnderlyingType(attrValue);
					else if (attrName.equalsIgnoreCase(IS_FLAGS))
						enumType.setIsFlags(StringUtilities
								.stringToBoolean(attrValue));
				}
			}
			result.setObject(enumType);
		} else if (tagName.equalsIgnoreCase(FUNCTION)) {
			Function function = new Function();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					function.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(NAME))
						function.setName(attrValue);
					else if (attrName.equalsIgnoreCase(RETURN_TYPE))
						function.setReturnTypeAttribute(attrValue);
					else if (attrName.equalsIgnoreCase(ENTITY_SET_PATH))
						function.setEntitySetPath(attrValue);
					else if (attrName.equalsIgnoreCase(IS_BOUND))
						function.setIsBound(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(IS_COMPOSABLE))
						function.setIsComposable(StringUtilities
								.stringToBoolean(attrValue));
				}
			}
			result.setObject(function);
		} else if (tagName.equalsIgnoreCase(TERM_ATTRIBUTE)) {
			Term term = new Term();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					term.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(NAME))
						term.setName(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						term.setType(attrValue);
					else if (attrName.equalsIgnoreCase(NULLABLE))
						term.setNullable(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(DEFAULT_VALUE))
						term.setDefaultValue(attrValue);
					else if (attrName.equalsIgnoreCase(APPLIES_TO))
						term.setAppliesTo(attrValue);
					else if (attrName.equalsIgnoreCase(MAX_LENGTH))
						term.setMaxLength(attrValue);
					else if (attrName.equalsIgnoreCase(PRECISION))
						term.setPrecision(attrValue);
					else if (attrName.equalsIgnoreCase(SCALE))
						term.setScale(attrValue);
					else if (attrName.equalsIgnoreCase(SRID))
						term.setSRID(attrValue);
				}
			}
			result.setObject(term);
		} else if (tagName.equalsIgnoreCase(ENTITY_TYPE)) {
			EntityTypeV4 entityType = new EntityTypeV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						entityType.setName(attrValue);
					else if (attrName.equalsIgnoreCase(BASE_TYPE))
						entityType.setBaseType(attrValue);
					else if (attrName.equalsIgnoreCase(HAS_STREAM))
						entityType.setMHasStream(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(ABSTRACT))
						entityType.setAbstract(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(OPEN_TYPE))
						entityType.setOpenType(StringUtilities
								.stringToBoolean(attrValue));
					else
						entityType.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(entityType);
		} else if (tagName.equalsIgnoreCase(KEY)) {
			Key key = new Key();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					key.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(key);
		} else if (tagName.equalsIgnoreCase(PROPERTY_REF)) {
			PropertyRefV4 propertyRef = new PropertyRefV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						propertyRef.setName(attrValue);
					else if (attrName.equalsIgnoreCase(ALIAS))
						propertyRef.setAlias(attrValue);
					else
						propertyRef.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(propertyRef);
		} else if (tagName.equalsIgnoreCase(PROPERTY)) {
			PropertyV4 property = new PropertyV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);

					if (attrName.equalsIgnoreCase(NAME))
						property.setName(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						property.setType(attrValue);
					else if (attrName.equalsIgnoreCase(NULLABLE))
						property.setNullable(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(MAX_LENGTH))
						property.setMaxLength(attrValue);
					else if (attrName.equalsIgnoreCase(FIXED_LENGTH))
						property.setFixedLength(attrValue);
					else if (attrName.equalsIgnoreCase(UNICODE))
						property.setUnicode(attrValue);
					else if (attrName.equalsIgnoreCase(M_FC_TARGET_PATH))
						property.setmFC_TargetPath(attrValue);
					else if (attrName.equalsIgnoreCase(M_FC_KEEP_IN_CONTENT))
						property.setmFC_KeepInContent(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(DEFAULT_VALUE))
						property.setDefaultValue(attrValue);
					else if (attrName.equalsIgnoreCase(PRECISION))
						property.setPrecision(attrValue);
					else if (attrName.equalsIgnoreCase(SCALE))
						property.setScale(attrValue);
					else if (attrName.equalsIgnoreCase(SRID))
						property.setSRID(attrValue);
					else
						property.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(property);
		} else if (tagName.equalsIgnoreCase(NAVIGATION_PROPERTY)) {
			NavigationPropertyV4 navigationProperty = new NavigationPropertyV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						navigationProperty.setName(attrValue);
					else if (attrName.equalsIgnoreCase(RELATIONSHIP))
						navigationProperty.setRelationship(attrValue);
					else if (attrName.equalsIgnoreCase(FROM_ROLE))
						navigationProperty.setFromRole(attrValue);
					else if (attrName.equalsIgnoreCase(TO_ROLE))
						navigationProperty.setToRole(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						navigationProperty.setType(attrValue);
					else if (attrName.equalsIgnoreCase(NULLABLE))
						navigationProperty.setNullable(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(PARTNER))
						navigationProperty.setPartner(attrValue);
					else if (attrName.equalsIgnoreCase(CONTAINS_TARGET))
						navigationProperty.setContainsTarget(StringUtilities
								.stringToBoolean(attrValue));
					else
						navigationProperty.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(navigationProperty);
		} else if (tagName.equalsIgnoreCase(NAVIGATION_PROPERTY_BINDING)) {
			NavigationPropertyBinding navigationPropertyBinding = new NavigationPropertyBinding();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					navigationPropertyBinding.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(PATH))
						navigationPropertyBinding.setPath(attrValue);
					else if (attrName.equalsIgnoreCase(TARGET))
						navigationPropertyBinding.setTarget(attrValue);
					else if (attrName.equalsIgnoreCase(ENTITY_SET))
						navigationPropertyBinding.setEntitySet(attrValue);
				}
			}
			result.setObject(navigationPropertyBinding);
		} else if (tagName.equalsIgnoreCase(RETURN_TYPE)) {
			ActionFunctionReturnType actionFunctionReturnType = new ActionFunctionReturnType();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					actionFunctionReturnType.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(MAX_LENGTH))
						actionFunctionReturnType.setMaxLength(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						actionFunctionReturnType.setType(attrValue);
					else if (attrName.equalsIgnoreCase(NULLABLE))
						actionFunctionReturnType.setNullable(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(ENTITY_SET_PATH))
						actionFunctionReturnType.setEntitySetPath(attrValue);
					else if (attrName.equalsIgnoreCase(PRECISION))
						actionFunctionReturnType.setPrecision(attrValue);
					else if (attrName.equalsIgnoreCase(SCALE))
						actionFunctionReturnType.setScale(attrValue);
					else if (attrName.equalsIgnoreCase(SRID))
						actionFunctionReturnType.setSRID(attrValue);
				}
			}
			result.setObject(actionFunctionReturnType);
		} else if (tagName.equalsIgnoreCase(ASSOCIATION)) {
			AssociationV3 association = new AssociationV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						association.setName(attrValue);
					else
						association.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(association);
		} else if (tagName.equalsIgnoreCase(END)) {
			End end = new End();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(TYPE))
						end.setType(attrValue);
					else if (attrName.equalsIgnoreCase(MULTIPLICITY))
						end.setMultiplicity(attrValue);
					else if (attrName.equalsIgnoreCase(ROLE))
						end.setRole(attrValue);
					else if (attrName.equalsIgnoreCase(ENTITY_SET))
						end.setEntitySet(attrValue);
					else
						end.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(end);
		} else if (tagName.equalsIgnoreCase(REFERENTIAL_CONSTRAINT)) {
			ReferentialConstraintV4 constraint = new ReferentialConstraintV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					constraint.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(PROPERTY))
						constraint.setProperty(attrValue);
					else if (attrName.equalsIgnoreCase(REFERENCED_PROPERTY))
						constraint.setReferencedProperty(attrValue);
				}
			}
			result.setObject(constraint);
		} else if (tagName.equalsIgnoreCase(PRINCIPAL)) {
			Principal principal = new Principal();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(ROLE))
						principal.setRole(attrValue);
					else
						principal.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(principal);
		} else if (tagName.equalsIgnoreCase(DEPENDENT)) {
			Dependent dependent = new Dependent();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(ROLE))
						dependent.setRole(attrValue);
					else
						dependent.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(dependent);
		} else if (tagName.equalsIgnoreCase(COMPLEX_TYPE)) {
			ComplexTypeV4 complexType = new ComplexTypeV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						complexType.setName(attrValue);
					else if (attrName.equalsIgnoreCase(BASE_TYPE))
						complexType.setBaseType(attrValue);
					else if (attrName.equalsIgnoreCase(ABSTRACT))
						complexType.setAbstract(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(OPEN_TYPE))
						complexType.setOpenType(StringUtilities
								.stringToBoolean(attrValue));
					else
						complexType.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(complexType);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.ENUM_TYPE)) {
			EnumTypeV4 enumType = new EnumTypeV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					enumType.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(NAME))
						enumType.setName(attrValue);
					else if (attrName
							.equalsIgnoreCase(ODataConstantsV3.UNDERLYING_TYPE))
						enumType.setUnderlyingType(attrValue);
					else if (attrName.equalsIgnoreCase(IS_FLAGS))
						enumType.setIsFlags(StringUtilities
								.stringToBoolean(attrValue));
				}
			}
			result.setObject(enumType);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.MEMBER)) {
			EnumTypeMember member = new EnumTypeMember();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					member.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(NAME))
						member.setName(attr.getValue(i));
					else if (attrName.equalsIgnoreCase(ODataConstantsV3.VALUE))

						try {
							member.setValue(Long.parseLong(attrValue));
						} catch (NumberFormatException e) {
							// do nothing
						}

				}
			}
			result.setObject(member);
		} else if (tagName.equalsIgnoreCase(ENTITY_CONTAINER)) {
			EntityContainerV4 container = new EntityContainerV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						container.setName(attrValue);
					else if (attrName
							.equalsIgnoreCase(M_IS_DEFAULT_ENTITY_CONTAINER)
							|| attrName
									.equalsIgnoreCase(IS_DEFAULT_ENTITY_CONTAINER))
						container.setDefault(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(EXTENDS))
						container.setExtends(attrValue);
					else
						container.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(container);
		} else if (tagName.equalsIgnoreCase(ENTITY_SET)) {
			EntitySetV4 entitySet = new EntitySetV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						entitySet.setName(attrValue);
					else if (attrName.equalsIgnoreCase(ENTITY_TYPE))
						entitySet.setEntityType(attrValue);
					else if (attrName
							.equalsIgnoreCase(INCLUDE_IN_SERVICE_DOCUMENT))
						entitySet.setIncludeInServiceDocument(StringUtilities
								.stringToBoolean(attrValue));
					else
						entitySet.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(entitySet);
		} else if (tagName.equalsIgnoreCase(ASSOCIATION_SET)) {
			AssociationSetV3 associationSet = new AssociationSetV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						associationSet.setName(attrValue);
					else if (attrName.equalsIgnoreCase(ASSOCIATION))
						associationSet.setAssociation(attrValue);
					else
						associationSet.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(associationSet);
		} else if (tagName.equalsIgnoreCase(FUNCTION_IMPORT)) {
			FunctionImportV4 functionImport = new FunctionImportV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						functionImport.setName(attrValue);
					else if (attrName.equalsIgnoreCase(RETURN_TYPE))
						functionImport.setReturnType(attrValue);
					else if (attrName.equalsIgnoreCase(M_HTTP_METHOD))
						functionImport.setmHttpMethod(attrValue);
					else if (attrName.equalsIgnoreCase(ENTITY_SET))
						functionImport.setEntitySet(attrValue);
					else if (attrName.equalsIgnoreCase(FUNCTION))
						functionImport.setFunction(attrValue);
					else if (attrName
							.equalsIgnoreCase(INCLUDE_IN_SERVICE_DOCUMENT))
						functionImport
								.setIncludeInServiceDocument(StringUtilities
										.stringToBoolean(attrValue));
					else
						functionImport.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(functionImport);
		} else if (tagName.equalsIgnoreCase(PARAMETER)) {
			ActionFunctionParameter parameter = new ActionFunctionParameter();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						parameter.setName(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						parameter.setType(attrValue);
					else if (attrName.equalsIgnoreCase(MODE))
						parameter.setMode(attrValue);
					else if (attrName.equalsIgnoreCase(MAX_LENGTH))
						parameter.setMaxLength(attrValue);
					else if (attrName.equalsIgnoreCase(NULLABLE))
						parameter.setNullable(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(PRECISION))
						parameter.setPrecision(attrValue);
					else if (attrName.equalsIgnoreCase(SCALE))
						parameter.setScale(attrValue);
					else if (attrName.equalsIgnoreCase(SRID))
						parameter.setSRID(attrValue);
					else if (attrName.equalsIgnoreCase(DEFAULT_VALUE))
						parameter.setDefaultValue(attrValue);
					else
						parameter.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(parameter);
		} else if (tagName.equalsIgnoreCase(DOCUMENTATION)) {
			Documentation documentation = new Documentation();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					documentation.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(documentation);
		} else if (tagName.equalsIgnoreCase(LONG_DESCRIPTION)) {
			LongDescription longDescription = new LongDescription();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					longDescription.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(longDescription);
		} else if (tagName.equalsIgnoreCase(DOCUMENTATION_SUMMARY)) {
			Summary summary = new Summary();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					summary.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(summary);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.ANNOTATIONS)) {
			AnnotationsV4 annotations = new AnnotationsV4();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					annotations.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(ODataConstantsV3.TARGET))
						annotations.setTarget(attrValue);
					if (attrName.equalsIgnoreCase(QUALIFIER))
						annotations.setTarget(attrValue);
				}
			}
			result.setObject(annotations);
		} else if (tagName.equalsIgnoreCase(ACTION_IMPORT)) {
			ActionImport actionImport = new ActionImport();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					actionImport.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(ACTION))
						actionImport.setAction(attrValue);
					else if (attrName.equalsIgnoreCase(NAME))
						actionImport.setName(attrValue);
					else if (attrName.equalsIgnoreCase(ENTITY_SET))
						actionImport.setEntitySet(attrValue);
					else if (attrName
							.equalsIgnoreCase(INCLUDE_IN_SERVICE_DOCUMENT))
						actionImport
								.setIncludeInServiceDocument(StringUtilities
										.stringToBoolean(attrValue));
				}
			}
			result.setObject(actionImport);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.PROPERTY_VALUE)) {
			PropertyValue propertyValue = new PropertyValue();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					propertyValue.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(ODataConstantsV3.PROPERTY))
						propertyValue.setProperty(attrValue);
				}
			}
			result.setObject(propertyValue);
		} else if (tagName.equalsIgnoreCase(ENTITY)) {
			Singleton singleton = new Singleton();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					singleton.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(TYPE))
						singleton.setType(attrValue);
					else if (attrName.equalsIgnoreCase(NAME))
						singleton.setName(attrValue);
				}
			}
			result.setObject(singleton);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
			RecordExpression record = new RecordExpression();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					record.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(TYPE))
						record.setType(attrValue);
				}
			}
			result.setObject(record);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.COLLECTION_EXPRESSION)) {
			CollectionExpression collection = new CollectionExpression();

			result.setObject(collection);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
			PathExpression path = new PathExpression();

			result.setObject(path);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
			StringConstantExpression stringConstantExpression = new StringConstantExpression();

			result.setObject(stringConstantExpression);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
			IntConstantExpression intConstantExpression = new IntConstantExpression();

			result.setObject(intConstantExpression);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
			FloatConstantExpression Float = new FloatConstantExpression();

			result.setObject(Float);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
			DecimalConstantExpression decimalConstantExpression = new DecimalConstantExpression();

			result.setObject(decimalConstantExpression);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
			BoolConstantExpression boolConstantExpression = new BoolConstantExpression();

			result.setObject(boolConstantExpression);
		} else if (tagName.equalsIgnoreCase(DATE)) {
			DateConstantExpression dateConstantExpression = new DateConstantExpression();

			result.setObject(dateConstantExpression);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
			DateTimeOffsetConstantExpression dateTimeOffsetConstantExpression = new DateTimeOffsetConstantExpression();

			result.setObject(dateTimeOffsetConstantExpression);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
			GuidConstantExpression guidConstantExpression = new GuidConstantExpression();

			result.setObject(guidConstantExpression);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
			BinaryConstantExpression binaryConstantExpression = new BinaryConstantExpression();

			result.setObject(binaryConstantExpression);
		} else if (tagName.equalsIgnoreCase(DURATION)) {
			DurationConstantExpression durationConstantExpression = new DurationConstantExpression();

			result.setObject(durationConstantExpression);
		} else if (tagName.equalsIgnoreCase(TIME_OF_DAY)) {
			TimeOfDayConstantExpression timeOfDayConstantExpression = new TimeOfDayConstantExpression();

			result.setObject(timeOfDayConstantExpression);
		} else if (tagName.equalsIgnoreCase(PROPERTY_PATH)) {
			PropertyPathExpression propertyPathExpression = new PropertyPathExpression();

			result.setObject(propertyPathExpression);
		} else if (tagName.equalsIgnoreCase(NAVIGATION_PROPERTY_PATH)) {
			NavigationPropertyPathExpression navigationPropertyPathExpression = new NavigationPropertyPathExpression();

			result.setObject(navigationPropertyPathExpression);
		} else if (tagName.equalsIgnoreCase(APPLY)) {
			ApplyExpression applyExpression = new ApplyExpression();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					applyExpression.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(FUNCTION))
						applyExpression.setFunction(attrValue);
				}
			}
			result.setObject(applyExpression);
		} else if (tagName.equalsIgnoreCase(IF)) {
			IfExpression ifExpression = new IfExpression();

			result.setObject(ifExpression);
		} else if (tagName.equalsIgnoreCase(CAST)
				|| tagName.equalsIgnoreCase(IS_OF)) {
			CastOrIsOfExpression castOrIsOfExpression = new CastOrIsOfExpression();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					castOrIsOfExpression.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase(TYPE))
						castOrIsOfExpression.setType(attrValue);
					else if (attrName.equalsIgnoreCase(MAX_LENGTH))
						castOrIsOfExpression.setMaxLength(attrValue);
					else if (attrName.equalsIgnoreCase(PRECISION))
						castOrIsOfExpression.setPrecision(attrValue);
					else if (attrName.equalsIgnoreCase(SCALE))
						castOrIsOfExpression.setScale(attrValue);
					else if (attrName.equalsIgnoreCase(SRID))
						castOrIsOfExpression.setSRID(attrValue);
					else if (attrName.equalsIgnoreCase(SCALE))
						castOrIsOfExpression.setScale(attrValue);
				}
				result.setObject(castOrIsOfExpression);
			}
		} else if (tagName.equalsIgnoreCase(LABELED_ELEMENT)) {
			LabeledElementExpression labeledElementExpression = new LabeledElementExpression();

			result.setObject(labeledElementExpression);
		} else if (tagName.equalsIgnoreCase(LABELED_ELEMENT_REFERENCE)) {
			LabeledElementReferenceExpression labeledElementReferenceExpression = new LabeledElementReferenceExpression();

			result.setObject(labeledElementReferenceExpression);
		} else if (tagName.equalsIgnoreCase(NULL)) {
			NullExpression nullExpression = new NullExpression();

			result.setObject(nullExpression);
		} else {
			if (edmx != null) {
				edmx.addNotSupported(tagName);
			}
		}

		return result;
	}

	public void setValue(TagObject tagObject, String value) {
		String tagName = tagObject.getTag();
		if (tagName.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
			PathExpression path = (PathExpression) tagObject.getObject();
			path.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
			StringConstantExpression stringConstantExpression = (StringConstantExpression) tagObject
					.getObject();
			stringConstantExpression.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
			IntConstantExpression Int = (IntConstantExpression) tagObject
					.getObject();
			Int.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
			FloatConstantExpression Float = (FloatConstantExpression) tagObject
					.getObject();
			Float.setValue(value);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
			DecimalConstantExpression Decimal = (DecimalConstantExpression) tagObject
					.getObject();
			Decimal.setValue(value);
		} else if (tagName.equalsIgnoreCase(DATE)) {
			DateConstantExpression DateTime = (DateConstantExpression) tagObject
					.getObject();
			DateTime.setValue(value);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
			DateTimeOffsetConstantExpression DateTimeOffset = (DateTimeOffsetConstantExpression) tagObject
					.getObject();
			DateTimeOffset.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
			GuidConstantExpression Guid = (GuidConstantExpression) tagObject
					.getObject();
			Guid.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
			BinaryConstantExpression Binary = (BinaryConstantExpression) tagObject
					.getObject();
			Binary.setValue(value.getBytes());
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
			BoolConstantExpression bool = (BoolConstantExpression) tagObject
					.getObject();
			bool.setValue(Boolean.valueOf(value));
		} else if (tagName.equalsIgnoreCase(DURATION)) {
			DurationConstantExpression durationConstantExpression = (DurationConstantExpression) tagObject
					.getObject();
			durationConstantExpression.setValue(value);
		} else if (tagName.equalsIgnoreCase(TIME_OF_DAY)) {
			TimeOfDayConstantExpression timeOfDayConstantExpression = (TimeOfDayConstantExpression) tagObject
					.getObject();
			timeOfDayConstantExpression.setValue(value);
		} else if (tagName.equalsIgnoreCase(PROPERTY_PATH)) {
			PropertyPathExpression propertyPathExpression = (PropertyPathExpression) tagObject
					.getObject();
			propertyPathExpression.setValue(value);
		} else if (tagName.equalsIgnoreCase(NAVIGATION_PROPERTY_PATH)) {
			NavigationPropertyPathExpression propertyPathExpression = (NavigationPropertyPathExpression) tagObject
					.getObject();
			propertyPathExpression.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
			BoolConstantExpression bool = (BoolConstantExpression) tagObject
					.getObject();
			bool.setValue(Boolean.valueOf(value));
		}

	}

	public void addChild(TagObject parent, TagObject tagObject) {
		String tag = tagObject.getTag();
		String parentTag = parent.getTag();

		if (parentTag.equalsIgnoreCase(EDMX_EDMX)) {
			EdmxV4 edmx = (EdmxV4) parent.getObject();

			if (tag.equalsIgnoreCase(EDMX_DATASERVICES)) {
				EdmxDataServices edmxDataServices = (EdmxDataServices) tagObject
						.getObject();
				edmx.setEdmxDataServices(edmxDataServices);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.EDMX_REFERENCE)) {
				EdmxReference reference = (EdmxReference) tagObject.getObject();
				EdmxReference[] references = edmx.getEdmxReferences();
				Arrays.add(edmx, references, reference);
			}
		} else if (parentTag.equalsIgnoreCase(ODataConstantsV3.EDMX_REFERENCE)) {
			EdmxReference edmxReference = (EdmxReference) parent.getObject();
			Object o = tagObject.getObject();

			if (tag.equalsIgnoreCase(EDMX_INCLUDE)) {
				EdmxInclude[] edmxIncludes = edmxReference.getEdmxIncludes();
				EdmxInclude edmxInclude = (EdmxInclude) o;
				Arrays.add(edmxReference, edmxIncludes, edmxInclude);
			} else if (tag.equalsIgnoreCase(EDMX_INCLUDE_ANNOTATIONS)) {
				EdmxIncludeAnnotations[] edmxIncludeAnnotations = edmxReference
						.getEdmxIncludeAnnotations();
				EdmxIncludeAnnotations includeAnnotations = (EdmxIncludeAnnotations) o;
				Arrays.add(edmxReference, edmxIncludeAnnotations,
						includeAnnotations);
			}
		} else if (parentTag.equalsIgnoreCase(EDMX_DATASERVICES)) {
			EdmxDataServices dataServices = (EdmxDataServices) parent
					.getObject();
			Object o = tagObject.getObject();

			if (tag.equalsIgnoreCase(SCHEMA)) {
				Schema[] schemas = dataServices.getSchemas();
				SchemaV4 schema = (SchemaV4) o;
				Arrays.add(dataServices, schemas, schema);
			}
		} else if (parentTag.equalsIgnoreCase(SCHEMA)) {
			SchemaV4 schema = (SchemaV4) parent.getObject();

			if (tag.equalsIgnoreCase(ENTITY_TYPE)) {
				EntityType[] entityTypes = schema.getEntityTypes();
				EntityTypeV4 entityType = (EntityTypeV4) tagObject.getObject();
				Arrays.add(schema, entityTypes, entityType);
			} else if (tag.equalsIgnoreCase(USING)) {
				Using[] usings = schema.getUsings();
				Using using = (Using) tagObject.getObject();
				Arrays.add(schema, usings, using);
			} else if (tag.equalsIgnoreCase(COMPLEX_TYPE)) {
				ComplexType[] complexTypes = schema.getComplexTypes();
				ComplexTypeV4 complexType = (ComplexTypeV4) tagObject
						.getObject();
				Arrays.add(schema, complexTypes, complexType);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.ENUM_TYPE)) {
				List<EnumTypeV4> enumTypes = schema.getEnumTypes();
				EnumTypeV4 enumType = (EnumTypeV4) tagObject.getObject();
				enumTypes.add(enumType);
			} else if (tag.equalsIgnoreCase(ASSOCIATION)) {
				Association[] associations = schema.getAssociations();
				AssociationV3 association = (AssociationV3) tagObject
						.getObject();
				Arrays.add(schema, associations, association);
			} else if (tag.equalsIgnoreCase(ENTITY_CONTAINER)) {
				EntityContainer[] entityContainers = schema
						.getEntityContainers();
				EntityContainerV4 entityContainer = (EntityContainerV4) tagObject
						.getObject();
				Arrays.add(schema, entityContainers, entityContainer);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.ANNOTATIONS)) {
				List<AnnotationsV4> annotationsArray = schema.getAnnotations();
				AnnotationsV4 annotations = (AnnotationsV4) tagObject
						.getObject();
				annotationsArray.add(annotations);
			} else if (tag.equalsIgnoreCase(FUNCTION)) {
				List<Function> functions = schema.getFunctions();
				Function function = (Function) tagObject.getObject();
				functions.add(function);
			} else if (tag.equalsIgnoreCase(TYPE_DEFINITION)) {
				List<TypeDefinition> typeDefinitions = schema
						.getTypeDefinitions();
				TypeDefinition typeDefinition = (TypeDefinition) tagObject
						.getObject();
				typeDefinitions.add(typeDefinition);
			} else if (tag.equalsIgnoreCase(ACTION)) {

				Action action = (Action) tagObject.getObject();
				schema.getActions().add(action);
			}
		} else if (parentTag.equalsIgnoreCase(FUNCTION)) {
			Function function = (Function) parent.getObject();
			if (tag.equalsIgnoreCase(PARAMETER)) {
				ActionFunctionParameter functionParameter = (ActionFunctionParameter) tagObject
						.getObject();
				function.getParameters().add(functionParameter);
			} else if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				function.getAnnotation().add(annotation);
			} else if (tag.equalsIgnoreCase(RETURN_TYPE)) {
				ActionFunctionReturnType functionReturnType = (ActionFunctionReturnType) tagObject
						.getObject();
				function.setReturnType(functionReturnType);
			}
		} else if (parentTag.equalsIgnoreCase(ACTION)) {
			Action action = (Action) parent.getObject();
			if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				action.getAnnotation().add(annotation);
			} else if (tag.equalsIgnoreCase(RETURN_TYPE)) {
				ActionFunctionReturnType functionReturnType = (ActionFunctionReturnType) tagObject
						.getObject();
				action.setReturnType(functionReturnType);
			}
		} else if (parentTag.equalsIgnoreCase(ENTITY_TYPE)) {
			EntityTypeV4 entityType = (EntityTypeV4) parent.getObject();
			if (tag.equalsIgnoreCase(KEY)) {
				Key key = (Key) tagObject.getObject();
				entityType.setKey(key);
			} else if (tag.equalsIgnoreCase(PROPERTY)) {
				Property[] properties = entityType.getProperties();
				PropertyV4 property = (PropertyV4) tagObject.getObject();
				Arrays.add(entityType, properties, property);
			} else if (tag.equalsIgnoreCase(NAVIGATION_PROPERTY)) {
				NavigationProperty[] navigationProperties = entityType
						.getNavigationProperties();
				NavigationPropertyV4 navigationProperty = (NavigationPropertyV4) tagObject
						.getObject();
				Arrays.add(entityType, navigationProperties, navigationProperty);
			} else if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				entityType.getAnnotation().add(annotation);
			}
		} else if (parentTag.equalsIgnoreCase(COMPLEX_TYPE)) {
			ComplexTypeV4 complexType = (ComplexTypeV4) parent.getObject();
			if (tag.equalsIgnoreCase(PROPERTY)) {
				Property[] properties = complexType.getProperties();
				PropertyV4 property = (PropertyV4) tagObject.getObject();
				Arrays.add(complexType, properties, property);
			}
		} else if (parentTag.equalsIgnoreCase(ASSOCIATION)) {
			AssociationV3 association = (AssociationV3) parent.getObject();
			if (tag.equalsIgnoreCase(END)) {
				End[] ends = association.getEnds();
				End end = (End) tagObject.getObject();
				Arrays.add(association, ends, end);
			} else if (tag.equalsIgnoreCase(REFERENTIAL_CONSTRAINT)) {
				ReferentialConstraint constraint = (ReferentialConstraint) tagObject
						.getObject();
				association.setReferentialConstraint(constraint);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				association.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = association
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(association, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = association
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(association, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(ENTITY_CONTAINER)) {
			EntityContainerV4 container = (EntityContainerV4) parent
					.getObject();
			if (tag.equalsIgnoreCase(ENTITY_SET)) {
				EntitySetV4 entitySet = (EntitySetV4) tagObject.getObject();
				EntitySet[] entitySets = container.getEntitySets();
				Arrays.add(container, entitySets, entitySet);
			} else if (tag.equalsIgnoreCase(FUNCTION_IMPORT)) {
				FunctionImport functionImport = (FunctionImport) tagObject
						.getObject();
				FunctionImport[] functionImports = container
						.getFunctionImports();
				Arrays.add(container, functionImports, functionImport);
			} else if (tag.equalsIgnoreCase(ENTITY)) {
				Singleton singleton = (Singleton) tagObject.getObject();
				container.getEntities().add(singleton);
			} else if (tag.equalsIgnoreCase(ASSOCIATION_SET)) {
				AssociationSet associationSet = (AssociationSet) tagObject
						.getObject();
				AssociationSet[] associationSets = container
						.getAssociationSets();
				Arrays.add(container, associationSets, associationSet);
			} else if (tag.equalsIgnoreCase(ACTION_IMPORT)) {
				ActionImport actionImport = (ActionImport) tagObject
						.getObject();
				container.getActionImports().add(actionImport);
			}
		} else if (parentTag.equalsIgnoreCase(REFERENTIAL_CONSTRAINT)) {
			ReferentialConstraint constraint = (ReferentialConstraint) parent
					.getObject();
			if (tag.equalsIgnoreCase(PRINCIPAL)) {
				Principal principal = (Principal) tagObject.getObject();
				constraint.setPrincipal(principal);
			} else if (tag.equalsIgnoreCase(DEPENDENT)) {
				Dependent dependent = (Dependent) tagObject.getObject();
				constraint.setDependent(dependent);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				constraint.setDocumentation(documentation);
			}
		} else if (parentTag.equalsIgnoreCase(ACTION_IMPORT)) {
			ActionImport actionImport = (ActionImport) parent.getObject();
			if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				actionImport.getAnnotation().add(annotation);
			}
		} else if (parentTag.equalsIgnoreCase(KEY)) {
			Key key = (Key) parent.getObject();
			if (tag.equalsIgnoreCase(PROPERTY_REF)) {
				PropertyRef propertyRef = (PropertyRef) tagObject.getObject();
				PropertyRef[] propertyRefs = key.getPropertyRefs();
				Arrays.add(key, propertyRefs, propertyRef);
			}
		} else if (parentTag.equalsIgnoreCase(PRINCIPAL)) {
			Principal principal = (Principal) parent.getObject();
			if (tag.equalsIgnoreCase(PROPERTY_REF)) {
				PropertyRef propertyRef = (PropertyRef) tagObject.getObject();
				PropertyRef[] propertyRefs = principal.getPropertyRefs();
				Arrays.add(principal, propertyRefs, propertyRef);
			}
		} else if (parentTag.equalsIgnoreCase(DEPENDENT)) {
			Dependent dependent = (Dependent) parent.getObject();
			if (tag.equalsIgnoreCase(PROPERTY_REF)) {
				PropertyRef propertyRef = (PropertyRef) tagObject.getObject();
				PropertyRef[] propertyRefs = dependent.getPropertyRefs();
				Arrays.add(dependent, propertyRefs, propertyRef);
			}
		} else if (parentTag.equalsIgnoreCase(ASSOCIATION_SET)) {
			AssociationSetV3 associationSet = (AssociationSetV3) parent
					.getObject();
			if (tag.equalsIgnoreCase(END)) {
				End end = (End) tagObject.getObject();
				End[] ends = associationSet.getEnds();
				Arrays.add(associationSet, ends, end);
			}
		} else if (parentTag.equalsIgnoreCase(ENTITY_SET)) {
			EntitySetV4 entitySet = (EntitySetV4) parent.getObject();
			if (tag.equalsIgnoreCase(NAVIGATION_PROPERTY_BINDING)) {
				NavigationPropertyBinding navigationPropertyBinding = (NavigationPropertyBinding) tagObject
						.getObject();
				entitySet.getNavigationPropertyBindings().add(
						navigationPropertyBinding);
			} else if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				entitySet.getAnnotation().add(annotation);
			}
		} else if (parentTag.equalsIgnoreCase(FUNCTION_IMPORT)) {
			FunctionImportV4 functionImport = (FunctionImportV4) parent
					.getObject();
			if (tag.equalsIgnoreCase(PARAMETER)) {
				Parameter parameter = (Parameter) tagObject.getObject();
				Parameter[] parameters = functionImport.getParameters();
				Arrays.add(functionImport, parameters, parameter);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				functionImport.setDocumentation(documentation);
			}
		} else if (parentTag.equalsIgnoreCase(PARAMETER)) {
			ActionFunctionParameter parameter = (ActionFunctionParameter) parent
					.getObject();
			if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				parameter.setDocumentation(documentation);
			}
		} else if (parentTag.equalsIgnoreCase(PROPERTY)) {
			PropertyV4 property = (PropertyV4) parent.getObject();
			if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				property.getAnnotation().add(annotation);
			}
		} else if (parentTag.equalsIgnoreCase(NAVIGATION_PROPERTY)) {
			NavigationPropertyV4 navigationProperty = (NavigationPropertyV4) parent
					.getObject();
			if (tag.equalsIgnoreCase(REFERENTIAL_CONSTRAINT)) {
				ReferentialConstraintV4 referentialConstraint = (ReferentialConstraintV4) tagObject
						.getObject();
				navigationProperty.getReferentialConstraint().add(
						referentialConstraint);
			} else if (tag.equalsIgnoreCase(ON_DELETE)) {
				OnDelete onDelete = (OnDelete) tagObject.getObject();
				navigationProperty.setOnDelete(onDelete);
			}
		} else if (parentTag.equalsIgnoreCase(END)) {
			End end = (End) parent.getObject();
			if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				end.setDocumentation(documentation);
			}
		} else if (parentTag.equalsIgnoreCase(USING)) {
			Using using = (Using) parent.getObject();
			if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				using.setDocumentation(documentation);
			}
		} else if (parentTag.equalsIgnoreCase(DOCUMENTATION)) {
			Documentation documentation = (Documentation) parent.getObject();
			if (tag.equalsIgnoreCase(LONG_DESCRIPTION)) {
				LongDescription longDescription = (LongDescription) tagObject
						.getObject();
				documentation.setLongDescription(longDescription);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION_SUMMARY)) {
				Summary summary = (Summary) tagObject.getObject();
				documentation.setSummary(summary);
			}
		} else if (parentTag.equalsIgnoreCase(ODataConstantsV3.ENUM_TYPE)) {
			EnumTypeV4 enumType = (EnumTypeV4) parent.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.MEMBER)) {
				EnumTypeMember member = (EnumTypeMember) tagObject.getObject();
				List<EnumTypeMember> members = enumType.getMembers();
				members.add(member);
			}
		} else if (parentTag.equalsIgnoreCase(ODataConstantsV3.ANNOTATIONS)) {
			AnnotationsV4 annotations = (AnnotationsV4) parent.getObject();
			if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				List<Annotation> annotationList = annotations.getAnnotations();
				annotationList.add(annotation);
			}
		} else if (parentTag
				.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
			RecordExpression record = (RecordExpression) parent.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.PROPERTY_VALUE)) {
				PropertyValue propertyValue = (PropertyValue) tagObject
						.getObject();
				List<PropertyValue> propertyValues = record.getPropertyValues();
				propertyValues.add(propertyValue);
			}
		} else if (parentTag.equalsIgnoreCase(TYPE_DEFINITION)) {
			TypeDefinition typeDefinition = (TypeDefinition) parent.getObject();
			if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				typeDefinition.getAnnotation().add(annotation);
			}
		} else if (parentTag.equalsIgnoreCase(ANNOTATION)) {
			Annotation annotation = (Annotation) parent.getObject();
			if (tag.equalsIgnoreCase(ENUM_MEMBER)) {
				String enumMember = (String) tagObject.getObject();
				annotation.getEnumMember().add(enumMember);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
				RecordExpression record = (RecordExpression) tagObject
						.getObject();
				annotation.setRecord(record);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.COLLECTION_EXPRESSION)) {
				CollectionExpression collection = (CollectionExpression) tagObject
						.getObject();
				annotation.setCollection(collection);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
				PathExpression path = (PathExpression) tagObject.getObject();
				annotation.setPath(path);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
				StringConstantExpression primitiveExpr = (StringConstantExpression) tagObject
						.getObject();
				annotation.setString(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
				IntConstantExpression primitiveExpr = (IntConstantExpression) tagObject
						.getObject();
				annotation.setInt(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
				FloatConstantExpression primitiveExpr = (FloatConstantExpression) tagObject
						.getObject();
				annotation.setFloat(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
				DecimalConstantExpression primitiveExpr = (DecimalConstantExpression) tagObject
						.getObject();
				annotation.setDecimal(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
				BoolConstantExpression primitiveExpr = (BoolConstantExpression) tagObject
						.getObject();
				annotation.setBool(primitiveExpr);
			} else if (tag.equalsIgnoreCase(DATE)) {
				DateConstantExpression primitiveExpr = (DateConstantExpression) tagObject
						.getObject();
				annotation.setDate(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
				DateTimeOffsetConstantExpression primitiveExpr = (DateTimeOffsetConstantExpression) tagObject
						.getObject();
				annotation.setDateTimeOffset(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
				GuidConstantExpression primitiveExpr = (GuidConstantExpression) tagObject
						.getObject();
				annotation.setGuid(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
				BinaryConstantExpression primitiveExpr = (BinaryConstantExpression) tagObject
						.getObject();
				annotation.setBinary(primitiveExpr);
			} else if (tag.equalsIgnoreCase(NULL)) {
				NullExpression nullExpression = (NullExpression) tagObject
						.getObject();
				annotation.setNull(nullExpression);
			} else if (tag.equalsIgnoreCase(APPLY)) {
				ApplyExpression applyExpression = (ApplyExpression) tagObject
						.getObject();
				annotation.setApply(applyExpression);
			} else if (tag.equalsIgnoreCase(IF)) {
				IfExpression ifExpression = (IfExpression) tagObject
						.getObject();
				annotation.setIf(ifExpression);
			} else if (tag.equalsIgnoreCase(LABELED_ELEMENT)) {
				LabeledElementExpression labeledElementExpression = (LabeledElementExpression) tagObject
						.getObject();
				annotation.setLabeledElement(labeledElementExpression);
			} else if (tag.equalsIgnoreCase(LABELED_ELEMENT_REFERENCE)) {
				LabeledElementReferenceExpression labeledElementReferenceExpression = (LabeledElementReferenceExpression) tagObject
						.getObject();
				annotation
						.setLabeledElementReference(labeledElementReferenceExpression);
			}
		} else if (parentTag.equalsIgnoreCase(IF)) {
			IfExpression ifExpression = (IfExpression) parent.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
				RecordExpression record = (RecordExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, record);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
				StringConstantExpression primitiveExpr = (StringConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
				IntConstantExpression primitiveExpr = (IntConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
				FloatConstantExpression primitiveExpr = (FloatConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
				DecimalConstantExpression primitiveExpr = (DecimalConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
				BoolConstantExpression primitiveExpr = (BoolConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(DATE)) {
				DateConstantExpression primitiveExpr = (DateConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
				DateTimeOffsetConstantExpression primitiveExpr = (DateTimeOffsetConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
				GuidConstantExpression primitiveExpr = (GuidConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
				BinaryConstantExpression primitiveExpr = (BinaryConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
				PathExpression path = (PathExpression) tagObject.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, path);
			} else if (tag.equalsIgnoreCase(PROPERTY_PATH)) {
				PropertyPathExpression path = (PropertyPathExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, path);
			} else if (tag.equalsIgnoreCase(NAVIGATION_PROPERTY_PATH)) {
				NavigationPropertyPathExpression path = (NavigationPropertyPathExpression) tagObject
						.getObject();
				Object[] childExpressions = ifExpression.getChildExpressions();
				Arrays.add(ifExpression, childExpressions, path);
			}
		} else if (parentTag.equalsIgnoreCase(ODataConstantsV3.PROPERTY_VALUE)) {
			PropertyValue propertyValue = (PropertyValue) parent.getObject();
			if (tag.equalsIgnoreCase(ENUM_MEMBER)) {
				String enumMember = (String) tagObject.getObject();
				propertyValue.getEnumMember().add(enumMember);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
				RecordExpression record = (RecordExpression) tagObject
						.getObject();
				propertyValue.setRecord(record);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.COLLECTION_EXPRESSION)) {
				CollectionExpression collection = (CollectionExpression) tagObject
						.getObject();
				propertyValue.setCollection(collection);
			} else if (tag.equalsIgnoreCase(APPLY)) {
				ApplyExpression applyExpression = (ApplyExpression) tagObject
						.getObject();
				propertyValue.setApply(applyExpression);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
				PathExpression path = (PathExpression) tagObject.getObject();
				propertyValue.setPath(path);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
				StringConstantExpression primitiveExpr = (StringConstantExpression) tagObject
						.getObject();
				propertyValue.setString(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
				IntConstantExpression primitiveExpr = (IntConstantExpression) tagObject
						.getObject();
				propertyValue.setInt(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
				FloatConstantExpression primitiveExpr = (FloatConstantExpression) tagObject
						.getObject();
				propertyValue.setFloat(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
				DecimalConstantExpression primitiveExpr = (DecimalConstantExpression) tagObject
						.getObject();
				propertyValue.setDecimal(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
				BoolConstantExpression primitiveExpr = (BoolConstantExpression) tagObject
						.getObject();
				propertyValue.setBool(primitiveExpr);
			} else if (tag.equalsIgnoreCase(DATE)) {
				DateConstantExpression primitiveExpr = (DateConstantExpression) tagObject
						.getObject();
				propertyValue.setDate(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
				DateTimeOffsetConstantExpression primitiveExpr = (DateTimeOffsetConstantExpression) tagObject
						.getObject();
				propertyValue.setDateTimeOffset(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
				GuidConstantExpression primitiveExpr = (GuidConstantExpression) tagObject
						.getObject();
				propertyValue.setGuid(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
				BinaryConstantExpression primitiveExpr = (BinaryConstantExpression) tagObject
						.getObject();
				propertyValue.setBinary(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				propertyValue.getAnnotation().add(annotation);
			}
		} else if (parentTag
				.equalsIgnoreCase(ODataConstantsV3.COLLECTION_EXPRESSION)) {
			CollectionExpression collection = (CollectionExpression) parent
					.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
				RecordExpression record = (RecordExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, record);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
				StringConstantExpression primitiveExpr = (StringConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
				IntConstantExpression primitiveExpr = (IntConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
				FloatConstantExpression primitiveExpr = (FloatConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
				DecimalConstantExpression primitiveExpr = (DecimalConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
				BoolConstantExpression primitiveExpr = (BoolConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(DATE)) {
				DateConstantExpression primitiveExpr = (DateConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
				DateTimeOffsetConstantExpression primitiveExpr = (DateTimeOffsetConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
				GuidConstantExpression primitiveExpr = (GuidConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
				BinaryConstantExpression primitiveExpr = (BinaryConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
				PathExpression path = (PathExpression) tagObject.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, path);
			} else if (tag.equalsIgnoreCase(PROPERTY_PATH)) {
				PropertyPathExpression path = (PropertyPathExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, path);
			} else if (tag.equalsIgnoreCase(NAVIGATION_PROPERTY_PATH)) {
				NavigationPropertyPathExpression path = (NavigationPropertyPathExpression) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, path);
			}
		} else if (parentTag.equalsIgnoreCase(APPLY)) {
			ApplyExpression applyExpression = (ApplyExpression) parent
					.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
				RecordExpression record = (RecordExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, record);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
				StringConstantExpression primitiveExpr = (StringConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
				IntConstantExpression primitiveExpr = (IntConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
				FloatConstantExpression primitiveExpr = (FloatConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
				DecimalConstantExpression primitiveExpr = (DecimalConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
				BoolConstantExpression primitiveExpr = (BoolConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(DATE)) {
				DateConstantExpression primitiveExpr = (DateConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
				DateTimeOffsetConstantExpression primitiveExpr = (DateTimeOffsetConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
				GuidConstantExpression primitiveExpr = (GuidConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
				BinaryConstantExpression primitiveExpr = (BinaryConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
				PathExpression path = (PathExpression) tagObject.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, path);
			} else if (tag.equalsIgnoreCase(PROPERTY_PATH)) {
				PropertyPathExpression path = (PropertyPathExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, path);
			} else if (tag.equalsIgnoreCase(NAVIGATION_PROPERTY_PATH)) {
				NavigationPropertyPathExpression path = (NavigationPropertyPathExpression) tagObject
						.getObject();
				Object[] childExpressions = applyExpression
						.getChildExpressions();
				Arrays.add(applyExpression, childExpressions, path);
			} else if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				applyExpression.getAnnotation().add(annotation);
			}
		} else if (parentTag.equalsIgnoreCase(LABELED_ELEMENT)) {
			LabeledElementExpression labeledElementExpression = (LabeledElementExpression) parent
					.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
				RecordExpression record = (RecordExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions, record);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
				StringConstantExpression primitiveExpr = (StringConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions,
						primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
				IntConstantExpression primitiveExpr = (IntConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions,
						primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
				FloatConstantExpression primitiveExpr = (FloatConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions,
						primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
				DecimalConstantExpression primitiveExpr = (DecimalConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions,
						primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
				BoolConstantExpression primitiveExpr = (BoolConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions,
						primitiveExpr);
			} else if (tag.equalsIgnoreCase(DATE)) {
				DateConstantExpression primitiveExpr = (DateConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions,
						primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
				DateTimeOffsetConstantExpression primitiveExpr = (DateTimeOffsetConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions,
						primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
				GuidConstantExpression primitiveExpr = (GuidConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions,
						primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
				BinaryConstantExpression primitiveExpr = (BinaryConstantExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions,
						primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
				PathExpression path = (PathExpression) tagObject.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions, path);
			} else if (tag.equalsIgnoreCase(PROPERTY_PATH)) {
				PropertyPathExpression path = (PropertyPathExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions, path);
			} else if (tag.equalsIgnoreCase(NAVIGATION_PROPERTY_PATH)) {
				NavigationPropertyPathExpression path = (NavigationPropertyPathExpression) tagObject
						.getObject();
				Object[] childExpressions = labeledElementExpression
						.getChildExpressions();
				Arrays.add(labeledElementExpression, childExpressions, path);
			} else if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				labeledElementExpression.getAnnotation().add(annotation);
			}
		} else if (parentTag.equalsIgnoreCase(CAST)
				|| parentTag.equalsIgnoreCase(IS_OF)) {
			CastOrIsOfExpression castOrIsOfExpression = (CastOrIsOfExpression) parent
					.getObject();
			if (tag.equalsIgnoreCase(ENUM_MEMBER)) {
				String enumMember = (String) tagObject.getObject();
				castOrIsOfExpression.getEnumMember().add(enumMember);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
				RecordExpression record = (RecordExpression) tagObject
						.getObject();
				castOrIsOfExpression.setRecord(record);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
				StringConstantExpression primitiveExpr = (StringConstantExpression) tagObject
						.getObject();
				castOrIsOfExpression.setString(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
				IntConstantExpression primitiveExpr = (IntConstantExpression) tagObject
						.getObject();
				castOrIsOfExpression.setInt(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
				FloatConstantExpression primitiveExpr = (FloatConstantExpression) tagObject
						.getObject();
				castOrIsOfExpression.setFloat(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
				DecimalConstantExpression primitiveExpr = (DecimalConstantExpression) tagObject
						.getObject();
				castOrIsOfExpression.setDecimal(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
				BoolConstantExpression primitiveExpr = (BoolConstantExpression) tagObject
						.getObject();
				castOrIsOfExpression.setBool(primitiveExpr);
			} else if (tag.equalsIgnoreCase(DATE)) {
				DateConstantExpression primitiveExpr = (DateConstantExpression) tagObject
						.getObject();
				castOrIsOfExpression.setDate(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
				DateTimeOffsetConstantExpression primitiveExpr = (DateTimeOffsetConstantExpression) tagObject
						.getObject();
				castOrIsOfExpression.setDateTimeOffset(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
				GuidConstantExpression primitiveExpr = (GuidConstantExpression) tagObject
						.getObject();
				castOrIsOfExpression.setGuid(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
				BinaryConstantExpression primitiveExpr = (BinaryConstantExpression) tagObject
						.getObject();
				castOrIsOfExpression.setBinary(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
				PathExpression path = (PathExpression) tagObject.getObject();
				castOrIsOfExpression.setPath(path);
			} else if (tag.equalsIgnoreCase(PROPERTY_PATH)) {
				PropertyPathExpression path = (PropertyPathExpression) tagObject
						.getObject();
				castOrIsOfExpression.setPropertyPath(path);
			} else if (tag.equalsIgnoreCase(NAVIGATION_PROPERTY_PATH)) {
				NavigationPropertyPathExpression path = (NavigationPropertyPathExpression) tagObject
						.getObject();
				castOrIsOfExpression.setNavigationPropertyPath(path);

			} else if (tag.equalsIgnoreCase(ANNOTATION)) {
				Annotation annotation = (Annotation) tagObject.getObject();
				castOrIsOfExpression.getAnnotation().add(annotation);
			}
		}
	}
}
