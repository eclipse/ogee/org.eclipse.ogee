/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl;

/**
 * Auxiliary class to represent an xml tag. Not to be used explicitly.
 */
public class TagObject {
	String tag;
	Object object;

	/**
	 * Returns the tag.
	 * 
	 * @return - the tag.
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * Sets tag
	 * 
	 * @param tag
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}

	/**
	 * Returns the object.
	 * 
	 * @return - the object.
	 */
	public Object getObject() {
		return object;
	}

	/**
	 * Sets object.
	 * 
	 * @param object
	 */
	public void setObject(Object object) {
		this.object = object;
	}
}
