/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl;

import org.eclipse.ogee.client.model.atom.AtomAuthor;
import org.eclipse.ogee.client.model.atom.AtomCategory;
import org.eclipse.ogee.client.model.atom.AtomContent;
import org.eclipse.ogee.client.model.atom.AtomEntry;
import org.eclipse.ogee.client.model.atom.AtomFeed;
import org.eclipse.ogee.client.model.atom.AtomLink;
import org.eclipse.ogee.client.model.atom.AtomSummary;
import org.eclipse.ogee.client.model.atom.Inline;
import org.eclipse.ogee.client.model.atom.Mproperties;
import org.eclipse.ogee.client.model.atom.ODataPropertyImpl;
import org.eclipse.ogee.client.model.generic.Arrays;
import org.xml.sax.Attributes;

/**
 * Auxiliary class for parsing an atom feed or entry. Not to be used explicitly.
 * 
 */
public class AtomFeedAndEntryTagFactory implements TagFactory {

	public TagObject createTagObject(String tagName, Attributes attr) {
		TagObject result = new TagObject();
		result.setTag(tagName);

		int attrCount = attr.getLength();

		if (tagName.equalsIgnoreCase(FEED)) {
			AtomFeed atomFeed = new AtomFeed(null, null, null, null);
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(XML_BASE))
						atomFeed.setXmlBase(attr.getValue(i));
					else if (attrName.startsWith(XMLNS))
						atomFeed.getNamespaces().put(attrName, attrValue);
					else
						atomFeed.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(atomFeed);
		} else if (tagName.equalsIgnoreCase(ENTRY)) {
			AtomEntry atomEntry = new AtomEntry(null, null, null, null);
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(XML_BASE))
						atomEntry.setXmlBase(attr.getValue(i));
					else if (attrName.startsWith(XMLNS))
						atomEntry.getNamespaces().put(attrName, attrValue);
					else
						atomEntry.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(atomEntry);
		} else if (tagName.equalsIgnoreCase(LINK)) {
			AtomLink link = new AtomLink();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(HREF))
						link.setHref(attrValue);
					else if (attrName.equalsIgnoreCase(REL))
						link.setRel(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						link.setType(attrValue);
					else if (attrName.equalsIgnoreCase(TITLE))
						link.setTitle(attrValue);
					else
						link.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(link);
		} else if (tagName.equalsIgnoreCase(CONTENT)) {

			AtomContent atomContent = new AtomContent();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(TYPE))
						atomContent.setType(attrValue);
					else if (attrName.equalsIgnoreCase(SRC))
						atomContent.setSrc(attrValue);
					else
						atomContent.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(atomContent);
		} else if (tagName.startsWith(D)) {
			ODataPropertyImpl valueObject = new ODataPropertyImpl();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(M_TYPE))
						valueObject.setType(attrValue);
					else
						valueObject.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(valueObject);
		} else if (tagName.equalsIgnoreCase(M_PROPERTIES)) {
			Mproperties mProperties = new Mproperties();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					mProperties.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(mProperties);
		} else if (tagName.equalsIgnoreCase(M_INLINE)) {
			Inline mInline = new Inline();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					mInline.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(mInline);
		} else if (tagName.equalsIgnoreCase(AUTHOR)) {
			AtomAuthor atomAuthor = new AtomAuthor();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					atomAuthor.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(atomAuthor);
		} else if (tagName.equalsIgnoreCase(CATEGORY)) {
			AtomCategory atomCategory = new AtomCategory();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					atomCategory.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(atomCategory);
		}
		return result;
	}

	public void setValue(TagObject tagObject, String value) {
		String tagName = tagObject.getTag();
		if (tagName.startsWith(D)) {
			ODataPropertyImpl valueObject = (ODataPropertyImpl) tagObject
					.getObject();
			valueObject.setValue(value);
			valueObject.setName(tagName);
		} else if (tagObject.getObject() == null && value != null) {
			tagObject.setObject(value);
		}
	}

	public void addChild(TagObject parent, TagObject tagObject) {
		String tag = tagObject.getTag();
		String parentTag = parent.getTag();

		if (parentTag.equalsIgnoreCase(ENTRY)) {
			AtomEntry atomEntry = (AtomEntry) parent.getObject();
			Object o = tagObject.getObject();

			if (tag.equalsIgnoreCase(ID))
				atomEntry.setAtomId(tagObject.getObject().toString());
			else if (tag.equalsIgnoreCase(TITLE_TAG))
				atomEntry.setAtomTitle(tagObject.getObject().toString());
			else if (tag.equalsIgnoreCase(UPDATED))
				atomEntry.setAtomUpdated(tagObject.getObject().toString());
			else if (tag.equalsIgnoreCase(SUMMARY)) {
				AtomSummary atomSummary = new AtomSummary();
				atomSummary.setContents(tagObject.getObject().toString());
				atomEntry.setAtomSummary(atomSummary);
			} else if (tag.equalsIgnoreCase(CONTENT)) {
				atomEntry.setContent((AtomContent) o);
			} else if (tag.equalsIgnoreCase(M_PROPERTIES)) {
				atomEntry.setMproperties((Mproperties) o);
			} else if (tag.equalsIgnoreCase(LINK)) {
				AtomLink[] atomLinks = (AtomLink[]) atomEntry.getLinks();
				AtomLink atomLink = (AtomLink) tagObject.getObject();
				Arrays.add(atomEntry, atomLinks, atomLink);
			} else if (tag.equalsIgnoreCase(AUTHOR)) {
				AtomAuthor atomAuthor = (AtomAuthor) o;
				atomEntry.setAtomAuthor(atomAuthor);
			} else if (tag.equalsIgnoreCase(CATEGORY)) {
				AtomCategory atomCategory = (AtomCategory) o;
				atomEntry.setAtomCategory(atomCategory);
			}
		} else if (parentTag.equalsIgnoreCase(FEED)) {
			AtomFeed atomFeed = (AtomFeed) parent.getObject();

			if (tag.equalsIgnoreCase(ID))
				atomFeed.setAtomId(tagObject.getObject().toString());
			else if (tag.equalsIgnoreCase(TITLE_TAG))
				atomFeed.setAtomTitle(tagObject.getObject().toString());
			else if (tag.equalsIgnoreCase(UPDATED))
				atomFeed.setAtomUpdated(tagObject.getObject().toString());
			else if (tag.equalsIgnoreCase(LINK)) {
				AtomLink[] atomLinks = atomFeed.getLinks();
				AtomLink atomLink = (AtomLink) tagObject.getObject();
				Arrays.add(atomFeed, atomLinks, atomLink);
			} else if (tag.equalsIgnoreCase(ENTRY)) {
				AtomEntry[] atomEntries = (AtomEntry[]) atomFeed.getEntries();
				AtomEntry atomEntry = (AtomEntry) tagObject.getObject();
				Arrays.add(atomFeed, atomEntries, atomEntry);
			} else if (tag.equalsIgnoreCase(AUTHOR)) {
				AtomAuthor atomAuthor = (AtomAuthor) tagObject.getObject();
				atomFeed.setAtomAuthor(atomAuthor);
			}
		} else if (parentTag.equalsIgnoreCase(LINK)) {
			AtomLink atomLink = (AtomLink) parent.getObject();
			if (tag.equalsIgnoreCase(M_INLINE)) {
				Inline mInline = (Inline) tagObject.getObject();
				atomLink.setmInline(mInline);
			}

		} else if (parentTag.equalsIgnoreCase(CONTENT)) {
			AtomContent atomContent = (AtomContent) parent.getObject();
			if (tag.equalsIgnoreCase(M_PROPERTIES)) {
				Mproperties mProperties = (Mproperties) tagObject.getObject();
				atomContent.setmProperties(mProperties);
			}
		} else if (parentTag.equalsIgnoreCase(M_PROPERTIES)) {
			Mproperties mProperties = (Mproperties) parent.getObject();
			if (tag.startsWith(D)) {
				ODataPropertyImpl dataValue = (ODataPropertyImpl) tagObject
						.getObject();
				mProperties.putDataValue(dataValue);
			}
		} else if (parentTag.startsWith(D)) {
			ODataPropertyImpl parentDataValue = (ODataPropertyImpl) parent
					.getObject();
			if (tag.startsWith(D)) {
				ODataPropertyImpl dataValue = (ODataPropertyImpl) tagObject
						.getObject();
				parentDataValue.putChildDataValue(dataValue);
			}
		} else if (parentTag.equalsIgnoreCase(M_INLINE)) {
			Inline inline = (Inline) parent.getObject();
			if (tag.equalsIgnoreCase(ENTRY)) {
				AtomEntry atomEntry = (AtomEntry) tagObject.getObject();
				inline.setEntry(atomEntry);
			} else if (tag.equalsIgnoreCase(FEED)) {
				AtomFeed atomFeed = (AtomFeed) tagObject.getObject();
				inline.setFeed(atomFeed);
			}
		} else if (parentTag.equalsIgnoreCase(AUTHOR)) {
			AtomAuthor atomAuthor = (AtomAuthor) parent.getObject();
			if (tag.equalsIgnoreCase(NAME_TAG)) {
				atomAuthor.setAtomName(tagObject.getObject().toString());
			} else if (tag.equalsIgnoreCase(URI_TAG)) {
				atomAuthor.setAtomUri(tagObject.getObject().toString());
			} else if (tag.equalsIgnoreCase(EMAIL)) {
				atomAuthor.setAtomEmail(tagObject.getObject().toString());
			}
		}
	}

}
