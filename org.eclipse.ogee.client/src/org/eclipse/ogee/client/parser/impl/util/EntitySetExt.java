/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.client.model.edmx.Property;

/**
 * Auxiliary object that holds additional data for an entity set navigation
 */
public class EntitySetExt implements Comparable<EntitySetExt> {
	private String navigationName;
	private String entityTypeName;
	private List<Property> keyProperties;
	private NavigationProperty navigationProperty;
	private boolean isCreatable;
	private boolean isUpdatable;
	private boolean isDeletable;

	private EntitySet entitySet;

	/**
	 * Default constructor
	 */
	public EntitySetExt() {
		this("", "");
	}

	/**
	 * Constructs a new entity set ext object with the given navigation name and
	 * entity type.
	 * 
	 * @param navigationName
	 *            - navigation's name.
	 * @param entityTypeName
	 *            - entity type's name.
	 */
	public EntitySetExt(String navigationName, String entityTypeName) {
		this.navigationName = navigationName;
		this.entityTypeName = entityTypeName;
		keyProperties = new ArrayList<Property>();
	}

	/**
	 * Returns the navigation property.
	 * 
	 * @return NavigationProperty
	 */
	public NavigationProperty getNavigationProperty() {
		return navigationProperty;
	}

	/**
	 * Sets navigation property.
	 * 
	 * @param navigationProperty
	 */
	public void setNavigationProperty(NavigationProperty navigationProperty) {
		this.navigationProperty = navigationProperty;
	}

	/**
	 * Returns the navigation property's name.
	 * 
	 * @return name of the navigation property
	 */
	public String getNavigationName() {
		return navigationName;
	}

	public void setNavigationName(String navigationName) {
		this.navigationName = navigationName;
	}

	/**
	 * Returns the entity type's name.
	 * 
	 * @return name of the entity type
	 */
	public String getEntityTypeName() {
		return entityTypeName;
	}

	public void setEntityTypeName(String entityTypeName) {
		this.entityTypeName = entityTypeName;
	}

	/**
	 * Returns the entity type's name without the namespace prefix.
	 * 
	 * @return name of the entity type without the namespace prefix.
	 */
	public String getEntityTypeNameNoNamespace() {
		return entityTypeName.substring(entityTypeName.lastIndexOf(".") + 1); //$NON-NLS-1$
	}

	/**
	 * Returns the key properties.
	 * 
	 * @return list of key properties
	 */
	public List<Property> getKeyProperties() {
		return keyProperties;
	}

	/**
	 * Sets the key properties.
	 * 
	 * @param keyProperties
	 */
	public void setKeyProperties(List<Property> keyProperties) {
		this.keyProperties = keyProperties;
	}

	@Override
	public String toString() {
		return "\nnavigationName - " + navigationName + "\nentityTypeName - " //$NON-NLS-1$ //$NON-NLS-2$
				+ entityTypeName + "\nkeyProperties - " + keyProperties; //$NON-NLS-1$
	}

	@Override
	public int compareTo(EntitySetExt that) {
		final int EQUAL = 0;

		if (this == that)
			return EQUAL;

		int comparison = this.navigationName.compareTo(that.navigationName);
		if (comparison != EQUAL) {
			return comparison;
		}

		assert this.equals(that) : "compareTo inconsistent with equals."; //$NON-NLS-1$

		return EQUAL;
	}

	@Override
	public int hashCode() {
		int hc = 0;
		String varstr = navigationName + entityTypeName;
		hc = varstr.hashCode();
		return hc;
	}

	/**
	 * Returns true if the entity set is creatable.
	 * 
	 * @return true if the entity set is creatable
	 */
	public boolean isCreatable() {
		return isCreatable;
	}

	/**
	 * Sets whether the entity set is createable.
	 * 
	 * @param isCreatable
	 */
	public void setCreatable(boolean isCreatable) {
		this.isCreatable = isCreatable;
	}

	/**
	 * Returns true if the entity set is updatable
	 * 
	 * @return true if the entity set is updatable
	 */
	public boolean isUpdatable() {
		return isUpdatable;
	}

	/**
	 * Sets whether the entity set is updatable
	 * 
	 * @param isUpdatable
	 */
	public void setUpdatable(boolean isUpdatable) {
		this.isUpdatable = isUpdatable;
	}

	/**
	 * Returns true if the entity set is deleteable
	 * 
	 * @return true if the entity set is deleteable
	 */
	public boolean isDeletable() {
		return isDeletable;
	}

	/**
	 * Sets whether the entity set is deleteable
	 * 
	 * @param isDeletable
	 */
	public void setDeletable(boolean isDeletable) {
		this.isDeletable = isDeletable;
	}

	/**
	 * Defines equality of state.
	 */
	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;
		if (!(aThat instanceof EntitySetExt))
			return false;

		EntitySetExt that = (EntitySetExt) aThat;
		return (this.navigationName.equals(that.navigationName) && (this.entityTypeName
				.equals(that.entityTypeName)));
	}

	/**
	 * @return the entitySet
	 */
	public EntitySet getEntitySet() {
		return entitySet;
	}

	/**
	 * @param entitySet
	 *            the entitySet to set
	 */
	public void setEntitySet(EntitySet entitySet) {
		this.entitySet = entitySet;
	}
}
