/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Stack;
import java.util.logging.Logger;

import org.eclipse.ogee.client.model.generic.ODataConstants;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Handler for OData Protocol
 * 
 * 
 */
public class ODataSaxHandler extends DefaultHandler {
	private TagFactory tagFactory;
	private final static Logger LOGGER = Logger.getLogger(ODataSaxHandler.class
			.getName());

	/**
	 * @param tagFactory
	 */
	public ODataSaxHandler(TagFactory tagFactory) {
		super();
		this.tagFactory = tagFactory;
	}

	private Stack<TagObject> tagStack = new Stack<TagObject>();

	private ByteArrayOutputStream contents = new ByteArrayOutputStream();// $JL-RESOURCE$

	private Object root;

	/**
	 * Start document.
	 */
	public void startDocument() {

	}

	/**
	 * End document.
	 */
	public void endDocument() {

	}

	/**
	 * Start element with the given parameters.
	 */
	public void startElement(String uri, String localName, String qname,
			Attributes attr) {
		contents.reset();

		TagObject tagObject;
		if (qname != null && qname.startsWith(ODataConstants.ATOM))
			tagObject = tagFactory.createTagObject(localName, attr);
		else if (qname != null && qname.startsWith(ODataConstants.APP))
			tagObject = tagFactory.createTagObject(localName, attr);
		else if (qname != null && qname.startsWith("edmx:"))
			tagObject = tagFactory.createTagObject(localName, attr);
		else
			tagObject = tagFactory.createTagObject(qname, attr);
		tagStack.push(tagObject);
	}

	/**
	 * End element with the given parameters.
	 */
	public void endElement(String uri, String localName, String qname) {
		TagObject tagObject = (TagObject) tagStack.pop();

		if (tagStack.empty()) {
			root = tagObject.getObject();
			if (contents != null) {
				tagFactory.setValue(tagObject, contents.toString());
			}
		}

		else {
			TagObject parent = (TagObject) tagStack.peek();
			tagFactory.setValue(tagObject, contents.toString());
			tagFactory.addChild(parent, tagObject);
		}
	}

	/**
	 * Writes a character.
	 */
	public void characters(char[] ch, int start, int length) {
		String string = new String(ch, start, length);
		try {
			contents.write(string.getBytes("UTF-8")); //$NON-NLS-1$
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
		}
	}

	/**
	 * Returns the root
	 * 
	 * @return - the root
	 */
	public Object getRoot() {
		return root;
	}
}
