/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * 
 * An Enum of Primitive types derived from the Entity Data Model According to
 * this specifications :
 * http://www.odata.org/documentation/overview#AbstractTypeSystem
 * 
 * 
 */
public enum EdmTypes {
	String, Single, Double, Byte, Date, DateTime, Time, Boolean, Decimal, Int16, Int32, Int64, Binary, SByte, Guid, DateTimeOffset, NOVALUE;

	private static final String EDM_PREFIX = "Edm."; //$NON-NLS-1$

	private String fullName = EDM_PREFIX + this.name();

	/**
	 * @return full Edm type string. For example "Edm.Int32" for Int32 type
	 */
	public String toString() {
		return fullName;
	}

	// lookup map for all types. include full name and short name, proper case
	// and lower case
	public static Map<String, EdmTypes> MAP = new HashMap<String, EdmTypes>();
	static {
		for (EdmTypes edmType : EdmTypes.values()) {
			MAP.put(edmType.name(), edmType);
			MAP.put(edmType.name().toLowerCase(Locale.ENGLISH), edmType);

			String key = EDM_PREFIX + edmType.name();
			MAP.put(key, edmType);
			MAP.put(key.toLowerCase(Locale.ENGLISH), edmType);
		}

		// handle null as NOVALUE
		MAP.put(null, NOVALUE);
	}

	/**
	 * Convert string representation to EdmTypes enum value. The parameter can
	 * be in full format (e.g. Edm.Int32) or in a short format (e.g. Int32).
	 * 
	 * @param type
	 *            String representation of the Edm type
	 * @return EdmType matching to the parameter string
	 */
	public static EdmTypes toEdmTypes(String type) {
		EdmTypes returnValue = MAP.get(type);
		if (returnValue == null) {
			// try lower case lookup
			returnValue = MAP.get(type.toLowerCase(Locale.ENGLISH));
			if (returnValue == null) {
				returnValue = NOVALUE;
			}
		}

		return returnValue;
	}

}
