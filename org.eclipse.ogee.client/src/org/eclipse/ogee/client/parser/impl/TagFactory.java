/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.client.parser.impl;

import org.eclipse.ogee.client.model.generic.ODataConstants;
import org.xml.sax.Attributes;

/**
 * A factory for creating xml tags.
 * 
 * 
 */
public interface TagFactory extends ODataConstants {
	/**
	 * Creates a tag object with the given name and attributes.
	 * 
	 * @param tagName
	 *            the name of the tag object to create
	 * @param attr
	 *            attributes for the tag
	 * @return TagObject
	 */
	public TagObject createTagObject(String tagName, Attributes attr);

	/**
	 * Appends a child tag object to a parent object
	 * 
	 * @param parent
	 * @param tagObject
	 */
	public void addChild(TagObject parent, TagObject tagObject);

	/**
	 * Sets the given value to be the value of the given tagObject.
	 * 
	 * @param tagObject
	 * @param value
	 */
	public void setValue(TagObject tagObject, String value);
}
