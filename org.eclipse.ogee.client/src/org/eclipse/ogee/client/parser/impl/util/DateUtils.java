/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl.util;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Date manipulation utilities.
 * 
 */
public final class DateUtils {
	private static final String MINUS_STR = "-"; //$NON-NLS-1$

	private static final String PLUS_STR = "+"; //$NON-NLS-1$

	/** The RFC 3339 format, The default HTTP format (RFC 1123) */
	public static final List<String> FORMAT_TIME_LIST = unmodifiableList(
			"yyyy-MM-dd'T'HH:mm:ssz", "yyyy-MM-dd'T'HH:mm:ss"); //$NON-NLS-1$ //$NON-NLS-2$

	/** The often used GMT time zone. */
	private static final java.util.TimeZone TIMEZONE_GMT = java.util.TimeZone
			.getTimeZone("GMT"); //$NON-NLS-1$

	private static final String CLOSE_BRACKETS = ")"; //$NON-NLS-1$

	private static final String OPEN_BRACKET = "("; //$NON-NLS-1$

	/**
	 * Formats a Date in the default HTTP format (RFC 1123).
	 * 
	 * @param date
	 *            The date to format.
	 * @return The formatted date.
	 */
	public static String format(final Date date) {
		return format(date, DateUtils.FORMAT_TIME_LIST.get(1));
	}

	/**
	 * Formats a Date in the RFC 3339 format
	 * 
	 * @param date
	 *            The date to format.
	 * @return The formatted date.
	 */
	public static String formatToDateTimeOffset(final Date date) {
		return format(date, DateUtils.FORMAT_TIME_LIST.get(0));
	}

	/**
	 * Formats a Date according to the first format in the array.
	 * 
	 * @param date
	 *            The date to format.
	 * @param format
	 *            The date format to use.
	 * @return The formatted date.
	 */
	public static String format(final Date date, final String format) {
		if (date == null) {
			throw new IllegalArgumentException("Date is null"); //$NON-NLS-1$
		}

		java.text.DateFormat formatter = null;
		if (FORMAT_TIME_LIST.get(0).equals(format)) {
			formatter = new DateFormater(TIMEZONE_GMT);
		} else {
			formatter = new java.text.SimpleDateFormat(format,
					java.util.Locale.US);
			formatter.setTimeZone(TIMEZONE_GMT);
		}
		return formatter.format(date);
	}

	/**
	 * Parses a formatted date into a Date object
	 * 
	 * @param date
	 *            The date to parse.
	 * @return The parsed date.
	 */
	public static Date parse(String date) {
		return parse(date, FORMAT_TIME_LIST);
	}

	/**
	 * Parses a formatted date into a Date object.
	 * 
	 * @return The parsed date.
	 */
	public static Date parse(String date, List<String> formats) {
		Date result = null;

		if (date == null) {
			throw new IllegalArgumentException("Date is null"); //$NON-NLS-1$
		}

		String format = null;
		int formatsSize = formats.size();

		for (int i = 0; (result == null) && (i < formatsSize); i++) {
			format = formats.get(i);
			java.text.DateFormat parser = null;

			if (FORMAT_TIME_LIST.get(0).equals(format)) {
				parser = new DateFormater(TIMEZONE_GMT);
			} else {
				parser = new java.text.SimpleDateFormat(format,
						java.util.Locale.US);
				parser.setTimeZone(TIMEZONE_GMT);
			}
			try {
				result = parser.parse(date);
			} catch (Exception e) {
				// Ignores error as the next format may work better
			}
		}

		return result;
	}

	/**
	 * Helper method to help initialize this class by providing unmodifiable
	 * lists based on arrays.
	 * 
	 * @param <T>
	 *            Any valid java object
	 * @param array
	 *            to be converted into an unmodifiable list
	 * @return unmodifiable list based on the provided array
	 */
	private static <T> List<T> unmodifiableList(final T... array) {
		return Collections.unmodifiableList(Arrays.asList(array));
	}

	/**
	 * Parses a formatted Json date into a Date object
	 * 
	 * @param jsonDate
	 *            The date to parse.
	 * @return The parsed date.
	 */
	public static Date parsJsonDate(String jsonDate) {
		if (jsonDate.contains(PLUS_STR) || jsonDate.contains(MINUS_STR)) {
			return DateUtils.parsJsonDateTimeOffset(jsonDate);
		}

		int idx1 = jsonDate.indexOf(OPEN_BRACKET);
		int idx2 = jsonDate.indexOf(CLOSE_BRACKETS);
		String s = jsonDate.substring(idx1 + 1, idx2);
		long l = Long.valueOf(s);
		DateFormater dateFormat = new DateFormater(new Date(l), TIMEZONE_GMT);
		return dateFormat.getDate();
	}

	/**
	 * Parses a formatted Json DateTimeOffset into a Date object
	 * 
	 * @param jsonDate
	 *            The date to parse.
	 * @return The parsed date.
	 */
	public static Date parsJsonDateTimeOffset(String jsonDate) {
		int idx1 = jsonDate.indexOf(OPEN_BRACKET);
		int idx2 = jsonDate.indexOf(CLOSE_BRACKETS);
		String strWithOffset = jsonDate.substring(idx1 + 1, idx2);

		int ndx3 = strWithOffset.lastIndexOf(PLUS_STR);
		if (ndx3 > 0) {
			String date = strWithOffset.substring(0, ndx3);
			String offfset = strWithOffset.substring(ndx3 + 1,
					strWithOffset.length());
			long dateAsLong = Long.valueOf(date);
			long offsetAsLong = Long.valueOf(offfset);
			DateFormater dateFormat = new DateFormater(new Date(dateAsLong
					+ offsetAsLong), TIMEZONE_GMT);
			return dateFormat.getDate();
		}

		int indx4 = strWithOffset.lastIndexOf(MINUS_STR);
		if (indx4 > 0) {
			String date = strWithOffset.substring(0, indx4);
			String offfset = strWithOffset.substring(indx4 + 1,
					strWithOffset.length());
			long dateAsLong = Long.valueOf(date);
			long offsetAsLong = Long.valueOf(offfset);
			DateFormater dateFormat = new DateFormater(new Date(dateAsLong
					- offsetAsLong), TIMEZONE_GMT);
			return dateFormat.getDate();
		}
		return null;
	}

	/**
	 * Formats a Date object to json representation Date
	 * 
	 * @param jsonDate
	 *            The date to parse.
	 * @return The parsed date.
	 */

	public static String formatToJsonDate(Date date) {
		String dateAsString = String.valueOf(date.getTime());
		return "/Date(" + dateAsString + ")/"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Formats a Date object to json representation DateTimeOffset
	 * 
	 * @param jsonDate
	 *            The date to parse.
	 * @return The parsed date.
	 */
	public static String formatToJsonDateTimeOffset(Date date) {
		String dateAsString = String.valueOf(date.getTime());
		String offset = String.valueOf(GetTimeZoneOffset());
		return "/Date(" + dateAsString + "+" + offset + ")/"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/**
	 * Returns the offset of this time zone from UTC at the specified date
	 * 
	 * @return
	 */
	public static int GetTimeZoneOffset() {
		// get Calendar instance
		Calendar cal = Calendar.getInstance();
		// get current TimeZone using getTimeZone method of Calendar class
		TimeZone timeZone = cal.getTimeZone();
		// getOffset(long date) method call for specified time zone.
		int offset = timeZone.getOffset(Calendar.ZONE_OFFSET);
		// convert milliseconds to minutes
		offset = (int) TimeUnit.MILLISECONDS.toMinutes(offset);
		return offset;
	}

	/**
	 * Private constructor to ensure that the class acts as a true utility class
	 * i.e. it isn't instatiable and extensible.
	 */
	private DateUtils() {

	}

}
