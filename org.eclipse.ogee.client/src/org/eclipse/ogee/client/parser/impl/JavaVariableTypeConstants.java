/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * Java Variable Type Constants
 */
public class JavaVariableTypeConstants {
	public static final String FLOAT = Float.class.getName();
	public static final String STRING = String.class.getName();
	public static final String INT = int.class.getName();
	public static final String DATE = Date.class.getName();
	public static final String DOUBLE = Double.class.getName();
	public static final String LONG = Long.class.getName();
	public static final String BOOLEAN = Boolean.class.getName();
	public static final String UUID = UUID.class.getName();
	public static final String BigDecimal = BigDecimal.class.getName();
	public static final String INTEGER = Integer.class.getName();
}
