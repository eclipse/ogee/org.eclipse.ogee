/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl.util;

public enum EntityAssociationType {
	OnlyOne, OnlyMany, All;

	public static EntityAssociationType multiplicityStringToEnum(
			String toMultiplicity) {
		EntityAssociationType entityAssociationType = null;
		if (toMultiplicity != null) {
			if (toMultiplicity.equals("*")) //$NON-NLS-1$
			{
				entityAssociationType = EntityAssociationType.OnlyMany;
			} else {
				entityAssociationType = EntityAssociationType.OnlyOne;
			}
		}

		return entityAssociationType;
	}
}
