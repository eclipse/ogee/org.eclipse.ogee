/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.ogee.client.exceptions.MarshalerException;
import org.eclipse.ogee.client.model.atom.AtomAuthor;
import org.eclipse.ogee.client.model.atom.AtomCategory;
import org.eclipse.ogee.client.model.atom.AtomContent;
import org.eclipse.ogee.client.model.atom.AtomEntry;
import org.eclipse.ogee.client.model.atom.AtomFeed;
import org.eclipse.ogee.client.model.atom.AtomLink;
import org.eclipse.ogee.client.model.atom.AtomSummary;
import org.eclipse.ogee.client.model.atom.Inline;
import org.eclipse.ogee.client.model.atom.Mproperties;
import org.eclipse.ogee.client.model.atom.ODataPropertyImpl;
import org.eclipse.ogee.client.model.edmx.Association;
import org.eclipse.ogee.client.model.edmx.AssociationSet;
import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.Dependent;
import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.EdmxDataServices;
import org.eclipse.ogee.client.model.edmx.End;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.Key;
import org.eclipse.ogee.client.model.edmx.LongDescription;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.client.model.edmx.Parameter;
import org.eclipse.ogee.client.model.edmx.Principal;
import org.eclipse.ogee.client.model.edmx.Property;
import org.eclipse.ogee.client.model.edmx.PropertyRef;
import org.eclipse.ogee.client.model.edmx.ReferentialConstraint;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.model.edmx.Summary;
import org.eclipse.ogee.client.parser.impl.util.DateFormater;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 * Auxiliary class for marshaling xmls. Not to be used explicitly.
 * 
 */
public class Marshaller implements
		org.eclipse.ogee.client.model.generic.ODataConstants {
	private static final String TRUE = "true"; //$NON-NLS-1$
	private static final String M_NULL = "m:null"; //$NON-NLS-1$
	private final static Logger LOGGER = Logger.getLogger(Marshaller.class
			.getName());
	public static final String CHARACTER_ENCODING = "UTF-8"; //$NON-NLS-1$
	private static final String EMPTY_STRING = ""; //$NON-NLS-1$

	public static final String HTTP_WWW_W3_ORG_2000_XMLNS = "http://www.w3.org/2000/xmlns/"; //$NON-NLS-1$

	public final static String ATOM_NS_URI = "http://www.w3.org/2005/Atom"; //$NON-NLS-1$
	public final static String M_NS_URI = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"; //$NON-NLS-1$
	public final static String D_NS_URI = "http://schemas.microsoft.com/ado/2007/08/dataservices"; //$NON-NLS-1$	

	private DocumentBuilderFactory factory;

	// Private constructor prevents instantiation from other classes
	private Marshaller() {
		this.factory = DocumentBuilderFactory.newInstance();
		this.factory.setNamespaceAware(true);
	}

	// SingletonHolder is loaded on the first execution of
	// Marshaller.getInstance()
	// or the first access to SingletonHolder.MARSHALLER_INSTANCE, not before.
	private static class SingletonHolder {
		public static final Marshaller MARSHALLER_INSTANCE = new Marshaller();
	}

	/**
	 * Returns the Marshaller instance.
	 * 
	 * @return the Marshaller instance
	 */
	public static Marshaller getInstance() {
		return SingletonHolder.MARSHALLER_INSTANCE;
	}

	/**
	 * Converts the atom feed into the SAP Data Protocol xml
	 * 
	 * @param atomFeed
	 *            the atom feed to marshal
	 * @return SAP Data Protocol xml representation of the atom feed
	 * @throws ParserConfigurationException
	 *             -
	 * @throws MarshalerException
	 */
	public String marshal(AtomFeed atomFeed) throws MarshalerException {
		if (atomFeed == null) {
			throw new MarshalerException("Atom Feed parameter cannot be NULL."); //$NON-NLS-1$
		}
		// creates new entry document
		DocumentBuilder docBuilder;
		try {
			docBuilder = this.factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			LOGGER.severe(e.getMessage());
			throw new MarshalerException(e);

		}
		Document feedDoc = docBuilder.newDocument();
		// add new atom feed element to the feed document
		addAtomFeed(feedDoc, null, atomFeed, true);
		// get entryDoc as string
		String xml = convertToString(feedDoc);

		return xml;
	}

	public String format(Edmx edmx) throws MarshalerException {
		if (edmx == null) {
			throw new MarshalerException("Edmx parameter cannot be NULL."); //$NON-NLS-1$
		}
		// creates new document
		DocumentBuilder docBuilder;
		try {
			docBuilder = this.factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			LOGGER.severe(e.getMessage());
			throw new MarshalerException(e);
		}
		Document edmxDoc = docBuilder.newDocument();
		// add new Edmx element to the edmx document
		addEdmx(edmxDoc, null, edmx, true);
		// get entryDoc as string
		String xml = convertToString(edmxDoc);

		return xml;
	}

	// adds new edmx
	private void addEdmx(Document doc, Element parentElem, Edmx edmx,
			boolean toAddNamespaces) throws MarshalerException {
		// creates root edmx element
		Element edmxElem = doc.createElementNS(
				"http://schemas.microsoft.com/ado/2007/06/edmx", "edmx:Edmx"); //$NON-NLS-1$ //$NON-NLS-2$

		// adds attributes to the edmx element
		Hashtable<String, String> attributes = edmx.getNamespaces();		
		attributes.put(XMLNS_M, M_NS_URI);
		edmx.setNamespaces(attributes);
		addAttributes(edmxElem, edmx.getAttributes(), toAddNamespaces);

		// add edmx:DataServices element to the edmx element
		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		Element dataServicesElement = addDataServicesElement(doc, edmxElem,
				edmxDataServices);

		// add Schema element(s) to the DataServices element
		Schema[] schemas = edmxDataServices.getSchemas();
		for (Schema schema : schemas) {
			Element schemaElement = addSchemaElement(doc, dataServicesElement,
					schema);

			// add EntityType element(s) to the Schema element
			EntityType[] entityTypes = schema.getEntityTypes();
			for (EntityType entityType : entityTypes) {
				addEntityTypeElement(doc, schemaElement, entityType);
			}

			// add ComplexType element(s) to the Schema element
			ComplexType[] complexTypes = schema.getComplexTypes();
			for (ComplexType complexType : complexTypes) {
				addComplexTypeElement(doc, schemaElement, complexType);
			}

			// add Association element(s) to the Schema element
			Association[] associations = schema.getAssociations();
			for (Association association : associations) {
				addAssociationElement(doc, schemaElement, association);
			}

			// add EntityContainer element(s) to the Schema element
			EntityContainer[] containers = schema.getEntityContainers();
			for (EntityContainer entityContainer : containers) {
				addEntityContainerElement(doc, schemaElement, entityContainer);
			}
		}

		if (parentElem == null) {
			// append edmx element to edmx document
			doc.appendChild(edmxElem);
		} else {
			// append edmx element to the parent element
			parentElem.appendChild(edmxElem);
		}
	}

	private void addComplexTypeElement(Document doc, Element schemaElement,
			ComplexType complexType) {
		if (complexType == null) {
			return;
		}

		// create new ComplexType element
		Element complexTypeElem = doc.createElement("ComplexType"); //$NON-NLS-1$
		// add attributes
		addAttributes(complexTypeElem, complexType.getAttributes());

		Property[] properties = complexType.getProperties();
		for (Property property : properties) {
			addPropertyElement(doc, complexTypeElem, property);
		}

		// append EntityType element to the parent element
		schemaElement.appendChild(complexTypeElem);

	}

	private void addEntityContainerElement(Document doc, Element schemaElement,
			EntityContainer entityContainer) {
		if (entityContainer == null) {
			return;
		}

		// create new EntityContainer element
		Element entityContainerElem = doc.createElement("EntityContainer"); //$NON-NLS-1$
		// add attributes
		addAttributes(entityContainerElem, entityContainer.getAttributes());

		EntitySet[] entitySets = entityContainer.getEntitySets();
		for (EntitySet entitySet : entitySets) {
			addEntitySetElement(doc, entityContainerElem, entitySet);
		}

		AssociationSet[] associationSets = entityContainer.getAssociationSets();
		for (AssociationSet associationSet : associationSets) {
			addAssociationSetElement(doc, entityContainerElem, associationSet);
		}

		FunctionImport[] functionImports = entityContainer.getFunctionImports();
		for (FunctionImport functionImport : functionImports) {
			addFunctionImportElement(doc, entityContainerElem, functionImport);
		}

		// append Association element to the parent element
		schemaElement.appendChild(entityContainerElem);

	}

	private void addFunctionImportElement(Document doc,
			Element entityContainerElem, FunctionImport functionImport) {
		if (functionImport == null) {
			return;
		}

		// create new FunctionImport element
		Element functionImportsElem = doc.createElement("FunctionImport"); //$NON-NLS-1$
		// add attributes
		addAttributes(functionImportsElem, functionImport.getAttributes());

		Parameter[] parameters = functionImport.getParameters();
		for (Parameter parameter : parameters) {
			addParameterElement(doc, functionImportsElem, parameter);
		}

		// append FunctionImport element to the parent element
		entityContainerElem.appendChild(functionImportsElem);

	}

	private void addParameterElement(Document doc, Element functionImportsElem,
			Parameter parameter) {
		if (parameter == null) {
			return;
		}

		// create new Parameter element
		Element parameterElem = doc.createElement("Parameter"); //$NON-NLS-1$
		// add attributes
		addAttributes(parameterElem, parameter.getAttributes());

		addDocumentationElement(doc, parameterElem,
				parameter.getDocumentation());

		// append Parameter element to the parent element
		functionImportsElem.appendChild(parameterElem);

	}

	private void addDocumentationElement(Document doc, Element parameterElem,
			Documentation documentation) {
		if (documentation == null) {
			return;
		}

		// create new FunctionImport element
		Element documentationElem = doc.createElement("Documentation"); //$NON-NLS-1$
		// add attributes
		addAttributes(documentationElem, documentation.getAttributes());

		addSummaryElement(doc, documentationElem, documentation.getSummary());

		addLongDescriptionElement(doc, documentationElem,
				documentation.getLongDescription());

		// append Documentation element to the parent element
		parameterElem.appendChild(documentationElem);

	}

	private void addLongDescriptionElement(Document doc,
			Element documentationElem, LongDescription longDescription) {
		if (longDescription == null) {
			return;
		}

		// create new LongDescription element
		Element longDescriptionElem = doc.createElement("LongDescription"); //$NON-NLS-1$
		// add attributes
		addAttributes(longDescriptionElem, longDescription.getAttributes());

		// create new value text element for the Summary element
		addTextNode(doc, longDescriptionElem, longDescription.getValue());

		// append LongDescription element to the parent element
		documentationElem.appendChild(longDescriptionElem);

	}

	private void addSummaryElement(Document doc, Element documentationElem,
			Summary summary) {

		if (summary == null) {
			return;
		}

		// create new Summary element
		Element summaryElem = doc.createElement("Summary"); //$NON-NLS-1$
		// add attributes
		addAttributes(summaryElem, summary.getAttributes());

		// create new value text element for the Summary element
		addTextNode(doc, summaryElem, summary.getValue());

		// append Summary element to the parent element
		documentationElem.appendChild(summaryElem);
	}

	private void addAssociationSetElement(Document doc,
			Element entityContainerElem, AssociationSet associationSet) {
		if (associationSet == null) {
			return;
		}

		// create new AssociationSet element
		Element associationSetElem = doc.createElement("AssociationSet"); //$NON-NLS-1$
		// add attributes
		addAttributes(associationSetElem, associationSet.getAttributes());

		End[] ends = associationSet.getEnds();
		for (End end : ends) {
			addEndElement(doc, associationSetElem, end);
		}

		// append AssociationSet element to the parent element
		entityContainerElem.appendChild(associationSetElem);

	}

	private void addEntitySetElement(Document doc, Element entityContainerElem,
			EntitySet entitySet) {
		if (entitySet == null) {
			return;
		}

		// create new Key element
		Element entitySetElem = doc.createElement("EntitySet"); //$NON-NLS-1$
		// add attributes
		addAttributes(entitySetElem, entitySet.getAttributes());

		// append EntitySet element to the parent element
		entityContainerElem.appendChild(entitySetElem);

	}

	private void addAssociationElement(Document doc, Element schemaElement,
			Association association) {
		if (association == null) {
			return;
		}

		// create new Association element
		Element associationElem = doc.createElement("Association"); //$NON-NLS-1$
		// add attributes
		addAttributes(associationElem, association.getAttributes());

		End[] ends = association.getEnds();
		for (End end : ends) {
			addEndElement(doc, associationElem, end);
		}

		addReferentialConstraintElement(doc, associationElem,
				association.getReferentialConstraint());

		// append Association element to the parent element
		schemaElement.appendChild(associationElem);

	}

	private void addReferentialConstraintElement(Document doc,
			Element associationElem, ReferentialConstraint referentialConstraint) {
		if (referentialConstraint == null) {
			return;
		}

		// create new Key element
		Element referentialConstraintElem = doc
				.createElement("ReferentialConstraint"); //$NON-NLS-1$
		// add attributes
		addAttributes(referentialConstraintElem,
				referentialConstraint.getAttributes());

		addPrincipalElement(doc, referentialConstraintElem,
				referentialConstraint.getPrincipal());

		addDependentElement(doc, referentialConstraintElem,
				referentialConstraint.getDependent());

		// append Key element to the parent element
		associationElem.appendChild(referentialConstraintElem);

	}

	private void addDependentElement(Document doc,
			Element referentialConstraintElem, Dependent dependent) {
		if (dependent == null) {
			return;
		}

		// create new Principal element
		Element dependentElem = doc.createElement("Dependent"); //$NON-NLS-1$
		// add attributes
		addAttributes(dependentElem, dependent.getAttributes());

		PropertyRef[] propertyRefs = dependent.getPropertyRefs();
		for (PropertyRef propertyRef : propertyRefs) {
			addPropertyRefElement(doc, dependentElem, propertyRef);
		}

		// append Key element to the parent element
		referentialConstraintElem.appendChild(dependentElem);

	}

	private void addPrincipalElement(Document doc,
			Element referentialConstraintElem, Principal principal) {
		if (principal == null) {
			return;
		}

		// create new Principal element
		Element principalElem = doc.createElement("Principal"); //$NON-NLS-1$
		// add attributes
		addAttributes(principalElem, principal.getAttributes());

		PropertyRef[] propertyRefs = principal.getPropertyRefs();
		for (PropertyRef propertyRef : propertyRefs) {
			addPropertyRefElement(doc, principalElem, propertyRef);
		}

		// append Key element to the parent element
		referentialConstraintElem.appendChild(principalElem);

	}

	private void addEndElement(Document doc, Element associationElem, End end) {
		if (end == null) {
			return;
		}

		// create new Key element
		Element endElem = doc.createElement("End"); //$NON-NLS-1$
		// add attributes
		addAttributes(endElem, end.getAttributes());

		// append Key element to the parent element
		associationElem.appendChild(endElem);

	}

	private void addEntityTypeElement(Document doc, Element schemaElement,
			EntityType entityType) {
		if (entityType == null) {
			return;
		}

		// create new EntityType element
		Element entityTypeElem = doc.createElement("EntityType"); //$NON-NLS-1$
		// add attributes
		addAttributes(entityTypeElem, entityType.getAttributes());

		addKeyElement(doc, entityTypeElem, entityType.getKey());

		Property[] properties = entityType.getProperties();
		for (Property property : properties) {
			addPropertyElement(doc, entityTypeElem, property);
		}

		NavigationProperty[] navigationProperties = entityType
				.getNavigationProperties();
		for (NavigationProperty navigationProperty : navigationProperties) {
			addNavigationPropertyElement(doc, entityTypeElem,
					navigationProperty);
		}

		// append EntityType element to the parent element
		schemaElement.appendChild(entityTypeElem);

	}

	private void addNavigationPropertyElement(Document doc,
			Element entityTypeElem, NavigationProperty navigationProperty) {
		if (navigationProperty == null) {
			return;
		}

		// create new Key element
		Element navPropElem = doc.createElement("NavigationProperty"); //$NON-NLS-1$
		// add attributes
		addAttributes(navPropElem, navigationProperty.getAttributes());

		// append Key element to the parent element
		entityTypeElem.appendChild(navPropElem);
	}

	private void addPropertyElement(Document doc, Element entityTypeElem,
			Property property) {
		if (property == null) {
			return;
		}

		// create new Key element
		Element propElem = doc.createElement("Property"); //$NON-NLS-1$
		// add attributes
		addAttributes(propElem, property.getAttributes());

		// append Key element to the parent element
		entityTypeElem.appendChild(propElem);

	}

	private Element addKeyElement(Document doc, Element entityTypeElem, Key key) {
		if (key == null) {
			return null;
		}

		// create new Key element
		Element keyElem = doc.createElement("Key"); //$NON-NLS-1$
		// add attributes
		addAttributes(keyElem, key.getAttributes());

		PropertyRef[] propertyRefs = key.getPropertyRefs();
		for (PropertyRef propertyRef : propertyRefs) {
			addPropertyRefElement(doc, keyElem, propertyRef);
		}
		// append Key element to the parent element
		entityTypeElem.appendChild(keyElem);

		return keyElem;

	}

	private void addPropertyRefElement(Document doc, Element keyElem,
			PropertyRef propertyRef) {
		if (propertyRef == null) {
			return;
		}

		// create new Key element
		Element propRefElem = doc.createElement("PropertyRef"); //$NON-NLS-1$
		// add attributes
		addAttributes(propRefElem, propertyRef.getAttributes());

		// append PropertyRef element to the parent element
		keyElem.appendChild(propRefElem);

	}

	private Element addSchemaElement(Document doc, Element dataServicesElem,
			Schema schema) {
		if (schema == null) {
			return null;
		}

		// create new Schema element
		Element schemaElem = doc.createElement("Schema"); //$NON-NLS-1$
		// add attributes
		addAttributes(schemaElem, schema.getAttributes());
		// append Schema element to the parent element
		dataServicesElem.appendChild(schemaElem);

		return schemaElem;

	}

	private Element addDataServicesElement(Document doc, Element edmxElem,
			EdmxDataServices edmxDataServices) {
		if (edmxDataServices == null) {
			return null;
		}

		// create new edmx:DataServices element
		Element dataServicesElem = doc.createElementNS(
				"http://schemas.microsoft.com/ado/2007/06/edmx", //$NON-NLS-1$
				"edmx:DataServices"); //$NON-NLS-1$
		// add attributes
		addAttributes(dataServicesElem, edmxDataServices.getAttributes());
		// append edmx:DataServices element to the parent element
		edmxElem.appendChild(dataServicesElem);

		return dataServicesElem;
	}

	// adds new atom feed entry
	private void addAtomFeed(Document doc, Element parentElem,
			AtomFeed atomFeed, boolean toAddNamespaces)
			throws MarshalerException {
		// creates root feed element
		Element feedElem = doc.createElementNS(ATOM_NS_URI, ATOM_FEED);

		// adds namespace attributes to the feed element
		Hashtable<String, String> attributes = atomFeed.getNamespaces();
		attributes.put(XMLNS_D, D_NS_URI);
		attributes.put(XMLNS_M, M_NS_URI);
		atomFeed.setNamespaces(attributes);
		addAttributes(feedElem, atomFeed.getAttributes(), toAddNamespaces);

		// add <atom:author> element to the feed element
		addAtomAuthor(doc, feedElem, atomFeed.getAtomAuthor());
		// add <atom:id> element to the feed element
		addElement(doc, feedElem, ATOM_NS_URI, ATOM_ID, atomFeed.getAtomId());
		// add atom links elements to the feed element
		addAtomLinks(doc, feedElem, atomFeed.getLinks());
		// add <atom:title> element to the feed element
		addElement(doc, feedElem, ATOM_NS_URI, ATOM_TITLE,
				atomFeed.getAtomTitle());
		// add <atom:updated> element to the feed element
		String updated = atomFeed.getAtomUpdated();
		if (updated == null) {
			DateFormater df = new DateFormater(TimeZone.getDefault());
			Calendar cal = Calendar.getInstance();
			updated = df.format(cal.getTime());
		}

		addElement(doc, feedElem, ATOM_NS_URI, ATOM_UPDATED, updated);

		// add entry elements to the feed element
		addAtomEntries(doc, feedElem, (AtomEntry[]) atomFeed.getEntries());

		if (parentElem == null) {
			// append atom feed element to feed document
			doc.appendChild(feedElem);
		} else {
			// append atom feed element to the parent element
			parentElem.appendChild(feedElem);
		}
	}

	// adds author element and attributes to the given element
	private void addAtomAuthor(Document doc, Element elem, AtomAuthor atomAuthor) {
		if (atomAuthor == null) {
			return;
		}

		String author = atomAuthor.getAtomName();
		if (author != null && !author.trim().equals(EMPTY_STRING)) {
			// create new atom author element
			Element authorElem = doc.createElementNS(ATOM_NS_URI, ATOM_AUTHOR);
			// add attributes to the atom author element
			addAttributes(authorElem, atomAuthor.getAttributes());
			// create new atom name element
			Element nameElem = doc.createElementNS(ATOM_NS_URI, ATOM_NAME);
			// add atom name text node value
			addTextNode(doc, nameElem, author);
			// append atom name element to the atom author element
			authorElem.appendChild(nameElem);
			// append atom author element to the parent element
			elem.appendChild(authorElem);
		}
	}

	// adds atom entry elements to the feed element
	private void addAtomEntries(Document feedDoc, Element feedElem,
			AtomEntry[] atomEntries) throws MarshalerException {
		if (atomEntries == null) {
			return;
		}

		for (int i = 0; i < atomEntries.length; i++) {
			AtomEntry atomEntry = atomEntries[i];
			if (atomEntry == null) {
				continue;
			}
			// add new <atom:entry> element to the feeds element
			addAtomEntry(feedDoc, feedElem, atomEntry, false);
		}
	}

	/**
	 * Converts the atom entry into the SAP Data Protocol xml
	 * 
	 * @param atomEntry
	 *            the atom entry to marshal
	 * @return SAP Data Protocol xml representation of the atom entry
	 * @throws ParserConfigurationException
	 * @throws MarshalerException
	 */
	public String marshal(AtomEntry atomEntry) throws MarshalerException {
		if (atomEntry == null) {
			throw new MarshalerException("Atom Entry parameter cannot be NULL."); //$NON-NLS-1$
		}
		// create new entry document
		DocumentBuilder docBuilder;
		try {
			docBuilder = this.factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			LOGGER.severe(e.getMessage());
			throw new MarshalerException(e);
		}
		Document entryDoc = docBuilder.newDocument();
		// add new <atom:entry> to the document
		addAtomEntry(entryDoc, null, atomEntry, true);
		// get entryDoc as string
		String xml = convertToString(entryDoc);

		return xml;
	}

	private void addAtomEntry(Document doc, Element parentElem,
			AtomEntry atomEntry, boolean toAddNamespaces)
			throws MarshalerException {
		// create new atom entry element
		Element entryElem = doc.createElementNS(ATOM_NS_URI, ATOM_ENTRY);
		// add namespace attributes to the entry element
		// adds namespace attributes to the feed element
		Hashtable<String, String> attributes = atomEntry.getNamespaces();
		attributes.put(XMLNS_D, D_NS_URI);
		attributes.put(XMLNS_M, M_NS_URI);
		atomEntry.setNamespaces(attributes);
		addAttributes(entryElem, atomEntry.getAttributes(), toAddNamespaces);
		// add content element to the entry element
		// example: <atom:content type="application/xml">
		// <m:properties>
		// <d:value>000000388111</d:value>
		// <d:scheme_id>IW_WF_TASK_GW</d:scheme_id>
		// ...
		// </m:properties>
		// </atom:content>
		addContent(atomEntry.getContent(), doc, entryElem);
		// add m properties
		addMproperties(atomEntry.getMproperties(), doc, entryElem);
		// add <atom:author> element to the entry element
		addAtomAuthor(doc, entryElem, atomEntry.getAtomAuthor());
		// add <atom:category> element to the entry element
		// addAtomCategory(doc, entryElem, atomEntry.getAtomCategory());
		addAtomCategory2(atomEntry.getAtomCategory(), doc, entryElem);
		// adds <atom:id> element to the entry element
		

		String atomId = atomEntry.getAtomId();
		if (atomId != null) {
			addElement(doc, entryElem, ATOM_NS_URI, ATOM_ID, atomId);
		}

		// ad atom link elements to the entry element		
		addAtomLinks(doc, entryElem, (AtomLink[]) atomEntry.getLinks());
		// add <atom:title> element to the entry element
		// example: <atom:title>WORKFLOW_TASK</atom:title>
		addElement(doc, entryElem, ATOM_NS_URI, ATOM_TITLE,
				atomEntry.getAtomTitle());
		// add <atom:updated> element to the entry element
		// example: <atom:updated>2010-11-21T09:27:22Z</atom:updated>
		String updated = atomEntry.getAtomUpdated();
		if (updated == null) {
			DateFormater df = new DateFormater(TimeZone.getDefault());
			Calendar cal = Calendar.getInstance();
			updated = df.format(cal.getTime());
		}

		addElement(doc, entryElem, ATOM_NS_URI, ATOM_UPDATED, updated);

		// add <atom:summary> element to the entry element
		// example: <atom:summary>BLA BLA</atom:summary>
		addAtomSummary(atomEntry.getAtomSummary(), doc, entryElem);

		if (parentElem == null) {
			// add root entry element to the entry document
			doc.appendChild(entryElem);
		} else {
			// add entry element to the parent element
			parentElem.appendChild(entryElem);
		}
	}

	private void addAtomCategory2(AtomCategory atomCategory, Document doc,
			Element entryElem) {
		if (atomCategory == null) {
			return;
		}
		// create new atom content element
		Element categoryElem = doc.createElementNS(ATOM_NS_URI, ATOM_CATEGORY);
		// add attributes to the atom content element
		addAttributes(categoryElem, atomCategory.getAttributes());

		// append atom content element to entry element
		entryElem.appendChild(categoryElem);

	}

	// adds attributes to an element
	private void addAttributes(Element elem, Hashtable<String, String> attrs,
			boolean toAddNamespaces) {
		if (attrs == null || attrs.isEmpty()) {
			return;
		}

		Set<String> keySet = attrs.keySet();
		for (String attr_key : keySet) {
			// check that no namespaces will be added if
			if (attr_key.startsWith(XMLNS) && !toAddNamespaces) {
				continue;
			}

			String attr_value = attrs.get(attr_key);

			elem.setAttribute(attr_key, attr_value);
		}
	}

	// adds attributes to an element
	private void addAttributes(Element elem, Hashtable<String, String> attrs) {
		if (attrs == null || attrs.isEmpty()) {
			return;
		}

		Set<String> keySet = attrs.keySet();
		for (String attr_key : keySet) {
			String attr_value = attrs.get(attr_key);
			String namespaceURI = null;

			if (attr_key.contains(M)) {
				namespaceURI = M_NS_URI;
			} else if (attr_key.contains("xmlns")) //$NON-NLS-1$
			{
				namespaceURI = "http://www.w3.org/2000/xmlns/"; //$NON-NLS-1$
			} 
			elem.setAttributeNS(namespaceURI, attr_key, attr_value);
		}
	}

	// adds <atom:content> element to the entry element
	// example: <atom:content type="application/xml">
	// <m:properties>
	// <d:value>000000388111</d:value>
	// <d:scheme_id>IW_WF_TASK_GW</d:scheme_id>
	// ...
	// </m:properties>
	// </atom:content>
	private void addContent(AtomContent atomContent, Document entryDoc,
			Element entryElem) {
		if (atomContent == null) {
			return;
		}
		// create new atom content element
		Element contentElem = entryDoc.createElementNS(ATOM_NS_URI,
				ATOM_CONTENT);
		// add attributes to the atom content element
		addAttributes(contentElem, atomContent.getAttributes());
		// if there are properties, add them to content
		Mproperties mProperties = atomContent.getmProperties();
		addMproperties(mProperties, entryDoc, contentElem);
		// append atom content element to entry element
		entryElem.appendChild(contentElem);
	}

	// adds <atom:content> element to the entry element
	// example: <atom:content type="application/xml">
	// <m:properties>
	// <d:value>000000388111</d:value>
	// <d:scheme_id>IW_WF_TASK_GW</d:scheme_id>
	// ...
	// </m:properties>
	// </atom:content>
	private void addMproperties(Mproperties mProperties, Document entryDoc,
			Element parentElem) {
		if (mProperties == null || mProperties.getDataValues().length == 0) {
			return;
		}

		// create new Mproperties element
		Element mpropsElem = entryDoc.createElement(M_PROPERTIES);
		// add attributes to the m:properties element
		addAttributes(mpropsElem, mProperties.getAttributes());
		// add m properties to the m:properties element
		addDataValues(mProperties.getDataValues(), entryDoc, mpropsElem);
		// append m properties element to parent element
		parentElem.appendChild(mpropsElem);
	}

	private void addDataValues(ODataPropertyImpl[] dataValues,
			Document entryDoc, Element dPropElem) {
		ODataPropertyImpl[] childDataValues = dataValues;
		for (int i = 0; i < childDataValues.length; i++) {
			ODataPropertyImpl childValue = childDataValues[i];
			String valueStr = childValue.getValue();
			if (valueStr != null) {
				valueStr = valueStr.trim();
			}
			// added for oDataLib 1.0 support
			if (valueStr == null || valueStr.equals(EMPTY_STRING)) {
				valueStr = EMPTY_STRING;
				childValue.setAttribute(M_NULL, TRUE);
			}
			Hashtable<String, String> attrs = childValue.getAttributes();

			// create new d property element
			Element childElement = entryDoc.createElement(childValue
					.getQualifiedName());

			// create new value text element for the m property element
			Text textNode = entryDoc.createTextNode(valueStr);
			// add text node element to the parent element
			childElement.appendChild(textNode);
			// add attributes
			addAttributes(childElement, attrs);
			// appends new d property element to the content element

			if (childValue.isComplexType())
				addDataValues(childValue.getChildDataValues(), entryDoc,
						childElement);

			dPropElem.appendChild(childElement);
		}
	}

	// adds new element to the parent element	
	private void addElement(Document doc, Element parentElem,
			String namespaceURI, String qualifiedName, String value) {
		verifyParameter(qualifiedName);
		value = verifyParameter(value);

		// create new element
		Element childElem = doc.createElementNS(namespaceURI, qualifiedName);
		// create new value text element
		addTextNode(doc, childElem, value);
		// appends new element to the parent element
		parentElem.appendChild(childElem);
	}

	// adds atom link elements to the given element	
	private void addAtomLinks(Document doc, Element elem, AtomLink[] links)
			throws MarshalerException {
		if (links == null) {
			return;
		}
		// add new atom link elements to the given element
		for (int i = 0; i < links.length; i++) {
			AtomLink link = links[i];
			if (link == null) {
				continue;
			}
			// create new atom link element
			Element linkElem = doc.createElementNS(ATOM_NS_URI, ATOM_LINK);
			// add attributes to the atom link element
			addAttributes(linkElem, link.getAttributes());
			// add Inline element to the link element
			addInline(link.getmInline(), doc, linkElem);
			// appends new link element to entry element
			elem.appendChild(linkElem);
		}
	}

	// add Inline element to the link element
	private void addInline(Inline inline, Document doc, Element linkElem)
			throws MarshalerException {
		if (inline == null) {
			return;
		}
		// create new m inline element
		Element inlineElem = doc.createElementNS(M_NS_URI, M_INLINE);
		// add attributes to the inline element
		addAttributes(inlineElem, inline.getAttributes());
		// append new m inline element to the link element
		linkElem.appendChild(inlineElem);
		// add atom entry
		if (inline.isEntry()) { // get atom entry from the inline object
			AtomEntry atomEntry = inline.getEntry();
			// add atom entry only if it is not NULL
			if (atomEntry != null) { // add new atom entry element to the link
										// element without namespaces
				addAtomEntry(doc, inlineElem, atomEntry, false);
			}
		} // add atom feed
		else if (inline.isFeed()) { // get atom feed from the inline object
			AtomFeed atomFeed = inline.getFeed();
			// add atom feed only if it is not NULL
			if (atomFeed != null) { // add new atom feed element to the link
									// element without namespaces
				addAtomFeed(doc, inlineElem, atomFeed, false);
			}
		}
	}

	// adds <atom:summary> element to the entry element
	// example: <atom:summary>BLA BLA</atom:summary>
	private void addAtomSummary(AtomSummary atomSummary, Document entryDoc,
			Element entryElem) {
		if (atomSummary == null) {
			return;
		}
		// create new atom summary element
		Element summaryElem = entryDoc.createElementNS(ATOM_NS_URI,
				ATOM_SUMMARY);
		// add attributes to the summary element
		addAttributes(summaryElem, atomSummary.getAttributes());
		// get atom summary contents
		String value = atomSummary.getContents();
		if (value == null) { // atom summary will not be appended if it's value
								// is NULL
			return;
		}
		// create new value text element
		addTextNode(entryDoc, summaryElem, value);
		// append atom summary element to entry element
		entryElem.appendChild(summaryElem);
	}

	// adds new text node to the parent element
	private void addTextNode(Document doc, Element parentElem, String value) {
		// create new value text element for the element
		Text textNode = doc.createTextNode(value);
		// add text node element to the parent element
		parentElem.appendChild(textNode);
	}

	private String convertToString(Document document) throws MarshalerException {
		StringWriter stringWriter = null;

		try {
			// Output the XML

			// set up a transformer
			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans;
			try {
				trans = transfac.newTransformer();
			} catch (TransformerConfigurationException e) {
				throw new MarshalerException(e);
			}
			document.setXmlStandalone(true);

			// create string from xml tree
			stringWriter = new StringWriter();
			StreamResult result = new StreamResult(stringWriter);
			DOMSource source = new DOMSource(document);

			try {
				trans.transform(source, result);
			} catch (TransformerException e) {
				throw new MarshalerException(e);
			}
			String xmlString = stringWriter.toString();
			// the string containing the XML
			return xmlString;
		} finally {
			if (stringWriter != null) {
				try {
					stringWriter.close();
				} catch (IOException e) {
					LOGGER.severe(e.getMessage());
				}
			}
		}
	}

	// verifies that a string parameters is not NULL or EMPTY string
	private String verifyParameter(String paramValue) { // make trim to the
														// string
		if (paramValue != null) {
			paramValue = paramValue.trim();
		}
		// check that the string is not NULL or EMPTY
		if (paramValue == null || paramValue.length() == 0) {
			return EMPTY_STRING;
		}

		return paramValue;
	}
}
