/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

//import org.eclipse.ogee.client.odatamodel.edmx.v3.EnumType;
import org.eclipse.ogee.client.exceptions.ParserException;
import org.eclipse.ogee.client.model.edm.v4.Annotation;
import org.eclipse.ogee.client.model.edm.v4.AnnotationsV4;
import org.eclipse.ogee.client.model.edm.v4.BoolConstantExpression;
import org.eclipse.ogee.client.model.edm.v4.CollectionExpression;
import org.eclipse.ogee.client.model.edm.v4.EntitySetV4;
import org.eclipse.ogee.client.model.edm.v4.PropertyPathExpression;
import org.eclipse.ogee.client.model.edm.v4.RecordExpression;
import org.eclipse.ogee.client.model.edm.v4.SchemaV4;
import org.eclipse.ogee.client.model.edmx.Association;
import org.eclipse.ogee.client.model.edmx.AssociationSet;
import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.EdmxDataServices;
import org.eclipse.ogee.client.model.edmx.End;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.Key;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.client.model.edmx.Property;
import org.eclipse.ogee.client.model.edmx.PropertyRef;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.model.edmx.v3.EntitySetV3;
import org.eclipse.ogee.client.model.edmx.v3.PropertyValue;
import org.eclipse.ogee.client.model.edmx.v3.ValueAnnotation;
import org.eclipse.ogee.client.model.edmx.v3.expressions.Record;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool;
import org.eclipse.ogee.client.nls.messages.Messages;

/**
 * A utility class for the Edmx object.
 */
public class EdmxUtilities {
	private static final String DOT = "."; //$NON-NLS-1$

	private static final String UPDATE_RESTRICTIONS_TERM_SUFFIX = ".UpdateRestrictions"; //$NON-NLS-1$
	private static final String UPDATABLE = "Updatable"; //$NON-NLS-1$   
	private static final String INSERT_RESTRICTIONS_TERM_SUFFIX = ".InsertRestrictions"; //$NON-NLS-1$
	private static final String INSERTABLE = "Insertable"; //$NON-NLS-1$
	private static final String DELETE_RESTRICTIONS_TERM_SUFFIX = ".DeleteRestrictions"; //$NON-NLS-1$
	private static final String DELETABLE = "Deletable"; //$NON-NLS-1$
	private static final String QUERYABLE_TERM_SUFFIX = ".Queryable"; //$NON-NLS-1$
	private static final String SORT_RESTRICTIONS_TERM_SUFFIX = ".SortRestrictions"; //$NON-NLS-1$
	private static final String SORTABLE = "Sortable"; //$NON-NLS-1$
	private static final String FILTER_RESTRICTIONS_TERM_SUFFIX = ".FilterRestrictions"; //$NON-NLS-1$
	private static final String FILTERABLE = "Filterable"; //$NON-NLS-1$
	private static final String CLIENT_PAGEABLE_TERM_SUFFIX = ".ClientPageable"; //$NON-NLS-1$

	/**
	 * Returns a list of all entity set extensions of the service.
	 * 
	 * @param edmxObj
	 *            an Edmx object that packages a service metadata
	 * @return list of entity set extensions of the service
	 */
	public static List<EntitySetExt> getAllEntitySetsExt(Edmx edmxObj) {
		List<EntitySet> entities = new ArrayList<EntitySet>();

		Schema[] schemas = edmxObj.getEdmxDataServices().getSchemas();
		for (int i = 0; i < schemas.length; i++) {
			Schema schema = schemas[i];
			EntityContainer[] entityContainers = schema.getEntityContainers();
			for (int j = 0; j < entityContainers.length; j++) {
				EntityContainer entityContainer = entityContainers[j];
				EntitySet[] entitySets = entityContainer.getEntitySets();
				for (int k = 0; k < entitySets.length; k++) {
					entities.add(entitySets[k]);
				}
			}
		}

		// building EntitySetExt objects
		return convertToEntitySetExt(entities, edmxObj);
	}

	/**
	 * Returns the end of the given association, according to the given role.
	 * 
	 * @param edmxAssociationSetEndRoleName
	 *            - the role of the association set end.
	 * @param edmxAssociation
	 *            - the association.
	 * @return - the end of the given association, according to the given role.
	 */
	public static org.eclipse.ogee.client.model.edmx.End getEnd(
			String edmxAssociationSetEndRoleName,
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation) {
		if (edmxAssociation == null) {
			return null;
		}
		End[] edmxAssociationEnds = edmxAssociation.getEnds();
		for (End edmxAssociationEnd : edmxAssociationEnds) {
			String edmxAssociationEndRoleName = edmxAssociationEnd.getRole();
			if (edmxAssociationEndRoleName
					.equals(edmxAssociationSetEndRoleName)) {
				return edmxAssociationEnd;
			}
		}

		return null;
	}

	/**
	 * Checks whether the role of the given association set end matches the
	 * given role, and if so, it returns it.
	 * 
	 * @param roleName
	 *            - the role
	 * @param edmxAssociationSetEnd
	 *            - association set end
	 * @param edmx
	 * @return - true whether the role of the given association set end matches
	 *         the given role, false otherwise.
	 */
	public static org.eclipse.ogee.client.model.edmx.End getEnd(
			String roleName,
			org.eclipse.ogee.client.model.edmx.End edmxAssociationSetEnd,
			Edmx edmx) {
		if (edmxAssociationSetEnd.getRole().equals(roleName)) {
			return edmxAssociationSetEnd;
		}

		return null;
	}

	/**
	 * Converts a list of entity sets to the appropriate entity set extension
	 * objects
	 * 
	 * @param entities
	 *            list of entity set objects of the service to convert
	 * @param edmxObj
	 *            an Edmx object that packages a service metadata
	 * @return list of entity set extensions represents the received service
	 *         entity sets
	 */
	public static List<EntitySetExt> convertToEntitySetExt(
			List<EntitySet> entities, Edmx edmxObj) {
		List<EntitySetExt> entitiesExts = new ArrayList<EntitySetExt>();
		for (EntitySet entity : entities) {
			EntitySetExt entitySetExt = new EntitySetExt(entity.getName(),
					entity.getEntityType());

			entitySetExt.setEntitySet(entity);

			// set the creatable / updateable / deletable attributes
			entitySetExt.setCreatable(isEntitySetCreatable(entity, edmxObj));
			entitySetExt.setUpdatable(isEntitySetUpdatable(entity, edmxObj));
			entitySetExt.setDeletable(isEntitySetDeletable(entity, edmxObj));

			// populate key properties according to property refs
			EntityType entityType = getEntityTypeByName(entity.getEntityType(),
					edmxObj.getEdmxDataServices().getSchemas());
			Property[] properties = entityType.getProperties();
			Key key = entityType.getKey();
			if (key != null) {
				PropertyRef[] propertyRefs = key.getPropertyRefs();
				for (PropertyRef propertyRef : propertyRefs) {
					entitySetExt.getKeyProperties().add(
							getPropertyByPropertyRef(propertyRef.getName(),
									properties));
				}
			}

			entitiesExts.add(entitySetExt);
		}

		return entitiesExts;
	}

	/**
	 * Returns a list of all addressable entity set extensions of the service.
	 * 
	 * @param edmxObj
	 *            an Edmx object that packages a service metadata
	 * @return list of addressable entity set extensions of the service
	 */
	public static List<EntitySetExt> getAllAddressableEntitySets(Edmx edmxObj) {
		LinkedList<EntitySet> addressableCollections = new LinkedList<EntitySet>();

		List<EntitySet> tmpList = EdmxUtilities.getAllEntitySets(edmxObj);

		for (EntitySet entitySet : tmpList) {
			if (isEntitySetAddressable(entitySet, edmxObj)) {
				addressableCollections.add(entitySet);
			}
		}

		return convertToEntitySetExt(addressableCollections, edmxObj);
	}

	/**
	 * Returns a list of all entity sets of the service
	 * 
	 * @param edmxObj
	 *            an Edmx object that packages a service metadata
	 * @return list of entity sets of the service
	 */
	public static List<EntitySet> getAllEntitySets(Edmx edmxObj) {
		List<EntitySet> entities = new ArrayList<EntitySet>();

		Schema[] schemas = edmxObj.getEdmxDataServices().getSchemas();
		for (int i = 0; i < schemas.length; i++) {
			Schema schema = schemas[i];
			EntityContainer[] entityContainers = schema.getEntityContainers();
			for (int j = 0; j < entityContainers.length; j++) {
				EntityContainer entityContainer = entityContainers[j];
				EntitySet[] entitySets = entityContainer.getEntitySets();
				for (int k = 0; k < entitySets.length; k++) {
					entities.add(entitySets[k]);
				}
			}
		}

		return entities;
	}

	/**
	 * Returns a list of all FunctionImports of the service
	 * 
	 * @param edmxObj
	 *            an Edmx object that packages a service metadata
	 * @return list of entity sets of the service
	 */
	public static List<FunctionImport> getAllFunctionImports(Edmx edmxObj) {
		List<FunctionImport> entities = new ArrayList<FunctionImport>();

		Schema[] schemas = edmxObj.getEdmxDataServices().getSchemas();
		for (int i = 0; i < schemas.length; i++) {
			Schema schema = schemas[i];
			EntityContainer[] entityContainers = schema.getEntityContainers();
			for (int j = 0; j < entityContainers.length; j++) {
				EntityContainer entityContainer = entityContainers[j];
				FunctionImport[] entitySets = entityContainer
						.getFunctionImports();
				for (int k = 0; k < entitySets.length; k++) {
					entities.add(entitySets[k]);
				}
			}
		}

		return entities;
	}

	/**
	 * Returns a list of all FunctionImports of the service
	 * 
	 * @param edmxObj
	 *            an Edmx object that packages a service metadata
	 * @return list of entity sets of the service
	 */
	public static List<FunctionImport> getAllFunctionImports(
			EdmxDataServices edmxDataServices) {
		List<FunctionImport> entities = new ArrayList<FunctionImport>();

		Schema[] schemas = edmxDataServices.getSchemas();
		for (int i = 0; i < schemas.length; i++) {
			Schema schema = schemas[i];
			EntityContainer[] entityContainers = schema.getEntityContainers();
			for (int j = 0; j < entityContainers.length; j++) {
				EntityContainer entityContainer = entityContainers[j];
				FunctionImport[] entitySets = entityContainer
						.getFunctionImports();
				for (int k = 0; k < entitySets.length; k++) {
					entities.add(entitySets[k]);
				}
			}
		}

		return entities;
	}

	/**
	 * Returns the entity type according to the given name
	 * 
	 * @param typeName
	 *            entity type name
	 * @param schemas
	 * @return Entity type
	 */
	public static EntityType getEntityTypeByName(String typeName,
			Schema[] schemas) {
		if (typeName.contains("(")) //$NON-NLS-1$
		{
			typeName = typeName.substring(typeName.lastIndexOf(DOT) + 1,
					typeName.length() - 1);
		}

		for (Schema schema : schemas) {
			String schemaNamespace = schema.getNamespace();
			String schemaAlias = schema.getAlias();

			EntityType[] entityTypes = schema.getEntityTypes();
			for (EntityType entityType : entityTypes) {
				if (compareWithNamespace(entityType.getName(), typeName,
						schemaNamespace, schemaAlias)) {
					return entityType;
				}
			}
		}

		return null;
	}

	/**
	 * Returns the complex type according to the given name
	 * 
	 * @param typeName
	 *            complex type name
	 * @param schemas
	 * @return Complex type
	 */
	public static ComplexType getComplexTypeByName(String typeName,
			Schema[] schemas) {
		for (Schema schema : schemas) {
			String schemaNamespace = schema.getNamespace();
			String schemaAlias = schema.getAlias();

			ComplexType[] complexTypes = schema.getComplexTypes();
			for (ComplexType complexType : complexTypes) {
				String complexTypeName = complexType.getName();
				if (compareWithNamespace(complexTypeName, typeName,
						schemaNamespace, schemaAlias)) {
					return complexType;
				}
			}
		}

		return null;
	}

	/**
	 * Returns the complex type according to the given name and Edmx.
	 * 
	 * @param typeName
	 * @param edmx
	 * @return - Complex type
	 */
	public static ComplexType getComplexTypeByName(String typeName, Edmx edmx) {
		if (typeName == null) {
			return null;
		}

		if (typeName.contains("(")) //$NON-NLS-1$
		{
			typeName = typeName.substring(typeName.lastIndexOf(DOT) + 1,
					typeName.length() - 1);
		}

		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		if (edmxDataServices == null) {
			return null;
		}

		Schema[] schemas = edmxDataServices.getSchemas();

		ComplexType complexType = getComplexTypeByName(typeName, schemas);

		return complexType;
	}

	/**
	 * Returns the entity type according to the given name and Edmx.
	 * 
	 * @param typeName
	 * @param edmx
	 * @return - Entity type
	 */
	public static EntityType getEntityTypeByName(String typeName, Edmx edmx) {
		if (typeName == null) {
			return null;
		}

		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		if (edmxDataServices == null) {
			return null;
		}

		Schema[] schemas = edmxDataServices.getSchemas();

		EntityType entityType = getEntityTypeByName(typeName, schemas);

		return entityType;
	}

	/**
	 * Returns a list of Property objects for a given entity type.
	 * 
	 * @param entityType
	 *            a given entity type
	 * @return list of Property objects for that type
	 */
	public static List<Property> getProperties(EntityType entityType) {
		List<Property> properties = new ArrayList<Property>();

		Property[] propertiesArray = entityType.getProperties();
		for (int i = 0; i < propertiesArray.length; i++) {
			properties.add(propertiesArray[i]);
		}

		return properties;
	}

	/**
	 * Returns the entity type properties of the given entity set.
	 * 
	 * @param entitySet
	 * @param edmxObj
	 * @return properties of the given entity
	 */
	public static Property[] getProperties(EntitySet entitySet, Edmx edmxObj) {
		String entityTypeName = entitySet.getEntityType();
		String namespace = extractNamespace(entityTypeName);
		Schema[] schemas = edmxObj.getEdmxDataServices().getSchemas();
		for (int i = 0; i < schemas.length; i++) {
			if (schemas[i].getNamespace().equals(namespace)) {
				EntityType[] entityTypes = schemas[i].getEntityTypes();
				for (int j = 0; j < entityTypes.length; j++) {
					String name = schemas[i].getNamespace()
							+ "." + entityTypes[j].getName(); //$NON-NLS-1$
					if (name.equals(entityTypeName)) {
						return entityTypes[j].getProperties();
					}
				}
			}
		}

		return new Property[0];
	}

	/**
	 * Returns the entity type properties of the given entityTypeName.
	 * 
	 * @param entityTypeName
	 * @param edmxObj
	 * @return properties of the given entity
	 */
	public static List<Property> getProperties(String entityTypeName,
			Edmx edmxObj) {
		List<Property> propertiesList = new ArrayList<Property>();

		String namespace = extractNamespace(entityTypeName);
		Schema[] schemas = edmxObj.getEdmxDataServices().getSchemas();

		if (schemas.length > 1) {
			for (int i = 0; i < schemas.length; i++) {
				String schemaNamespace = schemas[i].getNamespace();

				if (schemaNamespace.equals(namespace)) {
					EntityType[] entityTypes = schemas[i].getEntityTypes();
					for (int j = 0; j < entityTypes.length; j++) {
						String name = schemas[i].getNamespace() + DOT
								+ entityTypes[j].getName();
						if (name.equals(entityTypeName)) {
							Property[] properties = entityTypes[j]
									.getProperties();
							for (int k = 0; k < properties.length; k++) {
								propertiesList.add(properties[k]);
							}

							return propertiesList;
						}
					}
				}
			}
		}
		// Handle CSDL with only one schema
		List<Property> properties = getProperties(entityTypeName, schemas);
		if (properties != null) {
			return properties;
		}

		return propertiesList;
	}

	private static List<Property> getProperties(String entityTypeName,
			Schema[] schemas) {
		String namePrefixWithAliasName;
		String namePrefixWithSchemaName;
		String alias = schemas[0].getAlias();
		String schemaNamespace = schemas[0].getNamespace();

		if ((alias != null) && (!alias.isEmpty())) {
			namePrefixWithAliasName = (entityTypeName.contains(DOT) ? alias
					+ DOT : ""); //$NON-NLS-1$
		} else {

			namePrefixWithAliasName = (entityTypeName.contains(DOT) ? schemaNamespace
					+ DOT
					: ""); //$NON-NLS-1$
		}

		namePrefixWithSchemaName = (entityTypeName.contains(DOT) ? schemaNamespace
				+ DOT
				: ""); //$NON-NLS-1$

		List<EntityType> entityTypesList = new LinkedList<EntityType>();
		List<Schema> asList = Arrays.asList(schemas);

		for (Schema schema : asList) {
			EntityType[] entityTypes = schema.getEntityTypes();
			entityTypesList.addAll(Arrays.asList(entityTypes));
		}

		for (EntityType eType : entityTypesList) {
			String name = namePrefixWithAliasName + eType.getName();
			if (name.equals(entityTypeName)) {
				return Arrays.asList(eType.getProperties());
			}
			name = namePrefixWithSchemaName + eType.getName();
			if (name.equals(entityTypeName)) {
				return Arrays.asList(eType.getProperties());
			}
		}
		return null;
	}

	/**
	 * Returns the complex type properties of the given complexTypeName.
	 * 
	 * @param complexTypeName
	 * @param edmxObj
	 * @return properties of the given entity
	 */
	public static List<Property> getCtypeProperties(String complexTypeName,
			Edmx edmxObj) {
		List<Property> propertiesList = new ArrayList<Property>();

		String namespace = extractNamespace(complexTypeName);
		Schema[] schemas = edmxObj.getEdmxDataServices().getSchemas();

		if (schemas.length > 1) {
			for (int i = 0; i < schemas.length; i++) {
				if (schemas[i].getNamespace().equals(namespace)) {
					ComplexType[] cTypes = schemas[i].getComplexTypes();
					for (int j = 0; j < cTypes.length; j++) {
						String name = schemas[i].getNamespace() + DOT
								+ cTypes[j].getName();
						if (name.equals(complexTypeName)) {
							Property[] properties = cTypes[j].getProperties();
							for (int k = 0; k < properties.length; k++) {
								propertiesList.add(properties[k]);
							}

							return propertiesList;
						}
					}
				}
			}

		}

		ComplexType[] entityTypes = schemas[0].getComplexTypes();
		for (ComplexType eType : entityTypes) {
			String name = eType.getName();
			if (name.equals(complexTypeName)) {
				return Arrays.asList(eType.getProperties());
			}
		}

		return propertiesList;
	}

	private static String extractNamespace(String entityTypeName) {
		if (entityTypeName.contains(DOT)) {
			entityTypeName = entityTypeName.substring(0,
					entityTypeName.lastIndexOf(DOT));
		}
		return entityTypeName;
	}

	private static String removeNamespace(String name) {
		if (name.contains(DOT)) {
			name = name.substring(name.lastIndexOf(DOT) + 1);
		}
		return name;
	}

	/**
	 * Returns a list of entity set ext that can be navigated from the
	 * selectedEntitySet according to the given association type.
	 * 
	 * @param selectedEntitySetExt
	 * @param edmxObj
	 * @param assocType
	 * @return list of entity set ext
	 */
	public static List<EntitySetExt> getEntitySetNavigations(
			EntitySetExt selectedEntitySetExt, Edmx edmxObj,
			EntityAssociationType assocType) {
		List<EntitySetExt> navigated = new ArrayList<EntitySetExt>();

		EntityType selectedEntityType = EdmxUtilities.getEntityTypeByName(
				selectedEntitySetExt.getEntityTypeName(), edmxObj
						.getEdmxDataServices().getSchemas());

		// find the appropriate association object
		for (NavigationProperty navProperty : selectedEntityType
				.getNavigationProperties()) {

			String targetAssociaton = navProperty.getRelationship();
			for (Schema schema : edmxObj.getEdmxDataServices().getSchemas()) {
				if (schema == null) {
					continue;
				}

				for (Association association : schema.getAssociations()) {
					if (association == null) {
						continue;
					}

					String associationName = association.getName();
					String schemaNamespace = schema.getNamespace();
					if (associationName == null || targetAssociaton == null
							|| schemaNamespace == null) {
						continue;
					}

					if ((associationName.equals(targetAssociaton))
							|| (targetAssociaton.equals(schemaNamespace + DOT
									+ associationName))) {
						String toRole = navProperty.getToRole();
						String toMultiplicity = ""; //$NON-NLS-1$
						String toType = ""; //$NON-NLS-1$

						for (End end : association.getEnds()) {
							if (end.getRole().equals(toRole)) {
								toMultiplicity = end.getMultiplicity();
								toType = end.getType();
							}
						}

						List<AssociationSet> associationSets = getAssociationSets(
								association, edmxObj);
						EntitySet targetEntitySet = null;
						for (AssociationSet associationSet : associationSets) {
							for (int i = 0; i < associationSet.getEnds().length; i++) {
								if (associationSet.getEnds()[i].getEntitySet()
										.equalsIgnoreCase(
												selectedEntitySetExt
														.getEntitySet()
														.getName())) {
									targetEntitySet = getEntitySetByName(
											associationSet.getEnds()[(i + 1) % 2]
													.getEntitySet(), edmxObj);
									break;
								}
							}
						}

						EntityAssociationType type = EntityAssociationType
								.multiplicityStringToEnum(toMultiplicity);

						if (type.equals(assocType)
								|| assocType.equals(EntityAssociationType.All)) {
							EntitySetExt entitySetExt = new EntitySetExt(
									navProperty.getName(), toType);
							entitySetExt.setEntitySet(targetEntitySet);
							entitySetExt.setNavigationProperty(navProperty);

							// populate key properties according to property
							// refs
							EntityType entityType = getEntityTypeByName(toType,
									edmxObj.getEdmxDataServices().getSchemas());
							Property[] properties = entityType.getProperties();
							PropertyRef[] propertyRefs = entityType.getKey()
									.getPropertyRefs();
							for (PropertyRef propertyRef : propertyRefs) {
								entitySetExt.getKeyProperties().add(
										getPropertyByPropertyRef(
												propertyRef.getName(),
												properties));
							}

							// set the creatable / updateable / deletable
							// attributes
							EntitySet entitySet = getNavigationPropertyEntitySet(
									edmxObj, navProperty);
							entitySetExt.setCreatable(isEntitySetCreatable(
									entitySet, edmxObj));
							entitySetExt.setUpdatable(isEntitySetUpdatable(
									entitySet, edmxObj));
							entitySetExt.setDeletable(isEntitySetDeletable(
									entitySet, edmxObj));

							navigated.add(entitySetExt);
						}
					}
				}
			}
		}

		// add yourself if you navigate to multiplicity of 1
		if (assocType == EntityAssociationType.OnlyOne
				|| assocType == EntityAssociationType.All) {
			navigated.add(selectedEntitySetExt);
		}

		return navigated;
	}

	private static List<AssociationSet> getAssociationSets(
			Association association, Edmx edmxObj) {
		List<AssociationSet> sets = new LinkedList<AssociationSet>();
		Schema[] schemas = edmxObj.getEdmxDataServices().getSchemas();
		for (Schema schema : schemas) {
			EntityContainer[] entityContainers = schema.getEntityContainers();
			for (EntityContainer entityContainer : entityContainers) {
				AssociationSet[] associationSets = entityContainer
						.getAssociationSets();
				for (AssociationSet associationSet : associationSets) {
					if (associationSet.getAssociation()
							.equalsIgnoreCase(
									schema.getNamespace() + "."
											+ association.getName())) {
						sets.add(associationSet);
					}
				}
			}
		}
		return sets;
	}

	private static Property getPropertyByPropertyRef(String name,
			Property[] properties) {
		for (int i = 0; i < properties.length; i++) {
			if (properties[i].getName().equalsIgnoreCase(name)) {
				return properties[i];
			}
		}
		return null;
	}

	/**
	 * Checks the given entitySetExt for the given association multiplicity
	 * type.
	 * 
	 * @param entitySetExt
	 * @param associationType
	 * @param edmxObj
	 * @return true if the seleced entity set has navigations of the given
	 *         multiplicity
	 */
	public static boolean containsMultiplicity(EntitySetExt entitySetExt,
			EntityAssociationType associationType, Edmx edmxObj) {
		EntityType selectedEntityType = EdmxUtilities.getEntityTypeByName(
				entitySetExt.getEntityTypeName(), edmxObj.getEdmxDataServices()
						.getSchemas());

		// find the appropriate association object
		for (NavigationProperty navProperty : selectedEntityType
				.getNavigationProperties()) {
			String targetAssociaton = navProperty.getRelationship();
			for (Schema schema : edmxObj.getEdmxDataServices().getSchemas()) {
				if (schema == null) {
					continue;
				}

				for (Association association : schema.getAssociations()) {
					if (association == null) {
						continue;
					}

					String associationName = association.getName();
					String schemaNamespace = schema.getNamespace();
					if (associationName == null || targetAssociaton == null
							|| schemaNamespace == null) {
						continue;
					}

					if ((associationName.equals(targetAssociaton))
							|| (targetAssociaton.equals(schemaNamespace + DOT
									+ associationName))) {
						String toRole = navProperty.getToRole();
						String toMultiplicity = ""; //$NON-NLS-1$

						for (End end : association.getEnds()) {
							if (end.getRole().equals(toRole)) {
								toMultiplicity = end.getMultiplicity();
							}
						}

						EntityAssociationType type = EntityAssociationType
								.multiplicityStringToEnum(toMultiplicity);
						if (type.equals(associationType)) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	private static boolean compareWithNamespace(String type1, String type2,
			String namespace, String alias) {
		if (type1.equals(type2))
			return true;

		if (type1.contains(DOT)) {
			type1 = type1.substring(type1.lastIndexOf(DOT) + 1, type1.length());
		}

		if (type2.contains(DOT)) {
			String type2Namespace = type2.substring(0, type2.lastIndexOf(DOT));

			if (!type2Namespace.equals(namespace)
					&& !type2Namespace.equals(alias)) {
				// not the same namespace
				return false;
			}

			type2 = type2.substring(type2.lastIndexOf(DOT) + 1, type2.length());
		}

		return (type1.equals(type2));
	}

	/**
	 * Finds the association in the given Edmx by the given name.
	 * 
	 * @param name
	 *            - the name of the association.
	 * @param edmxObject
	 *            - the Edmx.
	 * @return - the association by its name, or null.
	 */
	public static Association getAssociation(String name, Edmx edmxObject) {
		Schema[] schemas = edmxObject.getEdmxDataServices().getSchemas();
		for (Schema schema : schemas) {
			String schemaNamespace = schema.getNamespace();
			String schemaAlias = schema.getAlias();

			Association[] associations = schema.getAssociations();
			for (Association association : associations) {
				if (compareWithNamespace(association.getName(), name,
						schemaNamespace, schemaAlias)) {
					return association;
				}
			}
		}

		return null;
	}

	/**
	 * Returns the name of the entity set representing the "to" role of the
	 * navigation property.
	 * 
	 * @param navProp
	 *            - navigation property
	 * @param edmxObject
	 *            - Edmx
	 * @return - the name of the entity set representing the "to" role of the
	 *         navigation property.
	 */
	public static String getNavigationPropertyEntitySetName(Edmx edmxObject,
			NavigationProperty navigationProperty) {
		return getNavigationPropertyEntitySet(edmxObject, navigationProperty)
				.getName();
	}

	/**
	 * Returns the entity set representing the "to" role of the navigation
	 * property.
	 * 
	 * @param navProp
	 *            - navigation property
	 * @param edmxObject
	 *            - Edmx
	 * @return - the entity set representing the "to" role of the navigation
	 *         property.
	 */
	public static EntitySet getNavigationPropertyEntitySet(Edmx edmxObject,
			NavigationProperty navigationProperty) {
		if (edmxObject == null) {
			return null;
		}

		if (navigationProperty == null) {
			return null;
		}

		String association = navigationProperty.getRelationship();
		String toRole = navigationProperty.getToRole();

		EdmxDataServices edmxDataServices = edmxObject.getEdmxDataServices();
		Schema[] schemas = edmxDataServices.getSchemas();
		for (Schema schema : schemas) {
			for (EntityContainer entityContainer : schema.getEntityContainers()) {
				for (AssociationSet associationSet : entityContainer
						.getAssociationSets()) {
					if (equalsWithNamespace(associationSet.getAssociation(),
							association)) {
						for (End end : associationSet.getEnds()) {
							if (end.getRole().equalsIgnoreCase(toRole)) {
								return EdmxUtilities.getEntitySetByName(
										end.getEntitySet(), edmxObject);
							}
						}
					}
				}
			}
		}

		return null;
	}

	public static boolean equalsWithNamespace(String s1, String s2) {
		if (s1 == s2)
			return true;

		if (s1 != null && s2 != null && s1.equalsIgnoreCase(s2))
			return true;

		if (s1 == null || s2 == null)
			return false;

		String name1 = removeNamespace(s1);
		String name2 = removeNamespace(s2);

		if (name1.equalsIgnoreCase(name2))
			return true;

		return false;

	}

	/**
	 * Returns the entity set according to the given name and Edmx.
	 * 
	 * @param edmxEntitySetName
	 *            - entity set name.
	 * @param edmx
	 *            - the Edmx.
	 * @return - the entity set according to the given name and Edmx.
	 */
	public static org.eclipse.ogee.client.model.edmx.EntitySet getEntitySetByName(
			String edmxEntitySetName, Edmx edmx) {
		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		Schema[] schemas = edmxDataServices.getSchemas();

		for (Schema schema : schemas) {
			String schemaNamespace = schema.getNamespace();
			String schemaAlias = schema.getAlias();

			EntityContainer[] entityContainers = schema.getEntityContainers();
			for (EntityContainer entityContainer : entityContainers) {
				EntitySet[] entitySets = entityContainer.getEntitySets();
				for (EntitySet entitySet : entitySets) {
					if (compareWithNamespace(entitySet.getName(),
							edmxEntitySetName, schemaNamespace, schemaAlias)) {
						return entitySet;
					}
				}
			}
		}

		return null;
	}

	/**
	 * Extracts the service's name from the given Edmx.
	 * 
	 * @param edmxObject
	 *            - Edmx.
	 * @return - service name.
	 * @throws ParserException
	 */
	public static String extractServiceName(Edmx edmx) throws ParserException {
		String serviceName = null;
		try {
			Schema[] schemas = edmx.getEdmxDataServices().getSchemas();
			if (schemas.length == 1) {
				EntityContainer[] entityContainers = schemas[0]
						.getEntityContainers();
				if ((entityContainers == null)
						|| (entityContainers.length == 0)) {
					// no entity containers in this schema,
					// take the service name from the namespace
					serviceName = schemas[0].getNamespace();
				} else {
					serviceName = schemas[0].getEntityContainers()[0].getName();
				}
			} else {
				for (Schema schema : schemas) {
					EntityContainer[] entityContainers = schema
							.getEntityContainers();
					for (EntityContainer entityContainer : entityContainers) {
						serviceName = entityContainer.getName();
					}
				}
			}
		} catch (Exception e) {
			throw new ParserException(e);
		}

		if (serviceName == null)
			throw new ParserException(Messages.getString("EdmxUtilities.0")); //$NON-NLS-1$

		return serviceName;
	}

	/**
	 * Returns the given association's namespace.
	 * 
	 * @param associationName
	 *            - the association's name.
	 * @param edmx
	 *            - Edmx.
	 * @return - the association's namespace.
	 */
	public static String getAssociationNamespace(String associationName,
			Edmx edmx) {
		String namespace = "";

		if (associationName != null) {
			associationName = associationName.trim();
		}

		if (associationName == null || associationName.isEmpty()) {
			return namespace;
		}

		if (edmx == null) {
			return null;
		}

		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		if (edmxDataServices == null) {
			return null;
		}

		Schema[] schemas = edmxDataServices.getSchemas();

		int lastIndexOfDot = associationName.lastIndexOf(DOT);
		if (lastIndexOfDot == -1) {
			// search for the association in all schemas
			// and return the namespace of the schema in which
			// the association was found.

			String name = associationName.substring(lastIndexOfDot + 1,
					associationName.length());
			if (name == null || name.isEmpty()) {
				return null;
			}

			for (Schema schema : schemas) {
				namespace = schema.getNamespace();
				Association[] associations = schema.getAssociations();
				for (Association association : associations) {
					if (name.equals(association.getName())) {
						return namespace;
					}
				}
			}
		} else {
			String prefix = associationName.substring(0, lastIndexOfDot);
			namespace = compareWithSchemasNamespace(prefix, schemas, edmx);
		}

		return namespace;
	}

	/**
	 * Returns the given entity type's namespace.
	 * 
	 * @param entityTypeName
	 *            - the entity type.
	 * @param edmx
	 *            = Edmx.
	 * @return - the namespace of the given entity type, or null if the entity
	 *         comes from an edmx reference.
	 */
	public static String getEntityTypeNamespace(String entityTypeName, Edmx edmx) {
		String namespace = "";

		if (entityTypeName != null) {
			entityTypeName = entityTypeName.trim();
		}

		if (entityTypeName == null || entityTypeName.isEmpty() || edmx == null) {
			return null;
		}

		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		if (edmxDataServices == null) {
			return null;
		}

		if (entityTypeName.contains("(")) //$NON-NLS-1$
		{
			entityTypeName = entityTypeName.substring(entityTypeName
					.lastIndexOf("(") + 1, entityTypeName.length() - 1); //$NON-NLS-1$
		}

		Schema[] schemas = edmxDataServices.getSchemas();

		int lastIndexOfDot = entityTypeName.lastIndexOf(DOT);

		if (lastIndexOfDot == -1) {
			// search for the entity type in all schemas
			// and return the namespace of the schema in which
			// the entity type was found.
			String name = entityTypeName.substring(lastIndexOfDot + 1,
					entityTypeName.length());
			if (name == null || name.isEmpty()) {
				return null;
			}

			for (Schema schema : schemas) {
				namespace = schema.getNamespace();
				EntityType[] entityTypes = schema.getEntityTypes();
				for (EntityType entityType : entityTypes) {
					if (name.equals(entityType.getName())) {
						return namespace;
					}
				}
			}
		} else {
			String prefix = entityTypeName.substring(0, lastIndexOfDot);

			// check the namespace (prefix), if exists in the metadata, return
			// it.
			// else, it may come from a reference, then return null.
			namespace = compareWithSchemasNamespace(prefix, schemas, edmx);
		}

		return namespace; // may be null
	}

	/**
	 * Returns the given complex type's namespace.
	 * 
	 * @param complexTypeName
	 *            - complex type's name.
	 * @param edmx
	 *            - Edmx.
	 * @return - the complex type's namespace.
	 */
	public static String getComplexTypeNamespace(String complexTypeName,
			Edmx edmx) {
		String namespace = "";

		if (complexTypeName != null) {
			complexTypeName = complexTypeName.trim();
		}

		if (complexTypeName == null || complexTypeName.isEmpty()) {
			return null;
		}

		if (edmx == null) {
			return null;
		}

		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		if (edmxDataServices == null) {
			return null;
		}

		if (complexTypeName.contains("(")) //$NON-NLS-1$
		{
			complexTypeName = complexTypeName.substring(complexTypeName
					.lastIndexOf("(") + 1, complexTypeName.length() - 1); //$NON-NLS-1$
		}

		Schema[] schemas = edmxDataServices.getSchemas();

		int lastIndexOfDot = complexTypeName.lastIndexOf(DOT);
		if (lastIndexOfDot == -1) {
			// search for the complex type in all schemas
			// and return the namespace of the schema in which
			// the complex type was found.

			String name = complexTypeName.substring(lastIndexOfDot + 1,
					complexTypeName.length());
			if (name == null || name.isEmpty()) {
				return null;
			}

			for (Schema schema : schemas) {
				namespace = schema.getNamespace();
				ComplexType[] complexTypes = schema.getComplexTypes();
				for (ComplexType complexType : complexTypes) {
					if (name.equals(complexType.getName())) {
						return namespace;
					}
				}
			}
		} else {
			String prefix = complexTypeName.substring(0, lastIndexOfDot);
			namespace = compareWithSchemasNamespace(prefix, schemas, edmx);
		}

		return namespace;
	}

	/**
	 * Returns the given association set's namespace.
	 * 
	 * @param associationSetName
	 *            - association set.
	 * @param edmx
	 *            - Edmx.
	 * @return - the association set's namespace.
	 */
	public static String getAssociationSetNamespace(String associationSetName,
			Edmx edmx) {
		String namespace = "";

		if (associationSetName != null) {
			associationSetName = associationSetName.trim();
		}

		if (associationSetName == null || associationSetName.isEmpty()) {
			return null;
		}

		if (edmx == null) {
			return null;
		}

		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		if (edmxDataServices == null) {
			return null;
		}

		if (associationSetName.contains("(")) //$NON-NLS-1$
		{
			associationSetName = associationSetName
					.substring(
							associationSetName.lastIndexOf("(") + 1, associationSetName.length() - 1); //$NON-NLS-1$
		}

		Schema[] schemas = edmxDataServices.getSchemas();

		int lastIndexOfDot = associationSetName.lastIndexOf(DOT);
		if (lastIndexOfDot == -1) {
			// search for the association set in all schemas
			// and return the namespace of the schema in which
			// the association set was found.

			String name = associationSetName.substring(lastIndexOfDot + 1,
					associationSetName.length());
			if (name == null || name.isEmpty()) {
				return null;
			}

			for (Schema schema : schemas) {
				namespace = schema.getNamespace();
				EntityContainer[] entityContainers = schema
						.getEntityContainers();
				for (EntityContainer entityContainer : entityContainers) {
					AssociationSet[] associationSets = entityContainer
							.getAssociationSets();
					for (AssociationSet associationSet : associationSets) {
						if (name.equals(associationSet.getName())) {
							return namespace;
						}
					}
				}
			}
		} else {
			String prefix = associationSetName.substring(0, lastIndexOfDot);
			namespace = compareWithSchemasNamespace(prefix, schemas, edmx);
		}

		return namespace;
	}

	/**
	 * Returns the given navigation property's namespace.
	 * 
	 * @param edmxNavigationPropertyName
	 *            - navigation property.
	 * @param edmx
	 *            - Edmx.
	 * @return - the navigation property's namespace.
	 */
	public static String getNavigationPropertyNamespace(
			String edmxNavigationPropertyName, Edmx edmx) {
		String namespace = "";

		if (edmxNavigationPropertyName != null) {
			edmxNavigationPropertyName = edmxNavigationPropertyName.trim();
		}

		if (edmxNavigationPropertyName == null
				|| edmxNavigationPropertyName.isEmpty()) {
			return null;
		}

		if (edmx == null) {
			return null;
		}

		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		if (edmxDataServices == null) {
			return null;
		}

		if (edmxNavigationPropertyName.contains("(")) //$NON-NLS-1$
		{
			edmxNavigationPropertyName = edmxNavigationPropertyName
					.substring(
							edmxNavigationPropertyName.lastIndexOf("(") + 1, edmxNavigationPropertyName.length() - 1); //$NON-NLS-1$
		}

		Schema[] schemas = edmxDataServices.getSchemas();

		int lastIndexOfDot = edmxNavigationPropertyName.lastIndexOf(DOT);
		if (lastIndexOfDot == -1) {
			// search for the navigation property in all schemas
			// and return the namespace of the schema in which
			// the navigation property was found.

			String name = edmxNavigationPropertyName.substring(
					lastIndexOfDot + 1, edmxNavigationPropertyName.length());
			if (name == null || name.isEmpty()) {
				return null;
			}

			for (Schema schema : schemas) {
				namespace = schema.getNamespace();
				EntityType[] entityTypes = schema.getEntityTypes();
				for (EntityType entityType : entityTypes) {
					NavigationProperty[] navigationProperties = entityType
							.getNavigationProperties();
					for (NavigationProperty navigationProperty : navigationProperties) {
						if (name.equals(navigationProperty.getName())) {
							return namespace;
						}
					}
				}
			}
		} else {
			String prefix = edmxNavigationPropertyName.substring(0,
					lastIndexOfDot);
			namespace = compareWithSchemasNamespace(prefix, schemas, edmx);
		}

		return namespace;
	}

	private static String compareWithSchemasNamespace(String prefix,
			Schema[] schemas, Edmx edmx) {
		for (Schema schema : schemas) {
			String namespace = schema.getNamespace();

			if (prefix.equals(schema.getAlias()) || prefix.equals(namespace)) {
				return namespace;
			}
		}

		return null;
	}

	/**
	 * Checks whether the given property is a complex type property.
	 * 
	 * @param property
	 *            - property.
	 * @param edmx
	 *            - Edmx.
	 * @return true if property is a complex type property, false otherwise.
	 */
	public static boolean isComplexTypeProperty(
			org.eclipse.ogee.client.model.edmx.Property property, Edmx edmx) {
		String type = property.getType();
		for (Schema schema : edmx.getEdmxDataServices().getSchemas()) {
			String namespace = schema.getNamespace();
			if (type.startsWith(namespace + ".")) //$NON-NLS-1$
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks whether the given type represented as string, is a complex type.
	 * 
	 * @param type
	 *            type name
	 * @param edmx
	 *            - Edmx
	 * @return - true whether the given type represented as string, is a complex
	 *         type, false otherwise.
	 */
	public static boolean isComplexType(String type, Edmx edmx) {
		if (type == null) {
			return false;
		}

		if (type.contains("(")) //$NON-NLS-1$
		{
			type = type.substring(type.lastIndexOf(DOT) + 1, type.length() - 1);
		}

		Schema[] schemas = edmx.getEdmxDataServices().getSchemas();
		for (Schema schema : schemas) {
			String schemaNamespace = schema.getNamespace();
			String schemaAlias = schema.getAlias();

			ComplexType[] complexTypes = schema.getComplexTypes();
			for (ComplexType complexType : complexTypes) {
				if (compareWithNamespace(complexType.getName(), type,
						schemaNamespace, schemaAlias)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Returns the schema by the given namespace.
	 * 
	 * @param namespace
	 *            - namespace
	 * @param edmx
	 *            - Edmx.
	 * @return - the schema by the given namespace.
	 */
	public static Schema getSchema(String namespace, Edmx edmx) {
		if (namespace != null) {
			namespace = namespace.trim();
		}
		if (namespace == null || namespace.isEmpty()) {
			return null;
		}

		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		if (edmxDataServices == null) {
			return null;
		}

		Schema[] schemas = edmxDataServices.getSchemas();
		if (schemas == null) {
			return null;
		}

		for (Schema schema : schemas) {
			String schemaNamespace = schema.getNamespace();

			if (namespace.equals(schemaNamespace)) {
				return schema;
			}
		}

		return null;
	}

	private static BoolConstantExpression isOperationEnabledInAnnotations(
			EntitySet entitySet, List<AnnotationsV4> annotationsList,
			String termSuffix, String propertyName) {
		for (AnnotationsV4 annotations : annotationsList) {
			String target = annotations.getTarget();
			if (target != null && target.equals(entitySet.getName())) {
				List<Annotation> annotationlstInAnnotations = annotations
						.getAnnotations();
				if (annotationlstInAnnotations != null
						&& annotationlstInAnnotations.size() > 0) {
					BoolConstantExpression propValue = findBoolConstantExpressionPropertyValue(
							annotationlstInAnnotations, termSuffix,
							propertyName);
					if (propValue != null) {
						return propValue;
					}
				}
			}
		}

		return null;
	}

	private static BoolConstantExpression isOperationEnabledInEntitySetV4(
			EntitySet entitySet, Edmx edmx, String termSuffix,
			String propertyName) {
		List<Annotation> annotationList = ((EntitySetV4) entitySet)
				.getAnnotation();
		if (annotationList != null && annotationList.size() > 0) {
			BoolConstantExpression propValue = findBoolConstantExpressionPropertyValue(
					annotationList, termSuffix, propertyName);
			if (propValue != null) {
				return propValue;
			}
		} else
		// with annotations tag
		{
			Schema[] schemas = edmx.getEdmxDataServices().getSchemas();

			if (schemas != null && schemas.length > 0) {
				for (Schema schema : schemas) {
					List<AnnotationsV4> annotationsList = ((SchemaV4) schema)
							.getAnnotations();
					if (annotationsList != null && annotationsList.size() > 0) {
						return isOperationEnabledInAnnotations(entitySet,
								annotationsList, termSuffix, propertyName);
					}
				}
			}
		}

		return null;
	}

	/**
	 * Checks if the provided EntitySet allows adding new entities
	 */
	public static boolean isEntitySetCreatable(EntitySet entitySet, Edmx edmx) {
		if (entitySet instanceof EntitySetV4) {
			BoolConstantExpression isOperationEnabled = isOperationEnabledInEntitySetV4(
					entitySet, edmx, INSERT_RESTRICTIONS_TERM_SUFFIX,
					INSERTABLE);

			if (isOperationEnabled != null) {
				return isOperationEnabled.isValue();
			}
		}

		if (entitySet instanceof EntitySetV3) {
			ValueAnnotation[] annotations = ((EntitySetV3) entitySet)
					.getValueAnnotations();
			Bool creatablePropValue = findBoolPropertyValue(annotations,
					INSERT_RESTRICTIONS_TERM_SUFFIX, INSERTABLE);
			if (creatablePropValue != null) {
				return Boolean.parseBoolean(creatablePropValue.getValue());
			}
		}

		return true; // default value
	}

	/**
	 * Checks if the provided EntitySet allows updating existing entities
	 */
	public static boolean isEntitySetUpdatable(EntitySet entitySet, Edmx edmx) {
		if (entitySet instanceof EntitySetV4) {
			BoolConstantExpression isOperationEnabled = isOperationEnabledInEntitySetV4(
					entitySet, edmx, UPDATE_RESTRICTIONS_TERM_SUFFIX, UPDATABLE);

			if (isOperationEnabled != null) {
				return isOperationEnabled.isValue();
			}
		}

		if (entitySet instanceof EntitySetV3) {
			ValueAnnotation[] annotations = ((EntitySetV3) entitySet)
					.getValueAnnotations();
			Bool updatablePropValue = findBoolPropertyValue(annotations,
					UPDATE_RESTRICTIONS_TERM_SUFFIX, UPDATABLE);
			if (updatablePropValue != null) {
				return Boolean.parseBoolean(updatablePropValue.getValue());
			}
		}

		return true; // default value
	}

	/**
	 * Checks if the provided EntitySet allows deleting existing entities
	 */
	public static boolean isEntitySetDeletable(EntitySet entitySet, Edmx edmx) {
		if (entitySet instanceof EntitySetV4) {
			BoolConstantExpression isOperationEnabled = isOperationEnabledInEntitySetV4(
					entitySet, edmx, DELETE_RESTRICTIONS_TERM_SUFFIX, DELETABLE);

			if (isOperationEnabled != null) {
				return isOperationEnabled.isValue();
			}
		}

		if (entitySet instanceof EntitySetV3) {
			ValueAnnotation[] annotations = ((EntitySetV3) entitySet)
					.getValueAnnotations();
			Bool deletablePropValue = findBoolPropertyValue(annotations,
					DELETE_RESTRICTIONS_TERM_SUFFIX, DELETABLE);
			if (deletablePropValue != null) {
				return Boolean.parseBoolean(deletablePropValue.getValue());
			}
		}

		return true; // default value
	}

	/**
	 * Checks if the provided EntitySet can be directly queried as a top level
	 * set
	 */
	public static boolean isEntitySetAddressable(EntitySet entitySet, Edmx edmx) {
		if (entitySet instanceof EntitySetV4) {
			return ((EntitySetV4) entitySet).getIncludeInServiceDocument();
		}

		if (entitySet instanceof EntitySetV3) {
			ValueAnnotation[] annotations = ((EntitySetV3) entitySet)
					.getValueAnnotations();
			for (ValueAnnotation valueAnnotation : annotations) {
				if (valueAnnotation.getTerm().endsWith(QUERYABLE_TERM_SUFFIX)) {
					return Boolean.parseBoolean(valueAnnotation.getBool()
							.getValue());
				}
			}
		}

		return true; // default value
	}

	/**
	 * Checks if the provided EntitySet supports paging
	 */
	public static boolean isEntitySetPageable(EntitySet entitySet, Edmx edmx) {
		if (entitySet instanceof EntitySetV3) {
			ValueAnnotation[] annotations = ((EntitySetV3) entitySet)
					.getValueAnnotations();
			for (ValueAnnotation valueAnnotation : annotations) {
				if (valueAnnotation.getTerm().endsWith(
						CLIENT_PAGEABLE_TERM_SUFFIX)) {
					return Boolean.parseBoolean(valueAnnotation.getBool()
							.getValue());
				}
			}
		}

		return true; // default value
	}

	/**
	 * Checks if the provided EntitySet supports sorting
	 */
	public static boolean isEntitySetSortable(EntitySet entitySet, Edmx edmx) {

		if (entitySet instanceof EntitySetV4) {
			BoolConstantExpression isOperationEnabled = isOperationEnabledInEntitySetV4(
					entitySet, edmx, SORT_RESTRICTIONS_TERM_SUFFIX, SORTABLE);

			if (isOperationEnabled != null) {
				return isOperationEnabled.isValue();
			}
		}

		if (entitySet instanceof EntitySetV3) {
			ValueAnnotation[] annotations = ((EntitySetV3) entitySet)
					.getValueAnnotations();
			Bool sortablePropValue = findBoolPropertyValue(annotations,
					SORT_RESTRICTIONS_TERM_SUFFIX, SORTABLE);
			if (sortablePropValue != null) {
				return Boolean.parseBoolean(sortablePropValue.getValue());
			}
		}

		return true; // default value
	}

	/**
	 * Checks if the provided Property supports sorting
	 */
	public static boolean isPropertySortable(EntitySet entitySet,
			Property property, Edmx edmx) {
		if (entitySet instanceof EntitySetV4) {
			// Annotation is a child element of EntitySet
			if (isEntitySetSortable(entitySet, edmx)) {
				List<Annotation> annotationList = ((EntitySetV4) entitySet)
						.getAnnotation();
				if (annotationList != null && annotationList.size() > 0) {
					for (Annotation annotation : annotationList) {
						return isTermAppliedOnProperty(property, annotation,
								"UnsortableProperties");
					}
				} else
				// with annotations element
				{
					Schema[] schemas = edmx.getEdmxDataServices().getSchemas();

					if (schemas != null && schemas.length > 0) {
						for (Schema schema : schemas) {
							List<AnnotationsV4> annotationsList = ((SchemaV4) schema)
									.getAnnotations();
							if (annotationsList != null
									&& annotationsList.size() > 0) {
								for (AnnotationsV4 annotations : annotationsList) {
									String target = annotations.getTarget();
									if (target != null
											&& target.equals(entitySet
													.getName())) {
										List<Annotation> annotationlstInAnnotations = annotations
												.getAnnotations();
										if (annotationlstInAnnotations != null
												&& annotationlstInAnnotations
														.size() > 0) {
											Annotation[] annotationArr = annotationlstInAnnotations
													.toArray(new Annotation[annotationlstInAnnotations
															.size()]);

											for (Annotation annotation : annotationArr) {
												if (annotation.getTerm() != null
														&& annotation
																.getTerm()
																.endsWith(
																		SORT_RESTRICTIONS_TERM_SUFFIX)) {
													return isTermAppliedOnProperty(
															property,
															annotation,
															"UnsortableProperties");
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return false;
		}

		else if (entitySet instanceof EntitySetV3) {
			// IGWDT does not support V3 vocabularies
		}

		return true; // default value
	}

	/**
	 * Checks if the provided Property supports filtering
	 */
	public static boolean isPropertyFilterable(EntitySet entitySet,
			Property property, Edmx edmx) {
		if (entitySet instanceof EntitySetV4) {
			// Annotation is a child element of EntitySet
			if (isEntitySetFilterable(entitySet, edmx)) {
				List<Annotation> annotationList = ((EntitySetV4) entitySet)
						.getAnnotation();
				if (annotationList != null && annotationList.size() > 0) {
					for (Annotation annotation : annotationList) {
						return isTermAppliedOnProperty(property, annotation,
								"NonFilterableProperties");
					}
				} else
				// with annotations element
				{
					Schema[] schemas = edmx.getEdmxDataServices().getSchemas();

					if (schemas != null && schemas.length > 0) {
						for (Schema schema : schemas) {
							List<AnnotationsV4> annotationsList = ((SchemaV4) schema)
									.getAnnotations();
							if (annotationsList != null
									&& annotationsList.size() > 0) {
								for (AnnotationsV4 annotations : annotationsList) {
									String target = annotations.getTarget();
									if (target != null
											&& target.equals(entitySet
													.getName())) {
										List<Annotation> annotationlstInAnnotations = annotations
												.getAnnotations();
										if (annotationlstInAnnotations != null
												&& annotationlstInAnnotations
														.size() > 0) {
											Annotation[] annotationArr = annotationlstInAnnotations
													.toArray(new Annotation[annotationlstInAnnotations
															.size()]);

											for (Annotation annotation : annotationArr) {
												if (annotation.getTerm() != null
														&& annotation
																.getTerm()
																.endsWith(
																		FILTER_RESTRICTIONS_TERM_SUFFIX)) {
													return isTermAppliedOnProperty(
															property,
															annotation,
															"NonFilterableProperties");
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return false;
		}

		else if (entitySet instanceof EntitySetV3) {
			// IGWDT does not support V3 vocabularies
		}

		return true; // default value
	}

	private static boolean isEntitySetFilterable(EntitySet entitySet, Edmx edmx) {
		if (entitySet instanceof EntitySetV4) {
			BoolConstantExpression isOperationEnabled = isOperationEnabledInEntitySetV4(
					entitySet, edmx, FILTER_RESTRICTIONS_TERM_SUFFIX,
					FILTERABLE);

			if (isOperationEnabled != null) {
				return isOperationEnabled.isValue();
			}
		}

		if (entitySet instanceof EntitySetV3) {
			ValueAnnotation[] annotations = ((EntitySetV3) entitySet)
					.getValueAnnotations();
			Bool sortablePropValue = findBoolPropertyValue(annotations,
					FILTER_RESTRICTIONS_TERM_SUFFIX, FILTERABLE);
			if (sortablePropValue != null) {
				return Boolean.parseBoolean(sortablePropValue.getValue());
			}
		}

		return true; // default value
	}

	private static boolean isTermAppliedOnProperty(Property property,
			Annotation annotation, String term) {
		RecordExpression recordExpression = annotation.getRecord();
		List<org.eclipse.ogee.client.model.edm.v4.PropertyValue> propertyValues = recordExpression
				.getPropertyValues();
		for (org.eclipse.ogee.client.model.edm.v4.PropertyValue propertyValue : propertyValues) {
			if (propertyValue.getProperty().equals(term)) {
				CollectionExpression collection = propertyValue.getCollection();
				Object[] childExpressions = collection.getChildExpressions();
				for (Object child : childExpressions) {
					PropertyPathExpression path = (PropertyPathExpression) child;
					if (path.getValue().equalsIgnoreCase(property.getName())) {
						return false;
					}
				}

			}
		}
		return true;
	}

	private static Bool findBoolPropertyValue(ValueAnnotation[] annotations,
			String valueAnnotationTerm, String propertyValueName) {
		for (ValueAnnotation valueAnnotation : annotations) {
			if (valueAnnotation.getTerm().endsWith(valueAnnotationTerm)) {
				Record record = valueAnnotation.getRecord();
				PropertyValue[] propertyValues = record.getPropertyValues();
				for (PropertyValue propertyValue : propertyValues) {
					if (propertyValue.getProperty().equals(propertyValueName)) {
						return propertyValue.getBool();
					}
				}
			}
		}

		return null;
	}

	private static BoolConstantExpression findBoolConstantExpressionPropertyValue(
			List<Annotation> annotationlst, String annotationTerm,
			String propertyValueName) {
		Annotation[] annotationArr = annotationlst
				.toArray(new Annotation[annotationlst.size()]);

		for (Annotation annotation : annotationArr) {
			if (annotation.getTerm() != null
					&& annotation.getTerm().endsWith(annotationTerm)) {
				RecordExpression recordExpression = annotation.getRecord();
				List<org.eclipse.ogee.client.model.edm.v4.PropertyValue> propertyValues = recordExpression
						.getPropertyValues();
				for (org.eclipse.ogee.client.model.edm.v4.PropertyValue propertyValue : propertyValues) {
					if (propertyValue.getProperty().equals(propertyValueName)) {
						if (propertyValue.getAttribute("Bool") != null) {
							BoolConstantExpression boolConstantExpression = new BoolConstantExpression();
							boolConstantExpression
									.setValue(Boolean.valueOf(propertyValue
											.getAttribute("Bool")));

							return boolConstantExpression;
						} else if (propertyValue.getBool() != null) {
							return propertyValue.getBool();
						}
					}
				}
			}
		}

		return null;
	}	

}
