/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl.v3;

import org.eclipse.ogee.client.model.edmx.Association;
import org.eclipse.ogee.client.model.edmx.AssociationSet;
import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.Dependent;
import org.eclipse.ogee.client.model.edmx.Documentation;
import org.eclipse.ogee.client.model.edmx.EdmxDataServices;
import org.eclipse.ogee.client.model.edmx.End;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.Key;
import org.eclipse.ogee.client.model.edmx.LongDescription;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.client.model.edmx.Parameter;
import org.eclipse.ogee.client.model.edmx.Principal;
import org.eclipse.ogee.client.model.edmx.Property;
import org.eclipse.ogee.client.model.edmx.PropertyRef;
import org.eclipse.ogee.client.model.edmx.ReferentialConstraint;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.model.edmx.Summary;
import org.eclipse.ogee.client.model.edmx.Using;
import org.eclipse.ogee.client.model.edmx.v3.Annotations;
import org.eclipse.ogee.client.model.edmx.v3.AssociationSetV3;
import org.eclipse.ogee.client.model.edmx.v3.AssociationV3;
import org.eclipse.ogee.client.model.edmx.v3.ComplexTypeV3;
import org.eclipse.ogee.client.model.edmx.v3.EdmxV3;
import org.eclipse.ogee.client.model.edmx.v3.EntityContainerV3;
import org.eclipse.ogee.client.model.edmx.v3.EntitySetV3;
import org.eclipse.ogee.client.model.edmx.v3.EntityTypeV3;
import org.eclipse.ogee.client.model.edmx.v3.EnumType;
import org.eclipse.ogee.client.model.edmx.v3.FunctionImportReturnType;
import org.eclipse.ogee.client.model.edmx.v3.FunctionImportV3;
import org.eclipse.ogee.client.model.edmx.v3.Member;
import org.eclipse.ogee.client.model.edmx.v3.NavigationPropertyV3;
import org.eclipse.ogee.client.model.edmx.v3.ParameterV3;
import org.eclipse.ogee.client.model.edmx.v3.PropertyRefV3;
import org.eclipse.ogee.client.model.edmx.v3.PropertyV3;
import org.eclipse.ogee.client.model.edmx.v3.PropertyValue;
import org.eclipse.ogee.client.model.edmx.v3.Reference;
import org.eclipse.ogee.client.model.edmx.v3.SchemaV3;
import org.eclipse.ogee.client.model.edmx.v3.TypeAnnotation;
import org.eclipse.ogee.client.model.edmx.v3.ValueAnnotation;
import org.eclipse.ogee.client.model.edmx.v3.ValueTerm;
import org.eclipse.ogee.client.model.edmx.v3.expressions.Collection;
import org.eclipse.ogee.client.model.edmx.v3.expressions.Path;
import org.eclipse.ogee.client.model.edmx.v3.expressions.Record;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Binary;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTime;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTimeOffset;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Guid;
import org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int;
import org.eclipse.ogee.client.model.generic.Arrays;
import org.eclipse.ogee.client.model.generic.StringUtilities;
import org.eclipse.ogee.client.model.generic.v3.ODataConstantsV3;
import org.eclipse.ogee.client.parser.impl.TagFactory;
import org.eclipse.ogee.client.parser.impl.TagObject;
import org.xml.sax.Attributes;

public class EdmxTagFactoryV3 implements TagFactory {
	private EdmxV3 edmx;

	public TagObject createTagObject(String tagName, Attributes attr) {
		TagObject result = new TagObject();
		result.setTag(tagName);

		int attrCount = attr.getLength();

		if (tagName.equalsIgnoreCase(ODataConstantsV3.EDMX_EDMX)) {
			edmx = new EdmxV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(VERSION))
						edmx.setVersion(attr.getValue(i));
					else if (attrName.startsWith(XMLNS))
						edmx.getNamespaces().put(attrName, attrValue);
					else
						edmx.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(edmx);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.EDMX_REFERENCE)) {
			Reference reference = new Reference();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(ODataConstantsV3.URL))
						reference.setUrl(attr.getValue(i));
					else
						reference.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(reference);
		} else if (tagName.equalsIgnoreCase(EDMX_DATASERVICES)) {
			EdmxDataServices edmxDataServices = new EdmxDataServices();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(M_DATA_SERVICE_VERSION))
						edmxDataServices.setmDataServiceVersion(attr
								.getValue(i));
					else if (attrName.startsWith(XMLNS))
						edmxDataServices.getNamespaces().put(attrName,
								attrValue);
					else
						edmxDataServices.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(edmxDataServices);
		} else if (tagName.equalsIgnoreCase(SCHEMA)) {
			SchemaV3 schema = new SchemaV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAMESPACE))
						schema.setNamespace(attrValue);
					else if (attrName.equalsIgnoreCase(ALIAS))
						schema.setAlias(attrValue);
					else if (attrName.startsWith(XMLNS))
						schema.getNamespaces().put(attrName, attrValue);
					else
						schema.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(schema);
		} else if (tagName.equalsIgnoreCase(USING)) {
			Using using = new Using();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAMESPACE))
						using.setNamespace(attrValue);
					else if (attrName.equalsIgnoreCase(ALIAS))
						using.setAlias(attrValue);
					else
						using.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(using);
		} else if (tagName.equalsIgnoreCase(ENTITY_TYPE)) {
			EntityTypeV3 entityType = new EntityTypeV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						entityType.setName(attrValue);
					else if (attrName.equalsIgnoreCase(BASE_TYPE))
						entityType.setBaseType(attrValue);					
					else if (attrName.equalsIgnoreCase(M_HAS_STREAM))
						entityType.setMHasStream(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(ABSTRACT))
						entityType.setAbstract(StringUtilities
								.stringToBoolean(attrValue));					
					else
						entityType.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(entityType);
		} else if (tagName.equalsIgnoreCase(KEY)) {
			Key key = new Key();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					key.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(key);
		} else if (tagName.equalsIgnoreCase(PROPERTY_REF)) {
			PropertyRefV3 propertyRef = new PropertyRefV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						propertyRef.setName(attrValue);
					else
						propertyRef.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(propertyRef);
		} else if (tagName.equalsIgnoreCase(PROPERTY)) {
			PropertyV3 property = new PropertyV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);

					if (attrName.equalsIgnoreCase(NAME))
						property.setName(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						property.setType(attrValue);
					else if (attrName.equalsIgnoreCase(NULLABLE))
						property.setNullable(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase(MAX_LENGTH))
						property.setMaxLength(attrValue);					
					else if (attrName.equalsIgnoreCase(FIXED_LENGTH))
						property.setFixedLength(attrValue);
					else if (attrName.equalsIgnoreCase(UNICODE))
						property.setUnicode(attrValue);
					else if (attrName.equalsIgnoreCase(M_FC_TARGET_PATH))
						property.setmFC_TargetPath(attrValue);
					else if (attrName.equalsIgnoreCase(M_FC_KEEP_IN_CONTENT))
						property.setmFC_KeepInContent(StringUtilities
								.stringToBoolean(attrValue));
					else
						property.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(property);
		} else if (tagName.equalsIgnoreCase(NAVIGATION_PROPERTY)) {
			NavigationPropertyV3 navigationProperty = new NavigationPropertyV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						navigationProperty.setName(attrValue);
					else if (attrName.equalsIgnoreCase(RELATIONSHIP))
						navigationProperty.setRelationship(attrValue);
					else if (attrName.equalsIgnoreCase(FROM_ROLE))
						navigationProperty.setFromRole(attrValue);
					else if (attrName.equalsIgnoreCase(TO_ROLE))
						navigationProperty.setToRole(attrValue);
					else
						navigationProperty.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(navigationProperty);
		} else if (tagName.equalsIgnoreCase(ASSOCIATION)) {
			AssociationV3 association = new AssociationV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						association.setName(attrValue);					
					else
						association.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(association);
		} else if (tagName.equalsIgnoreCase(END)) {
			End end = new End();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(TYPE))
						end.setType(attrValue);
					else if (attrName.equalsIgnoreCase(MULTIPLICITY))
						end.setMultiplicity(attrValue);
					else if (attrName.equalsIgnoreCase(ROLE))
						end.setRole(attrValue);
					else if (attrName.equalsIgnoreCase(ENTITY_SET))
						end.setEntitySet(attrValue);
					else
						end.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(end);
		} else if (tagName.equalsIgnoreCase(REFERENTIAL_CONSTRAINT)) {
			ReferentialConstraint constraint = new ReferentialConstraint();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					constraint.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(constraint);
		} else if (tagName.equalsIgnoreCase(PRINCIPAL)) {
			Principal principal = new Principal();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(ROLE))
						principal.setRole(attrValue);
					else
						principal.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(principal);
		} else if (tagName.equalsIgnoreCase(DEPENDENT)) {
			Dependent dependent = new Dependent();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(ROLE))
						dependent.setRole(attrValue);
					else
						dependent.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(dependent);
		} else if (tagName.equalsIgnoreCase(COMPLEX_TYPE)) {
			ComplexTypeV3 complexType = new ComplexTypeV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						complexType.setName(attrValue);
					else if (attrName.equalsIgnoreCase(BASE_TYPE))
						complexType.setBaseType(attrValue);
					else if (attrName.equalsIgnoreCase(ABSTRACT))
						complexType.setAbstract(StringUtilities
								.stringToBoolean(attrValue));
					else
						complexType.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(complexType);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.ENUM_TYPE)) {
			EnumType enumType = new EnumType();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						enumType.setName(attrValue);
					else if (attrName
							.equalsIgnoreCase(ODataConstantsV3.UNDERLYING_TYPE))
						enumType.setUnderlyingType(attrValue);
					else
						enumType.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(enumType);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.MEMBER)) {
			Member member = new Member();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						member.setName(attr.getValue(i));
					else if (attrName.equalsIgnoreCase(ODataConstantsV3.VALUE))
						// CHECKSTYLE:OFF
						try {
							member.setValue(Long.parseLong(attrValue));
						} catch (NumberFormatException e) {
							// do nothing
						}
					// CHECKSTYLE:ON
					else
						member.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(member);
		} else if (tagName.equalsIgnoreCase(ENTITY_CONTAINER)) {
			EntityContainerV3 container = new EntityContainerV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						container.setName(attrValue);
					else if (attrName
							.equalsIgnoreCase(M_IS_DEFAULT_ENTITY_CONTAINER))
						container.setDefault(StringUtilities
								.stringToBoolean(attrValue));
					else
						container.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(container);
		} else if (tagName.equalsIgnoreCase(ENTITY_SET)) {
			EntitySetV3 entitySet = new EntitySetV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						entitySet.setName(attrValue);
					else if (attrName.equalsIgnoreCase(ENTITY_TYPE))
						entitySet.setEntityType(attrValue);					
					else
						entitySet.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(entitySet);
		} else if (tagName.equalsIgnoreCase(ASSOCIATION_SET)) {
			AssociationSetV3 associationSet = new AssociationSetV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						associationSet.setName(attrValue);
					else if (attrName.equalsIgnoreCase(ASSOCIATION))
						associationSet.setAssociation(attrValue);					
					else
						associationSet.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(associationSet);
		} else if (tagName.equalsIgnoreCase(FUNCTION_IMPORT)) {
			FunctionImportV3 functionImport = new FunctionImportV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						functionImport.setName(attrValue);
					else if (attrName.equalsIgnoreCase(RETURN_TYPE))
						functionImport.setReturnType(attrValue);
					else if (attrName.equalsIgnoreCase(M_HTTP_METHOD))
						functionImport.setmHttpMethod(attrValue);
					else if (attrName.equalsIgnoreCase(ENTITY_SET))
						functionImport.setEntitySet(attrValue);
					else if (attrName
							.equalsIgnoreCase(ODataConstantsV3.IS_SIDE_EFFECTING))
						functionImport.setIsSideEffecting(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName
							.equalsIgnoreCase(ODataConstantsV3.IS_BINDABLE))
						functionImport.setBindable(StringUtilities
								.stringToBoolean(attrValue));
					else
						functionImport.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(functionImport);
		} else if (tagName.equalsIgnoreCase(RETURN_TYPE)) {
			FunctionImportReturnType functionImportReturnType = new FunctionImportReturnType();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					functionImportReturnType.setAttribute(attrName, attrValue);
					if (attrName.equalsIgnoreCase("EntitySetPath"))
						functionImportReturnType.setEntitySetPath(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						functionImportReturnType.setType(attrValue);
					else if (attrName.equalsIgnoreCase(MAX_LENGTH))
						functionImportReturnType.setMaxLength(attrValue);
					else if (attrName.equalsIgnoreCase(NULLABLE))
						functionImportReturnType.setNullable(StringUtilities
								.stringToBoolean(attrValue));
					else if (attrName.equalsIgnoreCase("Precision"))
						functionImportReturnType.setPrecision(attrValue);
					else if (attrName.equalsIgnoreCase("Scale"))
						functionImportReturnType.setScale(attrValue);
					else if (attrName.equalsIgnoreCase("SRID"))
						functionImportReturnType.setSRID(attrValue);
					else if (attrName.equalsIgnoreCase(UNICODE))
						functionImportReturnType.setUnicode(attrValue);
				}
			}
			result.setObject(functionImportReturnType);
		} else if (tagName.equalsIgnoreCase(PARAMETER)) {
			ParameterV3 parameter = new ParameterV3();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						parameter.setName(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						parameter.setType(attrValue);
					else if (attrName.equalsIgnoreCase(MODE))
						parameter.setMode(attrValue);
					else if (attrName.equalsIgnoreCase(MAX_LENGTH))
						parameter.setMaxLength(attrValue);
					else
						parameter.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(parameter);
		} else if (tagName.equalsIgnoreCase(DOCUMENTATION)) {
			Documentation documentation = new Documentation();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					documentation.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(documentation);
		} else if (tagName.equalsIgnoreCase(LONG_DESCRIPTION)) {
			LongDescription longDescription = new LongDescription();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					longDescription.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(longDescription);
		} else if (tagName.equalsIgnoreCase(DOCUMENTATION_SUMMARY)) {
			Summary summary = new Summary();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					summary.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(summary);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.VALUE_TERM)) {
			ValueTerm valueTerm = new ValueTerm();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(NAME))
						valueTerm.setName(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						valueTerm.setType(attrValue);
					else
						valueTerm.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(valueTerm);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.ANNOTATIONS)) {
			Annotations annotations = new Annotations();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(ODataConstantsV3.TARGET))
						annotations.setTarget(attrValue);
					else
						annotations.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(annotations);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
			ValueAnnotation valueAnnotation = new ValueAnnotation();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(ODataConstantsV3.TERM))
						valueAnnotation.setTerm(attrValue);
					else
						valueAnnotation.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(valueAnnotation);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
			TypeAnnotation typeAnnotation = new TypeAnnotation();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(ODataConstantsV3.TERM))
						typeAnnotation.setTerm(attrValue);
					else
						typeAnnotation.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(typeAnnotation);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.PROPERTY_VALUE)) {
			PropertyValue propertyValue = new PropertyValue();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(ODataConstantsV3.PROPERTY))
						propertyValue.setProperty(attrValue);
					else
						propertyValue.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(propertyValue);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
			Record record = new Record();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					record.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(record);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.COLLECTION_EXPRESSION)) {
			Collection collection = new Collection();

			result.setObject(collection);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
			Path path = new Path();

			result.setObject(path);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String String = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String();

			result.setObject(String);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int Int = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int();

			result.setObject(Int);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float Float = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float();

			result.setObject(Float);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal Decimal = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal();

			result.setObject(Decimal);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool Bool = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool();

			result.setObject(Bool);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.DATETIME_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTime DateTime = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTime();

			result.setObject(DateTime);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTimeOffset DateTimeOffset = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTimeOffset();

			result.setObject(DateTimeOffset);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Guid Guid = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Guid();

			result.setObject(Guid);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Binary Binary = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Binary();

			result.setObject(Binary);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BYTE_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte Byte = new org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte();

			result.setObject(Byte);
		} else {
			if (edmx != null) {
				edmx.addNotSupported(tagName);
			}
		}

		return result;
	}

	public void setValue(TagObject tagObject, String value) {
		String tagName = tagObject.getTag();
		if (tagName.equalsIgnoreCase(LONG_DESCRIPTION)) {
			LongDescription longDescription = (LongDescription) tagObject
					.getObject();
			longDescription.setValue(value);
		} else if (tagName.equalsIgnoreCase(DOCUMENTATION_SUMMARY)) {
			Summary summary = (Summary) tagObject.getObject();
			summary.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
			Path path = (Path) tagObject.getObject();
			path.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String string = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String) tagObject
					.getObject();
			string.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
			Int Int = (Int) tagObject.getObject();
			Int.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float Float = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float) tagObject
					.getObject();
			Float.setValue(value);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal Decimal = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal) tagObject
					.getObject();
			Decimal.setValue(value);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.DATETIME_EXPRESSION)) {
			DateTime DateTime = (DateTime) tagObject.getObject();
			DateTime.setValue(value);
		} else if (tagName
				.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
			DateTimeOffset DateTimeOffset = (DateTimeOffset) tagObject
					.getObject();
			DateTimeOffset.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
			Guid Guid = (Guid) tagObject.getObject();
			Guid.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
			Binary Binary = (Binary) tagObject.getObject();
			Binary.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BYTE_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte Byte = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte) tagObject
					.getObject();
			Byte.setValue(value);
		} else if (tagName.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
			org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool bool = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool) tagObject
					.getObject();
			bool.setValue(value);
		}

	}

	public void addChild(TagObject parent, TagObject tagObject) {
		String tag = tagObject.getTag();
		String parentTag = parent.getTag();

		if (parentTag.equalsIgnoreCase(EDMX_EDMX)) {
			EdmxV3 edmx = (EdmxV3) parent.getObject();

			if (tag.equalsIgnoreCase(EDMX_DATASERVICES)) {
				EdmxDataServices edmxDataServices = (EdmxDataServices) tagObject
						.getObject();
				edmx.setEdmxDataServices(edmxDataServices);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.EDMX_REFERENCE)) {
				Reference reference = (Reference) tagObject.getObject();
				Reference[] references = edmx.getReferences();
				Arrays.add(edmx, references, reference);
			}
		} else if (parentTag.equalsIgnoreCase(EDMX_DATASERVICES)) {
			EdmxDataServices dataServices = (EdmxDataServices) parent
					.getObject();
			Object o = tagObject.getObject();

			if (tag.equalsIgnoreCase(SCHEMA)) {
				Schema[] schemas = dataServices.getSchemas();
				Schema schema = (Schema) o;
				Arrays.add(dataServices, schemas, schema);
			}
		} else if (parentTag.equalsIgnoreCase(SCHEMA)) {
			SchemaV3 schema = (SchemaV3) parent.getObject();

			if (tag.equalsIgnoreCase(ENTITY_TYPE)) {
				EntityType[] entityTypes = schema.getEntityTypes();
				EntityTypeV3 entityType = (EntityTypeV3) tagObject.getObject();
				Arrays.add(schema, entityTypes, entityType);
			} else if (tag.equalsIgnoreCase(USING)) {
				Using[] usings = schema.getUsings();
				Using using = (Using) tagObject.getObject();
				Arrays.add(schema, usings, using);
			} else if (tag.equalsIgnoreCase(COMPLEX_TYPE)) {
				ComplexType[] complexTypes = schema.getComplexTypes();
				ComplexTypeV3 complexType = (ComplexTypeV3) tagObject
						.getObject();
				Arrays.add(schema, complexTypes, complexType);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.ENUM_TYPE)) {
				EnumType[] enumTypes = schema.getEnumTypes();
				EnumType enumType = (EnumType) tagObject.getObject();
				Arrays.add(schema, enumTypes, enumType);
			} else if (tag.equalsIgnoreCase(ASSOCIATION)) {
				Association[] associations = schema.getAssociations();
				AssociationV3 association = (AssociationV3) tagObject
						.getObject();
				Arrays.add(schema, associations, association);
			} else if (tag.equalsIgnoreCase(ENTITY_CONTAINER)) {
				EntityContainer[] entityContainers = schema
						.getEntityContainers();
				EntityContainerV3 entityContainer = (EntityContainerV3) tagObject
						.getObject();
				Arrays.add(schema, entityContainers, entityContainer);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				schema.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_TERM)) {
				ValueTerm[] valueTerms = schema.getValueTerms();
				ValueTerm valueTerm = (ValueTerm) tagObject.getObject();
				Arrays.add(schema, valueTerms, valueTerm);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.ANNOTATIONS)) {
				Annotations[] annotationsArray = schema.getAnnotations();
				Annotations annotations = (Annotations) tagObject.getObject();
				Arrays.add(schema, annotationsArray, annotations);
			}
		} else if (parentTag.equalsIgnoreCase(ENTITY_TYPE)) {
			EntityTypeV3 entityType = (EntityTypeV3) parent.getObject();
			if (tag.equalsIgnoreCase(KEY)) {
				Key key = (Key) tagObject.getObject();
				entityType.setKey(key);
			} else if (tag.equalsIgnoreCase(PROPERTY)) {
				Property[] properties = entityType.getProperties();
				PropertyV3 property = (PropertyV3) tagObject.getObject();
				Arrays.add(entityType, properties, property);
			} else if (tag.equalsIgnoreCase(NAVIGATION_PROPERTY)) {
				NavigationProperty[] navigationProperties = entityType
						.getNavigationProperties();
				NavigationPropertyV3 navigationProperty = (NavigationPropertyV3) tagObject
						.getObject();
				Arrays.add(entityType, navigationProperties, navigationProperty);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				entityType.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = entityType
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(entityType, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = entityType
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(entityType, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(COMPLEX_TYPE)) {
			ComplexTypeV3 complexType = (ComplexTypeV3) parent.getObject();
			if (tag.equalsIgnoreCase(PROPERTY)) {
				Property[] properties = complexType.getProperties();
				Property property = (Property) tagObject.getObject();
				Arrays.add(complexType, properties, property);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				complexType.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = complexType
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(complexType, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = complexType
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(complexType, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(ASSOCIATION)) {
			AssociationV3 association = (AssociationV3) parent.getObject();
			if (tag.equalsIgnoreCase(END)) {
				End[] ends = association.getEnds();
				End end = (End) tagObject.getObject();
				Arrays.add(association, ends, end);
			} else if (tag.equalsIgnoreCase(REFERENTIAL_CONSTRAINT)) {
				ReferentialConstraint constraint = (ReferentialConstraint) tagObject
						.getObject();
				association.setReferentialConstraint(constraint);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				association.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = association
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(association, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = association
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(association, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(ENTITY_CONTAINER)) {
			EntityContainerV3 container = (EntityContainerV3) parent
					.getObject();
			if (tag.equalsIgnoreCase(ENTITY_SET)) {
				EntitySet entitySet = (EntitySet) tagObject.getObject();
				EntitySet[] entitySets = container.getEntitySets();
				Arrays.add(container, entitySets, entitySet);
			} else if (tag.equalsIgnoreCase(ASSOCIATION_SET)) {
				AssociationSet associationSet = (AssociationSet) tagObject
						.getObject();
				AssociationSet[] associationSets = container
						.getAssociationSets();
				Arrays.add(container, associationSets, associationSet);
			} else if (tag.equalsIgnoreCase(FUNCTION_IMPORT)) {
				FunctionImportV3 functionImport = (FunctionImportV3) tagObject
						.getObject();
				FunctionImport[] functionImports = container
						.getFunctionImports();
				Arrays.add(container, functionImports, functionImport);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				container.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = container
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(container, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = container
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(container, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(REFERENTIAL_CONSTRAINT)) {
			ReferentialConstraint constraint = (ReferentialConstraint) parent
					.getObject();
			if (tag.equalsIgnoreCase(PRINCIPAL)) {
				Principal principal = (Principal) tagObject.getObject();
				constraint.setPrincipal(principal);
			} else if (tag.equalsIgnoreCase(DEPENDENT)) {
				Dependent dependent = (Dependent) tagObject.getObject();
				constraint.setDependent(dependent);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				constraint.setDocumentation(documentation);
			}
		} else if (parentTag.equalsIgnoreCase(KEY)) {
			Key key = (Key) parent.getObject();
			if (tag.equalsIgnoreCase(PROPERTY_REF)) {
				PropertyRef propertyRef = (PropertyRef) tagObject.getObject();
				PropertyRef[] propertyRefs = key.getPropertyRefs();
				Arrays.add(key, propertyRefs, propertyRef);
			}
		} else if (parentTag.equalsIgnoreCase(PRINCIPAL)) {
			Principal principal = (Principal) parent.getObject();
			if (tag.equalsIgnoreCase(PROPERTY_REF)) {
				PropertyRef propertyRef = (PropertyRef) tagObject.getObject();
				PropertyRef[] propertyRefs = principal.getPropertyRefs();
				Arrays.add(principal, propertyRefs, propertyRef);
			}
		} else if (parentTag.equalsIgnoreCase(DEPENDENT)) {
			Dependent dependent = (Dependent) parent.getObject();
			if (tag.equalsIgnoreCase(PROPERTY_REF)) {
				PropertyRef propertyRef = (PropertyRef) tagObject.getObject();
				PropertyRef[] propertyRefs = dependent.getPropertyRefs();
				Arrays.add(dependent, propertyRefs, propertyRef);
			}
		} else if (parentTag.equalsIgnoreCase(ASSOCIATION_SET)) {
			AssociationSetV3 associationSet = (AssociationSetV3) parent
					.getObject();
			if (tag.equalsIgnoreCase(END)) {
				End end = (End) tagObject.getObject();
				End[] ends = associationSet.getEnds();
				Arrays.add(associationSet, ends, end);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				associationSet.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = associationSet
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(associationSet, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = associationSet
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(associationSet, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(ENTITY_SET)) {
			EntitySetV3 entitySet = (EntitySetV3) parent.getObject();
			if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				entitySet.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = entitySet
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(entitySet, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = entitySet
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(entitySet, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(FUNCTION_IMPORT)) {
			FunctionImportV3 functionImport = (FunctionImportV3) parent
					.getObject();
			if (tag.equalsIgnoreCase(PARAMETER)) {
				Parameter parameter = (Parameter) tagObject.getObject();
				Parameter[] parameters = functionImport.getParameters();
				Arrays.add(functionImport, parameters, parameter);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				functionImport.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = functionImport
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(functionImport, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = functionImport
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(functionImport, valueAnnotations, valueAnnotation);
			} else if (tag.equalsIgnoreCase(RETURN_TYPE)) {
				FunctionImportReturnType functionImportReturnType = (FunctionImportReturnType) tagObject
						.getObject();
				functionImport.setReturnTypeElement(functionImportReturnType);
			}
		} else if (parentTag.equalsIgnoreCase(PARAMETER)) {
			ParameterV3 parameter = (ParameterV3) parent.getObject();
			if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				parameter.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = parameter
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(parameter, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = parameter
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(parameter, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(PROPERTY)) {
			PropertyV3 property = (PropertyV3) parent.getObject();
			if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				property.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = property
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(property, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = property
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(property, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(NAVIGATION_PROPERTY)) {
			NavigationPropertyV3 navigationProperty = (NavigationPropertyV3) parent
					.getObject();
			if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				navigationProperty.setDocumentation(documentation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation[] typeAnnotations = navigationProperty
						.getTypeAnnotations();
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				Arrays.add(navigationProperty, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation[] valueAnnotations = navigationProperty
						.getValueAnnotations();
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				Arrays.add(navigationProperty, valueAnnotations,
						valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(END)) {
			End end = (End) parent.getObject();
			if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				end.setDocumentation(documentation);
			}
		} else if (parentTag.equalsIgnoreCase(USING)) {
			Using using = (Using) parent.getObject();
			if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				using.setDocumentation(documentation);
			}
		} else if (parentTag.equalsIgnoreCase(DOCUMENTATION)) {
			Documentation documentation = (Documentation) parent.getObject();
			if (tag.equalsIgnoreCase(LONG_DESCRIPTION)) {
				LongDescription longDescription = (LongDescription) tagObject
						.getObject();
				documentation.setLongDescription(longDescription);
			} else if (tag.equalsIgnoreCase(DOCUMENTATION_SUMMARY)) {
				Summary summary = (Summary) tagObject.getObject();
				documentation.setSummary(summary);
			}
		} else if (parentTag.equalsIgnoreCase(ODataConstantsV3.ENUM_TYPE)) {
			EnumType enumType = (EnumType) parent.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.MEMBER)) {
				Member member = (Member) tagObject.getObject();
				Member[] members = enumType.getMembers();
				Arrays.add(enumType, members, member);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				TypeAnnotation[] typeAnnotations = enumType
						.getTypeAnnotations();
				Arrays.add(enumType, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				ValueAnnotation[] valueAnnotations = enumType
						.getValueAnnotations();
				Arrays.add(enumType, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(PROPERTY_REF)) {
			PropertyRefV3 propertyRefV3 = (PropertyRefV3) parent.getObject();
			if (tag.equalsIgnoreCase(DOCUMENTATION)) {
				Documentation documentation = (Documentation) tagObject
						.getObject();
				propertyRefV3.setDocumentation(documentation);
			}
		} else if (parentTag.equalsIgnoreCase(ODataConstantsV3.ANNOTATIONS)) {
			Annotations annotations = (Annotations) parent.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				ValueAnnotation[] valueAnnotations = annotations
						.getValueAnnotations();
				Arrays.add(annotations, valueAnnotations, valueAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				TypeAnnotation[] typeAnnotations = annotations
						.getTypeAnnotations();
				Arrays.add(annotations, typeAnnotations, typeAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
			TypeAnnotation typeAnnotation = (TypeAnnotation) parent.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.PROPERTY_VALUE)) {
				PropertyValue propertyValue = (PropertyValue) tagObject
						.getObject();
				PropertyValue[] propertyValues = typeAnnotation
						.getPropertyValues();
				Arrays.add(typeAnnotation, propertyValues, propertyValue);
			}
		} else if (parentTag
				.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
			Record record = (Record) parent.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.PROPERTY_VALUE)) {
				PropertyValue propertyValue = (PropertyValue) tagObject
						.getObject();
				PropertyValue[] propertyValues = record.getPropertyValues();
				Arrays.add(record, propertyValues, propertyValue);
			}
		} else if (parentTag.equalsIgnoreCase(ODataConstantsV3.VALUE_TERM)) {
			ValueTerm valueTerm = (ValueTerm) parent.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.TYPE_ANNOTATION)) {
				TypeAnnotation typeAnnotation = (TypeAnnotation) tagObject
						.getObject();
				TypeAnnotation[] typeAnnotations = valueTerm
						.getTypeAnnotations();
				Arrays.add(valueTerm, typeAnnotations, typeAnnotation);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
				ValueAnnotation valueAnnotation = (ValueAnnotation) tagObject
						.getObject();
				ValueAnnotation[] valueAnnotations = valueTerm
						.getValueAnnotations();
				Arrays.add(valueTerm, valueAnnotations, valueAnnotation);
			}
		} else if (parentTag.equalsIgnoreCase(ODataConstantsV3.VALUE_ANNOTAION)) {
			ValueAnnotation valueAnnotation = (ValueAnnotation) parent
					.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
				Record record = (Record) tagObject.getObject();
				valueAnnotation.setRecord(record);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.COLLECTION_EXPRESSION)) {
				Collection collection = (Collection) tagObject.getObject();
				valueAnnotation.setCollection(collection);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
				Path path = (Path) tagObject.getObject();
				valueAnnotation.setPath(path);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String primitiveExpr = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String) tagObject
						.getObject();
				valueAnnotation.setString(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int primitiveExpr = (Int) tagObject
						.getObject();
				valueAnnotation.setInt(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float primitiveExpr = (Float) tagObject
						.getObject();
				valueAnnotation.setFloat(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal primitiveExpr = (Decimal) tagObject
						.getObject();
				valueAnnotation.setDecimal(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool primitiveExpr = (Bool) tagObject
						.getObject();
				valueAnnotation.setBool(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIME_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTime primitiveExpr = (DateTime) tagObject
						.getObject();
				valueAnnotation.setDateTime(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTimeOffset primitiveExpr = (DateTimeOffset) tagObject
						.getObject();
				valueAnnotation.setDateTimeOffset(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Guid primitiveExpr = (Guid) tagObject
						.getObject();
				valueAnnotation.setGuid(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Binary primitiveExpr = (Binary) tagObject
						.getObject();
				valueAnnotation.setBinary(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BYTE_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte primitiveExpr = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte) tagObject
						.getObject();
				valueAnnotation.setByte(primitiveExpr);
			}
		} else if (parentTag.equalsIgnoreCase(ODataConstantsV3.PROPERTY_VALUE)) {
			PropertyValue propertyValue = (PropertyValue) parent.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
				Record record = (Record) tagObject.getObject();
				propertyValue.setRecord(record);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.COLLECTION_EXPRESSION)) {
				Collection collection = (Collection) tagObject.getObject();
				propertyValue.setCollection(collection);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
				Path path = (Path) tagObject.getObject();
				propertyValue.setPath(path);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String primitiveExpr = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String) tagObject
						.getObject();
				propertyValue.setString(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int primitiveExpr = (Int) tagObject
						.getObject();
				propertyValue.setInt(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float primitiveExpr = (Float) tagObject
						.getObject();
				propertyValue.setFloat(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal primitiveExpr = (Decimal) tagObject
						.getObject();
				propertyValue.setDecimal(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool primitiveExpr = (Bool) tagObject
						.getObject();
				propertyValue.setBool(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIME_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTime primitiveExpr = (DateTime) tagObject
						.getObject();
				propertyValue.setDateTime(primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTimeOffset primitiveExpr = (DateTimeOffset) tagObject
						.getObject();
				propertyValue.setDateTimeOffset(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Guid primitiveExpr = (Guid) tagObject
						.getObject();
				propertyValue.setGuid(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Binary primitiveExpr = (Binary) tagObject
						.getObject();
				propertyValue.setBinary(primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BYTE_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte primitiveExpr = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte) tagObject
						.getObject();
				propertyValue.setByte(primitiveExpr);
			}
		} else if (parentTag
				.equalsIgnoreCase(ODataConstantsV3.COLLECTION_EXPRESSION)) {
			Collection collection = (Collection) parent.getObject();
			if (tag.equalsIgnoreCase(ODataConstantsV3.RECORD_EXPRESSION)) {
				Record record = (Record) tagObject.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, record);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.STRING_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String primitiveExpr = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.INT_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int primitiveExpr = (Int) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.FLOAT_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float primitiveExpr = (Float) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DECIMAL_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal primitiveExpr = (Decimal) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BOOL_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool primitiveExpr = (Bool) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIME_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTime primitiveExpr = (DateTime) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag
					.equalsIgnoreCase(ODataConstantsV3.DATETIMEOFFSET_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTimeOffset primitiveExpr = (DateTimeOffset) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.GUID_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Guid primitiveExpr = (Guid) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BINARY_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Binary primitiveExpr = (Binary) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, primitiveExpr);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.PATH_EXPRESSION)) {
				Path path = (Path) tagObject.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, path);
			} else if (tag.equalsIgnoreCase(ODataConstantsV3.BYTE_EXPRESSION)) {
				org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte Byte = (org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte) tagObject
						.getObject();
				Object[] childExpressions = collection.getChildExpressions();
				Arrays.add(collection, childExpressions, Byte);
			}
		}
	}
}
