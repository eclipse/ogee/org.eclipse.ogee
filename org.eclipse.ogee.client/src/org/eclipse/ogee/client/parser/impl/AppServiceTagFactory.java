/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl;

import org.eclipse.ogee.client.model.atom.AppCollection;
import org.eclipse.ogee.client.model.atom.AppService;
import org.eclipse.ogee.client.model.atom.AppWorkspace;
import org.eclipse.ogee.client.model.atom.AtomLink;
import org.eclipse.ogee.client.model.generic.Arrays;
import org.xml.sax.Attributes;

/**
 * Auxiliary class for parsing an OData service document. Not to be used
 * explicitly.
 * 
 */
public class AppServiceTagFactory implements TagFactory {

	public TagObject createTagObject(String tagName, Attributes attr) {
		TagObject result = new TagObject();
		result.setTag(tagName);

		int attrCount = attr.getLength();

		if (tagName.equalsIgnoreCase(SERVICE)) {
			AppService appService = new AppService();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(XML_BASE))
						appService.setXmlBase(attrValue);
					else if (attrName.startsWith(XMLNS))
						appService.getNamespaces().put(attrName, attrValue);
					else
						appService.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(appService);

		} else if (tagName.equalsIgnoreCase(WORKSPACE)) {
			AppWorkspace appWorkspace = new AppWorkspace();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);					
					appWorkspace.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(appWorkspace);
		} else if (tagName.equalsIgnoreCase(COLLECTION)) {
			AppCollection appCollection = new AppCollection();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(HREF))
						appCollection.setHref(attrValue);
					else
						appCollection.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(appCollection);
		} else if (tagName.equalsIgnoreCase(LINK)) {
			AtomLink link = new AtomLink();
			if (attrCount > 0) {
				String attrName, attrValue;
				for (int i = 0; i < attrCount; i++) {
					attrName = attr.getQName(i);
					attrValue = attr.getValue(i);
					if (attrName.equalsIgnoreCase(HREF))
						link.setHref(attrValue);
					else if (attrName.equalsIgnoreCase(REL))
						link.setRel(attrValue);
					else if (attrName.equalsIgnoreCase(TYPE))
						link.setType(attrValue);
					else if (attrName.equalsIgnoreCase(TITLE))
						link.setTitle(attrValue);
					else
						link.setAttribute(attrName, attrValue);
				}
			}
			result.setObject(link);
		}
		return result;
	}

	public void setValue(TagObject tagObject, String value) {
		if (tagObject.getObject() == null && value != null)
			tagObject.setObject(value);
	}

	public void addChild(TagObject parent, TagObject tagObject) {

		String tag = tagObject.getTag();

		String parentTag = parent.getTag();

		if (parentTag.equalsIgnoreCase(SERVICE)) {
			AppService appService = (AppService) parent.getObject();

			if (tag.equalsIgnoreCase(WORKSPACE)) {
				AppWorkspace appWorkspace = (AppWorkspace) tagObject
						.getObject();
				AppWorkspace[] appWorkspaces = appService.getAppWorkspaces();
				Arrays.add(appService, appWorkspaces, appWorkspace);
			}
		} else if (parentTag.equalsIgnoreCase(WORKSPACE)) {
			AppWorkspace appWorkspace = (AppWorkspace) parent.getObject();

			if (tag.equalsIgnoreCase(TITLE_TAG)) {
				appWorkspace.setAtomTitle(tagObject.getObject().toString());
			} else if (tag.equalsIgnoreCase(COLLECTION)) {
				AppCollection appCollection = (AppCollection) tagObject
						.getObject();
				AppCollection[] appCollections = appWorkspace.getCollections();
				Arrays.add(appWorkspace, appCollections, appCollection);
			}
		} else if (parentTag.equalsIgnoreCase(COLLECTION)) {
			AppCollection appCollection = (AppCollection) parent.getObject();

			if (tag.equalsIgnoreCase(TITLE_TAG)) {
				appCollection.setAtomTitle(tagObject.getObject().toString());
			} else if (tag.equalsIgnoreCase(LINK)) {
				AtomLink[] atomLinks = appCollection.getLinks();
				AtomLink link = (AtomLink) tagObject.getObject();
				Arrays.add(appCollection, atomLinks, link);
			}
		}

	}

}
