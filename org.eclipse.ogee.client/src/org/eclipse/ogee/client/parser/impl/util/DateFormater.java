/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl.util;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateFormater extends java.text.DateFormat {
	//required for serialization and de-serialization
	private static final long serialVersionUID = 1L;
	
	
	//compiled pattern to match date time according to rfc3339
	private static volatile Pattern patterndateTimePattern ;
	private Calendar calendar;
	private static volatile DecimalFormat decimalFormatFour = new DecimalFormat("0000"); //$NON-NLS-1$
	private static volatile DecimalFormat decimalFormatTwo = new DecimalFormat("00"); //$NON-NLS-1$
	

	//constructors for DateFormater
	public DateFormater(){
		this(new SimpleTimeZone(0, "Z"));
	}
	
	public DateFormater(Date date) {
		this(date, new SimpleTimeZone(0, "Z"));
	}
	
	public DateFormater(Date date, TimeZone zone) {
		calendar = new GregorianCalendar(zone);
		calendar.setTime(date);
	}
	
	public DateFormater(TimeZone zone) {
		calendar = new GregorianCalendar(zone);
	}
	

	@Override
	public StringBuffer format(Date date, StringBuffer toAppendTo,
			FieldPosition fieldPosition) {
		return toAppendTo.append(new DateFormater(date));
	}

	@Override
	public Date parse(String source, ParsePosition pos) {
		Calendar calendar = new GregorianCalendar();
		return parseDateTimeZone(source,calendar);
	}
	
	//private function to create the string pattern  for date time according to rfc3339
	private String createRegexString(){
		String regexDate = "([0-9]{4})-([0-9]{2})-([0-9]{2})"; //$NON-NLS-1$
	    String regexTime = "([0-9]{2}):([0-9]{2}):([0-9]{2})(\\.[0-9]+)?"; //$NON-NLS-1$
	    String regexZone = "(?:([zZ])|(?:(\\+|\\-)([0-9]{2}):([0-9]{2})))"; //$NON-NLS-1$
		String regex = regexDate + "[tT\\s]" + regexTime +regexZone; //$NON-NLS-1$
		return regex;
	}
	
	
	public static String toString(Calendar calendar) {
		StringBuilder buffer = new StringBuilder();
		buffer.append(decimalFormatFour.format(calendar.get(Calendar.YEAR)));
		buffer.append("-"); //$NON-NLS-1$
		buffer.append(decimalFormatTwo.format(calendar.get(Calendar.MONTH) + 1));
		buffer.append("-"); //$NON-NLS-1$
		buffer.append(decimalFormatTwo.format(calendar.get(Calendar.DAY_OF_MONTH)));
		buffer.append("T"); //$NON-NLS-1$
		buffer.append(decimalFormatTwo.format(calendar.get(Calendar.HOUR_OF_DAY)));
		buffer.append(":"); //$NON-NLS-1$
		buffer.append(decimalFormatTwo.format(calendar.get(Calendar.MINUTE)));
		buffer.append(":"); //$NON-NLS-1$
		buffer.append(decimalFormatTwo.format(calendar.get(Calendar.SECOND)));

		int milliSeconds = calendar.get(Calendar.MILLISECOND);
		if (milliSeconds != 0) {
			buffer.append(".").append((int) (milliSeconds / 10F)); //$NON-NLS-1$
		}

		int timeZoneMins = (calendar.get(Calendar.ZONE_OFFSET) + calendar
				.get(Calendar.DST_OFFSET)) / 60000;
		if (timeZoneMins == 0) {
			buffer.append("Z"); //$NON-NLS-1$
		} else {
			if (timeZoneMins < 0) {
				timeZoneMins = -timeZoneMins;
				buffer.append("-"); //$NON-NLS-1$
			} else {
				buffer.append("+"); //$NON-NLS-1$
			}
			int timeZoneHours = timeZoneMins / 60;
			timeZoneMins -= timeZoneHours * 60;
			buffer.append(decimalFormatTwo.format(timeZoneHours));
			buffer.append(":"); //$NON-NLS-1$
			buffer.append(decimalFormatTwo.format(timeZoneMins));
		}
		return buffer.toString();
	}

	
	/**
	 * parse a given string in rfc3339 format which takes care of colon in time zone offset
	 * @param source - the date string 
	 * @return Date
	 */
	private Date parseDateTimeZone(String source ,  Calendar calendar) {
		
		patterndateTimePattern = Pattern.compile(createRegexString());
		Matcher matcher = patterndateTimePattern.matcher(source);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("Invalid date/time: " + source); //$NON-NLS-1$
		}
		calendar.clear();
		calendar.set(Calendar.YEAR, Integer.parseInt(matcher.group(1)));
		calendar.set(Calendar.MONTH, Integer.parseInt(matcher.group(2)) - 1);
		calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(matcher.group(3)));
		calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(matcher.group(4)));
		calendar.set(Calendar.MINUTE, Integer.parseInt(matcher.group(5)));
		calendar.set(Calendar.SECOND, Integer.parseInt(matcher.group(6)));
		if (matcher.group(7) != null) {
			float fraction = Float.parseFloat(matcher.group(7));
			calendar.set(Calendar.MILLISECOND, (int) (fraction * 1000F));
		}
		if (matcher.group(8) == null) {
			int sign = matcher.group(9).equals("-") ? -1 : 1; //$NON-NLS-1$
			int timeZoneHours;
			timeZoneHours = Integer.parseInt(matcher.group(10));
			int timeZoneMins = Integer.parseInt(matcher.group(11));
			int offset = sign * ((timeZoneHours * 60) + timeZoneMins);
			String id = Integer.toString(offset);
			calendar.setTimeZone(new SimpleTimeZone(offset * 60000, id));
		} else {
			calendar.setTimeZone(new SimpleTimeZone(0, "Z")); //$NON-NLS-1$
		}
		return calendar.getTime();
	}

	public Date getDate() {
		return calendar.getTime();
	}
	
	@Override
	public Date parse(String source) throws ParseException {
		return parse(source, (ParsePosition) null);
	}
	
	@Override
	public String toString() {
		return toString(calendar);
	}

}
