/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.impl;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.ogee.client.exceptions.TypeConversionException;
import org.eclipse.ogee.client.model.generic.ODataProperty;
import org.eclipse.ogee.client.parser.Representation;
import org.eclipse.ogee.client.parser.impl.util.DateUtils;

/**
 * Handles convention of OData Primitive Data Types to Java Objects and vice
 * versa.
 * 
 * http://www.odata.org/documentation/overview#AbstractTypeSystem
 * 
 * 
 */
public class TypeConverter {
	private static final String L = "L"; //$NON-NLS-1$

	private static final String EMPTY_STRING = ""; //$NON-NLS-1$

	private static final String DOT = "."; //$NON-NLS-1$

	private static Logger log = Logger.getAnonymousLogger();

	private Representation representation = Representation.ATOM;

	public static final TypeConverter JSON = new TypeConverter(
			Representation.JSON);
	public static final TypeConverter ATOM = new TypeConverter(
			Representation.ATOM);

	public static TypeConverter getInstance(Representation representation) {
		switch (representation) {
		case JSON:
			return TypeConverter.JSON;
		case ATOM:
			return TypeConverter.ATOM;
		default:
			return TypeConverter.ATOM;
		}
	}

	/**
	 * @param representation
	 */
	private TypeConverter(Representation representation) {
		this.representation = representation;
	}

	/**
	 * Converts ODataProperty object to Concrete Java Object according to
	 * edmType
	 * 
	 * @param property
	 *            - the ODataProperty value
	 * @param edmType
	 *            - the EDM type from metadata
	 * @return Java typed object
	 * @throws TypeConversionException
	 */
	public Object convertToConcreteObject(ODataProperty property, String edmType)
			throws TypeConversionException {
		Object res = null;
		switch (EdmTypes.toEdmTypes(edmType)) {
		case Boolean:
			res = this.getAsBoolean(property);
			break;
		case Byte:
			res = this.getAsString(property);
			break;
		case Double:
			res = this.getAsDouble(property);
			break;
		case DateTime:
			res = this.getAsDate(property);
			break;
		case Time:
			res = this.getAsString(property);
			break;
		case Int32:
			res = this.getAsInt(property);
			break;
		case String:
			res = this.getAsString(property);
			break;
		case Single:
			res = this.getAsFloat(property);
			break;
		case Decimal:
			res = this.getAsFloat(property);
			break;
		case Int16:
			res = this.getAsInt(property);
			break;
		case Int64:
			res = this.getAsLong(property);
			break;
		case Binary:
			res = this.getAsString(property);
			break;
		case SByte:
			res = this.getAsInt(property);
			break;
		case Guid:
			res = this.getAsUUID(property);
			break;
		case DateTimeOffset:
			res = this.getAsDate(property);
			break;
		default:
			if (edmType == null) {
				res = this.getAsString(property);
				return res;
			}
		}
		return res;
	}

	/**
	 * Returns the java type equivalent to EDM type as string.
	 * 
	 * @param type
	 *            - the EDM type from the metadata
	 * @return
	 * @throws TypeConversionException
	 */
	public String typeConvert(String type) throws TypeConversionException {
		switch (EdmTypes.toEdmTypes(type)) {
		case Boolean:
			type = JavaVariableTypeConstants.BOOLEAN;
			break;
		case Byte:
			type = JavaVariableTypeConstants.STRING;
			break;
		case Double:
			type = JavaVariableTypeConstants.DOUBLE;
			break;
		case DateTime:
			type = JavaVariableTypeConstants.DATE;
			break;
		case Time:
			type = JavaVariableTypeConstants.STRING;
			break;
		case Int32:
			type = JavaVariableTypeConstants.INTEGER;
			break;
		case String:
			type = JavaVariableTypeConstants.STRING;
			break;
		case Single:
			type = JavaVariableTypeConstants.FLOAT;
			break;
		case Decimal:
			type = JavaVariableTypeConstants.FLOAT;
			break;
		case Int16:
			type = JavaVariableTypeConstants.INTEGER;
			break;
		case Int64:
			type = JavaVariableTypeConstants.LONG;
			break;
		case Binary:
			type = JavaVariableTypeConstants.STRING;
			break;
		case SByte:
			type = JavaVariableTypeConstants.INT;
			break;
		case Guid:
			type = JavaVariableTypeConstants.UUID;
			break;
		case DateTimeOffset:
			type = JavaVariableTypeConstants.DATE;
			break;
		default:
			// This must be Complex Type
			if (type.contains(DOT)) {
				type = type.substring(type.indexOf(DOT) + 1);
				break;
			} else {
				throw new TypeConversionException(
						"Unknown EDM Type found : " + type); // TODO: //$NON-NLS-1$
																// Move
																// to
																// messages
			}
		}
		return type;
	}

	/**
	 * Converts from Java Object to String representation according to EDM type
	 * 
	 * @param edmxType
	 * @param value
	 * @return
	 */
	public String convertToString(String edmxType, Object value) {
		return convertToString(edmxType, value, this.representation);
	}

	/**
	 * Converts from Java Object to String representation according to EDM type
	 * and Representation
	 * 
	 * @param Representation
	 *            atom or json
	 * @param edmxType
	 * @param value
	 * @return
	 */
	public String convertToString(String edmxType, Object value,
			Representation representation) {
		String res = EMPTY_STRING;
		switch (EdmTypes.toEdmTypes(edmxType)) {
		case Boolean:
			res = value.toString();
			break;
		case Byte:
			res = value.toString();
			break;
		case Double:
			res = value.toString();
			break;
		case DateTime:
			if (representation.equals(Representation.ATOM)) {
				res = DateUtils.format((Date) value);
			} else if (representation.equals(Representation.JSON)) {
				res = DateUtils.formatToJsonDate((Date) value);
			}
			break;
		case DateTimeOffset:
			if (representation.equals(Representation.ATOM)) {
				res = DateUtils.formatToDateTimeOffset((Date) value);
			} else if (representation.equals(Representation.JSON)) {
				res = DateUtils.formatToJsonDateTimeOffset((Date) value);
			}
			break;
		case Time:
			res = value.toString();
			break;
		case Int32:
			res = value.toString();
			break;
		case String:
			res = value.toString();
			break;
		case Single:
			res = Float.toString((Float) value);
			break;
		case Decimal:
			res = Float.toString((Float) value);
			break;
		case Int16:
			res = value.toString();
			break;
		case Int64:
			res = value.toString();
			break;
		case Binary:
			res = value.toString();
			break;
		case SByte:
			res = value.toString();
			break;
		case Guid:
			res = value.toString();
			break;
		default:
			res = null;
			break;
		}
		return res;

	}

	/**
	 * Converts ODataProperty value to Float
	 * 
	 * @param property
	 * @return Float value
	 */
	public Float getAsFloat(ODataProperty property) {
		if (property == null || property.getValue() == null
				|| property.getValue().equalsIgnoreCase(EMPTY_STRING)) {
			return null;
		}
		return Float.valueOf((String) property.getValue());
	}

	/**
	 * Converts ODataProperty value to long
	 * 
	 * @param property
	 * @return Long value
	 */
	public Long getAsLong(ODataProperty property) {
		if (property == null || property.getValue() == null
				|| property.getValue().equalsIgnoreCase(EMPTY_STRING)) {
			return null;
		}

		if (property.getValue().contains(L)) {
			return Long.valueOf(property.getValue().replace(L, EMPTY_STRING));
		}

		return Long.valueOf(property.getValue());
	}

	/**
	 * Converts ODataProperty value to String
	 * 
	 * @param property
	 * @return String value
	 */
	public String getAsString(ODataProperty property) {
		if (property == null || property.getValue() == null) {
			return null;
		}
		return new String(property.getValue());
	}

	/**
	 * Converts ODataProperty value to Date
	 * 
	 * @param property
	 * @return Date value
	 */
	public Date getAsDate(ODataProperty property) {
		Date res = null;
		if (property == null || property.getValue() == null
				|| property.getValue().equals(EMPTY_STRING)) {
			return null;
		}

		try {
			return converteToDate(representation, property.getValue());
		} catch (Exception e) {
			/*
			 * Either illegal format or null, do not throw exception here.
			 */
			log.logp(Level.SEVERE,
					"TypeConverter", "getAsDate", e.getLocalizedMessage(), e); //$NON-NLS-1$//$NON-NLS-2$
		}
		return res;
	}

	/**
	 * Converts ODataProperty value to Double
	 * 
	 * @param property
	 * @return Double value
	 */
	public Double getAsDouble(ODataProperty property) {
		if (property == null || property.getValue() == null
				|| property.getValue().equalsIgnoreCase(EMPTY_STRING)) {
			return null;
		}

		return Double.valueOf(property.getValue());
	}

	/**
	 * Converts ODataProperty value to Boolean
	 * 
	 * @param property
	 * @return Boolean value
	 */
	public Boolean getAsBoolean(ODataProperty property) {
		if (property == null || property.getValue() == null
				|| property.getValue().equalsIgnoreCase(EMPTY_STRING)) {
			return null;
		}

		if (Boolean.getBoolean(property.getValue()) == Boolean.TRUE) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	/**
	 * Converts ODataProperty value to Integer
	 * 
	 * @param property
	 * @return Integer value
	 */
	public Integer getAsInt(ODataProperty property) {
		if (property == null || property.getValue() == null
				|| property.getValue().equalsIgnoreCase(EMPTY_STRING)) {
			return null;
		}

		return Integer.valueOf(property.getValue());
	}

	/**
	 * Converts ODataProperty to UUID
	 * 
	 * @param property
	 * @return UUID value
	 */
	public UUID getAsUUID(ODataProperty property) {
		if (property == null || property.getValue() == null
				|| property.getValue().equalsIgnoreCase(EMPTY_STRING)) {
			return null;
		}

		return java.util.UUID.fromString(property.getValue());
	}

	/**
	 * private factory method for Atom or JSON Date conversion.
	 * 
	 * @param representation
	 * @param date
	 * @return Date value
	 * @throws ParseException
	 */
	private Date converteToDate(Representation representation, String date)
			throws ParseException {
		switch (representation) {
		case JSON:
			return DateUtils.parsJsonDate(date);

		case ATOM:
			return DateUtils.parse(date);

		default:
			return DateUtils.parse(date);
		}
	}

	public Representation getRepresentation() {
		return representation;
	}

	public void setRepresentation(Representation representation) {
		this.representation = representation;
	}

}
