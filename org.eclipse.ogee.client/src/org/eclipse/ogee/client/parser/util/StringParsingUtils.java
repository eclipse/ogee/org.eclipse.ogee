/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.parser.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

/**
 * A utility class for parsing a URL string.
 */
public class StringParsingUtils {
	private static final String QUESTION_MARK_SIGN = "?"; //$NON-NLS-1$
	private static final String DOLLAR_SIGN = "$"; //$NON-NLS-1$
	private static final String SLASH_SUFFIX = "/"; //$NON-NLS-1$	
	private static final String SDATA = "/sdata/"; //$NON-NLS-1$

	/**
	 * Returns the compatibility level of SDATA
	 * 
	 * @param collectionUrl
	 * @return compatibility level of SDATA
	 */
	public static String getCompatabilityLevel(String collectionUrl) {
		if (collectionUrl.contains(SDATA)) {
			return "0.2"; //$NON-NLS-1$
		} else {
			return "1.0"; //$NON-NLS-1$
		}
	}

	/**
	 * Extracts the name of the service from the given URL
	 * 
	 * @param currentSelectedUrl
	 *            URL string
	 * @return the name of the service
	 */
	public static String extractServiceName(String currentSelectedUrl) {
		if (currentSelectedUrl.contains(DOLLAR_SIGN)) {
			currentSelectedUrl = currentSelectedUrl.substring(0,
					currentSelectedUrl.indexOf(DOLLAR_SIGN) - 1);
		}
		if (currentSelectedUrl.contains(QUESTION_MARK_SIGN)) {
			currentSelectedUrl = currentSelectedUrl.substring(0,
					currentSelectedUrl.indexOf(QUESTION_MARK_SIGN) - 1);
		}

		URL url;

		try {
			url = new URL(currentSelectedUrl);
		} catch (MalformedURLException e) {
			return null;
		}

		String path = url.getPath();
		List<String> list = Arrays.asList(path.split("/")); //$NON-NLS-1$
		int lsize = list.size();
		return list.get(lsize - 1);
	}

	/**
	 * Removes the parameters from the given URL
	 * 
	 * @param inputUri
	 *            URL string
	 * @return - the URL without the parameters.
	 */
	public static String removeURLParameters(String inputUri) {
		if (inputUri.contains(DOLLAR_SIGN)) {
			inputUri = inputUri.substring(0, inputUri.indexOf(DOLLAR_SIGN) - 1);
		}

		if (inputUri.contains(QUESTION_MARK_SIGN)) {
			inputUri = inputUri.substring(0,
					inputUri.indexOf(QUESTION_MARK_SIGN));
		}

		if (!inputUri.endsWith(SLASH_SUFFIX)) {
			inputUri = inputUri + SLASH_SUFFIX;
		}

		return inputUri;
	}
}
