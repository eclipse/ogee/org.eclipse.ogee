/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

/**
 * Represents constants to be used during connectivity.
 */
public class ConnectivityConstants {
	public static final String SET_COOKIE = "set-cookie"; //$NON-NLS-1$
	public static final String COOKIE = "Cookie"; //$NON-NLS-1$
	public static final String CONTENT_LENGTH = "Content-Length"; //$NON-NLS-1$
	public static final String CONTENT_TYPE = "content-type"; //$NON-NLS-1$

	public static final String FETCH = "Fetch"; //$NON-NLS-1$
	public static final String CATALOGSERVICE = "CATALOGSERVICE"; //$NON-NLS-1$
	public static final String REQUIRED = "Required"; //$NON-NLS-1$
	public static final String X_CSRF_TOKEN = "x-csrf-token"; //$NON-NLS-1$
	
	public static final String BASIC = "Basic "; //$NON-NLS-1$
	public static final String AUTHORIZATION = "Authorization"; //$NON-NLS-1$

	public static final String XML_HTTP_REQUEST = "XMLHttpRequest"; //$NON-NLS-1$
	public static final String X_REQUESTED_WITH = "X-Requested-With"; //$NON-NLS-1$
}
