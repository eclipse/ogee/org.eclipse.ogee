/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

import java.util.HashMap;

import org.eclipse.ogee.client.activator.Activator;
import org.eclipse.ogee.client.connectivity.api.IServerConnectionParameters;
import org.eclipse.ogee.client.util.TraceLogger;

/**
 * Represents the parameters of a server connection.
 */
public class ServerConnectionParameters extends HashMap<String, Object>
		implements IServerConnectionParameters {
	private static final long serialVersionUID = 8771629746353770346L;

	private static final String STORE_PASSWORD = "storePassword"; //$NON-NLS-1$
	private static final TraceLogger traceLogger = TraceLogger
			.getLogger(Activator.PLUGIN_ID);
	private String host;
	private String port;

	/**
	 * Constructor
	 * 
	 * @param host
	 * @param port
	 */
	public ServerConnectionParameters(String host, String port) {
		super();
		this.host = host;
		this.port = port;

		this.put(USE_SSL, false);
		this.put(STORE_PASSWORD, false);
	}

	@Override
	public String getHost() {
		return this.host;
	}

	@Override
	public String getPort() {
		return this.port;
	}

	/**
	 * Two configurations are considered to be duplicate in case their host
	 * name, port number and client are equal (with case ignored)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!obj.getClass().equals(this.getClass())) {
			return false;
		}

		final ServerConnectionParameters other = (ServerConnectionParameters) obj;

		String otherHost = other.getHost();
		String host = getHost();
		traceLogger.trace("OtherHost  is:   %1$s    Host is:  %2$s", otherHost,
				host);
		if ((host != null && !host.equalsIgnoreCase(otherHost))
				|| (otherHost != null && !otherHost.equalsIgnoreCase(host))) {
			return false;
		}

		String otherPort = other.getPort();
		String port = getPort();
		traceLogger.trace("ODataPort  is:   %1$s    Port is:  %2$s", otherPort,
				port);
		if ((port != null && !port.equalsIgnoreCase(otherPort))
				|| (otherPort != null && !otherPort.equalsIgnoreCase(port))) {
			return false;
		}

		traceLogger.trace("otherkeyset  is:   %1$s    ketset is:  %2$s",
				other.keySet(), keySet());
		if (keySet().equals(other.keySet())
				&& entrySet().equals(other.entrySet())) {
			return true;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int hostHashCode = (this.host == null ? 0 : this.host.hashCode());
		int portHashCode = (this.port == null ? 0 : this.port.hashCode());
		int hash = hostHashCode + portHashCode + super.hashCode();
		traceLogger.trace("HashCode  is:   %1$s", hash);
		return hash;
	}
}
