/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestRequest.Method;
import org.eclipse.ogee.client.exceptions.RestClientException;
import org.eclipse.ogee.client.nls.messages.Messages;

/**
 * A ChangeSet is an atomic unit of work that is made up of an unordered group
 * of one or more of the insert, update or delete operations. ChangeSets cannot
 * contain read requests and cannot be nested (i.e. a ChangeSet cannot contain a
 * ChangeSet).
 */
public class ChangeSet {
	private static final String BINARY = "binary"; //$NON-NLS-1$
	private static final String CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding"; //$NON-NLS-1$
	private static final String APPLICATION_HTTP = "application/http"; //$NON-NLS-1$
	private static final String CONTENT_TYPE = "Content-Type"; //$NON-NLS-1$

	private final static Logger LOGGER = Logger.getLogger(ChangeSet.class
			.getName());

	// the list of requests of the ChangeSet
	private List<IRestRequest> requests;

	private Headers headers;

	/**
	 * Constructor
	 */
	public ChangeSet() {
		this.requests = new ArrayList<IRestRequest>();
		headers = new Headers();
		headers.put(CONTENT_TYPE, APPLICATION_HTTP);
		headers.put(CONTENT_TRANSFER_ENCODING, BINARY);
	}

	/**
	 * Constructs a new ChangeSet with the given list of requests.
	 * 
	 * @param requests
	 * @throws RestClientException
	 *             - if one or more of the requests holds the GET method.
	 */
	public ChangeSet(List<IRestRequest> requests) throws RestClientException {
		for (IRestRequest iRestRequest : requests) {
			if (iRestRequest.getMethod().equals(Method.GET)) {
				LOGGER.severe(Method.GET.toString()
						+ Messages.getString("ChangeSet.0")); //$NON-NLS-1$
				throw new RestClientException(Messages.getString("ChangeSet.1")); //$NON-NLS-1$
			}
		}

		this.requests = requests;
	}

	/**
	 * Adds a request to the change set
	 * 
	 * @param request
	 *            IRestRequest that its http method is other than GET
	 * @throws RestClientException
	 *             if the request is of type http GET
	 */
	public void addRequest(IRestRequest request) throws RestClientException {
		if (request.getMethod().equals(Method.GET)) {
			LOGGER.severe(Method.GET.toString()
					+ Messages.getString("ChangeSet.2")); //$NON-NLS-1$
			throw new RestClientException(Messages.getString("ChangeSet.3")); //$NON-NLS-1$
		}

		this.requests.add(request);
	}

	/**
	 * Sets the given list of requests to the change set
	 * 
	 * @param requests
	 *            list of requests
	 * @throws RestClientException
	 *             if one of the requests is of type http GET
	 */
	public void setRequests(List<IRestRequest> requests)
			throws RestClientException {
		for (IRestRequest iRestRequest : requests) {
			if (iRestRequest.getMethod().equals(Method.GET)) {
				LOGGER.severe(Method.GET.toString()
						+ Messages.getString("ChangeSet.4")); //$NON-NLS-1$
				throw new RestClientException(Messages.getString("ChangeSet.5")); //$NON-NLS-1$
			}
		}

		this.requests = requests;
	}

	/**
	 * @return the requests
	 */
	public List<IRestRequest> getRequests() {
		return requests;
	}

	/**
	 * @return the headers
	 */
	public Headers getHeaders() {
		return headers;
	}
}