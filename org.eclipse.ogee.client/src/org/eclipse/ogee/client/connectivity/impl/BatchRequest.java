/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.exceptions.RestClientException;
import org.eclipse.ogee.client.nls.messages.Messages;
import org.eclipse.ogee.client.util.Util;

/**
 * An OData Batch Request is represented as a Multipart MIME v1.0 message
 * [RFC2046], a standard format allowing the representation of multiple parts,
 * each of which may have a different content type, within a single request.
 */
public class BatchRequest extends RestRequest {
	private static final String $BATCH = "$batch"; //$NON-NLS-1$
	private static final String LINE_SEPARATOR = "line.separator"; //$NON-NLS-1$
	private static final String CHANGESET2 = "--changeset_"; //$NON-NLS-1$
	private static final String MULTIPART_MIXED_BOUNDARY_CHANGESET = "multipart/mixed; boundary=changeset_"; //$NON-NLS-1$
	private static final String CONTENT_LENGTH2 = "Content-Length: "; //$NON-NLS-1$
	private static final String CONTENT_LENGTH = "Content-Length"; //$NON-NLS-1$
	private static final String CONTENT_TRANSFER_ENCODING2 = "Content-Transfer-Encoding: "; //$NON-NLS-1$
	private static final String CONTENT_TYPE2 = "Content-Type"; //$NON-NLS-1$
	private static final String CONTENT_TYPE = "Content-Type: "; //$NON-NLS-1$
	private static final String BATCH = "--batch_"; //$NON-NLS-1$
	private static final String APPLICATION_HTTP = "application/http"; //$NON-NLS-1$
	private static final String BINARY = "binary"; //$NON-NLS-1$
	private static final String CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding"; //$NON-NLS-1$
	private static final String MULTIPART_MIXED_BOUNDARY_BATCH = "multipart/mixed;boundary=batch_"; //$NON-NLS-1$

	private final static Logger LOGGER = Logger.getLogger(BatchRequest.class
			.getName());

	// a generated UUID
	private String boundry;

	// Possible values: IRestRequest (read), ChangeSet
	private List<Object> operations;

	/**
	 * Constructor
	 * 
	 * @param url
	 *            Service url suffixed with /$batch, e.g. OData/OData.svc/$batch
	 * @throws RestClientException
	 */
	public BatchRequest(String url) {
		super(url);

		if (!url.contains($BATCH)) {
			LOGGER.warning(Messages.getString("BatchRequest.0") + url + Messages.getString("BatchRequest.2")); //$NON-NLS-1$//$NON-NLS-2$
			char lastChar = url.charAt(url.length() - 1);
			if (lastChar == '/') {
				url = url + "$batch"; //$NON-NLS-1$
			} else {
				url = url + "/$batch"; //$NON-NLS-1$
			}
		}

		this.boundry = Util.generateBoundary();
		this.operations = new ArrayList<Object>();
		// a Batch request is always a POST request
		this.setMethod(Method.POST);
		// and always has the same content-type
		this.setContentType(MULTIPART_MIXED_BOUNDARY_BATCH + boundry);
	}

	/**
	 * Adds a read request to the batch request.
	 * 
	 * @param request
	 *            - the read request to be added.
	 * @throws RestClientException
	 */
	public void addReadOperation(IRestRequest request)
			throws RestClientException {
		Method requestMethod = request.getMethod();

		if (requestMethod != Method.GET) {
			LOGGER.severe(requestMethod.toString()
					+ Messages.getString("BatchRequest.5")); //$NON-NLS-1$
			throw new RestClientException(Messages.getString("BatchRequest.6")); //$NON-NLS-1$
		}

		Headers headers = new Headers();
		headers.put(CONTENT_TRANSFER_ENCODING, BINARY);
		request.setHeaders(headers);
		request.setContentType(APPLICATION_HTTP);

		this.operations.add(request);
	}

	/**
	 * Adds a ChangeSet to the batch request.
	 * 
	 * @param requests
	 *            - list of one or more of the insert, update or delete
	 *            operations, that constructs a ChangeSet.
	 * @throws RestClientException
	 */
	public void addChangeSet(List<IRestRequest> requests)
			throws RestClientException {
		ChangeSet changeSet = new ChangeSet(requests);
		addChangeSet(changeSet);
	}

	/**
	 * Adds a ChangeSet to the batch request.
	 * 
	 * @param changeSet
	 *            - the ChangeSet to be added.
	 */
	public void addChangeSet(ChangeSet changeSet) {
		this.operations.add(changeSet);
	}

	/**
	 * Builds and returns the body of the Batch request as a String object.
	 */
	@Override
	public String getBody() {
		StringBuffer result = new StringBuffer();

		// append the starting boundary
		result.append(BATCH + boundry + System.getProperty(LINE_SEPARATOR));

		// going over all requests of the Batch
		for (Object request : operations) {
			if (request instanceof IRestRequest) // READ request
			{
				result = handleReadRequest(result, request);
			} else if (request instanceof ChangeSet) // ChangeSet
			{
				result = handleChangeSetRequest(result, request);
			}
			// append closing boundary
			result.append(BATCH + boundry + System.getProperty(LINE_SEPARATOR));
		}
		return result.toString();
	}

	/**
	 * Handle the build of the body of a ChangeSet request.
	 * 
	 * @param result
	 * @param request
	 * @return result
	 * @throws IOException
	 * @throws RestClientException
	 */
	private StringBuffer handleChangeSetRequest(StringBuffer result,
			Object request) {
		String changeSetBoundry = Util.generateBoundary();
		result.append(CONTENT_TYPE + MULTIPART_MIXED_BOUNDARY_CHANGESET
				+ changeSetBoundry + System.getProperty(LINE_SEPARATOR)
				+ System.getProperty(LINE_SEPARATOR));

		ChangeSet changeSet = (ChangeSet) request;
		Headers changeSetHeaders = changeSet.getHeaders();

		Method changeSetRequestMethod;
		// get all the requests of the ChangeSet
		List<IRestRequest> requests = changeSet.getRequests();
		int size = requests.size();

		result.append(CHANGESET2 + changeSetBoundry
				+ System.getProperty(LINE_SEPARATOR));

		// going over all requests of the ChangeSet
		for (IRestRequest iRestRequest : requests) {
			// going over all the headers of each request and append them
			for (Entry<String, List<String>> entry : changeSetHeaders
					.entrySet()) {
				for (String value : entry.getValue()) {
					result.append(entry.getKey()
							+ ": " + value + System.getProperty(LINE_SEPARATOR)); //$NON-NLS-1$
				}
			}

			result.append(System.getProperty(LINE_SEPARATOR));

			changeSetRequestMethod = iRestRequest.getMethod();
			result.append(changeSetRequestMethod.toString()
					+ " " + iRestRequest.getUrl() + " HTTP/1.1 " + System.getProperty(LINE_SEPARATOR)); //$NON-NLS-1$ //$NON-NLS-2$
			if (changeSetRequestMethod.equals(Method.POST)
					|| changeSetRequestMethod.equals(Method.PUT)) {
				result.append(CONTENT_TYPE
						+ iRestRequest.getHeader(CONTENT_TYPE2)
						+ System.getProperty(LINE_SEPARATOR));
				result.append(CONTENT_LENGTH2
						+ iRestRequest.getHeader(CONTENT_LENGTH)
						+ System.getProperty(LINE_SEPARATOR));
			}

			result.append(System.getProperty(LINE_SEPARATOR));
			result.append(iRestRequest.getBody()
					+ System.getProperty(LINE_SEPARATOR)
					+ System.getProperty(LINE_SEPARATOR));

			if (--size == 0) // Last request in the ChangeSet
			{
				result.append(CHANGESET2 + changeSetBoundry);
			} else {
				result.append(CHANGESET2 + changeSetBoundry
						+ System.getProperty(LINE_SEPARATOR));
			}
		}
		result.append("--" + System.getProperty(LINE_SEPARATOR) + System.getProperty(LINE_SEPARATOR)); //$NON-NLS-1$

		return result;
	}

	/**
	 * Handle the build of a read request.
	 * 
	 * @param result
	 * @param request
	 * @return result
	 */
	private StringBuffer handleReadRequest(StringBuffer result, Object request) {
		IRestRequest readRequest;
		Headers headers;
		Method method;
		readRequest = (IRestRequest) request;
		headers = readRequest.getHeaders();

		// append the headers of the request
		result.append(CONTENT_TYPE + headers.get(CONTENT_TYPE2).get(0)
				+ System.getProperty(LINE_SEPARATOR));
		result.append(CONTENT_TRANSFER_ENCODING2
				+ headers.get(CONTENT_TRANSFER_ENCODING).get(0)
				+ System.getProperty(LINE_SEPARATOR));
		method = readRequest.getMethod();

		result.append(System.getProperty(LINE_SEPARATOR));

		String url = readRequest.getUrl();

		result.append(method.toString()
				+ " " + url + " HTTP/1.1 " + System.getProperty(LINE_SEPARATOR) + System.getProperty(LINE_SEPARATOR) + System.getProperty(LINE_SEPARATOR)); //$NON-NLS-1$ //$NON-NLS-2$

		return result;
	}
}