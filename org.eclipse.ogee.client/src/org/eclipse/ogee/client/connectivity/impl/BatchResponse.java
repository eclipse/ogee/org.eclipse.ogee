/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.eclipse.ogee.client.connectivity.api.IRestResponse;

/**
 * A Batch Response is a RestResponse that contains a list of responses for each
 * read and ChangeSet requests that was in the associated Batch request.
 */
public class BatchResponse extends RestResponse {
	private static final String LINE_SEPARATOR = "line.separator"; //$NON-NLS-1$
	private static final String HTTP = "HTTP"; //$NON-NLS-1$
	private static final String MULTIPART_MIXED = "multipart/mixed"; //$NON-NLS-1$

	private List<IRestResponse> responses;

	/**
	 * Constructor
	 */
	public BatchResponse() {
		super();
		this.responses = new ArrayList<IRestResponse>();
	}

	/**
	 * @return list of responses for each read request and ChangeSet that was in
	 *         the associated Batch request.
	 */
	public List<IRestResponse> getResponses() {
		return responses;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.connectivity.impl.RestResponse#setBody
	 * (java.lang.String)
	 */
	@Override
	public void setBody(String body) {
		super.setBody(body);
		buildResponseList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.client.connectivity.impl.RestResponse#setBody
	 * (byte[])
	 */
	@Override
	public void setBody(byte[] byteArrayBody) {
		super.setBody(byteArrayBody);
		buildResponseList();
	}

	/**
	 * Parses the response string using Scanners and inserts it to the list of
	 * responses.
	 */
	private void buildResponseList() {
		String body = this.getBody();

		// get the boundary and split according to it
		String boundary = body.substring(0,
				body.indexOf(System.getProperty(LINE_SEPARATOR)));
		Scanner scanner = new Scanner(body);
		scanner.useDelimiter(boundary);
		// a chunk of information from the response
		String chunk;

		while (scanner.hasNext()) {
			chunk = scanner.next();

			// end of response? skip
			if (chunk.startsWith("--")) //$NON-NLS-1$
				continue;

			// continue parsing
			IRestResponse response = handleChunk(chunk);

			// add the parsed response to the Batch response
			this.addResponse(response);
		}
	}

	/**
	 * Parses a chunk of data from a response.
	 * 
	 * @param mainChunk
	 *            - the data to be parsed.
	 * @return - the parsed response.
	 */
	private IRestResponse handleChunk(String mainChunk) {
		ChangeSetResponse changeSetResponse = null; // the returned response may
													// not be a ChangeSet

		// continue the parsing, splitting according to empty lines
		Scanner scanner = new Scanner(mainChunk);
		scanner.useDelimiter(System.getProperty(LINE_SEPARATOR)
				+ System.getProperty(LINE_SEPARATOR));
		String chunk;
		int i = 0;
		IRestResponse response = new RestResponse();

		while (scanner.hasNext()) {
			chunk = scanner.next();
			i++; // keeping a counter

			// skipping the first chunk of headers

			if (i == 2) {
				// are we in a ChangeSet?

				// ChangeSet boundary extraction
				String boundary = chunk.substring(0,
						chunk.indexOf(System.getProperty(LINE_SEPARATOR)));

				if (boundary.startsWith("--")) //$NON-NLS-1$
				{
					// start building a ChangeSet response
					changeSetResponse = new ChangeSetResponse();
					Scanner changeSetScanner = new Scanner(mainChunk);
					changeSetScanner.useDelimiter(boundary);
					String subChunk;

					// continue parsing according to the ChangeSet boundary

					while (changeSetScanner.hasNext()) {
						subChunk = changeSetScanner.next();

						if (subChunk.contains(MULTIPART_MIXED))
							subChunk = changeSetScanner.next(); // skip these
																// headers

						if (subChunk.startsWith("--")) //$NON-NLS-1$
							continue;

						// another scanner.... splitting according to empty
						// lines
						Scanner yetAnotherScanner = new Scanner(subChunk);
						yetAnotherScanner.useDelimiter(System
								.getProperty(LINE_SEPARATOR)
								+ System.getProperty(LINE_SEPARATOR));

						String yetAnotherChunk;

						while (yetAnotherScanner.hasNext()) {
							yetAnotherChunk = yetAnotherScanner.next();

							if (yetAnotherScanner.hasNext())
								yetAnotherChunk = yetAnotherScanner.next(); // skip

							if (yetAnotherChunk.startsWith(HTTP)) // we are at
																	// the
																	// headers
							{
								// insert them to the response
								response = insertHeaders(yetAnotherChunk,
										response);
							} else {
								// set the body of the response, may be empty
								response.setBody(yetAnotherChunk);
							}
						}
						// add the parsed response to the ChangeSet response
						changeSetResponse.addResponse(response);
					}
				} else {
					response = insertHeaders(chunk, response);
				}
			}

			if (i == 3 && !chunk.startsWith(HTTP)) {
				response.setBody(chunk);
			}
		}

		if (changeSetResponse != null)
			return changeSetResponse; // this is how we know the response
										// is a ChangeSet response

		return response;
	}

	private void addResponse(IRestResponse response) {
		this.responses.add(response);
	}

	/**
	 * Insert headers to the request.
	 * 
	 * @param chunk
	 * @param response
	 * @return - IRestResponse with the headers.
	 */
	private IRestResponse insertHeaders(String chunk, IRestResponse response) {
		Scanner scanner = new Scanner(chunk);
		scanner.useDelimiter(System.getProperty(LINE_SEPARATOR));
		String statusLine = null;
		int i = 0;

		while (scanner.hasNext()) {
			String next = scanner.next();
			i++;
			if (i == 1) {
				// get the status line
				statusLine = next;
			}
			if (i > 1) {
				// get the headers
				String[] split = next.split(": "); //$NON-NLS-1$
				response.setHeader(split[0], split[1]);
			}
		}

		response.setStatusCode(Integer.parseInt(statusLine.split(" ")[1])); //$NON-NLS-1$

		return response;
	}
}