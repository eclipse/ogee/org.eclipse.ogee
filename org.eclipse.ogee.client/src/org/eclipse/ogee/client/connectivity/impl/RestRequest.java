/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

import java.util.List;

import org.eclipse.ogee.client.connectivity.api.IRestRequest;

/**
 * Implementation of IRestRequest
 * 
 */
public class RestRequest implements IRestRequest {
	private Method method;
	private String url;
	private String body;
	private Headers headers = new Headers();

	/**
	 * Constructor
	 * 
	 * @param method
	 *            HTTP method of the request
	 * @param url
	 */
	public RestRequest(Method method, String url) {
		super();
		this.method = method;
		this.url = url;
	}

	/**
	 * Constructor Default request method is GET.
	 * 
	 * @param url
	 */
	public RestRequest(String url) {
		this(Method.GET, url);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ogee.client.connectivity.api.IRestRequest#setUrl(java.lang
	 * .String)
	 */
	@Override
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public void setBody(String body) {
		if (body == null) {
			return;
		}

		this.body = body;
		this.setHeader(ConnectivityConstants.CONTENT_LENGTH,
				String.valueOf(body.length()));
	}

	@Override
	public String getBody() {
		return body;
	}

	@Override
	public void setHeader(String header, String value) {
		headers.put(header, value);
	}

	@Override
	public String getHeader(String header) {
		return headers.getFirst(header);
	}

	@Override
	public List<String> getHeaders(String header) {
		return headers.get(header);
	}

	@Override
	public void setHeaders(Headers headers) {
		this.headers = headers;
	}

	@Override
	public Headers getHeaders() {
		return headers;
	}

	@Override
	public void setMethod(Method method) {
		this.method = method;
	}

	@Override
	public Method getMethod() {
		return method;
	}

	@Override
	public String getContentType() {
		List<String> list = headers.get(ConnectivityConstants.CONTENT_TYPE);
		if (list != null)
			return list.get(0);
		else
			return null;
	}

	@Override
	public void setContentType(String contentType) {
		headers.put(ConnectivityConstants.CONTENT_TYPE, contentType);
	}
}
