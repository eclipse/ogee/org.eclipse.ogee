/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Represents headers of an HTTP request.
 */
public class Headers extends HashMap<String, List<String>> {
	private static final long serialVersionUID = -8523137196553336053L;

	/**
	 * Adds a header with the given value.
	 * 
	 * @param header
	 * @param value
	 */
	public void put(String header, String value) {
		List<String> list = this.get(header);
		if (list == null) {
			list = new ArrayList<String>();
			this.put(header, list);
		}

		list.add(value);
	}

	@Override
	public List<String> get(Object key) {
		if (key != null) {
			return this.get(key.toString());
		}

		return super.get(key);
	}

	/**
	 * Returns the header value(s) according to the given key
	 * 
	 * @param key
	 * @return a list of the header value(s)
	 */
	public List<String> get(String key) {
		if (key != null) {
			String lowercaseKey = key.toLowerCase(Locale.ENGLISH);
			return super.get(lowercaseKey);
		}

		return super.get(key);
	}

	@Override
	public boolean containsKey(Object key) {
		if (key != null) {
			String lowercaseKey = key.toString().toLowerCase(Locale.ENGLISH);
			return super.containsKey(lowercaseKey);
		}
		return super.containsKey(key);
	}

	@Override
	public List<String> put(String key, List<String> value) {
		if (key != null) {
			String lowercaseKey = key.toString().toLowerCase(Locale.ENGLISH);
			return super.put(lowercaseKey, value);
		}
		return super.put(key, value);
	}

	/**
	 * Returns the first header value according to the given key
	 * 
	 * @param key
	 * @return header value
	 */
	public String getFirst(String key) {
		List<String> list = this.get(key);
		if (list != null)
			return list.get(0);
		else
			return null;
	}
}
