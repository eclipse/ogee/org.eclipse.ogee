/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.ogee.client.connectivity.api.IRestResponse;

/**
 * Implementation of IRestResponse
 * 
 */
public class RestResponse implements IRestResponse {
	private static final String UTF_8 = "UTF-8"; //$NON-NLS-1$
	private final static Logger LOGGER = Logger.getLogger(RestResponse.class
			.getName());
	private int responseStatusCode;
	private String body;
	private Headers headers = new Headers();
	private byte[] bodyBytes;

	@Override
	public int getStatusCode() {
		return responseStatusCode;
	}

	@Override
	public void setStatusCode(int statusCode) {
		this.responseStatusCode = statusCode;
	}

	@Override
	public String getBody() {
		try {
			if (body == null) {
				body = new String(bodyBytes, UTF_8);
			}
		} catch (UnsupportedEncodingException e) {
			LOGGER.severe(e.getMessage());
		}

		return body;
	}

	@Override
	public void setBody(String body) {
		if (body == null) {
			return;
		}

		try {
			this.bodyBytes = body.getBytes(UTF_8);
			this.body = body;
		} catch (UnsupportedEncodingException e) {
			LOGGER.severe(e.getMessage());
		}
	}

	@Override
	public String getHeader(String header) {
		return headers.getFirst(header);
	}

	@Override
	public List<String> getHeaders(String header) {
		return headers.get(header);
	}

	@Override
	public void setHeader(String header, String value) {
		headers.put(header, value);

	}

	@Override
	public void setHeaders(Headers headers) {
		this.headers = headers;

	}

	@Override
	public Headers getHeaders() {
		return headers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RestResponse [responseStatusCode=" + responseStatusCode + ", body=" + body + ", headers=" + headers //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ "]"; //$NON-NLS-1$
	}

	@Override
	public void setBody(byte[] byteArrayBody) {
		this.bodyBytes = byteArrayBody;
		this.body = null;

	}

	@Override
	public byte[] getBodyBytes() {
		if (this.bodyBytes != null) {
			return Arrays.copyOf(this.bodyBytes, this.bodyBytes.length);
		}
		return null;
	}

}
