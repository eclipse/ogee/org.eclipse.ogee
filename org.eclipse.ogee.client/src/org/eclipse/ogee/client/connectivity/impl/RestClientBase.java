/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ogee.client.activator.Activator;
import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestRequestInterceptor;
import org.eclipse.ogee.client.connectivity.api.IRestResponse;
import org.eclipse.ogee.client.connectivity.api.IRestResponseInterceptor;
import org.eclipse.ogee.client.connectivity.interceptors.SamlResponseInterceptor;
import org.eclipse.ogee.client.connectivity.security.UsernamePasswordCredentials;
import org.eclipse.ogee.client.exceptions.RestClientException;
import org.eclipse.ogee.client.util.TraceLogger;

/**
 * Base class of a rest client with an abstract method for sending a request
 * 
 */
public abstract class RestClientBase implements IRestClient {
	private static final TraceLogger traceLogger = TraceLogger
			.getLogger(Activator.PLUGIN_ID);
	private List<IRestRequestInterceptor> requestInterceptors = new ArrayList<IRestRequestInterceptor>();
	private List<IRestResponseInterceptor> responseInterceptors = new ArrayList<IRestResponseInterceptor>();

	@Override
	public IRestResponse execute(IRestRequest restRequest)
			throws RestClientException {

		traceLogger.trace("Inside execute method");
		RestClientException internalException = null;
		IRestResponse restResponse = null;

		for (IRestRequestInterceptor requestInterceptor : requestInterceptors) {
			requestInterceptor.onRequest(restRequest, this);
		}

		for (IRestResponseInterceptor responseInterceptor : responseInterceptors) {
			if (responseInterceptor instanceof SamlResponseInterceptor) {
				UsernamePasswordCredentials auth = (UsernamePasswordCredentials) ((SamlResponseInterceptor) responseInterceptor)
						.getBasicAuthentication();
				if (auth != null) {
					String userPassword = auth.getUserName()
							+ ":" + auth.getPassword(); //$NON-NLS-1$
					restRequest.getHeaders().put("credentials", userPassword); //$NON-NLS-1$
				}
			}
		}

		try {
			restResponse = send(restRequest);
		} catch (RestClientException e) {
			traceLogger.trace("RestClientException  is:   %1$s",
					e.getLocalizedMessage());
			internalException = e;
			if (restResponse == null)
				restResponse = e;
		}

		for (IRestResponseInterceptor responseInterceptor : responseInterceptors) {
			responseInterceptor.onResponse(restRequest, restResponse, this);
		}

		traceLogger
				.trace("Response status code  is: %1$s   response body is:  %2$s response headers are :  %3$s",
						restResponse.getStatusCode(), restResponse.getBody(),
						restResponse.getHeaders().toString());
		if (restResponse.getStatusCode() >= 400
				|| restResponse.getStatusCode() == 0) {

			if (restResponse instanceof RestClientException) {
				traceLogger.trace("RestClientException Occurred: %1$s",
						((RestClientException) restResponse)
								.getLocalizedMessage());
				throw (RestClientException) restResponse;
			}

			RestClientException restClientException;

			if (internalException == null)
				restClientException = new RestClientException();
			else
				restClientException = new RestClientException(internalException);

			restClientException.setStatusCode(restResponse.getStatusCode());
			restClientException.setBody(restResponse.getBody());
			restClientException.setHeaders(restResponse.getHeaders());
			throw restClientException;
		}

		if (restRequest instanceof BatchRequest
				&& !(restResponse instanceof BatchResponse)) {
			BatchResponse br = new BatchResponse();
			br.setBody(restResponse.getBodyBytes());
			br.setHeaders(restResponse.getHeaders());
			br.setStatusCode(restResponse.getStatusCode());
			return br;
		}

		return restResponse;
	}

	@Override
	public void registerRequestInterceptor(
			IRestRequestInterceptor requestInterceptor) {
		requestInterceptors.add(requestInterceptor);
	}

	@Override
	public List<IRestRequestInterceptor> getRequestInterceptors() {
		return requestInterceptors;
	}

	@Override
	public void unregisterRequestInterceptor(
			IRestRequestInterceptor requestInterceptor) {
		requestInterceptors.remove(requestInterceptor);
	}

	@Override
	public void registerResponseInterceptor(
			IRestResponseInterceptor responseInterceptor) {
		responseInterceptors.add(responseInterceptor);
	}

	@Override
	public List<IRestResponseInterceptor> getResponseInterceptors() {
		return responseInterceptors;
	}

	@Override
	public void unregisterResponseInterceptor(
			IRestResponseInterceptor responseInterceptor) {
		responseInterceptors.remove(responseInterceptor);
	}

	/**
	 * Abstract method that should be implemented and perform the connectivity
	 * logic. Should not be called directly. This method is called automatically
	 * from the execute method.
	 * 
	 * @param restRequest
	 *            IRestRequest the request
	 * @return IRestResponse the response
	 * @throws RestClientException
	 *             in case of connectivity error
	 */
	public abstract IRestResponse send(IRestRequest restRequest)
			throws RestClientException;

}
