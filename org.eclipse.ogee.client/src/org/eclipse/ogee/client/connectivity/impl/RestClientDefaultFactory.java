/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Locale;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.eclipse.ogee.client.activator.Activator;
import org.eclipse.ogee.client.connectivity.api.IAuthentication;
import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IServerConnectionParameters;
import org.eclipse.ogee.client.connectivity.api.RestClientAbstractFactory;
import org.eclipse.ogee.client.connectivity.interceptors.AcceptHeaderInterceptor;
import org.eclipse.ogee.client.connectivity.interceptors.AtomContentTypeRequestInterceptor;
import org.eclipse.ogee.client.connectivity.interceptors.CompatabilityCsrfRequestInterceptor;
import org.eclipse.ogee.client.connectivity.interceptors.CookiesResponseInterceptor;
import org.eclipse.ogee.client.connectivity.interceptors.CsrfInterceptor;
import org.eclipse.ogee.client.connectivity.interceptors.DataServiceVersionHeaderInterceptor;
import org.eclipse.ogee.client.connectivity.interceptors.FormAuthenticationResponseInterceptor;
import org.eclipse.ogee.client.connectivity.interceptors.JSONContentTypeRequestInterceptor;
import org.eclipse.ogee.client.connectivity.interceptors.SamlResponseInterceptor;
import org.eclipse.ogee.client.connectivity.security.IBasicAuthentication;
import org.eclipse.ogee.client.connectivity.security.X509Authentication;
import org.eclipse.ogee.client.parser.Representation;
import org.eclipse.ogee.client.util.TraceLogger;

public class RestClientDefaultFactory implements RestClientAbstractFactory {
	private static final TraceLogger traceLogger = TraceLogger
			.getLogger(Activator.PLUGIN_ID);

	private static Representation asEnum(String str) {
		for (Representation me : Representation.values()) {
			if (me.name().equalsIgnoreCase(str))
				return me;
		}
		return null;
	}

	@Override
	public IRestClient createInstance(
			IServerConnectionParameters serverConnection) {

		traceLogger.trace("Inside createInstance");
		IRestClient restClient = null;
		traceLogger.trace("restClient object initialized to null", restClient);
		if (serverConnection == null) {
			return null;
		}

		String representationStr = String.valueOf(
				serverConnection.get(IServerConnectionParameters.ODATA_FORMAT))
				.toUpperCase(Locale.ENGLISH);
		traceLogger.trace("ODataFormat  is:   %1$s", representationStr);
		Representation representation = asEnum(representationStr);

		if (representation == null) {
			representation = Representation.ATOM;
		} else {
			switch (asEnum(representationStr)) {
			case ATOM:
				representation = Representation.ATOM;
				break;
			case JSON:
				representation = Representation.JSON;
				break;
			default:
				representation = Representation.ATOM;
			}
		}

		IAuthentication authentication = (IAuthentication) serverConnection
				.get(IServerConnectionParameters.AUTHENTICATION);

		// for NPE on https URL

		if (representation == null) {
			restClient = new DefaultRestClient(serverConnection,
					(IBasicAuthentication) null, Representation.ATOM);
		} else {
			restClient = new DefaultRestClient(serverConnection,
					(IBasicAuthentication) null, representation);
		}

		traceLogger.trace("RestClient Initialized");
		traceLogger
				.trace("useSSL : %1$b", Boolean.valueOf(String
						.valueOf(serverConnection.get("useSSL"))));
		if (Boolean.TRUE.equals(serverConnection.get("useSSL"))) { //$NON-NLS-1$
			traceLogger.trace("HTTPS connection");
			File file = new File("jsecacerts"); //$NON-NLS-1$
			if (file.isFile() == false) {
				char SEP = File.separatorChar;
				File dir = new File(System.getProperty("java.home") + SEP //$NON-NLS-1$
						+ "lib" + SEP + "security"); //$NON-NLS-1$ //$NON-NLS-2$
				file = new File(dir, "jssecacerts"); //$NON-NLS-1$
				if (file.isFile() == false) {
					file = new File(dir, "cacerts"); //$NON-NLS-1$
				}
			}
			traceLogger.trace("Is file present: %1$b",
					Boolean.valueOf(file.isFile()));
			try {
				traceLogger.trace("Loading KeyStore");
				InputStream in = new FileInputStream(file);
				KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
				ks.load(in, "changeit".toCharArray()); //$NON-NLS-1$
				in.close();

				traceLogger.trace("Creating instance of SSLContext");
				SSLContext context = SSLContext.getInstance("SSL"); //$NON-NLS-1$
				TrustManagerFactory tmf = TrustManagerFactory
						.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				tmf.init(ks);
				X509TrustManager defaultTrustManager = (X509TrustManager) tmf
						.getTrustManagers()[0];
				SavingTrustManager tm = new SavingTrustManager(
						defaultTrustManager);
				context.init(null, new TrustManager[] { tm }, null);
				traceLogger.trace("Get SSLfactory instance from SSLContext");
				SSLSocketFactory factory = context.getSocketFactory();

				traceLogger.trace(
						"Opening connection to Host:  %1$s Port: %2$s",
						serverConnection.getHost(), serverConnection.getPort());
				SSLSocket socket = (SSLSocket) factory.createSocket(
						serverConnection.getHost(),
						Integer.parseInt(serverConnection.getPort()));
				socket.setSoTimeout(10000);
				try {
					traceLogger.trace("Stating SSL handshake....");
					socket.startHandshake();
					socket.close();
				} catch (SSLException e) {
					// do nothing
					traceLogger.trace("SSLException occurred:   %1$s",
							e.getLocalizedMessage());
				}

				X509Certificate[] chain = tm.chain;
				if (chain != null) {
					X509Certificate cert = chain[0];
					String alias = serverConnection.getHost()
							+ "-" + serverConnection.getPort(); //$NON-NLS-1$
					ks.setCertificateEntry(alias, cert);
					OutputStream out = new FileOutputStream("jssecacerts"); //$NON-NLS-1$
					ks.store(out, "changeit".toCharArray()); //$NON-NLS-1$
					out.close();
				}
				traceLogger.trace("Initializing X509Authentication");
				X509Authentication x509Authentication = new X509Authentication(
						factory);
				traceLogger.trace("Initialising the Rest Client for HTTPS url");
				restClient = new DefaultRestClient(serverConnection,
						x509Authentication, representation);
				traceLogger.trace("restClient object initialized to "
						+ restClient.toString());

			} catch (Exception e) {
				e.printStackTrace();
				traceLogger
						.trace("Exception thrown while opening a connection to host: "
								+ e.getLocalizedMessage().toString());
				traceLogger
						.trace("Exception thrown while opening a connection to host: "
								+ e.getMessage().toString());
			}
		} else {
			traceLogger.trace("Initialising the Rest Client for HTTP url");
			restClient = new DefaultRestClient(serverConnection,
					(IBasicAuthentication) authentication, representation);
			traceLogger.trace("restClient object initialized to "
					+ restClient.toString());
		}

		traceLogger.trace("Registering cookie response interceptor");
		restClient
				.registerResponseInterceptor(new CookiesResponseInterceptor());
		traceLogger.trace("Registering csrf response and request  interceptor");
		CsrfInterceptor csrf = new CsrfInterceptor();
		restClient.registerResponseInterceptor(csrf);
		restClient.registerRequestInterceptor(csrf);
		traceLogger
				.trace("Registering compatibility csrf  request  interceptor");
		restClient
				.registerRequestInterceptor(new CompatabilityCsrfRequestInterceptor());

		switch (representation) {
		case ATOM:
			restClient
					.registerRequestInterceptor(new AtomContentTypeRequestInterceptor());
			break;
		case JSON:
			restClient
					.registerRequestInterceptor(new JSONContentTypeRequestInterceptor());
			break;
		default:
			break;
		}

		traceLogger.trace("Registering accept header request interceptor");
		restClient.registerRequestInterceptor(new AcceptHeaderInterceptor());
		traceLogger
				.trace("Registering dataservise versionheader request interceptor");
		restClient
				.registerRequestInterceptor(new DataServiceVersionHeaderInterceptor());
		traceLogger.trace("Registering samlresponse interceptor");
		restClient.registerResponseInterceptor(new SamlResponseInterceptor(
				authentication));
		traceLogger.trace("Registering formauthenticationresponse interceptor");
		restClient
				.registerResponseInterceptor(new FormAuthenticationResponseInterceptor(
						authentication));

		return restClient;
	}

	private static class SavingTrustManager implements X509TrustManager {

		private final X509TrustManager tm;
		protected X509Certificate[] chain;

		SavingTrustManager(X509TrustManager tm) {
			this.tm = tm;
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {

			return new X509Certificate[0];
		}

		@Override
		public void checkClientTrusted(X509Certificate[] pChain, String authType)
				throws CertificateException {

			throw new UnsupportedOperationException();
		}

		@Override
		public void checkServerTrusted(X509Certificate[] pChain, String authType)
				throws CertificateException {

			this.chain = pChain;
			this.tm.checkServerTrusted(pChain, authType);
		}
	}
}
