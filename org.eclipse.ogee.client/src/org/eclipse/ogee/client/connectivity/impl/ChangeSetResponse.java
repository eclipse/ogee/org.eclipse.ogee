/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ogee.client.connectivity.api.IRestResponse;

/**
 * A ChangeSet Response is a RestResponse and contains a list of responses for
 * each request that was in the associated ChangeSet.
 */
public class ChangeSetResponse extends RestResponse {
	private List<IRestResponse> responses;

	/**
	 * Constructor
	 */
	public ChangeSetResponse() {
		super();
		this.responses = new ArrayList<IRestResponse>();
	}

	/**
	 * Adds the given response to the responses of the ChangeSet.
	 * 
	 * @param response
	 *            - the given response to be added.
	 */
	public void addResponse(IRestResponse response) {
		this.responses.add(response);
	}

	/**
	 * @return the responses of the ChangeSet
	 */
	public List<IRestResponse> getResponses() {
		return responses;
	}
}