/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.api;

import java.util.List;

import org.eclipse.ogee.client.connectivity.impl.Headers;

/**
 * Provides a simple interface to request REST content from a server
 */
public interface IRestRequest {
	/**
	 * HTTP methods enum
	 * 
	 */
	public enum Method {
		GET, POST, PUT, DELETE
	}

	/**
	 * Sets the request HTTP method. The possible values are GET, POST, PUT and
	 * DELETE
	 * 
	 * @param method
	 *            of the request
	 */
	public void setMethod(IRestRequest.Method method);

	/**
	 * Gets the request HTTP method.
	 * 
	 * @return String url of the request
	 */
	public IRestRequest.Method getMethod();

	/**
	 * Sets the request URL, relative to host.
	 * 
	 * @param url
	 *            of the request
	 */
	public void setUrl(String url);

	/**
	 * Gets the request URL, relative to host.
	 * 
	 * @return String url of the request
	 */
	public String getUrl();

	/**
	 * Sets the request body as a String object. The request body is only used
	 * for POST and PUT requests
	 * 
	 * @param body
	 *            representation of the request body
	 */
	public void setBody(String body);

	/**
	 * Gets the request body as a String object. The request body is only used
	 * for POST and PUT requests
	 * 
	 * @return String representation of the request body
	 */
	public String getBody();

	/**
	 * Sets a specific header value by name.
	 * 
	 * @param header
	 *            name to set
	 * @param value
	 *            header value to set
	 */
	public void setHeader(String header, String value);

	/**
	 * Gets a specific header by name.
	 * 
	 * @param header
	 *            header name to get
	 * @return String value of the first header with that name
	 */
	public String getHeader(String header);

	/**
	 * Gets a specific header list by name.
	 * 
	 * @param header
	 *            header name to get
	 * @return List<String> values of the header with that name
	 */
	public List<String> getHeaders(String header);

	/**
	 * Sets the request headers
	 */
	public void setHeaders(Headers headers);

	/**
	 * Gets the headers
	 * 
	 * @return Headers representation of the headers
	 */
	public Headers getHeaders();

	/**
	 * Gets the content type header of the request
	 * 
	 * @return content type of the request
	 */
	public String getContentType();

	/**
	 * Sets the content type header of the request
	 * 
	 * @param contentType
	 *            content type of the request
	 */
	public void setContentType(String contentType);
}
