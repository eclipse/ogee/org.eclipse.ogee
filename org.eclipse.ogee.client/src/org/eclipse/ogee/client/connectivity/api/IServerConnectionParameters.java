/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.api;

import java.util.Map;

/**
 * Interface which defines a server parameters
 */
public interface IServerConnectionParameters extends Map<String, Object> {

	/**
	 * Constant for specifying if SSL should be used
	 */
	public static final String USE_SSL = "useSSL"; //$NON-NLS-1$
	
	/**
	 * Constant for specifying the OData format that should be used. Possible
	 * values are: ATOM, JSON
	 */
	
	public static final String ODATA_FORMAT = "OData_Format"; //$NON-NLS-1$
	/**
	 * Constant for specifying the authentication object. Possible values:
	 * UsernamePasswordCredentials, X509Authentication
	 */
	
	public static final String AUTHENTICATION = "Authentication"; //$NON-NLS-1$	

	/**
	 * Gets the server host (FQDN, short host name or IP)
	 * 
	 * @return the host
	 */
	public String getHost();

	/**
	 * Gets the server port
	 * 
	 * @return the port
	 */
	public String getPort();

}
