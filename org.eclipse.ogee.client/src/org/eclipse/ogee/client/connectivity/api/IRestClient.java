/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.api;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;

import org.eclipse.ogee.client.exceptions.RestClientException;
import org.eclipse.ogee.client.parser.Representation;

/**
 * Interface of a REST client
 */
public interface IRestClient {
	/**
	 * Executes a given request
	 * 
	 * @param restRequest
	 *            a request object
	 * @return a response object
	 * @throws RestClientException
	 *             when an error occurred during execution
	 */
	public IRestResponse execute(IRestRequest restRequest)
			throws RestClientException;

	/**
	 * Registers the client with a request interceptor
	 * 
	 * @param requestInterceptor
	 *            request interceptor
	 */
	public void registerRequestInterceptor(
			IRestRequestInterceptor requestInterceptor);

	/**
	 * Returns a list of request interceptors the client is registered to
	 * 
	 * @return list of request interceptors the client is registered to
	 */
	public List<IRestRequestInterceptor> getRequestInterceptors();

	/**
	 * Unregisters the client from a given request interceptor
	 * 
	 * @param requestInterceptor
	 *            request interceptor
	 */
	public void unregisterRequestInterceptor(
			IRestRequestInterceptor requestInterceptor);

	/**
	 * Registers the client with a response interceptor
	 * 
	 * @param responseInterceptor
	 *            response interceptor
	 */
	public void registerResponseInterceptor(
			IRestResponseInterceptor responseInterceptor);

	/**
	 * Returns a list of response interceptors the client is registered to
	 * 
	 * @return list of response interceptors the client is registered to
	 */
	public List<IRestResponseInterceptor> getResponseInterceptors();

	/**
	 * Unregisters the client from a given response interceptor
	 * 
	 * @param responseInterceptor
	 *            response interceptor
	 */
	public void unregisterResponseInterceptor(
			IRestResponseInterceptor responseInterceptor);

	/**
	 * Gets a specific response cookie by name.
	 * 
	 * @param name
	 *            cookie name to get
	 * @return String value of the response cookie with that name
	 */
	public String getCookie(String name);

	/**
	 * Sets a specific cookie value by name.
	 * 
	 * @param name
	 *            cookie name to set
	 * @param value
	 *            cookie value to set
	 */
	public void setCookie(String name, String value);

	/**
	 * Sets the Map of request cookies
	 */
	public void setCookies(Map<String, String> cookies);

	/**
	 * Gets the collection of cookies set by the remote server
	 * 
	 * @return Map<String, String> representation of the cookie
	 */
	public Map<String, String> getCookies();

	/**
	 * @return Representation (ATOM / JSON)
	 */
	public Representation getRepresentation();

	/**
	 * Returns HttpURLConnection
	 * 
	 * @return httpURLConnection
	 */
	public HttpURLConnection getHttpURLConnection();
}
