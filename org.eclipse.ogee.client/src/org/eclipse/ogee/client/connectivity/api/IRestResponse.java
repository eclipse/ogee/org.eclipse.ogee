/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.api;

import java.util.List;

import org.eclipse.ogee.client.connectivity.impl.Headers;

/**
 * Provides a simple interface for receiving REST content from a server
 */
public interface IRestResponse {
	/**
	 * Gets the response status code based on the HTTP RFC (200 - OK and so on)
	 * 
	 * @return int value of the status of the response, according to the HTTP
	 *         spec
	 */
	public int getStatusCode();

	/**
	 * Sets the response status code based on the HTTP RFC (200 - OK and so on)
	 */
	public void setStatusCode(int statusCode);

	/**
	 * Gets the response as a String object. The string is initialized in the
	 * same encoding of the response.
	 * 
	 * @return String representation of the response body.
	 */
	public String getBody();

	/**
	 * Sets the response body
	 * 
	 * @param body
	 */
	public void setBody(String body);

	/**
	 * Gets a specific response headers list by name.
	 * 
	 * @param header
	 *            header name to get
	 * @return List<String> values of the response headers with that name
	 */
	public List<String> getHeaders(String header);

	/**
	 * Gets a specific response header by name.
	 * 
	 * @param header
	 *            header name to get
	 * @return String value of the first response header with that name
	 */
	public String getHeader(String header);

	/**
	 * Sets a specific header value by name.
	 * 
	 * @param header
	 *            name to set
	 * @param value
	 *            header value to set
	 */
	public void setHeader(String header, String value);

	/**
	 * Sets the request Headers
	 */
	public void setHeaders(Headers headers);

	/**
	 * Gets the Headers set by the remote server
	 * 
	 * @return Headers representation of the headers
	 */
	public Headers getHeaders();

	/**
	 * Sets the response body
	 * 
	 * @param byteArrayBody
	 *            byte array
	 */
	public void setBody(byte[] byteArrayBody);

	/**
	 * @return the response body as a byte array
	 */
	public byte[] getBodyBytes();

}
