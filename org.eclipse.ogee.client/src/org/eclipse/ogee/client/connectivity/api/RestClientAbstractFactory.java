/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.api;

/**
 * Interface of a Rest Client Abstract Factory
 * 
 */
public interface RestClientAbstractFactory {

	/**
	 * Creates a rest client instance based on the given server connection
	 * parameters
	 * 
	 * @param serverConnectionParameters
	 * @return rest client instance
	 */
	public abstract IRestClient createInstance(
			IServerConnectionParameters serverConnectionParameters);

}
