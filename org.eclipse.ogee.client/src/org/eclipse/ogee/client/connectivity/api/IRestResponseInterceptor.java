/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.api;

/**
 * This interface allows custom modifications of responses after they were
 * received from the server. For example: setting additional headers or cookies.
 * 
 * The implementing class should register itself to IRestClient via
 * registerResponseInterceptor(IRestResponseInterceptor restResponseInterceptor)
 * method
 */
public interface IRestResponseInterceptor {
	/**
	 * Called after the response was received from the server
	 * 
	 * @param restRequest
	 *            the request
	 * @param restResponse
	 *            the response
	 * @param restClient
	 *            the restClient which is sending the request
	 */
	public void onResponse(IRestRequest restRequest,
			IRestResponse restResponse, IRestClient restClient);

}
