/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.api;

/**
 * This interface allows custom modifications of requests before they are sent
 * to server. For example: setting additional headers or cookies
 * 
 * The implementing class should register itself to IRestClient via
 * registerRequestInterceptor(IRestRequestInterceptor restRequestInterceptor)
 * method
 */
public interface IRestRequestInterceptor {
	/**
	 * Called before the request is sent to server
	 * 
	 * @param restRequest
	 *            the request.
	 * @param restClient
	 *            the restClient which is sending the request
	 */
	public void onRequest(IRestRequest restRequest, IRestClient restClient);
}
