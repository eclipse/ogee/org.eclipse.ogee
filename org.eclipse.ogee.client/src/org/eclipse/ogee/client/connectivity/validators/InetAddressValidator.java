/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.validators;

import java.io.Serializable;

/**
 * Validator class for inet addresses (ip addresses)
 * 
 */
public class InetAddressValidator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String IPV4_REGEX = "^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$"; //$NON-NLS-1$
	private static final InetAddressValidator VALIDATOR = new InetAddressValidator();

	private final RegexValidator ipv4Validator = new RegexValidator(IPV4_REGEX);

	public static InetAddressValidator getInstance() {
		return VALIDATOR;
	}

	public boolean isValid(String inetAddress) {
		return isValidInet4Address(inetAddress);
	}

	private boolean isValidInet4Address(String inet4Address) {
		// verify that address conforms to generic IPv4 format
		String[] groups = ipv4Validator.match(inet4Address);

		if (groups == null)
			return false;

		// verify that address subgroups are legal
		for (int i = 0; i <= 3; i++) {
			String ipSegment = groups[i];
			if (ipSegment == null || ipSegment.length() <= 0) {
				return false;
			}

			int iIpSegment = 0;

			try {
				iIpSegment = Integer.parseInt(ipSegment);
			} catch (NumberFormatException e) {
				return false;
			}

			if (iIpSegment > 255) {
				return false;
			}

		}

		return true;
	}
}
