/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.validators;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.ogee.client.nls.messages.Messages;

/**
 * Validator class for regular expressions
 * 
 */
public class RegexValidator implements Serializable {

	private static final long serialVersionUID = 1L;
	private final Pattern[] patterns;

	public RegexValidator(String regex) {
		this(regex, true);
	}

	public RegexValidator(String regex, boolean caseSensitive) {
		this(new String[] { regex }, caseSensitive);
	}

	public RegexValidator(String[] regexs) {
		this(regexs, true);
	}

	public RegexValidator(String[] regexs, boolean caseSensitive) {
		if (regexs == null || regexs.length == 0) {
			throw new IllegalArgumentException(
					Messages.getString("RegexValidator.0")); //$NON-NLS-1$
		}
		patterns = new Pattern[regexs.length];
		int flags = (caseSensitive ? 0 : Pattern.CASE_INSENSITIVE);
		for (int i = 0; i < regexs.length; i++) {
			if (regexs[i] == null || regexs[i].length() == 0) {
				throw new IllegalArgumentException(
						Messages.getString("RegexValidator.1") + i + Messages.getString("RegexValidator.2")); //$NON-NLS-1$ //$NON-NLS-2$
			}
			patterns[i] = Pattern.compile(regexs[i], flags);
		}
	}

	public boolean isValid(String value) {
		if (value == null) {
			return false;
		}
		for (int i = 0; i < patterns.length; i++) {
			if (patterns[i].matcher(value).matches()) {
				return true;
			}
		}
		return false;
	}

	public String[] match(String value) {
		if (value == null) {
			return null;
		}
		for (int i = 0; i < patterns.length; i++) {
			Matcher matcher = patterns[i].matcher(value);
			if (matcher.matches()) {
				int count = matcher.groupCount();
				String[] groups = new String[count];
				for (int j = 0; j < count; j++) {
					groups[j] = matcher.group(j + 1);
				}
				return groups;
			}
		}
		return null;
	}

	public String validate(String value) {
		if (value == null) {
			return null;
		}
		for (int i = 0; i < patterns.length; i++) {
			Matcher matcher = patterns[i].matcher(value);
			if (matcher.matches()) {
				int count = matcher.groupCount();
				if (count == 1) {
					return matcher.group(1);
				}
				StringBuffer buffer = new StringBuffer();
				for (int j = 0; j < count; j++) {
					String component = matcher.group(j + 1);
					if (component != null) {
						buffer.append(component);
					}
				}
				return buffer.toString();
			}
		}
		return null;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("RegexValidator{"); //$NON-NLS-1$
		for (int i = 0; i < patterns.length; i++) {
			if (i > 0) {
				buffer.append(","); //$NON-NLS-1$
			}
			buffer.append(patterns[i].pattern());
		}
		buffer.append("}"); //$NON-NLS-1$
		return buffer.toString();
	}

}
