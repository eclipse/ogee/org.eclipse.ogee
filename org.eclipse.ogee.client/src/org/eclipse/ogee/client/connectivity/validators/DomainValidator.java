/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.validators;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Validator class for domain name
 */
public class DomainValidator implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String DOMAIN_LABEL_REGEX = "\\p{Alnum}(?>[\\p{Alnum}-]*\\p{Alnum})*"; //$NON-NLS-1$
	private static final String TOP_LABEL_REGEX = "\\p{Alpha}{2,}"; //$NON-NLS-1$
	private static final String DOMAIN_NAME_REGEX = "^(?:" + DOMAIN_LABEL_REGEX + "\\.)+" + "(" + TOP_LABEL_REGEX + ")$"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	private final boolean allowLocal;
	private static final DomainValidator DOMAIN_VALIDATOR = new DomainValidator(
			false);

	private static final DomainValidator DOMAIN_VALIDATOR_WITH_LOCAL = new DomainValidator(
			true);

	private final RegexValidator domainRegex = new RegexValidator(
			DOMAIN_NAME_REGEX);
	private final RegexValidator hostnameRegex = new RegexValidator(
			DOMAIN_LABEL_REGEX);

	public static DomainValidator getInstance() {
		return DOMAIN_VALIDATOR;
	}

	public static DomainValidator getInstance(boolean allowLocal) {
		if (allowLocal) {
			return DOMAIN_VALIDATOR_WITH_LOCAL;
		}
		return DOMAIN_VALIDATOR;
	}

	private DomainValidator(boolean allowLocal) {
		this.allowLocal = allowLocal;
	}

	public boolean isValid(String domain) {
		String[] groups = domainRegex.match(domain);
		if (groups != null && groups.length > 0) {
			return isValidTld(groups[0]);
		} else if (allowLocal) {
			if (hostnameRegex.isValid(domain)) {
				return true;
			}
		}
		return false;
	}

	private boolean isValidTld(String tld) {
		if (allowLocal) {
			return true;
		}

		return isValidLocalTld(tld) || isValidInfrastructureTld(tld)
				|| isValidGenericTld(tld) || isValidCountryCodeTld(tld);
	}

	private boolean isValidInfrastructureTld(String iTld) {
		return INFRASTRUCTURE_TLD_LIST.contains(chompLeadingDot(iTld
				.toLowerCase(Locale.ENGLISH)));
	}

	private boolean isValidGenericTld(String gTld) {
		return GENERIC_TLD_LIST.contains(chompLeadingDot(gTld
				.toLowerCase(Locale.ENGLISH)));
	}

	private boolean isValidCountryCodeTld(String ccTld) {
		return COUNTRY_CODE_TLD_LIST.contains(chompLeadingDot(ccTld
				.toLowerCase(Locale.ENGLISH)));
	}

	private boolean isValidLocalTld(String iTld) {
		return LOCAL_TLD_LIST.contains(chompLeadingDot(iTld
				.toLowerCase(Locale.ENGLISH)));
	}

	private String chompLeadingDot(String str) {
		if (str.startsWith(".")) //$NON-NLS-1$
		{
			return str.substring(1);
		} else {
			return str;
		}
	}

	private static final String[] INFRASTRUCTURE_TLDS = new String[] { "arpa", // internet infrastructure //$NON-NLS-1$
			"root" // diagnostic marker for non-truncated root zone //$NON-NLS-1$
	};

	private static final String[] GENERIC_TLDS = new String[] { "aero", // air transport industry //$NON-NLS-1$
			"asia", // Pan-Asia/Asia Pacific //$NON-NLS-1$
			"biz", // businesses //$NON-NLS-1$
			"cat", // Catalan linguistic/cultural community //$NON-NLS-1$
			"com", // commercial enterprises //$NON-NLS-1$
			"coop", // cooperative associations //$NON-NLS-1$
			"info", // informational sites //$NON-NLS-1$
			"jobs", // Human Resource managers //$NON-NLS-1$
			"mobi", // mobile products and services //$NON-NLS-1$
			"museum", // museums, surprisingly enough //$NON-NLS-1$
			"name", // individuals' sites //$NON-NLS-1$
			"net", // internet support infrastructure/business //$NON-NLS-1$
			"org", // noncommercial organizations //$NON-NLS-1$
			"pro", // credentialed professionals and entities //$NON-NLS-1$
			"tel", // contact data for businesses and individuals //$NON-NLS-1$
			"travel", // entities in the travel industry //$NON-NLS-1$
			"gov", // United States Government //$NON-NLS-1$
			"edu", // accredited postsecondary US education entities //$NON-NLS-1$
			"mil", // United States Military //$NON-NLS-1$
			"int", // organizations established by international treaty //$NON-NLS-1$
			"corp" //$NON-NLS-1$
	};

	private static final String[] COUNTRY_CODE_TLDS = new String[] { "ac", // Ascension Island //$NON-NLS-1$
			"ad", // Andorra //$NON-NLS-1$
			"ae", // United Arab Emirates //$NON-NLS-1$
			"af", // Afghanistan //$NON-NLS-1$
			"ag", // Antigua and Barbuda //$NON-NLS-1$
			"ai", // Anguilla //$NON-NLS-1$
			"al", // Albania //$NON-NLS-1$
			"am", // Armenia //$NON-NLS-1$
			"an", // Netherlands Antilles   //$NON-NLS-1$
			"ao", // Angola //$NON-NLS-1$
			"aq", // Antarctica //$NON-NLS-1$
			"ar", // Argentina //$NON-NLS-1$
			"as", // American Samoa //$NON-NLS-1$
			"at", // Austria //$NON-NLS-1$
			"au", // Australia (includes Ashmore and Cartier Islands and Coral Sea Islands) //$NON-NLS-1$
			"aw", // Aruba //$NON-NLS-1$
			"ax", // Åland //$NON-NLS-1$
			"az", // Azerbaijan //$NON-NLS-1$
			"ba", // Bosnia and Herzegovina //$NON-NLS-1$
			"bb", // Barbados //$NON-NLS-1$
			"bd", // Bangladesh //$NON-NLS-1$
			"be", // Belgium //$NON-NLS-1$
			"bf", // Burkina Faso //$NON-NLS-1$
			"bg", // Bulgaria //$NON-NLS-1$
			"bh", // Bahrain //$NON-NLS-1$
			"bi", // Burundi //$NON-NLS-1$
			"bj", // Benin //$NON-NLS-1$
			"bm", // Bermuda //$NON-NLS-1$
			"bn", // Brunei Darussalam //$NON-NLS-1$
			"bo", // Bolivia //$NON-NLS-1$
			"br", // Brazil //$NON-NLS-1$
			"bs", // Bahamas //$NON-NLS-1$
			"bt", // Bhutan //$NON-NLS-1$
			"bv", // Bouvet Island //$NON-NLS-1$
			"bw", // Botswana //$NON-NLS-1$
			"by", // Belarus //$NON-NLS-1$
			"bz", // Belize //$NON-NLS-1$
			"ca", // Canada //$NON-NLS-1$
			"cc", // Cocos (Keeling) Islands //$NON-NLS-1$
			"cd", // Democratic Republic of the Congo (formerly Zaire) //$NON-NLS-1$
			"cf", // Central African Republic //$NON-NLS-1$
			"cg", // Republic of the Congo //$NON-NLS-1$
			"ch", // Switzerland //$NON-NLS-1$
			"ci", // Côte d'Ivoire //$NON-NLS-1$
			"ck", // Cook Islands //$NON-NLS-1$
			"cl", // Chile //$NON-NLS-1$
			"cm", // Cameroon //$NON-NLS-1$
			"cn", // China, mainland //$NON-NLS-1$
			"co", // Colombia //$NON-NLS-1$
			"cr", // Costa Rica //$NON-NLS-1$
			"cu", // Cuba //$NON-NLS-1$
			"cv", // Cape Verde //$NON-NLS-1$
			"cx", // Christmas Island //$NON-NLS-1$
			"cy", // Cyprus //$NON-NLS-1$
			"cz", // Czech Republic //$NON-NLS-1$
			"de", // Germany //$NON-NLS-1$
			"dj", // Djibouti //$NON-NLS-1$
			"dk", // Denmark //$NON-NLS-1$
			"dm", // Dominica //$NON-NLS-1$
			"do", // Dominican Republic //$NON-NLS-1$
			"dz", // Algeria //$NON-NLS-1$
			"ec", // Ecuador //$NON-NLS-1$
			"ee", // Estonia //$NON-NLS-1$
			"eg", // Egypt //$NON-NLS-1$
			"er", // Eritrea //$NON-NLS-1$
			"es", // Spain //$NON-NLS-1$
			"et", // Ethiopia //$NON-NLS-1$
			"eu", // European Union //$NON-NLS-1$
			"fi", // Finland //$NON-NLS-1$
			"fj", // Fiji //$NON-NLS-1$
			"fk", // Falkland Islands //$NON-NLS-1$
			"fm", // Federated States of Micronesia //$NON-NLS-1$
			"fo", // Faroe Islands //$NON-NLS-1$
			"fr", // France //$NON-NLS-1$
			"ga", // Gabon //$NON-NLS-1$
			"gb", // Great Britain (United Kingdom) //$NON-NLS-1$
			"gd", // Grenada //$NON-NLS-1$
			"ge", // Georgia //$NON-NLS-1$
			"gf", // French Guiana //$NON-NLS-1$
			"gg", // Guernsey //$NON-NLS-1$
			"gh", // Ghana //$NON-NLS-1$
			"gi", // Gibraltar //$NON-NLS-1$
			"gl", // Greenland //$NON-NLS-1$
			"gm", // The Gambia //$NON-NLS-1$
			"gn", // Guinea //$NON-NLS-1$
			"gp", // Guadeloupe //$NON-NLS-1$
			"gq", // Equatorial Guinea //$NON-NLS-1$
			"gr", // Greece //$NON-NLS-1$
			"gs", // South Georgia and the South Sandwich Islands //$NON-NLS-1$
			"gt", // Guatemala //$NON-NLS-1$
			"gu", // Guam //$NON-NLS-1$
			"gw", // Guinea-Bissau //$NON-NLS-1$
			"gy", // Guyana //$NON-NLS-1$
			"hk", // Hong Kong //$NON-NLS-1$
			"hm", // Heard Island and McDonald Islands //$NON-NLS-1$
			"hn", // Honduras //$NON-NLS-1$
			"hr", // Croatia (Hrvatska) //$NON-NLS-1$
			"ht", // Haiti //$NON-NLS-1$
			"hu", // Hungary //$NON-NLS-1$
			"id", // Indonesia //$NON-NLS-1$
			"ie", // Ireland (Éire) //$NON-NLS-1$
			"il", // Israel //$NON-NLS-1$
			"im", // Isle of Man //$NON-NLS-1$
			"in", // India //$NON-NLS-1$
			"io", // British Indian Ocean Territory //$NON-NLS-1$
			"iq", // Iraq //$NON-NLS-1$
			"ir", // Iran //$NON-NLS-1$
			"is", // Iceland //$NON-NLS-1$
			"it", // Italy //$NON-NLS-1$
			"je", // Jersey //$NON-NLS-1$
			"jm", // Jamaica //$NON-NLS-1$
			"jo", // Jordan //$NON-NLS-1$
			"jp", // Japan //$NON-NLS-1$
			"ke", // Kenya //$NON-NLS-1$
			"kg", // Kyrgyzstan //$NON-NLS-1$
			"kh", // Cambodia (Khmer) //$NON-NLS-1$
			"ki", // Kiribati //$NON-NLS-1$
			"km", // Comoros //$NON-NLS-1$
			"kn", // Saint Kitts and Nevis //$NON-NLS-1$
			"kp", // North Korea //$NON-NLS-1$
			"kr", // South Korea //$NON-NLS-1$
			"kw", // Kuwait //$NON-NLS-1$
			"ky", // Cayman Islands //$NON-NLS-1$
			"kz", // Kazakhstan //$NON-NLS-1$
			"la", // Laos (currently being marketed as the official domain for Los Angeles) //$NON-NLS-1$
			"lb", // Lebanon //$NON-NLS-1$
			"lc", // Saint Lucia //$NON-NLS-1$
			"li", // Liechtenstein //$NON-NLS-1$
			"lk", // Sri Lanka //$NON-NLS-1$
			"lr", // Liberia //$NON-NLS-1$
			"ls", // Lesotho //$NON-NLS-1$
			"lt", // Lithuania //$NON-NLS-1$
			"lu", // Luxembourg //$NON-NLS-1$
			"lv", // Latvia //$NON-NLS-1$
			"ly", // Libya //$NON-NLS-1$
			"ma", // Morocco //$NON-NLS-1$
			"mc", // Monaco //$NON-NLS-1$
			"md", // Moldova //$NON-NLS-1$
			"me", // Montenegro //$NON-NLS-1$
			"mg", // Madagascar //$NON-NLS-1$
			"mh", // Marshall Islands //$NON-NLS-1$
			"mk", // Republic of Macedonia //$NON-NLS-1$
			"ml", // Mali //$NON-NLS-1$
			"mm", // Myanmar //$NON-NLS-1$
			"mn", // Mongolia //$NON-NLS-1$
			"mo", // Macau //$NON-NLS-1$
			"mp", // Northern Mariana Islands //$NON-NLS-1$
			"mq", // Martinique //$NON-NLS-1$
			"mr", // Mauritania //$NON-NLS-1$
			"ms", // Montserrat //$NON-NLS-1$
			"mt", // Malta //$NON-NLS-1$
			"mu", // Mauritius //$NON-NLS-1$
			"mv", // Maldives //$NON-NLS-1$
			"mw", // Malawi //$NON-NLS-1$
			"mx", // Mexico //$NON-NLS-1$
			"my", // Malaysia //$NON-NLS-1$
			"mz", // Mozambique //$NON-NLS-1$
			"na", // Namibia //$NON-NLS-1$
			"nc", // New Caledonia //$NON-NLS-1$
			"ne", // Niger //$NON-NLS-1$
			"nf", // Norfolk Island //$NON-NLS-1$
			"ng", // Nigeria //$NON-NLS-1$
			"ni", // Nicaragua //$NON-NLS-1$
			"nl", // Netherlands //$NON-NLS-1$
			"no", // Norway //$NON-NLS-1$
			"np", // Nepal //$NON-NLS-1$
			"nr", // Nauru //$NON-NLS-1$
			"nu", // Niue //$NON-NLS-1$
			"nz", // New Zealand //$NON-NLS-1$
			"om", // Oman //$NON-NLS-1$
			"pa", // Panama //$NON-NLS-1$
			"pe", // Peru //$NON-NLS-1$
			"pf", // French Polynesia With Clipperton Island //$NON-NLS-1$
			"pg", // Papua New Guinea //$NON-NLS-1$
			"ph", // Philippines //$NON-NLS-1$
			"pk", // Pakistan //$NON-NLS-1$
			"pl", // Poland //$NON-NLS-1$
			"pm", // Saint-Pierre and Miquelon //$NON-NLS-1$
			"pn", // Pitcairn Islands //$NON-NLS-1$
			"pr", // Puerto Rico //$NON-NLS-1$
			"ps", // Palestinian territories (PA-controlled West Bank and Gaza Strip) //$NON-NLS-1$
			"pt", // Portugal //$NON-NLS-1$
			"pw", // Palau //$NON-NLS-1$
			"py", // Paraguay //$NON-NLS-1$
			"qa", // Qatar //$NON-NLS-1$
			"re", // Réunion //$NON-NLS-1$
			"ro", // Romania //$NON-NLS-1$
			"rs", // Serbia //$NON-NLS-1$
			"ru", // Russia //$NON-NLS-1$
			"rw", // Rwanda //$NON-NLS-1$
			"sa", // Saudi Arabia //$NON-NLS-1$
			"sb", // Solomon Islands //$NON-NLS-1$
			"sc", // Seychelles //$NON-NLS-1$
			"sd", // Sudan //$NON-NLS-1$
			"se", // Sweden //$NON-NLS-1$
			"sg", // Singapore //$NON-NLS-1$
			"sh", // Saint Helena //$NON-NLS-1$
			"si", // Slovenia //$NON-NLS-1$
			"sj", // Svalbard and Jan Mayen Islands Not in use (Norwegian dependencies; see .no) //$NON-NLS-1$
			"sk", // Slovakia //$NON-NLS-1$
			"sl", // Sierra Leone //$NON-NLS-1$
			"sm", // San Marino //$NON-NLS-1$
			"sn", // Senegal //$NON-NLS-1$
			"so", // Somalia //$NON-NLS-1$
			"sr", // Suriname //$NON-NLS-1$
			"st", // São Tomé and Príncipe //$NON-NLS-1$
			"su", // Soviet Union (deprecated) //$NON-NLS-1$
			"sv", // El Salvador //$NON-NLS-1$
			"sy", // Syria //$NON-NLS-1$
			"sz", // Swaziland //$NON-NLS-1$
			"tc", // Turks and Caicos Islands //$NON-NLS-1$
			"td", // Chad //$NON-NLS-1$
			"tf", // French Southern and Antarctic Lands //$NON-NLS-1$
			"tg", // Togo //$NON-NLS-1$
			"th", // Thailand //$NON-NLS-1$
			"tj", // Tajikistan //$NON-NLS-1$
			"tk", // Tokelau //$NON-NLS-1$
			"tl", // East Timor (deprecated old code) //$NON-NLS-1$
			"tm", // Turkmenistan //$NON-NLS-1$
			"tn", // Tunisia //$NON-NLS-1$
			"to", // Tonga //$NON-NLS-1$
			"tp", // East Timor //$NON-NLS-1$
			"tr", // Turkey //$NON-NLS-1$
			"tt", // Trinidad and Tobago //$NON-NLS-1$
			"tv", // Tuvalu //$NON-NLS-1$
			"tw", // Taiwan, Republic of China //$NON-NLS-1$
			"tz", // Tanzania //$NON-NLS-1$
			"ua", // Ukraine //$NON-NLS-1$
			"ug", // Uganda //$NON-NLS-1$
			"uk", // United Kingdom //$NON-NLS-1$
			"um", // United States Minor Outlying Islands //$NON-NLS-1$
			"us", // United States of America //$NON-NLS-1$
			"uy", // Uruguay //$NON-NLS-1$
			"uz", // Uzbekistan //$NON-NLS-1$
			"va", // Vatican City State //$NON-NLS-1$
			"vc", // Saint Vincent and the Grenadines //$NON-NLS-1$
			"ve", // Venezuela //$NON-NLS-1$
			"vg", // British Virgin Islands //$NON-NLS-1$
			"vi", // U.S. Virgin Islands //$NON-NLS-1$
			"vn", // Vietnam //$NON-NLS-1$
			"vu", // Vanuatu //$NON-NLS-1$
			"wf", // Wallis and Futuna //$NON-NLS-1$
			"ws", // Samoa (formerly Western Samoa) //$NON-NLS-1$
			"ye", // Yemen //$NON-NLS-1$
			"yt", // Mayotte //$NON-NLS-1$
			"yu", // Serbia and Montenegro (originally Yugoslavia) //$NON-NLS-1$
			"za", // South Africa //$NON-NLS-1$
			"zm", // Zambia //$NON-NLS-1$
			"zw", // Zimbabwe //$NON-NLS-1$
	};

	private static final String[] LOCAL_TLDS = new String[] { "localhost", // RFC2606 defined //$NON-NLS-1$
			"localdomain" // Also widely used as localhost.localdomain //$NON-NLS-1$
	};

	private static final List<String> INFRASTRUCTURE_TLD_LIST = Arrays
			.asList(INFRASTRUCTURE_TLDS);
	private static final List<String> GENERIC_TLD_LIST = Arrays
			.asList(GENERIC_TLDS);
	private static final List<String> COUNTRY_CODE_TLD_LIST = Arrays
			.asList(COUNTRY_CODE_TLDS);
	private static final List<String> LOCAL_TLD_LIST = Arrays
			.asList(LOCAL_TLDS);
}
