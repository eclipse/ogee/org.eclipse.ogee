/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.security;

import javax.net.ssl.SSLSocketFactory;

import org.eclipse.ogee.client.connectivity.api.IAuthentication;

/**
 * An implementation of X.509 authentication to use with DefaultRestClient
 */
public class X509Authentication implements IAuthentication {
	private SSLSocketFactory sslSocketFactory;

	/**
	 * Constructor
	 * 
	 * @param sslSocketFactory
	 *            SSLSocketFactory
	 */
	public X509Authentication(SSLSocketFactory sslSocketFactory) {
		this.sslSocketFactory = sslSocketFactory;
	}

	/**
	 * @return SSLSocketFactory
	 */
	public SSLSocketFactory getSslSocketFactory() {
		return sslSocketFactory;
	}
}
