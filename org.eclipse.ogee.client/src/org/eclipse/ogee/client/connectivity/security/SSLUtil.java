/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.security;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

public class SSLUtil {

	/**
	 * Loads keystore from InputStream. The default KeyStore type is used.
	 * 
	 * @param inputStream
	 *            the keystore InputStream, can be FileInputstream
	 * @param password
	 *            the keystore password
	 * @return KeyStore
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 */
	public static KeyStore loadKeyStore(InputStream inputStream, String password)
			throws KeyStoreException, NoSuchAlgorithmException,
			CertificateException, IOException {
		KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		keyStore.load(inputStream, password.toCharArray());

		return keyStore;
	}

	/**
	 * Creates SSLContext using key store and trust store. The trustStore is
	 * optional and can be null. In case the trust store is null, the default
	 * trust store will be used. It will trust the CAs defined in the
	 * JRE_HOME\lib\security\cacerts file.
	 * 
	 * After the SSLContext was created, it can be used to create
	 * SSLSocketFactory that is required for the mutual authentication and it
	 * will use the X509 client certificate. Use <code>getSocketFactory()</code>
	 * method to create the SSLSocketFactory.
	 * 
	 * @param keyStore
	 * @param keyStorePassword
	 *            the password for the keystore (required)
	 * @param trustStore
	 *            (optional)
	 * @return SSLContext
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 * @throws UnrecoverableKeyException
	 * @throws KeyManagementException
	 */
	public static SSLContext createSSLContext(KeyStore keyStore,
			String keyStorePassword, KeyStore trustStore)
			throws NoSuchAlgorithmException, KeyStoreException,
			UnrecoverableKeyException, KeyManagementException {
		if (keyStorePassword == null) {
			throw new KeyStoreException("keyStorePassword cannot be null"); //$NON-NLS-1$
		}

		TrustManagerFactory trustManagerFactory = null;
		trustManagerFactory = TrustManagerFactory
				.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init(trustStore);

		KeyManagerFactory keyManagerFactory = null;
		keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory
				.getDefaultAlgorithm());
		keyManagerFactory.init(keyStore, keyStorePassword.toCharArray());

		SSLContext context = SSLContext.getInstance("SSL"); //$NON-NLS-1$
		context.init(keyManagerFactory.getKeyManagers(),
				trustManagerFactory.getTrustManagers(), new SecureRandom());

		return context;
	}
}
