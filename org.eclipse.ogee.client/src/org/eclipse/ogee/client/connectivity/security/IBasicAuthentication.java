/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.security;

import org.eclipse.ogee.client.connectivity.api.IAuthentication;

/**
 * Interface for the Basic authentication protocol
 * 
 */
public interface IBasicAuthentication extends IAuthentication {
	/**
	 * @return String username
	 */
	public String getUserName();

	/**
	 * @return String password
	 */
	public String getPassword();
}
