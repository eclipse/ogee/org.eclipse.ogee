/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.interceptors;

import java.util.logging.Logger;

import org.eclipse.ogee.client.activator.Activator;
import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestResponse;
import org.eclipse.ogee.client.connectivity.api.IRestResponseInterceptor;
import org.eclipse.ogee.client.connectivity.impl.ConnectivityConstants;
import org.eclipse.ogee.client.connectivity.impl.DefaultRestClient;
import org.eclipse.ogee.client.connectivity.impl.Headers;
import org.eclipse.ogee.client.connectivity.security.IBasicAuthentication;
import org.eclipse.ogee.client.exceptions.RestClientException;
import org.eclipse.ogee.client.util.Base64;
import org.eclipse.ogee.client.util.TraceLogger;

/**
 * An intercepter that adds an "Authorization" header to every response of Basic
 * auth.
 * 
 */
public class BasicAuthenticationResponseInterceptor implements
		IRestResponseInterceptor {
	private String authorizationString;

	private final static Logger LOGGER = Logger
			.getLogger(DefaultRestClient.class.getName());

	private static final TraceLogger traceLogger = TraceLogger
			.getLogger(Activator.PLUGIN_ID);

	/**
	 * Constructor
	 * 
	 * @param basicAuthentication
	 *            IBasicAuthentication object
	 */
	public BasicAuthenticationResponseInterceptor(
			IBasicAuthentication basicAuthentication) {
		super();

		this.authorizationString = ConnectivityConstants.BASIC
				+ Base64.encode(basicAuthentication.getUserName() + ":" //$NON-NLS-1$
						+ basicAuthentication.getPassword());
	}

	@Override
	public void onResponse(IRestRequest restRequest,
			IRestResponse restResponse, IRestClient restClient) {

		traceLogger.trace("Inside onResponse overrriden method");
		String authorizationHeader = restRequest
				.getHeader(ConnectivityConstants.AUTHORIZATION);
		// if authorizationHeader is null it means that this is a first time
		// that the flow enters BasicAuthenticationResponseInterceptor
		// an authentication header will be added only when a response
		// restResponse got 401 code and authorizationHeader is null
		restResponse.getBody();
		traceLogger.trace(
				"Response status code  is:   %1$s    response body is:  %2$s",
				restResponse.getStatusCode(), restResponse.getBody());
		if (restResponse.getStatusCode() == 401 && authorizationHeader == null) {
			restRequest.setHeader(ConnectivityConstants.AUTHORIZATION,
					authorizationString);

			try {
				IRestResponse response = restClient.execute(restRequest);
				// restResponse.setBody(response.getBodyBytes());
				restResponse.setBody(response.getBody());
				restResponse.setHeaders(response.getHeaders());
				restResponse.setStatusCode(response.getStatusCode());
			} catch (RestClientException e) {
				String body = e.getBody();
				Headers headers = e.getHeaders();
				int statusCode = e.getStatusCode();

				setResponse(restResponse, body, headers, statusCode);
				setLoggerErrorMessage(e.getMessage(), body, headers, statusCode);

				traceLogger
						.trace("RestClientException Occurred:  Response status code  is:   %1$s    response body is:  %2$s response headers:    %3$s",
								statusCode, body, headers);
			}
		}
	}

	private void setResponse(IRestResponse restResponse, String body,
			Headers headers, int statusCode) {
		restResponse.setBody(body);
		restResponse.setHeaders(headers);
		restResponse.setStatusCode(statusCode);
	}

	private void setLoggerErrorMessage(String message, String body,
			Headers headers, int statusCode) {
		String errorMessage = statusCode + "\n";

		if (message != null) {
			errorMessage = errorMessage + message + "\n";
		}

		if (body != null) {
			errorMessage = errorMessage + body + "\n";
		}

		if (headers != null) {
			errorMessage = errorMessage + headers + "\n";
		}

		LOGGER.severe(errorMessage);
	}
}
