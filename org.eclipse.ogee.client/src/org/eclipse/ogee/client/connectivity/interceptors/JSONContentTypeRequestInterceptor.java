/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.interceptors;

import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestRequestInterceptor;

/**
 * An intercepter that adds a "content-type" header to every request of
 * application/json.
 * 
 */
public class JSONContentTypeRequestInterceptor implements
		IRestRequestInterceptor {

	private static final String APPLICATION_JSON = "application/json"; //$NON-NLS-1$

	@Override
	public void onRequest(IRestRequest restRequest, IRestClient restClient) {
		if (restRequest.getContentType() == null) {
			restRequest.setContentType(APPLICATION_JSON);
		}
	}

}
