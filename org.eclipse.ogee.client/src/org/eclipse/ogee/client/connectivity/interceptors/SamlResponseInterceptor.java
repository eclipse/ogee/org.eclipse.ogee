/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.interceptors;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.eclipse.ogee.client.connectivity.api.IAuthentication;
import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestRequest.Method;
import org.eclipse.ogee.client.connectivity.api.IRestResponse;
import org.eclipse.ogee.client.connectivity.api.IRestResponseInterceptor;
import org.eclipse.ogee.client.connectivity.impl.DefaultRestClient;
import org.eclipse.ogee.client.connectivity.impl.Headers;
import org.eclipse.ogee.client.connectivity.impl.RestRequest;
import org.eclipse.ogee.client.connectivity.interceptors.FormAuthenticationResponseInterceptor.FormRestRequest;
import org.eclipse.ogee.client.connectivity.security.IBasicAuthentication;
import org.eclipse.ogee.client.exceptions.RestClientException;

/**
 * An intercepter that handles SAML authentication.
 * 
 */
public class SamlResponseInterceptor implements IRestResponseInterceptor {

	private static final String LOCATION = "location";
	private static final String REFERER = "Referer";
	private static final String APPLICATION_FORM_URLENCODED = "application/x-www-form-urlencoded";

	private final static Logger LOGGER = Logger
			.getLogger(DefaultRestClient.class.getName());
	private IAuthentication basicAuthentication;

	private List<String> setCookies;
	private String lastReferer;

	/**
	 * SAML Response interceptor must be initialized with username and password
	 * in case of basic authentication
	 * 
	 * @param basicAuthentication
	 */
	public SamlResponseInterceptor(IAuthentication basicAuthentication) // TODO
																		// change
																		// parameter
																		// type
																		// to
																		// IAuthentication
																		// and
																		// handle
																		// 2
																		// flows:
																		// username/password
																		// based
																		// and
																		// x.509
																		// certificate
	{
		this.basicAuthentication = basicAuthentication;
	}

	@Override
	public void onResponse(IRestRequest restRequest,
			IRestResponse restResponse, IRestClient restClient) {
		if (restRequest == null || restResponse == null || restClient == null
				|| restRequest instanceof SamlRestRequest
				|| restRequest instanceof FormRestRequest) {
			return;
		}

		try {
			// creates new SAMLRestRequest
			SamlRestRequest request = new SamlRestRequest(restRequest.getUrl());
			request.setMethod(restRequest.getMethod());
			request.setHeaders(restRequest.getHeaders());
			request.setBody(restRequest.getBody());
			request.setContentType(restRequest.getContentType());

			// save first response cookies
			this.setCookies = restResponse.getHeaders("set-cookie");

			// handles SAML redirects
			IRestResponse response = handleSamlRedirect(request, restResponse,
					restClient);

			// handles SAML: SP POST Request - IdP POST Response
			response = handleSamlPostRequest(request, response, restClient);

			// execute the original request
			if (response.getStatusCode() == 302) {
				response = handleOriginalRequest(restRequest, restClient,
						response);
			}

			if (response.getStatusCode() == 202) // Accepted by Server for Batch
													// processing
			{
				return;
			}
			// sets SAML response
			restResponse.setStatusCode(response.getStatusCode());
			restResponse.setBody(response.getBody());
			restResponse.setBody(response.getBodyBytes());
			restResponse.setHeaders(response.getHeaders());
		} catch (RestClientException e) {
			String body = e.getBody();
			Headers headers = e.getHeaders();
			int statusCode = e.getStatusCode();

			setResponse(restResponse, body, headers, statusCode);
			setLoggerErrorMessage(e.getMessage(), body, headers, statusCode);
		} catch (UnsupportedEncodingException e) {
			RestClientException rce = new RestClientException(
					"UnsupportedEncodingException", e);

			String body = rce.getBody();
			Headers headers = rce.getHeaders();
			int statusCode = rce.getStatusCode();

			setResponse(restResponse, body, headers, statusCode);
			setLoggerErrorMessage(rce.getMessage(), body, headers, statusCode);
		}
	}

	public IAuthentication getBasicAuthentication() {

		return this.basicAuthentication;
	}

	private IRestResponse handleOriginalRequest(IRestRequest restRequest,
			IRestClient restClient, IRestResponse response)
			throws RestClientException {
		String location = response.getHeader(LOCATION);
		IRestRequest lastRequest = new SamlRestRequest(location);

		if (this.lastReferer != null) {
			lastRequest.setHeader(REFERER, this.lastReferer);
		}

		lastRequest.setMethod(restRequest.getMethod());
		lastRequest.setBody(restRequest.getBody());

		response = restClient.execute(lastRequest);
		return response;
	}

	/**
	 * Handles SAML request: SP POST Request - IdP POST Response
	 * 
	 * @param originalRequest
	 * @param request
	 * @param restResponse
	 * @param restClient
	 * @param body
	 * @return
	 * @throws RestClientException
	 * @throws UnsupportedEncodingException
	 */
	private IRestResponse handleSamlPostRequest(SamlRestRequest request,
			IRestResponse response, IRestClient restClient)
			throws UnsupportedEncodingException, RestClientException {
		String responseBody = response.getBody();
		if (response.getStatusCode() != 200 || responseBody == null) {
			return response;
		}

		String samlType = null;
		if (responseBody.contains("SAMLRequest")) {
			samlType = "SAMLRequest";
		} else if (responseBody.contains("SAMLResponse")) {
			samlType = "SAMLResponse";

			setCookies(restClient);
		}

		if (samlType == null) {
			return response;
		}

		IRestResponse samlPostResponse = response;

		String actionString = "action=";
		int indexOfActionStart = responseBody.indexOf(actionString);
		int indexOfActionEnd = responseBody.indexOf("\"", indexOfActionStart
				+ actionString.length() + 1);
		String action = responseBody.substring(indexOfActionStart
				+ actionString.length() + 1, indexOfActionEnd);

		String relayStateStartString = "RelayState\" value=\"";
		String relayStateEndString = "\"";
		int indexOfRelayStateStart = responseBody
				.indexOf(relayStateStartString);
		if (indexOfRelayStateStart == -1) {
			relayStateStartString = "RelayState\" value=\'";
			relayStateEndString = "\'";
			indexOfRelayStateStart = responseBody
					.indexOf(relayStateStartString);
		}
		int indexOfRelayStateEnd = responseBody.indexOf(relayStateEndString,
				indexOfRelayStateStart + relayStateStartString.length() + 1);

		String relayState = responseBody.substring(indexOfRelayStateStart
				+ relayStateStartString.length(), indexOfRelayStateEnd);

		String samlResponseStartString = samlType + "\" value=\"";
		String samlResponseEndString = "\"";
		int indexOfSamlResponseStart = responseBody
				.indexOf(samlResponseStartString);
		if (indexOfSamlResponseStart == -1) {
			samlResponseStartString = samlType + "\" value=\'";
			samlResponseEndString = "\'";
			indexOfSamlResponseStart = responseBody
					.indexOf(samlResponseStartString);
		}
		int indexOfSamlResponseEnd = responseBody.indexOf(
				samlResponseEndString, indexOfSamlResponseStart
						+ samlResponseStartString.length() + 1);
		String samlResponse = responseBody.substring(indexOfSamlResponseStart
				+ samlResponseStartString.length(), indexOfSamlResponseEnd);
		samlResponse = URLEncoder.encode(samlResponse, "UTF-8");

		String parameters = samlType + "=" + samlResponse + "&RelayState="
				+ relayState;

		this.lastReferer = request.getUrl();
		URI actionUri = URI.create(action);
		if (actionUri.getScheme() == null) {
			if (this.basicAuthentication == null
					|| request.authenticationCounter > 0) {
				RestClientException restClientException = new RestClientException();
				restClientException.setStatusCode(401);
				restClientException.setBody("SAML Authentication failed");
				return restClientException;

			}

			URI requestUri = URI.create(lastReferer);
			String port = (String) (requestUri.getPort() == -1 ? "" : ":"
					+ requestUri.getPort());
			String refererHost = requestUri.getScheme() + "://"
					+ requestUri.getHost() + port;
			parameters = parameters
					+ "&j_username="
					+ ((IBasicAuthentication) this.basicAuthentication)
							.getUserName()
					+ "&j_password="
					+ ((IBasicAuthentication) this.basicAuthentication)
							.getPassword() + "&_rememberme=on"; // TODO: need to
																// understand if
																// names
			// of username and password are always: j_username and j_password
			action = refererHost + action;
			this.lastReferer = action;
			request.authenticationCounter++;
		}

		request.setUrl(action);
		request.setBody(parameters);
		request.getHeaders().clear();
		request.setHeader(REFERER, this.lastReferer);
		request.setMethod(Method.POST);
		request.setContentType(APPLICATION_FORM_URLENCODED);

		response = restClient.execute(request);
		Set<String> keys = restClient.getCookies().keySet();
		for (String key : keys) {
			if (key.startsWith("JTENANTSESSIONID")) // TODO need to understand
													// if JTENANTSESSIONID
													// exists in all saml impl
													// (generic)
			{
				return response;
			}
		}

		samlPostResponse = handleSamlPostRequest(request, response, restClient);

		return samlPostResponse;
	}

	// set only cookies got after the first original request
	private void setCookies(IRestClient restClient) {
		restClient.getCookies().clear();

		for (String cookie : this.setCookies) {
			String[] splitCookie = cookie.split("=");
			String name = splitCookie[0];
			String value = splitCookie[1];

			restClient.setCookie(name, value);
		}
	}

	/**
	 * Handles SAML request redirects
	 * 
	 * @param request
	 * @param response
	 * @param restClient
	 * @return
	 * @throws RestClientException
	 */
	private IRestResponse handleSamlRedirect(IRestRequest request,
			IRestResponse response, IRestClient restClient)
			throws RestClientException {
		String location = response.getHeader(LOCATION);
		int statusCode = response.getStatusCode();
		IRestResponse redirectResponse = response;

		if ((statusCode == 302 || statusCode == 301)
				&& location != null
				&& (location.contains("SAMLart") || location
						.contains("SAMLRequest"))) {
			URI locationUri = URI.create(location);
			if (locationUri.getScheme() == null) {
				if (this.basicAuthentication == null) {
					response.setStatusCode(401);
					return response;
				}

				URI requestUri = URI.create(request.getUrl());
				String port = (String) (requestUri.getPort() == -1 ? "" : ":"
						+ requestUri.getPort());
				String refererHost = requestUri.getScheme() + "://"
						+ requestUri.getHost() + port;
				location = refererHost + location; // TODO: should we provide
													// here j_username and
													// j_password ?
			}

			request.setUrl(location);
			request.setMethod(Method.GET);

			if (location.contains("SAMLart")) {
				setCookies(restClient);
			}

			response = restClient.execute(request);
			if (location.contains("SAMLart")) {
				redirectResponse = response;

				return redirectResponse;
			}

			response = restClient.execute(request);

			redirectResponse = handleSamlRedirect(request, response, restClient);
		}

		return redirectResponse;
	}

	/**
	 * private SAML request class
	 */
	protected static class SamlRestRequest extends RestRequest {
		public int authenticationCounter = 0;

		public SamlRestRequest(Method method, String url) {
			super(method, url);
		}

		public SamlRestRequest(String url) {
			super(url);
		}
	}

	/**
	 * Sets rest response
	 * 
	 * @param restResponse
	 * @param body
	 * @param headers
	 * @param statusCode
	 */
	private void setResponse(IRestResponse restResponse, String body,
			Headers headers, int statusCode) {
		restResponse.setBody(body);
		restResponse.setHeaders(headers);
		restResponse.setStatusCode(statusCode);
	}

	/**
	 * Sets logger error message
	 * 
	 * @param message
	 * @param body
	 * @param headers
	 * @param statusCode
	 */
	private void setLoggerErrorMessage(String message, String body,
			Headers headers, int statusCode) {
		String errorMessage = statusCode + "\n";

		if (message != null) {
			errorMessage = errorMessage + message + "\n";
		}

		if (body != null) {
			errorMessage = errorMessage + body + "\n";
		}

		if (headers != null) {
			errorMessage = errorMessage + headers + "\n";
		}

		LOGGER.severe(errorMessage);
	}
}
