/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.interceptors;

import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestRequestInterceptor;

/**
 * An interceptor that adds a "DataServiceVersion" header to every request. The
 * value of the header is defined in the constant
 * DATA_SERVICE_VERSION_HEADER_VALUE. If the client doesn’t send this header,
 * the server will respond with the maximum version it supports.
 * 
 * 
 */
public class DataServiceVersionHeaderInterceptor implements
		IRestRequestInterceptor {
	public static final String DATA_SERVICE_VERSION_HEADER = "DataServiceVersion"; //$NON-NLS-1$
	public static final String DATA_SERVICE_VERSION_HEADER_VALUE = "2.0"; //$NON-NLS-1$

	@Override
	public void onRequest(IRestRequest restRequest, IRestClient restClient) {
		if (restRequest.getHeader(DATA_SERVICE_VERSION_HEADER) == null) {
			restRequest.setHeader(DATA_SERVICE_VERSION_HEADER,
					DATA_SERVICE_VERSION_HEADER_VALUE);
		}
	}
}
