/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.interceptors;

import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestRequestInterceptor;
import org.eclipse.ogee.client.parser.Representation;

/**
 * An interceptor that adds an "Accept" header to every request. The value of
 * the header is defined in the constant ACCEPT_HEADER_VALUE and it is
 * "application/xml,application/xhtml+xml,text/html" This means that the
 * preferred content is OData (application/xml). 
 * 
 * 
 */
public class AcceptHeaderInterceptor implements IRestRequestInterceptor {

	public static final String ACCEPT_HEADER_VALUE_ATOM = "text/html, application/xhtml+xml, */*"; //$NON-NLS-1$
	public static final String ACCEPT_HEADER_VALUE_JSON = "text/html, application/json, */*"; //$NON-NLS-1$
	public static final String ACCEPT_HEADER = "Accept"; //$NON-NLS-1$

	@Override
	public void onRequest(IRestRequest restRequest, IRestClient restClient) {
		if (restClient.getRepresentation().equals(Representation.JSON)) {
			restRequest.setHeader(ACCEPT_HEADER, ACCEPT_HEADER_VALUE_JSON);
		} else if (restClient.getRepresentation().equals(Representation.ATOM)) {
			restRequest.setHeader(ACCEPT_HEADER, ACCEPT_HEADER_VALUE_ATOM);
		}
	}
}
