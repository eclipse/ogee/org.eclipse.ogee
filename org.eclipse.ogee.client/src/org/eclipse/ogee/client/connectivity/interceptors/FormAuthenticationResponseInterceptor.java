/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.interceptors;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.logging.Logger;

import org.eclipse.ogee.client.connectivity.api.IAuthentication;
import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestRequest.Method;
import org.eclipse.ogee.client.connectivity.api.IRestResponse;
import org.eclipse.ogee.client.connectivity.api.IRestResponseInterceptor;
import org.eclipse.ogee.client.connectivity.impl.ConnectivityConstants;
import org.eclipse.ogee.client.connectivity.impl.DefaultRestClient;
import org.eclipse.ogee.client.connectivity.impl.Headers;
import org.eclipse.ogee.client.connectivity.impl.RestRequest;
import org.eclipse.ogee.client.connectivity.interceptors.SamlResponseInterceptor.SamlRestRequest;
import org.eclipse.ogee.client.connectivity.security.IBasicAuthentication;
import org.eclipse.ogee.client.exceptions.RestClientException;

/**
 * An intercepter that handles form based authentication.
 * 
 */
public class FormAuthenticationResponseInterceptor implements
		IRestResponseInterceptor {
	private static final String LOCATION = "location";
	private static final String REFERER = "Referer";
	private static final String APPLICATION_FORM_URLENCODED = "application/x-www-form-urlencoded";

	private final static Logger LOGGER = Logger
			.getLogger(DefaultRestClient.class.getName());
	private IAuthentication basicAuthentication;
	private String lastReferer;

	/**
	 * Form Response interceptor must be initialized with username and password
	 * in case of basic authentication
	 * 
	 * @param basicAuthentication
	 */
	public FormAuthenticationResponseInterceptor(
			IAuthentication basicAuthentication) {
		this.basicAuthentication = basicAuthentication;
	}

	@Override
	public void onResponse(IRestRequest restRequest,
			IRestResponse restResponse, IRestClient restClient) {
		if (restRequest == null || restResponse == null || restClient == null
				|| restRequest instanceof FormRestRequest
				|| restRequest instanceof SamlRestRequest) {
			return;
		}

		String header = restRequest
				.getHeader(ConnectivityConstants.X_CSRF_TOKEN);
		if (header != null
				&& header.equalsIgnoreCase(ConnectivityConstants.FETCH))
			return;

		try {
			// creates new FormRestRequest
			FormRestRequest request = new FormRestRequest(restRequest.getUrl());
			request.setMethod(restRequest.getMethod());
			request.setHeaders(restRequest.getHeaders());
			request.setBody(restRequest.getBody());
			request.setContentType(restRequest.getContentType());

			IRestResponse response = handleFormPostRequest(request,
					restResponse, restClient);

			if (response.getStatusCode() == 302) {
				response = handleRedirect(restRequest, restClient, response);
			}

			if (response.getStatusCode() == 202)// Accepted by Server for Batch
												// processing
			{
				return;
			}

			// sets response
			restResponse.setStatusCode(response.getStatusCode());
			restResponse.setBody(response.getBody());
			restResponse.setBody(response.getBodyBytes());
			restResponse.setHeaders(response.getHeaders());
		} catch (RestClientException e) {
			String body = e.getBody();
			Headers headers = e.getHeaders();
			int statusCode = e.getStatusCode();

			setResponse(restResponse, body, headers, statusCode);
			setLoggerErrorMessage(e.getMessage(), body, headers, statusCode);
		} catch (UnsupportedEncodingException e) {
			RestClientException rce = new RestClientException(
					"UnsupportedEncodingException", e);

			String body = rce.getBody();
			Headers headers = rce.getHeaders();
			int statusCode = rce.getStatusCode();

			setResponse(restResponse, body, headers, statusCode);
			setLoggerErrorMessage(rce.getMessage(), body, headers, statusCode);
		}
	}

	private IRestResponse handleRedirect(IRestRequest restRequest,
			IRestClient restClient, IRestResponse response)
			throws RestClientException {
		String location = response.getHeader(LOCATION);
		IRestRequest lastRequest = new FormRestRequest(location);

		if (this.lastReferer != null) {
			lastRequest.setHeader(REFERER, this.lastReferer);
		}

		lastRequest.setMethod(restRequest.getMethod());
		lastRequest.setBody(restRequest.getBody());

		response = restClient.execute(lastRequest);
		return response;
	}

	private IRestResponse handleFormPostRequest(FormRestRequest request,
			IRestResponse response, IRestClient restClient)
			throws UnsupportedEncodingException, RestClientException {
		String responseBody = response.getBody();
		if (response.getStatusCode() != 200 || responseBody == null) {
			return response;
		}

		IRestResponse formPostResponse = response;

		String actionString = "action=";
		int indexOfActionStart = responseBody.indexOf(actionString);
		if (indexOfActionStart == -1)
			return response;
		int indexOfActionEnd = responseBody.indexOf("\"", indexOfActionStart
				+ actionString.length() + 1);
		String action = responseBody.substring(indexOfActionStart
				+ actionString.length() + 1, indexOfActionEnd);
		String saltStartString = "j_salt\" value=";
		String saltEndString = "\"";
		int indexOfSaltStart = responseBody.indexOf("j_salt\" value=");
		int indexOfSaltEnd = responseBody.indexOf(saltEndString,
				indexOfSaltStart + saltStartString.length() + 1);
		String salt = responseBody
				.substring(indexOfSaltStart + saltStartString.length() + 1,
						indexOfSaltEnd);
		salt = URLEncoder.encode(salt, "UTF-8");
		String parameters = null;
		this.lastReferer = request.getUrl();
		URI actionUri = URI.create(action);
		if (actionUri.getScheme() == null) {
			if (this.basicAuthentication == null) {
				RestClientException restClientException = new RestClientException();
				restClientException.setStatusCode(401);
				restClientException.setBody("Form Authentication failed");
				return restClientException;
			}

			URI requestUri = URI.create(lastReferer);
			String port = (String) (requestUri.getPort() == -1 ? "" : ":"
					+ requestUri.getPort());
			String referer = requestUri.getScheme() + "://"
					+ requestUri.getHost() + port + requestUri.getPath();
			if (!referer.endsWith("/"))
				referer = referer + "/";
			parameters = "&no_cert_storing=on"
					+ "&j_salt="
					+ salt
					+ "&j_username="
					+ ((IBasicAuthentication) this.basicAuthentication)
							.getUserName()
					+ "&j_password="
					+ ((IBasicAuthentication) this.basicAuthentication)
							.getPassword() + "&uidPasswordLogon=Log On";
			action = referer + action;
			this.lastReferer = action;
		}

		request.setUrl(action);
		request.setBody(parameters);
		request.getHeaders().clear();
		request.setHeader(REFERER, this.lastReferer);
		request.setMethod(Method.POST);
		request.setContentType(APPLICATION_FORM_URLENCODED);

		response = restClient.execute(request);

		formPostResponse = handleFormPostRequest(request, response, restClient);

		return formPostResponse;
	}

	/**
	 * private Form request class
	 */
	protected static class FormRestRequest extends RestRequest {
		public FormRestRequest(Method method, String url) {
			super(method, url);
		}

		public FormRestRequest(String url) {
			super(url);
		}
	}

	/**
	 * Sets rest response
	 * 
	 * @param restResponse
	 * @param body
	 * @param headers
	 * @param statusCode
	 */
	private void setResponse(IRestResponse restResponse, String body,
			Headers headers, int statusCode) {
		restResponse.setBody(body);
		restResponse.setHeaders(headers);
		restResponse.setStatusCode(statusCode);
	}

	/**
	 * Sets logger error message
	 * 
	 * @param message
	 * @param body
	 * @param headers
	 * @param statusCode
	 */
	private void setLoggerErrorMessage(String message, String body,
			Headers headers, int statusCode) {
		String errorMessage = statusCode + "\n";

		if (message != null) {
			errorMessage = errorMessage + message + "\n";
		}

		if (body != null) {
			errorMessage = errorMessage + body + "\n";
		}

		if (headers != null) {
			errorMessage = errorMessage + headers + "\n";
		}

		LOGGER.severe(errorMessage);
	}
}
