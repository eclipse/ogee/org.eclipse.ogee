/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.interceptors;

import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestRequestInterceptor;
import org.eclipse.ogee.client.connectivity.impl.ConnectivityConstants;
import org.eclipse.ogee.client.connectivity.security.IBasicAuthentication;
import org.eclipse.ogee.client.util.Base64;

/**
 * An intercepter that adds an "Authorization" header to every request of Basic
 * auth.
 * 
 */
public class BasicAuthenticationRequestInterceptor implements
		IRestRequestInterceptor {
	private String authorizationString;

	/**
	 * Constructor
	 * 
	 * @param basicAuthentication
	 *            IBasicAuthentication object
	 */
	public BasicAuthenticationRequestInterceptor(
			IBasicAuthentication basicAuthentication) {
		super();

		this.authorizationString = ConnectivityConstants.BASIC
				+ Base64.encode(basicAuthentication.getUserName() + ":" //$NON-NLS-1$
						+ basicAuthentication.getPassword());
	}

	@Override
	public void onRequest(IRestRequest restRequest, IRestClient restClient) {
		restRequest.setHeader(ConnectivityConstants.AUTHORIZATION,
				authorizationString);
	}

}
