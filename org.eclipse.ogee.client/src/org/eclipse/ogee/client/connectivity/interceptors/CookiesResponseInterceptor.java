/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.interceptors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.ogee.client.activator.Activator;
import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestResponse;
import org.eclipse.ogee.client.connectivity.api.IRestResponseInterceptor;
import org.eclipse.ogee.client.connectivity.impl.ConnectivityConstants;
import org.eclipse.ogee.client.connectivity.impl.Headers;
import org.eclipse.ogee.client.util.TraceLogger;

/**
 * Response interceptor that sets cookies as a result of set-cookie header in
 * the response
 * 
 * 
 */
public class CookiesResponseInterceptor implements IRestResponseInterceptor {

	private static final TraceLogger traceLogger = TraceLogger
			.getLogger(Activator.PLUGIN_ID);

	@Override
	public void onResponse(IRestRequest restRequest,
			IRestResponse restResponse, IRestClient restClient) {

		traceLogger
				.trace("Inside onResponse of CookieResponseInterceptor method");
		Map<String, String> cookies = new HashMap<String, String>();

		Headers headers = restResponse.getHeaders();
		if (headers == null) {
			return;
		}

		List<String> cookiesList = headers
				.get(ConnectivityConstants.SET_COOKIE);

		if (cookiesList == null)
			return;

		for (String cookie : cookiesList) {
			if (cookie == null)
				continue;
			cookie = cookie.substring(0, cookie.indexOf(";")); //$NON-NLS-1$
			String cookieName = cookie.substring(0, cookie.indexOf("=")); //$NON-NLS-1$
			String cookieValue = cookie.substring(
					cookie.indexOf("=") + 1, cookie.length()); //$NON-NLS-1$
			cookies.put(cookieName, cookieValue);
			restClient.setCookie(cookieName, cookieValue);

		}
	}
}
