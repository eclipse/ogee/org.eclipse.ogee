/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.connectivity.interceptors;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.ogee.client.activator.Activator;
import org.eclipse.ogee.client.connectivity.api.IRestClient;
import org.eclipse.ogee.client.connectivity.api.IRestRequest;
import org.eclipse.ogee.client.connectivity.api.IRestRequestInterceptor;
import org.eclipse.ogee.client.connectivity.api.IRestResponse;
import org.eclipse.ogee.client.connectivity.api.IRestResponseInterceptor;
import org.eclipse.ogee.client.connectivity.impl.ConnectivityConstants;
import org.eclipse.ogee.client.connectivity.impl.DefaultRestClient;
import org.eclipse.ogee.client.connectivity.impl.Headers;
import org.eclipse.ogee.client.connectivity.impl.RestRequest;
import org.eclipse.ogee.client.exceptions.RestClientException;
import org.eclipse.ogee.client.util.TraceLogger;
import org.eclipse.ogee.client.util.Util;

/**
 * CSRF interceptor deals with CSRF (Cross-site request forgery) prevention. Act
 * both as a request interceptor and a response interceptor, to identify the
 * need to fetch a CSRF token and attach it to requests. Used in
 * DefaultRestClient implementation.
 * 
 * 
 */
public class CsrfInterceptor implements IRestResponseInterceptor,
		IRestRequestInterceptor {
	private final static Logger LOGGER = Logger
			.getLogger(DefaultRestClient.class.getName());

	private static final TraceLogger traceLogger = TraceLogger
			.getLogger(Activator.PLUGIN_ID);
	private String CSRFToken = null;

	@Override
	public void onResponse(IRestRequest restRequest,
			IRestResponse restResponse, IRestClient restClient) {
		String header = restResponse
				.getHeader(ConnectivityConstants.X_CSRF_TOKEN);
		traceLogger
				.trace("RestClientException Occurred:  Response status code  is:   %1$s    response body is:  %2$s csrfresponse token:    %3$s",
						restResponse.getStatusCode(), restResponse.getBody(),
						header);
		if (restResponse.getStatusCode() == 403 && header != null
				&& header.equalsIgnoreCase(ConnectivityConstants.REQUIRED)) {
			try {
				String token = getCSRFToken(restRequest, restClient);
				this.CSRFToken = token;
				restRequest
						.setHeader(ConnectivityConstants.X_CSRF_TOKEN, token);

				IRestResponse newResponse = restClient.execute(restRequest);
				restResponse.setBody(newResponse.getBody());
				restResponse.setHeaders(newResponse.getHeaders());
				restResponse.setStatusCode(newResponse.getStatusCode());
			} catch (RestClientException e) {
				String body = e.getBody();
				Headers headers = e.getHeaders();
				int statusCode = e.getStatusCode();

				setResponse(restResponse, body, headers, statusCode);
				setLoggerErrorMessage(e.getMessage(), body, headers, statusCode);
			} catch (URISyntaxException e) {
				RestClientException rce = new RestClientException(
						"URISyntaxException", e);

				String body = rce.getBody();
				Headers headers = rce.getHeaders();
				int statusCode = rce.getStatusCode();

				setResponse(restResponse, body, headers, statusCode);
				setLoggerErrorMessage(e.getMessage(), body, headers, statusCode);
			}
		}
	}

	private String getCSRFToken(IRestRequest restRequest, IRestClient restClient)
			throws URISyntaxException {
		if (restRequest instanceof CsrfRestRequest) {
			return null;
		}

		URI requestUri = new URI(restRequest.getUrl());
		String requestUriPath = requestUri.getRawPath();
		if (requestUriPath == null || requestUriPath.isEmpty()) {
			return null;
		}

		// remove all parameters from the url
		requestUriPath = Util.removeURLParameters(requestUriPath);

		IRestRequest request = new CsrfRestRequest(requestUriPath);
		request.setHeader(ConnectivityConstants.X_CSRF_TOKEN,
				ConnectivityConstants.FETCH);
		try {
			IRestResponse response = restClient.execute(request);

			String token = response
					.getHeader(ConnectivityConstants.X_CSRF_TOKEN);

			List<String> cookiesList = response
					.getHeaders(ConnectivityConstants.SET_COOKIE);

			if (cookiesList == null)
				return token;

			for (String cookie : cookiesList) {
				if (cookie == null)
					continue;
				cookie = cookie.substring(0, cookie.indexOf(";")); //$NON-NLS-1$				
			}

			return token;
		} catch (RestClientException e) {
			String body = e.getBody();
			Headers headers = e.getHeaders();
			int statusCode = e.getStatusCode();

			setLoggerErrorMessage(e.getMessage(), body, headers, statusCode);
		}

		return null;
	}

	@Override
	public void onRequest(IRestRequest restRequest, IRestClient restClient) {

		if (this.CSRFToken != null && this.CSRFToken.length() > 0) {
			restRequest.setHeader(ConnectivityConstants.X_CSRF_TOKEN,
					this.CSRFToken);
		}

	}

	private void setResponse(IRestResponse restResponse, String body,
			Headers headers, int statusCode) {
		restResponse.setBody(body);
		restResponse.setHeaders(headers);
		restResponse.setStatusCode(statusCode);
	}

	private void setLoggerErrorMessage(String message, String body,
			Headers headers, int statusCode) {
		String errorMessage = statusCode + "\n";

		if (message != null) {
			errorMessage = errorMessage + message + "\n";
		}

		if (body != null) {
			errorMessage = errorMessage + body + "\n";
		}

		if (headers != null) {
			errorMessage = errorMessage + headers + "\n";
		}

		LOGGER.severe(errorMessage);
	}

	private static class CsrfRestRequest extends RestRequest {
		public CsrfRestRequest(Method method, String url) {
			super(method, url);
		}

		public CsrfRestRequest(String url) {
			super(url);
		}
	}
}
