/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.client.jsonsimple.parser;

public class Yytoken {
	public static final int TYPE_VALUE = 0;// JSON primitive value:
											// string,number,boolean,null
	public static final int TYPE_LEFT_BRACE = 1;
	public static final int TYPE_RIGHT_BRACE = 2;
	public static final int TYPE_LEFT_SQUARE = 3;
	public static final int TYPE_RIGHT_SQUARE = 4;
	public static final int TYPE_COMMA = 5;
	public static final int TYPE_COLON = 6;
	public static final int TYPE_EOF = -1;// end of file

	public int type = 0;
	public Object value = null;

	public Yytoken(int type, Object value) {
		this.type = type;
		this.value = value;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		switch (type) {
		case TYPE_VALUE:
			sb.append("VALUE(").append(value).append(")"); //$NON-NLS-1$ //$NON-NLS-2$
			break;
		case TYPE_LEFT_BRACE:
			sb.append("LEFT BRACE({)"); //$NON-NLS-1$
			break;
		case TYPE_RIGHT_BRACE:
			sb.append("RIGHT BRACE(})"); //$NON-NLS-1$
			break;
		case TYPE_LEFT_SQUARE:
			sb.append("LEFT SQUARE([)"); //$NON-NLS-1$
			break;
		case TYPE_RIGHT_SQUARE:
			sb.append("RIGHT SQUARE(])"); //$NON-NLS-1$
			break;
		case TYPE_COMMA:
			sb.append("COMMA(,)"); //$NON-NLS-1$
			break;
		case TYPE_COLON:
			sb.append("COLON(:)"); //$NON-NLS-1$
			break;
		case TYPE_EOF:
			sb.append("END OF FILE"); //$NON-NLS-1$
			break;
		}
		return sb.toString();
	}
}
