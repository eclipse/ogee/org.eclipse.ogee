/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.EMFEventType;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.validation.util.MessageUtil;

public class PropertyNameConstraint extends ModelConstraint {

	public PropertyNameConstraint() {
		super();
	}

	public PropertyNameConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for properties...
		if (!(object instanceof Property)) {
			return ctx.createSuccessStatus();
		}

		status = checkValidName(ctx, (Property) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkUniqueName(ctx, (Property) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkValidName(IValidationContext ctx, Property property) {
		EMFEventType eType = ctx.getEventType();
		String nameToBeChecked;

		// In case of batch mode
		if (eType == EMFEventType.NULL) {
			nameToBeChecked = property.getName();
			// In case of live mode
		} else {
			nameToBeChecked = (String) ctx.getFeatureNewValue(); // In fact a
																	// safe cast
		}

		if (this.containsInvalidCharacters(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.PropertyConstraint_NameInvalid });
		}

		if (this.hasInvalidLength(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.ModelConstraint_SimpleIdentifierTooLong });
		}

		return ctx.createSuccessStatus();
	}

	private IStatus checkUniqueName(IValidationContext ctx, Property property) {
		EObject eContainer = property.eContainer();
		List<EObject> alreadyVisited = new LinkedList<EObject>();
		return checkUniqueNameInheritedContent(ctx, alreadyVisited, eContainer,
				property);
	}

	private IStatus checkUniqueNameInheritedContent(IValidationContext ctx,
			List<EObject> alreadyVisited, EObject eContainer, Property property) {
		EObject eObject;
		Property p;
		NavigationProperty np;
		String nameToBeChecked = property.getName();

		if (eContainer == null) {
			return ctx.createSuccessStatus();
		} else if (alreadyVisited.contains(eContainer)) {
			return ctx.createSuccessStatus(); // inheritance cycle checked
												// somewhere else...
		}

		Iterator<EObject> iEObjects = eContainer.eContents().iterator();
		while (iEObjects.hasNext()) {
			eObject = iEObjects.next();
			if ((eObject instanceof Property) && eObject != property) {
				p = (Property) eObject;
				if ((p.getName() != null)
						&& p.getName().equals(property.getName())) {

					nameToBeChecked = MessageUtil
							.generateErrorMsg(nameToBeChecked);

					// return ctx.createFailureStatus(new Object[] {
					// property.getName(),
					// Messages.PropertyConstraint_NameNotUnique });
					return ctx.createFailureStatus(new Object[] {
							nameToBeChecked,
							Messages.PropertyConstraint_NameNotUnique });
				}
			} else if (eObject instanceof NavigationProperty) {
				np = (NavigationProperty) eObject;
				if ((np.getName() != null)
						&& np.getName().equals(property.getName())) {

					return ctx.createFailureStatus(new Object[] {
							nameToBeChecked,
							Messages.PropertyConstraint_NameNotUnique });
					// return ctx.createFailureStatus(new Object[] {
					// property.getName(),
					// Messages.PropertyConstraint_NameNotUnique });
				}
			}
		}

		alreadyVisited.add(eContainer);

		if (eContainer instanceof ComplexType) {
			if (property.getName() != null
					&& property.getName().equals(
							((ComplexType) eContainer).getName())) {

				nameToBeChecked = MessageUtil.generateErrorMsg(nameToBeChecked);

				// return ctx.createFailureStatus(new Object[] {
				// property.getName(),
				// Messages.PropertyConstraint_NameNotUnique });
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.PropertyConstraint_NameNotUnique });
			}
			return checkUniqueNameInheritedContent(ctx, alreadyVisited,
					((ComplexType) eContainer).getBaseType(), property);
		} else if (eContainer instanceof EntityType) {
			if (property.getName() != null
					&& property.getName().equals(
							((EntityType) eContainer).getName())) {

				nameToBeChecked = MessageUtil.generateErrorMsg(nameToBeChecked);
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.PropertyConstraint_NameNotUnique });
				// return ctx.createFailureStatus(new Object[] {
				// property.getName(),
				// Messages.PropertyConstraint_NameNotUnique });
			}
			return checkUniqueNameInheritedContent(ctx, alreadyVisited,
					((EntityType) eContainer).getBaseType(), property);
		}

		return ctx.createSuccessStatus();
	}

}
