/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;

public class ReferentialConstraintConstraint extends ModelConstraint {

	public ReferentialConstraintConstraint() {
		super();
	}

	public ReferentialConstraintConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for referential constraint...
		if (!(object instanceof ReferentialConstraint)) {
			return ctx.createSuccessStatus();
		}

		status = checkRoles(ctx, (ReferentialConstraint) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkKeyMappings(ctx, (ReferentialConstraint) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkRoles(IValidationContext ctx,
			ReferentialConstraint referentialConstraint) {
		Association association = (Association) referentialConstraint
				.eContainer();
		if (association == null) {
			return ctx.createSuccessStatus(); // checked already by the Ecore
												// model
		}

		List<Role> ends = association.getEnds();

		if (referentialConstraint.getDependent() == null
				|| referentialConstraint.getPrincipal() == null) {
			return ctx.createSuccessStatus(); // checked already by the Ecore
												// model
		}

		if (!ends.contains(referentialConstraint.getDependent())) {
			return ctx
					.createFailureStatus(new Object[] { Messages.ReferentialConstraintConstraint_DependentRoleNotInScope });
		}
		if (!ends.contains(referentialConstraint.getPrincipal())) {
			return ctx
					.createFailureStatus(new Object[] { Messages.ReferentialConstraintConstraint_PrincipalRoleNotInScope });
		}
		return ctx.createSuccessStatus();
	}

	private IStatus checkKeyMappings(IValidationContext ctx,
			ReferentialConstraint referentialConstraint) {
		Property principalProperty = null;
		Property dependentProperty = null;
		IPropertyTypeUsage dependentTypeUsage = null;
		IPropertyTypeUsage principalTypeUsage = null;
		Iterator<Property> keys = referentialConstraint.getKeyMappings()
				.keySet().iterator();
		List<Property> principalKeys;
		List<Property> dependentKeys;
		List<Property> dependentProps;

		EntityType dependentEntityType = null;
		EntityType principalEntityType = null;

		if (referentialConstraint.getDependent() == null
				|| referentialConstraint.getDependent().getType() == null
				|| referentialConstraint.getPrincipal() == null
				|| referentialConstraint.getPrincipal().getType() == null) {
			return ctx.createSuccessStatus(); // checked already by Ecore model
		}

		dependentEntityType = referentialConstraint.getDependent().getType();
		principalEntityType = referentialConstraint.getPrincipal().getType();

		// Check if all keys of the principal entity type are mapped
		principalKeys = this.getAllKeyProperties(principalEntityType);
		if (principalKeys.size() != referentialConstraint.getKeyMappings()
				.size()) {
			return ctx
					.createFailureStatus(new Object[] { Messages.ReferentialConstraintConstraint_KeyMapping });
		}

		dependentKeys = this.getAllKeyProperties(dependentEntityType);
		dependentProps = this.getAllNonKeyProperties(dependentEntityType);

		while (keys.hasNext()) {
			principalProperty = keys.next();
			dependentProperty = referentialConstraint.getKeyMappings().get(
					principalProperty);
			if (dependentProperty == null) {
				continue; // checked already elsewhere

			}

			// Check if principal property is valid
			if (!principalKeys.contains(principalProperty)) {
				return ctx
						.createFailureStatus(new Object[] { Messages.ReferentialConstraintConstraint_IncorrectPrincipalKey });
			}

			// Check if dependent property is valid
			if (!dependentKeys.contains(dependentProperty)
					&& !dependentProps.contains(dependentProperty)) {
				return ctx
						.createFailureStatus(new Object[] { Messages.ReferentialConstraintConstraint_IncorrectDependentKey });
			}

			// Check if types are matching...
			dependentTypeUsage = dependentProperty.getType();
			principalTypeUsage = principalProperty.getType();
			if (dependentTypeUsage == null || principalTypeUsage == null) {
				continue; // checked elsewhere
			}
			if (!dependentTypeUsage.eClass()
					.equals(principalTypeUsage.eClass())) {
				return ctx
						.createFailureStatus(new Object[] { Messages.ReferentialConstraintConstraint_IncorrectKeyMapping });
			}
			if (dependentTypeUsage instanceof SimpleTypeUsage) {
				if (((SimpleTypeUsage) dependentTypeUsage).getSimpleType() != null
						&& ((SimpleTypeUsage) dependentTypeUsage)
								.getSimpleType().getType() != null
						&& ((SimpleTypeUsage) principalTypeUsage)
								.getSimpleType() != null
						&& !((SimpleTypeUsage) dependentTypeUsage)
								.getSimpleType()
								.getType()
								.equals(((SimpleTypeUsage) principalTypeUsage)
										.getSimpleType().getType())) {
					return ctx
							.createFailureStatus(new Object[] { Messages.ReferentialConstraintConstraint_IncorrectKeyMapping });
				}
			} else if (dependentTypeUsage instanceof ComplexTypeUsage) {
				if (((ComplexTypeUsage) dependentTypeUsage).getComplexType() != null
						&& ((ComplexTypeUsage) dependentTypeUsage)
								.getComplexType().getName() != null
						&& ((ComplexTypeUsage) principalTypeUsage)
								.getComplexType() != null
						&& ((ComplexTypeUsage) dependentTypeUsage)
								.getComplexType()
								.getName()
								.equals(((ComplexTypeUsage) principalTypeUsage)
										.getComplexType().getName())) {
					return ctx
							.createFailureStatus(new Object[] { Messages.ReferentialConstraintConstraint_IncorrectKeyMapping });
				}
			}
		}

		return ctx.createSuccessStatus();
	}

	private List<Property> getAllNonKeyProperties(EntityType entityType) {
		List<Property> props = new LinkedList<Property>();
		List<EntityType> visitedEntityTypes = new LinkedList<EntityType>();
		EntityType baseType = entityType.getBaseType();
		props.addAll(entityType.getProperties());
		visitedEntityTypes.add(entityType);

		while (baseType != null && !visitedEntityTypes.contains(baseType)) {
			props.addAll(baseType.getProperties());
			visitedEntityTypes.add(baseType);
			baseType = baseType.getBaseType();
		}

		return props;
	}

	private List<Property> getAllKeyProperties(EntityType entityType) {
		List<Property> keys = new LinkedList<Property>();
		List<EntityType> visitedEntityTypes = new LinkedList<EntityType>();
		EntityType baseType = entityType.getBaseType();
		keys.addAll(entityType.getKeys());
		visitedEntityTypes.add(entityType);

		while (baseType != null && !visitedEntityTypes.contains(baseType)) {
			keys.addAll(baseType.getKeys());
			visitedEntityTypes.add(baseType);
			baseType = baseType.getBaseType();
		}

		return keys;
	}

}
