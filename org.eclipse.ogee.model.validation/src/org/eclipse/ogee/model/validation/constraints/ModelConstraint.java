/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.regex.Pattern;

import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.model.IModelConstraint;
import org.eclipse.emf.validation.service.IConstraintDescriptor;

public abstract class ModelConstraint extends AbstractModelConstraint implements
		IModelConstraint {

	private static final String nonSpclCharacter = "\\w+"; //$NON-NLS-1$
	private static final String onlyCharacter = "[a-zA-Z_]"; //$NON-NLS-1$
	private static final int maximumLengthOfSimpleIdentifier = 128;
	private static final int maximumLengthOfNameSpace = 511;

	private IConstraintDescriptor descriptor;

	public ModelConstraint() {
		super();
	}

	public ModelConstraint(IConstraintDescriptor descriptor) {
		this.descriptor = descriptor;
	}

	@Override
	public IConstraintDescriptor getDescriptor() {
		return this.descriptor;
	}

	protected boolean hasInvalidNameSpaceLength(String name) {
		if (name != null && name.length() > maximumLengthOfNameSpace) {
			return true;
		}
		return false;
	}

	protected boolean containsInvalidCharactersInNamespace(String name) {

		boolean firstLetterOnlyCharacters = false;

		if (name == null) {
			return false;
		}

		if (name.trim().length() > 0) {
			firstLetterOnlyCharacters = Pattern.matches(onlyCharacter,
					name.substring(0, 1));
		}
		return (!firstLetterOnlyCharacters);
	}

	protected boolean hasInvalidLength(String name) {
		if (name != null && name.length() > maximumLengthOfSimpleIdentifier) {
			return true;
		}
		return false;
	}

	protected boolean containsInvalidCharacters(String name) {
		boolean noSpecialCharacters = false;
		boolean firstLetterOnlyCharacters = false;

		if (name == null) {
			return false;
		}

		noSpecialCharacters = Pattern.matches(nonSpclCharacter, name);

		if (name.trim().length() > 0) {
			firstLetterOnlyCharacters = Pattern.matches(onlyCharacter,
					name.substring(0, 1));
		}
		return (!noSpecialCharacters || !firstLetterOnlyCharacters);
	}

}
