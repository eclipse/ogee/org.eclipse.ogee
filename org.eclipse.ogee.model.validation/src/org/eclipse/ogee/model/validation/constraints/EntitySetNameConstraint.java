/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.EMFEventType;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.validation.util.MessageUtil;

public class EntitySetNameConstraint extends ModelConstraint {

	public EntitySetNameConstraint() {
		super();
	}

	public EntitySetNameConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for entity sets...
		if (!(object instanceof EntitySet)) {
			return ctx.createSuccessStatus();
		}

		status = checkValidName(ctx, (EntitySet) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkUniqueName(ctx, (EntitySet) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkValidName(IValidationContext ctx, EntitySet entitySet) {
		EMFEventType eType = ctx.getEventType();
		String nameToBeChecked;

		// In case of batch mode
		if (eType == EMFEventType.NULL) {
			nameToBeChecked = entitySet.getName();
			// In case of live mode
		} else {
			nameToBeChecked = (String) ctx.getFeatureNewValue(); // In fact a
																	// safe cast
		}

		if (this.containsInvalidCharacters(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.EntitySetConstraint_NameInvalid });
		}

		if (this.hasInvalidLength(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.ModelConstraint_SimpleIdentifierTooLong });
		}

		return ctx.createSuccessStatus();
	}

	private IStatus checkUniqueName(IValidationContext ctx, EntitySet entitySet) {
		EntityContainer container = (EntityContainer) entitySet.eContainer();
		EntitySet es;
		AssociationSet as;
		FunctionImport f;
		String nameToBeChecked = entitySet.getName();

		if (container == null) {
			return ctx.createSuccessStatus();
		}

		Iterator<AssociationSet> iAssociationSets = container
				.getAssociationSets().iterator();
		while (iAssociationSets.hasNext()) {
			as = iAssociationSets.next();

			if ((as.getName() != null)
					&& as.getName().equals(entitySet.getName())) {

				nameToBeChecked = MessageUtil.generateErrorMsg(nameToBeChecked);
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.EntitySetConstraint_NameNotUnique });
				// return ctx.createFailureStatus(new Object[] {
				// entitySet.getName(),
				// Messages.EntitySetConstraint_NameNotUnique });
			}
		}

		Iterator<FunctionImport> iFunctions = container.getFunctionImports()
				.iterator();
		while (iFunctions.hasNext()) {
			f = iFunctions.next();
			if ((f.getName() != null)
					&& f.getName().equals(entitySet.getName())) {
				nameToBeChecked = MessageUtil.generateErrorMsg(nameToBeChecked);
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.EntitySetConstraint_NameNotUnique });
				// return ctx.createFailureStatus(new Object[] {
				// entitySet.getName(),
				// Messages.EntitySetConstraint_NameNotUnique });
			}
		}

		Iterator<EntitySet> iEntitySets = container.getEntitySets().iterator();
		while (iEntitySets.hasNext()) {
			es = iEntitySets.next();
			if ((es != entitySet) && (es.getName() != null)
					&& es.getName().equals(entitySet.getName())) {
				nameToBeChecked = MessageUtil.generateErrorMsg(nameToBeChecked);
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.EntitySetConstraint_NameNotUnique });
				// return ctx.createFailureStatus(new Object[] {
				// entitySet.getName(),
				// Messages.EntitySetConstraint_NameNotUnique });
			}
		}

		return ctx.createSuccessStatus();
	}

}
