/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.Schema;

public class EntityContainerConstraint extends ModelConstraint {

	public EntityContainerConstraint() {
		super();
	}

	public EntityContainerConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for entity containers...
		if (!(object instanceof EntityContainer)) {
			return ctx.createSuccessStatus();
		}

		status = checkDefaultContainer(ctx, (EntityContainer) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkDefaultContainer(IValidationContext ctx,
			EntityContainer container) {
		Schema schema = (Schema) container.eContainer();
		boolean defaultContainerExists = false;
		if (schema == null) {
			return ctx.createSuccessStatus();
		}

		Iterator<EntityContainer> iEntityContainers = schema.getContainers()
				.iterator();
		while (iEntityContainers.hasNext()) {
			if (iEntityContainers.next().isDefault()) {
				defaultContainerExists = true;
			}
		}

		if (!defaultContainerExists) {
			return ctx.createFailureStatus(new Object[] { container.getName(),
					Messages.EntityContainerConstraint_NoDefault });
		}

		return ctx.createSuccessStatus();
	}

}
