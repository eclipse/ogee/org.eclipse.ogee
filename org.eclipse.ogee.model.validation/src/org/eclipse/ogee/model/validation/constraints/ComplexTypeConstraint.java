/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.util.ModelException;
import org.eclipse.ogee.model.odata.util.OdataModelHelper;

public class ComplexTypeConstraint extends ModelConstraint {

	public ComplexTypeConstraint() {
		super();
	}

	public ComplexTypeConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for complex types...
		if (!(object instanceof ComplexType)) {
			return ctx.createSuccessStatus();
		}

		status = checkBaseTypeExists(ctx, (ComplexType) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkBaseTypeNoInheritanceCycle(ctx, (ComplexType) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkBaseTypeExists(IValidationContext ctx,
			ComplexType complexType) {
		List<Schema> schemataInScope = new LinkedList<Schema>();

		ComplexType baseType = complexType.getBaseType();
		// return if no base type at all...
		if (baseType == null) {
			return ctx.createSuccessStatus();
		}
		// base type is available...

		// retrieve the service (parent container)...
		Schema schema = (Schema) complexType.eContainer();
		if (schema == null) {
			return ctx.createSuccessStatus();
		}

		// retrieve all schemata in scope...
		OdataModelHelper helper = new OdataModelHelper(null);
		try {
			schemataInScope = helper.getSchemataInScope(schema);
		} catch (ModelException e) {
			return ctx.createFailureStatus(new Object[] {
					complexType.getName(),
					Messages.ComplexTypeConstraint_BaseTypeNotInScope });
		}

		Iterator<Schema> schemataIterator = schemataInScope.iterator();
		Iterator<ComplexType> iterator;

		// for each schema in scope check the available complex types...
		while (schemataIterator.hasNext()) {
			schema = schemataIterator.next();
			iterator = schema.getComplexTypes().iterator();
			while (iterator.hasNext()) {
				ComplexType ct = iterator.next();
				if (ct.equals(baseType)) {
					// base type is available and part of the service ...
					return ctx.createSuccessStatus();
				}
			}
		}

		// base type doesn't exist...
		return ctx.createFailureStatus(new Object[] { complexType.getName(),
				Messages.ComplexTypeConstraint_BaseTypeNotInScope });
	}

	private IStatus checkBaseTypeNoInheritanceCycle(IValidationContext ctx,
			ComplexType complexType) {
		ComplexType baseType = complexType.getBaseType();
		// return if no base type at all...
		if (baseType == null) {
			return ctx.createSuccessStatus();
		}
		// base type is available...

		List<ComplexType> allBaseTypes = new LinkedList<ComplexType>();
		allBaseTypes.add(complexType);
		while (baseType != null) {
			if (allBaseTypes.contains(baseType)) {
				// Inheritance cycle detected
				return ctx.createFailureStatus(new Object[] {
						complexType.getName(),
						Messages.ComplexTypeConstraint_InheritanceCycle });
			}
			allBaseTypes.add(baseType);
			baseType = baseType.getBaseType();
		}

		// No inheritance cycle detected
		return ctx.createSuccessStatus();
	}

}
