/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.model.odata.util.ModelException;
import org.eclipse.ogee.model.odata.util.OdataModelHelper;
import org.eclipse.ogee.model.validation.impl.ValueAnnotationVerifier;

public class ValueAnnotationConstraint extends ModelConstraint {

	private List<IStatus> allStatus;

	public ValueAnnotationConstraint() {
		super();
	}

	public ValueAnnotationConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for value annotations...
		if (!(object instanceof ValueAnnotation)) {
			return ctx.createSuccessStatus();
		}

		ValueAnnotation valueAnnotation = (ValueAnnotation) object;
		// Only validate the main EDMX and its schemata
		EDMXSet edmxSet;
		Schema schema;
		if (valueAnnotation.eContainer() != null
				&& valueAnnotation.eContainer().eContainer() != null) {
			schema = (Schema) valueAnnotation.eContainer();
			edmxSet = (EDMXSet) valueAnnotation.eContainer().eContainer();
		} else {
			return ctx.createSuccessStatus();
		}
		if (edmxSet.getMainEDMX() == null
				|| !edmxSet.getMainEDMX().getDataService().getSchemata()
						.contains(schema)) {
			return ctx.createSuccessStatus();
		}

		// Here, it is really a value annotation defined in regards to the main
		// EDMX...
		status = checkTermExists(ctx, (ValueAnnotation) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkIsApplicable(ctx, (ValueAnnotation) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		checkValue(ctx, (ValueAnnotation) object);

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkIsApplicable(IValidationContext ctx,
			ValueAnnotation valueAnnotation) {
		EDMXSet edmxSet = null;
		if (valueAnnotation.getTarget() == null
				|| valueAnnotation.getTerm() == null) {
			// checked elsewhere...
			return ctx.createSuccessStatus();
		}
		if (valueAnnotation.eContainer() != null
				&& valueAnnotation.eContainer().eContainer() != null) {
			edmxSet = (EDMXSet) valueAnnotation.eContainer().eContainer();
		} else {
			// checked elsewhere...
			return ctx.createSuccessStatus();
		}
		try {
			IVocabularyContext context = IModelContext.INSTANCE
					.getVocabularyContext(edmxSet);
			Iterator<String> namespaceIterator = context
					.getAvailableVocabularies().iterator();
			String namespace;
			IVocabulary vocabulary;
			while (namespaceIterator.hasNext()) {
				namespace = namespaceIterator.next();
				vocabulary = context.provideVocabulary(namespace);
				if (vocabulary.getApplicableValueTerms(
						valueAnnotation.getTarget()).contains(
						valueAnnotation.getTerm())) {
					// According to current vocabulary the term is applicable to
					// the given target
					return ctx.createSuccessStatus();
				}
			}

		} catch (ModelAPIException e) {
			return ctx
					.createFailureStatus(new Object[] {
							valueAnnotation.getTerm().getName(),
							Messages.ValueAnnotationConstraint_TermNotApplicableCheckFailed });
		}
		return ctx.createFailureStatus(new Object[] {
				valueAnnotation.getTerm().getName(),
				Messages.ValueAnnotationConstraint_TermNotApplicable });
	}

	private void checkValue(IValidationContext ctx,
			ValueAnnotation valueAnnotation) {
		Iterator<String> messageIterator;
		String message;
		ValueAnnotationVerifier valueVerifier = new ValueAnnotationVerifier();
		if (!valueVerifier.execute(valueAnnotation)) {
			messageIterator = valueVerifier.getMessages().iterator();
			while (messageIterator.hasNext()) {
				message = messageIterator.next();
				allStatus.add(ctx.createFailureStatus(new Object[] {
						valueAnnotation.getTerm().getName(), message }));
			}
		}
	}

	private IStatus checkTermExists(IValidationContext ctx,
			ValueAnnotation valueAnnotation) {
		List<Schema> schemataInScope;

		boolean isInScope = false;
		ValueTerm valueTerm = valueAnnotation.getTerm();

		if (valueTerm == null) {
			// checked by EMF model...
			return ctx.createSuccessStatus();
		}

		Schema schema = (Schema) valueAnnotation.eContainer();
		if (schema == null) {
			return ctx.createSuccessStatus();
		}

		OdataModelHelper helper = new OdataModelHelper(null);
		try {
			schemataInScope = helper.getSchemataInScope(schema);
		} catch (ModelException e) {
			return ctx.createFailureStatus(new Object[] {
					valueAnnotation.getTerm().getName(),
					Messages.ValueAnnotationConstraint_TermNotInScope });
		}

		Iterator<Schema> schemaIterator = schemataInScope.iterator();
		while (schemaIterator.hasNext()) {
			schema = schemaIterator.next();
			if (schema.getValueTerms().contains(valueTerm)) {
				isInScope = true;
				break;
			}
		}
		if (!isInScope) {
			return ctx.createFailureStatus(new Object[] {
					valueAnnotation.getTerm().getName(),
					Messages.ValueAnnotationConstraint_TermNotInScope });
		}
		return ctx.createSuccessStatus();
	}

}
