/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.EMFEventType;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;

public class EnumMemberNameConstraint extends ModelConstraint {

	public EnumMemberNameConstraint() {
		super();
	}

	public EnumMemberNameConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for enum member...
		if (!(object instanceof EnumMember)) {
			return ctx.createSuccessStatus();
		}

		status = checkUniqueName(ctx, (EnumMember) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkValidName(ctx, (EnumMember) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkUniqueName(IValidationContext ctx,
			EnumMember enumMember) {
		EnumType enumType = (EnumType) enumMember.eContainer();
		EnumMember em;

		EMFEventType eType = ctx.getEventType();
		String nameToBeChecked;

		// In case of batch mode
		if (eType == EMFEventType.NULL) {
			nameToBeChecked = enumMember.getName();
			// In case of live mode
		} else {
			nameToBeChecked = (String) ctx.getFeatureNewValue(); // In fact a
																	// safe cast
		}

		if (enumType == null) {
			return ctx.createSuccessStatus();
		}

		Iterator<EnumMember> iEnumMembers = enumType.getMembers().iterator();
		while (iEnumMembers.hasNext()) {
			em = iEnumMembers.next();
			if ((em != enumMember) && (em.getName() != null)
					&& em.getName().equals(nameToBeChecked)) {
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.EnumMemberConstraint_NameNotUnique });
			}
		}
		return ctx.createSuccessStatus();
	}

	private IStatus checkValidName(IValidationContext ctx, EnumMember enumMember) {
		EMFEventType eType = ctx.getEventType();
		String nameToBeChecked;

		// In case of batch mode
		if (eType == EMFEventType.NULL) {
			nameToBeChecked = enumMember.getName();
			// In case of live mode
		} else {
			nameToBeChecked = (String) ctx.getFeatureNewValue(); // In fact a
																	// safe cast
		}

		if (this.containsInvalidCharacters(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.EnumMemberConstraint_NameInvalid });
		}

		if (this.hasInvalidLength(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.ModelConstraint_SimpleIdentifierTooLong });
		}

		return ctx.createSuccessStatus();
	}

}
