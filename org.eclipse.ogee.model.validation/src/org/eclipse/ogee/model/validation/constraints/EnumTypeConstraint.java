/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EnumType;

public class EnumTypeConstraint extends ModelConstraint {

	public EnumTypeConstraint() {
		super();
	}

	public EnumTypeConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for enum types...
		if (!(object instanceof EnumType)) {
			return ctx.createSuccessStatus();
		}

		status = checkValidUnderlyingType(ctx, (EnumType) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkValidUnderlyingType(IValidationContext ctx,
			EnumType enumType) {
		EDMTypes edmType = enumType.getUnderlyingType();

		if (edmType == null) {
			return ctx.createSuccessStatus();
		}

		if (enumType.getApplicableUnderlyingTypes().contains(edmType)) {
			return ctx.createSuccessStatus();
		}

		return ctx.createFailureStatus(new Object[] { enumType.getName(),
				Messages.EnumTypeConstraint_InvalidUnderlyingType });
	}

}
