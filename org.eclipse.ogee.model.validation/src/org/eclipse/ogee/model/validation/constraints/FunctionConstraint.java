/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.Binding;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.util.ModelException;
import org.eclipse.ogee.model.odata.util.OdataModelHelper;

public class FunctionConstraint extends ModelConstraint {

	public FunctionConstraint() {
		super();
	}

	public FunctionConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for functions...
		if (!(object instanceof FunctionImport)) {
			return ctx.createSuccessStatus();
		}

		status = checkReturnDefinition(ctx, (FunctionImport) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		// status = checkBinding(ctx, (FunctionImport) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkBinding(IValidationContext ctx, FunctionImport function) {
		Binding binding = function.getBinding();
		Parameter firstParameter = null;
		EntityTypeUsage firstParameterTypeUsage;

		if (binding == null || binding.getType() == null) {
			return ctx.createSuccessStatus(); // already checked in Ecore model
		}

		// Binding is defined, so there must be a first parameter
		if (function.getParameters().size() == 0
				|| function.getParameters().get(0) == null) {
			return ctx.createFailureStatus(new Object[] { function.getName(),
					Messages.FunctionConstraint_ParameterMissing });
		}

		firstParameter = function.getParameters().get(0);

		// First parameter must be an entity type usage
		if (!(firstParameter.getType() instanceof EntityTypeUsage)) {
			return ctx.createFailureStatus(new Object[] { function.getName(),
					Messages.FunctionConstraint_InvalidBinding });
		}
		firstParameterTypeUsage = (EntityTypeUsage) firstParameter.getType();

		// Check if first parameter and binding refer to exactly the same type
		if ((binding.isCollection() != firstParameterTypeUsage.isCollection())
				|| !binding.getType().equals(
						firstParameterTypeUsage.getEntityType())) {
			return ctx.createFailureStatus(new Object[] { function.getName(),
					Messages.FunctionConstraint_InvalidBinding });
		}

		return ctx.createSuccessStatus();
	}

	private IStatus checkReturnDefinition(IValidationContext ctx,
			FunctionImport function) {
		List<Schema> schemataInScope = new LinkedList<Schema>();
		Iterator<Schema> schemaIterator;
		Iterator<EntityContainer> containerIterator;
		boolean isInScope;

		EntityContainer container = (EntityContainer) function.eContainer();
		if (container == null) {
			return ctx.createSuccessStatus(); // already checked in Ecore model
		}
		Schema schema = (Schema) container.eContainer();
		if (schema == null) {
			return ctx.createSuccessStatus(); // already checked in Ecore model
		}

		if (!(function.getReturnType() instanceof ReturnEntityTypeUsage)) {
			return ctx.createSuccessStatus();
		}

		// Retrieve all schemata in scope...
		OdataModelHelper helper = new OdataModelHelper(null);
		try {
			schemataInScope = helper.getSchemataInScope(schema);
		} catch (ModelException e) {
			return ctx.createFailureStatus(new Object[] { function.getName(),
					Messages.FunctionConstraint_InvalidReturnDefinition });
		}

		ReturnEntityTypeUsage rtu = (ReturnEntityTypeUsage) function
				.getReturnType();

		EntitySet entitySet = rtu.getEntitySet();

		if (entitySet != null) {

			// Check if return entity set is in scope
			isInScope = false;
			schemaIterator = schemataInScope.iterator();
			while (schemaIterator.hasNext()) {
				schema = schemaIterator.next();
				containerIterator = schema.getContainers().iterator();
				while (containerIterator.hasNext()) {
					container = containerIterator.next();
					if (container.getEntitySets().contains(entitySet)) {
						isInScope = true;
						break;
					}
				}
				if (isInScope) {
					break;
				}
			}
			if (!isInScope) {
				return ctx.createFailureStatus(new Object[] {
						function.getName(),
						Messages.FunctionConstraint_EntitySetNotInScope });
			}

		} else {
			// Check if entitySet is set
			if (rtu.getEntitySet() == null) {
				return ctx.createFailureStatus(new Object[] {
						function.getName(),
						Messages.FunctionConstraint_EntitySetNotSet });
			}

		}

		EntityType returnType = rtu.getEntityType();
		if (returnType != null) {
			isInScope = false;
			schemaIterator = schemataInScope.iterator();
			while (schemaIterator.hasNext()) {
				schema = schemaIterator.next();
				if (schema.getEntityTypes().contains(returnType)) {
					// Check return entity type is in scope
					isInScope = true;
					break;
				}
			}
			if (!isInScope) {
				return ctx.createFailureStatus(new Object[] {
						function.getName(),
						Messages.FunctionConstraint_EntityTypeNotInScope });
			}
		}

		return ctx.createSuccessStatus();
	}

}
