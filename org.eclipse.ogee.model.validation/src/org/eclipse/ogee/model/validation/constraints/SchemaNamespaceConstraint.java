/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.Schema;

public class SchemaNamespaceConstraint extends ModelConstraint {

	public SchemaNamespaceConstraint() {
		super();
	}

	public SchemaNamespaceConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for schema namespace...
		if (!(object instanceof Schema)) {
			return ctx.createSuccessStatus();
		}

		Schema schema = (Schema) object;
		String name = schema.getNamespace();
		status = checkValidName(ctx, name, schema);
		if (!status.isOK()) {
			allStatus.add(status);
		}
		String[] subStrings = name.split("\\.");
		for (String str : subStrings) {
			status = checkValidNamespacePartName(ctx, str, schema);
			if (!status.isOK()) {
				allStatus.add(status);
			}
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}

	}

	private IStatus checkValidName(IValidationContext ctx,
			String nameToBeChecked, Schema schema) {

		if (this.hasInvalidNameSpaceLength(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] {
					schema.getNamespace(),
					Messages.NameSpaceConstraint_LengthTooLong });
		}

		if (this.containsInvalidCharactersInNamespace(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] {
					schema.getNamespace(),
					Messages.NameSpaceConstraint_NameInvalid });
		}

		return ctx.createSuccessStatus();
	}

	private IStatus checkValidNamespacePartName(IValidationContext ctx,
			String nameToBeChecked, Schema schema) {

		if (this.hasInvalidLength(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] {
					schema.getNamespace(),
					Messages.NameSpaceConstraint_PartLengthTooLong });
		}

		if (this.containsInvalidCharacters(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] {
					schema.getNamespace(),
					Messages.NameSpaceConstraint_PartNameInvalid });
		}

		return ctx.createSuccessStatus();
	}
}
