/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.util.ModelException;
import org.eclipse.ogee.model.odata.util.OdataModelHelper;

public class EntityTypeConstraint extends ModelConstraint {

	public EntityTypeConstraint() {
		super();
	}

	public EntityTypeConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for entity types...
		if (!(object instanceof EntityType)) {
			return ctx.createSuccessStatus();
		}

		status = checkBaseTypeExists(ctx, (EntityType) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkBaseTypeNoInheritanceCycle(ctx, (EntityType) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkAtLeastOneKey(ctx, (EntityType) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}

	}

	private IStatus checkAtLeastOneKey(IValidationContext ctx,
			EntityType entityType) {
		EntityType baseType = entityType.getBaseType();
		// no base type defined...entity type must define keys on its own...
		if (baseType == null && entityType.getKeys().size() == 0) {
			return ctx.createFailureStatus(new Object[] { entityType.getName(),
					Messages.EntityTypeConstraint_KeyMustExist });
		}
		if (baseType != null && entityType.getKeys().size() > 0) {
			return ctx.createFailureStatus(new Object[] { entityType.getName(),
					Messages.EntityTypeConstraint_KeysInBaseType });
		}
		if (baseType == null && entityType.getKeys().size() > 0) {
			return ctx.createSuccessStatus();
		}

		// search for keys in the inheritance hierarchy...
		List<EntityType> allBaseTypes = new LinkedList<EntityType>();
		allBaseTypes.add(entityType);
		while (baseType != null && !allBaseTypes.contains(baseType)) {
			if (baseType.getKeys().size() > 0) {
				return ctx.createSuccessStatus();
			}
			allBaseTypes.add(baseType);
			baseType = baseType.getBaseType();
		}
		return ctx.createFailureStatus(new Object[] { entityType.getName(),
				Messages.EntityTypeConstraint_KeyMustExist });
	}

	private IStatus checkBaseTypeExists(IValidationContext ctx,
			EntityType entityType) {
		List<Schema> schemataInScope = new LinkedList<Schema>();

		EntityType baseType = entityType.getBaseType();
		// return if no base type at all...
		if (baseType == null) {
			return ctx.createSuccessStatus();
		}
		// base type is available...

		// retrieve the service (parent container)...
		Schema schema = (Schema) entityType.eContainer();
		if (schema == null) {
			return ctx.createSuccessStatus();
		}

		// retrieve schemata in scope
		OdataModelHelper helper = new OdataModelHelper(null);
		try {
			schemataInScope = helper.getSchemataInScope(schema);
		} catch (ModelException e) {
			return ctx.createFailureStatus(new Object[] { entityType.getName(),
					Messages.EntityTypeConstraint_BaseTypeNotInScope });
		}

		// for each schema in scope check if base type exists...
		Iterator<Schema> schemaIterator = schemataInScope.iterator();
		while (schemaIterator.hasNext()) {
			schema = schemaIterator.next();
			Iterator<EntityType> iterator = schema.getEntityTypes().iterator();
			while (iterator.hasNext()) {
				EntityType et = iterator.next();
				if (et.equals(baseType)) {
					// base type is available and part of the service ...
					return ctx.createSuccessStatus();
				}
			}
		}

		// base type doesn't exist...
		return ctx.createFailureStatus(new Object[] { entityType.getName(),
				Messages.EntityTypeConstraint_BaseTypeNotInScope });
	}

	private IStatus checkBaseTypeNoInheritanceCycle(IValidationContext ctx,
			EntityType entityType) {
		EntityType baseType = entityType.getBaseType();
		// return if no base type at all...
		if (baseType == null) {
			return ctx.createSuccessStatus();
		}
		// base type is available...

		List<EntityType> allBaseTypes = new LinkedList<EntityType>();
		allBaseTypes.add(entityType);
		while (baseType != null) {
			if (allBaseTypes.contains(baseType)) {
				// Inheritance cycle detected
				return ctx.createFailureStatus(new Object[] {
						entityType.getName(),
						Messages.EntityTypeConstraint_InheritanceCycle });
			}
			allBaseTypes.add(baseType);
			baseType = baseType.getBaseType();
		}

		// No inheritance cycle detected
		return ctx.createSuccessStatus();
	}

}
