/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.EMFEventType;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.validation.util.MessageUtil;

public class ComplexTypeNameConstraint extends ModelConstraint {

	public ComplexTypeNameConstraint() {
		super();
	}

	public ComplexTypeNameConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for complex types...
		if (!(object instanceof ComplexType)) {
			return ctx.createSuccessStatus();
		}

		status = checkValidName(ctx, (ComplexType) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkUniqueName(ctx, (ComplexType) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkValidName(IValidationContext ctx,
			ComplexType complexType) {
		EMFEventType eType = ctx.getEventType();
		String nameToBeChecked;

		// In case of batch mode
		if (eType == EMFEventType.NULL) {
			nameToBeChecked = complexType.getName();
			// In case of live mode
		} else {
			nameToBeChecked = (String) ctx.getFeatureNewValue(); // In fact a
																	// safe cast
		}

		if (this.containsInvalidCharacters(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.ComplexTypeConstraint_NameInvalid });
		}

		if (this.hasInvalidLength(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.ModelConstraint_SimpleIdentifierTooLong });
		}

		return ctx.createSuccessStatus();
	}

	private IStatus checkUniqueName(IValidationContext ctx,
			ComplexType complexType) {
		EMFEventType eType = ctx.getEventType();
		String name;
		EnumType enumType;

		if (eType == EMFEventType.NULL) {
			// in case of batch validation mode
			name = complexType.getName();
		} else {
			// in case of live validation mode
			name = (String) ctx.getFeatureNewValue();
		}

		if (complexType.eContainer() instanceof Schema) {
			Schema schema = (Schema) complexType.eContainer();
			ComplexType ct;
			Association a;
			EntityType et;

			if (schema == null) {
				return ctx.createSuccessStatus();
			}

			Iterator<EnumType> iEnumTypes = schema.getEnumTypes().iterator();
			while (iEnumTypes.hasNext()) {
				enumType = iEnumTypes.next();
				if ((enumType.getName() != null && enumType.getName().equals(
						name))) {
					return ctx.createFailureStatus(new Object[] { name,
							Messages.ComplexTypeConstraint_NameNotUnique });
				}
			}

			Iterator<Association> iAssociations = schema.getAssociations()
					.iterator();
			while (iAssociations.hasNext()) {
				a = iAssociations.next();
				if ((a.getName() != null) && a.getName().equals(name)) {

					name = MessageUtil.generateErrorMsg(name);

					return ctx.createFailureStatus(new Object[] { name,
							Messages.ComplexTypeConstraint_NameNotUnique });
				}
			}

			Iterator<EntityType> iEntityTypes = schema.getEntityTypes()
					.iterator();
			while (iEntityTypes.hasNext()) {
				et = iEntityTypes.next();
				if ((et.getName() != null) && et.getName().equals(name)) {

					name = MessageUtil.generateErrorMsg(name);

					return ctx.createFailureStatus(new Object[] { name,
							Messages.ComplexTypeConstraint_NameNotUnique });
				}
			}

			Iterator<ComplexType> iComplexTypes = schema.getComplexTypes()
					.iterator();
			while (iComplexTypes.hasNext()) {
				ct = iComplexTypes.next();
				if ((ct != complexType) && (ct.getName() != null)
						&& ct.getName().equals(name)) {

					name = MessageUtil.generateErrorMsg(name);

					return ctx.createFailureStatus(new Object[] { name,
							Messages.ComplexTypeConstraint_NameNotUnique });
				}
			}
		}
		return ctx.createSuccessStatus();
	}

}
