/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Role;

public class AssociationSetEndConstraint extends ModelConstraint {

	public AssociationSetEndConstraint() {
		super();
	}

	public AssociationSetEndConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for association set ends...
		if (!(object instanceof AssociationSetEnd)) {
			return ctx.createSuccessStatus();
		}

		status = checkConsistentEntityTypeReferenced(ctx,
				(AssociationSetEnd) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkConsistentEntityTypeReferenced(IValidationContext ctx,
			AssociationSetEnd associationSetEnd) {
		EntitySet entitySet = associationSetEnd.getEntitySet();
		if (entitySet == null) {
			return ctx.createSuccessStatus();
		}
		Role role = associationSetEnd.getRole();
		if (role == null) {
			return ctx.createSuccessStatus();
		}

		EntityType entityType = role.getType();
		if (entityType == null) {
			return ctx.createSuccessStatus();
		}

		if ((entityType != entitySet.getType())
				&& !entityType.isDerivedFrom(entitySet.getType())) {
			return ctx
					.createFailureStatus(new Object[] { Messages.AssociationSetEndConstraint_InconsistentEntityType });
		}

		return ctx.createSuccessStatus();
	}

}
