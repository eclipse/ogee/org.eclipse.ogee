/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.util.ModelException;
import org.eclipse.ogee.model.odata.util.OdataModelHelper;

public class EntitySetConstraint extends ModelConstraint {

	public EntitySetConstraint() {
		super();
	}

	public EntitySetConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for entity sets...
		if (!(object instanceof EntitySet)) {
			return ctx.createSuccessStatus();
		}

		status = checkEntityTypeExists(ctx, (EntitySet) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkEntityTypeExists(IValidationContext ctx,
			EntitySet entitySet) {
		List<Schema> schemataInScope;
		boolean isInScope = false;
		EntityType entityType = entitySet.getType();
		if (entityType == null) {
			return ctx.createSuccessStatus();
		}

		EntityContainer container = (EntityContainer) entitySet.eContainer();
		if (container == null) {
			return ctx.createSuccessStatus();
		}

		Schema schema = (Schema) container.eContainer();
		if (schema == null) {
			return ctx.createSuccessStatus();
		}

		OdataModelHelper helper = new OdataModelHelper(null);
		try {
			schemataInScope = helper.getSchemataInScope(schema);
		} catch (ModelException e) {
			return ctx.createFailureStatus(new Object[] { entitySet.getName(),
					Messages.EntitySetConstraint_EntityTypeNotInScope });
		}

		Iterator<Schema> schemaIterator = schemataInScope.iterator();
		while (schemaIterator.hasNext()) {
			schema = schemaIterator.next();
			if (schema.getEntityTypes().contains(entityType)) {
				isInScope = true;
				break;
			}
		}
		if (!isInScope) {
			return ctx.createFailureStatus(new Object[] { entitySet.getName(),
					Messages.EntitySetConstraint_EntityTypeNotInScope });
		}

		return ctx.createSuccessStatus();
	}

}
