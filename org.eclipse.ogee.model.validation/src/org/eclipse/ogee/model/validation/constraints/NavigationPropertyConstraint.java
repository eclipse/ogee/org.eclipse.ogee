/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.util.ModelException;
import org.eclipse.ogee.model.odata.util.OdataModelHelper;

public class NavigationPropertyConstraint extends ModelConstraint {

	public NavigationPropertyConstraint() {
		super();
	}

	public NavigationPropertyConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for navigation properties...
		if (!(object instanceof NavigationProperty)) {
			return ctx.createSuccessStatus();
		}

		status = checkAssociationExists(ctx, (NavigationProperty) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkRoles(ctx, (NavigationProperty) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkAssociationExists(IValidationContext ctx,
			NavigationProperty navigationProperty) {
		List<Schema> schemataInScope = new LinkedList<Schema>();

		Association association = navigationProperty.getRelationship();
		if (association == null) {
			return ctx.createSuccessStatus();
		}

		Schema schema = (Schema) navigationProperty.eContainer().eContainer();
		if (schema == null) {
			return ctx.createSuccessStatus();
		}

		// Retrieve all schemata in scope...
		OdataModelHelper helper = new OdataModelHelper(null);
		try {
			schemataInScope = helper.getSchemataInScope(schema);
		} catch (ModelException e) {
			return ctx
					.createFailureStatus(new Object[] {
							navigationProperty.getName(),
							Messages.NavigationPropertyConstraint_AssociationNotInScope });
		}

		Iterator<Schema> schemaIterator = schemataInScope.iterator();
		Iterator<Association> iter;
		while (schemaIterator.hasNext()) {
			schema = schemaIterator.next();
			iter = schema.getAssociations().iterator();
			while (iter.hasNext()) {
				Association associationTemp = iter.next();
				if (associationTemp.equals(association)) {
					return ctx.createSuccessStatus();
				}
			}
		}

		return ctx.createFailureStatus(new Object[] {
				navigationProperty.getName(),
				Messages.NavigationPropertyConstraint_AssociationNotInScope });
	}

	private IStatus checkRoles(IValidationContext ctx,
			NavigationProperty navigationProperty) {
		Association association = navigationProperty.getRelationship();
		if (association == null) {
			return ctx.createSuccessStatus();
		}

		List<Role> ends = association.getEnds();
		boolean toRoleExists = false;
		boolean fromRoleExists = false;

		if (navigationProperty.getFromRole() == null
				|| navigationProperty.getToRole() == null) {
			return ctx.createSuccessStatus(); // checked already by the Ecore
												// Model
		}

		if (navigationProperty.getFromRole().equals(
				navigationProperty.getToRole())) {
			return ctx.createFailureStatus(new Object[] {
					navigationProperty.getName(),
					Messages.NavigationPropertyConstraint_SameRoles });
		}

		Iterator<Role> iter = ends.iterator();
		while (iter.hasNext()) {
			Role role = iter.next();
			if (navigationProperty.getFromRole().equals(role)) {
				fromRoleExists = true;
			} else if (navigationProperty.getToRole().equals(role)) {
				toRoleExists = true;
			}
		}

		if (!fromRoleExists) {
			return ctx.createFailureStatus(new Object[] {
					navigationProperty.getName(),
					Messages.NavigationPropertyConstraint_FromRoleNotInScope });
		} else if (!toRoleExists) {
			return ctx.createFailureStatus(new Object[] {
					navigationProperty.getName(),
					Messages.NavigationPropertyConstraint_ToRoleNotInScope });
		}

		return ctx.createSuccessStatus();

	}

}
