/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.EMFEventType;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.validation.util.MessageUtil;

public class ParameterNameConstraint extends ModelConstraint {

	public ParameterNameConstraint() {
		super();
	}

	public ParameterNameConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for parameters...
		if (!(object instanceof Parameter)) {
			return ctx.createSuccessStatus();
		}

		status = checkValidName(ctx, (Parameter) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkUniqueName(ctx, (Parameter) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkValidName(IValidationContext ctx, Parameter parameter) {
		EMFEventType eType = ctx.getEventType();
		String nameToBeChecked;

		// In case of batch mode
		if (eType == EMFEventType.NULL) {
			nameToBeChecked = parameter.getName();
			// In case of live mode
		} else {
			nameToBeChecked = (String) ctx.getFeatureNewValue(); // In fact a
																	// safe cast
		}

		if (this.containsInvalidCharacters(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.ParameterConstraint_NameInvalid });
		}

		if (this.hasInvalidLength(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.ModelConstraint_SimpleIdentifierTooLong });
		}

		return ctx.createSuccessStatus();
	}

	private IStatus checkUniqueName(IValidationContext ctx, Parameter parameter) {
		FunctionImport function = (FunctionImport) parameter.eContainer();
		Parameter p;
		String nameToBeChecked = parameter.getName();

		if (function == null) {
			return ctx.createSuccessStatus(); // already checked by Ecore model
		}

		Iterator<Parameter> iParameters = function.getParameters().iterator();
		while (iParameters.hasNext()) {
			p = iParameters.next();
			if ((p != parameter) && (p.getName() != null)
					&& p.getName().equals(parameter.getName())) {

				nameToBeChecked = MessageUtil.generateErrorMsg(nameToBeChecked);
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.ParameterConstraint_NameNotUnique });

				// return ctx.createFailureStatus(new Object[] {
				// parameter.getName(),
				// Messages.ParameterConstraint_NameNotUnique });
			}
		}

		return ctx.createSuccessStatus();
	}

}
