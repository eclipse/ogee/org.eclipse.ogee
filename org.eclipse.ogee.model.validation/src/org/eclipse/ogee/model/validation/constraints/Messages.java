/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.ogee.model.validation.constraints.messages"; //$NON-NLS-1$

	public static String NameSpaceConstraint_NameInvalid;
	public static String NameSpaceConstraint_LengthTooLong;
	public static String NameSpaceConstraint_PartLengthTooLong;
	public static String NameSpaceConstraint_PartNameInvalid;

	public static String ModelConstraint_SimpleIdentifierTooLong;
	public static String EntityContainerConstraint_NameNotUnique;
	public static String EntityContainerConstraint_NameInvalid;
	public static String EntityContainerConstraint_NoDefault;
	public static String EntityTypeConstraint_InheritanceCycle;
	public static String EntityTypeConstraint_BaseTypeNotInScope;
	public static String EntityTypeConstraint_KeyMustExist;
	public static String EntityTypeConstraint_NameNotUnique;
	public static String EntityTypeConstraint_NameInvalid;
	public static String EntityTypeConstraint_KeysInBaseType;
	public static String ComplexTypeConstraint_BaseTypeNotInScope;
	public static String ComplexTypeConstraint_InheritanceCycle;
	public static String ComplexTypeConstraint_NameNotUnique;
	public static String ComplexTypeConstraint_NameInvalid;
	public static String PropertyConstraint_NameNotUnique;
	public static String PropertyConstraint_NameInvalid;
	public static String PropertyConstraint_IfKeyNullableNotTrue;
	public static String PropertyConstraint_ComplexTypeNotInScope;
	public static String NavigationPropertyConstraint_NameNotUnique;
	public static String NavigationPropertyConstraint_NameInvalid;
	public static String NavigationPropertyConstraint_FromRoleNotInScope;
	public static String NavigationPropertyConstraint_ToRoleNotInScope;
	public static String NavigationPropertyConstraint_SameRoles;
	public static String NavigationPropertyConstraint_AssociationNotInScope;
	public static String ReferentialConstraintConstraint_DependentRoleNotInScope;
	public static String ReferentialConstraintConstraint_PrincipalRoleNotInScope;
	public static String ReferentialConstraintConstraint_KeyMapping;
	public static String ReferentialConstraintConstraint_IncorrectDependentKey;
	public static String ReferentialConstraintConstraint_IncorrectPrincipalKey;
	public static String ReferentialConstraintConstraint_IncorrectKeyMapping;
	public static String AssociationConstraint_NameNotUnique;
	public static String AssociationConstraint_NameInvalid;
	public static String AssociationEndConstraint_EntityTypeNotInScope;
	public static String EntitySetConstraint_NameNotUnique;
	public static String EntitySetConstraint_NameInvalid;
	public static String EntitySetConstraint_EntityTypeNotInScope;
	public static String AssociationSetConstraint_NameNotUnique;
	public static String AssociationSetConstraint_NameInvalid;
	public static String AssociationSetConstraint_AssociationNotInScope;
	public static String AssociationSetEndConstraint_InconsistentEntityType;
	public static String ParameterConstraint_NameNotUnique;
	public static String ParameterConstraint_NameInvalid;
	public static String FunctionConstraint_NameNotUnique;
	public static String FunctionConstraint_NameInvalid;
	public static String FunctionConstraint_InvalidReturnDefinition;
	public static String FunctionConstraint_EntityTypeNotInScope;
	public static String FunctionConstraint_EntitySetNotInScope;

	public static String FunctionConstraint_EntitySetNotSet;

	public static String FunctionConstraint_InvalidBinding;
	public static String FunctionConstraint_ParameterMissing;
	public static String EnumTypeConstraint_NameInvalid;
	public static String EnumTypeConstraint_NameNotUnique;
	public static String EnumTypeConstraint_InvalidUnderlyingType;
	public static String EnumMemberConstraint_NameInvalid;
	public static String EnumMemberConstraint_NameNotUnique;
	public static String EnumMemberConstraint_ValueInvalidSByte;
	public static String EnumMemberConstraint_ValueInvalidByte;
	public static String EnumMemberConstraint_ValueInvalidInt16;
	public static String EnumMemberConstraint_ValueInvalidInt32;
	public static String EnumMemberConstraint_ValueInvalidInt64;
	public static String ValueAnnotationConstraint_TermNotInScope;
	public static String ValueAnnotationConstraint_TermNotApplicable;
	public static String ValueAnnotationConstraint_TermNotApplicableCheckFailed;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
		// Default constructor
	}
}
