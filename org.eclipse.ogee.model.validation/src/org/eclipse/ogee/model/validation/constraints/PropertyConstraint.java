/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.util.ModelException;
import org.eclipse.ogee.model.odata.util.OdataModelHelper;

public class PropertyConstraint extends ModelConstraint {

	private static final String KEYS_FEATURE = "keys"; //$NON-NLS-1$

	public PropertyConstraint() {
		super();
	}

	public PropertyConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for properties...
		if (!(object instanceof Property)) {
			return ctx.createSuccessStatus();
		}

		status = checkIfKeyNullableFalse(ctx, (Property) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkIfComplexTypeThanInScope(ctx, (Property) object);

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkIfComplexTypeThanInScope(IValidationContext ctx,
			Property property) {
		List<Schema> schemataInScope = new LinkedList<Schema>();

		IPropertyTypeUsage typeUsage = property.getType();
		if (!(typeUsage instanceof ComplexTypeUsage)) {
			return ctx.createSuccessStatus();
		}
		ComplexTypeUsage complexTypeUsage = (ComplexTypeUsage) typeUsage;
		ComplexType complexType = complexTypeUsage.getComplexType();
		if (complexType == null) {
			return ctx.createSuccessStatus();
		}
		if (property.eContainer() == null
				|| property.eContainer().eContainer() == null) {
			return ctx.createFailureStatus(new Object[] { property.getName(),
					Messages.PropertyConstraint_ComplexTypeNotInScope });
		}
		Schema schema = (Schema) property.eContainer().eContainer();
		// Retrieve schemata in scope
		OdataModelHelper helper = new OdataModelHelper(null);
		try {
			schemataInScope = helper.getSchemataInScope(schema);
		} catch (ModelException e) {
			return ctx.createFailureStatus(new Object[] { property.getName(),
					Messages.PropertyConstraint_ComplexTypeNotInScope });
		}
		// Check if complex type is in scope
		Iterator<Schema> schemaIterator = schemataInScope.iterator();
		while (schemaIterator.hasNext()) {
			schema = schemaIterator.next();
			if (schema.getComplexTypes().contains(complexType)) {
				return ctx.createSuccessStatus();
			}
		}
		return ctx.createFailureStatus(new Object[] { property.getName(),
				Messages.PropertyConstraint_ComplexTypeNotInScope });
	}

	private IStatus checkIfKeyNullableFalse(IValidationContext ctx,
			Property property) {
		EStructuralFeature feature = null;

		// Property is initial or nullable is anyway false ...
		if (property == null || !property.isNullable()) {
			return ctx.createSuccessStatus();
		}

		feature = property.eContainingFeature();
		// No container element available...
		if (feature == null) {
			return ctx.createSuccessStatus();
		}

		if (KEYS_FEATURE.equals(feature.getName())) {
			return ctx.createFailureStatus(new Object[] { property.getName(),
					Messages.PropertyConstraint_IfKeyNullableNotTrue });
		}

		return ctx.createSuccessStatus();
	}

}
