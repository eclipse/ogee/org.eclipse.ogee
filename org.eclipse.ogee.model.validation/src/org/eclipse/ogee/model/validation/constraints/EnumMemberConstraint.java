/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.IntegerValue;

public class EnumMemberConstraint extends ModelConstraint {

	public EnumMemberConstraint() {
		super();
	}

	public EnumMemberConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for enum members...
		if (!(object instanceof EnumMember)) {
			return ctx.createSuccessStatus();
		}

		status = checkValidValue(ctx, (EnumMember) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkValidValue(IValidationContext ctx,
			EnumMember enumMember) {

		IntegerValue value = enumMember.getValue();
		if (value == null) {
			// Checked already as part of the OData ecore model definition
			return ctx.createSuccessStatus();
		}

		EnumType enumType = (EnumType) enumMember.eContainer();
		if (enumType == null) {
			// Checked already as part of the OData ecore model definition
			return ctx.createSuccessStatus();
		}

		EDMTypes edmType = enumType.getUnderlyingType();
		if (edmType == null) {
			// The default in case the underlying type is not set...
			edmType = EDMTypes.INT32;
		}

		if (edmType == value.getEDMType()) {
			// Value applies to underlying type
			return ctx.createSuccessStatus();
		} else {
			switch (edmType) {
			case BYTE:
				return ctx.createFailureStatus(new Object[] {
						enumMember.getName(),
						Messages.EnumMemberConstraint_ValueInvalidByte });
			case SBYTE:
				return ctx.createFailureStatus(new Object[] {
						enumMember.getName(),
						Messages.EnumMemberConstraint_ValueInvalidSByte });
			case INT16:
				return ctx.createFailureStatus(new Object[] {
						enumMember.getName(),
						Messages.EnumMemberConstraint_ValueInvalidInt16 });
			case INT32:
				return ctx.createFailureStatus(new Object[] {
						enumMember.getName(),
						Messages.EnumMemberConstraint_ValueInvalidInt32 });
			case INT64:
				return ctx.createFailureStatus(new Object[] {
						enumMember.getName(),
						Messages.EnumMemberConstraint_ValueInvalidInt64 });
			default:
				return ctx.createSuccessStatus(); // requested type checked in
													// enum type constraint, no
													// problem for this
													// constraint
			}
		}

	}

}
