/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.EMFEventType;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.Schema;

public class EnumTypeNameConstraint extends ModelConstraint {

	public EnumTypeNameConstraint() {
		super();
	}

	public EnumTypeNameConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for enum types...
		if (!(object instanceof EnumType)) {
			return ctx.createSuccessStatus();
		}

		status = checkUniqueName(ctx, (EnumType) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkValidName(ctx, (EnumType) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkUniqueName(IValidationContext ctx, EnumType enumType) {
		Schema schema = (Schema) enumType.eContainer();
		EnumType et;
		Association a;
		ComplexType c;
		EntityType entityType;

		EMFEventType eType = ctx.getEventType();
		String nameToBeChecked;

		// In case of batch mode
		if (eType == EMFEventType.NULL) {
			nameToBeChecked = enumType.getName();
			// In case of live mode
		} else {
			nameToBeChecked = (String) ctx.getFeatureNewValue(); // In fact a
																	// safe cast
		}

		if (schema == null) {
			return ctx.createSuccessStatus();
		}

		Iterator<EnumType> iEnumTypes = schema.getEnumTypes().iterator();
		while (iEnumTypes.hasNext()) {
			et = iEnumTypes.next();
			if ((et != enumType) && (et.getName() != null)
					&& et.getName().equals(nameToBeChecked)) {
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.EnumTypeConstraint_NameNotUnique });
			}
		}

		Iterator<Association> iAssociations = schema.getAssociations()
				.iterator();
		while (iAssociations.hasNext()) {
			a = iAssociations.next();
			if (a.getName() != null && a.getName().equals(nameToBeChecked)) {
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.EnumTypeConstraint_NameNotUnique });
			}
		}

		Iterator<ComplexType> iComplexTypes = schema.getComplexTypes()
				.iterator();
		while (iComplexTypes.hasNext()) {
			c = iComplexTypes.next();
			if (c.getName() != null && c.getName().equals(nameToBeChecked)) {
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.EnumTypeConstraint_NameNotUnique });
			}
		}

		Iterator<EntityType> iEntityTypes = schema.getEntityTypes().iterator();
		while (iEntityTypes.hasNext()) {
			entityType = iEntityTypes.next();
			if (entityType.getName() != null
					&& entityType.getName().equals(nameToBeChecked)) {
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.EnumTypeConstraint_NameNotUnique });
			}
		}

		return ctx.createSuccessStatus();
	}

	private IStatus checkValidName(IValidationContext ctx, EnumType enumType) {
		EMFEventType eType = ctx.getEventType();
		String nameToBeChecked;

		// In case of batch mode
		if (eType == EMFEventType.NULL) {
			nameToBeChecked = enumType.getName();
			// In case of live mode
		} else {
			nameToBeChecked = (String) ctx.getFeatureNewValue(); // In fact a
																	// safe cast
		}

		if (this.containsInvalidCharacters(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.EnumTypeConstraint_NameInvalid });
		}

		if (this.hasInvalidLength(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.ModelConstraint_SimpleIdentifierTooLong });
		}

		return ctx.createSuccessStatus();
	}

}
