/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.constraints;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.EMFEventType;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.validation.util.MessageUtil;

public class NavigationPropertyNameConstraint extends ModelConstraint {

	public NavigationPropertyNameConstraint() {
		super();
	}

	public NavigationPropertyNameConstraint(IConstraintDescriptor descriptor) {
		super(descriptor);
	}

	@Override
	public IStatus validate(IValidationContext ctx) {
		List<IStatus> allStatus = new LinkedList<IStatus>();
		IStatus status;

		EObject object = ctx.getTarget();
		// Only validate constraint for navigation properties...
		if (!(object instanceof NavigationProperty)) {
			return ctx.createSuccessStatus();
		}

		status = checkValidName(ctx, (NavigationProperty) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		status = checkUniqueName(ctx, (NavigationProperty) object);
		if (!status.isOK()) {
			allStatus.add(status);
		}

		if (allStatus.size() == 0) {
			return ctx.createSuccessStatus();
		} else {
			return ConstraintStatus.createMultiStatus(ctx, allStatus);
		}
	}

	private IStatus checkValidName(IValidationContext ctx,
			NavigationProperty navigationProperty) {
		EMFEventType eType = ctx.getEventType();
		String nameToBeChecked;

		// In case of batch mode
		if (eType == EMFEventType.NULL) {
			nameToBeChecked = navigationProperty.getName();
			// In case of live mode
		} else {
			nameToBeChecked = (String) ctx.getFeatureNewValue(); // In fact a
																	// safe cast
		}

		if (this.containsInvalidCharacters(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.NavigationPropertyConstraint_NameInvalid });
		}

		if (this.hasInvalidLength(nameToBeChecked)) {
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.ModelConstraint_SimpleIdentifierTooLong });
		}

		return ctx.createSuccessStatus();
	}

	private IStatus checkUniqueName(IValidationContext ctx,
			NavigationProperty navigationProperty) {
		EntityType entityType = (EntityType) navigationProperty.eContainer();
		List<EntityType> alreadyVisited = new LinkedList<EntityType>();
		return checkUniqueNameInheritedContent(ctx, alreadyVisited, entityType,
				navigationProperty);
	}

	private IStatus checkUniqueNameInheritedContent(IValidationContext ctx,
			List<EntityType> alreadyVisited, EntityType entityType,
			NavigationProperty navigationProperty) {
		NavigationProperty np;
		Property p;
		String nameToBeChecked = navigationProperty.getName();

		if (entityType == null) {
			return ctx.createSuccessStatus();
		} else if (alreadyVisited.contains(entityType)) {
			return ctx.createSuccessStatus(); // inheritance cycle checked
												// somewhere else...
		}

		if (navigationProperty.getName() != null
				&& navigationProperty.getName().equals(entityType.getName())) {

			nameToBeChecked = MessageUtil.generateErrorMsg(nameToBeChecked);
			return ctx.createFailureStatus(new Object[] { nameToBeChecked,
					Messages.NavigationPropertyConstraint_NameNotUnique });

			// return ctx.createFailureStatus(new Object[] {
			// navigationProperty.getName(),
			// Messages.NavigationPropertyConstraint_NameNotUnique });
		}

		Iterator<NavigationProperty> iNavigationProperties = entityType
				.getNavigationProperties().iterator();
		while (iNavigationProperties.hasNext()) {
			np = iNavigationProperties.next();
			if ((np != navigationProperty) && (np.getName() != null)
					&& np.getName().equals(navigationProperty.getName())) {

				nameToBeChecked = MessageUtil.generateErrorMsg(nameToBeChecked);
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.NavigationPropertyConstraint_NameNotUnique });

				// return ctx.createFailureStatus(new Object[] {
				// navigationProperty.getName(),
				// Messages.NavigationPropertyConstraint_NameNotUnique });
			}
		}

		Iterator<Property> iProperties = entityType.getProperties().iterator();
		while (iProperties.hasNext()) {
			p = iProperties.next();
			if ((p.getName() != null)
					&& p.getName().equals(navigationProperty.getName())) {

				nameToBeChecked = MessageUtil.generateErrorMsg(nameToBeChecked);
				return ctx.createFailureStatus(new Object[] { nameToBeChecked,
						Messages.NavigationPropertyConstraint_NameNotUnique });

				// return ctx.createFailureStatus(new Object[] {
				// navigationProperty.getName(),
				// Messages.NavigationPropertyConstraint_NameNotUnique });
			}
		}

		alreadyVisited.add(entityType);

		return checkUniqueNameInheritedContent(ctx, alreadyVisited,
				entityType.getBaseType(), navigationProperty);
	}

}
