/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.util;

/**
 * This utility class wraps very long message strings
 * 
 */
public class MessageUtil {

	public static final int LENGTH = 100;

	/**
	 * This method will generate wrapped error message in case it's too long
	 * 
	 * @param artifact
	 *            's name to be checked
	 * @return wrapped error message
	 */
	public static String generateErrorMsg(String nameToBeChecked) {
		String errorMsg = nameToBeChecked;
		int len = nameToBeChecked.length();
		if (len > LENGTH) {
			errorMsg = errorMsg.concat(System.getProperty("line.separator")); //$NON-NLS-1$
		}
		return errorMsg;
	}
}
