/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.vocabularies.ICoreVocabulary;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.PathValue;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.RecordValue;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.model.odata.impl.PropertyToValueMapImpl;
import org.eclipse.ogee.model.odata.util.OdataSwitch;
import org.eclipse.osgi.util.NLS;

public class ValueAnnotationVerifier extends OdataSwitch<Boolean> {

	private List<String> messages;
	private ValueAnnotation valueAnnotation;
	private Schema schema;

	public Boolean execute(ValueAnnotation valueAnnotation) {
		EObject currentObject;
		this.messages = new LinkedList<String>();

		if (valueAnnotation == null || valueAnnotation.getTarget() == null
				|| valueAnnotation.getTarget().eContainer() == null
				|| valueAnnotation.getTerm() == null
				|| valueAnnotation.getTerm().getType() == null
				|| valueAnnotation.eContainer() == null) {
			return Boolean.FALSE;
		}

		this.schema = (Schema) valueAnnotation.eContainer();
		this.valueAnnotation = valueAnnotation;

		Boolean result;
		// Iterate over the value annotation tree...
		TreeIterator<EObject> treeIterator = EcoreUtil.getAllContents(
				valueAnnotation, true);
		result = this.doSwitch(valueAnnotation);
		if (result != null) {
			return result;
		}
		while (treeIterator.hasNext()) {
			currentObject = treeIterator.next();
			result = this.doSwitch(currentObject);
			if (result != null) {
				return result;
			}
		}

		if (this.messages.size() == 0) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	public List<String> getMessages() {
		return this.messages;
	}

	@Override
	public Boolean caseValueAnnotation(ValueAnnotation object) {
		ValueTerm term = object.getTerm();
		AnnotationValue annotationValue = object.getAnnotationValue();

		if (term.getType() instanceof ComplexTypeUsage) {
			ComplexTypeUsage ctu = (ComplexTypeUsage) term.getType();
			if (ctu.isCollection()) {
				if (annotationValue instanceof ValueCollection) {
					return null;
				}
			} else {
				if (annotationValue instanceof RecordValue) {
					return null;
				}
			}
		} else if (term.getType() instanceof SimpleTypeUsage) {
			SimpleTypeUsage stu = (SimpleTypeUsage) term.getType();
			if (stu.isCollection()) {
				if (annotationValue instanceof ValueCollection) {
					return null;
				}
			} else {
				if ((annotationValue instanceof PathValue)
						|| (annotationValue instanceof SimpleValue)) {
					return null;
				}
			}
		}

		this.messages
				.add(NLS.bind(
						Messages.ValueAnnotationVerifier_InvalidValueTermValueCombination,
						term.getName()));
		return new Boolean(false);
	}

	@Override
	public Boolean caseRecordValue(RecordValue object) {
		Iterator<Entry<Property, AnnotationValue>> propertyValueMapIterator = object
				.getPropertyValues().entrySet().iterator();
		Entry<Property, AnnotationValue> currentEntry;
		Property property;
		AnnotationValue value;
		while (propertyValueMapIterator.hasNext()) {
			currentEntry = propertyValueMapIterator.next();
			property = currentEntry.getKey();
			value = currentEntry.getValue();
			if (!this.checkValidPropertyLevel(property, value)) {
				this.messages
						.add(NLS.bind(
								Messages.ValueAnnotationVerifier_InvalidPropertyValueMapping,
								property.getName(), this.valueAnnotation
										.getTerm().getName()));
			}
		}
		return null;
	}

	@Override
	public Boolean caseSimpleValue(SimpleValue object) {
		EDMTypes edmType;
		PropertyToValueMapImpl mapping = null;
		boolean isCollection = false;
		if (object.eContainer() == null) {
			return null;
		} else if (object.eContainer() instanceof ValueCollection) {
			isCollection = true;
			if (object.eContainer().eContainer() instanceof PropertyToValueMapImpl) {
				mapping = (PropertyToValueMapImpl) object.eContainer()
						.eContainer();
			}
		} else if (object.eContainer() instanceof PropertyToValueMapImpl) {
			mapping = (PropertyToValueMapImpl) object.eContainer();
		}

		if (mapping == null) {
			if ((this.valueAnnotation.getTerm().getType() instanceof SimpleTypeUsage)
					&& !object.getEDMType().equals(
							((SimpleTypeUsage) this.valueAnnotation.getTerm()
									.getType()).getSimpleType().getType())) {
				this.messages
						.add(NLS.bind(
								Messages.ValueAnnotationVerifier_InvalidValueTermValueCombination,
								this.valueAnnotation.getTerm().getName()));
			}
			return null;
		}

		Property property = mapping.getKey();
		if (property == null) {
			this.messages
					.add(NLS.bind(
							Messages.ValueAnnotationVerifier_InvalidPropertyValueMappingPropertyKeyNull,
							this.valueAnnotation.getTerm().getName()));
			return null;
		}

		IPropertyTypeUsage ptu = property.getType();
		if (ptu == null) {
			this.messages.add(NLS.bind(
					Messages.ValueAnnotationVerifier_PropertyMustDefineType,
					property.getName(), this.valueAnnotation.getTerm()
							.getName()));
			return null;
		}

		if (ptu instanceof SimpleTypeUsage) {
			if (((SimpleTypeUsage) ptu).isCollection() != isCollection) {
				this.messages
						.add(NLS.bind(
								Messages.ValueAnnotationVerifier_InvalidValuesForCollectionFlag,
								this.valueAnnotation.getTerm().getName()));
			}
			if (object.getEDMType().equals(
					((SimpleTypeUsage) ptu).getSimpleType().getType())) {
				// Check if this is a path value and if yes trigger path value
				// verification...
				try {
					// Get the is property path value term...
					ICoreVocabulary vocabulary = (ICoreVocabulary) IModelContext.INSTANCE
							.getVocabularyContext((EDMXSet) schema.eContainer())
							.provideVocabulary(ICoreVocabulary.VC_NAMESPACE);
					ValueTerm valueTerm = vocabulary
							.getValueTermIsPropertyPath();

					// Check if property is annotated with the is property path
					// value term...
					Iterator<Annotation> annotationIterator = property
							.getAnnotations().iterator();
					Annotation annotation;
					while (annotationIterator.hasNext()) {
						annotation = annotationIterator.next();
						if (annotation instanceof ValueAnnotation
								&& ((ValueAnnotation) annotation).getTerm() == valueTerm) {
							// Yes, this is a path value and needs to be
							// verified....
							PathValueVerifier pathVerifier = new PathValueVerifier();
							if (!pathVerifier.execute(this.valueAnnotation
									.getTarget(), object.getValueObject()
									.toString(), ptu, false)) {
								this.messages
										.add(NLS.bind(
												Messages.ValueAnnotationVerifier_InvalidPathValue,
												object.getValueObject()));
							}
							break;
						}
					}
				} catch (ModelAPIException e) {
					this.messages.add(NLS.bind(
							Messages.ValueAnnotationVerifier_InvalidPathValue,
							object.getValueObject()));
				}
				return null;
			}
		} else if (ptu instanceof EnumTypeUsage) {
			if (((EnumTypeUsage) ptu).isCollection() != isCollection) {
				this.messages
						.add(NLS.bind(
								Messages.ValueAnnotationVerifier_InvalidValuesForCollectionFlag,
								this.valueAnnotation.getTerm().getName()));
			}
			edmType = ((EnumTypeUsage) ptu).getEnumType().getUnderlyingType();
			if (edmType == null) {
				edmType = EDMTypes.INT32;
			}
			if (object.getEDMType().equals(edmType)) {
				return null;
			}

		}
		this.messages
				.add(Messages.ValueAnnotationVerifier_InvalidValueTermValueCombination);
		return null;
	}

	@Override
	public Boolean casePathValue(PathValue object) {
		IPropertyTypeUsage propertyTypeUsage = null;
		if (object.eContainer() instanceof ValueAnnotation) {
			propertyTypeUsage = ((ValueAnnotation) object.eContainer())
					.getTerm().getType();
		} else if (object.eContainer() instanceof PropertyToValueMapImpl
				&& ((PropertyToValueMapImpl) object.eContainer()).getKey() != null) {
			propertyTypeUsage = ((PropertyToValueMapImpl) object.eContainer())
					.getKey().getType();
		}

		PathValueVerifier pathVerifier = new PathValueVerifier();
		if (!pathVerifier.execute(this.valueAnnotation.getTarget(),
				object.getPath(), propertyTypeUsage, true)) {
			this.messages.add(NLS.bind(
					Messages.ValueAnnotationVerifier_InvalidPathValue,
					object.getPath()));
		}
		return null;
	}

	private boolean checkValidPropertyLevel(EObject eObject,
			AnnotationValue annotationValue) {
		if (eObject == null || annotationValue == null) {
			return false;
		}

		if (annotationValue == this.valueAnnotation.getAnnotationValue()) {
			if (this.valueAnnotation.getTerm().getType() instanceof ComplexTypeUsage) {
				if (eObject == ((ComplexTypeUsage) this.valueAnnotation
						.getTerm().getType()).getComplexType()) {
					return true;
				}
			} else if (this.valueAnnotation.getTerm().getType() instanceof EnumTypeUsage) {
				if (eObject == ((EnumTypeUsage) this.valueAnnotation.getTerm()
						.getType()).getEnumType()) {
					return true;
				}
			} else if (this.valueAnnotation.getTerm().getType() instanceof SimpleTypeUsage) {
				if (eObject == ((SimpleTypeUsage) this.valueAnnotation
						.getTerm().getType()).getSimpleType()) {
					return true;
				}
			}
		}

		if (annotationValue.eContainer() == null) {
			return false;
		}
		EObject container = eObject.eContainer();
		if (annotationValue.eContainer().eContainer() instanceof AnnotationValue) {
			AnnotationValue parentAnnotationValue = (AnnotationValue) annotationValue
					.eContainer().eContainer();
			return checkValidPropertyLevel(container, parentAnnotationValue);
		}
		return false;
	}

}
