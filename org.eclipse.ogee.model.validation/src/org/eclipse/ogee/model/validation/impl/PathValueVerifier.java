/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.util.OdataSwitch;

public class PathValueVerifier extends OdataSwitch<List<EObject>> {

	private String[] pathTokens;
	private String currentToken;
	private List<String> messages;
	private Schema schema;
	private IPropertyTypeUsage propertyTypeUsage;
	private boolean isPathValue;

	public boolean execute(EObject pathRoot, String pathValue,
			IPropertyTypeUsage propertyTypeUsage, boolean isPathValue) {
		EObject currentObject;

		this.messages = new LinkedList<String>();

		if ((pathRoot instanceof Property)
				|| (pathRoot instanceof NavigationProperty)) {
			pathRoot = pathRoot.eContainer();
		}

		if (pathRoot == null || pathValue == null) {
			return Boolean.FALSE;
		}

		// Parse the path....
		StringTokenizer pathTokenizer = new StringTokenizer(pathValue, "/"); //$NON-NLS-1$
		if (pathTokenizer.countTokens() == 0) {
			return Boolean.TRUE;
		}

		if (pathRoot instanceof Schema) {
			this.schema = (Schema) pathRoot;
		} else {
			EObject container = pathRoot.eContainer();
			while (container != null) {
				if (container instanceof Schema) {
					this.schema = (Schema) container;
					break;
				}
				container = container.eContainer();
			}
		}

		this.isPathValue = isPathValue;
		this.propertyTypeUsage = propertyTypeUsage;
		this.pathTokens = new String[pathTokenizer.countTokens() + 1];
		this.pathTokens[0] = null;

		int tokenIndex = 1;
		while (pathTokenizer.hasMoreTokens()) {
			this.pathTokens[tokenIndex] = pathTokenizer.nextToken();
			tokenIndex++;
		}

		tokenIndex = 0;

		List<EObject> currentObjects = new LinkedList<EObject>();
		List<EObject> nextCurrentObjects;
		List<EObject> tempCurrentObjects;

		currentObjects.add(pathRoot);

		Iterator<EObject> currentObjectIterator;
		while (currentObjects.size() > 0 && tokenIndex < this.pathTokens.length) {
			this.currentToken = this.pathTokens[tokenIndex];
			nextCurrentObjects = new LinkedList<EObject>();
			currentObjectIterator = currentObjects.iterator();
			while (currentObjectIterator.hasNext()) {
				currentObject = currentObjectIterator.next();
				tempCurrentObjects = this.doSwitch(currentObject);
				if (tempCurrentObjects != null) {
					if (tokenIndex == (this.pathTokens.length - 1)) {
						return Boolean.TRUE;
					}
					nextCurrentObjects.addAll(tempCurrentObjects);
				}
			}
			currentObjects = nextCurrentObjects;
			tokenIndex++;
		}

		return Boolean.FALSE;
	}

	public List<String> getMessages() {
		return this.messages;
	}

	@Override
	public List<EObject> caseEntityContainer(EntityContainer object) {
		if (this.currentToken == null) {
			return object.eContents();
		}
		return null;
	}

	@Override
	public List<EObject> caseEntitySet(EntitySet object) {
		return this.doSwitch(object.getType());
	}

	@Override
	public List<EObject> caseEntityType(EntityType object) {
		List<EObject> nextPathObjects = new LinkedList<EObject>();

		if (this.currentToken == null
				|| this.currentToken.equals(object.getName())
				|| (object.eContainer() != null && this
						.qualifiedNameMatchesToken(object.getName(),
								(Schema) object.eContainer()))) {
			nextPathObjects.addAll(object.eContents());

			// Include content of all base types...
			EntityType baseType = object.getBaseType();
			// Either stop when base type is initial or cycle detected...
			while (baseType != null
					&& !nextPathObjects.containsAll(baseType.eContents())) {
				nextPathObjects.addAll(baseType.eContents());
				baseType = baseType.getBaseType();
			}

			return nextPathObjects;
		}
		return null;
	}

	@Override
	public List<EObject> caseComplexType(ComplexType object) {
		List<EObject> nextPathObjects = new LinkedList<EObject>();

		if (this.currentToken == null
				|| this.currentToken.equals(object.getName())
				|| (object.eContainer() != null && this
						.qualifiedNameMatchesToken(object.getName(),
								(Schema) object.eContainer().eContainer()))) {
			nextPathObjects.addAll(object.eContents());

			// Include properties of all base types...
			ComplexType baseType = object.getBaseType();
			// Either stop when base type is initial or cycle detected...
			while (baseType != null
					&& !nextPathObjects.containsAll(baseType.getProperties())) {
				nextPathObjects.addAll(baseType.getProperties());
				baseType = baseType.getBaseType();
			}

			return nextPathObjects;
		}
		return null;
	}

	@Override
	public List<EObject> caseProperty(Property object) {
		List<EObject> nextPathObjects = new LinkedList<EObject>();

		if (this.currentToken == null
				|| this.currentToken.equals(object.getName())
				|| (object.eContainer() != null && this
						.qualifiedNameMatchesToken(object.getName(),
								(Schema) object.eContainer().eContainer()))) {
			if (object.getType() instanceof ComplexTypeUsage) {
				nextPathObjects.add(((ComplexTypeUsage) object.getType())
						.getComplexType());
			} else if (this.isPathValue
					&& ((object.getType() instanceof SimpleTypeUsage) && this.propertyTypeUsage != null)) {
				if ((this.propertyTypeUsage instanceof SimpleTypeUsage)
						&& (((SimpleTypeUsage) this.propertyTypeUsage)
								.getSimpleType()) != null
						&& ((SimpleTypeUsage) object.getType()).getSimpleType() != null
						&& ((SimpleTypeUsage) object.getType())
								.getSimpleType()
								.getType()
								.equals((((SimpleTypeUsage) this.propertyTypeUsage)
										.getSimpleType()).getType())) {
					return nextPathObjects;
				} else {
					return null;
				}
			}
			return nextPathObjects;
		}
		return null;
	}

	@Override
	public List<EObject> caseNavigationProperty(NavigationProperty object) {
		List<EObject> nextPathObjects = new LinkedList<EObject>();

		if (this.currentToken == null
				|| this.currentToken.equals(object.getName())
				|| (object.eContainer() != null && this
						.qualifiedNameMatchesToken(object.getName(),
								(Schema) object.eContainer().eContainer()))) {
			return nextPathObjects;
		}

		return null;
	}

	private boolean qualifiedNameMatchesToken(String name, Schema schema) {
		if (this.currentToken == null || schema == null) {
			return false;
		}

		// check if qualified name matches the current token...
		if (this.currentToken.equals(schema.getNamespace() + "." + name)) { //$NON-NLS-1$
			return true;
		}

		if (this.schema == null) {
			// name and token don't match...
			return false;
		}

		// check if alias name combination matches the current token...
		Iterator<Using> usingIterator = this.schema.getUsings().iterator();
		Using using;
		while (usingIterator.hasNext()) {
			using = usingIterator.next();
			if (using.getUsedNamespace() == schema) {
				return this.currentToken.equals(using.getAlias() + "." + name); //$NON-NLS-1$
			}
		}

		// name and token don't match...
		return false;
	}

}
