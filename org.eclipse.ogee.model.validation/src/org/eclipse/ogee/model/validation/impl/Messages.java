/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.impl;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.ogee.model.validation.impl.messages"; //$NON-NLS-1$

	public static String Constraint_EntityTypeNameConstraint_Name;
	public static String Constraint_EntityTypeNameConstraint_Description;
	public static String Constraint_EntityTypeNameConstraint_Message;

	public static String ValueAnnotationVerifier_InvalidValueTermValueCombination;
	public static String ValueAnnotationVerifier_InvalidPropertyValueMapping;
	public static String ValueAnnotationVerifier_InvalidPropertyValueMappingPropertyKeyNull;
	public static String ValueAnnotationVerifier_PropertyMustDefineType;
	public static String ValueAnnotationVerifier_InvalidValuesForCollectionFlag;
	public static String ValueAnnotationVerifier_InvalidPathValue;

	

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
