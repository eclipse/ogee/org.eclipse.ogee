/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.impl;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.model.Category;
import org.eclipse.emf.validation.model.ConstraintSeverity;
import org.eclipse.emf.validation.model.EvaluationMode;
import org.eclipse.emf.validation.service.IConstraintDescriptor;

public class ConstraintDescriptor implements IConstraintDescriptor {

	private String name;
	private String id;
	private String pluginId;
	private String description;
	private ConstraintSeverity severity;
	private int statusCode;
	private EvaluationMode<?> evaluationMode;
	private boolean enabled;
	private Set<Category> categories;
	private String messagePattern;
	private Throwable exception;

	public ConstraintDescriptor(String name, String id, String pluginId,
			ConstraintSeverity severity, EvaluationMode<?> evaluationMode,
			boolean enabled) {
		this.name = name;
		this.id = id;
		this.pluginId = pluginId;
		this.evaluationMode = evaluationMode;
		this.severity = severity;
		this.enabled = enabled;

		this.categories = new HashSet<Category>();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public String getPluginId() {
		return this.pluginId;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public ConstraintSeverity getSeverity() {
		return this.severity;
	}

	@Override
	public int getStatusCode() {
		return this.statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public EvaluationMode<?> getEvaluationMode() {
		return this.evaluationMode;
	}

	@Override
	public boolean targetsTypeOf(EObject eObject) {
		return true;
	}

	@Override
	public boolean targetsEvent(Notification notification) {
		return true;
	}

	@Override
	public boolean isBatch() {
		return (this.evaluationMode == EvaluationMode.BATCH);
	}

	@Override
	public boolean isLive() {
		return (this.evaluationMode == EvaluationMode.LIVE);
	}

	@Override
	public boolean isError() {
		return (this.severity == ConstraintSeverity.ERROR);
	}

	@Override
	public Throwable getException() {
		return this.exception;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public void setError(Throwable exception) {
		this.exception = exception;
	}

	@Override
	public Set<Category> getCategories() {
		return this.categories;
	}

	@Override
	public void addCategory(Category category) {
		if (!this.categories.contains(category)) {
			this.categories.add(category);
		}
	}

	@Override
	public void removeCategory(Category category) {
		this.categories.remove(category);
	}

	@Override
	public String getMessagePattern() {
		return this.messagePattern;
	}

	public void setMessagePatter(String messagePattern) {
		this.messagePattern = messagePattern;
	}

	@Override
	public String getBody() {
		return null;
	}

}
