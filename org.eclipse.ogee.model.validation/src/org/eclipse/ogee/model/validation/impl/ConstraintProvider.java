/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.validation.impl;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.emf.validation.model.Category;
import org.eclipse.emf.validation.model.CategoryManager;
import org.eclipse.emf.validation.model.ConstraintSeverity;
import org.eclipse.emf.validation.model.EvaluationMode;
import org.eclipse.emf.validation.model.IModelConstraint;
import org.eclipse.emf.validation.service.AbstractConstraintProvider;
import org.eclipse.emf.validation.service.ConstraintExistsException;
import org.eclipse.ogee.model.validation.Activator;
import org.eclipse.ogee.model.validation.constraints.EntityTypeNameConstraint;

public class ConstraintProvider extends AbstractConstraintProvider {

	private static final String MODEL_VALIDATION_CATEGORY = "org.eclipse.ogee.model.validation"; //$NON-NLS-1$

	private static final String ENTITY_TYPE_NAME_CONSTRAINT = "EntityTypeNameConstraint"; //$NON-NLS-1$

	private List<IModelConstraint> myConstraints = new java.util.ArrayList<IModelConstraint>();

	@Override
	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {

		ConstraintDescriptor descriptor;
		Category myCategory = CategoryManager.getInstance().findCategory(
				MODEL_VALIDATION_CATEGORY);

		// Entity Type Constraints
		// Name...
		descriptor = new ConstraintDescriptor(
				Messages.Constraint_EntityTypeNameConstraint_Name,
				ENTITY_TYPE_NAME_CONSTRAINT, Activator.PLUGIN_ID,
				ConstraintSeverity.ERROR, EvaluationMode.BATCH, true);
		descriptor.addCategory(myCategory);
		descriptor
				.setDescription(Messages.Constraint_EntityTypeNameConstraint_Description);
		descriptor
				.setMessagePatter(Messages.Constraint_EntityTypeNameConstraint_Message);
		this.myConstraints.add(new EntityTypeNameConstraint(descriptor));

		try {
			this.registerConstraints(this.myConstraints);
		} catch (ConstraintExistsException e) {
			super.setInitializationData(config, propertyName, data);
		}
	}

	@Override
	protected List<IModelConstraint> getConstraints() {
		return this.myConstraints;
	}

}
