/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.navigation.tree;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.ogee.model.edit.odata.provider.OdataItemProviderAdapterFactory;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class MNViewLabelProvider extends AdapterFactoryLabelProvider implements
		ILightweightLabelDecorator {

	public MNViewLabelProvider() {
		super(new OdataItemProviderAdapterFactory());
	}

	public Image getImage(Object element) {
		if (element instanceof ReferencedSchemasFolder)
			return ReferencedSchemasFolder.getImage();

		IItemLabelProvider labelProvider = (IItemLabelProvider) this.adapterFactory
				.adapt(element, IItemLabelProvider.class);
		if ((labelProvider != null)
				&& (labelProvider.getImage(element) != null)) {
			URL url = (URL) labelProvider.getImage(element);
			Image image = this.getImage(url);
			return image;
		}
		return super.getImage(element);
	}

	public String getText(Object element) {
		if (element instanceof ReferencedSchemasFolder) {
			return ((ReferencedSchemasFolder) element).getLabel();
		}

		IItemLabelProvider labelProvider = (IItemLabelProvider) this.adapterFactory
				.adapt(element, IItemLabelProvider.class);
		if (labelProvider != null) {
			String ret = labelProvider.getText(element);
			return ret;
		}
		return super.getText(element);
	}

	private Image getImage(URL imageURL) {
		Image image = null;
		ImageRegistry registry = Activator.getDefault().getImageRegistry();
		image = registry.get(imageURL.getPath());
		if (image != null) {
			return image;
		}
		ImageDescriptor desc = ImageDescriptor.createFromURL(imageURL);
		registry.put(imageURL.getPath(), desc);
		image = registry.get(imageURL.getPath());
		return image;
	}

	@Override
	public void decorate(Object element, IDecoration decoration) {
		IMarker[] problems = null;
		EObject eObject = (EObject) element;
		String eObjectURI = EcoreUtil.getURI(eObject).toString();

		List<String> eObjectURIs = new LinkedList<String>();
		eObjectURIs.add(eObjectURI);

		// in case a child element has error markers, its parent displays error
		// markers as well
		TreeIterator<EObject> iterator = eObject.eAllContents();
		while (iterator.hasNext()) {
			eObjectURI = EcoreUtil.getURI(iterator.next()).toString();
			eObjectURIs.add(eObjectURI);
		}

		int markerPriority = 0;

		IResource res = getResourceFromEObject(eObject);
		if (res != null) {
			int depth = IResource.DEPTH_INFINITE;
			try {
				problems = res.findMarkers(IMarker.PROBLEM, true, depth);
			} catch (CoreException e) {
				Logger.getFrameworkLogger()
						.logError(
								Messages.MNViewLabelProvider_ProblemMarkers_RetrievalException,
								e);
			}

			if (problems == null) {
				return;
			}

			String markerSourceId = ""; //$NON-NLS-1$
			for (int i = 0; i < problems.length; i++) {
				try {
					markerSourceId = (String) problems[i]
							.getAttribute(IMarker.SOURCE_ID);
				} catch (CoreException e) {
					Logger.getFrameworkLogger()
							.logError(
									Messages.MNViewLabelProvider_ProblemMarkers_SourceRetrievalException,
									e);
				}
				if (eObjectURIs.contains(markerSourceId)) {
					ImageDescriptor descriptor = null;
					try {
						markerPriority = (Integer) problems[i]
								.getAttribute(IMarker.PRIORITY);
					} catch (CoreException e) {
						Logger.getFrameworkLogger()
								.logError(
										Messages.MNViewLabelProvider_ProblemMarkers_PriorityRetrievalException,
										e);
					}
					if (markerPriority == IMarker.PRIORITY_HIGH) {
						Bundle bundle = FrameworkUtil
								.getBundle(this.getClass());
						URL url = FileLocator.find(bundle, new Path(
								"icons/error_co.gif"), null); //$NON-NLS-1$
						descriptor = ImageDescriptor.createFromURL(url);
					}
					if (markerPriority == IMarker.PRIORITY_NORMAL) {
						Bundle bundle = FrameworkUtil
								.getBundle(this.getClass());
						URL url = FileLocator.find(bundle, new Path(
								"icons/warning_co.gif"), null); //$NON-NLS-1$
						descriptor = ImageDescriptor.createFromURL(url);
					}

					if (descriptor != null)
						decoration.addOverlay(descriptor,
								IDecoration.BOTTOM_LEFT);
				}
			}
		}
	}

	private IResource getResourceFromEObject(EObject eObject) {
		IResource res = null;
		Resource eResource = eObject.eResource();
		if (eResource != null) {
			URI eUri = eResource.getURI();

			if (eUri.isPlatformResource()) {
				String platformString = eUri.toPlatformString(true);
				res = ResourcesPlugin.getWorkspace().getRoot()
						.findMember(platformString);
			}
		}
		return res;
	}
}
