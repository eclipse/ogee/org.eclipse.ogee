/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.navigation.tree;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.eclipse.ogee.navigation.tree"; //$NON-NLS-1$

	// The structural features
	public static final String KEY_PROPERTY_FEATURE = "keys"; //$NON-NLS-1$
	public static final String PROPERTY_FEATURE = "properties"; //$NON-NLS-1$
	public static final String NAVIGATION_PROPERTY_FEATURE = "navigationProperties"; //$NON-NLS-1$

	public static final String REFERENCED_SCHEMAS = "referencedschemas"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator() {
		plugin = this;
	}

	public void start(BundleContext context) throws NavigationTreeException {
		try {
			super.start(context);
		} catch (Exception e) {
			throw new NavigationTreeException(e);
		}
	}

	public void stop(BundleContext context) throws NavigationTreeException {
		plugin = null;
		try {
			super.stop(context);
		} catch (Exception e) {
			throw new NavigationTreeException(e);
		}
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		super.initializeImageRegistry(reg);
		Bundle bundle = Platform.getBundle(PLUGIN_ID);

		IPath path = new Path("icons/referencedschemas.gif"); //$NON-NLS-1$
		URL url = FileLocator.find(bundle, path, null);
		ImageDescriptor desc = ImageDescriptor.createFromURL(url);
		reg.put(REFERENCED_SCHEMAS, desc);
	}
}
