/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.navigation.tree;

public class NavigationTreeException extends Exception {

	private static final long serialVersionUID = -2890261761400370669L;

	public NavigationTreeException() {
		super();
	}

	public NavigationTreeException(Throwable exception) {
		super(exception);
	}

}
