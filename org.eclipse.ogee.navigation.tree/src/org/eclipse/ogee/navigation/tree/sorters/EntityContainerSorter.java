/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.navigation.tree.sorters;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.EntitySet;

public class EntityContainerSorter extends ViewerSorter {

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		if ((e1 instanceof EntitySet) && (e2 instanceof EntitySet)) {
			return super.compare(viewer, e1, e2);
		} else if (e1 instanceof EntitySet) {
			return -1;
		} else if (e2 instanceof EntitySet) {
			return 1;
		} else if ((e1 instanceof AssociationSet)
				&& (e2 instanceof AssociationSet)) {
			return super.compare(viewer, e1, e2);
		} else if (e1 instanceof AssociationSet) {
			return -1;
		} else if (e2 instanceof AssociationSet) {
			return 1;
		} else {
			return super.compare(viewer, e1, e2);
		}
	}

}
