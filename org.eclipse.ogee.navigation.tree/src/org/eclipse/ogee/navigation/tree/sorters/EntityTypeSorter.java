/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.navigation.tree.sorters;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.ogee.navigation.tree.Activator;

public class EntityTypeSorter extends ViewerSorter {

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {

		EObject eo1 = (EObject) e1;
		EObject eo2 = (EObject) e2;

		if ((eo1.eContainingFeature().getName() == Activator.KEY_PROPERTY_FEATURE)
				&& (eo2.eContainingFeature().getName() == Activator.KEY_PROPERTY_FEATURE)) {
			return super.compare(viewer, e1, e2);
		} else if (eo1.eContainingFeature().getName() == Activator.KEY_PROPERTY_FEATURE) {
			return -1;
		} else if (eo2.eContainingFeature().getName() == Activator.KEY_PROPERTY_FEATURE) {
			return 1;
		} else if ((eo1.eContainingFeature().getName() == Activator.PROPERTY_FEATURE)
				&& (eo2.eContainingFeature().getName() == Activator.PROPERTY_FEATURE)) {
			return super.compare(viewer, e1, e2);
		} else if (eo1.eContainingFeature().getName() == Activator.PROPERTY_FEATURE) {
			return -1;
		} else if (eo2.eContainingFeature().getName() == Activator.PROPERTY_FEATURE) {
			return 1;
		} else {
			return super.compare(viewer, e1, e2);
		}

	}

}
