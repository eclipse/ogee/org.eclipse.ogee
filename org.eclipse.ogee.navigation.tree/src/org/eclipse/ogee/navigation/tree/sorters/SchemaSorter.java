/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.navigation.tree.sorters;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;

public class SchemaSorter extends ViewerSorter {

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {

		if ((e1 instanceof EntityType) && (e2 instanceof EntityType)) {
			return super.compare(viewer, e1, e2);
		} else if (e1 instanceof EntityContainer) {
			return 1;
		} else if (e2 instanceof EntityContainer) {
			return -1;
		} else if (e1 instanceof EntityType) {
			return -1;
		} else if (e2 instanceof EntityType) {
			return 1;
		} else if ((e1 instanceof ComplexType) && (e2 instanceof ComplexType)) {
			return super.compare(viewer, e1, e2);
		} else if (e1 instanceof ComplexType) {
			return -1;
		} else if (e2 instanceof ComplexType) {
			return 1;
		} else if ((e1 instanceof Association) && (e2 instanceof Association)) {
			return super.compare(viewer, e1, e2);
		} else if (e1 instanceof EnumType) {
			return -1;
		} else if (e2 instanceof EnumType) {
			return 1;
		} else if (e1 instanceof Association) {
			return -1;
		} else if (e2 instanceof Association) {
			return 1;
		} else {
			return super.compare(viewer, e1, e2);
		}
	}

}
