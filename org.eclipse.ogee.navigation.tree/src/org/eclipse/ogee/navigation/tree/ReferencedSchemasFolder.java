/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.navigation.tree;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.swt.graphics.Image;

public class ReferencedSchemasFolder {
	private EDMXSet edmxSet = null;
	private List<Object> referencedSchemas;
	private static String folderLabel = Messages.ReferencedSchemasFolder;

	public ReferencedSchemasFolder(EDMXSet edmxSet) {
		this.edmxSet = edmxSet;
		referencedSchemas = new LinkedList<Object>();
	}

	public static Image getImage() {
		ImageRegistry registry = Activator.getDefault().getImageRegistry();
		Image image = registry.get(Activator.REFERENCED_SCHEMAS);
		return image;
	}

	public String getLabel() {
		return folderLabel;
	}

	public EDMXSet getEdmxSet() {
		return edmxSet;
	}

	public List<Object> getReferncedSchemas() {
		return referencedSchemas;
	}

}
