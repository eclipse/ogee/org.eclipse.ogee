/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.navigation.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IResourceContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.BooleanValue;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class AddReference extends AbstractHandler {

	private EDMX edmx;
	private EDMXSet edmxSet;
	private ValueTerm valueTerm;
	private Schema schema1_1;
	private Schema schema1_2;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		ISelection selection = window.getSelectionService().getSelection();

		// If we have a selection lets open the editor
		if (selection != null) {
			try {
				TreeSelection tselection = (TreeSelection) selection;
				Schema schema = (Schema) tselection.getFirstElement();
				edmxSet = (EDMXSet) schema.eContainer();
				createExampleEDMX();
				addEDMX();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	private void createExampleEDMX() {
		EntityType entity = null;
		Property property = null;
		EntitySet entitySet = null;
		BooleanValue booleanValue = null;

		// Create an example EDMX
		edmx = OdataFactory.eINSTANCE.createEDMX();
		DataService dataService = OdataFactory.eINSTANCE.createDataService();
		dataService.setVersion("1"); //$NON-NLS-1$

		// Create an example vocabulary
		schema1_1 = OdataFactory.eINSTANCE.createSchema();
		schema1_1.setNamespace("custom_schema"); //$NON-NLS-1$
		schema1_1.getClassifiers().add(SchemaClassifier.SERVICE);

		// Add a single value term "customizable"
		valueTerm = OdataFactory.eINSTANCE.createValueTerm();
		valueTerm.setName("customizable"); //$NON-NLS-1$
		SimpleType type = OdataFactory.eINSTANCE.createSimpleType();
		type.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		type.setDefaultValue(booleanValue);
		SimpleTypeUsage usage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		usage.setSimpleType(type);
		valueTerm.setType(usage);

		schema1_1.getValueTerms().add(valueTerm);

		// create entity type
		entity = OdataFactory.eINSTANCE.createEntityType();
		entity.setName("ET"); //$NON-NLS-1$
		schema1_1.getEntityTypes().add(entity);

		// Define usage types
		SimpleType simpleTypeP1 = OdataFactory.eINSTANCE.createSimpleType();
		simpleTypeP1.setType(EDMTypes.STRING);
		simpleTypeP1.setMaxLength(10);
		SimpleTypeUsage usageP1 = OdataFactory.eINSTANCE
				.createSimpleTypeUsage();
		usageP1.setSimpleType(simpleTypeP1);
		SimpleType simpleTypeP2 = OdataFactory.eINSTANCE.createSimpleType();
		simpleTypeP2.setType(EDMTypes.STRING);
		simpleTypeP2.setMaxLength(10);
		SimpleTypeUsage usageK1 = OdataFactory.eINSTANCE
				.createSimpleTypeUsage();
		usageK1.setSimpleType(simpleTypeP2);

		// create key property
		property = OdataFactory.eINSTANCE.createProperty();
		property.setName("K1"); //$NON-NLS-1$
		property.setNullable(false);
		property.setType(usageK1);
		entity.getKeys().add(property);

		// create attribute property
		property = OdataFactory.eINSTANCE.createProperty();
		property.setName("P1"); //$NON-NLS-1$
		property.setNullable(true);
		property.setType(usageP1);
		entity.getProperties().add(property);

		// create entity container
		EntityContainer container = OdataFactory.eINSTANCE
				.createEntityContainer();
		container.setName("custom_schema"); //$NON-NLS-1$
		container.setDefault(true);
		schema1_1.getContainers().add(container);

		// create entity set
		entitySet = OdataFactory.eINSTANCE.createEntitySet();
		entitySet.setName("ES1"); //$NON-NLS-1$
		entitySet.setType(entity);
		container.getEntitySets().add(entitySet);

		// Add the schema to the example EDMX
		dataService.getSchemata().add(schema1_1);

		// Create another example vocabulary
		schema1_2 = OdataFactory.eINSTANCE.createSchema();
		schema1_2.setNamespace("custom_vocabulary_2"); //$NON-NLS-1$
		schema1_2.getClassifiers().add(SchemaClassifier.VOCABULARY);

		// Add a single value term "customizable2"
		valueTerm = OdataFactory.eINSTANCE.createValueTerm();
		valueTerm.setName("customizable2"); //$NON-NLS-1$
		type = OdataFactory.eINSTANCE.createSimpleType();
		type.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		type.setDefaultValue(booleanValue);
		usage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		usage.setSimpleType(type);
		valueTerm.setType(usage);

		schema1_2.getValueTerms().add(valueTerm);

		// Add the vocabulary to the example EDMX
		dataService.getSchemata().add(schema1_2);

		edmx.setDataService(dataService);
	}

	public void addEDMX() {
		try {
			IModelContext.INSTANCE.getResourceContext(edmxSet);
			IResourceContext resourceContext = IModelContext.INSTANCE
					.getResourceContext(edmxSet);
			resourceContext.addEDMX(edmx);
		} catch (ModelAPIException e) {
			e.printStackTrace();
		}
	}
}
