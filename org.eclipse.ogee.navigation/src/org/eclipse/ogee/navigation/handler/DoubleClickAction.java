/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.navigation.handler;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

public class DoubleClickAction extends Action {

	protected Shell shell;

	protected TreeViewer viewer;

	public DoubleClickAction() {
		super();
	}

	public void run() {
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		final ISelection selection = window.getSelectionService()
				.getSelection();
		final IWorkbenchPage page = window.getActivePage();

		// If we have a selection lets open the editor
		if ((selection != null) && (page != null)) {
			if (selection instanceof TreeSelection) {
				PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
					@Override
					public void run() {
						EObject selectedEObject = (EObject) ((TreeSelection) selection)
								.getFirstElement();
						Resource resource = selectedEObject.eResource();
						if (resource == null) {
							return;
						}
						URI uri = resource.getURI();
						if (!uri.isPlatform()) {
							return;
						}
						String uriString = uri.trimFragment().toPlatformString(
								true);
						Path path = new Path(uriString);
						IFile odataModelFile = ResourcesPlugin.getWorkspace()
								.getRoot().getFile(path.makeAbsolute());

						try {
							IDE.openEditor(page, odataModelFile);
						} catch (PartInitException e) {
							e.printStackTrace();
						}

					}
				});
			}

		}

	}
}