/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.navigation;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.graphiti.ui.editor.DiagramEditorInput;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.navigator.ILinkHelper;
import org.eclipse.ui.part.FileEditorInput;

/**
 * Provides information to the Common Navigator (Project Explorer) on how to
 * link active OData editors with selections of diagram nodes.
 * 
 */
public class ODataEditorLinkHelper implements ILinkHelper {

	/**
	 * Return a selection that contains the file that the given editor input
	 * represent. The {@link StructuredSelection} will be compared with nodes in
	 * the tree. See also extension point
	 * {@code "org.eclipse.ui.navigator.linkHelper"}.
	 */
	@Override
	public IStructuredSelection findSelection(IEditorInput editorInput) {

		if (editorInput instanceof DiagramEditorInput) {
			if (editorInput.exists()) {
				DiagramEditorInput diagramEditorInput = (DiagramEditorInput) editorInput;
				final IFile file = getFile(diagramEditorInput.getUri());
				if (file != null) {
					return new StructuredSelection(file);
				}
			}
		}
		return StructuredSelection.EMPTY;
	}

	/**
	 * Links IFile/EObject to FileEditorInput.
	 */
	@Override
	public void activateEditor(IWorkbenchPage aPage,
			IStructuredSelection aSelection) {

		if (aSelection == null || aSelection.isEmpty()) {
			return;
		}
		Object selection = aSelection.getFirstElement();
		if (selection instanceof IFile) {
			IEditorInput fileInput = new FileEditorInput((IFile) selection);
			IEditorPart editor = null;
			if ((editor = aPage.findEditor(fileInput)) != null) {
				aPage.bringToTop(editor);
			}
		} else if (selection instanceof EObject) {
			EObject eObject = (EObject) selection;
			if (eObject != null) {
				URI uri = EcoreUtil.getURI(eObject);
				String platformString = uri.toPlatformString(true);
				IFile file = ResourcesPlugin.getWorkspace().getRoot()
						.getFile(new Path(platformString));
				if (file.exists()) {
					IEditorInput fileInput = new FileEditorInput(file);
					IEditorPart editor = null;
					if ((editor = aPage.findEditor(fileInput)) != null) {
						aPage.bringToTop(editor);
					}
				}
			}
		}
	}

	private static IFile getFile(URI uri) {

		if (uri == null) {
			return null;
		}

		final IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace()
				.getRoot();

		// File URIs
		final String filePath = getWorkspaceFilePath(uri.trimFragment());
		if (filePath == null) {
			final IPath location = Path.fromOSString(uri.toString());
			final IFile file = workspaceRoot.getFileForLocation(location);
			if (file != null) {
				return file;
			}
			return null;
		}
		// Platform resource URIs
		final IResource workspaceResource = workspaceRoot.findMember(filePath);
		return (IFile) workspaceResource;
	}

	private static String getWorkspaceFilePath(URI uri) {

		if (uri.isPlatform()) {
			return uri.toPlatformString(true);
		}
		return null;
	}

}
