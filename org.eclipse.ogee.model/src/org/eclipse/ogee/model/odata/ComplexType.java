/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.ComplexType#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ComplexType#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ComplexType#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ComplexType#getBaseType <em>Base Type</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ComplexType#getDerivedTypes <em>Derived Types</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getComplexType()
 * @model
 * @generated
 */
public interface ComplexType extends IDocumentable, IAnnotationTarget, ITypeTerm, IRecordValueType {
	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getComplexType_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getProperties();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getComplexType_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ComplexType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getComplexType_Abstract()
	 * @model
	 * @generated
	 */
	boolean isAbstract();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ComplexType#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
	void setAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Base Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.ogee.model.odata.ComplexType#getDerivedTypes <em>Derived Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Type</em>' reference.
	 * @see #setBaseType(ComplexType)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getComplexType_BaseType()
	 * @see org.eclipse.ogee.model.odata.ComplexType#getDerivedTypes
	 * @model opposite="derivedTypes"
	 * @generated
	 */
	ComplexType getBaseType();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ComplexType#getBaseType <em>Base Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Type</em>' reference.
	 * @see #getBaseType()
	 * @generated
	 */
	void setBaseType(ComplexType value);

	/**
	 * Returns the value of the '<em><b>Derived Types</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.ComplexType}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.ogee.model.odata.ComplexType#getBaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Types</em>' reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getComplexType_DerivedTypes()
	 * @see org.eclipse.ogee.model.odata.ComplexType#getBaseType
	 * @model opposite="baseType"
	 * @generated
	 */
	EList<ComplexType> getDerivedTypes();

} // ComplexType
