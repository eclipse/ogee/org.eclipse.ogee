/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Return Entity Type Usage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#getEntitySet <em>Entity Set</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#getEntityType <em>Entity Type</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#isCollection <em>Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getReturnEntityTypeUsage()
 * @model
 * @generated
 */
public interface ReturnEntityTypeUsage extends IFunctionReturnTypeUsage {
	/**
	 * Returns the value of the '<em><b>Entity Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity Set</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Set</em>' reference.
	 * @see #setEntitySet(EntitySet)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getReturnEntityTypeUsage_EntitySet()
	 * @model
	 * @generated
	 */
	EntitySet getEntitySet();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#getEntitySet <em>Entity Set</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity Set</em>' reference.
	 * @see #getEntitySet()
	 * @generated
	 */
	void setEntitySet(EntitySet value);

	/**
	 * Returns the value of the '<em><b>Entity Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Type</em>' reference.
	 * @see #setEntityType(EntityType)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getReturnEntityTypeUsage_EntityType()
	 * @model required="true"
	 * @generated
	 */
	EntityType getEntityType();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#getEntityType <em>Entity Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity Type</em>' reference.
	 * @see #getEntityType()
	 * @generated
	 */
	void setEntityType(EntityType value);

	/**
	 * Returns the value of the '<em><b>Collection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection</em>' attribute.
	 * @see #setCollection(boolean)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getReturnEntityTypeUsage_Collection()
	 * @model
	 * @generated
	 */
	boolean isCollection();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#isCollection <em>Collection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collection</em>' attribute.
	 * @see #isCollection()
	 * @generated
	 */
	void setCollection(boolean value);

} // ReturnEntityTypeUsage
