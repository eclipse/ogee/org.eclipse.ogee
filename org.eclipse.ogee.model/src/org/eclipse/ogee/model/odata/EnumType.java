/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.EnumType#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EnumType#isFlags <em>Flags</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EnumType#getMembers <em>Members</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EnumType#getUnderlyingType <em>Underlying Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getEnumType()
 * @model
 * @generated
 */
public interface EnumType extends IDocumentable, IAnnotationTarget {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEnumType_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EnumType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Flags</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flags</em>' attribute.
	 * @see #setFlags(boolean)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEnumType_Flags()
	 * @model
	 * @generated
	 */
	boolean isFlags();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EnumType#isFlags <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Flags</em>' attribute.
	 * @see #isFlags()
	 * @generated
	 */
	void setFlags(boolean value);

	/**
	 * Returns the value of the '<em><b>Members</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.EnumMember}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Members</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Members</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEnumType_Members()
	 * @model containment="true"
	 * @generated
	 */
	EList<EnumMember> getMembers();

	/**
	 * Returns the value of the '<em><b>Underlying Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.ogee.model.odata.EDMTypes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Underlying Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Underlying Type</em>' attribute.
	 * @see org.eclipse.ogee.model.odata.EDMTypes
	 * @see #setUnderlyingType(EDMTypes)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEnumType_UnderlyingType()
	 * @model
	 * @generated
	 */
	EDMTypes getUnderlyingType();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EnumType#getUnderlyingType <em>Underlying Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Underlying Type</em>' attribute.
	 * @see org.eclipse.ogee.model.odata.EDMTypes
	 * @see #getUnderlyingType()
	 * @generated
	 */
	void setUnderlyingType(EDMTypes value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<EDMTypes> getApplicableUnderlyingTypes();

} // EnumType
