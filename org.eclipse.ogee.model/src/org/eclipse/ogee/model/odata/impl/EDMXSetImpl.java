/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Schema;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EDMX Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXSetImpl#getImportSource <em>Import Source</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXSetImpl#getImportDate <em>Import Date</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXSetImpl#getMainEDMX <em>Main EDMX</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXSetImpl#getEdmx <em>Edmx</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXSetImpl#getFullScopeEDMX <em>Full Scope EDMX</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXSetImpl#getAnnotationsScopeEDMX <em>Annotations Scope EDMX</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXSetImpl#getSchemata <em>Schemata</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EDMXSetImpl extends EObjectImpl implements EDMXSet {
	/**
	 * The default value of the '{@link #getImportSource() <em>Import Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportSource()
	 * @generated
	 * @ordered
	 */
	protected static final String IMPORT_SOURCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImportSource() <em>Import Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportSource()
	 * @generated
	 * @ordered
	 */
	protected String importSource = IMPORT_SOURCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getImportDate() <em>Import Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date IMPORT_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImportDate() <em>Import Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportDate()
	 * @generated
	 * @ordered
	 */
	protected Date importDate = IMPORT_DATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEdmx() <em>Edmx</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdmx()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap edmx;

	/**
	 * The cached value of the '{@link #getSchemata() <em>Schemata</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemata()
	 * @generated
	 * @ordered
	 */
	protected EList<Schema> schemata;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EDMXSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.EDMX_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImportSource() {
		return importSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImportSource(String newImportSource) {
		String oldImportSource = importSource;
		importSource = newImportSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.EDMX_SET__IMPORT_SOURCE, oldImportSource, importSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getImportDate() {
		return importDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImportDate(Date newImportDate) {
		Date oldImportDate = importDate;
		importDate = newImportDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.EDMX_SET__IMPORT_DATE, oldImportDate, importDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDMX getMainEDMX() {
		return (EDMX)getEdmx().get(OdataPackage.Literals.EDMX_SET__MAIN_EDMX, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMainEDMX(EDMX newMainEDMX, NotificationChain msgs) {
		return ((FeatureMap.Internal)getEdmx()).basicAdd(OdataPackage.Literals.EDMX_SET__MAIN_EDMX, newMainEDMX, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMainEDMX(EDMX newMainEDMX) {
		((FeatureMap.Internal)getEdmx()).set(OdataPackage.Literals.EDMX_SET__MAIN_EDMX, newMainEDMX);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getEdmx() {
		if (edmx == null) {
			edmx = new BasicFeatureMap(this, OdataPackage.EDMX_SET__EDMX);
		}
		return edmx;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EDMX> getFullScopeEDMX() {
		return getEdmx().list(OdataPackage.Literals.EDMX_SET__FULL_SCOPE_EDMX);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EDMX> getAnnotationsScopeEDMX() {
		return getEdmx().list(OdataPackage.Literals.EDMX_SET__ANNOTATIONS_SCOPE_EDMX);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Schema> getSchemata() {
		if (schemata == null) {
			schemata = new EObjectContainmentEList<Schema>(Schema.class, this, OdataPackage.EDMX_SET__SCHEMATA);
		}
		return schemata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.EDMX_SET__MAIN_EDMX:
				return basicSetMainEDMX(null, msgs);
			case OdataPackage.EDMX_SET__EDMX:
				return ((InternalEList<?>)getEdmx()).basicRemove(otherEnd, msgs);
			case OdataPackage.EDMX_SET__FULL_SCOPE_EDMX:
				return ((InternalEList<?>)getFullScopeEDMX()).basicRemove(otherEnd, msgs);
			case OdataPackage.EDMX_SET__ANNOTATIONS_SCOPE_EDMX:
				return ((InternalEList<?>)getAnnotationsScopeEDMX()).basicRemove(otherEnd, msgs);
			case OdataPackage.EDMX_SET__SCHEMATA:
				return ((InternalEList<?>)getSchemata()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.EDMX_SET__IMPORT_SOURCE:
				return getImportSource();
			case OdataPackage.EDMX_SET__IMPORT_DATE:
				return getImportDate();
			case OdataPackage.EDMX_SET__MAIN_EDMX:
				return getMainEDMX();
			case OdataPackage.EDMX_SET__EDMX:
				if (coreType) return getEdmx();
				return ((FeatureMap.Internal)getEdmx()).getWrapper();
			case OdataPackage.EDMX_SET__FULL_SCOPE_EDMX:
				return getFullScopeEDMX();
			case OdataPackage.EDMX_SET__ANNOTATIONS_SCOPE_EDMX:
				return getAnnotationsScopeEDMX();
			case OdataPackage.EDMX_SET__SCHEMATA:
				return getSchemata();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.EDMX_SET__IMPORT_SOURCE:
				setImportSource((String)newValue);
				return;
			case OdataPackage.EDMX_SET__IMPORT_DATE:
				setImportDate((Date)newValue);
				return;
			case OdataPackage.EDMX_SET__MAIN_EDMX:
				setMainEDMX((EDMX)newValue);
				return;
			case OdataPackage.EDMX_SET__EDMX:
				((FeatureMap.Internal)getEdmx()).set(newValue);
				return;
			case OdataPackage.EDMX_SET__FULL_SCOPE_EDMX:
				getFullScopeEDMX().clear();
				getFullScopeEDMX().addAll((Collection<? extends EDMX>)newValue);
				return;
			case OdataPackage.EDMX_SET__ANNOTATIONS_SCOPE_EDMX:
				getAnnotationsScopeEDMX().clear();
				getAnnotationsScopeEDMX().addAll((Collection<? extends EDMX>)newValue);
				return;
			case OdataPackage.EDMX_SET__SCHEMATA:
				getSchemata().clear();
				getSchemata().addAll((Collection<? extends Schema>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.EDMX_SET__IMPORT_SOURCE:
				setImportSource(IMPORT_SOURCE_EDEFAULT);
				return;
			case OdataPackage.EDMX_SET__IMPORT_DATE:
				setImportDate(IMPORT_DATE_EDEFAULT);
				return;
			case OdataPackage.EDMX_SET__MAIN_EDMX:
				setMainEDMX((EDMX)null);
				return;
			case OdataPackage.EDMX_SET__EDMX:
				getEdmx().clear();
				return;
			case OdataPackage.EDMX_SET__FULL_SCOPE_EDMX:
				getFullScopeEDMX().clear();
				return;
			case OdataPackage.EDMX_SET__ANNOTATIONS_SCOPE_EDMX:
				getAnnotationsScopeEDMX().clear();
				return;
			case OdataPackage.EDMX_SET__SCHEMATA:
				getSchemata().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.EDMX_SET__IMPORT_SOURCE:
				return IMPORT_SOURCE_EDEFAULT == null ? importSource != null : !IMPORT_SOURCE_EDEFAULT.equals(importSource);
			case OdataPackage.EDMX_SET__IMPORT_DATE:
				return IMPORT_DATE_EDEFAULT == null ? importDate != null : !IMPORT_DATE_EDEFAULT.equals(importDate);
			case OdataPackage.EDMX_SET__MAIN_EDMX:
				return getMainEDMX() != null;
			case OdataPackage.EDMX_SET__EDMX:
				return edmx != null && !edmx.isEmpty();
			case OdataPackage.EDMX_SET__FULL_SCOPE_EDMX:
				return !getFullScopeEDMX().isEmpty();
			case OdataPackage.EDMX_SET__ANNOTATIONS_SCOPE_EDMX:
				return !getAnnotationsScopeEDMX().isEmpty();
			case OdataPackage.EDMX_SET__SCHEMATA:
				return schemata != null && !schemata.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (importSource: "); //$NON-NLS-1$
		result.append(importSource);
		result.append(", importDate: "); //$NON-NLS-1$
		result.append(importDate);
		result.append(", edmx: "); //$NON-NLS-1$
		result.append(edmx);
		result.append(')');
		return result.toString();
	}

} //EDMXSetImpl
