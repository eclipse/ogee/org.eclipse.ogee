/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.odata.impl;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.ogee.model.odata.impl.messages"; //$NON-NLS-1$
	public static String ODATAFACTORYIMP_CLASS;
	public static String ODATAFACTORYIMP_DATATYPE;
	public static String ODATAFACTORYIMP_INVALID_CLASSIFIER;
	public static String ODATAFACTORYIMP_INVALID_ENUM;
	public static String ODATAFACTORYIMP_VALUE;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
