/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.IDocumentable;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Role;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.AssociationImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.AssociationImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.AssociationImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.AssociationImpl#getEnds <em>Ends</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.AssociationImpl#getReferentialConstraint <em>Referential Constraint</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.AssociationImpl#getAssociationSets <em>Association Sets</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssociationImpl extends EObjectImpl implements Association {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected Documentation documentation;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEnds() <em>Ends</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnds()
	 * @generated
	 * @ordered
	 */
	protected EList<Role> ends;

	/**
	 * The cached value of the '{@link #getReferentialConstraint() <em>Referential Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferentialConstraint()
	 * @generated
	 * @ordered
	 */
	protected ReferentialConstraint referentialConstraint;

	/**
	 * The cached value of the '{@link #getAssociationSets() <em>Association Sets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociationSets()
	 * @generated
	 * @ordered
	 */
	protected EList<AssociationSet> associationSets;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.ASSOCIATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectWithInverseResolvingEList<Annotation>(Annotation.class, this, OdataPackage.ASSOCIATION__ANNOTATIONS, OdataPackage.ANNOTATION__TARGET);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDocumentation(Documentation newDocumentation, NotificationChain msgs) {
		Documentation oldDocumentation = documentation;
		documentation = newDocumentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.ASSOCIATION__DOCUMENTATION, oldDocumentation, newDocumentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentation(Documentation newDocumentation) {
		if (newDocumentation != documentation) {
			NotificationChain msgs = null;
			if (documentation != null)
				msgs = ((InternalEObject)documentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.ASSOCIATION__DOCUMENTATION, null, msgs);
			if (newDocumentation != null)
				msgs = ((InternalEObject)newDocumentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.ASSOCIATION__DOCUMENTATION, null, msgs);
			msgs = basicSetDocumentation(newDocumentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ASSOCIATION__DOCUMENTATION, newDocumentation, newDocumentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ASSOCIATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Role> getEnds() {
		if (ends == null) {
			ends = new EObjectContainmentEList<Role>(Role.class, this, OdataPackage.ASSOCIATION__ENDS);
		}
		return ends;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferentialConstraint getReferentialConstraint() {
		return referentialConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReferentialConstraint(ReferentialConstraint newReferentialConstraint, NotificationChain msgs) {
		ReferentialConstraint oldReferentialConstraint = referentialConstraint;
		referentialConstraint = newReferentialConstraint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.ASSOCIATION__REFERENTIAL_CONSTRAINT, oldReferentialConstraint, newReferentialConstraint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferentialConstraint(ReferentialConstraint newReferentialConstraint) {
		if (newReferentialConstraint != referentialConstraint) {
			NotificationChain msgs = null;
			if (referentialConstraint != null)
				msgs = ((InternalEObject)referentialConstraint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.ASSOCIATION__REFERENTIAL_CONSTRAINT, null, msgs);
			if (newReferentialConstraint != null)
				msgs = ((InternalEObject)newReferentialConstraint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.ASSOCIATION__REFERENTIAL_CONSTRAINT, null, msgs);
			msgs = basicSetReferentialConstraint(newReferentialConstraint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ASSOCIATION__REFERENTIAL_CONSTRAINT, newReferentialConstraint, newReferentialConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssociationSet> getAssociationSets() {
		if (associationSets == null) {
			associationSets = new EObjectWithInverseResolvingEList<AssociationSet>(AssociationSet.class, this, OdataPackage.ASSOCIATION__ASSOCIATION_SETS, OdataPackage.ASSOCIATION_SET__ASSOCIATION);
		}
		return associationSets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION__ANNOTATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAnnotations()).basicAdd(otherEnd, msgs);
			case OdataPackage.ASSOCIATION__ASSOCIATION_SETS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAssociationSets()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case OdataPackage.ASSOCIATION__DOCUMENTATION:
				return basicSetDocumentation(null, msgs);
			case OdataPackage.ASSOCIATION__ENDS:
				return ((InternalEList<?>)getEnds()).basicRemove(otherEnd, msgs);
			case OdataPackage.ASSOCIATION__REFERENTIAL_CONSTRAINT:
				return basicSetReferentialConstraint(null, msgs);
			case OdataPackage.ASSOCIATION__ASSOCIATION_SETS:
				return ((InternalEList<?>)getAssociationSets()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION__ANNOTATIONS:
				return getAnnotations();
			case OdataPackage.ASSOCIATION__DOCUMENTATION:
				return getDocumentation();
			case OdataPackage.ASSOCIATION__NAME:
				return getName();
			case OdataPackage.ASSOCIATION__ENDS:
				return getEnds();
			case OdataPackage.ASSOCIATION__REFERENTIAL_CONSTRAINT:
				return getReferentialConstraint();
			case OdataPackage.ASSOCIATION__ASSOCIATION_SETS:
				return getAssociationSets();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case OdataPackage.ASSOCIATION__DOCUMENTATION:
				setDocumentation((Documentation)newValue);
				return;
			case OdataPackage.ASSOCIATION__NAME:
				setName((String)newValue);
				return;
			case OdataPackage.ASSOCIATION__ENDS:
				getEnds().clear();
				getEnds().addAll((Collection<? extends Role>)newValue);
				return;
			case OdataPackage.ASSOCIATION__REFERENTIAL_CONSTRAINT:
				setReferentialConstraint((ReferentialConstraint)newValue);
				return;
			case OdataPackage.ASSOCIATION__ASSOCIATION_SETS:
				getAssociationSets().clear();
				getAssociationSets().addAll((Collection<? extends AssociationSet>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case OdataPackage.ASSOCIATION__DOCUMENTATION:
				setDocumentation((Documentation)null);
				return;
			case OdataPackage.ASSOCIATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case OdataPackage.ASSOCIATION__ENDS:
				getEnds().clear();
				return;
			case OdataPackage.ASSOCIATION__REFERENTIAL_CONSTRAINT:
				setReferentialConstraint((ReferentialConstraint)null);
				return;
			case OdataPackage.ASSOCIATION__ASSOCIATION_SETS:
				getAssociationSets().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case OdataPackage.ASSOCIATION__DOCUMENTATION:
				return documentation != null;
			case OdataPackage.ASSOCIATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case OdataPackage.ASSOCIATION__ENDS:
				return ends != null && !ends.isEmpty();
			case OdataPackage.ASSOCIATION__REFERENTIAL_CONSTRAINT:
				return referentialConstraint != null;
			case OdataPackage.ASSOCIATION__ASSOCIATION_SETS:
				return associationSets != null && !associationSets.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IDocumentable.class) {
			switch (derivedFeatureID) {
				case OdataPackage.ASSOCIATION__DOCUMENTATION: return OdataPackage.IDOCUMENTABLE__DOCUMENTATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IDocumentable.class) {
			switch (baseFeatureID) {
				case OdataPackage.IDOCUMENTABLE__DOCUMENTATION: return OdataPackage.ASSOCIATION__DOCUMENTATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //AssociationImpl
