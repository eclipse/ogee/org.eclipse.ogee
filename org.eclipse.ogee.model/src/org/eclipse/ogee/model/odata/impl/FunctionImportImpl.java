/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.Binding;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IDocumentable;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Parameter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Import</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.FunctionImportImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.FunctionImportImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.FunctionImportImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.FunctionImportImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.FunctionImportImpl#getBinding <em>Binding</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.FunctionImportImpl#isSideEffecting <em>Side Effecting</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.FunctionImportImpl#getReturnType <em>Return Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FunctionImportImpl extends EObjectImpl implements FunctionImport {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected Documentation documentation;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> parameters;

	/**
	 * The cached value of the '{@link #getBinding() <em>Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinding()
	 * @generated
	 * @ordered
	 */
	protected Binding binding;

	/**
	 * The default value of the '{@link #isSideEffecting() <em>Side Effecting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSideEffecting()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SIDE_EFFECTING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSideEffecting() <em>Side Effecting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSideEffecting()
	 * @generated
	 * @ordered
	 */
	protected boolean sideEffecting = SIDE_EFFECTING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReturnType() <em>Return Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnType()
	 * @generated
	 * @ordered
	 */
	protected IFunctionReturnTypeUsage returnType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionImportImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.FUNCTION_IMPORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectWithInverseResolvingEList<Annotation>(Annotation.class, this, OdataPackage.FUNCTION_IMPORT__ANNOTATIONS, OdataPackage.ANNOTATION__TARGET);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDocumentation(Documentation newDocumentation, NotificationChain msgs) {
		Documentation oldDocumentation = documentation;
		documentation = newDocumentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.FUNCTION_IMPORT__DOCUMENTATION, oldDocumentation, newDocumentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentation(Documentation newDocumentation) {
		if (newDocumentation != documentation) {
			NotificationChain msgs = null;
			if (documentation != null)
				msgs = ((InternalEObject)documentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.FUNCTION_IMPORT__DOCUMENTATION, null, msgs);
			if (newDocumentation != null)
				msgs = ((InternalEObject)newDocumentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.FUNCTION_IMPORT__DOCUMENTATION, null, msgs);
			msgs = basicSetDocumentation(newDocumentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.FUNCTION_IMPORT__DOCUMENTATION, newDocumentation, newDocumentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.FUNCTION_IMPORT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<Parameter>(Parameter.class, this, OdataPackage.FUNCTION_IMPORT__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Binding getBinding() {
		return binding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBinding(Binding newBinding, NotificationChain msgs) {
		Binding oldBinding = binding;
		binding = newBinding;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.FUNCTION_IMPORT__BINDING, oldBinding, newBinding);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBinding(Binding newBinding) {
		if (newBinding != binding) {
			NotificationChain msgs = null;
			if (binding != null)
				msgs = ((InternalEObject)binding).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.FUNCTION_IMPORT__BINDING, null, msgs);
			if (newBinding != null)
				msgs = ((InternalEObject)newBinding).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.FUNCTION_IMPORT__BINDING, null, msgs);
			msgs = basicSetBinding(newBinding, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.FUNCTION_IMPORT__BINDING, newBinding, newBinding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSideEffecting() {
		return sideEffecting;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSideEffecting(boolean newSideEffecting) {
		boolean oldSideEffecting = sideEffecting;
		sideEffecting = newSideEffecting;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.FUNCTION_IMPORT__SIDE_EFFECTING, oldSideEffecting, sideEffecting));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IFunctionReturnTypeUsage getReturnType() {
		return returnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReturnType(IFunctionReturnTypeUsage newReturnType, NotificationChain msgs) {
		IFunctionReturnTypeUsage oldReturnType = returnType;
		returnType = newReturnType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.FUNCTION_IMPORT__RETURN_TYPE, oldReturnType, newReturnType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnType(IFunctionReturnTypeUsage newReturnType) {
		if (newReturnType != returnType) {
			NotificationChain msgs = null;
			if (returnType != null)
				msgs = ((InternalEObject)returnType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.FUNCTION_IMPORT__RETURN_TYPE, null, msgs);
			if (newReturnType != null)
				msgs = ((InternalEObject)newReturnType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.FUNCTION_IMPORT__RETURN_TYPE, null, msgs);
			msgs = basicSetReturnType(newReturnType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.FUNCTION_IMPORT__RETURN_TYPE, newReturnType, newReturnType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.FUNCTION_IMPORT__ANNOTATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAnnotations()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.FUNCTION_IMPORT__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case OdataPackage.FUNCTION_IMPORT__DOCUMENTATION:
				return basicSetDocumentation(null, msgs);
			case OdataPackage.FUNCTION_IMPORT__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
			case OdataPackage.FUNCTION_IMPORT__BINDING:
				return basicSetBinding(null, msgs);
			case OdataPackage.FUNCTION_IMPORT__RETURN_TYPE:
				return basicSetReturnType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.FUNCTION_IMPORT__ANNOTATIONS:
				return getAnnotations();
			case OdataPackage.FUNCTION_IMPORT__DOCUMENTATION:
				return getDocumentation();
			case OdataPackage.FUNCTION_IMPORT__NAME:
				return getName();
			case OdataPackage.FUNCTION_IMPORT__PARAMETERS:
				return getParameters();
			case OdataPackage.FUNCTION_IMPORT__BINDING:
				return getBinding();
			case OdataPackage.FUNCTION_IMPORT__SIDE_EFFECTING:
				return isSideEffecting();
			case OdataPackage.FUNCTION_IMPORT__RETURN_TYPE:
				return getReturnType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.FUNCTION_IMPORT__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case OdataPackage.FUNCTION_IMPORT__DOCUMENTATION:
				setDocumentation((Documentation)newValue);
				return;
			case OdataPackage.FUNCTION_IMPORT__NAME:
				setName((String)newValue);
				return;
			case OdataPackage.FUNCTION_IMPORT__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends Parameter>)newValue);
				return;
			case OdataPackage.FUNCTION_IMPORT__BINDING:
				setBinding((Binding)newValue);
				return;
			case OdataPackage.FUNCTION_IMPORT__SIDE_EFFECTING:
				setSideEffecting((Boolean)newValue);
				return;
			case OdataPackage.FUNCTION_IMPORT__RETURN_TYPE:
				setReturnType((IFunctionReturnTypeUsage)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.FUNCTION_IMPORT__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case OdataPackage.FUNCTION_IMPORT__DOCUMENTATION:
				setDocumentation((Documentation)null);
				return;
			case OdataPackage.FUNCTION_IMPORT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case OdataPackage.FUNCTION_IMPORT__PARAMETERS:
				getParameters().clear();
				return;
			case OdataPackage.FUNCTION_IMPORT__BINDING:
				setBinding((Binding)null);
				return;
			case OdataPackage.FUNCTION_IMPORT__SIDE_EFFECTING:
				setSideEffecting(SIDE_EFFECTING_EDEFAULT);
				return;
			case OdataPackage.FUNCTION_IMPORT__RETURN_TYPE:
				setReturnType((IFunctionReturnTypeUsage)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.FUNCTION_IMPORT__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case OdataPackage.FUNCTION_IMPORT__DOCUMENTATION:
				return documentation != null;
			case OdataPackage.FUNCTION_IMPORT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case OdataPackage.FUNCTION_IMPORT__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
			case OdataPackage.FUNCTION_IMPORT__BINDING:
				return binding != null;
			case OdataPackage.FUNCTION_IMPORT__SIDE_EFFECTING:
				return sideEffecting != SIDE_EFFECTING_EDEFAULT;
			case OdataPackage.FUNCTION_IMPORT__RETURN_TYPE:
				return returnType != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IDocumentable.class) {
			switch (derivedFeatureID) {
				case OdataPackage.FUNCTION_IMPORT__DOCUMENTATION: return OdataPackage.IDOCUMENTABLE__DOCUMENTATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IDocumentable.class) {
			switch (baseFeatureID) {
				case OdataPackage.IDOCUMENTABLE__DOCUMENTATION: return OdataPackage.FUNCTION_IMPORT__DOCUMENTATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", sideEffecting: "); //$NON-NLS-1$
		result.append(sideEffecting);
		result.append(')');
		return result.toString();
	}

} //FunctionImportImpl
