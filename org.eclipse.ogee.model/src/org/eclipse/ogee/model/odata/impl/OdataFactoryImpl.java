/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Map;
import java.util.UUID;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.BinaryValue;
import org.eclipse.ogee.model.odata.Binding;
import org.eclipse.ogee.model.odata.BooleanValue;
import org.eclipse.ogee.model.odata.ByteValue;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.DateTimeOffsetValue;
import org.eclipse.ogee.model.odata.DateTimeValue;
import org.eclipse.ogee.model.odata.DecimalValue;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.DoubleValue;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXAnnotationsReference;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.EnumValue;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.GuidValue;
import org.eclipse.ogee.model.odata.IncludeRestriction;
import org.eclipse.ogee.model.odata.Int16Value;
import org.eclipse.ogee.model.odata.Int32Value;
import org.eclipse.ogee.model.odata.Int64Value;
import org.eclipse.ogee.model.odata.Multiplicity;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.PathValue;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.RecordValue;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.SByteValue;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SingleValue;
import org.eclipse.ogee.model.odata.StringValue;
import org.eclipse.ogee.model.odata.TimeValue;
import org.eclipse.ogee.model.odata.TypeAnnotation;
import org.eclipse.ogee.model.odata.TypeTermTargets;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.model.odata.ValueTermTargets;
import org.eclipse.ogee.model.odata.util.DateTime;
import org.eclipse.ogee.model.odata.util.DateTimeOffset;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OdataFactoryImpl extends EFactoryImpl implements OdataFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OdataFactory init() {
		try {
			OdataFactory theOdataFactory = (OdataFactory)EPackage.Registry.INSTANCE.getEFactory("http://odata/1.0");  //$NON-NLS-1$
			if (theOdataFactory != null) {
				return theOdataFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OdataFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OdataFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OdataPackage.ENTITY_TYPE: return createEntityType();
			case OdataPackage.PROPERTY: return createProperty();
			case OdataPackage.COMPLEX_TYPE: return createComplexType();
			case OdataPackage.ASSOCIATION: return createAssociation();
			case OdataPackage.NAVIGATION_PROPERTY: return createNavigationProperty();
			case OdataPackage.FUNCTION_IMPORT: return createFunctionImport();
			case OdataPackage.PARAMETER: return createParameter();
			case OdataPackage.ENTITY_SET: return createEntitySet();
			case OdataPackage.ASSOCIATION_SET: return createAssociationSet();
			case OdataPackage.ROLE: return createRole();
			case OdataPackage.ASSOCIATION_SET_END: return createAssociationSetEnd();
			case OdataPackage.EDMX: return createEDMX();
			case OdataPackage.REFERENTIAL_CONSTRAINT: return createReferentialConstraint();
			case OdataPackage.SIMPLE_TYPE: return createSimpleType();
			case OdataPackage.BINDING: return createBinding();
			case OdataPackage.VALUE_TERM: return createValueTerm();
			case OdataPackage.VALUE_ANNOTATION: return createValueAnnotation();
			case OdataPackage.TYPE_ANNOTATION: return createTypeAnnotation();
			case OdataPackage.ENTITY_CONTAINER: return createEntityContainer();
			case OdataPackage.ENUM_TYPE: return createEnumType();
			case OdataPackage.ENUM_MEMBER: return createEnumMember();
			case OdataPackage.DOCUMENTATION: return createDocumentation();
			case OdataPackage.EDMX_SET: return createEDMXSet();
			case OdataPackage.ENTITY_TYPE_USAGE: return createEntityTypeUsage();
			case OdataPackage.COMPLEX_TYPE_USAGE: return createComplexTypeUsage();
			case OdataPackage.ENUM_TYPE_USAGE: return createEnumTypeUsage();
			case OdataPackage.SIMPLE_TYPE_USAGE: return createSimpleTypeUsage();
			case OdataPackage.EDMX_REFERENCE: return createEDMXReference();
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE: return createEDMXAnnotationsReference();
			case OdataPackage.SCHEMA: return createSchema();
			case OdataPackage.DATA_SERVICE: return createDataService();
			case OdataPackage.INCLUDE_RESTRICTION: return createIncludeRestriction();
			case OdataPackage.PATH_VALUE: return createPathValue();
			case OdataPackage.USING: return createUsing();
			case OdataPackage.RECORD_VALUE: return createRecordValue();
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE: return createReturnEntityTypeUsage();
			case OdataPackage.PROPERTY_MAPPING: return (EObject)createPropertyMapping();
			case OdataPackage.VALUE_COLLECTION: return createValueCollection();
			case OdataPackage.PROPERTY_TO_VALUE_MAP: return (EObject)createPropertyToValueMap();
			case OdataPackage.BOOLEAN_VALUE: return createBooleanValue();
			case OdataPackage.STRING_VALUE: return createStringValue();
			case OdataPackage.BINARY_VALUE: return createBinaryValue();
			case OdataPackage.DATE_TIME_VALUE: return createDateTimeValue();
			case OdataPackage.DECIMAL_VALUE: return createDecimalValue();
			case OdataPackage.GUID_VALUE: return createGuidValue();
			case OdataPackage.TIME_VALUE: return createTimeValue();
			case OdataPackage.SINGLE_VALUE: return createSingleValue();
			case OdataPackage.DOUBLE_VALUE: return createDoubleValue();
			case OdataPackage.SBYTE_VALUE: return createSByteValue();
			case OdataPackage.INT16_VALUE: return createInt16Value();
			case OdataPackage.INT32_VALUE: return createInt32Value();
			case OdataPackage.INT64_VALUE: return createInt64Value();
			case OdataPackage.DATE_TIME_OFFSET_VALUE: return createDateTimeOffsetValue();
			case OdataPackage.BYTE_VALUE: return createByteValue();
			case OdataPackage.ENUM_VALUE: return createEnumValue();
			default:
				throw new IllegalArgumentException(Messages.ODATAFACTORYIMP_CLASS + eClass.getName() + Messages.ODATAFACTORYIMP_INVALID_CLASSIFIER);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OdataPackage.EDM_TYPES:
				return createEDMTypesFromString(eDataType, initialValue);
			case OdataPackage.MULTIPLICITY:
				return createMultiplicityFromString(eDataType, initialValue);
			case OdataPackage.TYPE_TERM_TARGETS:
				return createTypeTermTargetsFromString(eDataType, initialValue);
			case OdataPackage.VALUE_TERM_TARGETS:
				return createValueTermTargetsFromString(eDataType, initialValue);
			case OdataPackage.SCHEMA_CLASSIFIER:
				return createSchemaClassifierFromString(eDataType, initialValue);
			case OdataPackage.GUID_VALUE_TYPE:
				return createGuidValueTypeFromString(eDataType, initialValue);
			case OdataPackage.DATE_TIME_OFFSET_VALUE_TYPE:
				return createDateTimeOffsetValueTypeFromString(eDataType, initialValue);
			case OdataPackage.DATE_TIME:
				return createDateTimeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException(Messages.ODATAFACTORYIMP_DATATYPE + eDataType.getName() + Messages.ODATAFACTORYIMP_INVALID_CLASSIFIER);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OdataPackage.EDM_TYPES:
				return convertEDMTypesToString(eDataType, instanceValue);
			case OdataPackage.MULTIPLICITY:
				return convertMultiplicityToString(eDataType, instanceValue);
			case OdataPackage.TYPE_TERM_TARGETS:
				return convertTypeTermTargetsToString(eDataType, instanceValue);
			case OdataPackage.VALUE_TERM_TARGETS:
				return convertValueTermTargetsToString(eDataType, instanceValue);
			case OdataPackage.SCHEMA_CLASSIFIER:
				return convertSchemaClassifierToString(eDataType, instanceValue);
			case OdataPackage.GUID_VALUE_TYPE:
				return convertGuidValueTypeToString(eDataType, instanceValue);
			case OdataPackage.DATE_TIME_OFFSET_VALUE_TYPE:
				return convertDateTimeOffsetValueTypeToString(eDataType, instanceValue);
			case OdataPackage.DATE_TIME:
				return convertDateTimeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException(Messages.ODATAFACTORYIMP_DATATYPE + eDataType.getName() + Messages.ODATAFACTORYIMP_INVALID_CLASSIFIER);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityType createEntityType() {
		EntityTypeImpl entityType = new EntityTypeImpl();
		return entityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexType createComplexType() {
		ComplexTypeImpl complexType = new ComplexTypeImpl();
		return complexType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association createAssociation() {
		AssociationImpl association = new AssociationImpl();
		return association;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NavigationProperty createNavigationProperty() {
		NavigationPropertyImpl navigationProperty = new NavigationPropertyImpl();
		return navigationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionImport createFunctionImport() {
		FunctionImportImpl functionImport = new FunctionImportImpl();
		return functionImport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntitySet createEntitySet() {
		EntitySetImpl entitySet = new EntitySetImpl();
		return entitySet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationSet createAssociationSet() {
		AssociationSetImpl associationSet = new AssociationSetImpl();
		return associationSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role createRole() {
		RoleImpl role = new RoleImpl();
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationSetEnd createAssociationSetEnd() {
		AssociationSetEndImpl associationSetEnd = new AssociationSetEndImpl();
		return associationSetEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDMX createEDMX() {
		EDMXImpl edmx = new EDMXImpl();
		return edmx;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferentialConstraint createReferentialConstraint() {
		ReferentialConstraintImpl referentialConstraint = new ReferentialConstraintImpl();
		return referentialConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType createSimpleType() {
		SimpleTypeImpl simpleType = new SimpleTypeImpl();
		return simpleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Binding createBinding() {
		BindingImpl binding = new BindingImpl();
		return binding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueTerm createValueTerm() {
		ValueTermImpl valueTerm = new ValueTermImpl();
		return valueTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueAnnotation createValueAnnotation() {
		ValueAnnotationImpl valueAnnotation = new ValueAnnotationImpl();
		return valueAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeAnnotation createTypeAnnotation() {
		TypeAnnotationImpl typeAnnotation = new TypeAnnotationImpl();
		return typeAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityContainer createEntityContainer() {
		EntityContainerImpl entityContainer = new EntityContainerImpl();
		return entityContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumType createEnumType() {
		EnumTypeImpl enumType = new EnumTypeImpl();
		return enumType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumMember createEnumMember() {
		EnumMemberImpl enumMember = new EnumMemberImpl();
		return enumMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Documentation createDocumentation() {
		DocumentationImpl documentation = new DocumentationImpl();
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDMXSet createEDMXSet() {
		EDMXSetImpl edmxSet = new EDMXSetImpl();
		return edmxSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityTypeUsage createEntityTypeUsage() {
		EntityTypeUsageImpl entityTypeUsage = new EntityTypeUsageImpl();
		return entityTypeUsage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeUsage createComplexTypeUsage() {
		ComplexTypeUsageImpl complexTypeUsage = new ComplexTypeUsageImpl();
		return complexTypeUsage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumTypeUsage createEnumTypeUsage() {
		EnumTypeUsageImpl enumTypeUsage = new EnumTypeUsageImpl();
		return enumTypeUsage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleTypeUsage createSimpleTypeUsage() {
		SimpleTypeUsageImpl simpleTypeUsage = new SimpleTypeUsageImpl();
		return simpleTypeUsage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDMXReference createEDMXReference() {
		EDMXReferenceImpl edmxReference = new EDMXReferenceImpl();
		return edmxReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDMXAnnotationsReference createEDMXAnnotationsReference() {
		EDMXAnnotationsReferenceImpl edmxAnnotationsReference = new EDMXAnnotationsReferenceImpl();
		return edmxAnnotationsReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Schema createSchema() {
		SchemaImpl schema = new SchemaImpl();
		return schema;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataService createDataService() {
		DataServiceImpl dataService = new DataServiceImpl();
		return dataService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IncludeRestriction createIncludeRestriction() {
		IncludeRestrictionImpl includeRestriction = new IncludeRestrictionImpl();
		return includeRestriction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathValue createPathValue() {
		PathValueImpl pathValue = new PathValueImpl();
		return pathValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Using createUsing() {
		UsingImpl using = new UsingImpl();
		return using;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordValue createRecordValue() {
		RecordValueImpl recordValue = new RecordValueImpl();
		return recordValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnEntityTypeUsage createReturnEntityTypeUsage() {
		ReturnEntityTypeUsageImpl returnEntityTypeUsage = new ReturnEntityTypeUsageImpl();
		return returnEntityTypeUsage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Property, Property> createPropertyMapping() {
		PropertyMappingImpl propertyMapping = new PropertyMappingImpl();
		return propertyMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueCollection createValueCollection() {
		ValueCollectionImpl valueCollection = new ValueCollectionImpl();
		return valueCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Property, AnnotationValue> createPropertyToValueMap() {
		PropertyToValueMapImpl propertyToValueMap = new PropertyToValueMapImpl();
		return propertyToValueMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanValue createBooleanValue() {
		BooleanValueImpl booleanValue = new BooleanValueImpl();
		return booleanValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringValue createStringValue() {
		StringValueImpl stringValue = new StringValueImpl();
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryValue createBinaryValue() {
		BinaryValueImpl binaryValue = new BinaryValueImpl();
		return binaryValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimeValue createDateTimeValue() {
		DateTimeValueImpl dateTimeValue = new DateTimeValueImpl();
		return dateTimeValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecimalValue createDecimalValue() {
		DecimalValueImpl decimalValue = new DecimalValueImpl();
		return decimalValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GuidValue createGuidValue() {
		GuidValueImpl guidValue = new GuidValueImpl();
		return guidValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeValue createTimeValue() {
		TimeValueImpl timeValue = new TimeValueImpl();
		return timeValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleValue createSingleValue() {
		SingleValueImpl singleValue = new SingleValueImpl();
		return singleValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DoubleValue createDoubleValue() {
		DoubleValueImpl doubleValue = new DoubleValueImpl();
		return doubleValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SByteValue createSByteValue() {
		SByteValueImpl sByteValue = new SByteValueImpl();
		return sByteValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Int16Value createInt16Value() {
		Int16ValueImpl int16Value = new Int16ValueImpl();
		return int16Value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Int32Value createInt32Value() {
		Int32ValueImpl int32Value = new Int32ValueImpl();
		return int32Value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Int64Value createInt64Value() {
		Int64ValueImpl int64Value = new Int64ValueImpl();
		return int64Value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimeOffsetValue createDateTimeOffsetValue() {
		DateTimeOffsetValueImpl dateTimeOffsetValue = new DateTimeOffsetValueImpl();
		return dateTimeOffsetValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ByteValue createByteValue() {
		ByteValueImpl byteValue = new ByteValueImpl();
		return byteValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumValue createEnumValue() {
		EnumValueImpl enumValue = new EnumValueImpl();
		return enumValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDMTypes createEDMTypesFromString(EDataType eDataType, String initialValue) {
		EDMTypes result = EDMTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException(Messages.ODATAFACTORYIMP_VALUE + initialValue + Messages.ODATAFACTORYIMP_INVALID_ENUM + eDataType.getName() + "'");  //$NON-NLS-1$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEDMTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multiplicity createMultiplicityFromString(EDataType eDataType, String initialValue) {
		Multiplicity result = Multiplicity.get(initialValue);
		if (result == null) throw new IllegalArgumentException(Messages.ODATAFACTORYIMP_VALUE + initialValue + Messages.ODATAFACTORYIMP_INVALID_ENUM + eDataType.getName() + "'"); //$NON-NLS-1$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMultiplicityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeTermTargets createTypeTermTargetsFromString(EDataType eDataType, String initialValue) {
		TypeTermTargets result = TypeTermTargets.get(initialValue);
		if (result == null) throw new IllegalArgumentException(Messages.ODATAFACTORYIMP_VALUE + initialValue + Messages.ODATAFACTORYIMP_INVALID_ENUM + eDataType.getName() + "'"); //$NON-NLS-1$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTypeTermTargetsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueTermTargets createValueTermTargetsFromString(EDataType eDataType, String initialValue) {
		ValueTermTargets result = ValueTermTargets.get(initialValue);
		if (result == null) throw new IllegalArgumentException(Messages.ODATAFACTORYIMP_VALUE + initialValue + Messages.ODATAFACTORYIMP_INVALID_ENUM + eDataType.getName() + "'"); //$NON-NLS-1$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertValueTermTargetsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SchemaClassifier createSchemaClassifierFromString(EDataType eDataType, String initialValue) {
		SchemaClassifier result = SchemaClassifier.get(initialValue);
		if (result == null) throw new IllegalArgumentException(Messages.ODATAFACTORYIMP_VALUE + initialValue + Messages.ODATAFACTORYIMP_INVALID_ENUM + eDataType.getName() + "'"); //$NON-NLS-1$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSchemaClassifierToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public UUID createGuidValueTypeFromString(EDataType eDataType, String initialValue) {
		return UUID.fromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertGuidValueTypeToString(EDataType eDataType, Object instanceValue) {
		return ((UUID) instanceValue).toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public DateTimeOffset createDateTimeOffsetValueTypeFromString(EDataType eDataType, String initialValue) {
		return DateTimeOffset.fromXMLFormat(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertDateTimeOffsetValueTypeToString(EDataType eDataType, Object instanceValue) {
		return ((DateTimeOffset)instanceValue).toXMLFormat();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public DateTime createDateTimeFromString(EDataType eDataType, String initialValue) {
		return DateTime.fromXMLFormat(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertDateTimeToString(EDataType eDataType, Object instanceValue) {
		return ((DateTime)instanceValue).toXMLFormat();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OdataPackage getOdataPackage() {
		return (OdataPackage)getEPackage();
	}	

} //OdataFactoryImpl
