/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXAnnotationsReference;
import org.eclipse.ogee.model.odata.IncludeRestriction;
import org.eclipse.ogee.model.odata.OdataPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EDMX Annotations Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXAnnotationsReferenceImpl#getIncludeRestrictions <em>Include Restrictions</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXAnnotationsReferenceImpl#getReferencedEDMX <em>Referenced EDMX</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EDMXAnnotationsReferenceImpl extends EObjectImpl implements EDMXAnnotationsReference {
	/**
	 * The cached value of the '{@link #getIncludeRestrictions() <em>Include Restrictions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludeRestrictions()
	 * @generated
	 * @ordered
	 */
	protected EList<IncludeRestriction> includeRestrictions;

	/**
	 * The cached value of the '{@link #getReferencedEDMX() <em>Referenced EDMX</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedEDMX()
	 * @generated
	 * @ordered
	 */
	protected EDMX referencedEDMX;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EDMXAnnotationsReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.EDMX_ANNOTATIONS_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IncludeRestriction> getIncludeRestrictions() {
		if (includeRestrictions == null) {
			includeRestrictions = new EObjectContainmentEList<IncludeRestriction>(IncludeRestriction.class, this, OdataPackage.EDMX_ANNOTATIONS_REFERENCE__INCLUDE_RESTRICTIONS);
		}
		return includeRestrictions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDMX getReferencedEDMX() {
		if (referencedEDMX != null && referencedEDMX.eIsProxy()) {
			InternalEObject oldReferencedEDMX = (InternalEObject)referencedEDMX;
			referencedEDMX = (EDMX)eResolveProxy(oldReferencedEDMX);
			if (referencedEDMX != oldReferencedEDMX) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.EDMX_ANNOTATIONS_REFERENCE__REFERENCED_EDMX, oldReferencedEDMX, referencedEDMX));
			}
		}
		return referencedEDMX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDMX basicGetReferencedEDMX() {
		return referencedEDMX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencedEDMX(EDMX newReferencedEDMX) {
		EDMX oldReferencedEDMX = referencedEDMX;
		referencedEDMX = newReferencedEDMX;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.EDMX_ANNOTATIONS_REFERENCE__REFERENCED_EDMX, oldReferencedEDMX, referencedEDMX));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE__INCLUDE_RESTRICTIONS:
				return ((InternalEList<?>)getIncludeRestrictions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE__INCLUDE_RESTRICTIONS:
				return getIncludeRestrictions();
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE__REFERENCED_EDMX:
				if (resolve) return getReferencedEDMX();
				return basicGetReferencedEDMX();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE__INCLUDE_RESTRICTIONS:
				getIncludeRestrictions().clear();
				getIncludeRestrictions().addAll((Collection<? extends IncludeRestriction>)newValue);
				return;
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE__REFERENCED_EDMX:
				setReferencedEDMX((EDMX)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE__INCLUDE_RESTRICTIONS:
				getIncludeRestrictions().clear();
				return;
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE__REFERENCED_EDMX:
				setReferencedEDMX((EDMX)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE__INCLUDE_RESTRICTIONS:
				return includeRestrictions != null && !includeRestrictions.isEmpty();
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE__REFERENCED_EDMX:
				return referencedEDMX != null;
		}
		return super.eIsSet(featureID);
	}

} //EDMXAnnotationsReferenceImpl
