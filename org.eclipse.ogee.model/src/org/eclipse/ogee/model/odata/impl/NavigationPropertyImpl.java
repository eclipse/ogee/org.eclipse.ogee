/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.IDocumentable;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Role;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Navigation Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl#getRelationship <em>Relationship</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl#getFromRole <em>From Role</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl#getToRole <em>To Role</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl#isContainsTarget <em>Contains Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NavigationPropertyImpl extends EObjectImpl implements NavigationProperty {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected Documentation documentation;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRelationship() <em>Relationship</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationship()
	 * @generated
	 * @ordered
	 */
	protected Association relationship;

	/**
	 * The cached value of the '{@link #getFromRole() <em>From Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromRole()
	 * @generated
	 * @ordered
	 */
	protected Role fromRole;

	/**
	 * The cached value of the '{@link #getToRole() <em>To Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToRole()
	 * @generated
	 * @ordered
	 */
	protected Role toRole;

	/**
	 * The default value of the '{@link #isContainsTarget() <em>Contains Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isContainsTarget()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONTAINS_TARGET_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isContainsTarget() <em>Contains Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isContainsTarget()
	 * @generated
	 * @ordered
	 */
	protected boolean containsTarget = CONTAINS_TARGET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NavigationPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.NAVIGATION_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectWithInverseResolvingEList<Annotation>(Annotation.class, this, OdataPackage.NAVIGATION_PROPERTY__ANNOTATIONS, OdataPackage.ANNOTATION__TARGET);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDocumentation(Documentation newDocumentation, NotificationChain msgs) {
		Documentation oldDocumentation = documentation;
		documentation = newDocumentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION, oldDocumentation, newDocumentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentation(Documentation newDocumentation) {
		if (newDocumentation != documentation) {
			NotificationChain msgs = null;
			if (documentation != null)
				msgs = ((InternalEObject)documentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION, null, msgs);
			if (newDocumentation != null)
				msgs = ((InternalEObject)newDocumentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION, null, msgs);
			msgs = basicSetDocumentation(newDocumentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION, newDocumentation, newDocumentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.NAVIGATION_PROPERTY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association getRelationship() {
		if (relationship != null && relationship.eIsProxy()) {
			InternalEObject oldRelationship = (InternalEObject)relationship;
			relationship = (Association)eResolveProxy(oldRelationship);
			if (relationship != oldRelationship) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.NAVIGATION_PROPERTY__RELATIONSHIP, oldRelationship, relationship));
			}
		}
		return relationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association basicGetRelationship() {
		return relationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelationship(Association newRelationship) {
		Association oldRelationship = relationship;
		relationship = newRelationship;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.NAVIGATION_PROPERTY__RELATIONSHIP, oldRelationship, relationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role getFromRole() {
		if (fromRole != null && fromRole.eIsProxy()) {
			InternalEObject oldFromRole = (InternalEObject)fromRole;
			fromRole = (Role)eResolveProxy(oldFromRole);
			if (fromRole != oldFromRole) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.NAVIGATION_PROPERTY__FROM_ROLE, oldFromRole, fromRole));
			}
		}
		return fromRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role basicGetFromRole() {
		return fromRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromRole(Role newFromRole) {
		Role oldFromRole = fromRole;
		fromRole = newFromRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.NAVIGATION_PROPERTY__FROM_ROLE, oldFromRole, fromRole));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role getToRole() {
		if (toRole != null && toRole.eIsProxy()) {
			InternalEObject oldToRole = (InternalEObject)toRole;
			toRole = (Role)eResolveProxy(oldToRole);
			if (toRole != oldToRole) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.NAVIGATION_PROPERTY__TO_ROLE, oldToRole, toRole));
			}
		}
		return toRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role basicGetToRole() {
		return toRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToRole(Role newToRole) {
		Role oldToRole = toRole;
		toRole = newToRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.NAVIGATION_PROPERTY__TO_ROLE, oldToRole, toRole));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isContainsTarget() {
		return containsTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainsTarget(boolean newContainsTarget) {
		boolean oldContainsTarget = containsTarget;
		containsTarget = newContainsTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.NAVIGATION_PROPERTY__CONTAINS_TARGET, oldContainsTarget, containsTarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.NAVIGATION_PROPERTY__ANNOTATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAnnotations()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.NAVIGATION_PROPERTY__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION:
				return basicSetDocumentation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.NAVIGATION_PROPERTY__ANNOTATIONS:
				return getAnnotations();
			case OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION:
				return getDocumentation();
			case OdataPackage.NAVIGATION_PROPERTY__NAME:
				return getName();
			case OdataPackage.NAVIGATION_PROPERTY__RELATIONSHIP:
				if (resolve) return getRelationship();
				return basicGetRelationship();
			case OdataPackage.NAVIGATION_PROPERTY__FROM_ROLE:
				if (resolve) return getFromRole();
				return basicGetFromRole();
			case OdataPackage.NAVIGATION_PROPERTY__TO_ROLE:
				if (resolve) return getToRole();
				return basicGetToRole();
			case OdataPackage.NAVIGATION_PROPERTY__CONTAINS_TARGET:
				return isContainsTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.NAVIGATION_PROPERTY__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION:
				setDocumentation((Documentation)newValue);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__NAME:
				setName((String)newValue);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__RELATIONSHIP:
				setRelationship((Association)newValue);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__FROM_ROLE:
				setFromRole((Role)newValue);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__TO_ROLE:
				setToRole((Role)newValue);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__CONTAINS_TARGET:
				setContainsTarget((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.NAVIGATION_PROPERTY__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION:
				setDocumentation((Documentation)null);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__RELATIONSHIP:
				setRelationship((Association)null);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__FROM_ROLE:
				setFromRole((Role)null);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__TO_ROLE:
				setToRole((Role)null);
				return;
			case OdataPackage.NAVIGATION_PROPERTY__CONTAINS_TARGET:
				setContainsTarget(CONTAINS_TARGET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.NAVIGATION_PROPERTY__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION:
				return documentation != null;
			case OdataPackage.NAVIGATION_PROPERTY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case OdataPackage.NAVIGATION_PROPERTY__RELATIONSHIP:
				return relationship != null;
			case OdataPackage.NAVIGATION_PROPERTY__FROM_ROLE:
				return fromRole != null;
			case OdataPackage.NAVIGATION_PROPERTY__TO_ROLE:
				return toRole != null;
			case OdataPackage.NAVIGATION_PROPERTY__CONTAINS_TARGET:
				return containsTarget != CONTAINS_TARGET_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IDocumentable.class) {
			switch (derivedFeatureID) {
				case OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION: return OdataPackage.IDOCUMENTABLE__DOCUMENTATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IDocumentable.class) {
			switch (baseFeatureID) {
				case OdataPackage.IDOCUMENTABLE__DOCUMENTATION: return OdataPackage.NAVIGATION_PROPERTY__DOCUMENTATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", containsTarget: "); //$NON-NLS-1$
		result.append(containsTarget);
		result.append(')');
		return result.toString();
	}

} //NavigationPropertyImpl
