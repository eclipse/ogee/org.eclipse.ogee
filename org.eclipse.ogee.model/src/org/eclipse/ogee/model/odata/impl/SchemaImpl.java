/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.TypeAnnotation;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueTerm;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Schema</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getNamespace <em>Namespace</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getContainers <em>Containers</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getEntityTypes <em>Entity Types</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getAssociations <em>Associations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getEnumTypes <em>Enum Types</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getComplexTypes <em>Complex Types</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getUsings <em>Usings</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getValueTerms <em>Value Terms</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getValueAnnotations <em>Value Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getTypeAnnotations <em>Type Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getAlias <em>Alias</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getClassifiers <em>Classifiers</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.SchemaImpl#getDataServices <em>Data Services</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SchemaImpl extends EObjectImpl implements Schema {
	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected Documentation documentation;

	/**
	 * The default value of the '{@link #getNamespace() <em>Namespace</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamespace()
	 * @generated
	 * @ordered
	 */
	protected static final String NAMESPACE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNamespace() <em>Namespace</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamespace()
	 * @generated
	 * @ordered
	 */
	protected String namespace = NAMESPACE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getContainers() <em>Containers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainers()
	 * @generated
	 * @ordered
	 */
	protected EList<EntityContainer> containers;

	/**
	 * The cached value of the '{@link #getEntityTypes() <em>Entity Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntityTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<EntityType> entityTypes;

	/**
	 * The cached value of the '{@link #getAssociations() <em>Associations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociations()
	 * @generated
	 * @ordered
	 */
	protected EList<Association> associations;

	/**
	 * The cached value of the '{@link #getEnumTypes() <em>Enum Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<EnumType> enumTypes;

	/**
	 * The cached value of the '{@link #getComplexTypes() <em>Complex Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComplexTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<ComplexType> complexTypes;

	/**
	 * The cached value of the '{@link #getUsings() <em>Usings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsings()
	 * @generated
	 * @ordered
	 */
	protected EList<Using> usings;

	/**
	 * The cached value of the '{@link #getValueTerms() <em>Value Terms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueTerms()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueTerm> valueTerms;

	/**
	 * The cached value of the '{@link #getValueAnnotations() <em>Value Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueAnnotation> valueAnnotations;

	/**
	 * The cached value of the '{@link #getTypeAnnotations() <em>Type Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<TypeAnnotation> typeAnnotations;

	/**
	 * The default value of the '{@link #getAlias() <em>Alias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlias()
	 * @generated
	 * @ordered
	 */
	protected static final String ALIAS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAlias() <em>Alias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlias()
	 * @generated
	 * @ordered
	 */
	protected String alias = ALIAS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getClassifiers() <em>Classifiers</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<SchemaClassifier> classifiers;

	/**
	 * The cached value of the '{@link #getDataServices() <em>Data Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataServices()
	 * @generated
	 * @ordered
	 */
	protected EList<DataService> dataServices;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SchemaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.SCHEMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDocumentation(Documentation newDocumentation, NotificationChain msgs) {
		Documentation oldDocumentation = documentation;
		documentation = newDocumentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.SCHEMA__DOCUMENTATION, oldDocumentation, newDocumentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentation(Documentation newDocumentation) {
		if (newDocumentation != documentation) {
			NotificationChain msgs = null;
			if (documentation != null)
				msgs = ((InternalEObject)documentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.SCHEMA__DOCUMENTATION, null, msgs);
			if (newDocumentation != null)
				msgs = ((InternalEObject)newDocumentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.SCHEMA__DOCUMENTATION, null, msgs);
			msgs = basicSetDocumentation(newDocumentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.SCHEMA__DOCUMENTATION, newDocumentation, newDocumentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamespace(String newNamespace) {
		String oldNamespace = namespace;
		namespace = newNamespace;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.SCHEMA__NAMESPACE, oldNamespace, namespace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntityContainer> getContainers() {
		if (containers == null) {
			containers = new EObjectContainmentEList<EntityContainer>(EntityContainer.class, this, OdataPackage.SCHEMA__CONTAINERS);
		}
		return containers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntityType> getEntityTypes() {
		if (entityTypes == null) {
			entityTypes = new EObjectContainmentEList<EntityType>(EntityType.class, this, OdataPackage.SCHEMA__ENTITY_TYPES);
		}
		return entityTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Association> getAssociations() {
		if (associations == null) {
			associations = new EObjectContainmentEList<Association>(Association.class, this, OdataPackage.SCHEMA__ASSOCIATIONS);
		}
		return associations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnumType> getEnumTypes() {
		if (enumTypes == null) {
			enumTypes = new EObjectContainmentEList<EnumType>(EnumType.class, this, OdataPackage.SCHEMA__ENUM_TYPES);
		}
		return enumTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexType> getComplexTypes() {
		if (complexTypes == null) {
			complexTypes = new EObjectContainmentEList<ComplexType>(ComplexType.class, this, OdataPackage.SCHEMA__COMPLEX_TYPES);
		}
		return complexTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Using> getUsings() {
		if (usings == null) {
			usings = new EObjectContainmentEList<Using>(Using.class, this, OdataPackage.SCHEMA__USINGS);
		}
		return usings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueTerm> getValueTerms() {
		if (valueTerms == null) {
			valueTerms = new EObjectContainmentEList<ValueTerm>(ValueTerm.class, this, OdataPackage.SCHEMA__VALUE_TERMS);
		}
		return valueTerms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueAnnotation> getValueAnnotations() {
		if (valueAnnotations == null) {
			valueAnnotations = new EObjectContainmentEList<ValueAnnotation>(ValueAnnotation.class, this, OdataPackage.SCHEMA__VALUE_ANNOTATIONS);
		}
		return valueAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeAnnotation> getTypeAnnotations() {
		if (typeAnnotations == null) {
			typeAnnotations = new EObjectContainmentEList<TypeAnnotation>(TypeAnnotation.class, this, OdataPackage.SCHEMA__TYPE_ANNOTATIONS);
		}
		return typeAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlias(String newAlias) {
		String oldAlias = alias;
		alias = newAlias;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.SCHEMA__ALIAS, oldAlias, alias));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SchemaClassifier> getClassifiers() {
		if (classifiers == null) {
			classifiers = new EDataTypeUniqueEList<SchemaClassifier>(SchemaClassifier.class, this, OdataPackage.SCHEMA__CLASSIFIERS);
		}
		return classifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataService> getDataServices() {
		if (dataServices == null) {
			dataServices = new EObjectWithInverseResolvingEList.ManyInverse<DataService>(DataService.class, this, OdataPackage.SCHEMA__DATA_SERVICES, OdataPackage.DATA_SERVICE__SCHEMATA);
		}
		return dataServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.SCHEMA__DATA_SERVICES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDataServices()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.SCHEMA__DOCUMENTATION:
				return basicSetDocumentation(null, msgs);
			case OdataPackage.SCHEMA__CONTAINERS:
				return ((InternalEList<?>)getContainers()).basicRemove(otherEnd, msgs);
			case OdataPackage.SCHEMA__ENTITY_TYPES:
				return ((InternalEList<?>)getEntityTypes()).basicRemove(otherEnd, msgs);
			case OdataPackage.SCHEMA__ASSOCIATIONS:
				return ((InternalEList<?>)getAssociations()).basicRemove(otherEnd, msgs);
			case OdataPackage.SCHEMA__ENUM_TYPES:
				return ((InternalEList<?>)getEnumTypes()).basicRemove(otherEnd, msgs);
			case OdataPackage.SCHEMA__COMPLEX_TYPES:
				return ((InternalEList<?>)getComplexTypes()).basicRemove(otherEnd, msgs);
			case OdataPackage.SCHEMA__USINGS:
				return ((InternalEList<?>)getUsings()).basicRemove(otherEnd, msgs);
			case OdataPackage.SCHEMA__VALUE_TERMS:
				return ((InternalEList<?>)getValueTerms()).basicRemove(otherEnd, msgs);
			case OdataPackage.SCHEMA__VALUE_ANNOTATIONS:
				return ((InternalEList<?>)getValueAnnotations()).basicRemove(otherEnd, msgs);
			case OdataPackage.SCHEMA__TYPE_ANNOTATIONS:
				return ((InternalEList<?>)getTypeAnnotations()).basicRemove(otherEnd, msgs);
			case OdataPackage.SCHEMA__DATA_SERVICES:
				return ((InternalEList<?>)getDataServices()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.SCHEMA__DOCUMENTATION:
				return getDocumentation();
			case OdataPackage.SCHEMA__NAMESPACE:
				return getNamespace();
			case OdataPackage.SCHEMA__CONTAINERS:
				return getContainers();
			case OdataPackage.SCHEMA__ENTITY_TYPES:
				return getEntityTypes();
			case OdataPackage.SCHEMA__ASSOCIATIONS:
				return getAssociations();
			case OdataPackage.SCHEMA__ENUM_TYPES:
				return getEnumTypes();
			case OdataPackage.SCHEMA__COMPLEX_TYPES:
				return getComplexTypes();
			case OdataPackage.SCHEMA__USINGS:
				return getUsings();
			case OdataPackage.SCHEMA__VALUE_TERMS:
				return getValueTerms();
			case OdataPackage.SCHEMA__VALUE_ANNOTATIONS:
				return getValueAnnotations();
			case OdataPackage.SCHEMA__TYPE_ANNOTATIONS:
				return getTypeAnnotations();
			case OdataPackage.SCHEMA__ALIAS:
				return getAlias();
			case OdataPackage.SCHEMA__CLASSIFIERS:
				return getClassifiers();
			case OdataPackage.SCHEMA__DATA_SERVICES:
				return getDataServices();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.SCHEMA__DOCUMENTATION:
				setDocumentation((Documentation)newValue);
				return;
			case OdataPackage.SCHEMA__NAMESPACE:
				setNamespace((String)newValue);
				return;
			case OdataPackage.SCHEMA__CONTAINERS:
				getContainers().clear();
				getContainers().addAll((Collection<? extends EntityContainer>)newValue);
				return;
			case OdataPackage.SCHEMA__ENTITY_TYPES:
				getEntityTypes().clear();
				getEntityTypes().addAll((Collection<? extends EntityType>)newValue);
				return;
			case OdataPackage.SCHEMA__ASSOCIATIONS:
				getAssociations().clear();
				getAssociations().addAll((Collection<? extends Association>)newValue);
				return;
			case OdataPackage.SCHEMA__ENUM_TYPES:
				getEnumTypes().clear();
				getEnumTypes().addAll((Collection<? extends EnumType>)newValue);
				return;
			case OdataPackage.SCHEMA__COMPLEX_TYPES:
				getComplexTypes().clear();
				getComplexTypes().addAll((Collection<? extends ComplexType>)newValue);
				return;
			case OdataPackage.SCHEMA__USINGS:
				getUsings().clear();
				getUsings().addAll((Collection<? extends Using>)newValue);
				return;
			case OdataPackage.SCHEMA__VALUE_TERMS:
				getValueTerms().clear();
				getValueTerms().addAll((Collection<? extends ValueTerm>)newValue);
				return;
			case OdataPackage.SCHEMA__VALUE_ANNOTATIONS:
				getValueAnnotations().clear();
				getValueAnnotations().addAll((Collection<? extends ValueAnnotation>)newValue);
				return;
			case OdataPackage.SCHEMA__TYPE_ANNOTATIONS:
				getTypeAnnotations().clear();
				getTypeAnnotations().addAll((Collection<? extends TypeAnnotation>)newValue);
				return;
			case OdataPackage.SCHEMA__ALIAS:
				setAlias((String)newValue);
				return;
			case OdataPackage.SCHEMA__CLASSIFIERS:
				getClassifiers().clear();
				getClassifiers().addAll((Collection<? extends SchemaClassifier>)newValue);
				return;
			case OdataPackage.SCHEMA__DATA_SERVICES:
				getDataServices().clear();
				getDataServices().addAll((Collection<? extends DataService>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.SCHEMA__DOCUMENTATION:
				setDocumentation((Documentation)null);
				return;
			case OdataPackage.SCHEMA__NAMESPACE:
				setNamespace(NAMESPACE_EDEFAULT);
				return;
			case OdataPackage.SCHEMA__CONTAINERS:
				getContainers().clear();
				return;
			case OdataPackage.SCHEMA__ENTITY_TYPES:
				getEntityTypes().clear();
				return;
			case OdataPackage.SCHEMA__ASSOCIATIONS:
				getAssociations().clear();
				return;
			case OdataPackage.SCHEMA__ENUM_TYPES:
				getEnumTypes().clear();
				return;
			case OdataPackage.SCHEMA__COMPLEX_TYPES:
				getComplexTypes().clear();
				return;
			case OdataPackage.SCHEMA__USINGS:
				getUsings().clear();
				return;
			case OdataPackage.SCHEMA__VALUE_TERMS:
				getValueTerms().clear();
				return;
			case OdataPackage.SCHEMA__VALUE_ANNOTATIONS:
				getValueAnnotations().clear();
				return;
			case OdataPackage.SCHEMA__TYPE_ANNOTATIONS:
				getTypeAnnotations().clear();
				return;
			case OdataPackage.SCHEMA__ALIAS:
				setAlias(ALIAS_EDEFAULT);
				return;
			case OdataPackage.SCHEMA__CLASSIFIERS:
				getClassifiers().clear();
				return;
			case OdataPackage.SCHEMA__DATA_SERVICES:
				getDataServices().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.SCHEMA__DOCUMENTATION:
				return documentation != null;
			case OdataPackage.SCHEMA__NAMESPACE:
				return NAMESPACE_EDEFAULT == null ? namespace != null : !NAMESPACE_EDEFAULT.equals(namespace);
			case OdataPackage.SCHEMA__CONTAINERS:
				return containers != null && !containers.isEmpty();
			case OdataPackage.SCHEMA__ENTITY_TYPES:
				return entityTypes != null && !entityTypes.isEmpty();
			case OdataPackage.SCHEMA__ASSOCIATIONS:
				return associations != null && !associations.isEmpty();
			case OdataPackage.SCHEMA__ENUM_TYPES:
				return enumTypes != null && !enumTypes.isEmpty();
			case OdataPackage.SCHEMA__COMPLEX_TYPES:
				return complexTypes != null && !complexTypes.isEmpty();
			case OdataPackage.SCHEMA__USINGS:
				return usings != null && !usings.isEmpty();
			case OdataPackage.SCHEMA__VALUE_TERMS:
				return valueTerms != null && !valueTerms.isEmpty();
			case OdataPackage.SCHEMA__VALUE_ANNOTATIONS:
				return valueAnnotations != null && !valueAnnotations.isEmpty();
			case OdataPackage.SCHEMA__TYPE_ANNOTATIONS:
				return typeAnnotations != null && !typeAnnotations.isEmpty();
			case OdataPackage.SCHEMA__ALIAS:
				return ALIAS_EDEFAULT == null ? alias != null : !ALIAS_EDEFAULT.equals(alias);
			case OdataPackage.SCHEMA__CLASSIFIERS:
				return classifiers != null && !classifiers.isEmpty();
			case OdataPackage.SCHEMA__DATA_SERVICES:
				return dataServices != null && !dataServices.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (namespace: "); //$NON-NLS-1$
		result.append(namespace);
		result.append(", alias: "); //$NON-NLS-1$
		result.append(alias);
		result.append(", classifiers: "); //$NON-NLS-1$
		result.append(classifiers);
		result.append(')');
		return result.toString();
	}

} //SchemaImpl
