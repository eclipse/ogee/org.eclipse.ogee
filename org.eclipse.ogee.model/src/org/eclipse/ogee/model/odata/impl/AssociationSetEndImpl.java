/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Role;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association Set End</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.AssociationSetEndImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.AssociationSetEndImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.AssociationSetEndImpl#getRole <em>Role</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.AssociationSetEndImpl#getEntitySet <em>Entity Set</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssociationSetEndImpl extends EObjectImpl implements AssociationSetEnd {
	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected Documentation documentation;

	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The cached value of the '{@link #getRole() <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected Role role;

	/**
	 * The cached value of the '{@link #getEntitySet() <em>Entity Set</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntitySet()
	 * @generated
	 * @ordered
	 */
	protected EntitySet entitySet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationSetEndImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.ASSOCIATION_SET_END;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDocumentation(Documentation newDocumentation, NotificationChain msgs) {
		Documentation oldDocumentation = documentation;
		documentation = newDocumentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.ASSOCIATION_SET_END__DOCUMENTATION, oldDocumentation, newDocumentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentation(Documentation newDocumentation) {
		if (newDocumentation != documentation) {
			NotificationChain msgs = null;
			if (documentation != null)
				msgs = ((InternalEObject)documentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.ASSOCIATION_SET_END__DOCUMENTATION, null, msgs);
			if (newDocumentation != null)
				msgs = ((InternalEObject)newDocumentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.ASSOCIATION_SET_END__DOCUMENTATION, null, msgs);
			msgs = basicSetDocumentation(newDocumentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ASSOCIATION_SET_END__DOCUMENTATION, newDocumentation, newDocumentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectWithInverseResolvingEList<Annotation>(Annotation.class, this, OdataPackage.ASSOCIATION_SET_END__ANNOTATIONS, OdataPackage.ANNOTATION__TARGET);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role getRole() {
		if (role != null && role.eIsProxy()) {
			InternalEObject oldRole = (InternalEObject)role;
			role = (Role)eResolveProxy(oldRole);
			if (role != oldRole) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.ASSOCIATION_SET_END__ROLE, oldRole, role));
			}
		}
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role basicGetRole() {
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole(Role newRole) {
		Role oldRole = role;
		role = newRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ASSOCIATION_SET_END__ROLE, oldRole, role));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntitySet getEntitySet() {
		if (entitySet != null && entitySet.eIsProxy()) {
			InternalEObject oldEntitySet = (InternalEObject)entitySet;
			entitySet = (EntitySet)eResolveProxy(oldEntitySet);
			if (entitySet != oldEntitySet) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.ASSOCIATION_SET_END__ENTITY_SET, oldEntitySet, entitySet));
			}
		}
		return entitySet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntitySet basicGetEntitySet() {
		return entitySet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntitySet(EntitySet newEntitySet) {
		EntitySet oldEntitySet = entitySet;
		entitySet = newEntitySet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ASSOCIATION_SET_END__ENTITY_SET, oldEntitySet, entitySet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION_SET_END__ANNOTATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAnnotations()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION_SET_END__DOCUMENTATION:
				return basicSetDocumentation(null, msgs);
			case OdataPackage.ASSOCIATION_SET_END__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION_SET_END__DOCUMENTATION:
				return getDocumentation();
			case OdataPackage.ASSOCIATION_SET_END__ANNOTATIONS:
				return getAnnotations();
			case OdataPackage.ASSOCIATION_SET_END__ROLE:
				if (resolve) return getRole();
				return basicGetRole();
			case OdataPackage.ASSOCIATION_SET_END__ENTITY_SET:
				if (resolve) return getEntitySet();
				return basicGetEntitySet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION_SET_END__DOCUMENTATION:
				setDocumentation((Documentation)newValue);
				return;
			case OdataPackage.ASSOCIATION_SET_END__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case OdataPackage.ASSOCIATION_SET_END__ROLE:
				setRole((Role)newValue);
				return;
			case OdataPackage.ASSOCIATION_SET_END__ENTITY_SET:
				setEntitySet((EntitySet)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION_SET_END__DOCUMENTATION:
				setDocumentation((Documentation)null);
				return;
			case OdataPackage.ASSOCIATION_SET_END__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case OdataPackage.ASSOCIATION_SET_END__ROLE:
				setRole((Role)null);
				return;
			case OdataPackage.ASSOCIATION_SET_END__ENTITY_SET:
				setEntitySet((EntitySet)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.ASSOCIATION_SET_END__DOCUMENTATION:
				return documentation != null;
			case OdataPackage.ASSOCIATION_SET_END__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case OdataPackage.ASSOCIATION_SET_END__ROLE:
				return role != null;
			case OdataPackage.ASSOCIATION_SET_END__ENTITY_SET:
				return entitySet != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IAnnotationTarget.class) {
			switch (derivedFeatureID) {
				case OdataPackage.ASSOCIATION_SET_END__ANNOTATIONS: return OdataPackage.IANNOTATION_TARGET__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IAnnotationTarget.class) {
			switch (baseFeatureID) {
				case OdataPackage.IANNOTATION_TARGET__ANNOTATIONS: return OdataPackage.ASSOCIATION_SET_END__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //AssociationSetEndImpl
