/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.IDocumentable;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Role;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Referential Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.ReferentialConstraintImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.ReferentialConstraintImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.ReferentialConstraintImpl#getPrincipal <em>Principal</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.ReferentialConstraintImpl#getDependent <em>Dependent</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.ReferentialConstraintImpl#getKeyMappings <em>Key Mappings</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReferentialConstraintImpl extends EObjectImpl implements ReferentialConstraint {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected Documentation documentation;

	/**
	 * The cached value of the '{@link #getPrincipal() <em>Principal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrincipal()
	 * @generated
	 * @ordered
	 */
	protected Role principal;

	/**
	 * The cached value of the '{@link #getDependent() <em>Dependent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependent()
	 * @generated
	 * @ordered
	 */
	protected Role dependent;

	/**
	 * The cached value of the '{@link #getKeyMappings() <em>Key Mappings</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyMappings()
	 * @generated
	 * @ordered
	 */
	protected EMap<Property, Property> keyMappings;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferentialConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.REFERENTIAL_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectWithInverseResolvingEList<Annotation>(Annotation.class, this, OdataPackage.REFERENTIAL_CONSTRAINT__ANNOTATIONS, OdataPackage.ANNOTATION__TARGET);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDocumentation(Documentation newDocumentation, NotificationChain msgs) {
		Documentation oldDocumentation = documentation;
		documentation = newDocumentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION, oldDocumentation, newDocumentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentation(Documentation newDocumentation) {
		if (newDocumentation != documentation) {
			NotificationChain msgs = null;
			if (documentation != null)
				msgs = ((InternalEObject)documentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION, null, msgs);
			if (newDocumentation != null)
				msgs = ((InternalEObject)newDocumentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION, null, msgs);
			msgs = basicSetDocumentation(newDocumentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION, newDocumentation, newDocumentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role getPrincipal() {
		if (principal != null && principal.eIsProxy()) {
			InternalEObject oldPrincipal = (InternalEObject)principal;
			principal = (Role)eResolveProxy(oldPrincipal);
			if (principal != oldPrincipal) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.REFERENTIAL_CONSTRAINT__PRINCIPAL, oldPrincipal, principal));
			}
		}
		return principal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role basicGetPrincipal() {
		return principal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrincipal(Role newPrincipal) {
		Role oldPrincipal = principal;
		principal = newPrincipal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.REFERENTIAL_CONSTRAINT__PRINCIPAL, oldPrincipal, principal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role getDependent() {
		if (dependent != null && dependent.eIsProxy()) {
			InternalEObject oldDependent = (InternalEObject)dependent;
			dependent = (Role)eResolveProxy(oldDependent);
			if (dependent != oldDependent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.REFERENTIAL_CONSTRAINT__DEPENDENT, oldDependent, dependent));
			}
		}
		return dependent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role basicGetDependent() {
		return dependent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependent(Role newDependent) {
		Role oldDependent = dependent;
		dependent = newDependent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.REFERENTIAL_CONSTRAINT__DEPENDENT, oldDependent, dependent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Property, Property> getKeyMappings() {
		if (keyMappings == null) {
			keyMappings = new EcoreEMap<Property,Property>(OdataPackage.Literals.PROPERTY_MAPPING, PropertyMappingImpl.class, this, OdataPackage.REFERENTIAL_CONSTRAINT__KEY_MAPPINGS);
		}
		return keyMappings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.REFERENTIAL_CONSTRAINT__ANNOTATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAnnotations()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.REFERENTIAL_CONSTRAINT__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION:
				return basicSetDocumentation(null, msgs);
			case OdataPackage.REFERENTIAL_CONSTRAINT__KEY_MAPPINGS:
				return ((InternalEList<?>)getKeyMappings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.REFERENTIAL_CONSTRAINT__ANNOTATIONS:
				return getAnnotations();
			case OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION:
				return getDocumentation();
			case OdataPackage.REFERENTIAL_CONSTRAINT__PRINCIPAL:
				if (resolve) return getPrincipal();
				return basicGetPrincipal();
			case OdataPackage.REFERENTIAL_CONSTRAINT__DEPENDENT:
				if (resolve) return getDependent();
				return basicGetDependent();
			case OdataPackage.REFERENTIAL_CONSTRAINT__KEY_MAPPINGS:
				if (coreType) return getKeyMappings();
				else return getKeyMappings().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.REFERENTIAL_CONSTRAINT__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION:
				setDocumentation((Documentation)newValue);
				return;
			case OdataPackage.REFERENTIAL_CONSTRAINT__PRINCIPAL:
				setPrincipal((Role)newValue);
				return;
			case OdataPackage.REFERENTIAL_CONSTRAINT__DEPENDENT:
				setDependent((Role)newValue);
				return;
			case OdataPackage.REFERENTIAL_CONSTRAINT__KEY_MAPPINGS:
				((EStructuralFeature.Setting)getKeyMappings()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.REFERENTIAL_CONSTRAINT__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION:
				setDocumentation((Documentation)null);
				return;
			case OdataPackage.REFERENTIAL_CONSTRAINT__PRINCIPAL:
				setPrincipal((Role)null);
				return;
			case OdataPackage.REFERENTIAL_CONSTRAINT__DEPENDENT:
				setDependent((Role)null);
				return;
			case OdataPackage.REFERENTIAL_CONSTRAINT__KEY_MAPPINGS:
				getKeyMappings().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.REFERENTIAL_CONSTRAINT__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION:
				return documentation != null;
			case OdataPackage.REFERENTIAL_CONSTRAINT__PRINCIPAL:
				return principal != null;
			case OdataPackage.REFERENTIAL_CONSTRAINT__DEPENDENT:
				return dependent != null;
			case OdataPackage.REFERENTIAL_CONSTRAINT__KEY_MAPPINGS:
				return keyMappings != null && !keyMappings.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IDocumentable.class) {
			switch (derivedFeatureID) {
				case OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION: return OdataPackage.IDOCUMENTABLE__DOCUMENTATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IDocumentable.class) {
			switch (baseFeatureID) {
				case OdataPackage.IDOCUMENTABLE__DOCUMENTATION: return OdataPackage.REFERENTIAL_CONSTRAINT__DOCUMENTATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ReferentialConstraintImpl
