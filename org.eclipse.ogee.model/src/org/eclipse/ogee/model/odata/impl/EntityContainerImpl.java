/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IDocumentable;
import org.eclipse.ogee.model.odata.OdataPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entity Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityContainerImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityContainerImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityContainerImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityContainerImpl#getEntitySets <em>Entity Sets</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityContainerImpl#isDefault <em>Default</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityContainerImpl#getAssociationSets <em>Association Sets</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityContainerImpl#getFunctionImports <em>Function Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EntityContainerImpl extends EObjectImpl implements EntityContainer {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected Documentation documentation;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEntitySets() <em>Entity Sets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntitySets()
	 * @generated
	 * @ordered
	 */
	protected EList<EntitySet> entitySets;

	/**
	 * The default value of the '{@link #isDefault() <em>Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDefault()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEFAULT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDefault() <em>Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDefault()
	 * @generated
	 * @ordered
	 */
	protected boolean default_ = DEFAULT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAssociationSets() <em>Association Sets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociationSets()
	 * @generated
	 * @ordered
	 */
	protected EList<AssociationSet> associationSets;

	/**
	 * The cached value of the '{@link #getFunctionImports() <em>Function Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionImports()
	 * @generated
	 * @ordered
	 */
	protected EList<FunctionImport> functionImports;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntityContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.ENTITY_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectWithInverseResolvingEList<Annotation>(Annotation.class, this, OdataPackage.ENTITY_CONTAINER__ANNOTATIONS, OdataPackage.ANNOTATION__TARGET);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDocumentation(Documentation newDocumentation, NotificationChain msgs) {
		Documentation oldDocumentation = documentation;
		documentation = newDocumentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_CONTAINER__DOCUMENTATION, oldDocumentation, newDocumentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentation(Documentation newDocumentation) {
		if (newDocumentation != documentation) {
			NotificationChain msgs = null;
			if (documentation != null)
				msgs = ((InternalEObject)documentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.ENTITY_CONTAINER__DOCUMENTATION, null, msgs);
			if (newDocumentation != null)
				msgs = ((InternalEObject)newDocumentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.ENTITY_CONTAINER__DOCUMENTATION, null, msgs);
			msgs = basicSetDocumentation(newDocumentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_CONTAINER__DOCUMENTATION, newDocumentation, newDocumentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_CONTAINER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntitySet> getEntitySets() {
		if (entitySets == null) {
			entitySets = new EObjectContainmentEList<EntitySet>(EntitySet.class, this, OdataPackage.ENTITY_CONTAINER__ENTITY_SETS);
		}
		return entitySets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDefault() {
		return default_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefault(boolean newDefault) {
		boolean oldDefault = default_;
		default_ = newDefault;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_CONTAINER__DEFAULT, oldDefault, default_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssociationSet> getAssociationSets() {
		if (associationSets == null) {
			associationSets = new EObjectContainmentEList<AssociationSet>(AssociationSet.class, this, OdataPackage.ENTITY_CONTAINER__ASSOCIATION_SETS);
		}
		return associationSets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionImport> getFunctionImports() {
		if (functionImports == null) {
			functionImports = new EObjectContainmentEList<FunctionImport>(FunctionImport.class, this, OdataPackage.ENTITY_CONTAINER__FUNCTION_IMPORTS);
		}
		return functionImports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.ENTITY_CONTAINER__ANNOTATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAnnotations()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.ENTITY_CONTAINER__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case OdataPackage.ENTITY_CONTAINER__DOCUMENTATION:
				return basicSetDocumentation(null, msgs);
			case OdataPackage.ENTITY_CONTAINER__ENTITY_SETS:
				return ((InternalEList<?>)getEntitySets()).basicRemove(otherEnd, msgs);
			case OdataPackage.ENTITY_CONTAINER__ASSOCIATION_SETS:
				return ((InternalEList<?>)getAssociationSets()).basicRemove(otherEnd, msgs);
			case OdataPackage.ENTITY_CONTAINER__FUNCTION_IMPORTS:
				return ((InternalEList<?>)getFunctionImports()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.ENTITY_CONTAINER__ANNOTATIONS:
				return getAnnotations();
			case OdataPackage.ENTITY_CONTAINER__DOCUMENTATION:
				return getDocumentation();
			case OdataPackage.ENTITY_CONTAINER__NAME:
				return getName();
			case OdataPackage.ENTITY_CONTAINER__ENTITY_SETS:
				return getEntitySets();
			case OdataPackage.ENTITY_CONTAINER__DEFAULT:
				return isDefault();
			case OdataPackage.ENTITY_CONTAINER__ASSOCIATION_SETS:
				return getAssociationSets();
			case OdataPackage.ENTITY_CONTAINER__FUNCTION_IMPORTS:
				return getFunctionImports();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.ENTITY_CONTAINER__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case OdataPackage.ENTITY_CONTAINER__DOCUMENTATION:
				setDocumentation((Documentation)newValue);
				return;
			case OdataPackage.ENTITY_CONTAINER__NAME:
				setName((String)newValue);
				return;
			case OdataPackage.ENTITY_CONTAINER__ENTITY_SETS:
				getEntitySets().clear();
				getEntitySets().addAll((Collection<? extends EntitySet>)newValue);
				return;
			case OdataPackage.ENTITY_CONTAINER__DEFAULT:
				setDefault((Boolean)newValue);
				return;
			case OdataPackage.ENTITY_CONTAINER__ASSOCIATION_SETS:
				getAssociationSets().clear();
				getAssociationSets().addAll((Collection<? extends AssociationSet>)newValue);
				return;
			case OdataPackage.ENTITY_CONTAINER__FUNCTION_IMPORTS:
				getFunctionImports().clear();
				getFunctionImports().addAll((Collection<? extends FunctionImport>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.ENTITY_CONTAINER__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case OdataPackage.ENTITY_CONTAINER__DOCUMENTATION:
				setDocumentation((Documentation)null);
				return;
			case OdataPackage.ENTITY_CONTAINER__NAME:
				setName(NAME_EDEFAULT);
				return;
			case OdataPackage.ENTITY_CONTAINER__ENTITY_SETS:
				getEntitySets().clear();
				return;
			case OdataPackage.ENTITY_CONTAINER__DEFAULT:
				setDefault(DEFAULT_EDEFAULT);
				return;
			case OdataPackage.ENTITY_CONTAINER__ASSOCIATION_SETS:
				getAssociationSets().clear();
				return;
			case OdataPackage.ENTITY_CONTAINER__FUNCTION_IMPORTS:
				getFunctionImports().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.ENTITY_CONTAINER__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case OdataPackage.ENTITY_CONTAINER__DOCUMENTATION:
				return documentation != null;
			case OdataPackage.ENTITY_CONTAINER__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case OdataPackage.ENTITY_CONTAINER__ENTITY_SETS:
				return entitySets != null && !entitySets.isEmpty();
			case OdataPackage.ENTITY_CONTAINER__DEFAULT:
				return default_ != DEFAULT_EDEFAULT;
			case OdataPackage.ENTITY_CONTAINER__ASSOCIATION_SETS:
				return associationSets != null && !associationSets.isEmpty();
			case OdataPackage.ENTITY_CONTAINER__FUNCTION_IMPORTS:
				return functionImports != null && !functionImports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IDocumentable.class) {
			switch (derivedFeatureID) {
				case OdataPackage.ENTITY_CONTAINER__DOCUMENTATION: return OdataPackage.IDOCUMENTABLE__DOCUMENTATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IDocumentable.class) {
			switch (baseFeatureID) {
				case OdataPackage.IDOCUMENTABLE__DOCUMENTATION: return OdataPackage.ENTITY_CONTAINER__DOCUMENTATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", default: "); //$NON-NLS-1$
		result.append(default_);
		result.append(')');
		return result.toString();
	}

} //EntityContainerImpl
