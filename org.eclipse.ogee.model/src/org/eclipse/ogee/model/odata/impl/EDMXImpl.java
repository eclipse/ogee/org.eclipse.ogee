/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXAnnotationsReference;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.OdataPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EDMX</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXImpl#getDataService <em>Data Service</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXImpl#getAnnotationsReferences <em>Annotations References</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXImpl#getReferences <em>References</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EDMXImpl#getURI <em>URI</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EDMXImpl extends EObjectImpl implements EDMX {
	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected Documentation documentation;

	/**
	 * The cached value of the '{@link #getDataService() <em>Data Service</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataService()
	 * @generated
	 * @ordered
	 */
	protected DataService dataService;

	/**
	 * The cached value of the '{@link #getAnnotationsReferences() <em>Annotations References</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotationsReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<EDMXAnnotationsReference> annotationsReferences;

	/**
	 * The cached value of the '{@link #getReferences() <em>References</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<EDMXReference> references;

	/**
	 * The default value of the '{@link #getURI() <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURI()
	 * @generated
	 * @ordered
	 */
	protected static final String URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getURI() <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURI()
	 * @generated
	 * @ordered
	 */
	protected String uri = URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EDMXImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.EDMX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDocumentation(Documentation newDocumentation, NotificationChain msgs) {
		Documentation oldDocumentation = documentation;
		documentation = newDocumentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.EDMX__DOCUMENTATION, oldDocumentation, newDocumentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentation(Documentation newDocumentation) {
		if (newDocumentation != documentation) {
			NotificationChain msgs = null;
			if (documentation != null)
				msgs = ((InternalEObject)documentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.EDMX__DOCUMENTATION, null, msgs);
			if (newDocumentation != null)
				msgs = ((InternalEObject)newDocumentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.EDMX__DOCUMENTATION, null, msgs);
			msgs = basicSetDocumentation(newDocumentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.EDMX__DOCUMENTATION, newDocumentation, newDocumentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataService getDataService() {
		return dataService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataService(DataService newDataService, NotificationChain msgs) {
		DataService oldDataService = dataService;
		dataService = newDataService;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.EDMX__DATA_SERVICE, oldDataService, newDataService);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataService(DataService newDataService) {
		if (newDataService != dataService) {
			NotificationChain msgs = null;
			if (dataService != null)
				msgs = ((InternalEObject)dataService).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.EDMX__DATA_SERVICE, null, msgs);
			if (newDataService != null)
				msgs = ((InternalEObject)newDataService).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.EDMX__DATA_SERVICE, null, msgs);
			msgs = basicSetDataService(newDataService, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.EDMX__DATA_SERVICE, newDataService, newDataService));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EDMXAnnotationsReference> getAnnotationsReferences() {
		if (annotationsReferences == null) {
			annotationsReferences = new EObjectContainmentEList<EDMXAnnotationsReference>(EDMXAnnotationsReference.class, this, OdataPackage.EDMX__ANNOTATIONS_REFERENCES);
		}
		return annotationsReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EDMXReference> getReferences() {
		if (references == null) {
			references = new EObjectContainmentEList<EDMXReference>(EDMXReference.class, this, OdataPackage.EDMX__REFERENCES);
		}
		return references;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getURI() {
		return uri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setURI(String newURI) {
		String oldURI = uri;
		uri = newURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.EDMX__URI, oldURI, uri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.EDMX__DOCUMENTATION:
				return basicSetDocumentation(null, msgs);
			case OdataPackage.EDMX__DATA_SERVICE:
				return basicSetDataService(null, msgs);
			case OdataPackage.EDMX__ANNOTATIONS_REFERENCES:
				return ((InternalEList<?>)getAnnotationsReferences()).basicRemove(otherEnd, msgs);
			case OdataPackage.EDMX__REFERENCES:
				return ((InternalEList<?>)getReferences()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.EDMX__DOCUMENTATION:
				return getDocumentation();
			case OdataPackage.EDMX__DATA_SERVICE:
				return getDataService();
			case OdataPackage.EDMX__ANNOTATIONS_REFERENCES:
				return getAnnotationsReferences();
			case OdataPackage.EDMX__REFERENCES:
				return getReferences();
			case OdataPackage.EDMX__URI:
				return getURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.EDMX__DOCUMENTATION:
				setDocumentation((Documentation)newValue);
				return;
			case OdataPackage.EDMX__DATA_SERVICE:
				setDataService((DataService)newValue);
				return;
			case OdataPackage.EDMX__ANNOTATIONS_REFERENCES:
				getAnnotationsReferences().clear();
				getAnnotationsReferences().addAll((Collection<? extends EDMXAnnotationsReference>)newValue);
				return;
			case OdataPackage.EDMX__REFERENCES:
				getReferences().clear();
				getReferences().addAll((Collection<? extends EDMXReference>)newValue);
				return;
			case OdataPackage.EDMX__URI:
				setURI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.EDMX__DOCUMENTATION:
				setDocumentation((Documentation)null);
				return;
			case OdataPackage.EDMX__DATA_SERVICE:
				setDataService((DataService)null);
				return;
			case OdataPackage.EDMX__ANNOTATIONS_REFERENCES:
				getAnnotationsReferences().clear();
				return;
			case OdataPackage.EDMX__REFERENCES:
				getReferences().clear();
				return;
			case OdataPackage.EDMX__URI:
				setURI(URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.EDMX__DOCUMENTATION:
				return documentation != null;
			case OdataPackage.EDMX__DATA_SERVICE:
				return dataService != null;
			case OdataPackage.EDMX__ANNOTATIONS_REFERENCES:
				return annotationsReferences != null && !annotationsReferences.isEmpty();
			case OdataPackage.EDMX__REFERENCES:
				return references != null && !references.isEmpty();
			case OdataPackage.EDMX__URI:
				return URI_EDEFAULT == null ? uri != null : !URI_EDEFAULT.equals(uri);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (URI: "); //$NON-NLS-1$
		result.append(uri);
		result.append(')');
		return result.toString();
	}

} //EDMXImpl
