/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IRecordValueType;
import org.eclipse.ogee.model.odata.ITypeTerm;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entity Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#getNavigationProperties <em>Navigation Properties</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#getKeys <em>Keys</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#getBaseType <em>Base Type</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#isMedia <em>Media</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#getEntitySets <em>Entity Sets</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl#getDerivedTypes <em>Derived Types</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EntityTypeImpl extends EObjectImpl implements EntityType {
	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected Documentation documentation;

	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<Property> properties;

	/**
	 * The cached value of the '{@link #getNavigationProperties() <em>Navigation Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNavigationProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<NavigationProperty> navigationProperties;

	/**
	 * The cached value of the '{@link #getKeys() <em>Keys</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeys()
	 * @generated
	 * @ordered
	 */
	protected EList<Property> keys;

	/**
	 * The cached value of the '{@link #getBaseType() <em>Base Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseType()
	 * @generated
	 * @ordered
	 */
	protected EntityType baseType;

	/**
	 * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * The default value of the '{@link #isMedia() <em>Media</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMedia()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MEDIA_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isMedia() <em>Media</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMedia()
	 * @generated
	 * @ordered
	 */
	protected boolean media = MEDIA_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEntitySets() <em>Entity Sets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntitySets()
	 * @generated
	 * @ordered
	 */
	protected EList<EntitySet> entitySets;

	/**
	 * The cached value of the '{@link #getDerivedTypes() <em>Derived Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<EntityType> derivedTypes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntityTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.ENTITY_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Documentation getDocumentation() {
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDocumentation(Documentation newDocumentation, NotificationChain msgs) {
		Documentation oldDocumentation = documentation;
		documentation = newDocumentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_TYPE__DOCUMENTATION, oldDocumentation, newDocumentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentation(Documentation newDocumentation) {
		if (newDocumentation != documentation) {
			NotificationChain msgs = null;
			if (documentation != null)
				msgs = ((InternalEObject)documentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdataPackage.ENTITY_TYPE__DOCUMENTATION, null, msgs);
			if (newDocumentation != null)
				msgs = ((InternalEObject)newDocumentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdataPackage.ENTITY_TYPE__DOCUMENTATION, null, msgs);
			msgs = basicSetDocumentation(newDocumentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_TYPE__DOCUMENTATION, newDocumentation, newDocumentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectWithInverseResolvingEList<Annotation>(Annotation.class, this, OdataPackage.ENTITY_TYPE__ANNOTATIONS, OdataPackage.ANNOTATION__TARGET);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Property> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<Property>(Property.class, this, OdataPackage.ENTITY_TYPE__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NavigationProperty> getNavigationProperties() {
		if (navigationProperties == null) {
			navigationProperties = new EObjectContainmentEList<NavigationProperty>(NavigationProperty.class, this, OdataPackage.ENTITY_TYPE__NAVIGATION_PROPERTIES);
		}
		return navigationProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Property> getKeys() {
		if (keys == null) {
			keys = new EObjectContainmentEList<Property>(Property.class, this, OdataPackage.ENTITY_TYPE__KEYS);
		}
		return keys;
	}
	
	public boolean isDerivedFrom(EntityType entityType){
		  if(this == entityType){
		   return true;
		  }
		  EntityType baseType = this.getBaseType();
		  if(baseType == null){
		   return false;
		  }
		  return baseType.isDerivedFrom(entityType);
		}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityType getBaseType() {
		if (baseType != null && baseType.eIsProxy()) {
			InternalEObject oldBaseType = (InternalEObject)baseType;
			baseType = (EntityType)eResolveProxy(oldBaseType);
			if (baseType != oldBaseType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.ENTITY_TYPE__BASE_TYPE, oldBaseType, baseType));
			}
		}
		return baseType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityType basicGetBaseType() {
		return baseType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBaseType(EntityType newBaseType, NotificationChain msgs) {
		EntityType oldBaseType = baseType;
		baseType = newBaseType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_TYPE__BASE_TYPE, oldBaseType, newBaseType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseType(EntityType newBaseType) {
		if (newBaseType != baseType) {
			NotificationChain msgs = null;
			if (baseType != null)
				msgs = ((InternalEObject)baseType).eInverseRemove(this, OdataPackage.ENTITY_TYPE__DERIVED_TYPES, EntityType.class, msgs);
			if (newBaseType != null)
				msgs = ((InternalEObject)newBaseType).eInverseAdd(this, OdataPackage.ENTITY_TYPE__DERIVED_TYPES, EntityType.class, msgs);
			msgs = basicSetBaseType(newBaseType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_TYPE__BASE_TYPE, newBaseType, newBaseType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstract(boolean newAbstract) {
		boolean oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_TYPE__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMedia() {
		return media;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMedia(boolean newMedia) {
		boolean oldMedia = media;
		media = newMedia;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.ENTITY_TYPE__MEDIA, oldMedia, media));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntitySet> getEntitySets() {
		if (entitySets == null) {
			entitySets = new EObjectWithInverseResolvingEList<EntitySet>(EntitySet.class, this, OdataPackage.ENTITY_TYPE__ENTITY_SETS, OdataPackage.ENTITY_SET__TYPE);
		}
		return entitySets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntityType> getDerivedTypes() {
		if (derivedTypes == null) {
			derivedTypes = new EObjectWithInverseResolvingEList<EntityType>(EntityType.class, this, OdataPackage.ENTITY_TYPE__DERIVED_TYPES, OdataPackage.ENTITY_TYPE__BASE_TYPE);
		}
		return derivedTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.ENTITY_TYPE__ANNOTATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAnnotations()).basicAdd(otherEnd, msgs);
			case OdataPackage.ENTITY_TYPE__BASE_TYPE:
				if (baseType != null)
					msgs = ((InternalEObject)baseType).eInverseRemove(this, OdataPackage.ENTITY_TYPE__DERIVED_TYPES, EntityType.class, msgs);
				return basicSetBaseType((EntityType)otherEnd, msgs);
			case OdataPackage.ENTITY_TYPE__ENTITY_SETS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEntitySets()).basicAdd(otherEnd, msgs);
			case OdataPackage.ENTITY_TYPE__DERIVED_TYPES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDerivedTypes()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdataPackage.ENTITY_TYPE__DOCUMENTATION:
				return basicSetDocumentation(null, msgs);
			case OdataPackage.ENTITY_TYPE__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case OdataPackage.ENTITY_TYPE__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case OdataPackage.ENTITY_TYPE__NAVIGATION_PROPERTIES:
				return ((InternalEList<?>)getNavigationProperties()).basicRemove(otherEnd, msgs);
			case OdataPackage.ENTITY_TYPE__KEYS:
				return ((InternalEList<?>)getKeys()).basicRemove(otherEnd, msgs);
			case OdataPackage.ENTITY_TYPE__BASE_TYPE:
				return basicSetBaseType(null, msgs);
			case OdataPackage.ENTITY_TYPE__ENTITY_SETS:
				return ((InternalEList<?>)getEntitySets()).basicRemove(otherEnd, msgs);
			case OdataPackage.ENTITY_TYPE__DERIVED_TYPES:
				return ((InternalEList<?>)getDerivedTypes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.ENTITY_TYPE__DOCUMENTATION:
				return getDocumentation();
			case OdataPackage.ENTITY_TYPE__ANNOTATIONS:
				return getAnnotations();
			case OdataPackage.ENTITY_TYPE__NAME:
				return getName();
			case OdataPackage.ENTITY_TYPE__PROPERTIES:
				return getProperties();
			case OdataPackage.ENTITY_TYPE__NAVIGATION_PROPERTIES:
				return getNavigationProperties();
			case OdataPackage.ENTITY_TYPE__KEYS:
				return getKeys();
			case OdataPackage.ENTITY_TYPE__BASE_TYPE:
				if (resolve) return getBaseType();
				return basicGetBaseType();
			case OdataPackage.ENTITY_TYPE__ABSTRACT:
				return isAbstract();
			case OdataPackage.ENTITY_TYPE__MEDIA:
				return isMedia();
			case OdataPackage.ENTITY_TYPE__ENTITY_SETS:
				return getEntitySets();
			case OdataPackage.ENTITY_TYPE__DERIVED_TYPES:
				return getDerivedTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.ENTITY_TYPE__DOCUMENTATION:
				setDocumentation((Documentation)newValue);
				return;
			case OdataPackage.ENTITY_TYPE__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case OdataPackage.ENTITY_TYPE__NAME:
				setName((String)newValue);
				return;
			case OdataPackage.ENTITY_TYPE__PROPERTIES:
				getProperties().clear();
				getProperties().addAll((Collection<? extends Property>)newValue);
				return;
			case OdataPackage.ENTITY_TYPE__NAVIGATION_PROPERTIES:
				getNavigationProperties().clear();
				getNavigationProperties().addAll((Collection<? extends NavigationProperty>)newValue);
				return;
			case OdataPackage.ENTITY_TYPE__KEYS:
				getKeys().clear();
				getKeys().addAll((Collection<? extends Property>)newValue);
				return;
			case OdataPackage.ENTITY_TYPE__BASE_TYPE:
				setBaseType((EntityType)newValue);
				return;
			case OdataPackage.ENTITY_TYPE__ABSTRACT:
				setAbstract((Boolean)newValue);
				return;
			case OdataPackage.ENTITY_TYPE__MEDIA:
				setMedia((Boolean)newValue);
				return;
			case OdataPackage.ENTITY_TYPE__ENTITY_SETS:
				getEntitySets().clear();
				getEntitySets().addAll((Collection<? extends EntitySet>)newValue);
				return;
			case OdataPackage.ENTITY_TYPE__DERIVED_TYPES:
				getDerivedTypes().clear();
				getDerivedTypes().addAll((Collection<? extends EntityType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.ENTITY_TYPE__DOCUMENTATION:
				setDocumentation((Documentation)null);
				return;
			case OdataPackage.ENTITY_TYPE__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case OdataPackage.ENTITY_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case OdataPackage.ENTITY_TYPE__PROPERTIES:
				getProperties().clear();
				return;
			case OdataPackage.ENTITY_TYPE__NAVIGATION_PROPERTIES:
				getNavigationProperties().clear();
				return;
			case OdataPackage.ENTITY_TYPE__KEYS:
				getKeys().clear();
				return;
			case OdataPackage.ENTITY_TYPE__BASE_TYPE:
				setBaseType((EntityType)null);
				return;
			case OdataPackage.ENTITY_TYPE__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
			case OdataPackage.ENTITY_TYPE__MEDIA:
				setMedia(MEDIA_EDEFAULT);
				return;
			case OdataPackage.ENTITY_TYPE__ENTITY_SETS:
				getEntitySets().clear();
				return;
			case OdataPackage.ENTITY_TYPE__DERIVED_TYPES:
				getDerivedTypes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.ENTITY_TYPE__DOCUMENTATION:
				return documentation != null;
			case OdataPackage.ENTITY_TYPE__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case OdataPackage.ENTITY_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case OdataPackage.ENTITY_TYPE__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case OdataPackage.ENTITY_TYPE__NAVIGATION_PROPERTIES:
				return navigationProperties != null && !navigationProperties.isEmpty();
			case OdataPackage.ENTITY_TYPE__KEYS:
				return keys != null && !keys.isEmpty();
			case OdataPackage.ENTITY_TYPE__BASE_TYPE:
				return baseType != null;
			case OdataPackage.ENTITY_TYPE__ABSTRACT:
				return abstract_ != ABSTRACT_EDEFAULT;
			case OdataPackage.ENTITY_TYPE__MEDIA:
				return media != MEDIA_EDEFAULT;
			case OdataPackage.ENTITY_TYPE__ENTITY_SETS:
				return entitySets != null && !entitySets.isEmpty();
			case OdataPackage.ENTITY_TYPE__DERIVED_TYPES:
				return derivedTypes != null && !derivedTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IAnnotationTarget.class) {
			switch (derivedFeatureID) {
				case OdataPackage.ENTITY_TYPE__ANNOTATIONS: return OdataPackage.IANNOTATION_TARGET__ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == ITypeTerm.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == IRecordValueType.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IAnnotationTarget.class) {
			switch (baseFeatureID) {
				case OdataPackage.IANNOTATION_TARGET__ANNOTATIONS: return OdataPackage.ENTITY_TYPE__ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == ITypeTerm.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == IRecordValueType.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", abstract: "); //$NON-NLS-1$
		result.append(abstract_);
		result.append(", media: "); //$NON-NLS-1$
		result.append(media);
		result.append(')');
		return result.toString();
	}

} //EntityTypeImpl
