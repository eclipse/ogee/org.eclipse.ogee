/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Return Entity Type Usage</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl#getEntitySet <em>Entity Set</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl#getEntityType <em>Entity Type</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl#isCollection <em>Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReturnEntityTypeUsageImpl extends EObjectImpl implements ReturnEntityTypeUsage {
	/**
	 * The cached value of the '{@link #getEntitySet() <em>Entity Set</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntitySet()
	 * @generated
	 * @ordered
	 */
	protected EntitySet entitySet;

	/**
	 * The cached value of the '{@link #getEntityType() <em>Entity Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntityType()
	 * @generated
	 * @ordered
	 */
	protected EntityType entityType;

	/**
	 * The default value of the '{@link #isCollection() <em>Collection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCollection()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COLLECTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCollection() <em>Collection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCollection()
	 * @generated
	 * @ordered
	 */
	protected boolean collection = COLLECTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReturnEntityTypeUsageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdataPackage.Literals.RETURN_ENTITY_TYPE_USAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntitySet getEntitySet() {
		if (entitySet != null && entitySet.eIsProxy()) {
			InternalEObject oldEntitySet = (InternalEObject)entitySet;
			entitySet = (EntitySet)eResolveProxy(oldEntitySet);
			if (entitySet != oldEntitySet) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_SET, oldEntitySet, entitySet));
			}
		}
		return entitySet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntitySet basicGetEntitySet() {
		return entitySet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntitySet(EntitySet newEntitySet) {
		EntitySet oldEntitySet = entitySet;
		entitySet = newEntitySet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_SET, oldEntitySet, entitySet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityType getEntityType() {
		if (entityType != null && entityType.eIsProxy()) {
			InternalEObject oldEntityType = (InternalEObject)entityType;
			entityType = (EntityType)eResolveProxy(oldEntityType);
			if (entityType != oldEntityType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_TYPE, oldEntityType, entityType));
			}
		}
		return entityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityType basicGetEntityType() {
		return entityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntityType(EntityType newEntityType) {
		EntityType oldEntityType = entityType;
		entityType = newEntityType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_TYPE, oldEntityType, entityType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCollection() {
		return collection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollection(boolean newCollection) {
		boolean oldCollection = collection;
		collection = newCollection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdataPackage.RETURN_ENTITY_TYPE_USAGE__COLLECTION, oldCollection, collection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_SET:
				if (resolve) return getEntitySet();
				return basicGetEntitySet();
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_TYPE:
				if (resolve) return getEntityType();
				return basicGetEntityType();
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__COLLECTION:
				return isCollection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_SET:
				setEntitySet((EntitySet)newValue);
				return;
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_TYPE:
				setEntityType((EntityType)newValue);
				return;
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__COLLECTION:
				setCollection((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_SET:
				setEntitySet((EntitySet)null);
				return;
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_TYPE:
				setEntityType((EntityType)null);
				return;
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__COLLECTION:
				setCollection(COLLECTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_SET:
				return entitySet != null;
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__ENTITY_TYPE:
				return entityType != null;
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE__COLLECTION:
				return collection != COLLECTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (collection: "); //$NON-NLS-1$
		result.append(collection);
		result.append(')');
		return result.toString();
	}

} //ReturnEntityTypeUsageImpl
