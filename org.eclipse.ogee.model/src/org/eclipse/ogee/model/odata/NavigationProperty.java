/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Navigation Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.NavigationProperty#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.NavigationProperty#getRelationship <em>Relationship</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.NavigationProperty#getFromRole <em>From Role</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.NavigationProperty#getToRole <em>To Role</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.NavigationProperty#isContainsTarget <em>Contains Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getNavigationProperty()
 * @model
 * @generated
 */
public interface NavigationProperty extends IAnnotationTarget, IDocumentable {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getNavigationProperty_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.NavigationProperty#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Relationship</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationship</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationship</em>' reference.
	 * @see #setRelationship(Association)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getNavigationProperty_Relationship()
	 * @model required="true"
	 * @generated
	 */
	Association getRelationship();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.NavigationProperty#getRelationship <em>Relationship</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relationship</em>' reference.
	 * @see #getRelationship()
	 * @generated
	 */
	void setRelationship(Association value);

	/**
	 * Returns the value of the '<em><b>From Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Role</em>' reference.
	 * @see #setFromRole(Role)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getNavigationProperty_FromRole()
	 * @model required="true"
	 * @generated
	 */
	Role getFromRole();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.NavigationProperty#getFromRole <em>From Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Role</em>' reference.
	 * @see #getFromRole()
	 * @generated
	 */
	void setFromRole(Role value);

	/**
	 * Returns the value of the '<em><b>To Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Role</em>' reference.
	 * @see #setToRole(Role)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getNavigationProperty_ToRole()
	 * @model required="true"
	 * @generated
	 */
	Role getToRole();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.NavigationProperty#getToRole <em>To Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Role</em>' reference.
	 * @see #getToRole()
	 * @generated
	 */
	void setToRole(Role value);

	/**
	 * Returns the value of the '<em><b>Contains Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains Target</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains Target</em>' attribute.
	 * @see #setContainsTarget(boolean)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getNavigationProperty_ContainsTarget()
	 * @model
	 * @generated
	 */
	boolean isContainsTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.NavigationProperty#isContainsTarget <em>Contains Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contains Target</em>' attribute.
	 * @see #isContainsTarget()
	 * @generated
	 */
	void setContainsTarget(boolean value);

} // NavigationProperty
