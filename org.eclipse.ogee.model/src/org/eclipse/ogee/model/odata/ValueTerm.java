/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.ValueTerm#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ValueTerm#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ValueTerm#getValueAnnotations <em>Value Annotations</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getValueTerm()
 * @model
 * @generated
 */
public interface ValueTerm extends IAnnotationTarget, IDocumentable {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getValueTerm_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ValueTerm#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(IPropertyTypeUsage)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getValueTerm_Type()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IPropertyTypeUsage getType();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ValueTerm#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(IPropertyTypeUsage value);

	/**
	 * Returns the value of the '<em><b>Value Annotations</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.ValueAnnotation}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.ogee.model.odata.ValueAnnotation#getTerm <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Annotations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Annotations</em>' reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getValueTerm_ValueAnnotations()
	 * @see org.eclipse.ogee.model.odata.ValueAnnotation#getTerm
	 * @model opposite="term"
	 * @generated
	 */
	EList<ValueAnnotation> getValueAnnotations();

} // ValueTerm
