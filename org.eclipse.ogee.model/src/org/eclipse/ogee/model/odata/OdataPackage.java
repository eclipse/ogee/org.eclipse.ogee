/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.ogee.model.odata.OdataFactory
 * @model kind="package"
 * @generated
 */
public interface OdataPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "odata"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://odata/1.0"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "odata"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OdataPackage eINSTANCE = org.eclipse.ogee.model.odata.impl.OdataPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.IDocumentable <em>IDocumentable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.IDocumentable
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIDocumentable()
	 * @generated
	 */
	int IDOCUMENTABLE = 24;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDOCUMENTABLE__DOCUMENTATION = 0;

	/**
	 * The number of structural features of the '<em>IDocumentable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDOCUMENTABLE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl <em>Entity Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EntityTypeImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEntityType()
	 * @generated
	 */
	int ENTITY_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__DOCUMENTATION = IDOCUMENTABLE__DOCUMENTATION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__ANNOTATIONS = IDOCUMENTABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__NAME = IDOCUMENTABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__PROPERTIES = IDOCUMENTABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Navigation Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__NAVIGATION_PROPERTIES = IDOCUMENTABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Keys</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__KEYS = IDOCUMENTABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Base Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__BASE_TYPE = IDOCUMENTABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__ABSTRACT = IDOCUMENTABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Media</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__MEDIA = IDOCUMENTABLE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Entity Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__ENTITY_SETS = IDOCUMENTABLE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Derived Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE__DERIVED_TYPES = IDOCUMENTABLE_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Entity Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE_FEATURE_COUNT = IDOCUMENTABLE_FEATURE_COUNT + 10;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.IAnnotationTarget <em>IAnnotation Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.IAnnotationTarget
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIAnnotationTarget()
	 * @generated
	 */
	int IANNOTATION_TARGET = 20;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IANNOTATION_TARGET__ANNOTATIONS = 0;

	/**
	 * The number of structural features of the '<em>IAnnotation Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IANNOTATION_TARGET_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.PropertyImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__NAME = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__TYPE = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Nullable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__NULLABLE = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>For Etag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__FOR_ETAG = IANNOTATION_TARGET_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.ComplexTypeImpl <em>Complex Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.ComplexTypeImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getComplexType()
	 * @generated
	 */
	int COMPLEX_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE__DOCUMENTATION = IDOCUMENTABLE__DOCUMENTATION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE__ANNOTATIONS = IDOCUMENTABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE__PROPERTIES = IDOCUMENTABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE__NAME = IDOCUMENTABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE__ABSTRACT = IDOCUMENTABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Base Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE__BASE_TYPE = IDOCUMENTABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Derived Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE__DERIVED_TYPES = IDOCUMENTABLE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Complex Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_FEATURE_COUNT = IDOCUMENTABLE_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.AssociationImpl <em>Association</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.AssociationImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getAssociation()
	 * @generated
	 */
	int ASSOCIATION = 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION__NAME = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ends</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION__ENDS = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Referential Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION__REFERENTIAL_CONSTRAINT = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Association Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION__ASSOCIATION_SETS = IANNOTATION_TARGET_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Association</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl <em>Navigation Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getNavigationProperty()
	 * @generated
	 */
	int NAVIGATION_PROPERTY = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAVIGATION_PROPERTY__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAVIGATION_PROPERTY__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAVIGATION_PROPERTY__NAME = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Relationship</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAVIGATION_PROPERTY__RELATIONSHIP = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>From Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAVIGATION_PROPERTY__FROM_ROLE = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>To Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAVIGATION_PROPERTY__TO_ROLE = IANNOTATION_TARGET_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Contains Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAVIGATION_PROPERTY__CONTAINS_TARGET = IANNOTATION_TARGET_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Navigation Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAVIGATION_PROPERTY_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.FunctionImportImpl <em>Function Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.FunctionImportImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getFunctionImport()
	 * @generated
	 */
	int FUNCTION_IMPORT = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_IMPORT__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_IMPORT__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_IMPORT__NAME = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_IMPORT__PARAMETERS = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_IMPORT__BINDING = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Side Effecting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_IMPORT__SIDE_EFFECTING = IANNOTATION_TARGET_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_IMPORT__RETURN_TYPE = IANNOTATION_TARGET_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Function Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_IMPORT_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.ParameterImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 6;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__NAME = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__TYPE = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EntitySetImpl <em>Entity Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EntitySetImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEntitySet()
	 * @generated
	 */
	int ENTITY_SET = 7;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_SET__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_SET__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_SET__TYPE = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_SET__NAME = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Entity Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_SET_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.AssociationSetImpl <em>Association Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.AssociationSetImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getAssociationSet()
	 * @generated
	 */
	int ASSOCIATION_SET = 8;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET__ASSOCIATION = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET__NAME = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Ends</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET__ENDS = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Association Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.RoleImpl <em>Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.RoleImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 9;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__NAME = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__TYPE = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__MULTIPLICITY = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.AssociationSetEndImpl <em>Association Set End</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.AssociationSetEndImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getAssociationSetEnd()
	 * @generated
	 */
	int ASSOCIATION_SET_END = 10;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET_END__DOCUMENTATION = IDOCUMENTABLE__DOCUMENTATION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET_END__ANNOTATIONS = IDOCUMENTABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET_END__ROLE = IDOCUMENTABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Entity Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET_END__ENTITY_SET = IDOCUMENTABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Association Set End</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_SET_END_FEATURE_COUNT = IDOCUMENTABLE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EDMXImpl <em>EDMX</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EDMXImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEDMX()
	 * @generated
	 */
	int EDMX = 11;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX__DOCUMENTATION = IDOCUMENTABLE__DOCUMENTATION;

	/**
	 * The feature id for the '<em><b>Data Service</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX__DATA_SERVICE = IDOCUMENTABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Annotations References</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX__ANNOTATIONS_REFERENCES = IDOCUMENTABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>References</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX__REFERENCES = IDOCUMENTABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX__URI = IDOCUMENTABLE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>EDMX</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_FEATURE_COUNT = IDOCUMENTABLE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.ReferentialConstraintImpl <em>Referential Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.ReferentialConstraintImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getReferentialConstraint()
	 * @generated
	 */
	int REFERENTIAL_CONSTRAINT = 12;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_CONSTRAINT__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_CONSTRAINT__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Principal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_CONSTRAINT__PRINCIPAL = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Dependent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_CONSTRAINT__DEPENDENT = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Key Mappings</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_CONSTRAINT__KEY_MAPPINGS = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Referential Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_CONSTRAINT_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.IParameterTypeUsage <em>IParameter Type Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.IParameterTypeUsage
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIParameterTypeUsage()
	 * @generated
	 */
	int IPARAMETER_TYPE_USAGE = 15;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPARAMETER_TYPE_USAGE__COLLECTION = 0;

	/**
	 * The number of structural features of the '<em>IParameter Type Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPARAMETER_TYPE_USAGE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.IPropertyTypeUsage <em>IProperty Type Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.IPropertyTypeUsage
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIPropertyTypeUsage()
	 * @generated
	 */
	int IPROPERTY_TYPE_USAGE = 13;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPROPERTY_TYPE_USAGE__COLLECTION = IPARAMETER_TYPE_USAGE__COLLECTION;

	/**
	 * The number of structural features of the '<em>IProperty Type Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPROPERTY_TYPE_USAGE_FEATURE_COUNT = IPARAMETER_TYPE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.SimpleTypeImpl <em>Simple Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.SimpleTypeImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSimpleType()
	 * @generated
	 */
	int SIMPLE_TYPE = 14;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Max Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE__MAX_LENGTH = 1;

	/**
	 * The feature id for the '<em><b>Precision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE__PRECISION = 2;

	/**
	 * The feature id for the '<em><b>Scale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE__SCALE = 3;

	/**
	 * The feature id for the '<em><b>Fixed Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE__FIXED_LENGTH = 4;

	/**
	 * The feature id for the '<em><b>Default Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE__DEFAULT_VALUE = 5;

	/**
	 * The number of structural features of the '<em>Simple Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.BindingImpl <em>Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.BindingImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getBinding()
	 * @generated
	 */
	int BINDING = 16;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__COLLECTION = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.ValueTermImpl <em>Value Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.ValueTermImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getValueTerm()
	 * @generated
	 */
	int VALUE_TERM = 17;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TERM__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TERM__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TERM__NAME = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TERM__TYPE = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Value Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TERM__VALUE_ANNOTATIONS = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Value Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TERM_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.AnnotationImpl <em>Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.AnnotationImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getAnnotation()
	 * @generated
	 */
	int ANNOTATION = 27;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__QUALIFIER = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__TARGET = 1;

	/**
	 * The number of structural features of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.ValueAnnotationImpl <em>Value Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.ValueAnnotationImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getValueAnnotation()
	 * @generated
	 */
	int VALUE_ANNOTATION = 18;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_ANNOTATION__QUALIFIER = ANNOTATION__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_ANNOTATION__TARGET = ANNOTATION__TARGET;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_ANNOTATION__DOCUMENTATION = ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Term</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_ANNOTATION__TERM = ANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Annotation Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_ANNOTATION__ANNOTATION_VALUE = ANNOTATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Value Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_ANNOTATION_FEATURE_COUNT = ANNOTATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.TypeAnnotationImpl <em>Type Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.TypeAnnotationImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getTypeAnnotation()
	 * @generated
	 */
	int TYPE_ANNOTATION = 19;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_ANNOTATION__QUALIFIER = ANNOTATION__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_ANNOTATION__TARGET = ANNOTATION__TARGET;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_ANNOTATION__DOCUMENTATION = ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Term</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_ANNOTATION__TERM = ANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Annotation Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_ANNOTATION__ANNOTATION_VALUES = ANNOTATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Type Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_ANNOTATION_FEATURE_COUNT = ANNOTATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EntityContainerImpl <em>Entity Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EntityContainerImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEntityContainer()
	 * @generated
	 */
	int ENTITY_CONTAINER = 21;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_CONTAINER__ANNOTATIONS = IANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_CONTAINER__DOCUMENTATION = IANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_CONTAINER__NAME = IANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Entity Sets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_CONTAINER__ENTITY_SETS = IANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_CONTAINER__DEFAULT = IANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Association Sets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_CONTAINER__ASSOCIATION_SETS = IANNOTATION_TARGET_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Function Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_CONTAINER__FUNCTION_IMPORTS = IANNOTATION_TARGET_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Entity Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_CONTAINER_FEATURE_COUNT = IANNOTATION_TARGET_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EnumTypeImpl <em>Enum Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EnumTypeImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEnumType()
	 * @generated
	 */
	int ENUM_TYPE = 22;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__DOCUMENTATION = IDOCUMENTABLE__DOCUMENTATION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__ANNOTATIONS = IDOCUMENTABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__NAME = IDOCUMENTABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__FLAGS = IDOCUMENTABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Members</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__MEMBERS = IDOCUMENTABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Underlying Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__UNDERLYING_TYPE = IDOCUMENTABLE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Enum Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE_FEATURE_COUNT = IDOCUMENTABLE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EnumMemberImpl <em>Enum Member</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EnumMemberImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEnumMember()
	 * @generated
	 */
	int ENUM_MEMBER = 23;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_MEMBER__DOCUMENTATION = IDOCUMENTABLE__DOCUMENTATION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_MEMBER__NAME = IDOCUMENTABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_MEMBER__VALUE = IDOCUMENTABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Enum Member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_MEMBER_FEATURE_COUNT = IDOCUMENTABLE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.DocumentationImpl <em>Documentation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.DocumentationImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDocumentation()
	 * @generated
	 */
	int DOCUMENTATION = 25;

	/**
	 * The feature id for the '<em><b>Summary</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENTATION__SUMMARY = 0;

	/**
	 * The feature id for the '<em><b>Long Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENTATION__LONG_DESCRIPTION = 1;

	/**
	 * The number of structural features of the '<em>Documentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENTATION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.ITypeTerm <em>IType Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.ITypeTerm
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getITypeTerm()
	 * @generated
	 */
	int ITYPE_TERM = 26;

	/**
	 * The number of structural features of the '<em>IType Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPE_TERM_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EDMXSetImpl <em>EDMX Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EDMXSetImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEDMXSet()
	 * @generated
	 */
	int EDMX_SET = 28;

	/**
	 * The feature id for the '<em><b>Import Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_SET__IMPORT_SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Import Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_SET__IMPORT_DATE = 1;

	/**
	 * The feature id for the '<em><b>Main EDMX</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_SET__MAIN_EDMX = 2;

	/**
	 * The feature id for the '<em><b>Edmx</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_SET__EDMX = 3;

	/**
	 * The feature id for the '<em><b>Full Scope EDMX</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_SET__FULL_SCOPE_EDMX = 4;

	/**
	 * The feature id for the '<em><b>Annotations Scope EDMX</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_SET__ANNOTATIONS_SCOPE_EDMX = 5;

	/**
	 * The feature id for the '<em><b>Schemata</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_SET__SCHEMATA = 6;

	/**
	 * The number of structural features of the '<em>EDMX Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_SET_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EntityTypeUsageImpl <em>Entity Type Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EntityTypeUsageImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEntityTypeUsage()
	 * @generated
	 */
	int ENTITY_TYPE_USAGE = 29;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE_USAGE__COLLECTION = IPARAMETER_TYPE_USAGE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Entity Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE_USAGE__ENTITY_TYPE = IPARAMETER_TYPE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Entity Type Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_TYPE_USAGE_FEATURE_COUNT = IPARAMETER_TYPE_USAGE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl <em>Complex Type Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getComplexTypeUsage()
	 * @generated
	 */
	int COMPLEX_TYPE_USAGE = 30;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_USAGE__COLLECTION = IPROPERTY_TYPE_USAGE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Complex Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_USAGE__COMPLEX_TYPE = IPROPERTY_TYPE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Complex Type Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_USAGE_FEATURE_COUNT = IPROPERTY_TYPE_USAGE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl <em>Enum Type Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEnumTypeUsage()
	 * @generated
	 */
	int ENUM_TYPE_USAGE = 31;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE_USAGE__COLLECTION = IPROPERTY_TYPE_USAGE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Enum Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE_USAGE__ENUM_TYPE = IPROPERTY_TYPE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enum Type Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE_USAGE_FEATURE_COUNT = IPROPERTY_TYPE_USAGE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.SimpleTypeUsageImpl <em>Simple Type Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.SimpleTypeUsageImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSimpleTypeUsage()
	 * @generated
	 */
	int SIMPLE_TYPE_USAGE = 32;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE_USAGE__COLLECTION = IPROPERTY_TYPE_USAGE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Simple Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE_USAGE__SIMPLE_TYPE = IPROPERTY_TYPE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simple Type Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE_USAGE_FEATURE_COUNT = IPROPERTY_TYPE_USAGE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage <em>IFunction Return Type Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIFunctionReturnTypeUsage()
	 * @generated
	 */
	int IFUNCTION_RETURN_TYPE_USAGE = 33;

	/**
	 * The number of structural features of the '<em>IFunction Return Type Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFUNCTION_RETURN_TYPE_USAGE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EDMXReferenceImpl <em>EDMX Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EDMXReferenceImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEDMXReference()
	 * @generated
	 */
	int EDMX_REFERENCE = 34;

	/**
	 * The feature id for the '<em><b>Referenced EDMX</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_REFERENCE__REFERENCED_EDMX = 0;

	/**
	 * The number of structural features of the '<em>EDMX Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_REFERENCE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EDMXAnnotationsReferenceImpl <em>EDMX Annotations Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EDMXAnnotationsReferenceImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEDMXAnnotationsReference()
	 * @generated
	 */
	int EDMX_ANNOTATIONS_REFERENCE = 35;

	/**
	 * The feature id for the '<em><b>Include Restrictions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_ANNOTATIONS_REFERENCE__INCLUDE_RESTRICTIONS = 0;

	/**
	 * The feature id for the '<em><b>Referenced EDMX</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_ANNOTATIONS_REFERENCE__REFERENCED_EDMX = 1;

	/**
	 * The number of structural features of the '<em>EDMX Annotations Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDMX_ANNOTATIONS_REFERENCE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.SchemaImpl <em>Schema</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.SchemaImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSchema()
	 * @generated
	 */
	int SCHEMA = 36;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__DOCUMENTATION = IDOCUMENTABLE__DOCUMENTATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__NAMESPACE = IDOCUMENTABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Containers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__CONTAINERS = IDOCUMENTABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Entity Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__ENTITY_TYPES = IDOCUMENTABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Associations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__ASSOCIATIONS = IDOCUMENTABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Enum Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__ENUM_TYPES = IDOCUMENTABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Complex Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__COMPLEX_TYPES = IDOCUMENTABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Usings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__USINGS = IDOCUMENTABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Value Terms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__VALUE_TERMS = IDOCUMENTABLE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Value Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__VALUE_ANNOTATIONS = IDOCUMENTABLE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Type Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__TYPE_ANNOTATIONS = IDOCUMENTABLE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Alias</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__ALIAS = IDOCUMENTABLE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__CLASSIFIERS = IDOCUMENTABLE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Data Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA__DATA_SERVICES = IDOCUMENTABLE_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>Schema</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEMA_FEATURE_COUNT = IDOCUMENTABLE_FEATURE_COUNT + 13;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.DataServiceImpl <em>Data Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.DataServiceImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDataService()
	 * @generated
	 */
	int DATA_SERVICE = 37;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SERVICE__VERSION = 0;

	/**
	 * The feature id for the '<em><b>Schemata</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SERVICE__SCHEMATA = 1;

	/**
	 * The number of structural features of the '<em>Data Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SERVICE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.IncludeRestrictionImpl <em>Include Restriction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.IncludeRestrictionImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIncludeRestriction()
	 * @generated
	 */
	int INCLUDE_RESTRICTION = 38;

	/**
	 * The feature id for the '<em><b>Term Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE_RESTRICTION__TERM_NAMESPACE = 0;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE_RESTRICTION__QUALIFIER = 1;

	/**
	 * The number of structural features of the '<em>Include Restriction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE_RESTRICTION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.AnnotationValue <em>Annotation Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.AnnotationValue
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getAnnotationValue()
	 * @generated
	 */
	int ANNOTATION_VALUE = 44;

	/**
	 * The number of structural features of the '<em>Annotation Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_VALUE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.ConstantExpression <em>Constant Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.ConstantExpression
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getConstantExpression()
	 * @generated
	 */
	int CONSTANT_EXPRESSION = 50;

	/**
	 * The number of structural features of the '<em>Constant Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_EXPRESSION_FEATURE_COUNT = ANNOTATION_VALUE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.SimpleValue <em>Simple Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.SimpleValue
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSimpleValue()
	 * @generated
	 */
	int SIMPLE_VALUE = 39;

	/**
	 * The number of structural features of the '<em>Simple Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_VALUE_FEATURE_COUNT = CONSTANT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.DynamicExpression <em>Dynamic Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.DynamicExpression
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDynamicExpression()
	 * @generated
	 */
	int DYNAMIC_EXPRESSION = 49;

	/**
	 * The number of structural features of the '<em>Dynamic Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_EXPRESSION_FEATURE_COUNT = ANNOTATION_VALUE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.PathValueImpl <em>Path Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.PathValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getPathValue()
	 * @generated
	 */
	int PATH_VALUE = 40;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_VALUE__PATH = DYNAMIC_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Path Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_VALUE_FEATURE_COUNT = DYNAMIC_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.UsingImpl <em>Using</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.UsingImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getUsing()
	 * @generated
	 */
	int USING = 41;

	/**
	 * The feature id for the '<em><b>Alias</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USING__ALIAS = 0;

	/**
	 * The feature id for the '<em><b>Used Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USING__USED_NAMESPACE = 1;

	/**
	 * The number of structural features of the '<em>Using</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USING_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.RecordValueImpl <em>Record Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.RecordValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getRecordValue()
	 * @generated
	 */
	int RECORD_VALUE = 42;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_VALUE__TYPE = DYNAMIC_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property Values</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_VALUE__PROPERTY_VALUES = DYNAMIC_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Record Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_VALUE_FEATURE_COUNT = DYNAMIC_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.IRecordValueType <em>IRecord Value Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.IRecordValueType
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIRecordValueType()
	 * @generated
	 */
	int IRECORD_VALUE_TYPE = 43;

	/**
	 * The number of structural features of the '<em>IRecord Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECORD_VALUE_TYPE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl <em>Return Entity Type Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getReturnEntityTypeUsage()
	 * @generated
	 */
	int RETURN_ENTITY_TYPE_USAGE = 45;

	/**
	 * The feature id for the '<em><b>Entity Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ENTITY_TYPE_USAGE__ENTITY_SET = IFUNCTION_RETURN_TYPE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Entity Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ENTITY_TYPE_USAGE__ENTITY_TYPE = IFUNCTION_RETURN_TYPE_USAGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ENTITY_TYPE_USAGE__COLLECTION = IFUNCTION_RETURN_TYPE_USAGE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Return Entity Type Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ENTITY_TYPE_USAGE_FEATURE_COUNT = IFUNCTION_RETURN_TYPE_USAGE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.PropertyMappingImpl <em>Property Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.PropertyMappingImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getPropertyMapping()
	 * @generated
	 */
	int PROPERTY_MAPPING = 46;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_MAPPING__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_MAPPING__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Property Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_MAPPING_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.ValueCollectionImpl <em>Value Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.ValueCollectionImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getValueCollection()
	 * @generated
	 */
	int VALUE_COLLECTION = 47;

	/**
	 * The feature id for the '<em><b>Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_COLLECTION__VALUES = DYNAMIC_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Value Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_COLLECTION_FEATURE_COUNT = DYNAMIC_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.PropertyToValueMapImpl <em>Property To Value Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.PropertyToValueMapImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getPropertyToValueMap()
	 * @generated
	 */
	int PROPERTY_TO_VALUE_MAP = 48;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TO_VALUE_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TO_VALUE_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Property To Value Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TO_VALUE_MAP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.BooleanValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getBooleanValue()
	 * @generated
	 */
	int BOOLEAN_VALUE = 51;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE__VALUE = SIMPLE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_FEATURE_COUNT = SIMPLE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.StringValueImpl <em>String Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.StringValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getStringValue()
	 * @generated
	 */
	int STRING_VALUE = 52;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE__VALUE = SIMPLE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_FEATURE_COUNT = SIMPLE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.BinaryValueImpl <em>Binary Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.BinaryValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getBinaryValue()
	 * @generated
	 */
	int BINARY_VALUE = 53;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_VALUE__VALUE = SIMPLE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Binary Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_VALUE_FEATURE_COUNT = SIMPLE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.DateTimeValueImpl <em>Date Time Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.DateTimeValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDateTimeValue()
	 * @generated
	 */
	int DATE_TIME_VALUE = 54;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_TIME_VALUE__VALUE = SIMPLE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Date Time Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_TIME_VALUE_FEATURE_COUNT = SIMPLE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.NumberValue <em>Number Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.NumberValue
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getNumberValue()
	 * @generated
	 */
	int NUMBER_VALUE = 56;

	/**
	 * The number of structural features of the '<em>Number Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_VALUE_FEATURE_COUNT = SIMPLE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.DecimalValueImpl <em>Decimal Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.DecimalValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDecimalValue()
	 * @generated
	 */
	int DECIMAL_VALUE = 55;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECIMAL_VALUE__VALUE = NUMBER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Decimal Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECIMAL_VALUE_FEATURE_COUNT = NUMBER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.GuidValueImpl <em>Guid Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.GuidValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getGuidValue()
	 * @generated
	 */
	int GUID_VALUE = 57;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUID_VALUE__VALUE = SIMPLE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Guid Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUID_VALUE_FEATURE_COUNT = SIMPLE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.TimeValueImpl <em>Time Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.TimeValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getTimeValue()
	 * @generated
	 */
	int TIME_VALUE = 58;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_VALUE__VALUE = SIMPLE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Time Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_VALUE_FEATURE_COUNT = SIMPLE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.CollectableExpression <em>Collectable Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.CollectableExpression
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getCollectableExpression()
	 * @generated
	 */
	int COLLECTABLE_EXPRESSION = 59;

	/**
	 * The number of structural features of the '<em>Collectable Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTABLE_EXPRESSION_FEATURE_COUNT = ANNOTATION_VALUE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.FloatValue <em>Float Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.FloatValue
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getFloatValue()
	 * @generated
	 */
	int FLOAT_VALUE = 69;

	/**
	 * The number of structural features of the '<em>Float Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_FEATURE_COUNT = NUMBER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.SingleValueImpl <em>Single Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.SingleValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSingleValue()
	 * @generated
	 */
	int SINGLE_VALUE = 60;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_VALUE__VALUE = FLOAT_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Single Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_VALUE_FEATURE_COUNT = FLOAT_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.DoubleValueImpl <em>Double Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.DoubleValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDoubleValue()
	 * @generated
	 */
	int DOUBLE_VALUE = 61;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_VALUE__VALUE = FLOAT_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Double Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_VALUE_FEATURE_COUNT = FLOAT_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.IntegerValue <em>Integer Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.IntegerValue
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIntegerValue()
	 * @generated
	 */
	int INTEGER_VALUE = 68;

	/**
	 * The number of structural features of the '<em>Integer Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_FEATURE_COUNT = NUMBER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.SByteValueImpl <em>SByte Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.SByteValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSByteValue()
	 * @generated
	 */
	int SBYTE_VALUE = 62;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBYTE_VALUE__VALUE = INTEGER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SByte Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBYTE_VALUE_FEATURE_COUNT = INTEGER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.Int16ValueImpl <em>Int16 Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.Int16ValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getInt16Value()
	 * @generated
	 */
	int INT16_VALUE = 63;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT16_VALUE__VALUE = INTEGER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int16 Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT16_VALUE_FEATURE_COUNT = INTEGER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.Int32ValueImpl <em>Int32 Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.Int32ValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getInt32Value()
	 * @generated
	 */
	int INT32_VALUE = 64;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT32_VALUE__VALUE = INTEGER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int32 Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT32_VALUE_FEATURE_COUNT = INTEGER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.Int64ValueImpl <em>Int64 Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.Int64ValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getInt64Value()
	 * @generated
	 */
	int INT64_VALUE = 65;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT64_VALUE__VALUE = INTEGER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int64 Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT64_VALUE_FEATURE_COUNT = INTEGER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.DateTimeOffsetValueImpl <em>Date Time Offset Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.DateTimeOffsetValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDateTimeOffsetValue()
	 * @generated
	 */
	int DATE_TIME_OFFSET_VALUE = 66;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_TIME_OFFSET_VALUE__VALUE = SIMPLE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Date Time Offset Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_TIME_OFFSET_VALUE_FEATURE_COUNT = SIMPLE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.ByteValueImpl <em>Byte Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.ByteValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getByteValue()
	 * @generated
	 */
	int BYTE_VALUE = 67;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_VALUE__VALUE = INTEGER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Byte Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_VALUE_FEATURE_COUNT = INTEGER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.impl.EnumValueImpl <em>Enum Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.impl.EnumValueImpl
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEnumValue()
	 * @generated
	 */
	int ENUM_VALUE = 70;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__VALUE = DYNAMIC_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enum Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE_FEATURE_COUNT = DYNAMIC_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.EDMTypes <em>EDM Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.EDMTypes
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEDMTypes()
	 * @generated
	 */
	int EDM_TYPES = 71;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.Multiplicity <em>Multiplicity</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.Multiplicity
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getMultiplicity()
	 * @generated
	 */
	int MULTIPLICITY = 72;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.TypeTermTargets <em>Type Term Targets</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.TypeTermTargets
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getTypeTermTargets()
	 * @generated
	 */
	int TYPE_TERM_TARGETS = 73;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.ValueTermTargets <em>Value Term Targets</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.ValueTermTargets
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getValueTermTargets()
	 * @generated
	 */
	int VALUE_TERM_TARGETS = 74;

	/**
	 * The meta object id for the '{@link org.eclipse.ogee.model.odata.SchemaClassifier <em>Schema Classifier</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.SchemaClassifier
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSchemaClassifier()
	 * @generated
	 */
	int SCHEMA_CLASSIFIER = 75;


	/**
	 * The meta object id for the '<em>Guid Value Type</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.UUID
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getGuidValueType()
	 * @generated
	 */
	int GUID_VALUE_TYPE = 76;

	/**
	 * The meta object id for the '<em>Date Time Offset Value Type</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.util.DateTimeOffset
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDateTimeOffsetValueType()
	 * @generated
	 */
	int DATE_TIME_OFFSET_VALUE_TYPE = 77;

	/**
	 * The meta object id for the '<em>Date Time</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.ogee.model.odata.util.DateTime
	 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDateTime()
	 * @generated
	 */
	int DATE_TIME = 78;

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EntityType <em>Entity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Type</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityType
	 * @generated
	 */
	EClass getEntityType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EntityType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityType#getName()
	 * @see #getEntityType()
	 * @generated
	 */
	EAttribute getEntityType_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EntityType#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityType#getProperties()
	 * @see #getEntityType()
	 * @generated
	 */
	EReference getEntityType_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EntityType#getNavigationProperties <em>Navigation Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Navigation Properties</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityType#getNavigationProperties()
	 * @see #getEntityType()
	 * @generated
	 */
	EReference getEntityType_NavigationProperties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EntityType#getKeys <em>Keys</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Keys</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityType#getKeys()
	 * @see #getEntityType()
	 * @generated
	 */
	EReference getEntityType_Keys();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.EntityType#getBaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Type</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityType#getBaseType()
	 * @see #getEntityType()
	 * @generated
	 */
	EReference getEntityType_BaseType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EntityType#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityType#isAbstract()
	 * @see #getEntityType()
	 * @generated
	 */
	EAttribute getEntityType_Abstract();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EntityType#isMedia <em>Media</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Media</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityType#isMedia()
	 * @see #getEntityType()
	 * @generated
	 */
	EAttribute getEntityType_Media();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.ogee.model.odata.EntityType#getEntitySets <em>Entity Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Entity Sets</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityType#getEntitySets()
	 * @see #getEntityType()
	 * @generated
	 */
	EReference getEntityType_EntitySets();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.ogee.model.odata.EntityType#getDerivedTypes <em>Derived Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Derived Types</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityType#getDerivedTypes()
	 * @see #getEntityType()
	 * @generated
	 */
	EReference getEntityType_DerivedTypes();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see org.eclipse.ogee.model.odata.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Property#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.Property#getName()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.Property#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see org.eclipse.ogee.model.odata.Property#getType()
	 * @see #getProperty()
	 * @generated
	 */
	EReference getProperty_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Property#isNullable <em>Nullable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nullable</em>'.
	 * @see org.eclipse.ogee.model.odata.Property#isNullable()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Nullable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Property#isForEtag <em>For Etag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>For Etag</em>'.
	 * @see org.eclipse.ogee.model.odata.Property#isForEtag()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_ForEtag();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.ComplexType <em>Complex Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type</em>'.
	 * @see org.eclipse.ogee.model.odata.ComplexType
	 * @generated
	 */
	EClass getComplexType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.ComplexType#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see org.eclipse.ogee.model.odata.ComplexType#getProperties()
	 * @see #getComplexType()
	 * @generated
	 */
	EReference getComplexType_Properties();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.ComplexType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.ComplexType#getName()
	 * @see #getComplexType()
	 * @generated
	 */
	EAttribute getComplexType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.ComplexType#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see org.eclipse.ogee.model.odata.ComplexType#isAbstract()
	 * @see #getComplexType()
	 * @generated
	 */
	EAttribute getComplexType_Abstract();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.ComplexType#getBaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Type</em>'.
	 * @see org.eclipse.ogee.model.odata.ComplexType#getBaseType()
	 * @see #getComplexType()
	 * @generated
	 */
	EReference getComplexType_BaseType();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.ogee.model.odata.ComplexType#getDerivedTypes <em>Derived Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Derived Types</em>'.
	 * @see org.eclipse.ogee.model.odata.ComplexType#getDerivedTypes()
	 * @see #getComplexType()
	 * @generated
	 */
	EReference getComplexType_DerivedTypes();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Association <em>Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Association</em>'.
	 * @see org.eclipse.ogee.model.odata.Association
	 * @generated
	 */
	EClass getAssociation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Association#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.Association#getName()
	 * @see #getAssociation()
	 * @generated
	 */
	EAttribute getAssociation_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.Association#getEnds <em>Ends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ends</em>'.
	 * @see org.eclipse.ogee.model.odata.Association#getEnds()
	 * @see #getAssociation()
	 * @generated
	 */
	EReference getAssociation_Ends();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.Association#getReferentialConstraint <em>Referential Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Referential Constraint</em>'.
	 * @see org.eclipse.ogee.model.odata.Association#getReferentialConstraint()
	 * @see #getAssociation()
	 * @generated
	 */
	EReference getAssociation_ReferentialConstraint();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.ogee.model.odata.Association#getAssociationSets <em>Association Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Association Sets</em>'.
	 * @see org.eclipse.ogee.model.odata.Association#getAssociationSets()
	 * @see #getAssociation()
	 * @generated
	 */
	EReference getAssociation_AssociationSets();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.NavigationProperty <em>Navigation Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Navigation Property</em>'.
	 * @see org.eclipse.ogee.model.odata.NavigationProperty
	 * @generated
	 */
	EClass getNavigationProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.NavigationProperty#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.NavigationProperty#getName()
	 * @see #getNavigationProperty()
	 * @generated
	 */
	EAttribute getNavigationProperty_Name();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.NavigationProperty#getRelationship <em>Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Relationship</em>'.
	 * @see org.eclipse.ogee.model.odata.NavigationProperty#getRelationship()
	 * @see #getNavigationProperty()
	 * @generated
	 */
	EReference getNavigationProperty_Relationship();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.NavigationProperty#getFromRole <em>From Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From Role</em>'.
	 * @see org.eclipse.ogee.model.odata.NavigationProperty#getFromRole()
	 * @see #getNavigationProperty()
	 * @generated
	 */
	EReference getNavigationProperty_FromRole();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.NavigationProperty#getToRole <em>To Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To Role</em>'.
	 * @see org.eclipse.ogee.model.odata.NavigationProperty#getToRole()
	 * @see #getNavigationProperty()
	 * @generated
	 */
	EReference getNavigationProperty_ToRole();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.NavigationProperty#isContainsTarget <em>Contains Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Contains Target</em>'.
	 * @see org.eclipse.ogee.model.odata.NavigationProperty#isContainsTarget()
	 * @see #getNavigationProperty()
	 * @generated
	 */
	EAttribute getNavigationProperty_ContainsTarget();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.FunctionImport <em>Function Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Import</em>'.
	 * @see org.eclipse.ogee.model.odata.FunctionImport
	 * @generated
	 */
	EClass getFunctionImport();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.FunctionImport#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.FunctionImport#getName()
	 * @see #getFunctionImport()
	 * @generated
	 */
	EAttribute getFunctionImport_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.FunctionImport#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see org.eclipse.ogee.model.odata.FunctionImport#getParameters()
	 * @see #getFunctionImport()
	 * @generated
	 */
	EReference getFunctionImport_Parameters();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.FunctionImport#getBinding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Binding</em>'.
	 * @see org.eclipse.ogee.model.odata.FunctionImport#getBinding()
	 * @see #getFunctionImport()
	 * @generated
	 */
	EReference getFunctionImport_Binding();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.FunctionImport#isSideEffecting <em>Side Effecting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Side Effecting</em>'.
	 * @see org.eclipse.ogee.model.odata.FunctionImport#isSideEffecting()
	 * @see #getFunctionImport()
	 * @generated
	 */
	EAttribute getFunctionImport_SideEffecting();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.FunctionImport#getReturnType <em>Return Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Return Type</em>'.
	 * @see org.eclipse.ogee.model.odata.FunctionImport#getReturnType()
	 * @see #getFunctionImport()
	 * @generated
	 */
	EReference getFunctionImport_ReturnType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see org.eclipse.ogee.model.odata.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Parameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.Parameter#getName()
	 * @see #getParameter()
	 * @generated
	 */
	EAttribute getParameter_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.Parameter#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see org.eclipse.ogee.model.odata.Parameter#getType()
	 * @see #getParameter()
	 * @generated
	 */
	EReference getParameter_Type();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EntitySet <em>Entity Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Set</em>'.
	 * @see org.eclipse.ogee.model.odata.EntitySet
	 * @generated
	 */
	EClass getEntitySet();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.EntitySet#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.eclipse.ogee.model.odata.EntitySet#getType()
	 * @see #getEntitySet()
	 * @generated
	 */
	EReference getEntitySet_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EntitySet#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.EntitySet#getName()
	 * @see #getEntitySet()
	 * @generated
	 */
	EAttribute getEntitySet_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.AssociationSet <em>Association Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Association Set</em>'.
	 * @see org.eclipse.ogee.model.odata.AssociationSet
	 * @generated
	 */
	EClass getAssociationSet();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.AssociationSet#getAssociation <em>Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Association</em>'.
	 * @see org.eclipse.ogee.model.odata.AssociationSet#getAssociation()
	 * @see #getAssociationSet()
	 * @generated
	 */
	EReference getAssociationSet_Association();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.AssociationSet#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.AssociationSet#getName()
	 * @see #getAssociationSet()
	 * @generated
	 */
	EAttribute getAssociationSet_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.AssociationSet#getEnds <em>Ends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ends</em>'.
	 * @see org.eclipse.ogee.model.odata.AssociationSet#getEnds()
	 * @see #getAssociationSet()
	 * @generated
	 */
	EReference getAssociationSet_Ends();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role</em>'.
	 * @see org.eclipse.ogee.model.odata.Role
	 * @generated
	 */
	EClass getRole();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Role#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.Role#getName()
	 * @see #getRole()
	 * @generated
	 */
	EAttribute getRole_Name();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.Role#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.eclipse.ogee.model.odata.Role#getType()
	 * @see #getRole()
	 * @generated
	 */
	EReference getRole_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Role#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multiplicity</em>'.
	 * @see org.eclipse.ogee.model.odata.Role#getMultiplicity()
	 * @see #getRole()
	 * @generated
	 */
	EAttribute getRole_Multiplicity();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.AssociationSetEnd <em>Association Set End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Association Set End</em>'.
	 * @see org.eclipse.ogee.model.odata.AssociationSetEnd
	 * @generated
	 */
	EClass getAssociationSetEnd();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.AssociationSetEnd#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see org.eclipse.ogee.model.odata.AssociationSetEnd#getRole()
	 * @see #getAssociationSetEnd()
	 * @generated
	 */
	EReference getAssociationSetEnd_Role();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.AssociationSetEnd#getEntitySet <em>Entity Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Entity Set</em>'.
	 * @see org.eclipse.ogee.model.odata.AssociationSetEnd#getEntitySet()
	 * @see #getAssociationSetEnd()
	 * @generated
	 */
	EReference getAssociationSetEnd_EntitySet();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EDMX <em>EDMX</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EDMX</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMX
	 * @generated
	 */
	EClass getEDMX();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.EDMX#getDataService <em>Data Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Service</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMX#getDataService()
	 * @see #getEDMX()
	 * @generated
	 */
	EReference getEDMX_DataService();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EDMX#getAnnotationsReferences <em>Annotations References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotations References</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMX#getAnnotationsReferences()
	 * @see #getEDMX()
	 * @generated
	 */
	EReference getEDMX_AnnotationsReferences();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EDMX#getReferences <em>References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>References</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMX#getReferences()
	 * @see #getEDMX()
	 * @generated
	 */
	EReference getEDMX_References();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EDMX#getURI <em>URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>URI</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMX#getURI()
	 * @see #getEDMX()
	 * @generated
	 */
	EAttribute getEDMX_URI();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.ReferentialConstraint <em>Referential Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Referential Constraint</em>'.
	 * @see org.eclipse.ogee.model.odata.ReferentialConstraint
	 * @generated
	 */
	EClass getReferentialConstraint();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.ReferentialConstraint#getPrincipal <em>Principal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Principal</em>'.
	 * @see org.eclipse.ogee.model.odata.ReferentialConstraint#getPrincipal()
	 * @see #getReferentialConstraint()
	 * @generated
	 */
	EReference getReferentialConstraint_Principal();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.ReferentialConstraint#getDependent <em>Dependent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dependent</em>'.
	 * @see org.eclipse.ogee.model.odata.ReferentialConstraint#getDependent()
	 * @see #getReferentialConstraint()
	 * @generated
	 */
	EReference getReferentialConstraint_Dependent();

	/**
	 * Returns the meta object for the map '{@link org.eclipse.ogee.model.odata.ReferentialConstraint#getKeyMappings <em>Key Mappings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Key Mappings</em>'.
	 * @see org.eclipse.ogee.model.odata.ReferentialConstraint#getKeyMappings()
	 * @see #getReferentialConstraint()
	 * @generated
	 */
	EReference getReferentialConstraint_KeyMappings();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.IPropertyTypeUsage <em>IProperty Type Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IProperty Type Usage</em>'.
	 * @see org.eclipse.ogee.model.odata.IPropertyTypeUsage
	 * @generated
	 */
	EClass getIPropertyTypeUsage();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.SimpleType <em>Simple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Type</em>'.
	 * @see org.eclipse.ogee.model.odata.SimpleType
	 * @generated
	 */
	EClass getSimpleType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.SimpleType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.ogee.model.odata.SimpleType#getType()
	 * @see #getSimpleType()
	 * @generated
	 */
	EAttribute getSimpleType_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.SimpleType#getMaxLength <em>Max Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Length</em>'.
	 * @see org.eclipse.ogee.model.odata.SimpleType#getMaxLength()
	 * @see #getSimpleType()
	 * @generated
	 */
	EAttribute getSimpleType_MaxLength();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.SimpleType#getPrecision <em>Precision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Precision</em>'.
	 * @see org.eclipse.ogee.model.odata.SimpleType#getPrecision()
	 * @see #getSimpleType()
	 * @generated
	 */
	EAttribute getSimpleType_Precision();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.SimpleType#getScale <em>Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scale</em>'.
	 * @see org.eclipse.ogee.model.odata.SimpleType#getScale()
	 * @see #getSimpleType()
	 * @generated
	 */
	EAttribute getSimpleType_Scale();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.SimpleType#getDefaultValue <em>Default Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Value</em>'.
	 * @see org.eclipse.ogee.model.odata.SimpleType#getDefaultValue()
	 * @see #getSimpleType()
	 * @generated
	 */
	EReference getSimpleType_DefaultValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.SimpleType#getFixedLength <em>Fixed Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fixed Length</em>'.
	 * @see org.eclipse.ogee.model.odata.SimpleType#getFixedLength()
	 * @see #getSimpleType()
	 * @generated
	 */
	EAttribute getSimpleType_FixedLength();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.IParameterTypeUsage <em>IParameter Type Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IParameter Type Usage</em>'.
	 * @see org.eclipse.ogee.model.odata.IParameterTypeUsage
	 * @generated
	 */
	EClass getIParameterTypeUsage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.IParameterTypeUsage#isCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collection</em>'.
	 * @see org.eclipse.ogee.model.odata.IParameterTypeUsage#isCollection()
	 * @see #getIParameterTypeUsage()
	 * @generated
	 */
	EAttribute getIParameterTypeUsage_Collection();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Binding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binding</em>'.
	 * @see org.eclipse.ogee.model.odata.Binding
	 * @generated
	 */
	EClass getBinding();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Binding#isCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collection</em>'.
	 * @see org.eclipse.ogee.model.odata.Binding#isCollection()
	 * @see #getBinding()
	 * @generated
	 */
	EAttribute getBinding_Collection();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.Binding#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.eclipse.ogee.model.odata.Binding#getType()
	 * @see #getBinding()
	 * @generated
	 */
	EReference getBinding_Type();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.ValueTerm <em>Value Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Term</em>'.
	 * @see org.eclipse.ogee.model.odata.ValueTerm
	 * @generated
	 */
	EClass getValueTerm();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.ValueTerm#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.ValueTerm#getName()
	 * @see #getValueTerm()
	 * @generated
	 */
	EAttribute getValueTerm_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.ValueTerm#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see org.eclipse.ogee.model.odata.ValueTerm#getType()
	 * @see #getValueTerm()
	 * @generated
	 */
	EReference getValueTerm_Type();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.ogee.model.odata.ValueTerm#getValueAnnotations <em>Value Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value Annotations</em>'.
	 * @see org.eclipse.ogee.model.odata.ValueTerm#getValueAnnotations()
	 * @see #getValueTerm()
	 * @generated
	 */
	EReference getValueTerm_ValueAnnotations();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.ValueAnnotation <em>Value Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Annotation</em>'.
	 * @see org.eclipse.ogee.model.odata.ValueAnnotation
	 * @generated
	 */
	EClass getValueAnnotation();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.ValueAnnotation#getTerm <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Term</em>'.
	 * @see org.eclipse.ogee.model.odata.ValueAnnotation#getTerm()
	 * @see #getValueAnnotation()
	 * @generated
	 */
	EReference getValueAnnotation_Term();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.ValueAnnotation#getAnnotationValue <em>Annotation Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Value</em>'.
	 * @see org.eclipse.ogee.model.odata.ValueAnnotation#getAnnotationValue()
	 * @see #getValueAnnotation()
	 * @generated
	 */
	EReference getValueAnnotation_AnnotationValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.TypeAnnotation <em>Type Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Annotation</em>'.
	 * @see org.eclipse.ogee.model.odata.TypeAnnotation
	 * @generated
	 */
	EClass getTypeAnnotation();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.TypeAnnotation#getTerm <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Term</em>'.
	 * @see org.eclipse.ogee.model.odata.TypeAnnotation#getTerm()
	 * @see #getTypeAnnotation()
	 * @generated
	 */
	EReference getTypeAnnotation_Term();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.TypeAnnotation#getAnnotationValues <em>Annotation Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotation Values</em>'.
	 * @see org.eclipse.ogee.model.odata.TypeAnnotation#getAnnotationValues()
	 * @see #getTypeAnnotation()
	 * @generated
	 */
	EReference getTypeAnnotation_AnnotationValues();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.IAnnotationTarget <em>IAnnotation Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IAnnotation Target</em>'.
	 * @see org.eclipse.ogee.model.odata.IAnnotationTarget
	 * @generated
	 */
	EClass getIAnnotationTarget();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.ogee.model.odata.IAnnotationTarget#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Annotations</em>'.
	 * @see org.eclipse.ogee.model.odata.IAnnotationTarget#getAnnotations()
	 * @see #getIAnnotationTarget()
	 * @generated
	 */
	EReference getIAnnotationTarget_Annotations();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EntityContainer <em>Entity Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Container</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityContainer
	 * @generated
	 */
	EClass getEntityContainer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EntityContainer#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityContainer#getName()
	 * @see #getEntityContainer()
	 * @generated
	 */
	EAttribute getEntityContainer_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EntityContainer#getEntitySets <em>Entity Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entity Sets</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityContainer#getEntitySets()
	 * @see #getEntityContainer()
	 * @generated
	 */
	EReference getEntityContainer_EntitySets();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EntityContainer#isDefault <em>Default</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityContainer#isDefault()
	 * @see #getEntityContainer()
	 * @generated
	 */
	EAttribute getEntityContainer_Default();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EntityContainer#getAssociationSets <em>Association Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Association Sets</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityContainer#getAssociationSets()
	 * @see #getEntityContainer()
	 * @generated
	 */
	EReference getEntityContainer_AssociationSets();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EntityContainer#getFunctionImports <em>Function Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Function Imports</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityContainer#getFunctionImports()
	 * @see #getEntityContainer()
	 * @generated
	 */
	EReference getEntityContainer_FunctionImports();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EnumType <em>Enum Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Type</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumType
	 * @generated
	 */
	EClass getEnumType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EnumType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumType#getName()
	 * @see #getEnumType()
	 * @generated
	 */
	EAttribute getEnumType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EnumType#isFlags <em>Flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Flags</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumType#isFlags()
	 * @see #getEnumType()
	 * @generated
	 */
	EAttribute getEnumType_Flags();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EnumType#getMembers <em>Members</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Members</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumType#getMembers()
	 * @see #getEnumType()
	 * @generated
	 */
	EReference getEnumType_Members();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EnumType#getUnderlyingType <em>Underlying Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Underlying Type</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumType#getUnderlyingType()
	 * @see #getEnumType()
	 * @generated
	 */
	EAttribute getEnumType_UnderlyingType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EnumMember <em>Enum Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Member</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumMember
	 * @generated
	 */
	EClass getEnumMember();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EnumMember#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumMember#getName()
	 * @see #getEnumMember()
	 * @generated
	 */
	EAttribute getEnumMember_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.EnumMember#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumMember#getValue()
	 * @see #getEnumMember()
	 * @generated
	 */
	EReference getEnumMember_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.IDocumentable <em>IDocumentable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IDocumentable</em>'.
	 * @see org.eclipse.ogee.model.odata.IDocumentable
	 * @generated
	 */
	EClass getIDocumentable();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.IDocumentable#getDocumentation <em>Documentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Documentation</em>'.
	 * @see org.eclipse.ogee.model.odata.IDocumentable#getDocumentation()
	 * @see #getIDocumentable()
	 * @generated
	 */
	EReference getIDocumentable_Documentation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Documentation <em>Documentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Documentation</em>'.
	 * @see org.eclipse.ogee.model.odata.Documentation
	 * @generated
	 */
	EClass getDocumentation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Documentation#getSummary <em>Summary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Summary</em>'.
	 * @see org.eclipse.ogee.model.odata.Documentation#getSummary()
	 * @see #getDocumentation()
	 * @generated
	 */
	EAttribute getDocumentation_Summary();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Documentation#getLongDescription <em>Long Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Long Description</em>'.
	 * @see org.eclipse.ogee.model.odata.Documentation#getLongDescription()
	 * @see #getDocumentation()
	 * @generated
	 */
	EAttribute getDocumentation_LongDescription();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.ITypeTerm <em>IType Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IType Term</em>'.
	 * @see org.eclipse.ogee.model.odata.ITypeTerm
	 * @generated
	 */
	EClass getITypeTerm();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see org.eclipse.ogee.model.odata.Annotation
	 * @generated
	 */
	EClass getAnnotation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Annotation#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Qualifier</em>'.
	 * @see org.eclipse.ogee.model.odata.Annotation#getQualifier()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_Qualifier();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.Annotation#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.ogee.model.odata.Annotation#getTarget()
	 * @see #getAnnotation()
	 * @generated
	 */
	EReference getAnnotation_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EDMXSet <em>EDMX Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EDMX Set</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXSet
	 * @generated
	 */
	EClass getEDMXSet();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EDMXSet#getImportSource <em>Import Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import Source</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXSet#getImportSource()
	 * @see #getEDMXSet()
	 * @generated
	 */
	EAttribute getEDMXSet_ImportSource();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.EDMXSet#getImportDate <em>Import Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import Date</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXSet#getImportDate()
	 * @see #getEDMXSet()
	 * @generated
	 */
	EAttribute getEDMXSet_ImportDate();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.EDMXSet#getMainEDMX <em>Main EDMX</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Main EDMX</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXSet#getMainEDMX()
	 * @see #getEDMXSet()
	 * @generated
	 */
	EReference getEDMXSet_MainEDMX();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.ogee.model.odata.EDMXSet#getEdmx <em>Edmx</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Edmx</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXSet#getEdmx()
	 * @see #getEDMXSet()
	 * @generated
	 */
	EAttribute getEDMXSet_Edmx();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EDMXSet#getFullScopeEDMX <em>Full Scope EDMX</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Full Scope EDMX</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXSet#getFullScopeEDMX()
	 * @see #getEDMXSet()
	 * @generated
	 */
	EReference getEDMXSet_FullScopeEDMX();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EDMXSet#getAnnotationsScopeEDMX <em>Annotations Scope EDMX</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotations Scope EDMX</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXSet#getAnnotationsScopeEDMX()
	 * @see #getEDMXSet()
	 * @generated
	 */
	EReference getEDMXSet_AnnotationsScopeEDMX();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EDMXSet#getSchemata <em>Schemata</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Schemata</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXSet#getSchemata()
	 * @see #getEDMXSet()
	 * @generated
	 */
	EReference getEDMXSet_Schemata();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EntityTypeUsage <em>Entity Type Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Type Usage</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityTypeUsage
	 * @generated
	 */
	EClass getEntityTypeUsage();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.EntityTypeUsage#getEntityType <em>Entity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Entity Type</em>'.
	 * @see org.eclipse.ogee.model.odata.EntityTypeUsage#getEntityType()
	 * @see #getEntityTypeUsage()
	 * @generated
	 */
	EReference getEntityTypeUsage_EntityType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.ComplexTypeUsage <em>Complex Type Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Usage</em>'.
	 * @see org.eclipse.ogee.model.odata.ComplexTypeUsage
	 * @generated
	 */
	EClass getComplexTypeUsage();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.ComplexTypeUsage#getComplexType <em>Complex Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Complex Type</em>'.
	 * @see org.eclipse.ogee.model.odata.ComplexTypeUsage#getComplexType()
	 * @see #getComplexTypeUsage()
	 * @generated
	 */
	EReference getComplexTypeUsage_ComplexType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EnumTypeUsage <em>Enum Type Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Type Usage</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumTypeUsage
	 * @generated
	 */
	EClass getEnumTypeUsage();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.EnumTypeUsage#getEnumType <em>Enum Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Enum Type</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumTypeUsage#getEnumType()
	 * @see #getEnumTypeUsage()
	 * @generated
	 */
	EReference getEnumTypeUsage_EnumType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.SimpleTypeUsage <em>Simple Type Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Type Usage</em>'.
	 * @see org.eclipse.ogee.model.odata.SimpleTypeUsage
	 * @generated
	 */
	EClass getSimpleTypeUsage();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.ogee.model.odata.SimpleTypeUsage#getSimpleType <em>Simple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Simple Type</em>'.
	 * @see org.eclipse.ogee.model.odata.SimpleTypeUsage#getSimpleType()
	 * @see #getSimpleTypeUsage()
	 * @generated
	 */
	EReference getSimpleTypeUsage_SimpleType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage <em>IFunction Return Type Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IFunction Return Type Usage</em>'.
	 * @see org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage
	 * @generated
	 */
	EClass getIFunctionReturnTypeUsage();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EDMXReference <em>EDMX Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EDMX Reference</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXReference
	 * @generated
	 */
	EClass getEDMXReference();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.EDMXReference#getReferencedEDMX <em>Referenced EDMX</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced EDMX</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXReference#getReferencedEDMX()
	 * @see #getEDMXReference()
	 * @generated
	 */
	EReference getEDMXReference_ReferencedEDMX();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EDMXAnnotationsReference <em>EDMX Annotations Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EDMX Annotations Reference</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXAnnotationsReference
	 * @generated
	 */
	EClass getEDMXAnnotationsReference();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.EDMXAnnotationsReference#getIncludeRestrictions <em>Include Restrictions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Include Restrictions</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXAnnotationsReference#getIncludeRestrictions()
	 * @see #getEDMXAnnotationsReference()
	 * @generated
	 */
	EReference getEDMXAnnotationsReference_IncludeRestrictions();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.EDMXAnnotationsReference#getReferencedEDMX <em>Referenced EDMX</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced EDMX</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMXAnnotationsReference#getReferencedEDMX()
	 * @see #getEDMXAnnotationsReference()
	 * @generated
	 */
	EReference getEDMXAnnotationsReference_ReferencedEDMX();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Schema <em>Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schema</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema
	 * @generated
	 */
	EClass getSchema();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Schema#getNamespace <em>Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Namespace</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getNamespace()
	 * @see #getSchema()
	 * @generated
	 */
	EAttribute getSchema_Namespace();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.Schema#getContainers <em>Containers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Containers</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getContainers()
	 * @see #getSchema()
	 * @generated
	 */
	EReference getSchema_Containers();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.Schema#getEntityTypes <em>Entity Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entity Types</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getEntityTypes()
	 * @see #getSchema()
	 * @generated
	 */
	EReference getSchema_EntityTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.Schema#getAssociations <em>Associations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Associations</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getAssociations()
	 * @see #getSchema()
	 * @generated
	 */
	EReference getSchema_Associations();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.Schema#getEnumTypes <em>Enum Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Enum Types</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getEnumTypes()
	 * @see #getSchema()
	 * @generated
	 */
	EReference getSchema_EnumTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.Schema#getComplexTypes <em>Complex Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Complex Types</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getComplexTypes()
	 * @see #getSchema()
	 * @generated
	 */
	EReference getSchema_ComplexTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.Schema#getUsings <em>Usings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Usings</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getUsings()
	 * @see #getSchema()
	 * @generated
	 */
	EReference getSchema_Usings();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.Schema#getValueTerms <em>Value Terms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value Terms</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getValueTerms()
	 * @see #getSchema()
	 * @generated
	 */
	EReference getSchema_ValueTerms();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.Schema#getValueAnnotations <em>Value Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value Annotations</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getValueAnnotations()
	 * @see #getSchema()
	 * @generated
	 */
	EReference getSchema_ValueAnnotations();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.Schema#getTypeAnnotations <em>Type Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Type Annotations</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getTypeAnnotations()
	 * @see #getSchema()
	 * @generated
	 */
	EReference getSchema_TypeAnnotations();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Schema#getAlias <em>Alias</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Alias</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getAlias()
	 * @see #getSchema()
	 * @generated
	 */
	EAttribute getSchema_Alias();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.ogee.model.odata.Schema#getClassifiers <em>Classifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Classifiers</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getClassifiers()
	 * @see #getSchema()
	 * @generated
	 */
	EAttribute getSchema_Classifiers();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.ogee.model.odata.Schema#getDataServices <em>Data Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Data Services</em>'.
	 * @see org.eclipse.ogee.model.odata.Schema#getDataServices()
	 * @see #getSchema()
	 * @generated
	 */
	EReference getSchema_DataServices();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.DataService <em>Data Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Service</em>'.
	 * @see org.eclipse.ogee.model.odata.DataService
	 * @generated
	 */
	EClass getDataService();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.DataService#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.eclipse.ogee.model.odata.DataService#getVersion()
	 * @see #getDataService()
	 * @generated
	 */
	EAttribute getDataService_Version();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.ogee.model.odata.DataService#getSchemata <em>Schemata</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Schemata</em>'.
	 * @see org.eclipse.ogee.model.odata.DataService#getSchemata()
	 * @see #getDataService()
	 * @generated
	 */
	EReference getDataService_Schemata();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.IncludeRestriction <em>Include Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Include Restriction</em>'.
	 * @see org.eclipse.ogee.model.odata.IncludeRestriction
	 * @generated
	 */
	EClass getIncludeRestriction();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.IncludeRestriction#getTermNamespace <em>Term Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Term Namespace</em>'.
	 * @see org.eclipse.ogee.model.odata.IncludeRestriction#getTermNamespace()
	 * @see #getIncludeRestriction()
	 * @generated
	 */
	EAttribute getIncludeRestriction_TermNamespace();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.IncludeRestriction#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Qualifier</em>'.
	 * @see org.eclipse.ogee.model.odata.IncludeRestriction#getQualifier()
	 * @see #getIncludeRestriction()
	 * @generated
	 */
	EAttribute getIncludeRestriction_Qualifier();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.SimpleValue <em>Simple Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Value</em>'.
	 * @see org.eclipse.ogee.model.odata.SimpleValue
	 * @generated
	 */
	EClass getSimpleValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.PathValue <em>Path Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Value</em>'.
	 * @see org.eclipse.ogee.model.odata.PathValue
	 * @generated
	 */
	EClass getPathValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.PathValue#getPath <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Path</em>'.
	 * @see org.eclipse.ogee.model.odata.PathValue#getPath()
	 * @see #getPathValue()
	 * @generated
	 */
	EAttribute getPathValue_Path();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Using <em>Using</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Using</em>'.
	 * @see org.eclipse.ogee.model.odata.Using
	 * @generated
	 */
	EClass getUsing();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Using#getAlias <em>Alias</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Alias</em>'.
	 * @see org.eclipse.ogee.model.odata.Using#getAlias()
	 * @see #getUsing()
	 * @generated
	 */
	EAttribute getUsing_Alias();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.Using#getUsedNamespace <em>Used Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Used Namespace</em>'.
	 * @see org.eclipse.ogee.model.odata.Using#getUsedNamespace()
	 * @see #getUsing()
	 * @generated
	 */
	EReference getUsing_UsedNamespace();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.RecordValue <em>Record Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record Value</em>'.
	 * @see org.eclipse.ogee.model.odata.RecordValue
	 * @generated
	 */
	EClass getRecordValue();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.RecordValue#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.eclipse.ogee.model.odata.RecordValue#getType()
	 * @see #getRecordValue()
	 * @generated
	 */
	EReference getRecordValue_Type();

	/**
	 * Returns the meta object for the map '{@link org.eclipse.ogee.model.odata.RecordValue#getPropertyValues <em>Property Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Property Values</em>'.
	 * @see org.eclipse.ogee.model.odata.RecordValue#getPropertyValues()
	 * @see #getRecordValue()
	 * @generated
	 */
	EReference getRecordValue_PropertyValues();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.IRecordValueType <em>IRecord Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRecord Value Type</em>'.
	 * @see org.eclipse.ogee.model.odata.IRecordValueType
	 * @generated
	 */
	EClass getIRecordValueType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.AnnotationValue <em>Annotation Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation Value</em>'.
	 * @see org.eclipse.ogee.model.odata.AnnotationValue
	 * @generated
	 */
	EClass getAnnotationValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.ReturnEntityTypeUsage <em>Return Entity Type Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Return Entity Type Usage</em>'.
	 * @see org.eclipse.ogee.model.odata.ReturnEntityTypeUsage
	 * @generated
	 */
	EClass getReturnEntityTypeUsage();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#getEntitySet <em>Entity Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Entity Set</em>'.
	 * @see org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#getEntitySet()
	 * @see #getReturnEntityTypeUsage()
	 * @generated
	 */
	EReference getReturnEntityTypeUsage_EntitySet();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#getEntityType <em>Entity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Entity Type</em>'.
	 * @see org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#getEntityType()
	 * @see #getReturnEntityTypeUsage()
	 * @generated
	 */
	EReference getReturnEntityTypeUsage_EntityType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#isCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collection</em>'.
	 * @see org.eclipse.ogee.model.odata.ReturnEntityTypeUsage#isCollection()
	 * @see #getReturnEntityTypeUsage()
	 * @generated
	 */
	EAttribute getReturnEntityTypeUsage_Collection();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Property Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Mapping</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.ogee.model.odata.Property" keyRequired="true"
	 *        valueType="org.eclipse.ogee.model.odata.Property" valueRequired="true"
	 * @generated
	 */
	EClass getPropertyMapping();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPropertyMapping()
	 * @generated
	 */
	EReference getPropertyMapping_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPropertyMapping()
	 * @generated
	 */
	EReference getPropertyMapping_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.ValueCollection <em>Value Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Collection</em>'.
	 * @see org.eclipse.ogee.model.odata.ValueCollection
	 * @generated
	 */
	EClass getValueCollection();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.ogee.model.odata.ValueCollection#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Values</em>'.
	 * @see org.eclipse.ogee.model.odata.ValueCollection#getValues()
	 * @see #getValueCollection()
	 * @generated
	 */
	EReference getValueCollection_Values();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Property To Value Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property To Value Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.ogee.model.odata.Property" keyRequired="true"
	 *        valueType="org.eclipse.ogee.model.odata.AnnotationValue" valueContainment="true" valueRequired="true"
	 * @generated
	 */
	EClass getPropertyToValueMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPropertyToValueMap()
	 * @generated
	 */
	EReference getPropertyToValueMap_Key();

	/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getPropertyToValueMap()
	 * @generated
	 */
	EReference getPropertyToValueMap_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.DynamicExpression <em>Dynamic Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dynamic Expression</em>'.
	 * @see org.eclipse.ogee.model.odata.DynamicExpression
	 * @generated
	 */
	EClass getDynamicExpression();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.ConstantExpression <em>Constant Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Expression</em>'.
	 * @see org.eclipse.ogee.model.odata.ConstantExpression
	 * @generated
	 */
	EClass getConstantExpression();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.BooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Value</em>'.
	 * @see org.eclipse.ogee.model.odata.BooleanValue
	 * @generated
	 */
	EClass getBooleanValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.BooleanValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.BooleanValue#getValue()
	 * @see #getBooleanValue()
	 * @generated
	 */
	EAttribute getBooleanValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.StringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Value</em>'.
	 * @see org.eclipse.ogee.model.odata.StringValue
	 * @generated
	 */
	EClass getStringValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.StringValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.StringValue#getValue()
	 * @see #getStringValue()
	 * @generated
	 */
	EAttribute getStringValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.BinaryValue <em>Binary Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Value</em>'.
	 * @see org.eclipse.ogee.model.odata.BinaryValue
	 * @generated
	 */
	EClass getBinaryValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.BinaryValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.BinaryValue#getValue()
	 * @see #getBinaryValue()
	 * @generated
	 */
	EAttribute getBinaryValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.DateTimeValue <em>Date Time Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Date Time Value</em>'.
	 * @see org.eclipse.ogee.model.odata.DateTimeValue
	 * @generated
	 */
	EClass getDateTimeValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.DateTimeValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.DateTimeValue#getValue()
	 * @see #getDateTimeValue()
	 * @generated
	 */
	EAttribute getDateTimeValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.DecimalValue <em>Decimal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decimal Value</em>'.
	 * @see org.eclipse.ogee.model.odata.DecimalValue
	 * @generated
	 */
	EClass getDecimalValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.DecimalValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.DecimalValue#getValue()
	 * @see #getDecimalValue()
	 * @generated
	 */
	EAttribute getDecimalValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.NumberValue <em>Number Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Value</em>'.
	 * @see org.eclipse.ogee.model.odata.NumberValue
	 * @generated
	 */
	EClass getNumberValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.GuidValue <em>Guid Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guid Value</em>'.
	 * @see org.eclipse.ogee.model.odata.GuidValue
	 * @generated
	 */
	EClass getGuidValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.GuidValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.GuidValue#getValue()
	 * @see #getGuidValue()
	 * @generated
	 */
	EAttribute getGuidValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.TimeValue <em>Time Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Value</em>'.
	 * @see org.eclipse.ogee.model.odata.TimeValue
	 * @generated
	 */
	EClass getTimeValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.TimeValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.TimeValue#getValue()
	 * @see #getTimeValue()
	 * @generated
	 */
	EAttribute getTimeValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.CollectableExpression <em>Collectable Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collectable Expression</em>'.
	 * @see org.eclipse.ogee.model.odata.CollectableExpression
	 * @generated
	 */
	EClass getCollectableExpression();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.SingleValue <em>Single Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Value</em>'.
	 * @see org.eclipse.ogee.model.odata.SingleValue
	 * @generated
	 */
	EClass getSingleValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.SingleValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.SingleValue#getValue()
	 * @see #getSingleValue()
	 * @generated
	 */
	EAttribute getSingleValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.DoubleValue <em>Double Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double Value</em>'.
	 * @see org.eclipse.ogee.model.odata.DoubleValue
	 * @generated
	 */
	EClass getDoubleValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.DoubleValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.DoubleValue#getValue()
	 * @see #getDoubleValue()
	 * @generated
	 */
	EAttribute getDoubleValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.SByteValue <em>SByte Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SByte Value</em>'.
	 * @see org.eclipse.ogee.model.odata.SByteValue
	 * @generated
	 */
	EClass getSByteValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.SByteValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.SByteValue#getValue()
	 * @see #getSByteValue()
	 * @generated
	 */
	EAttribute getSByteValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Int16Value <em>Int16 Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int16 Value</em>'.
	 * @see org.eclipse.ogee.model.odata.Int16Value
	 * @generated
	 */
	EClass getInt16Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Int16Value#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.Int16Value#getValue()
	 * @see #getInt16Value()
	 * @generated
	 */
	EAttribute getInt16Value_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Int32Value <em>Int32 Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int32 Value</em>'.
	 * @see org.eclipse.ogee.model.odata.Int32Value
	 * @generated
	 */
	EClass getInt32Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Int32Value#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.Int32Value#getValue()
	 * @see #getInt32Value()
	 * @generated
	 */
	EAttribute getInt32Value_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.Int64Value <em>Int64 Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int64 Value</em>'.
	 * @see org.eclipse.ogee.model.odata.Int64Value
	 * @generated
	 */
	EClass getInt64Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.Int64Value#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.Int64Value#getValue()
	 * @see #getInt64Value()
	 * @generated
	 */
	EAttribute getInt64Value_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.DateTimeOffsetValue <em>Date Time Offset Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Date Time Offset Value</em>'.
	 * @see org.eclipse.ogee.model.odata.DateTimeOffsetValue
	 * @generated
	 */
	EClass getDateTimeOffsetValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.DateTimeOffsetValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.DateTimeOffsetValue#getValue()
	 * @see #getDateTimeOffsetValue()
	 * @generated
	 */
	EAttribute getDateTimeOffsetValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.ByteValue <em>Byte Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Byte Value</em>'.
	 * @see org.eclipse.ogee.model.odata.ByteValue
	 * @generated
	 */
	EClass getByteValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.ogee.model.odata.ByteValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.ByteValue#getValue()
	 * @see #getByteValue()
	 * @generated
	 */
	EAttribute getByteValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.IntegerValue <em>Integer Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Value</em>'.
	 * @see org.eclipse.ogee.model.odata.IntegerValue
	 * @generated
	 */
	EClass getIntegerValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.FloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Value</em>'.
	 * @see org.eclipse.ogee.model.odata.FloatValue
	 * @generated
	 */
	EClass getFloatValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.ogee.model.odata.EnumValue <em>Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Value</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumValue
	 * @generated
	 */
	EClass getEnumValue();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.ogee.model.odata.EnumValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see org.eclipse.ogee.model.odata.EnumValue#getValue()
	 * @see #getEnumValue()
	 * @generated
	 */
	EReference getEnumValue_Value();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.ogee.model.odata.EDMTypes <em>EDM Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EDM Types</em>'.
	 * @see org.eclipse.ogee.model.odata.EDMTypes
	 * @generated
	 */
	EEnum getEDMTypes();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.ogee.model.odata.Multiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Multiplicity</em>'.
	 * @see org.eclipse.ogee.model.odata.Multiplicity
	 * @generated
	 */
	EEnum getMultiplicity();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.ogee.model.odata.TypeTermTargets <em>Type Term Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Type Term Targets</em>'.
	 * @see org.eclipse.ogee.model.odata.TypeTermTargets
	 * @generated
	 */
	EEnum getTypeTermTargets();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.ogee.model.odata.ValueTermTargets <em>Value Term Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Value Term Targets</em>'.
	 * @see org.eclipse.ogee.model.odata.ValueTermTargets
	 * @generated
	 */
	EEnum getValueTermTargets();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.ogee.model.odata.SchemaClassifier <em>Schema Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Schema Classifier</em>'.
	 * @see org.eclipse.ogee.model.odata.SchemaClassifier
	 * @generated
	 */
	EEnum getSchemaClassifier();

	/**
	 * Returns the meta object for data type '{@link java.util.UUID <em>Guid Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Guid Value Type</em>'.
	 * @see java.util.UUID
	 * @model instanceClass="java.util.UUID"
	 *        extendedMetaData="pattern='' enumeration=''"
	 * @generated
	 */
	EDataType getGuidValueType();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.ogee.model.odata.util.DateTimeOffset <em>Date Time Offset Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Date Time Offset Value Type</em>'.
	 * @see org.eclipse.ogee.model.odata.util.DateTimeOffset
	 * @model instanceClass="org.eclipse.ogee.model.odata.util.DateTimeOffset"
	 * @generated
	 */
	EDataType getDateTimeOffsetValueType();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.ogee.model.odata.util.DateTime <em>Date Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Date Time</em>'.
	 * @see org.eclipse.ogee.model.odata.util.DateTime
	 * @model instanceClass="org.eclipse.ogee.model.odata.util.DateTime"
	 * @generated
	 */
	EDataType getDateTime();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OdataFactory getOdataFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EntityTypeImpl <em>Entity Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EntityTypeImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEntityType()
		 * @generated
		 */
		EClass ENTITY_TYPE = eINSTANCE.getEntityType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_TYPE__NAME = eINSTANCE.getEntityType_Name();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_TYPE__PROPERTIES = eINSTANCE.getEntityType_Properties();

		/**
		 * The meta object literal for the '<em><b>Navigation Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_TYPE__NAVIGATION_PROPERTIES = eINSTANCE.getEntityType_NavigationProperties();

		/**
		 * The meta object literal for the '<em><b>Keys</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_TYPE__KEYS = eINSTANCE.getEntityType_Keys();

		/**
		 * The meta object literal for the '<em><b>Base Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_TYPE__BASE_TYPE = eINSTANCE.getEntityType_BaseType();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_TYPE__ABSTRACT = eINSTANCE.getEntityType_Abstract();

		/**
		 * The meta object literal for the '<em><b>Media</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_TYPE__MEDIA = eINSTANCE.getEntityType_Media();

		/**
		 * The meta object literal for the '<em><b>Entity Sets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_TYPE__ENTITY_SETS = eINSTANCE.getEntityType_EntitySets();

		/**
		 * The meta object literal for the '<em><b>Derived Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_TYPE__DERIVED_TYPES = eINSTANCE.getEntityType_DerivedTypes();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.PropertyImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__NAME = eINSTANCE.getProperty_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY__TYPE = eINSTANCE.getProperty_Type();

		/**
		 * The meta object literal for the '<em><b>Nullable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__NULLABLE = eINSTANCE.getProperty_Nullable();

		/**
		 * The meta object literal for the '<em><b>For Etag</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__FOR_ETAG = eINSTANCE.getProperty_ForEtag();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.ComplexTypeImpl <em>Complex Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.ComplexTypeImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getComplexType()
		 * @generated
		 */
		EClass COMPLEX_TYPE = eINSTANCE.getComplexType();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE__PROPERTIES = eINSTANCE.getComplexType_Properties();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE__NAME = eINSTANCE.getComplexType_Name();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE__ABSTRACT = eINSTANCE.getComplexType_Abstract();

		/**
		 * The meta object literal for the '<em><b>Base Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE__BASE_TYPE = eINSTANCE.getComplexType_BaseType();

		/**
		 * The meta object literal for the '<em><b>Derived Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE__DERIVED_TYPES = eINSTANCE.getComplexType_DerivedTypes();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.AssociationImpl <em>Association</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.AssociationImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getAssociation()
		 * @generated
		 */
		EClass ASSOCIATION = eINSTANCE.getAssociation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSOCIATION__NAME = eINSTANCE.getAssociation_Name();

		/**
		 * The meta object literal for the '<em><b>Ends</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION__ENDS = eINSTANCE.getAssociation_Ends();

		/**
		 * The meta object literal for the '<em><b>Referential Constraint</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION__REFERENTIAL_CONSTRAINT = eINSTANCE.getAssociation_ReferentialConstraint();

		/**
		 * The meta object literal for the '<em><b>Association Sets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION__ASSOCIATION_SETS = eINSTANCE.getAssociation_AssociationSets();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl <em>Navigation Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.NavigationPropertyImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getNavigationProperty()
		 * @generated
		 */
		EClass NAVIGATION_PROPERTY = eINSTANCE.getNavigationProperty();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAVIGATION_PROPERTY__NAME = eINSTANCE.getNavigationProperty_Name();

		/**
		 * The meta object literal for the '<em><b>Relationship</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAVIGATION_PROPERTY__RELATIONSHIP = eINSTANCE.getNavigationProperty_Relationship();

		/**
		 * The meta object literal for the '<em><b>From Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAVIGATION_PROPERTY__FROM_ROLE = eINSTANCE.getNavigationProperty_FromRole();

		/**
		 * The meta object literal for the '<em><b>To Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAVIGATION_PROPERTY__TO_ROLE = eINSTANCE.getNavigationProperty_ToRole();

		/**
		 * The meta object literal for the '<em><b>Contains Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAVIGATION_PROPERTY__CONTAINS_TARGET = eINSTANCE.getNavigationProperty_ContainsTarget();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.FunctionImportImpl <em>Function Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.FunctionImportImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getFunctionImport()
		 * @generated
		 */
		EClass FUNCTION_IMPORT = eINSTANCE.getFunctionImport();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_IMPORT__NAME = eINSTANCE.getFunctionImport_Name();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_IMPORT__PARAMETERS = eINSTANCE.getFunctionImport_Parameters();

		/**
		 * The meta object literal for the '<em><b>Binding</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_IMPORT__BINDING = eINSTANCE.getFunctionImport_Binding();

		/**
		 * The meta object literal for the '<em><b>Side Effecting</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_IMPORT__SIDE_EFFECTING = eINSTANCE.getFunctionImport_SideEffecting();

		/**
		 * The meta object literal for the '<em><b>Return Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_IMPORT__RETURN_TYPE = eINSTANCE.getFunctionImport_ReturnType();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.ParameterImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER__NAME = eINSTANCE.getParameter_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER__TYPE = eINSTANCE.getParameter_Type();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EntitySetImpl <em>Entity Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EntitySetImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEntitySet()
		 * @generated
		 */
		EClass ENTITY_SET = eINSTANCE.getEntitySet();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_SET__TYPE = eINSTANCE.getEntitySet_Type();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_SET__NAME = eINSTANCE.getEntitySet_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.AssociationSetImpl <em>Association Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.AssociationSetImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getAssociationSet()
		 * @generated
		 */
		EClass ASSOCIATION_SET = eINSTANCE.getAssociationSet();

		/**
		 * The meta object literal for the '<em><b>Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_SET__ASSOCIATION = eINSTANCE.getAssociationSet_Association();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSOCIATION_SET__NAME = eINSTANCE.getAssociationSet_Name();

		/**
		 * The meta object literal for the '<em><b>Ends</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_SET__ENDS = eINSTANCE.getAssociationSet_Ends();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.RoleImpl <em>Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.RoleImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getRole()
		 * @generated
		 */
		EClass ROLE = eINSTANCE.getRole();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE__NAME = eINSTANCE.getRole_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE__TYPE = eINSTANCE.getRole_Type();

		/**
		 * The meta object literal for the '<em><b>Multiplicity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE__MULTIPLICITY = eINSTANCE.getRole_Multiplicity();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.AssociationSetEndImpl <em>Association Set End</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.AssociationSetEndImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getAssociationSetEnd()
		 * @generated
		 */
		EClass ASSOCIATION_SET_END = eINSTANCE.getAssociationSetEnd();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_SET_END__ROLE = eINSTANCE.getAssociationSetEnd_Role();

		/**
		 * The meta object literal for the '<em><b>Entity Set</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_SET_END__ENTITY_SET = eINSTANCE.getAssociationSetEnd_EntitySet();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EDMXImpl <em>EDMX</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EDMXImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEDMX()
		 * @generated
		 */
		EClass EDMX = eINSTANCE.getEDMX();

		/**
		 * The meta object literal for the '<em><b>Data Service</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDMX__DATA_SERVICE = eINSTANCE.getEDMX_DataService();

		/**
		 * The meta object literal for the '<em><b>Annotations References</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDMX__ANNOTATIONS_REFERENCES = eINSTANCE.getEDMX_AnnotationsReferences();

		/**
		 * The meta object literal for the '<em><b>References</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDMX__REFERENCES = eINSTANCE.getEDMX_References();

		/**
		 * The meta object literal for the '<em><b>URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDMX__URI = eINSTANCE.getEDMX_URI();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.ReferentialConstraintImpl <em>Referential Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.ReferentialConstraintImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getReferentialConstraint()
		 * @generated
		 */
		EClass REFERENTIAL_CONSTRAINT = eINSTANCE.getReferentialConstraint();

		/**
		 * The meta object literal for the '<em><b>Principal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_CONSTRAINT__PRINCIPAL = eINSTANCE.getReferentialConstraint_Principal();

		/**
		 * The meta object literal for the '<em><b>Dependent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_CONSTRAINT__DEPENDENT = eINSTANCE.getReferentialConstraint_Dependent();

		/**
		 * The meta object literal for the '<em><b>Key Mappings</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_CONSTRAINT__KEY_MAPPINGS = eINSTANCE.getReferentialConstraint_KeyMappings();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.IPropertyTypeUsage <em>IProperty Type Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.IPropertyTypeUsage
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIPropertyTypeUsage()
		 * @generated
		 */
		EClass IPROPERTY_TYPE_USAGE = eINSTANCE.getIPropertyTypeUsage();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.SimpleTypeImpl <em>Simple Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.SimpleTypeImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSimpleType()
		 * @generated
		 */
		EClass SIMPLE_TYPE = eINSTANCE.getSimpleType();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_TYPE__TYPE = eINSTANCE.getSimpleType_Type();

		/**
		 * The meta object literal for the '<em><b>Max Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_TYPE__MAX_LENGTH = eINSTANCE.getSimpleType_MaxLength();

		/**
		 * The meta object literal for the '<em><b>Precision</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_TYPE__PRECISION = eINSTANCE.getSimpleType_Precision();

		/**
		 * The meta object literal for the '<em><b>Scale</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_TYPE__SCALE = eINSTANCE.getSimpleType_Scale();

		/**
		 * The meta object literal for the '<em><b>Default Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_TYPE__DEFAULT_VALUE = eINSTANCE.getSimpleType_DefaultValue();

		/**
		 * The meta object literal for the '<em><b>Fixed Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_TYPE__FIXED_LENGTH = eINSTANCE.getSimpleType_FixedLength();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.IParameterTypeUsage <em>IParameter Type Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.IParameterTypeUsage
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIParameterTypeUsage()
		 * @generated
		 */
		EClass IPARAMETER_TYPE_USAGE = eINSTANCE.getIParameterTypeUsage();

		/**
		 * The meta object literal for the '<em><b>Collection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPARAMETER_TYPE_USAGE__COLLECTION = eINSTANCE.getIParameterTypeUsage_Collection();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.BindingImpl <em>Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.BindingImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getBinding()
		 * @generated
		 */
		EClass BINDING = eINSTANCE.getBinding();

		/**
		 * The meta object literal for the '<em><b>Collection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINDING__COLLECTION = eINSTANCE.getBinding_Collection();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING__TYPE = eINSTANCE.getBinding_Type();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.ValueTermImpl <em>Value Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.ValueTermImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getValueTerm()
		 * @generated
		 */
		EClass VALUE_TERM = eINSTANCE.getValueTerm();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_TERM__NAME = eINSTANCE.getValueTerm_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_TERM__TYPE = eINSTANCE.getValueTerm_Type();

		/**
		 * The meta object literal for the '<em><b>Value Annotations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_TERM__VALUE_ANNOTATIONS = eINSTANCE.getValueTerm_ValueAnnotations();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.ValueAnnotationImpl <em>Value Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.ValueAnnotationImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getValueAnnotation()
		 * @generated
		 */
		EClass VALUE_ANNOTATION = eINSTANCE.getValueAnnotation();

		/**
		 * The meta object literal for the '<em><b>Term</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_ANNOTATION__TERM = eINSTANCE.getValueAnnotation_Term();

		/**
		 * The meta object literal for the '<em><b>Annotation Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_ANNOTATION__ANNOTATION_VALUE = eINSTANCE.getValueAnnotation_AnnotationValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.TypeAnnotationImpl <em>Type Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.TypeAnnotationImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getTypeAnnotation()
		 * @generated
		 */
		EClass TYPE_ANNOTATION = eINSTANCE.getTypeAnnotation();

		/**
		 * The meta object literal for the '<em><b>Term</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_ANNOTATION__TERM = eINSTANCE.getTypeAnnotation_Term();

		/**
		 * The meta object literal for the '<em><b>Annotation Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_ANNOTATION__ANNOTATION_VALUES = eINSTANCE.getTypeAnnotation_AnnotationValues();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.IAnnotationTarget <em>IAnnotation Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.IAnnotationTarget
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIAnnotationTarget()
		 * @generated
		 */
		EClass IANNOTATION_TARGET = eINSTANCE.getIAnnotationTarget();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IANNOTATION_TARGET__ANNOTATIONS = eINSTANCE.getIAnnotationTarget_Annotations();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EntityContainerImpl <em>Entity Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EntityContainerImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEntityContainer()
		 * @generated
		 */
		EClass ENTITY_CONTAINER = eINSTANCE.getEntityContainer();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_CONTAINER__NAME = eINSTANCE.getEntityContainer_Name();

		/**
		 * The meta object literal for the '<em><b>Entity Sets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_CONTAINER__ENTITY_SETS = eINSTANCE.getEntityContainer_EntitySets();

		/**
		 * The meta object literal for the '<em><b>Default</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_CONTAINER__DEFAULT = eINSTANCE.getEntityContainer_Default();

		/**
		 * The meta object literal for the '<em><b>Association Sets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_CONTAINER__ASSOCIATION_SETS = eINSTANCE.getEntityContainer_AssociationSets();

		/**
		 * The meta object literal for the '<em><b>Function Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_CONTAINER__FUNCTION_IMPORTS = eINSTANCE.getEntityContainer_FunctionImports();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EnumTypeImpl <em>Enum Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EnumTypeImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEnumType()
		 * @generated
		 */
		EClass ENUM_TYPE = eINSTANCE.getEnumType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENUM_TYPE__NAME = eINSTANCE.getEnumType_Name();

		/**
		 * The meta object literal for the '<em><b>Flags</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENUM_TYPE__FLAGS = eINSTANCE.getEnumType_Flags();

		/**
		 * The meta object literal for the '<em><b>Members</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_TYPE__MEMBERS = eINSTANCE.getEnumType_Members();

		/**
		 * The meta object literal for the '<em><b>Underlying Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENUM_TYPE__UNDERLYING_TYPE = eINSTANCE.getEnumType_UnderlyingType();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EnumMemberImpl <em>Enum Member</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EnumMemberImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEnumMember()
		 * @generated
		 */
		EClass ENUM_MEMBER = eINSTANCE.getEnumMember();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENUM_MEMBER__NAME = eINSTANCE.getEnumMember_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_MEMBER__VALUE = eINSTANCE.getEnumMember_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.IDocumentable <em>IDocumentable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.IDocumentable
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIDocumentable()
		 * @generated
		 */
		EClass IDOCUMENTABLE = eINSTANCE.getIDocumentable();

		/**
		 * The meta object literal for the '<em><b>Documentation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDOCUMENTABLE__DOCUMENTATION = eINSTANCE.getIDocumentable_Documentation();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.DocumentationImpl <em>Documentation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.DocumentationImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDocumentation()
		 * @generated
		 */
		EClass DOCUMENTATION = eINSTANCE.getDocumentation();

		/**
		 * The meta object literal for the '<em><b>Summary</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENTATION__SUMMARY = eINSTANCE.getDocumentation_Summary();

		/**
		 * The meta object literal for the '<em><b>Long Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENTATION__LONG_DESCRIPTION = eINSTANCE.getDocumentation_LongDescription();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.ITypeTerm <em>IType Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.ITypeTerm
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getITypeTerm()
		 * @generated
		 */
		EClass ITYPE_TERM = eINSTANCE.getITypeTerm();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.AnnotationImpl <em>Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.AnnotationImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getAnnotation()
		 * @generated
		 */
		EClass ANNOTATION = eINSTANCE.getAnnotation();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION__QUALIFIER = eINSTANCE.getAnnotation_Qualifier();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATION__TARGET = eINSTANCE.getAnnotation_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EDMXSetImpl <em>EDMX Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EDMXSetImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEDMXSet()
		 * @generated
		 */
		EClass EDMX_SET = eINSTANCE.getEDMXSet();

		/**
		 * The meta object literal for the '<em><b>Import Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDMX_SET__IMPORT_SOURCE = eINSTANCE.getEDMXSet_ImportSource();

		/**
		 * The meta object literal for the '<em><b>Import Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDMX_SET__IMPORT_DATE = eINSTANCE.getEDMXSet_ImportDate();

		/**
		 * The meta object literal for the '<em><b>Main EDMX</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDMX_SET__MAIN_EDMX = eINSTANCE.getEDMXSet_MainEDMX();

		/**
		 * The meta object literal for the '<em><b>Edmx</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDMX_SET__EDMX = eINSTANCE.getEDMXSet_Edmx();

		/**
		 * The meta object literal for the '<em><b>Full Scope EDMX</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDMX_SET__FULL_SCOPE_EDMX = eINSTANCE.getEDMXSet_FullScopeEDMX();

		/**
		 * The meta object literal for the '<em><b>Annotations Scope EDMX</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDMX_SET__ANNOTATIONS_SCOPE_EDMX = eINSTANCE.getEDMXSet_AnnotationsScopeEDMX();

		/**
		 * The meta object literal for the '<em><b>Schemata</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDMX_SET__SCHEMATA = eINSTANCE.getEDMXSet_Schemata();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EntityTypeUsageImpl <em>Entity Type Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EntityTypeUsageImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEntityTypeUsage()
		 * @generated
		 */
		EClass ENTITY_TYPE_USAGE = eINSTANCE.getEntityTypeUsage();

		/**
		 * The meta object literal for the '<em><b>Entity Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_TYPE_USAGE__ENTITY_TYPE = eINSTANCE.getEntityTypeUsage_EntityType();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl <em>Complex Type Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.ComplexTypeUsageImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getComplexTypeUsage()
		 * @generated
		 */
		EClass COMPLEX_TYPE_USAGE = eINSTANCE.getComplexTypeUsage();

		/**
		 * The meta object literal for the '<em><b>Complex Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_USAGE__COMPLEX_TYPE = eINSTANCE.getComplexTypeUsage_ComplexType();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl <em>Enum Type Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EnumTypeUsageImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEnumTypeUsage()
		 * @generated
		 */
		EClass ENUM_TYPE_USAGE = eINSTANCE.getEnumTypeUsage();

		/**
		 * The meta object literal for the '<em><b>Enum Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_TYPE_USAGE__ENUM_TYPE = eINSTANCE.getEnumTypeUsage_EnumType();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.SimpleTypeUsageImpl <em>Simple Type Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.SimpleTypeUsageImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSimpleTypeUsage()
		 * @generated
		 */
		EClass SIMPLE_TYPE_USAGE = eINSTANCE.getSimpleTypeUsage();

		/**
		 * The meta object literal for the '<em><b>Simple Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_TYPE_USAGE__SIMPLE_TYPE = eINSTANCE.getSimpleTypeUsage_SimpleType();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage <em>IFunction Return Type Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIFunctionReturnTypeUsage()
		 * @generated
		 */
		EClass IFUNCTION_RETURN_TYPE_USAGE = eINSTANCE.getIFunctionReturnTypeUsage();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EDMXReferenceImpl <em>EDMX Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EDMXReferenceImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEDMXReference()
		 * @generated
		 */
		EClass EDMX_REFERENCE = eINSTANCE.getEDMXReference();

		/**
		 * The meta object literal for the '<em><b>Referenced EDMX</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDMX_REFERENCE__REFERENCED_EDMX = eINSTANCE.getEDMXReference_ReferencedEDMX();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EDMXAnnotationsReferenceImpl <em>EDMX Annotations Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EDMXAnnotationsReferenceImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEDMXAnnotationsReference()
		 * @generated
		 */
		EClass EDMX_ANNOTATIONS_REFERENCE = eINSTANCE.getEDMXAnnotationsReference();

		/**
		 * The meta object literal for the '<em><b>Include Restrictions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDMX_ANNOTATIONS_REFERENCE__INCLUDE_RESTRICTIONS = eINSTANCE.getEDMXAnnotationsReference_IncludeRestrictions();

		/**
		 * The meta object literal for the '<em><b>Referenced EDMX</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDMX_ANNOTATIONS_REFERENCE__REFERENCED_EDMX = eINSTANCE.getEDMXAnnotationsReference_ReferencedEDMX();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.SchemaImpl <em>Schema</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.SchemaImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSchema()
		 * @generated
		 */
		EClass SCHEMA = eINSTANCE.getSchema();

		/**
		 * The meta object literal for the '<em><b>Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEMA__NAMESPACE = eINSTANCE.getSchema_Namespace();

		/**
		 * The meta object literal for the '<em><b>Containers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEMA__CONTAINERS = eINSTANCE.getSchema_Containers();

		/**
		 * The meta object literal for the '<em><b>Entity Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEMA__ENTITY_TYPES = eINSTANCE.getSchema_EntityTypes();

		/**
		 * The meta object literal for the '<em><b>Associations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEMA__ASSOCIATIONS = eINSTANCE.getSchema_Associations();

		/**
		 * The meta object literal for the '<em><b>Enum Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEMA__ENUM_TYPES = eINSTANCE.getSchema_EnumTypes();

		/**
		 * The meta object literal for the '<em><b>Complex Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEMA__COMPLEX_TYPES = eINSTANCE.getSchema_ComplexTypes();

		/**
		 * The meta object literal for the '<em><b>Usings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEMA__USINGS = eINSTANCE.getSchema_Usings();

		/**
		 * The meta object literal for the '<em><b>Value Terms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEMA__VALUE_TERMS = eINSTANCE.getSchema_ValueTerms();

		/**
		 * The meta object literal for the '<em><b>Value Annotations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEMA__VALUE_ANNOTATIONS = eINSTANCE.getSchema_ValueAnnotations();

		/**
		 * The meta object literal for the '<em><b>Type Annotations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEMA__TYPE_ANNOTATIONS = eINSTANCE.getSchema_TypeAnnotations();

		/**
		 * The meta object literal for the '<em><b>Alias</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEMA__ALIAS = eINSTANCE.getSchema_Alias();

		/**
		 * The meta object literal for the '<em><b>Classifiers</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEMA__CLASSIFIERS = eINSTANCE.getSchema_Classifiers();

		/**
		 * The meta object literal for the '<em><b>Data Services</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEMA__DATA_SERVICES = eINSTANCE.getSchema_DataServices();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.DataServiceImpl <em>Data Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.DataServiceImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDataService()
		 * @generated
		 */
		EClass DATA_SERVICE = eINSTANCE.getDataService();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SERVICE__VERSION = eINSTANCE.getDataService_Version();

		/**
		 * The meta object literal for the '<em><b>Schemata</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SERVICE__SCHEMATA = eINSTANCE.getDataService_Schemata();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.IncludeRestrictionImpl <em>Include Restriction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.IncludeRestrictionImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIncludeRestriction()
		 * @generated
		 */
		EClass INCLUDE_RESTRICTION = eINSTANCE.getIncludeRestriction();

		/**
		 * The meta object literal for the '<em><b>Term Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE_RESTRICTION__TERM_NAMESPACE = eINSTANCE.getIncludeRestriction_TermNamespace();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE_RESTRICTION__QUALIFIER = eINSTANCE.getIncludeRestriction_Qualifier();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.SimpleValue <em>Simple Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.SimpleValue
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSimpleValue()
		 * @generated
		 */
		EClass SIMPLE_VALUE = eINSTANCE.getSimpleValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.PathValueImpl <em>Path Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.PathValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getPathValue()
		 * @generated
		 */
		EClass PATH_VALUE = eINSTANCE.getPathValue();

		/**
		 * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PATH_VALUE__PATH = eINSTANCE.getPathValue_Path();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.UsingImpl <em>Using</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.UsingImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getUsing()
		 * @generated
		 */
		EClass USING = eINSTANCE.getUsing();

		/**
		 * The meta object literal for the '<em><b>Alias</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USING__ALIAS = eINSTANCE.getUsing_Alias();

		/**
		 * The meta object literal for the '<em><b>Used Namespace</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USING__USED_NAMESPACE = eINSTANCE.getUsing_UsedNamespace();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.RecordValueImpl <em>Record Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.RecordValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getRecordValue()
		 * @generated
		 */
		EClass RECORD_VALUE = eINSTANCE.getRecordValue();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECORD_VALUE__TYPE = eINSTANCE.getRecordValue_Type();

		/**
		 * The meta object literal for the '<em><b>Property Values</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECORD_VALUE__PROPERTY_VALUES = eINSTANCE.getRecordValue_PropertyValues();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.IRecordValueType <em>IRecord Value Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.IRecordValueType
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIRecordValueType()
		 * @generated
		 */
		EClass IRECORD_VALUE_TYPE = eINSTANCE.getIRecordValueType();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.AnnotationValue <em>Annotation Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.AnnotationValue
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getAnnotationValue()
		 * @generated
		 */
		EClass ANNOTATION_VALUE = eINSTANCE.getAnnotationValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl <em>Return Entity Type Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.ReturnEntityTypeUsageImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getReturnEntityTypeUsage()
		 * @generated
		 */
		EClass RETURN_ENTITY_TYPE_USAGE = eINSTANCE.getReturnEntityTypeUsage();

		/**
		 * The meta object literal for the '<em><b>Entity Set</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETURN_ENTITY_TYPE_USAGE__ENTITY_SET = eINSTANCE.getReturnEntityTypeUsage_EntitySet();

		/**
		 * The meta object literal for the '<em><b>Entity Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETURN_ENTITY_TYPE_USAGE__ENTITY_TYPE = eINSTANCE.getReturnEntityTypeUsage_EntityType();

		/**
		 * The meta object literal for the '<em><b>Collection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RETURN_ENTITY_TYPE_USAGE__COLLECTION = eINSTANCE.getReturnEntityTypeUsage_Collection();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.PropertyMappingImpl <em>Property Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.PropertyMappingImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getPropertyMapping()
		 * @generated
		 */
		EClass PROPERTY_MAPPING = eINSTANCE.getPropertyMapping();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_MAPPING__KEY = eINSTANCE.getPropertyMapping_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_MAPPING__VALUE = eINSTANCE.getPropertyMapping_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.ValueCollectionImpl <em>Value Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.ValueCollectionImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getValueCollection()
		 * @generated
		 */
		EClass VALUE_COLLECTION = eINSTANCE.getValueCollection();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_COLLECTION__VALUES = eINSTANCE.getValueCollection_Values();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.PropertyToValueMapImpl <em>Property To Value Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.PropertyToValueMapImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getPropertyToValueMap()
		 * @generated
		 */
		EClass PROPERTY_TO_VALUE_MAP = eINSTANCE.getPropertyToValueMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TO_VALUE_MAP__KEY = eINSTANCE.getPropertyToValueMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TO_VALUE_MAP__VALUE = eINSTANCE.getPropertyToValueMap_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.DynamicExpression <em>Dynamic Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.DynamicExpression
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDynamicExpression()
		 * @generated
		 */
		EClass DYNAMIC_EXPRESSION = eINSTANCE.getDynamicExpression();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.ConstantExpression <em>Constant Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.ConstantExpression
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getConstantExpression()
		 * @generated
		 */
		EClass CONSTANT_EXPRESSION = eINSTANCE.getConstantExpression();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.BooleanValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getBooleanValue()
		 * @generated
		 */
		EClass BOOLEAN_VALUE = eINSTANCE.getBooleanValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_VALUE__VALUE = eINSTANCE.getBooleanValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.StringValueImpl <em>String Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.StringValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getStringValue()
		 * @generated
		 */
		EClass STRING_VALUE = eINSTANCE.getStringValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_VALUE__VALUE = eINSTANCE.getStringValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.BinaryValueImpl <em>Binary Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.BinaryValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getBinaryValue()
		 * @generated
		 */
		EClass BINARY_VALUE = eINSTANCE.getBinaryValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_VALUE__VALUE = eINSTANCE.getBinaryValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.DateTimeValueImpl <em>Date Time Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.DateTimeValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDateTimeValue()
		 * @generated
		 */
		EClass DATE_TIME_VALUE = eINSTANCE.getDateTimeValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATE_TIME_VALUE__VALUE = eINSTANCE.getDateTimeValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.DecimalValueImpl <em>Decimal Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.DecimalValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDecimalValue()
		 * @generated
		 */
		EClass DECIMAL_VALUE = eINSTANCE.getDecimalValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECIMAL_VALUE__VALUE = eINSTANCE.getDecimalValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.NumberValue <em>Number Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.NumberValue
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getNumberValue()
		 * @generated
		 */
		EClass NUMBER_VALUE = eINSTANCE.getNumberValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.GuidValueImpl <em>Guid Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.GuidValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getGuidValue()
		 * @generated
		 */
		EClass GUID_VALUE = eINSTANCE.getGuidValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUID_VALUE__VALUE = eINSTANCE.getGuidValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.TimeValueImpl <em>Time Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.TimeValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getTimeValue()
		 * @generated
		 */
		EClass TIME_VALUE = eINSTANCE.getTimeValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_VALUE__VALUE = eINSTANCE.getTimeValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.CollectableExpression <em>Collectable Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.CollectableExpression
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getCollectableExpression()
		 * @generated
		 */
		EClass COLLECTABLE_EXPRESSION = eINSTANCE.getCollectableExpression();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.SingleValueImpl <em>Single Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.SingleValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSingleValue()
		 * @generated
		 */
		EClass SINGLE_VALUE = eINSTANCE.getSingleValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SINGLE_VALUE__VALUE = eINSTANCE.getSingleValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.DoubleValueImpl <em>Double Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.DoubleValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDoubleValue()
		 * @generated
		 */
		EClass DOUBLE_VALUE = eINSTANCE.getDoubleValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOUBLE_VALUE__VALUE = eINSTANCE.getDoubleValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.SByteValueImpl <em>SByte Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.SByteValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSByteValue()
		 * @generated
		 */
		EClass SBYTE_VALUE = eINSTANCE.getSByteValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SBYTE_VALUE__VALUE = eINSTANCE.getSByteValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.Int16ValueImpl <em>Int16 Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.Int16ValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getInt16Value()
		 * @generated
		 */
		EClass INT16_VALUE = eINSTANCE.getInt16Value();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT16_VALUE__VALUE = eINSTANCE.getInt16Value_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.Int32ValueImpl <em>Int32 Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.Int32ValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getInt32Value()
		 * @generated
		 */
		EClass INT32_VALUE = eINSTANCE.getInt32Value();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT32_VALUE__VALUE = eINSTANCE.getInt32Value_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.Int64ValueImpl <em>Int64 Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.Int64ValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getInt64Value()
		 * @generated
		 */
		EClass INT64_VALUE = eINSTANCE.getInt64Value();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT64_VALUE__VALUE = eINSTANCE.getInt64Value_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.DateTimeOffsetValueImpl <em>Date Time Offset Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.DateTimeOffsetValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDateTimeOffsetValue()
		 * @generated
		 */
		EClass DATE_TIME_OFFSET_VALUE = eINSTANCE.getDateTimeOffsetValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATE_TIME_OFFSET_VALUE__VALUE = eINSTANCE.getDateTimeOffsetValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.ByteValueImpl <em>Byte Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.ByteValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getByteValue()
		 * @generated
		 */
		EClass BYTE_VALUE = eINSTANCE.getByteValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BYTE_VALUE__VALUE = eINSTANCE.getByteValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.IntegerValue <em>Integer Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.IntegerValue
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getIntegerValue()
		 * @generated
		 */
		EClass INTEGER_VALUE = eINSTANCE.getIntegerValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.FloatValue <em>Float Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.FloatValue
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getFloatValue()
		 * @generated
		 */
		EClass FLOAT_VALUE = eINSTANCE.getFloatValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.impl.EnumValueImpl <em>Enum Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.impl.EnumValueImpl
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEnumValue()
		 * @generated
		 */
		EClass ENUM_VALUE = eINSTANCE.getEnumValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_VALUE__VALUE = eINSTANCE.getEnumValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.EDMTypes <em>EDM Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.EDMTypes
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getEDMTypes()
		 * @generated
		 */
		EEnum EDM_TYPES = eINSTANCE.getEDMTypes();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.Multiplicity <em>Multiplicity</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.Multiplicity
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getMultiplicity()
		 * @generated
		 */
		EEnum MULTIPLICITY = eINSTANCE.getMultiplicity();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.TypeTermTargets <em>Type Term Targets</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.TypeTermTargets
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getTypeTermTargets()
		 * @generated
		 */
		EEnum TYPE_TERM_TARGETS = eINSTANCE.getTypeTermTargets();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.ValueTermTargets <em>Value Term Targets</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.ValueTermTargets
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getValueTermTargets()
		 * @generated
		 */
		EEnum VALUE_TERM_TARGETS = eINSTANCE.getValueTermTargets();

		/**
		 * The meta object literal for the '{@link org.eclipse.ogee.model.odata.SchemaClassifier <em>Schema Classifier</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.SchemaClassifier
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getSchemaClassifier()
		 * @generated
		 */
		EEnum SCHEMA_CLASSIFIER = eINSTANCE.getSchemaClassifier();

		/**
		 * The meta object literal for the '<em>Guid Value Type</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.UUID
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getGuidValueType()
		 * @generated
		 */
		EDataType GUID_VALUE_TYPE = eINSTANCE.getGuidValueType();

		/**
		 * The meta object literal for the '<em>Date Time Offset Value Type</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.util.DateTimeOffset
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDateTimeOffsetValueType()
		 * @generated
		 */
		EDataType DATE_TIME_OFFSET_VALUE_TYPE = eINSTANCE.getDateTimeOffsetValueType();

		/**
		 * The meta object literal for the '<em>Date Time</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.ogee.model.odata.util.DateTime
		 * @see org.eclipse.ogee.model.odata.impl.OdataPackageImpl#getDateTime()
		 * @generated
		 */
		EDataType DATE_TIME = eINSTANCE.getDateTime();

	}

} //OdataPackage
