/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.TypeAnnotation#getTerm <em>Term</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.TypeAnnotation#getAnnotationValues <em>Annotation Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getTypeAnnotation()
 * @model
 * @generated
 */
public interface TypeAnnotation extends Annotation, IDocumentable {
	/**
	 * Returns the value of the '<em><b>Term</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Term</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Term</em>' reference.
	 * @see #setTerm(ITypeTerm)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getTypeAnnotation_Term()
	 * @model required="true"
	 * @generated
	 */
	ITypeTerm getTerm();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.TypeAnnotation#getTerm <em>Term</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Term</em>' reference.
	 * @see #getTerm()
	 * @generated
	 */
	void setTerm(ITypeTerm value);

	/**
	 * Returns the value of the '<em><b>Annotation Values</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.RecordValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Values</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getTypeAnnotation_AnnotationValues()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<RecordValue> getAnnotationValues();

} // TypeAnnotation
