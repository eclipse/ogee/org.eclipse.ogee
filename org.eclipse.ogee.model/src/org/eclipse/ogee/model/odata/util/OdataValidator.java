/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.odata.util;

import java.util.Map;
import java.util.UUID;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.validation.model.EvaluationMode;
import org.eclipse.emf.validation.model.IConstraintStatus;
import org.eclipse.emf.validation.service.IBatchValidator;
import org.eclipse.emf.validation.service.ModelValidationService;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.BinaryValue;
import org.eclipse.ogee.model.odata.Binding;
import org.eclipse.ogee.model.odata.BooleanValue;
import org.eclipse.ogee.model.odata.ByteValue;
import org.eclipse.ogee.model.odata.CollectableExpression;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.ConstantExpression;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.DateTimeOffsetValue;
import org.eclipse.ogee.model.odata.DateTimeValue;
import org.eclipse.ogee.model.odata.DecimalValue;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.DoubleValue;
import org.eclipse.ogee.model.odata.DynamicExpression;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXAnnotationsReference;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.EnumValue;
import org.eclipse.ogee.model.odata.FloatValue;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.GuidValue;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IDocumentable;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.IParameterTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.IRecordValueType;
import org.eclipse.ogee.model.odata.ITypeTerm;
import org.eclipse.ogee.model.odata.IncludeRestriction;
import org.eclipse.ogee.model.odata.Int16Value;
import org.eclipse.ogee.model.odata.Int32Value;
import org.eclipse.ogee.model.odata.Int64Value;
import org.eclipse.ogee.model.odata.IntegerValue;
import org.eclipse.ogee.model.odata.Multiplicity;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.NumberValue;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.PathValue;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.RecordValue;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.SByteValue;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.model.odata.SingleValue;
import org.eclipse.ogee.model.odata.StringValue;
import org.eclipse.ogee.model.odata.TimeValue;
import org.eclipse.ogee.model.odata.TypeAnnotation;
import org.eclipse.ogee.model.odata.TypeTermTargets;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.model.odata.ValueTermTargets;


/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.ogee.model.odata.OdataPackage
 * @generated
 */
public class OdataValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final OdataValidator INSTANCE = new OdataValidator();

	private final IBatchValidator batchValidator;
	
	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "org.eclipse.ogee.model.odata"; //$NON-NLS-1$

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	public OdataValidator() {
		super();
		batchValidator = (IBatchValidator) ModelValidationService.getInstance().newValidator(
                EvaluationMode.BATCH);
        batchValidator.setIncludeLiveConstraints(true);
        batchValidator.setReportSuccesses(false);
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return OdataPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case OdataPackage.ENTITY_TYPE:
				return validateEntityType((EntityType)value, diagnostics, context);
			case OdataPackage.PROPERTY:
				return validateProperty((Property)value, diagnostics, context);
			case OdataPackage.COMPLEX_TYPE:
				return validateComplexType((ComplexType)value, diagnostics, context);
			case OdataPackage.ASSOCIATION:
				return validateAssociation((Association)value, diagnostics, context);
			case OdataPackage.NAVIGATION_PROPERTY:
				return validateNavigationProperty((NavigationProperty)value, diagnostics, context);
			case OdataPackage.FUNCTION_IMPORT:
				return validateFunctionImport((FunctionImport)value, diagnostics, context);
			case OdataPackage.PARAMETER:
				return validateParameter((Parameter)value, diagnostics, context);
			case OdataPackage.ENTITY_SET:
				return validateEntitySet((EntitySet)value, diagnostics, context);
			case OdataPackage.ASSOCIATION_SET:
				return validateAssociationSet((AssociationSet)value, diagnostics, context);
			case OdataPackage.ROLE:
				return validateRole((Role)value, diagnostics, context);
			case OdataPackage.ASSOCIATION_SET_END:
				return validateAssociationSetEnd((AssociationSetEnd)value, diagnostics, context);
			case OdataPackage.EDMX:
				return validateEDMX((EDMX)value, diagnostics, context);
			case OdataPackage.REFERENTIAL_CONSTRAINT:
				return validateReferentialConstraint((ReferentialConstraint)value, diagnostics, context);
			case OdataPackage.IPROPERTY_TYPE_USAGE:
				return validateIPropertyTypeUsage((IPropertyTypeUsage)value, diagnostics, context);
			case OdataPackage.SIMPLE_TYPE:
				return validateSimpleType((SimpleType)value, diagnostics, context);
			case OdataPackage.IPARAMETER_TYPE_USAGE:
				return validateIParameterTypeUsage((IParameterTypeUsage)value, diagnostics, context);
			case OdataPackage.BINDING:
				return validateBinding((Binding)value, diagnostics, context);
			case OdataPackage.VALUE_TERM:
				return validateValueTerm((ValueTerm)value, diagnostics, context);
			case OdataPackage.VALUE_ANNOTATION:
				return validateValueAnnotation((ValueAnnotation)value, diagnostics, context);
			case OdataPackage.TYPE_ANNOTATION:
				return validateTypeAnnotation((TypeAnnotation)value, diagnostics, context);
			case OdataPackage.IANNOTATION_TARGET:
				return validateIAnnotationTarget((IAnnotationTarget)value, diagnostics, context);
			case OdataPackage.ENTITY_CONTAINER:
				return validateEntityContainer((EntityContainer)value, diagnostics, context);
			case OdataPackage.ENUM_TYPE:
				return validateEnumType((EnumType)value, diagnostics, context);
			case OdataPackage.ENUM_MEMBER:
				return validateEnumMember((EnumMember)value, diagnostics, context);
			case OdataPackage.IDOCUMENTABLE:
				return validateIDocumentable((IDocumentable)value, diagnostics, context);
			case OdataPackage.DOCUMENTATION:
				return validateDocumentation((Documentation)value, diagnostics, context);
			case OdataPackage.ITYPE_TERM:
				return validateITypeTerm((ITypeTerm)value, diagnostics, context);
			case OdataPackage.ANNOTATION:
				return validateAnnotation((Annotation)value, diagnostics, context);
			case OdataPackage.EDMX_SET:
				return validateEDMXSet((EDMXSet)value, diagnostics, context);
			case OdataPackage.ENTITY_TYPE_USAGE:
				return validateEntityTypeUsage((EntityTypeUsage)value, diagnostics, context);
			case OdataPackage.COMPLEX_TYPE_USAGE:
				return validateComplexTypeUsage((ComplexTypeUsage)value, diagnostics, context);
			case OdataPackage.ENUM_TYPE_USAGE:
				return validateEnumTypeUsage((EnumTypeUsage)value, diagnostics, context);
			case OdataPackage.SIMPLE_TYPE_USAGE:
				return validateSimpleTypeUsage((SimpleTypeUsage)value, diagnostics, context);
			case OdataPackage.IFUNCTION_RETURN_TYPE_USAGE:
				return validateIFunctionReturnTypeUsage((IFunctionReturnTypeUsage)value, diagnostics, context);
			case OdataPackage.EDMX_REFERENCE:
				return validateEDMXReference((EDMXReference)value, diagnostics, context);
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE:
				return validateEDMXAnnotationsReference((EDMXAnnotationsReference)value, diagnostics, context);
			case OdataPackage.SCHEMA:
				return validateSchema((Schema)value, diagnostics, context);
			case OdataPackage.DATA_SERVICE:
				return validateDataService((DataService)value, diagnostics, context);
			case OdataPackage.INCLUDE_RESTRICTION:
				return validateIncludeRestriction((IncludeRestriction)value, diagnostics, context);
			case OdataPackage.SIMPLE_VALUE:
				return validateSimpleValue((SimpleValue)value, diagnostics, context);
			case OdataPackage.PATH_VALUE:
				return validatePathValue((PathValue)value, diagnostics, context);
			case OdataPackage.USING:
				return validateUsing((Using)value, diagnostics, context);
			case OdataPackage.RECORD_VALUE:
				return validateRecordValue((RecordValue)value, diagnostics, context);
			case OdataPackage.IRECORD_VALUE_TYPE:
				return validateIRecordValueType((IRecordValueType)value, diagnostics, context);
			case OdataPackage.ANNOTATION_VALUE:
				return validateAnnotationValue((AnnotationValue)value, diagnostics, context);
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE:
				return validateReturnEntityTypeUsage((ReturnEntityTypeUsage)value, diagnostics, context);
			case OdataPackage.PROPERTY_MAPPING:
				return validatePropertyMapping((Map.Entry<?, ?>)value, diagnostics, context);
			case OdataPackage.VALUE_COLLECTION:
				return validateValueCollection((ValueCollection)value, diagnostics, context);
			case OdataPackage.PROPERTY_TO_VALUE_MAP:
				return validatePropertyToValueMap((Map.Entry<?, ?>)value, diagnostics, context);
			case OdataPackage.DYNAMIC_EXPRESSION:
				return validateDynamicExpression((DynamicExpression)value, diagnostics, context);
			case OdataPackage.CONSTANT_EXPRESSION:
				return validateConstantExpression((ConstantExpression)value, diagnostics, context);
			case OdataPackage.BOOLEAN_VALUE:
				return validateBooleanValue((BooleanValue)value, diagnostics, context);
			case OdataPackage.STRING_VALUE:
				return validateStringValue((StringValue)value, diagnostics, context);
			case OdataPackage.BINARY_VALUE:
				return validateBinaryValue((BinaryValue)value, diagnostics, context);
			case OdataPackage.DATE_TIME_VALUE:
				return validateDateTimeValue((DateTimeValue)value, diagnostics, context);
			case OdataPackage.DECIMAL_VALUE:
				return validateDecimalValue((DecimalValue)value, diagnostics, context);
			case OdataPackage.NUMBER_VALUE:
				return validateNumberValue((NumberValue)value, diagnostics, context);
			case OdataPackage.GUID_VALUE:
				return validateGuidValue((GuidValue)value, diagnostics, context);
			case OdataPackage.TIME_VALUE:
				return validateTimeValue((TimeValue)value, diagnostics, context);
			case OdataPackage.COLLECTABLE_EXPRESSION:
				return validateCollectableExpression((CollectableExpression)value, diagnostics, context);
			case OdataPackage.SINGLE_VALUE:
				return validateSingleValue((SingleValue)value, diagnostics, context);
			case OdataPackage.DOUBLE_VALUE:
				return validateDoubleValue((DoubleValue)value, diagnostics, context);
			case OdataPackage.SBYTE_VALUE:
				return validateSByteValue((SByteValue)value, diagnostics, context);
			case OdataPackage.INT16_VALUE:
				return validateInt16Value((Int16Value)value, diagnostics, context);
			case OdataPackage.INT32_VALUE:
				return validateInt32Value((Int32Value)value, diagnostics, context);
			case OdataPackage.INT64_VALUE:
				return validateInt64Value((Int64Value)value, diagnostics, context);
			case OdataPackage.DATE_TIME_OFFSET_VALUE:
				return validateDateTimeOffsetValue((DateTimeOffsetValue)value, diagnostics, context);
			case OdataPackage.BYTE_VALUE:
				return validateByteValue((ByteValue)value, diagnostics, context);
			case OdataPackage.INTEGER_VALUE:
				return validateIntegerValue((IntegerValue)value, diagnostics, context);
			case OdataPackage.FLOAT_VALUE:
				return validateFloatValue((FloatValue)value, diagnostics, context);
			case OdataPackage.ENUM_VALUE:
				return validateEnumValue((EnumValue)value, diagnostics, context);
			case OdataPackage.EDM_TYPES:
				return validateEDMTypes((EDMTypes)value, diagnostics, context);
			case OdataPackage.MULTIPLICITY:
				return validateMultiplicity((Multiplicity)value, diagnostics, context);
			case OdataPackage.TYPE_TERM_TARGETS:
				return validateTypeTermTargets((TypeTermTargets)value, diagnostics, context);
			case OdataPackage.VALUE_TERM_TARGETS:
				return validateValueTermTargets((ValueTermTargets)value, diagnostics, context);
			case OdataPackage.SCHEMA_CLASSIFIER:
				return validateSchemaClassifier((SchemaClassifier)value, diagnostics, context);
			case OdataPackage.GUID_VALUE_TYPE:
				return validateGuidValueType((UUID)value, diagnostics, context);
			case OdataPackage.DATE_TIME_OFFSET_VALUE_TYPE:
				return validateDateTimeOffsetValueType((DateTimeOffset)value, diagnostics, context);
			case OdataPackage.DATE_TIME:
				return validateDateTime((DateTime)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEntityType(EntityType entityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(entityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProperty(Property property, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(property, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexType(ComplexType complexType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociation(Association association, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(association, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNavigationProperty(NavigationProperty navigationProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(navigationProperty, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFunctionImport(FunctionImport functionImport, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(functionImport, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateParameter(Parameter parameter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(parameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEntitySet(EntitySet entitySet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(entitySet, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociationSet(AssociationSet associationSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(associationSet, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRole(Role role, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(role, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociationSetEnd(AssociationSetEnd associationSetEnd, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(associationSetEnd, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReferentialConstraint(ReferentialConstraint referentialConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(referentialConstraint, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIPropertyTypeUsage(IPropertyTypeUsage iPropertyTypeUsage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iPropertyTypeUsage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleType(SimpleType simpleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(simpleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIParameterTypeUsage(IParameterTypeUsage iParameterTypeUsage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iParameterTypeUsage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinding(Binding binding, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(binding, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValueTerm(ValueTerm valueTerm, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(valueTerm, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValueAnnotation(ValueAnnotation valueAnnotation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(valueAnnotation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTypeAnnotation(TypeAnnotation typeAnnotation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(typeAnnotation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIAnnotationTarget(IAnnotationTarget iAnnotationTarget, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iAnnotationTarget, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEntityContainer(EntityContainer entityContainer, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(entityContainer, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumType(EnumType enumType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumMember(EnumMember enumMember, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumMember, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIDocumentable(IDocumentable iDocumentable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iDocumentable, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDocumentation(Documentation documentation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(documentation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateITypeTerm(ITypeTerm iTypeTerm, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iTypeTerm, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAnnotation(Annotation annotation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(annotation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEDMXSet(EDMXSet edmxSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(edmxSet, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(edmxSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(edmxSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(edmxSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(edmxSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(edmxSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(edmxSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(edmxSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(edmxSet, diagnostics, context);
		if (result || diagnostics != null) result &= validateEDMXSet_DelegateToValidationFW(edmxSet, diagnostics, context);
		return result;
	}

	/**
	 * Validates the DelegateToValidationFW constraint of '<em>EDMX Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 */
	public boolean validateEDMXSet_DelegateToValidationFW(EDMXSet edmxSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
        IStatus status = Status.OK_STATUS;
        
        if (diagnostics != null) {
        	status = batchValidator.validate(
                	edmxSet,
                    new NullProgressMonitor());	              
                
                appendDiagnostics(status, diagnostics);
        }       
        return status.isOK();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEntityTypeUsage(EntityTypeUsage entityTypeUsage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(entityTypeUsage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeUsage(ComplexTypeUsage complexTypeUsage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeUsage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumTypeUsage(EnumTypeUsage enumTypeUsage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumTypeUsage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleTypeUsage(SimpleTypeUsage simpleTypeUsage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(simpleTypeUsage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIFunctionReturnTypeUsage(IFunctionReturnTypeUsage iFunctionReturnTypeUsage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iFunctionReturnTypeUsage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEDMXReference(EDMXReference edmxReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(edmxReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEDMXAnnotationsReference(EDMXAnnotationsReference edmxAnnotationsReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(edmxAnnotationsReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSchema(Schema schema, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(schema, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataService(DataService dataService, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataService, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIncludeRestriction(IncludeRestriction includeRestriction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(includeRestriction, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleValue(SimpleValue simpleValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(simpleValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePathValue(PathValue pathValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(pathValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUsing(Using using, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(using, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRecordValue(RecordValue recordValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(recordValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIRecordValueType(IRecordValueType iRecordValueType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iRecordValueType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAnnotationValue(AnnotationValue annotationValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(annotationValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReturnEntityTypeUsage(ReturnEntityTypeUsage returnEntityTypeUsage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(returnEntityTypeUsage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePropertyMapping(Map.Entry<?, ?> propertyMapping, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)propertyMapping, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValueCollection(ValueCollection valueCollection, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(valueCollection, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePropertyToValueMap(Map.Entry<?, ?> propertyToValueMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)propertyToValueMap, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDynamicExpression(DynamicExpression dynamicExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dynamicExpression, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConstantExpression(ConstantExpression constantExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(constantExpression, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBooleanValue(BooleanValue booleanValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(booleanValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStringValue(StringValue stringValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(stringValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinaryValue(BinaryValue binaryValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(binaryValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDateTimeValue(DateTimeValue dateTimeValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dateTimeValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDecimalValue(DecimalValue decimalValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(decimalValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNumberValue(NumberValue numberValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(numberValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGuidValue(GuidValue guidValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(guidValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimeValue(TimeValue timeValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(timeValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCollectableExpression(CollectableExpression collectableExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(collectableExpression, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSingleValue(SingleValue singleValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(singleValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDoubleValue(DoubleValue doubleValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(doubleValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSByteValue(SByteValue sByteValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(sByteValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInt16Value(Int16Value int16Value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(int16Value, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInt32Value(Int32Value int32Value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(int32Value, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInt64Value(Int64Value int64Value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(int64Value, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDateTimeOffsetValue(DateTimeOffsetValue dateTimeOffsetValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dateTimeOffsetValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateByteValue(ByteValue byteValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(byteValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIntegerValue(IntegerValue integerValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(integerValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFloatValue(FloatValue floatValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(floatValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumValue(EnumValue enumValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEDMX(EDMX edmx, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(edmx, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEDMTypes(EDMTypes edmTypes, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMultiplicity(Multiplicity multiplicity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTypeTermTargets(TypeTermTargets typeTermTargets, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValueTermTargets(ValueTermTargets valueTermTargets, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSchemaClassifier(SchemaClassifier schemaClassifier, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGuidValueType(UUID guidValueType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDateTimeOffsetValueType(DateTimeOffset dateTimeOffsetValueType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDateTime(DateTime dateTime, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}
	
	private void appendDiagnostics(IStatus status, DiagnosticChain diagnostics) {
	    if (status.isMultiStatus()) {
	        IStatus[] children = status.getChildren();
	        
	        for (int i = 0; i < children.length; i++) {
	            appendDiagnostics(children[i], diagnostics);
	        }
	    } else if (status instanceof IConstraintStatus) {
	        diagnostics.add(new BasicDiagnostic(
	            status.getSeverity(),
	            status.getPlugin(),
	            status.getCode(),
	            status.getMessage(),
	            ((IConstraintStatus) status).getResultLocus().toArray()));
	    }
	}

} //OdataValidator
