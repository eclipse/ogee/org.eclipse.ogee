/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.odata.util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;

public class OdataModelHelper {
	
	private EDMXSet edmxSet;
	
	public OdataModelHelper(EDMXSet edmxSet){
		this.edmxSet = edmxSet;
	}
	
	public List<Schema> getSchemataInScope(Schema schema) throws ModelException{
		List<Schema> schemataInScope = new LinkedList<Schema>();

		// Check input
		if (schema == null) {
			// TODO correct Exception
			throw new ModelException();
		}

		if (this.edmxSet != null
				&& !this.edmxSet.getSchemata().contains(schema)) {
			throw new ModelException();
		}

		List<DataService> dataServices = schema.getDataServices();
		if (dataServices.size() == 0) {
			return schemataInScope;
		}

		Iterator<DataService> dataServiceIterator = dataServices.iterator();
		DataService dataService;
		EDMX edmx;
		List<EDMX> allReferencedEDMX;
		Iterator<EDMX> edmxIterator;

		while (dataServiceIterator.hasNext()) {
			dataService = dataServiceIterator.next();

			// All schemata within the same data service...
			schemataInScope.addAll(dataService.getSchemata());

			edmx = (EDMX) dataService.eContainer();
			if (edmx == null) {
				continue;
			}

			// Calculate all referenced EDMX, including transitive referenced
			// EDMX
			allReferencedEDMX = calculateAllReferencedEDMX(edmx);
			edmxIterator = allReferencedEDMX.iterator();
			while (edmxIterator.hasNext()) {
				edmx = edmxIterator.next();
				if (edmx.getDataService() != null) {
					// Add all schemata of (transitive) referenced EDMX
					schemataInScope.addAll(edmx.getDataService().getSchemata());
				}
			}

		}
		
		return schemataInScope;
	}
	
	private List<EDMX> calculateAllReferencedEDMX(EDMX edmx){
		EDMXReference edmxRef;
		EDMX nextEdmx;
		List<EDMX> allReferencedEDMX = new LinkedList<EDMX>();
		
		Iterator<EDMXReference> edmxRefIterator = edmx.getReferences().iterator();
		while(edmxRefIterator.hasNext()){
			edmxRef = edmxRefIterator.next();
			nextEdmx = edmxRef.getReferencedEDMX();
			if(nextEdmx!=null){
				allReferencedEDMX.addAll(this.calculateAllReferencedEDMX(nextEdmx));
				allReferencedEDMX.add(nextEdmx);
			}
		}
		
		return allReferencedEDMX;
	}

}
