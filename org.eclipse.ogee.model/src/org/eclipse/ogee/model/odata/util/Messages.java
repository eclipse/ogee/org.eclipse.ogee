/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.odata.util;

import java.lang.reflect.Field;
import java.net.URL;

import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.osgi.util.NLS;

public class Messages extends NLS implements ResourceLocator {
	
	private static final String BUNDLE_NAME = "org.eclipse.ogee.model.odata.util.messages"; //$NON-NLS-1$
	
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	public Messages() {
	}

	@Override
	public URL getBaseURL() {
		return getClass().getResource("message.properties"); //$NON-NLS-1$
	}

	@Override
	public Object getImage(String key) {
		return null; //no image
	}

	@Override
	public String getString(String key) {
		try {
			Field myField = getClass().getField(key);
			return (String) myField.get(null);
		} catch (Exception exc) {
			return null;
		}
	}

	@Override
	public String getString(String key, boolean translate) {
		return getString(key);  //always translate...
	}

	@Override
	public String getString(String key, Object[] substitutions) {
		return NLS.bind(getString(key), substitutions);
	}

	@Override
	public String getString(String key, Object[] substitutions,
			boolean translate) {
		return getString(key, substitutions); //always translate...
	}
	
}
