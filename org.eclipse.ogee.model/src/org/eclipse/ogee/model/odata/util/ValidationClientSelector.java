/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.odata.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.validation.model.IClientSelector;

public class ValidationClientSelector implements IClientSelector {

	@Override
	public boolean selects(Object object) {
		if (object instanceof EObject) {
			EObject objToTest = (EObject) object;
			String extension = EcoreUtil.getURI(objToTest).fileExtension();
			// accept also extension "null" for the Unit Test
			return (extension == null || "odata".equals(extension)); //$NON-NLS-1$
		}
		return false;
	}

}
