/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.odata.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.Binding;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueTerm;

public class OdataSchemataReferencer extends OdataSwitch<List<EObject>> {

	private List<Schema> inScopeSchemata = null;
	private List<Schema> outOfScopeSchemata = null;
	
	public List<EObject> execute(List<Schema> inScopeSchemata, List<Schema> outOfScopeSchemata){
		if(this.inScopeSchemata!=null){
			throw new IllegalStateException();
		}
		
		List<EObject> result = new LinkedList<EObject>();
		List<EObject> tempResult;
		
		this.inScopeSchemata = new LinkedList<Schema>(inScopeSchemata);
		if(outOfScopeSchemata != null){
			this.outOfScopeSchemata = new LinkedList<Schema>(outOfScopeSchemata);
		}else{
			this.outOfScopeSchemata = new LinkedList<Schema>();
		}
		if(this.outOfScopeSchemata != null){
			this.inScopeSchemata.removeAll(this.outOfScopeSchemata);
		}
		
		EObject currentObject;
		TreeIterator<EObject> treeIterator = EcoreUtil.getAllContents(this.inScopeSchemata, true);
		while(treeIterator.hasNext()){
			currentObject = treeIterator.next();
			tempResult = this.doSwitch(currentObject);
			if(tempResult!=null){
				result.addAll(tempResult);
			}
		}
		
		this.inScopeSchemata = null;
		this.outOfScopeSchemata = null;
		return result;
	}
	
	@Override
	public List<EObject> caseUsing(Using object) {
		List<EObject> usedObjects = new LinkedList<EObject>();
		
		// used namespace
		Schema usedSchema = object.getUsedNamespace();
		if(usedSchema != null && !this.inScopeSchemata.contains(usedSchema)){
			usedObjects.add(usedSchema);
		}
		
		return usedObjects;
	}
	
	@Override
	public List<EObject> caseComplexType(ComplexType object) {
		List<EObject> usedObjects = new LinkedList<EObject>();
		
		// base type
		ComplexType baseType = object.getBaseType();
		if(baseType!=null && !this.inScopeSchemata.contains(baseType.eContainer())){
			usedObjects.add(baseType);
		}
		
		return usedObjects;
	}

	@Override
	public List<EObject> caseEntityType(EntityType object) {
		List<EObject> usedObjects = new LinkedList<EObject>();
		
		// base type
		EntityType baseType = object.getBaseType();
		if(baseType!=null && !this.inScopeSchemata.contains(baseType.eContainer())){
			usedObjects.add(baseType);
		}
		
		return usedObjects;
	}

	@Override
	public List<EObject> caseNavigationProperty(NavigationProperty object) {
		List<EObject> usedObjects = new LinkedList<EObject>();

		// association
		Association association = object.getRelationship();
		if(association!=null && !this.inScopeSchemata.contains(association.eContainer())){
			usedObjects.add(association);
		}

		// role from
		Role role = object.getFromRole();
		if(role!=null && !this.inScopeSchemata.contains(role.eContainer().eContainer())){
			usedObjects.add(association);
		}

		// role to
		role = object.getToRole();
		if(role!=null && !this.inScopeSchemata.contains(role.eContainer().eContainer())){
			usedObjects.add(association);
		}
		
		return usedObjects;
	}

	@Override
	public List<EObject> caseEntitySet(EntitySet object) {
		List<EObject> usedObjects = new LinkedList<EObject>();

		// entity type
		EntityType type = object.getType();
		if(type!=null && !this.inScopeSchemata.contains(type.eContainer())){
			usedObjects.add(type);
		}
		
		return usedObjects;
	}

	@Override
	public List<EObject> caseRole(Role object) {
		List<EObject> usedObjects = new LinkedList<EObject>();

		// entity type
		EntityType type = object.getType();
		if(type!=null && !this.inScopeSchemata.contains(type.eContainer())){
			usedObjects.add(type);
		}
		
		return usedObjects;
	}

	@Override
	public List<EObject> caseAssociationSet(AssociationSet object) {
		List<EObject> usedObjects = new LinkedList<EObject>();

		// association
		Association association = object.getAssociation();
		if(association!=null && !this.inScopeSchemata.contains(association.eContainer())){
			usedObjects.add(association);
		}
		
		return usedObjects;
	}

	@Override
	public List<EObject> caseEntityTypeUsage(EntityTypeUsage object) {
		List<EObject> usedObjects = new LinkedList<EObject>();

		// entity type
		EntityType type = object.getEntityType();
		if(type!=null && !this.inScopeSchemata.contains(type.eContainer())){
			usedObjects.add(type);
		}
		
		return usedObjects;
	}

	@Override
	public List<EObject> caseComplexTypeUsage(ComplexTypeUsage object) {
		List<EObject> usedObjects = new LinkedList<EObject>();

		// complex type
		ComplexType type = object.getComplexType();
		if(type!=null && !this.inScopeSchemata.contains(type.eContainer())){
			usedObjects.add(type);
		}
		
		return usedObjects;
	}

	@Override
	public List<EObject> casePropertyMapping(Entry<Property, Property> object) {
		List<EObject> usedObjects = new LinkedList<EObject>();

		// key
		Property property = object.getKey();
		if(property!=null && !this.inScopeSchemata.contains(property.eContainer().eContainer())){
			usedObjects.add(property);
		}
				
		// property
		property = object.getValue();
		if(property!=null && !this.inScopeSchemata.contains(property.eContainer().eContainer())){
			usedObjects.add(property);
		}

		return usedObjects;
	}

	@Override
	public List<EObject> caseValueAnnotation(ValueAnnotation object) {
		List<EObject> usedObjects = new LinkedList<EObject>();

		// term
		ValueTerm term = object.getTerm();
		if(term!=null && !this.inScopeSchemata.contains(term.eContainer())){
			usedObjects.add(term);
		}

		// target
		IAnnotationTarget target = object.getTarget();
		if(target!=null && !this.inScopeSchemata.contains(getAncestorWithBehavior(target,Schema.class))){
			usedObjects.add(target);
		}
		
		return usedObjects;
	}
	
	@Override
	public List<EObject> caseBinding(Binding object) {
		List<EObject> usedObjects = new LinkedList<EObject>();

		// entity type
		EntityType type = object.getType();
		if(type!=null && !this.inScopeSchemata.contains(type.eContainer())){
			usedObjects.add(type);
		}
		
		return usedObjects;
	}

	@Override
	public List<EObject> caseReturnEntityTypeUsage(ReturnEntityTypeUsage object) {
		List<EObject> usedObjects = new LinkedList<EObject>();

		// entity type
		EntityType type = object.getEntityType();
		if(type!=null && !this.inScopeSchemata.contains(type.eContainer())){
			usedObjects.add(type);
		}
		
		return usedObjects;
	}

	private EObject getAncestorWithBehavior(EObject node, Class<? extends EObject> behavior) {
		EObject object = node;
		while(object!=null) {
			if(behavior.isInstance(object)){
				break;
			}
			object = object.eContainer();
		}
		return object;
	}
	
}