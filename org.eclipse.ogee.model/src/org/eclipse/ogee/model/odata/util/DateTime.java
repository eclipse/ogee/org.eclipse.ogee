/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.odata.util;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class DateTime {

	private Date date = null;
	
	public DateTime(Date date){
		super();
		this.date = date;
	}

	public static DateTime fromXMLFormat(String xml){

		try{
			XMLGregorianCalendar data = DatatypeFactory.newInstance().newXMLGregorianCalendar(xml);
			data.normalize();
			Date date = data.toGregorianCalendar().getTime();
			return new DateTime(date);
		}catch(DatatypeConfigurationException e){
			throw new IllegalArgumentException(e);
		}catch(NullPointerException e){
			throw new IllegalArgumentException(e);
		}
		
	}

	public String toXMLFormat(){
		
		try{
			GregorianCalendar value = new GregorianCalendar();
			value.setTime(this.date);
			value.setTimeZone(TimeZone.getTimeZone("Z")); //$NON-NLS-1$
			XMLGregorianCalendar data = DatatypeFactory.newInstance().newXMLGregorianCalendar(value);
			return data.toXMLFormat();
		}catch (DatatypeConfigurationException e){
			throw new IllegalArgumentException(e);
		}catch(NullPointerException e){
			throw new IllegalArgumentException(e);
		}
		
	}
	
	public String toString(){
		return this.toXMLFormat();
	}

	public Date getDateValue(){
		return (Date)this.date.clone();
	}
	
}
