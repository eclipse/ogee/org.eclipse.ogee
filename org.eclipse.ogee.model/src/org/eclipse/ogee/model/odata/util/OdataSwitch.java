/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.odata.util;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.BinaryValue;
import org.eclipse.ogee.model.odata.Binding;
import org.eclipse.ogee.model.odata.BooleanValue;
import org.eclipse.ogee.model.odata.ByteValue;
import org.eclipse.ogee.model.odata.CollectableExpression;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.ConstantExpression;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.DateTimeOffsetValue;
import org.eclipse.ogee.model.odata.DateTimeValue;
import org.eclipse.ogee.model.odata.DecimalValue;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.DoubleValue;
import org.eclipse.ogee.model.odata.DynamicExpression;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXAnnotationsReference;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.EnumValue;
import org.eclipse.ogee.model.odata.FloatValue;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.GuidValue;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IDocumentable;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.IParameterTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.IRecordValueType;
import org.eclipse.ogee.model.odata.ITypeTerm;
import org.eclipse.ogee.model.odata.IncludeRestriction;
import org.eclipse.ogee.model.odata.Int16Value;
import org.eclipse.ogee.model.odata.Int32Value;
import org.eclipse.ogee.model.odata.Int64Value;
import org.eclipse.ogee.model.odata.IntegerValue;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.NumberValue;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.PathValue;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.RecordValue;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.SByteValue;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.model.odata.SingleValue;
import org.eclipse.ogee.model.odata.StringValue;
import org.eclipse.ogee.model.odata.TimeValue;
import org.eclipse.ogee.model.odata.TypeAnnotation;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.model.odata.ValueTerm;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.ogee.model.odata.OdataPackage
 * @generated
 */
public class OdataSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OdataPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OdataSwitch() {
		if (modelPackage == null) {
			modelPackage = OdataPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OdataPackage.ENTITY_TYPE: {
				EntityType entityType = (EntityType)theEObject;
				T result = caseEntityType(entityType);
				if (result == null) result = caseIDocumentable(entityType);
				if (result == null) result = caseIAnnotationTarget(entityType);
				if (result == null) result = caseITypeTerm(entityType);
				if (result == null) result = caseIRecordValueType(entityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.PROPERTY: {
				Property property = (Property)theEObject;
				T result = caseProperty(property);
				if (result == null) result = caseIAnnotationTarget(property);
				if (result == null) result = caseIDocumentable(property);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.COMPLEX_TYPE: {
				ComplexType complexType = (ComplexType)theEObject;
				T result = caseComplexType(complexType);
				if (result == null) result = caseIDocumentable(complexType);
				if (result == null) result = caseIAnnotationTarget(complexType);
				if (result == null) result = caseITypeTerm(complexType);
				if (result == null) result = caseIRecordValueType(complexType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ASSOCIATION: {
				Association association = (Association)theEObject;
				T result = caseAssociation(association);
				if (result == null) result = caseIAnnotationTarget(association);
				if (result == null) result = caseIDocumentable(association);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.NAVIGATION_PROPERTY: {
				NavigationProperty navigationProperty = (NavigationProperty)theEObject;
				T result = caseNavigationProperty(navigationProperty);
				if (result == null) result = caseIAnnotationTarget(navigationProperty);
				if (result == null) result = caseIDocumentable(navigationProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.FUNCTION_IMPORT: {
				FunctionImport functionImport = (FunctionImport)theEObject;
				T result = caseFunctionImport(functionImport);
				if (result == null) result = caseIAnnotationTarget(functionImport);
				if (result == null) result = caseIDocumentable(functionImport);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.PARAMETER: {
				Parameter parameter = (Parameter)theEObject;
				T result = caseParameter(parameter);
				if (result == null) result = caseIAnnotationTarget(parameter);
				if (result == null) result = caseIDocumentable(parameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ENTITY_SET: {
				EntitySet entitySet = (EntitySet)theEObject;
				T result = caseEntitySet(entitySet);
				if (result == null) result = caseIAnnotationTarget(entitySet);
				if (result == null) result = caseIDocumentable(entitySet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ASSOCIATION_SET: {
				AssociationSet associationSet = (AssociationSet)theEObject;
				T result = caseAssociationSet(associationSet);
				if (result == null) result = caseIAnnotationTarget(associationSet);
				if (result == null) result = caseIDocumentable(associationSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ROLE: {
				Role role = (Role)theEObject;
				T result = caseRole(role);
				if (result == null) result = caseIAnnotationTarget(role);
				if (result == null) result = caseIDocumentable(role);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ASSOCIATION_SET_END: {
				AssociationSetEnd associationSetEnd = (AssociationSetEnd)theEObject;
				T result = caseAssociationSetEnd(associationSetEnd);
				if (result == null) result = caseIDocumentable(associationSetEnd);
				if (result == null) result = caseIAnnotationTarget(associationSetEnd);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.EDMX: {
				EDMX edmx = (EDMX)theEObject;
				T result = caseEDMX(edmx);
				if (result == null) result = caseIDocumentable(edmx);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.REFERENTIAL_CONSTRAINT: {
				ReferentialConstraint referentialConstraint = (ReferentialConstraint)theEObject;
				T result = caseReferentialConstraint(referentialConstraint);
				if (result == null) result = caseIAnnotationTarget(referentialConstraint);
				if (result == null) result = caseIDocumentable(referentialConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.IPROPERTY_TYPE_USAGE: {
				IPropertyTypeUsage iPropertyTypeUsage = (IPropertyTypeUsage)theEObject;
				T result = caseIPropertyTypeUsage(iPropertyTypeUsage);
				if (result == null) result = caseIParameterTypeUsage(iPropertyTypeUsage);
				if (result == null) result = caseIFunctionReturnTypeUsage(iPropertyTypeUsage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.SIMPLE_TYPE: {
				SimpleType simpleType = (SimpleType)theEObject;
				T result = caseSimpleType(simpleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.IPARAMETER_TYPE_USAGE: {
				IParameterTypeUsage iParameterTypeUsage = (IParameterTypeUsage)theEObject;
				T result = caseIParameterTypeUsage(iParameterTypeUsage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.BINDING: {
				Binding binding = (Binding)theEObject;
				T result = caseBinding(binding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.VALUE_TERM: {
				ValueTerm valueTerm = (ValueTerm)theEObject;
				T result = caseValueTerm(valueTerm);
				if (result == null) result = caseIAnnotationTarget(valueTerm);
				if (result == null) result = caseIDocumentable(valueTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.VALUE_ANNOTATION: {
				ValueAnnotation valueAnnotation = (ValueAnnotation)theEObject;
				T result = caseValueAnnotation(valueAnnotation);
				if (result == null) result = caseAnnotation(valueAnnotation);
				if (result == null) result = caseIDocumentable(valueAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.TYPE_ANNOTATION: {
				TypeAnnotation typeAnnotation = (TypeAnnotation)theEObject;
				T result = caseTypeAnnotation(typeAnnotation);
				if (result == null) result = caseAnnotation(typeAnnotation);
				if (result == null) result = caseIDocumentable(typeAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.IANNOTATION_TARGET: {
				IAnnotationTarget iAnnotationTarget = (IAnnotationTarget)theEObject;
				T result = caseIAnnotationTarget(iAnnotationTarget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ENTITY_CONTAINER: {
				EntityContainer entityContainer = (EntityContainer)theEObject;
				T result = caseEntityContainer(entityContainer);
				if (result == null) result = caseIAnnotationTarget(entityContainer);
				if (result == null) result = caseIDocumentable(entityContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ENUM_TYPE: {
				EnumType enumType = (EnumType)theEObject;
				T result = caseEnumType(enumType);
				if (result == null) result = caseIDocumentable(enumType);
				if (result == null) result = caseIAnnotationTarget(enumType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ENUM_MEMBER: {
				EnumMember enumMember = (EnumMember)theEObject;
				T result = caseEnumMember(enumMember);
				if (result == null) result = caseIDocumentable(enumMember);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.IDOCUMENTABLE: {
				IDocumentable iDocumentable = (IDocumentable)theEObject;
				T result = caseIDocumentable(iDocumentable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.DOCUMENTATION: {
				Documentation documentation = (Documentation)theEObject;
				T result = caseDocumentation(documentation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ITYPE_TERM: {
				ITypeTerm iTypeTerm = (ITypeTerm)theEObject;
				T result = caseITypeTerm(iTypeTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ANNOTATION: {
				Annotation annotation = (Annotation)theEObject;
				T result = caseAnnotation(annotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.EDMX_SET: {
				EDMXSet edmxSet = (EDMXSet)theEObject;
				T result = caseEDMXSet(edmxSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ENTITY_TYPE_USAGE: {
				EntityTypeUsage entityTypeUsage = (EntityTypeUsage)theEObject;
				T result = caseEntityTypeUsage(entityTypeUsage);
				if (result == null) result = caseIParameterTypeUsage(entityTypeUsage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.COMPLEX_TYPE_USAGE: {
				ComplexTypeUsage complexTypeUsage = (ComplexTypeUsage)theEObject;
				T result = caseComplexTypeUsage(complexTypeUsage);
				if (result == null) result = caseIPropertyTypeUsage(complexTypeUsage);
				if (result == null) result = caseIParameterTypeUsage(complexTypeUsage);
				if (result == null) result = caseIFunctionReturnTypeUsage(complexTypeUsage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ENUM_TYPE_USAGE: {
				EnumTypeUsage enumTypeUsage = (EnumTypeUsage)theEObject;
				T result = caseEnumTypeUsage(enumTypeUsage);
				if (result == null) result = caseIPropertyTypeUsage(enumTypeUsage);
				if (result == null) result = caseIParameterTypeUsage(enumTypeUsage);
				if (result == null) result = caseIFunctionReturnTypeUsage(enumTypeUsage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.SIMPLE_TYPE_USAGE: {
				SimpleTypeUsage simpleTypeUsage = (SimpleTypeUsage)theEObject;
				T result = caseSimpleTypeUsage(simpleTypeUsage);
				if (result == null) result = caseIPropertyTypeUsage(simpleTypeUsage);
				if (result == null) result = caseIParameterTypeUsage(simpleTypeUsage);
				if (result == null) result = caseIFunctionReturnTypeUsage(simpleTypeUsage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.IFUNCTION_RETURN_TYPE_USAGE: {
				IFunctionReturnTypeUsage iFunctionReturnTypeUsage = (IFunctionReturnTypeUsage)theEObject;
				T result = caseIFunctionReturnTypeUsage(iFunctionReturnTypeUsage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.EDMX_REFERENCE: {
				EDMXReference edmxReference = (EDMXReference)theEObject;
				T result = caseEDMXReference(edmxReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.EDMX_ANNOTATIONS_REFERENCE: {
				EDMXAnnotationsReference edmxAnnotationsReference = (EDMXAnnotationsReference)theEObject;
				T result = caseEDMXAnnotationsReference(edmxAnnotationsReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.SCHEMA: {
				Schema schema = (Schema)theEObject;
				T result = caseSchema(schema);
				if (result == null) result = caseIDocumentable(schema);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.DATA_SERVICE: {
				DataService dataService = (DataService)theEObject;
				T result = caseDataService(dataService);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.INCLUDE_RESTRICTION: {
				IncludeRestriction includeRestriction = (IncludeRestriction)theEObject;
				T result = caseIncludeRestriction(includeRestriction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.SIMPLE_VALUE: {
				SimpleValue simpleValue = (SimpleValue)theEObject;
				T result = caseSimpleValue(simpleValue);
				if (result == null) result = caseConstantExpression(simpleValue);
				if (result == null) result = caseCollectableExpression(simpleValue);
				if (result == null) result = caseAnnotationValue(simpleValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.PATH_VALUE: {
				PathValue pathValue = (PathValue)theEObject;
				T result = casePathValue(pathValue);
				if (result == null) result = caseDynamicExpression(pathValue);
				if (result == null) result = caseAnnotationValue(pathValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.USING: {
				Using using = (Using)theEObject;
				T result = caseUsing(using);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.RECORD_VALUE: {
				RecordValue recordValue = (RecordValue)theEObject;
				T result = caseRecordValue(recordValue);
				if (result == null) result = caseDynamicExpression(recordValue);
				if (result == null) result = caseCollectableExpression(recordValue);
				if (result == null) result = caseAnnotationValue(recordValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.IRECORD_VALUE_TYPE: {
				IRecordValueType iRecordValueType = (IRecordValueType)theEObject;
				T result = caseIRecordValueType(iRecordValueType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ANNOTATION_VALUE: {
				AnnotationValue annotationValue = (AnnotationValue)theEObject;
				T result = caseAnnotationValue(annotationValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.RETURN_ENTITY_TYPE_USAGE: {
				ReturnEntityTypeUsage returnEntityTypeUsage = (ReturnEntityTypeUsage)theEObject;
				T result = caseReturnEntityTypeUsage(returnEntityTypeUsage);
				if (result == null) result = caseIFunctionReturnTypeUsage(returnEntityTypeUsage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.PROPERTY_MAPPING: {
				@SuppressWarnings("unchecked") Map.Entry<Property, Property> propertyMapping = (Map.Entry<Property, Property>)theEObject;
				T result = casePropertyMapping(propertyMapping);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.VALUE_COLLECTION: {
				ValueCollection valueCollection = (ValueCollection)theEObject;
				T result = caseValueCollection(valueCollection);
				if (result == null) result = caseDynamicExpression(valueCollection);
				if (result == null) result = caseAnnotationValue(valueCollection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.PROPERTY_TO_VALUE_MAP: {
				@SuppressWarnings("unchecked") Map.Entry<Property, AnnotationValue> propertyToValueMap = (Map.Entry<Property, AnnotationValue>)theEObject;
				T result = casePropertyToValueMap(propertyToValueMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.DYNAMIC_EXPRESSION: {
				DynamicExpression dynamicExpression = (DynamicExpression)theEObject;
				T result = caseDynamicExpression(dynamicExpression);
				if (result == null) result = caseAnnotationValue(dynamicExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.CONSTANT_EXPRESSION: {
				ConstantExpression constantExpression = (ConstantExpression)theEObject;
				T result = caseConstantExpression(constantExpression);
				if (result == null) result = caseCollectableExpression(constantExpression);
				if (result == null) result = caseAnnotationValue(constantExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.BOOLEAN_VALUE: {
				BooleanValue booleanValue = (BooleanValue)theEObject;
				T result = caseBooleanValue(booleanValue);
				if (result == null) result = caseSimpleValue(booleanValue);
				if (result == null) result = caseConstantExpression(booleanValue);
				if (result == null) result = caseCollectableExpression(booleanValue);
				if (result == null) result = caseAnnotationValue(booleanValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.STRING_VALUE: {
				StringValue stringValue = (StringValue)theEObject;
				T result = caseStringValue(stringValue);
				if (result == null) result = caseSimpleValue(stringValue);
				if (result == null) result = caseConstantExpression(stringValue);
				if (result == null) result = caseCollectableExpression(stringValue);
				if (result == null) result = caseAnnotationValue(stringValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.BINARY_VALUE: {
				BinaryValue binaryValue = (BinaryValue)theEObject;
				T result = caseBinaryValue(binaryValue);
				if (result == null) result = caseSimpleValue(binaryValue);
				if (result == null) result = caseConstantExpression(binaryValue);
				if (result == null) result = caseCollectableExpression(binaryValue);
				if (result == null) result = caseAnnotationValue(binaryValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.DATE_TIME_VALUE: {
				DateTimeValue dateTimeValue = (DateTimeValue)theEObject;
				T result = caseDateTimeValue(dateTimeValue);
				if (result == null) result = caseSimpleValue(dateTimeValue);
				if (result == null) result = caseConstantExpression(dateTimeValue);
				if (result == null) result = caseCollectableExpression(dateTimeValue);
				if (result == null) result = caseAnnotationValue(dateTimeValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.DECIMAL_VALUE: {
				DecimalValue decimalValue = (DecimalValue)theEObject;
				T result = caseDecimalValue(decimalValue);
				if (result == null) result = caseNumberValue(decimalValue);
				if (result == null) result = caseSimpleValue(decimalValue);
				if (result == null) result = caseConstantExpression(decimalValue);
				if (result == null) result = caseCollectableExpression(decimalValue);
				if (result == null) result = caseAnnotationValue(decimalValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.NUMBER_VALUE: {
				NumberValue numberValue = (NumberValue)theEObject;
				T result = caseNumberValue(numberValue);
				if (result == null) result = caseSimpleValue(numberValue);
				if (result == null) result = caseConstantExpression(numberValue);
				if (result == null) result = caseCollectableExpression(numberValue);
				if (result == null) result = caseAnnotationValue(numberValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.GUID_VALUE: {
				GuidValue guidValue = (GuidValue)theEObject;
				T result = caseGuidValue(guidValue);
				if (result == null) result = caseSimpleValue(guidValue);
				if (result == null) result = caseConstantExpression(guidValue);
				if (result == null) result = caseCollectableExpression(guidValue);
				if (result == null) result = caseAnnotationValue(guidValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.TIME_VALUE: {
				TimeValue timeValue = (TimeValue)theEObject;
				T result = caseTimeValue(timeValue);
				if (result == null) result = caseSimpleValue(timeValue);
				if (result == null) result = caseConstantExpression(timeValue);
				if (result == null) result = caseCollectableExpression(timeValue);
				if (result == null) result = caseAnnotationValue(timeValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.COLLECTABLE_EXPRESSION: {
				CollectableExpression collectableExpression = (CollectableExpression)theEObject;
				T result = caseCollectableExpression(collectableExpression);
				if (result == null) result = caseAnnotationValue(collectableExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.SINGLE_VALUE: {
				SingleValue singleValue = (SingleValue)theEObject;
				T result = caseSingleValue(singleValue);
				if (result == null) result = caseFloatValue(singleValue);
				if (result == null) result = caseNumberValue(singleValue);
				if (result == null) result = caseSimpleValue(singleValue);
				if (result == null) result = caseConstantExpression(singleValue);
				if (result == null) result = caseCollectableExpression(singleValue);
				if (result == null) result = caseAnnotationValue(singleValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.DOUBLE_VALUE: {
				DoubleValue doubleValue = (DoubleValue)theEObject;
				T result = caseDoubleValue(doubleValue);
				if (result == null) result = caseFloatValue(doubleValue);
				if (result == null) result = caseNumberValue(doubleValue);
				if (result == null) result = caseSimpleValue(doubleValue);
				if (result == null) result = caseConstantExpression(doubleValue);
				if (result == null) result = caseCollectableExpression(doubleValue);
				if (result == null) result = caseAnnotationValue(doubleValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.SBYTE_VALUE: {
				SByteValue sByteValue = (SByteValue)theEObject;
				T result = caseSByteValue(sByteValue);
				if (result == null) result = caseIntegerValue(sByteValue);
				if (result == null) result = caseNumberValue(sByteValue);
				if (result == null) result = caseSimpleValue(sByteValue);
				if (result == null) result = caseConstantExpression(sByteValue);
				if (result == null) result = caseCollectableExpression(sByteValue);
				if (result == null) result = caseAnnotationValue(sByteValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.INT16_VALUE: {
				Int16Value int16Value = (Int16Value)theEObject;
				T result = caseInt16Value(int16Value);
				if (result == null) result = caseIntegerValue(int16Value);
				if (result == null) result = caseNumberValue(int16Value);
				if (result == null) result = caseSimpleValue(int16Value);
				if (result == null) result = caseConstantExpression(int16Value);
				if (result == null) result = caseCollectableExpression(int16Value);
				if (result == null) result = caseAnnotationValue(int16Value);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.INT32_VALUE: {
				Int32Value int32Value = (Int32Value)theEObject;
				T result = caseInt32Value(int32Value);
				if (result == null) result = caseIntegerValue(int32Value);
				if (result == null) result = caseNumberValue(int32Value);
				if (result == null) result = caseSimpleValue(int32Value);
				if (result == null) result = caseConstantExpression(int32Value);
				if (result == null) result = caseCollectableExpression(int32Value);
				if (result == null) result = caseAnnotationValue(int32Value);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.INT64_VALUE: {
				Int64Value int64Value = (Int64Value)theEObject;
				T result = caseInt64Value(int64Value);
				if (result == null) result = caseIntegerValue(int64Value);
				if (result == null) result = caseNumberValue(int64Value);
				if (result == null) result = caseSimpleValue(int64Value);
				if (result == null) result = caseConstantExpression(int64Value);
				if (result == null) result = caseCollectableExpression(int64Value);
				if (result == null) result = caseAnnotationValue(int64Value);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.DATE_TIME_OFFSET_VALUE: {
				DateTimeOffsetValue dateTimeOffsetValue = (DateTimeOffsetValue)theEObject;
				T result = caseDateTimeOffsetValue(dateTimeOffsetValue);
				if (result == null) result = caseSimpleValue(dateTimeOffsetValue);
				if (result == null) result = caseConstantExpression(dateTimeOffsetValue);
				if (result == null) result = caseCollectableExpression(dateTimeOffsetValue);
				if (result == null) result = caseAnnotationValue(dateTimeOffsetValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.BYTE_VALUE: {
				ByteValue byteValue = (ByteValue)theEObject;
				T result = caseByteValue(byteValue);
				if (result == null) result = caseIntegerValue(byteValue);
				if (result == null) result = caseNumberValue(byteValue);
				if (result == null) result = caseSimpleValue(byteValue);
				if (result == null) result = caseConstantExpression(byteValue);
				if (result == null) result = caseCollectableExpression(byteValue);
				if (result == null) result = caseAnnotationValue(byteValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.INTEGER_VALUE: {
				IntegerValue integerValue = (IntegerValue)theEObject;
				T result = caseIntegerValue(integerValue);
				if (result == null) result = caseNumberValue(integerValue);
				if (result == null) result = caseSimpleValue(integerValue);
				if (result == null) result = caseConstantExpression(integerValue);
				if (result == null) result = caseCollectableExpression(integerValue);
				if (result == null) result = caseAnnotationValue(integerValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.FLOAT_VALUE: {
				FloatValue floatValue = (FloatValue)theEObject;
				T result = caseFloatValue(floatValue);
				if (result == null) result = caseNumberValue(floatValue);
				if (result == null) result = caseSimpleValue(floatValue);
				if (result == null) result = caseConstantExpression(floatValue);
				if (result == null) result = caseCollectableExpression(floatValue);
				if (result == null) result = caseAnnotationValue(floatValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdataPackage.ENUM_VALUE: {
				EnumValue enumValue = (EnumValue)theEObject;
				T result = caseEnumValue(enumValue);
				if (result == null) result = caseDynamicExpression(enumValue);
				if (result == null) result = caseAnnotationValue(enumValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntityType(EntityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProperty(Property object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexType(ComplexType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Association</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Association</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssociation(Association object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Navigation Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Navigation Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNavigationProperty(NavigationProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Import</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Import</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionImport(FunctionImport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameter(Parameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntitySet(EntitySet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Association Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Association Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssociationSet(AssociationSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRole(Role object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Association Set End</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Association Set End</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssociationSetEnd(AssociationSetEnd object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Referential Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Referential Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferentialConstraint(ReferentialConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IProperty Type Usage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IProperty Type Usage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPropertyTypeUsage(IPropertyTypeUsage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleType(SimpleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IParameter Type Usage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IParameter Type Usage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIParameterTypeUsage(IParameterTypeUsage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinding(Binding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValueTerm(ValueTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValueAnnotation(ValueAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeAnnotation(TypeAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IAnnotation Target</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IAnnotation Target</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIAnnotationTarget(IAnnotationTarget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntityContainer(EntityContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumType(EnumType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Member</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Member</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumMember(EnumMember object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IDocumentable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IDocumentable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIDocumentable(IDocumentable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Documentation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Documentation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocumentation(Documentation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IType Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IType Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseITypeTerm(ITypeTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotation(Annotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EDMX Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EDMX Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEDMXSet(EDMXSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity Type Usage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity Type Usage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntityTypeUsage(EntityTypeUsage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Usage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Usage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeUsage(ComplexTypeUsage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Type Usage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Type Usage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumTypeUsage(EnumTypeUsage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Type Usage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Type Usage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleTypeUsage(SimpleTypeUsage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IFunction Return Type Usage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IFunction Return Type Usage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIFunctionReturnTypeUsage(IFunctionReturnTypeUsage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EDMX Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EDMX Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEDMXReference(EDMXReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EDMX Annotations Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EDMX Annotations Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEDMXAnnotationsReference(EDMXAnnotationsReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Schema</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Schema</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSchema(Schema object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Service</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Service</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataService(DataService object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Include Restriction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Include Restriction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIncludeRestriction(IncludeRestriction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleValue(SimpleValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathValue(PathValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Using</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Using</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUsing(Using object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordValue(RecordValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IRecord Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IRecord Value Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIRecordValueType(IRecordValueType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotationValue(AnnotationValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Return Entity Type Usage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Return Entity Type Usage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReturnEntityTypeUsage(ReturnEntityTypeUsage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyMapping(Map.Entry<Property, Property> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Collection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Collection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValueCollection(ValueCollection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property To Value Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property To Value Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyToValueMap(Map.Entry<Property, AnnotationValue> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dynamic Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dynamic Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDynamicExpression(DynamicExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstantExpression(ConstantExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanValue(BooleanValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringValue(StringValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryValue(BinaryValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Date Time Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Date Time Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDateTimeValue(DateTimeValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Decimal Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decimal Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecimalValue(DecimalValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumberValue(NumberValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Guid Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Guid Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGuidValue(GuidValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimeValue(TimeValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Collectable Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Collectable Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCollectableExpression(CollectableExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleValue(SingleValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Double Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Double Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDoubleValue(DoubleValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SByte Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SByte Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSByteValue(SByteValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int16 Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int16 Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInt16Value(Int16Value object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int32 Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int32 Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInt32Value(Int32Value object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int64 Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int64 Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInt64Value(Int64Value object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Date Time Offset Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Date Time Offset Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDateTimeOffsetValue(DateTimeOffsetValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Byte Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Byte Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseByteValue(ByteValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntegerValue(IntegerValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloatValue(FloatValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumValue(EnumValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EDMX</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EDMX</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEDMX(EDMX object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OdataSwitch
