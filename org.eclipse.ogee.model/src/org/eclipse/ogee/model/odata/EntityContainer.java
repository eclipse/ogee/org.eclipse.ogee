/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.EntityContainer#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EntityContainer#getEntitySets <em>Entity Sets</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EntityContainer#isDefault <em>Default</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EntityContainer#getAssociationSets <em>Association Sets</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EntityContainer#getFunctionImports <em>Function Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getEntityContainer()
 * @model
 * @generated
 */
public interface EntityContainer extends IAnnotationTarget, IDocumentable {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEntityContainer_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EntityContainer#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Entity Sets</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.EntitySet}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity Sets</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Sets</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEntityContainer_EntitySets()
	 * @model containment="true"
	 * @generated
	 */
	EList<EntitySet> getEntitySets();

	/**
	 * Returns the value of the '<em><b>Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default</em>' attribute.
	 * @see #setDefault(boolean)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEntityContainer_Default()
	 * @model
	 * @generated
	 */
	boolean isDefault();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EntityContainer#isDefault <em>Default</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default</em>' attribute.
	 * @see #isDefault()
	 * @generated
	 */
	void setDefault(boolean value);

	/**
	 * Returns the value of the '<em><b>Association Sets</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.AssociationSet}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association Sets</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association Sets</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEntityContainer_AssociationSets()
	 * @model containment="true"
	 * @generated
	 */
	EList<AssociationSet> getAssociationSets();

	/**
	 * Returns the value of the '<em><b>Function Imports</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.FunctionImport}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Imports</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEntityContainer_FunctionImports()
	 * @model containment="true"
	 * @generated
	 */
	EList<FunctionImport> getFunctionImports();

} // EntityContainer
