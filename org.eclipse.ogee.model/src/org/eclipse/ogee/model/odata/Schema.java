/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schema</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getNamespace <em>Namespace</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getContainers <em>Containers</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getEntityTypes <em>Entity Types</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getAssociations <em>Associations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getEnumTypes <em>Enum Types</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getComplexTypes <em>Complex Types</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getUsings <em>Usings</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getValueTerms <em>Value Terms</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getValueAnnotations <em>Value Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getTypeAnnotations <em>Type Annotations</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getAlias <em>Alias</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getClassifiers <em>Classifiers</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Schema#getDataServices <em>Data Services</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema()
 * @model
 * @generated
 */
public interface Schema extends IDocumentable {
	/**
	 * Returns the value of the '<em><b>Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Namespace</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Namespace</em>' attribute.
	 * @see #setNamespace(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_Namespace()
	 * @model
	 * @generated
	 */
	String getNamespace();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.Schema#getNamespace <em>Namespace</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Namespace</em>' attribute.
	 * @see #getNamespace()
	 * @generated
	 */
	void setNamespace(String value);

	/**
	 * Returns the value of the '<em><b>Containers</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.EntityContainer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containers</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_Containers()
	 * @model containment="true"
	 * @generated
	 */
	EList<EntityContainer> getContainers();

	/**
	 * Returns the value of the '<em><b>Entity Types</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.EntityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Types</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_EntityTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<EntityType> getEntityTypes();

	/**
	 * Returns the value of the '<em><b>Associations</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.Association}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associations</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_Associations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Association> getAssociations();

	/**
	 * Returns the value of the '<em><b>Enum Types</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.EnumType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enum Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum Types</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_EnumTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<EnumType> getEnumTypes();

	/**
	 * Returns the value of the '<em><b>Complex Types</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.ComplexType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Complex Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Complex Types</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_ComplexTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComplexType> getComplexTypes();

	/**
	 * Returns the value of the '<em><b>Usings</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.Using}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Usings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Usings</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_Usings()
	 * @model containment="true"
	 * @generated
	 */
	EList<Using> getUsings();

	/**
	 * Returns the value of the '<em><b>Value Terms</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.ValueTerm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Terms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Terms</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_ValueTerms()
	 * @model containment="true"
	 * @generated
	 */
	EList<ValueTerm> getValueTerms();

	/**
	 * Returns the value of the '<em><b>Value Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.ValueAnnotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Annotations</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_ValueAnnotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ValueAnnotation> getValueAnnotations();

	/**
	 * Returns the value of the '<em><b>Type Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.TypeAnnotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Annotations</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_TypeAnnotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<TypeAnnotation> getTypeAnnotations();

	/**
	 * Returns the value of the '<em><b>Alias</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alias</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alias</em>' attribute.
	 * @see #setAlias(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_Alias()
	 * @model
	 * @generated
	 */
	String getAlias();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.Schema#getAlias <em>Alias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alias</em>' attribute.
	 * @see #getAlias()
	 * @generated
	 */
	void setAlias(String value);

	/**
	 * Returns the value of the '<em><b>Classifiers</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.SchemaClassifier}.
	 * The literals are from the enumeration {@link org.eclipse.ogee.model.odata.SchemaClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifiers</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifiers</em>' attribute list.
	 * @see org.eclipse.ogee.model.odata.SchemaClassifier
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_Classifiers()
	 * @model
	 * @generated
	 */
	EList<SchemaClassifier> getClassifiers();

	/**
	 * Returns the value of the '<em><b>Data Services</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.DataService}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.ogee.model.odata.DataService#getSchemata <em>Schemata</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Services</em>' reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getSchema_DataServices()
	 * @see org.eclipse.ogee.model.odata.DataService#getSchemata
	 * @model opposite="schemata"
	 * @generated
	 */
	EList<DataService> getDataServices();

} // Schema
