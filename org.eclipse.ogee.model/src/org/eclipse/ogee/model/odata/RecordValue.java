/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.RecordValue#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.RecordValue#getPropertyValues <em>Property Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getRecordValue()
 * @model
 * @generated
 */
public interface RecordValue extends DynamicExpression, CollectableExpression {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(IRecordValueType)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getRecordValue_Type()
	 * @model
	 * @generated
	 */
	IRecordValueType getType();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.RecordValue#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(IRecordValueType value);

	/**
	 * Returns the value of the '<em><b>Property Values</b></em>' map.
	 * The key is of type {@link org.eclipse.ogee.model.odata.Property},
	 * and the value is of type {@link org.eclipse.ogee.model.odata.AnnotationValue},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Values</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Values</em>' map.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getRecordValue_PropertyValues()
	 * @model mapType="org.eclipse.ogee.model.odata.PropertyToValueMap<org.eclipse.ogee.model.odata.Property, org.eclipse.ogee.model.odata.AnnotationValue>"
	 * @generated
	 */
	EMap<Property, AnnotationValue> getPropertyValues();

} // RecordValue
