/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Value Term Targets</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.ogee.model.odata.OdataPackage#getValueTermTargets()
 * @model
 * @generated
 */
public enum ValueTermTargets implements Enumerator {
	/**
	 * The '<em><b>Complex Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPLEX_TYPE_VALUE
	 * @generated
	 * @ordered
	 */
	COMPLEX_TYPE(0, "ComplexType", "ComplexType"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Entity Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENTITY_TYPE_VALUE
	 * @generated
	 * @ordered
	 */
	ENTITY_TYPE(1, "EntityType", "EntityType"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Property</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROPERTY_VALUE
	 * @generated
	 * @ordered
	 */
	PROPERTY(2, "Property", "Property"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Function</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FUNCTION_VALUE
	 * @generated
	 * @ordered
	 */
	FUNCTION(4, "Function", "Function"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Function Parameter</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FUNCTION_PARAMETER_VALUE
	 * @generated
	 * @ordered
	 */
	FUNCTION_PARAMETER(5, "FunctionParameter", "FunctionParameter"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Entity Set</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENTITY_SET_VALUE
	 * @generated
	 * @ordered
	 */
	ENTITY_SET(6, "EntitySet", "EntitySet"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Association Set</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSOCIATION_SET_VALUE
	 * @generated
	 * @ordered
	 */
	ASSOCIATION_SET(7, "AssociationSet", "Association"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Entity Container</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENTITY_CONTAINER_VALUE
	 * @generated
	 * @ordered
	 */
	ENTITY_CONTAINER(8, "EntityContainer", "EntityContainer"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Role</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ROLE_VALUE
	 * @generated
	 * @ordered
	 */
	ROLE(9, "Role", "Role"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Referential Constraint</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REFERENTIAL_CONSTRAINT_VALUE
	 * @generated
	 * @ordered
	 */
	REFERENTIAL_CONSTRAINT(10, "ReferentialConstraint", "ReferentialConstraint"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Association Set End</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSOCIATION_SET_END_VALUE
	 * @generated
	 * @ordered
	 */
	ASSOCIATION_SET_END(11, "AssociationSetEnd", "AssociationSetEnd"), //$NON-NLS-1$ //$NON-NLS-2$
	
	/**
	 * The '<em><b>Navigation Property</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NAVIGATION_PROPERTY_VALUE
	 * @generated
	 * @ordered
	 */
	NAVIGATION_PROPERTY(3, "NavigationProperty", "NavigationProperty"); //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Complex Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Complex Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPLEX_TYPE
	 * @model name="ComplexType"
	 * @generated
	 * @ordered
	 */
	public static final int COMPLEX_TYPE_VALUE = 0;

	/**
	 * The '<em><b>Entity Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Entity Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENTITY_TYPE
	 * @model name="EntityType"
	 * @generated
	 * @ordered
	 */
	public static final int ENTITY_TYPE_VALUE = 1;

	/**
	 * The '<em><b>Property</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Property</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PROPERTY
	 * @model name="Property"
	 * @generated
	 * @ordered
	 */
	public static final int PROPERTY_VALUE = 2;

	/**
	 * The '<em><b>Function</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Function</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FUNCTION
	 * @model name="Function"
	 * @generated
	 * @ordered
	 */
	public static final int FUNCTION_VALUE = 4;

	/**
	 * The '<em><b>Function Parameter</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Function Parameter</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FUNCTION_PARAMETER
	 * @model name="FunctionParameter"
	 * @generated
	 * @ordered
	 */
	public static final int FUNCTION_PARAMETER_VALUE = 5;

	/**
	 * The '<em><b>Entity Set</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Entity Set</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENTITY_SET
	 * @model name="EntitySet"
	 * @generated
	 * @ordered
	 */
	public static final int ENTITY_SET_VALUE = 6;

	/**
	 * The '<em><b>Association Set</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Association Set</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSOCIATION_SET
	 * @model name="AssociationSet" literal="Association"
	 * @generated
	 * @ordered
	 */
	public static final int ASSOCIATION_SET_VALUE = 7;

	/**
	 * The '<em><b>Entity Container</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Entity Container</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENTITY_CONTAINER
	 * @model name="EntityContainer"
	 * @generated
	 * @ordered
	 */
	public static final int ENTITY_CONTAINER_VALUE = 8;

	/**
	 * The '<em><b>Role</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Role</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ROLE
	 * @model name="Role"
	 * @generated
	 * @ordered
	 */
	public static final int ROLE_VALUE = 9;

	/**
	 * The '<em><b>Referential Constraint</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Referential Constraint</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REFERENTIAL_CONSTRAINT
	 * @model name="ReferentialConstraint"
	 * @generated
	 * @ordered
	 */
	public static final int REFERENTIAL_CONSTRAINT_VALUE = 10;

	/**
	 * The '<em><b>Association Set End</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Association Set End</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSOCIATION_SET_END
	 * @model name="AssociationSetEnd"
	 * @generated
	 * @ordered
	 */
	public static final int ASSOCIATION_SET_END_VALUE = 11;

	/**
	 * The '<em><b>Navigation Property</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Navigation Property</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NAVIGATION_PROPERTY
	 * @model name="NavigationProperty"
	 * @generated
	 * @ordered
	 */
	public static final int NAVIGATION_PROPERTY_VALUE = 3;

	/**
	 * An array of all the '<em><b>Value Term Targets</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ValueTermTargets[] VALUES_ARRAY =
		new ValueTermTargets[] {
			COMPLEX_TYPE,
			ENTITY_TYPE,
			PROPERTY,
			FUNCTION,
			FUNCTION_PARAMETER,
			ENTITY_SET,
			ASSOCIATION_SET,
			ENTITY_CONTAINER,
			ROLE,
			REFERENTIAL_CONSTRAINT,
			ASSOCIATION_SET_END,
			NAVIGATION_PROPERTY,
		};

	/**
	 * A public read-only list of all the '<em><b>Value Term Targets</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ValueTermTargets> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Value Term Targets</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ValueTermTargets get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ValueTermTargets result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Value Term Targets</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ValueTermTargets getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ValueTermTargets result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Value Term Targets</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ValueTermTargets get(int value) {
		switch (value) {
			case COMPLEX_TYPE_VALUE: return COMPLEX_TYPE;
			case ENTITY_TYPE_VALUE: return ENTITY_TYPE;
			case PROPERTY_VALUE: return PROPERTY;
			case FUNCTION_VALUE: return FUNCTION;
			case FUNCTION_PARAMETER_VALUE: return FUNCTION_PARAMETER;
			case ENTITY_SET_VALUE: return ENTITY_SET;
			case ASSOCIATION_SET_VALUE: return ASSOCIATION_SET;
			case ENTITY_CONTAINER_VALUE: return ENTITY_CONTAINER;
			case ROLE_VALUE: return ROLE;
			case REFERENTIAL_CONSTRAINT_VALUE: return REFERENTIAL_CONSTRAINT;
			case ASSOCIATION_SET_END_VALUE: return ASSOCIATION_SET_END;
			case NAVIGATION_PROPERTY_VALUE: return NAVIGATION_PROPERTY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ValueTermTargets(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ValueTermTargets
