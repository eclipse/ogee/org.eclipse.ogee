/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Import</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.FunctionImport#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.FunctionImport#getParameters <em>Parameters</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.FunctionImport#getBinding <em>Binding</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.FunctionImport#isSideEffecting <em>Side Effecting</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.FunctionImport#getReturnType <em>Return Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getFunctionImport()
 * @model
 * @generated
 */
public interface FunctionImport extends IAnnotationTarget, IDocumentable {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getFunctionImport_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.FunctionImport#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getFunctionImport_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parameter> getParameters();

	/**
	 * Returns the value of the '<em><b>Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding</em>' containment reference.
	 * @see #setBinding(Binding)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getFunctionImport_Binding()
	 * @model containment="true"
	 * @generated
	 */
	Binding getBinding();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.FunctionImport#getBinding <em>Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binding</em>' containment reference.
	 * @see #getBinding()
	 * @generated
	 */
	void setBinding(Binding value);

	/**
	 * Returns the value of the '<em><b>Side Effecting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Side Effecting</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Side Effecting</em>' attribute.
	 * @see #setSideEffecting(boolean)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getFunctionImport_SideEffecting()
	 * @model
	 * @generated
	 */
	boolean isSideEffecting();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.FunctionImport#isSideEffecting <em>Side Effecting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Side Effecting</em>' attribute.
	 * @see #isSideEffecting()
	 * @generated
	 */
	void setSideEffecting(boolean value);

	/**
	 * Returns the value of the '<em><b>Return Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Type</em>' containment reference.
	 * @see #setReturnType(IFunctionReturnTypeUsage)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getFunctionImport_ReturnType()
	 * @model containment="true"
	 * @generated
	 */
	IFunctionReturnTypeUsage getReturnType();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.FunctionImport#getReturnType <em>Return Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Type</em>' containment reference.
	 * @see #getReturnType()
	 * @generated
	 */
	void setReturnType(IFunctionReturnTypeUsage value);

} // FunctionImport
