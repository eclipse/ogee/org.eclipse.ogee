/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>EDM Types</b></em>', and utility methods for working with them. <!--
 * end-user-doc -->
 * 
 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMTypes()
 * @model
 * @generated
 */
public enum EDMTypes implements Enumerator {
	/**
	 * The '<em><b>String</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #STRING_VALUE
	 * @generated
	 * @ordered
	 */
	STRING(0, "String", "Edm.String"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Decimal</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #DECIMAL_VALUE
	 * @generated
	 * @ordered
	 */
	DECIMAL(1, "Decimal", "Edm.Decimal"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Binary</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #BINARY_VALUE
	 * @generated
	 * @ordered
	 */
	BINARY(2, "Binary", "Edm.Binary"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Byte</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #BYTE_VALUE
	 * @generated
	 * @ordered
	 */
	BYTE(4, "Byte", "Edm.Byte"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Boolean</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #BOOLEAN_VALUE
	 * @generated
	 * @ordered
	 */
	BOOLEAN(5, "Boolean", "Edm.Boolean"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Single</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #SINGLE_VALUE
	 * @generated
	 * @ordered
	 */
	SINGLE(6, "Single", "Edm.Single"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Double</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #DOUBLE_VALUE
	 * @generated
	 * @ordered
	 */
	DOUBLE(7, "Double", "Edm.Double"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Guid</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #GUID_VALUE
	 * @generated
	 * @ordered
	 */
	GUID(8, "Guid", "Edm.Guid"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Date Time Offset</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #DATE_TIME_OFFSET_VALUE
	 * @generated
	 * @ordered
	 */
	DATE_TIME_OFFSET(9, "DateTimeOffset", "Edm.DateTimeOffset"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Int16</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #INT16_VALUE
	 * @generated
	 * @ordered
	 */
	INT16(10, "Int16", "Edm.Int16"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Int32</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #INT32_VALUE
	 * @generated
	 * @ordered
	 */
	INT32(11, "Int32", "Edm.Int32"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Int64</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #INT64_VALUE
	 * @generated
	 * @ordered
	 */
	INT64(12, "Int64", "Edm.Int64"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>SByte</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #SBYTE_VALUE
	 * @generated
	 * @ordered
	 */
	SBYTE(13, "SByte", "Edm.SByte"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Time</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #TIME_VALUE
	 * @generated
	 * @ordered
	 */
	TIME(14, "Time", "Edm.Time"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Date Time</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #DATE_TIME_VALUE
	 * @generated
	 * @ordered
	 */
	DATE_TIME(15, "DateTime", "Edm.DateTime"); //$NON-NLS-1$//$NON-NLS-2$

	/**
	 * The '<em><b>String</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>String</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #STRING
	 * @model name="String" literal="Edm.String"
	 * @generated
	 * @ordered
	 */
	public static final int STRING_VALUE = 0;

	/**
	 * The '<em><b>Decimal</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Decimal</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #DECIMAL
	 * @model name="Decimal" literal="Edm.Decimal"
	 * @generated
	 * @ordered
	 */
	public static final int DECIMAL_VALUE = 1;

	/**
	 * The '<em><b>Binary</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Binary</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #BINARY
	 * @model name="Binary" literal="Edm.Binary"
	 * @generated
	 * @ordered
	 */
	public static final int BINARY_VALUE = 2;

	/**
	 * The '<em><b>Byte</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Byte</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #BYTE
	 * @model name="Byte" literal="Edm.Byte"
	 * @generated
	 * @ordered
	 */
	public static final int BYTE_VALUE = 4;

	/**
	 * The '<em><b>Boolean</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Boolean</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #BOOLEAN
	 * @model name="Boolean" literal="Edm.Boolean"
	 * @generated
	 * @ordered
	 */
	public static final int BOOLEAN_VALUE = 5;

	/**
	 * The '<em><b>Single</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Single</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #SINGLE
	 * @model name="Single" literal="Edm.Single"
	 * @generated
	 * @ordered
	 */
	public static final int SINGLE_VALUE = 6;

	/**
	 * The '<em><b>Double</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Double</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #DOUBLE
	 * @model name="Double" literal="Edm.Double"
	 * @generated
	 * @ordered
	 */
	public static final int DOUBLE_VALUE = 7;

	/**
	 * The '<em><b>Guid</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Guid</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #GUID
	 * @model name="Guid" literal="Edm.Guid"
	 * @generated
	 * @ordered
	 */
	public static final int GUID_VALUE = 8;

	/**
	 * The '<em><b>Date Time Offset</b></em>' literal value. <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Date Time Offset</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #DATE_TIME_OFFSET
	 * @model name="DateTimeOffset" literal="Edm.DateTimeOffset"
	 * @generated
	 * @ordered
	 */
	public static final int DATE_TIME_OFFSET_VALUE = 9;

	/**
	 * The '<em><b>Int16</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Int16</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #INT16
	 * @model name="Int16" literal="Edm.Int16"
	 * @generated
	 * @ordered
	 */
	public static final int INT16_VALUE = 10;

	/**
	 * The '<em><b>Int32</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Int32</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #INT32
	 * @model name="Int32" literal="Edm.Int32"
	 * @generated
	 * @ordered
	 */
	public static final int INT32_VALUE = 11;

	/**
	 * The '<em><b>Int64</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Int64</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #INT64
	 * @model name="Int64" literal="Edm.Int64"
	 * @generated
	 * @ordered
	 */
	public static final int INT64_VALUE = 12;

	/**
	 * The '<em><b>SByte</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SByte</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #SBYTE
	 * @model name="SByte" literal="Edm.SByte"
	 * @generated
	 * @ordered
	 */
	public static final int SBYTE_VALUE = 13;

	/**
	 * The '<em><b>Time</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Time</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #TIME
	 * @model name="Time" literal="Edm.Time"
	 * @generated
	 * @ordered
	 */
	public static final int TIME_VALUE = 14;

	/**
	 * The '<em><b>Date Time</b></em>' literal value. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Date Time</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #DATE_TIME
	 * @model name="DateTime" literal="Edm.DateTime"
	 * @generated
	 * @ordered
	 */
	public static final int DATE_TIME_VALUE = 15;

	/**
	 * An array of all the '<em><b>EDM Types</b></em>' enumerators. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final EDMTypes[] VALUES_ARRAY = new EDMTypes[] { STRING,
			DECIMAL, BINARY, BYTE, BOOLEAN, SINGLE, DOUBLE, GUID,
			DATE_TIME_OFFSET, INT16, INT32, INT64, SBYTE, TIME, DATE_TIME, };

	/**
	 * A public read-only list of all the '<em><b>EDM Types</b></em>'
	 * enumerators. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<EDMTypes> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>EDM Types</b></em>' literal with the specified
	 * literal value. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static EDMTypes get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EDMTypes result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>EDM Types</b></em>' literal with the specified name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static EDMTypes getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EDMTypes result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>EDM Types</b></em>' literal with the specified
	 * integer value. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static EDMTypes get(int value) {
		switch (value) {
		case STRING_VALUE:
			return STRING;
		case DECIMAL_VALUE:
			return DECIMAL;
		case BINARY_VALUE:
			return BINARY;
		case BYTE_VALUE:
			return BYTE;
		case BOOLEAN_VALUE:
			return BOOLEAN;
		case SINGLE_VALUE:
			return SINGLE;
		case DOUBLE_VALUE:
			return DOUBLE;
		case GUID_VALUE:
			return GUID;
		case DATE_TIME_OFFSET_VALUE:
			return DATE_TIME_OFFSET;
		case INT16_VALUE:
			return INT16;
		case INT32_VALUE:
			return INT32;
		case INT64_VALUE:
			return INT64;
		case SBYTE_VALUE:
			return SBYTE;
		case TIME_VALUE:
			return TIME;
		case DATE_TIME_VALUE:
			return DATE_TIME;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	private EDMTypes(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string
	 * representation. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // EDMTypes
