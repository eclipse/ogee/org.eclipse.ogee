/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EDMX Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMXReference#getReferencedEDMX <em>Referenced EDMX</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXReference()
 * @model
 * @generated
 */
public interface EDMXReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Referenced EDMX</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced EDMX</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced EDMX</em>' reference.
	 * @see #setReferencedEDMX(EDMX)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXReference_ReferencedEDMX()
	 * @model required="true"
	 * @generated
	 */
	EDMX getReferencedEDMX();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EDMXReference#getReferencedEDMX <em>Referenced EDMX</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced EDMX</em>' reference.
	 * @see #getReferencedEDMX()
	 * @generated
	 */
	void setReferencedEDMX(EDMX value);

} // EDMXReference
