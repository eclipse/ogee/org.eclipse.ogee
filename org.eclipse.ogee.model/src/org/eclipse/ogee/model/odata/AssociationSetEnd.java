/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association Set End</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.AssociationSetEnd#getRole <em>Role</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.AssociationSetEnd#getEntitySet <em>Entity Set</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getAssociationSetEnd()
 * @model
 * @generated
 */
public interface AssociationSetEnd extends IDocumentable, IAnnotationTarget {
	/**
	 * Returns the value of the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' reference.
	 * @see #setRole(Role)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getAssociationSetEnd_Role()
	 * @model required="true"
	 * @generated
	 */
	Role getRole();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.AssociationSetEnd#getRole <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' reference.
	 * @see #getRole()
	 * @generated
	 */
	void setRole(Role value);

	/**
	 * Returns the value of the '<em><b>Entity Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity Set</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Set</em>' reference.
	 * @see #setEntitySet(EntitySet)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getAssociationSetEnd_EntitySet()
	 * @model required="true"
	 * @generated
	 */
	EntitySet getEntitySet();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.AssociationSetEnd#getEntitySet <em>Entity Set</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity Set</em>' reference.
	 * @see #getEntitySet()
	 * @generated
	 */
	void setEntitySet(EntitySet value);

} // AssociationSetEnd
