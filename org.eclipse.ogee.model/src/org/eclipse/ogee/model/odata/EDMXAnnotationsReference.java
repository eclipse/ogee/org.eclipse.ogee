/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EDMX Annotations Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMXAnnotationsReference#getIncludeRestrictions <em>Include Restrictions</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMXAnnotationsReference#getReferencedEDMX <em>Referenced EDMX</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXAnnotationsReference()
 * @model
 * @generated
 */
public interface EDMXAnnotationsReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Include Restrictions</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.IncludeRestriction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Include Restrictions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include Restrictions</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXAnnotationsReference_IncludeRestrictions()
	 * @model containment="true"
	 * @generated
	 */
	EList<IncludeRestriction> getIncludeRestrictions();

	/**
	 * Returns the value of the '<em><b>Referenced EDMX</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced EDMX</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced EDMX</em>' reference.
	 * @see #setReferencedEDMX(EDMX)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXAnnotationsReference_ReferencedEDMX()
	 * @model required="true"
	 * @generated
	 */
	EDMX getReferencedEDMX();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EDMXAnnotationsReference#getReferencedEDMX <em>Referenced EDMX</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced EDMX</em>' reference.
	 * @see #getReferencedEDMX()
	 * @generated
	 */
	void setReferencedEDMX(EDMX value);

} // EDMXAnnotationsReference
