/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.ValueAnnotation#getTerm <em>Term</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ValueAnnotation#getAnnotationValue <em>Annotation Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getValueAnnotation()
 * @model
 * @generated
 */
public interface ValueAnnotation extends Annotation, IDocumentable {
	/**
	 * Returns the value of the '<em><b>Term</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.ogee.model.odata.ValueTerm#getValueAnnotations <em>Value Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Term</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Term</em>' reference.
	 * @see #setTerm(ValueTerm)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getValueAnnotation_Term()
	 * @see org.eclipse.ogee.model.odata.ValueTerm#getValueAnnotations
	 * @model opposite="valueAnnotations" required="true"
	 * @generated
	 */
	ValueTerm getTerm();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ValueAnnotation#getTerm <em>Term</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Term</em>' reference.
	 * @see #getTerm()
	 * @generated
	 */
	void setTerm(ValueTerm value);

	/**
	 * Returns the value of the '<em><b>Annotation Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Value</em>' containment reference.
	 * @see #setAnnotationValue(AnnotationValue)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getValueAnnotation_AnnotationValue()
	 * @model containment="true"
	 * @generated
	 */
	AnnotationValue getAnnotationValue();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ValueAnnotation#getAnnotationValue <em>Annotation Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Value</em>' containment reference.
	 * @see #getAnnotationValue()
	 * @generated
	 */
	void setAnnotationValue(AnnotationValue value);

} // ValueAnnotation
