/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EDMX</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMX#getDataService <em>Data Service</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMX#getAnnotationsReferences <em>Annotations References</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMX#getReferences <em>References</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMX#getURI <em>URI</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMX()
 * @model
 * @generated
 */
public interface EDMX extends IDocumentable {
	/**
	 * Returns the value of the '<em><b>Data Service</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Service</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Service</em>' containment reference.
	 * @see #setDataService(DataService)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMX_DataService()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DataService getDataService();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EDMX#getDataService <em>Data Service</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Service</em>' containment reference.
	 * @see #getDataService()
	 * @generated
	 */
	void setDataService(DataService value);

	/**
	 * Returns the value of the '<em><b>Annotations References</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.EDMXAnnotationsReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotations References</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotations References</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMX_AnnotationsReferences()
	 * @model containment="true"
	 * @generated
	 */
	EList<EDMXAnnotationsReference> getAnnotationsReferences();

	/**
	 * Returns the value of the '<em><b>References</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.EDMXReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>References</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMX_References()
	 * @model containment="true"
	 * @generated
	 */
	EList<EDMXReference> getReferences();

	/**
	 * Returns the value of the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>URI</em>' attribute.
	 * @see #setURI(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMX_URI()
	 * @model
	 * @generated
	 */
	String getURI();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EDMX#getURI <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>URI</em>' attribute.
	 * @see #getURI()
	 * @generated
	 */
	void setURI(String value);

} // EDMX
