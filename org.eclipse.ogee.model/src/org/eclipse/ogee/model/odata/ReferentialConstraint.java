/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referential Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.ReferentialConstraint#getPrincipal <em>Principal</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ReferentialConstraint#getDependent <em>Dependent</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.ReferentialConstraint#getKeyMappings <em>Key Mappings</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getReferentialConstraint()
 * @model
 * @generated
 */
public interface ReferentialConstraint extends IAnnotationTarget, IDocumentable {
	/**
	 * Returns the value of the '<em><b>Principal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Principal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Principal</em>' reference.
	 * @see #setPrincipal(Role)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getReferentialConstraint_Principal()
	 * @model required="true"
	 * @generated
	 */
	Role getPrincipal();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ReferentialConstraint#getPrincipal <em>Principal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Principal</em>' reference.
	 * @see #getPrincipal()
	 * @generated
	 */
	void setPrincipal(Role value);

	/**
	 * Returns the value of the '<em><b>Dependent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependent</em>' reference.
	 * @see #setDependent(Role)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getReferentialConstraint_Dependent()
	 * @model required="true"
	 * @generated
	 */
	Role getDependent();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.ReferentialConstraint#getDependent <em>Dependent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dependent</em>' reference.
	 * @see #getDependent()
	 * @generated
	 */
	void setDependent(Role value);

	/**
	 * Returns the value of the '<em><b>Key Mappings</b></em>' map.
	 * The key is of type {@link org.eclipse.ogee.model.odata.Property},
	 * and the value is of type {@link org.eclipse.ogee.model.odata.Property},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key Mappings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key Mappings</em>' map.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getReferentialConstraint_KeyMappings()
	 * @model mapType="org.eclipse.ogee.model.odata.PropertyMapping<org.eclipse.ogee.model.odata.Property, org.eclipse.ogee.model.odata.Property>"
	 * @generated
	 */
	EMap<Property, Property> getKeyMappings();

} // ReferentialConstraint
