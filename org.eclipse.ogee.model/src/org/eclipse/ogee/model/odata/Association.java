/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.Association#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Association#getEnds <em>Ends</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Association#getReferentialConstraint <em>Referential Constraint</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.Association#getAssociationSets <em>Association Sets</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getAssociation()
 * @model
 * @generated
 */
public interface Association extends IAnnotationTarget, IDocumentable {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getAssociation_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.Association#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Ends</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ends</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ends</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getAssociation_Ends()
	 * @model containment="true" lower="2" upper="2"
	 * @generated
	 */
	EList<Role> getEnds();

	/**
	 * Returns the value of the '<em><b>Referential Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referential Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referential Constraint</em>' containment reference.
	 * @see #setReferentialConstraint(ReferentialConstraint)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getAssociation_ReferentialConstraint()
	 * @model containment="true"
	 * @generated
	 */
	ReferentialConstraint getReferentialConstraint();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.Association#getReferentialConstraint <em>Referential Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referential Constraint</em>' containment reference.
	 * @see #getReferentialConstraint()
	 * @generated
	 */
	void setReferentialConstraint(ReferentialConstraint value);

	/**
	 * Returns the value of the '<em><b>Association Sets</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.AssociationSet}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.ogee.model.odata.AssociationSet#getAssociation <em>Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association Sets</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association Sets</em>' reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getAssociation_AssociationSets()
	 * @see org.eclipse.ogee.model.odata.AssociationSet#getAssociation
	 * @model opposite="association"
	 * @generated
	 */
	EList<AssociationSet> getAssociationSets();

} // Association
