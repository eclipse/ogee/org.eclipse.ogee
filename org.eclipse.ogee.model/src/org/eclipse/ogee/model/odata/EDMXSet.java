/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.odata;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EDMX Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMXSet#getImportSource <em>Import Source</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMXSet#getImportDate <em>Import Date</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMXSet#getMainEDMX <em>Main EDMX</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMXSet#getEdmx <em>Edmx</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMXSet#getFullScopeEDMX <em>Full Scope EDMX</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMXSet#getAnnotationsScopeEDMX <em>Annotations Scope EDMX</em>}</li>
 *   <li>{@link org.eclipse.ogee.model.odata.EDMXSet#getSchemata <em>Schemata</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXSet()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='DelegateToValidationFW'"
 * @generated
 */
public interface EDMXSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Import Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Source</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Source</em>' attribute.
	 * @see #setImportSource(String)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXSet_ImportSource()
	 * @model
	 * @generated
	 */
	String getImportSource();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EDMXSet#getImportSource <em>Import Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import Source</em>' attribute.
	 * @see #getImportSource()
	 * @generated
	 */
	void setImportSource(String value);

	/**
	 * Returns the value of the '<em><b>Import Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Date</em>' attribute.
	 * @see #setImportDate(Date)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXSet_ImportDate()
	 * @model
	 * @generated
	 */
	Date getImportDate();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EDMXSet#getImportDate <em>Import Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import Date</em>' attribute.
	 * @see #getImportDate()
	 * @generated
	 */
	void setImportDate(Date value);

	/**
	 * Returns the value of the '<em><b>Main EDMX</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main EDMX</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main EDMX</em>' containment reference.
	 * @see #setMainEDMX(EDMX)
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXSet_MainEDMX()
	 * @model containment="true" required="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#edmx'"
	 * @generated
	 */
	EDMX getMainEDMX();

	/**
	 * Sets the value of the '{@link org.eclipse.ogee.model.odata.EDMXSet#getMainEDMX <em>Main EDMX</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main EDMX</em>' containment reference.
	 * @see #getMainEDMX()
	 * @generated
	 */
	void setMainEDMX(EDMX value);

	/**
	 * Returns the value of the '<em><b>Edmx</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edmx</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edmx</em>' attribute list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXSet_Edmx()
	 * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group'"
	 * @generated
	 */
	FeatureMap getEdmx();

	/**
	 * Returns the value of the '<em><b>Full Scope EDMX</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.EDMX}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Full Scope EDMX</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Full Scope EDMX</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXSet_FullScopeEDMX()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#edmx'"
	 * @generated
	 */
	EList<EDMX> getFullScopeEDMX();

	/**
	 * Returns the value of the '<em><b>Annotations Scope EDMX</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.EDMX}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotations Scope EDMX</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotations Scope EDMX</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXSet_AnnotationsScopeEDMX()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="group='#edmx'"
	 * @generated
	 */
	EList<EDMX> getAnnotationsScopeEDMX();

	/**
	 * Returns the value of the '<em><b>Schemata</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.ogee.model.odata.Schema}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schemata</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schemata</em>' containment reference list.
	 * @see org.eclipse.ogee.model.odata.OdataPackage#getEDMXSet_Schemata()
	 * @model containment="true" keys="namespace"
	 * @generated
	 */
	EList<Schema> getSchemata();

} // EDMXSet
