<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.ogee.model.api" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appinfo>
         <meta.schema plugin="org.eclipse.ogee.model.api" id="org.eclipse.ogee.model.api.schemata" name="OData Model API: Registered Schema Providers"/>
      </appinfo>
      <documentation>
         A schema groups model elements and defines a globally unique namespace for it. Typically, elements of the same schema depend on each other and will be used together in a common modeling context.

With regards to OData modeling schemata can be associated with data services, annotation sets or vocabularies. 
Especially vocabularies will be re-used very often. Some vocabularies may always be needed to express very basic modeling aspects, such as service capabilities, unit of measure and currency handling or the like.

Multiple schemata can be stored in files. However, a schema may also be provided by some implementation logic, being distributed like coding, always available consistently and out-of-the-box, independent from any file.

This extension point can be used to register schema provider classes used for OData modeling.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appinfo>
            <meta.element />
         </appinfo>
      </annotation>
      <complexType>
         <choice>
            <element ref="schema" minOccurs="1" maxOccurs="unbounded"/>
         </choice>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute translatable="true"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="schema">
      <annotation>
         <documentation>
            A schema that is provided by registration. It becomes available automatically without the need to import a file.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="namespace" type="string" use="required">
            <annotation>
               <documentation>
                  A unique identifier for the schema.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="uri" type="string">
            <annotation>
               <documentation>
                  A well-known URL that could be used to retrieve the original file.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="classifier" use="required">
            <annotation>
               <documentation>
                  A classification of the intended schema usage. The model supports these classifiers: Vocabulary, AnnotationSet and Service.
               </documentation>
            </annotation>
            <simpleType>
               <restriction base="string">
                  <enumeration value="Vocabulary">
                  </enumeration>
               </restriction>
            </simpleType>
         </attribute>
         <attribute name="description" type="string" use="required">
            <annotation>
               <documentation>
                  A user-displayable (translatable) schema description.
               </documentation>
               <appinfo>
                  <meta.attribute translatable="true"/>
               </appinfo>
            </annotation>
         </attribute>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  The class used as schema provider.
               </documentation>
               <appinfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.ogee.model.api.ISchemaProvider"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appinfo>
         <meta.section type="since"/>
      </appinfo>
      <documentation>
         0.4.0
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="examples"/>
      </appinfo>
      <documentation>
         The following vocabularies are provided as part of the core package already:
- CoreVocabulary
- CapabilityVocabulary
- MeasuresVocabulary
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="apiinfo"/>
      </appinfo>
      <documentation>
         The provider implements interface org.eclipse.ogee.model.api.ISchemaProvider.
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="implementation"/>
      </appinfo>
      <documentation>
         Adapter classes shall be derived from org.eclipse.ogee.model.api.vocabularies.SchemaAdapterImpl. For vocabulary adapters VocabularyAdapterImpl must be used as base class.
      </documentation>
   </annotation>


</schema>
