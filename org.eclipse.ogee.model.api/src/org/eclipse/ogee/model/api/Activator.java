/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
	
	public static String problemMarkerId = "org.eclipse.ogee.model.ProblemMarker"; //$NON-NLS-1$
	public static String PLUGIN_ID = "org.eclipse.ogee.model.api"; //$NON-NLS-1$

	private static BundleContext context;
	

	static BundleContext getContext() {
		return context;
	}


	public void start(BundleContext bundleContext) throws ModelAPIException {
		Activator.context = bundleContext;
	}

	public void stop(BundleContext bundleContext) throws ModelAPIException {
		Activator.context = null;
	}

}
