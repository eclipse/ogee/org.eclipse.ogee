/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

import java.util.List;

/**
 */
public interface IVocabularyContext {

	/**
	 * Provides the vocabulary instance for a given schema namespace and the EDMXSet of the context.
	 * The schema identified by the namespace must be classified as <I>Vocabulary</I>.
	 * <B>Missing vocabularies will be loaded automatically</B> if a schema provider has been registered.
	 * @param namespace The unique schema identifier
	 * @return The vocabulary
	 * @see IVocabulary
	 * @see "Extension Point org.eclipse.ogee.model.api.schemata"
	 */
	public IVocabulary provideVocabulary(String namespace) throws ModelAPIException;

	/**
	 * Returns the vocabulary instance for a given schema namespace and the EDMXSet of the context.
	 * The schema identified by the namespace must be classified as <I>Vocabulary</I>.
	 * Missing vocabularies will not be be loaded
	 * @param namespace The unique schema identifier
	 * @return The vocabulary
	 * @see IVocabulary
	 * @see "Extension Point org.eclipse.ogee.model.api.schemata"
	 */
	public IVocabulary getVocabulary(String namespace) throws ModelAPIException;
	
	/**
	 * Provides a user readable text for a vocabulary schema
	 * @param namespace The unique identifier of the vocabulary
	 * @return The vocabulary description
	 * @see "Extension Point org.eclipse.ogee.model.api.schemata"
	 */
	public String getVocabularyDescription(String namespace) throws ModelAPIException; 

	/**
	 * Returns all registered vocabulary namespaces, which are available "out of the box"
	 * @return The list of namespaces 
	 * @see "Extension Point org.eclipse.ogee.model.api.schemata"
	 */
	public List<String> getRegisteredVocabularies() throws ModelAPIException;
	
	/**
	 * Returns all available vocabularies, means all registered schemata as well as imported vocabularies.
	 * @return The list of namespaces 
	 * @throws ModelAPIException
	 */
	public List<String> getAvailableVocabularies() throws ModelAPIException;

}
