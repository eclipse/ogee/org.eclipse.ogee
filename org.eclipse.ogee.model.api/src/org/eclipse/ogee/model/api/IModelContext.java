/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.ogee.model.api.impl.ModelContextImpl;
import org.eclipse.ogee.model.odata.EDMXSet;

/**
 * The IModelContext interface is used as entry point. 
 * It provides an INSTANCE as singleton to be used independent from a single EDMXSet. 
 * For each single EDMXSet one IResourceContext can be retrieved. 
 * The handling of vocabularies is supported by IVocabularyContext. 
 * Even if instances of both types correspond to each other one to one they are separated by its functional scope. 
 * Furthermore, IModelContext can create validator instances and it controls instances of TransactionalEditingDomain.
 *
 */
public interface IModelContext {

	static final IModelContext INSTANCE = new ModelContextImpl();
	
	/**
	 * For each OData model file a resource context can be used for convenient access.
	 * A resource context handles included schemata, files and references.
	 * A resource context provides also a vocabulary context.
	 * @param edmxSet The EDMXSet that the context is requested for
	 * @return The resource context object
	 * @see IResourceContext
	 */
	public IResourceContext getResourceContext(EDMXSet edmxSet) throws ModelAPIException;
	
	/**
	 * For each OData model file a vocabulary context can be used.
	 * @param edmxSet The EDMXSet that the context is requested for
	 * @return The vocabulary context object
	 */
	public IVocabularyContext getVocabularyContext(EDMXSet edmxSet) throws ModelAPIException;

	/**
	 * Creates and returns an OData validator instance.
	 * @return The OData validator
	 */
	public IValidator createValidator();
	
	/**
	 * Returns a new OData model with a main EDMX instance and one associated schema. 
	 * A file has not yet been assigned.
	 * The edit domain has not been created as still no file is assigned.
	 * Vocabularies can be fetched into the model via corresponding VocabularyContext.
	 * @param namespace The unique identifier of the new service
	 * @return EDMXSet The EDMXSet that has been created
	 * @see IModelContext#getResourceContext(EDMXSet)
	 * @see IResourceContext#getVocabularyContext()
	 * @see IVocabularyContext#provideVocabulary(String)
	 */
    public EDMXSet createServiceModel(String namespace) throws ModelAPIException;
    
	/**
	 * Creates a OData model file.
	 * The file will not be saved automatically.
	 * @param resourceURI The platform URI for the model file to be created
	 * @param serviceModel The EDMXSet to be stored in the file
	 * @param viewModel The root node of any EMF model used by UI views, for example a diagram file
	 * @return The OData model file that has been created
	 * @see IModelContext#createServiceModel(String)
	 * @see IModelContext#getTransaction(ITransactionSubscriber, IFile)
	 * @see IModelContext#disposeTransaction(ITransactionSubscriber, TransactionalEditingDomain)
	 */
    public IFile createModelFile(URI resourceURI, EDMXSet serviceModel, EObject viewModel) throws ModelAPIException;
    
    /**
     * Saves a model file, but only changed resources.
     * @param file The OData file to be saved.
     * @throws ModelAPIException
     */
    public void saveModelFile(IFile file) throws ModelAPIException;
    
	/** 
	 * Returns the transaction or editing domain for the given OData model file.
	 * In case a transaction for the given OData model file already exists, this transaction is returned.
	 * Otherwise a new transaction is created for the model file.
	 * @param odataModelFile The OData model file for which the transaction is retrieved
	 * @return The transaction which is valid for the given OData model file
	 * @throws ModelAPIException
	 */
	public TransactionalEditingDomain getTransaction(IFile odataModelFile) throws ModelAPIException;
	
	/**
	 * Returns the transaction or editing domain for the given EMF object.
	 * An exception is thrown in case the EMF object does not belong to an file located within the current Eclipse workspace.
	 * @param eObject The EMF object from which the OData model file is going to be extracted
	 * @return The transaction which is valid for the given extracted OData model file
	 * @throws ModelAPIException
	 * @see IModelContext#getTransaction(IFile)
	 */
	public TransactionalEditingDomain getTransaction(EObject eObject) throws ModelAPIException;

	/**
	 * Disposes the given transaction or editing domain.
	 * @param transaction The editing domain going to be disposed
	 * @throws ModelAPIException
	 */
	public void removeTransaction(TransactionalEditingDomain transaction) throws ModelAPIException;
	
}
