/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.CollectableExpression;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.model.odata.ValueTerm;

/**
 * 
 */
public interface IVocabulary extends ISchemaAdapter {

	/**
	 * Returns the corresponding vocabulary context
	 * @see IVocabularyContext 
	 */
	public IVocabularyContext getContext() throws ModelAPIException;

	/**
	 * Returns a value term of the vocabulary
	 * @param name The locally unique value term name
	 * @return The value term instance
	 * @see ValueTerm 
	 * @throws ModelAPIException
	 */
	public ValueTerm getValueTerm(String name) throws ModelAPIException;

	/**
	 * Returns all applicable terms for a given EObject
	 * Depending on the type EObject terms are selected that apply to this type or that have no limitation to apply
	 * The search is executed recursively up to annotations by the core vocabulary
	 * @param: EObject The object for which valid terms are searched
	 * @return The list of valid annotations
	 * @see ValueTerm
	 * @see ValueAnnotation
	 * @throws ModelAPIException
	 */
	public List<ValueTerm> getApplicableValueTerms(EObject object) throws ModelAPIException;
	
	/**
	 * Creates a new value annotation and assigns it to the target node as well as to the target schema annotation list. 
	 * The annotation value is created corresponding to the defined value term data type (simple or complex).
	 * If the target schema is not available as ancestor then the caller is responsible to update the schema value annotation list. 
	 * @Each created value collection has no entries.
	 * Each created simple value refers to null value.
	 * Each created path value refers to null value.
	 * Each created record value contains all entries with Entry<Property, AnnotationValue>, both bound correctly.
	 * @param target The target node to be annotated
	 * @param term The value term to be used
	 * @return The new annotation object with initialized simple or complex value  
	 * @throws ModelAPIException
	 */
	public ValueAnnotation createValueAnnotation(IAnnotationTarget target, ValueTerm term) throws ModelAPIException;
	
	/**
	 * Creates a new value for one given collection.
	 * The value is not automatically added to the collection.
	 * To allow determination of the value type the collection must be assigned to an annotation (value tree) already and the term type must be defined.
	 * @param collection The collection for which the new value is needed (e.g. record value or simple value).
	 * @return The new value that still needs to be added to the collection.
	 * @throws ModelAPIException
	 */
	public CollectableExpression createCollectionValue(ValueCollection collection) throws ModelAPIException;
	
}