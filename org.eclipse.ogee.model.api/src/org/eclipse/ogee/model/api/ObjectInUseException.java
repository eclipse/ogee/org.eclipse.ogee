/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

public class ObjectInUseException extends ModelAPIException {

	private static final long serialVersionUID = 1L;
	
	private List<EObject> usingObjects;
	
	public ObjectInUseException(List<EObject> usingObjects){
		super();
		this.usingObjects = usingObjects;
	}
	
	public ObjectInUseException(String message,List<EObject> usingObjects){
		super(message);
		this.usingObjects = usingObjects;
	}
	
	public ObjectInUseException(String message,Throwable throwable,List<EObject> usingObjects){
		super(message,throwable);
		this.usingObjects = usingObjects;
	}
	
	public List<EObject> getUsingObjects(){
		return this.usingObjects;
	}

}
