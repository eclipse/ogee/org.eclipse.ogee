/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

import org.eclipse.ogee.model.odata.Schema;

/**
 * Schema adapters can provide typed access to a schema
 * Vocabularies are a specific kind of schema adapters
 * @see IVocabulary
 * @see IVocabularyContext#provideVocabulary(String)
 */
public interface ISchemaAdapter {

	/**
	 * Returns the schema that is handled by the adapter
	 */
	public Schema getSchema( ) throws ModelAPIException;
	
}
