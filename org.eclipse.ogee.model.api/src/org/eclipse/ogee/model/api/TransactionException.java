/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

public class TransactionException extends ModelAPIException {

	private static final long serialVersionUID = 5945590084708358321L;
	
	public TransactionException(){
		
	}
	
	public TransactionException(String message){
		super(message);
	}

	public TransactionException(String message, Throwable error){
		super(message, error);
	}

}
