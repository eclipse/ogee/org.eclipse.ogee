/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

import org.eclipse.ogee.model.odata.Schema;

/**
 *
 */
public interface ISchemaProvider {

	/**
	 * Provides the namespace that is handled by this provider. 
	 * @return The namespace.
	 * @throws ModelAPIException
	 */
	public String getNamespace() throws ModelAPIException;
	
	/**
	 * Creates a new schema instance, still not assigned to an EDMX node.
	 * Within one EDMXSet only one schema instance is created for one given namespace.
	 * The vocabulary context provides access to other vocabularies in the same context, required to load completely.
	 * @param vocabularyContext The vocabulary context 
	 * @return The new schema instance
	 * @throws ModelAPIException
	 * @see {@link org.eclipse.ogee.model.api.IVocabularyContext}
	 */
	public Schema createSchema(IVocabularyContext vocabularyContext) throws ModelAPIException;

	/**
	 * Provides a specialized adapter for the schema.
	 * Typically, the adapter will implement a more specific interface to enable full-typed schema consumption.
	 * Furthermore, standard adapter methods may be redefined for optimization.
	 * @param context The vocabulary context provides access to other vocabularies.
	 * @param schema The schema instance, unique within EDMXSet. 
	 * @return The new adapter instance
	 * @throws ModelAPIException
	 * @see {@link org.eclipse.ogee.model.api.IVocabularyContext}
	 */
	public SchemaAdapterImpl createAdapter(IVocabularyContext context, Schema schema) throws ModelAPIException; 

}
