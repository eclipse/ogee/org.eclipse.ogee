/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies.impl;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.ogee.model.api.vocabularies.impl.messages"; //$NON-NLS-1$
	public static String BatchSupported;
	public static String ClientPageable;
	public static String Countable;
	public static String DeleteRestrictions;
	public static String Expandable;
	public static String FilterRestrictions;
	public static String IndexableByKey;
	public static String InsertRestrictions;
	public static String IsoCurrency;
	public static String Navigable;
	public static String Queryable;
	public static String QueryFunctions;
	public static String Scale;
	public static String SortRestrictions;
	public static String Unit;
	public static String UpdateRestrictions;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
