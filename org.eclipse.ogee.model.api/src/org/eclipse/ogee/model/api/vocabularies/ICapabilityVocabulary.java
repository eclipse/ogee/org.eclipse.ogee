/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies;

import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ValueTerm;

public interface ICapabilityVocabulary extends IVocabulary {

	/**
	 *
	 */
	public static final String VC_NAMESPACE                            = "Org.OData.Capabilities.V1";         //$NON-NLS-1$
	public static final String VC_DEFAULT_ALIAS                        = "C";                                 //$NON-NLS-1$
	
	/**
	 * value term names
	 */
	public static final String VT_INSERT_RESTRICTIONS                  = "InsertRestrictions";                //$NON-NLS-1$
	public static final String VT_UPDATE_RESTRICTIONS                  = "UpdateRestrictions";                //$NON-NLS-1$
	public static final String VT_DELETE_RESTRICTIONS                  = "DeleteRestrictions";                //$NON-NLS-1$
	public static final String VT_COUNTABLE                            = "Countable";                         //$NON-NLS-1$
	public static final String VT_QUERYABLE                            = "Queryable";                         //$NON-NLS-1$
	public static final String VT_CLIENT_PAGEABLE                      = "ClientPageable";                    //$NON-NLS-1$
	public static final String VT_EXPANDABLE                           = "Expandable";                        //$NON-NLS-1$
	public static final String VT_NAVIGABLE                            = "Navigable";                         //$NON-NLS-1$
	public static final String VT_INDEXABLE_BY_KEY                     = "IndexableByKey";                    //$NON-NLS-1$
	public static final String VT_BATCH_SUPPORTED                      = "BatchSupported";                    //$NON-NLS-1$
	public static final String VT_QUERY_FUNCTIONS                      = "QueryFunctions";                    //$NON-NLS-1$
	public static final String VT_FILTER_RESTRICTIONS                  = "FilterRestrictions";                //$NON-NLS-1$
	public static final String VT_SORT_RESTRICTIONS                    = "SortRestrictions";                  //$NON-NLS-1$
	
	/**
	 * complex type names
	 */
	public static final String CT_INSERT_RESTRICTIONS                  = "InsertRestrictionsType";            //$NON-NLS-1$
	public static final String CT_UPDATE_RESTRICTIONS                  = "UpdateRestrictionsType";            //$NON-NLS-1$
	public static final String CT_DELETE_RESTRICTIONS                  = "DeleteRestrictionsType";            //$NON-NLS-1$
	public static final String CT_FILTER_RESTRICTIONS                  = "FilterRestrictionsType";            //$NON-NLS-1$
	public static final String CT_SORT_RESTRICTIONS                    = "SortRestrictionsType";              //$NON-NLS-1$

	/**
	 * complex type property names
	 */
	public static final String CP_INSERTABLE                           = "Insertable";                        //$NON-NLS-1$
	public static final String CP_NON_INSERTABLE_PROPERTIES            = "NonInsertableProperties";           //$NON-NLS-1$
	public static final String CP_NON_INSERTABLE_NAVIGATION_PROPERTIES = "NonInsertableNavigationProperties"; //$NON-NLS-1$
	public static final String CP_UPDATABLE                            = "Updatable";                         //$NON-NLS-1$
	public static final String CP_NON_UPDATABLE_PROPERTIES             = "NonUpdatableProperties";            //$NON-NLS-1$
	public static final String CP_NON_UPDATABLE_NAVIGATION_PROPERTIES  = "NonUpdatableNavigationProperties";  //$NON-NLS-1$
	public static final String CP_DELETABLE                            = "Deletable";                         //$NON-NLS-1$
	public static final String CP_NON_DELETABLE_NAVIGATION_PROPERTIES  = "NonDeletableNavigationProperties";  //$NON-NLS-1$
	public static final String CP_FILTERABLE                           = "Filterable";                        //$NON-NLS-1$
	public static final String CP_REQUIRED_PROPERTIES                  = "RequiredProperties";                //$NON-NLS-1$
	public static final String CP_NON_FILTERABLE_PROPERTIES            = "NonFilterableProperties";           //$NON-NLS-1$
	public static final String CP_SORTABLE                             = "Sortable";                          //$NON-NLS-1$
	public static final String CP_ASCENDING_ONLY_PROPERTIES            = "AscendingOnlyProperties";           //$NON-NLS-1$
	public static final String CP_DESCENDING_ONLY_PROPERTIES           = "DescendingOnlyProperties";          //$NON-NLS-1$
	public static final String CP_UNSORTABLE_PROPERTIES                = "UnsortableProperties";              //$NON-NLS-1$
	
	/**
	 * value term instances
	 */
	public ValueTerm getValueTermInsertRestrictions();
	public ValueTerm getValueTermUpdateRestrictions();
	public ValueTerm getValueTermDeleteRestrictions();
	public ValueTerm getValueTermCountable();
	public ValueTerm getValueTermQueryable();
	public ValueTerm getValueTermClientPageable();
	public ValueTerm getValueTermExpandable();
	public ValueTerm getValueTermNavigable();
	public ValueTerm getValueTermIndexableByKey();
	public ValueTerm getValueTermBatchSupported();
	public ValueTerm getValueTermQueryFunctions();
	public ValueTerm getValueTermFilterRestrictions();
	public ValueTerm getValueTermSortRestrictions();

	/**
	 * complex type instances
	 */
	public ComplexType getComplexTypeInsertRestrictions();
	public ComplexType getComplexTypeUpdateRestrictions();
	public ComplexType getComplexTypeDeleteRestrictions();
	public ComplexType getComplexTypeFilterRestrictions();
	public ComplexType getComplexTypeSortRestrictions();
	
	/**
	 * term property instances
	 */
	public Property getTermPropertyInsertable();
	public Property getTermPropertyNonInsertableProperties();
	public Property getTermPropertyNonInsertableNavigationProperties();
	public Property getTermPropertyUpdatable();
	public Property getTermPropertyNonUpdatableProperties();
	public Property getTermPropertyNonUpdatableNavigationProperties();
	public Property getTermPropertyDeletable();
	public Property getTermPropertyNonDeletableNavigationProperties();
	public Property getTermPropertyFilterable();
	public Property getTermPropertyRequiredProperties();
	public Property getTermPropertyNonFilterableProperties();
	public Property getTermPropertySortable();
	public Property getTermPropertyAscendingOnlyProperties();
	public Property getTermPropertyDescendingOnlyProperties();
	public Property getTermPropertyUnsortableProperties();
	
}
