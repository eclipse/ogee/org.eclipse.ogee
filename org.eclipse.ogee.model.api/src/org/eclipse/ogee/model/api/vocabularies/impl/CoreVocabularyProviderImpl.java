/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies.impl;

import org.eclipse.ogee.model.api.ISchemaProvider;
import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.SchemaAdapterImpl;
import org.eclipse.ogee.model.api.vocabularies.ICoreVocabulary;
import org.eclipse.ogee.model.odata.BooleanValue;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.StringValue;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueTerm;

public class CoreVocabularyProviderImpl implements ISchemaProvider {

	@Override
	public String getNamespace() {
		return ICoreVocabulary.VC_NAMESPACE;
	}

	@Override
	public SchemaAdapterImpl createAdapter(IVocabularyContext context, Schema schema) {
		return new CoreVocabularyAdapterImpl(context, schema);
	}

	@Override
	public Schema createSchema(IVocabularyContext vocabularyContext) {
		Schema          schema       = null;
		ValueAnnotation annotation   = null;
		SimpleType      simpleType   = null;
		SimpleTypeUsage simpleUsage  = null;
		BooleanValue    booleanValue = null;
		StringValue     stringValue  = null;

		// create schema instance
		schema = OdataFactory.eINSTANCE.createSchema();
		schema.setNamespace(ICoreVocabulary.VC_NAMESPACE);
		schema.getClassifiers().add(SchemaClassifier.VOCABULARY);

		// value term
		ValueTerm vtAppliesToAssociationSet = OdataFactory.eINSTANCE.createValueTerm();
		vtAppliesToAssociationSet.setName(ICoreVocabulary.VT_APPLIES_TO_ASSOCIATION_SET);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtAppliesToAssociationSet.setType(simpleUsage);
		schema.getValueTerms().add(vtAppliesToAssociationSet);

		// value term
		ValueTerm vtAppliesToEntitySet = OdataFactory.eINSTANCE.createValueTerm();
		vtAppliesToEntitySet.setName(ICoreVocabulary.VT_APPLIES_TO_ENTITY_SET);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtAppliesToEntitySet.setType(simpleUsage);
		schema.getValueTerms().add(vtAppliesToEntitySet);

		// value term
		ValueTerm vtAppliesToEntityType = OdataFactory.eINSTANCE.createValueTerm();
		vtAppliesToEntityType.setName(ICoreVocabulary.VT_APPLIES_TO_ENTITY_TYPE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtAppliesToEntityType.setType(simpleUsage);
		schema.getValueTerms().add(vtAppliesToEntityType);

		// value term
		ValueTerm vtAppliesToComplexType = OdataFactory.eINSTANCE.createValueTerm();
		vtAppliesToComplexType.setName(ICoreVocabulary.VT_APPLIES_TO_COMPLEX_TYPE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtAppliesToComplexType.setType(simpleUsage);
		schema.getValueTerms().add(vtAppliesToComplexType);

		// value term
		ValueTerm vtAppliesToProperty = OdataFactory.eINSTANCE.createValueTerm();
		vtAppliesToProperty.setName(ICoreVocabulary.VT_APPLIES_TO_PROPERTY);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtAppliesToProperty.setType(simpleUsage);
		schema.getValueTerms().add(vtAppliesToProperty);
		
		// value term
		ValueTerm vtAppliesToNavigationProperty = OdataFactory.eINSTANCE.createValueTerm();
		vtAppliesToNavigationProperty.setName(ICoreVocabulary.VT_APPLIES_TO_NAVIGATION_PROPERTY);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtAppliesToNavigationProperty.setType(simpleUsage);
		schema.getValueTerms().add(vtAppliesToNavigationProperty);
		
		// value term
		ValueTerm vtAppliesToParameter = OdataFactory.eINSTANCE.createValueTerm();
		vtAppliesToParameter.setName(ICoreVocabulary.VT_APPLIES_TO_PARAMETER);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtAppliesToParameter.setType(simpleUsage);
		schema.getValueTerms().add(vtAppliesToParameter);

		// value term
		ValueTerm vtAppliesToValueTerm = OdataFactory.eINSTANCE.createValueTerm();
		vtAppliesToValueTerm.setName(ICoreVocabulary.VT_APPLIES_TO_VALUE_TERM);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtAppliesToValueTerm.setType(simpleUsage);
		schema.getValueTerms().add(vtAppliesToValueTerm);
		
		// value term
		ValueTerm vtAppliesToEntityContainer = OdataFactory.eINSTANCE.createValueTerm();
		vtAppliesToEntityContainer.setName(ICoreVocabulary.VT_APPLIES_TO_ENTITY_CONTAINER);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtAppliesToEntityContainer.setType(simpleUsage);
		schema.getValueTerms().add(vtAppliesToEntityContainer);
		
		// value term
		ValueTerm vtRequiresType = OdataFactory.eINSTANCE.createValueTerm();
		vtRequiresType.setName(ICoreVocabulary.VT_REQUIRES_TYPE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtRequiresType.setType(simpleUsage);
		schema.getValueTerms().add(vtRequiresType);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtRequiresType);
		annotation.setTerm(vtAppliesToProperty);
		schema.getValueAnnotations().add(annotation);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtRequiresType);
		annotation.setTerm(vtAppliesToValueTerm);
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtIsPropertyPath = OdataFactory.eINSTANCE.createValueTerm();
		vtIsPropertyPath.setName(ICoreVocabulary.VT_IS_PROPERTY_PATH);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtIsPropertyPath.setType(simpleUsage);
		schema.getValueTerms().add(vtIsPropertyPath);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtIsPropertyPath);
		annotation.setTerm(vtAppliesToProperty);
		schema.getValueAnnotations().add(annotation);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtIsPropertyPath);
		annotation.setTerm(vtAppliesToValueTerm);
		schema.getValueAnnotations().add(annotation);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtIsPropertyPath);
		annotation.setTerm(vtRequiresType);
		stringValue = OdataFactory.eINSTANCE.createStringValue();
		stringValue.setValue(EDMTypes.STRING.toString());
		annotation.setAnnotationValue(stringValue);
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtIsURI = OdataFactory.eINSTANCE.createValueTerm();
		vtIsURI.setName(ICoreVocabulary.VT_IS_URI);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtIsURI.setType(simpleUsage);
		schema.getValueTerms().add(vtIsURI);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtIsURI);
		annotation.setTerm(vtAppliesToProperty);
		schema.getValueAnnotations().add(annotation);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtIsURI);
		annotation.setTerm(vtAppliesToValueTerm);
		schema.getValueAnnotations().add(annotation);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtIsURI);
		annotation.setTerm(vtRequiresType);
		stringValue = OdataFactory.eINSTANCE.createStringValue();
		stringValue.setValue(EDMTypes.STRING.toString());
		annotation.setAnnotationValue(stringValue);
		schema.getValueAnnotations().add(annotation);
		
		return schema;
	}

}
