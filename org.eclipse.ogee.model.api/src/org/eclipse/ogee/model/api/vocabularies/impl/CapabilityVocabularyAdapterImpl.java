/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies.impl;

import java.util.ListIterator;

import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.vocabularies.ICapabilityVocabulary;
import org.eclipse.ogee.model.api.vocabularies.VocabularyAdapterImpl;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.ValueTerm;

public class CapabilityVocabularyAdapterImpl extends VocabularyAdapterImpl implements ICapabilityVocabulary {

	/**
	 * value term instances
	 */
	private ValueTerm valueTermInsertRestrictions     = null;
	private ValueTerm valueTermUpdateRestrictions     = null;
	private ValueTerm valueTermDeleteRestrictions     = null;
	private ValueTerm valueTermCountable              = null;
	private ValueTerm valueTermQueryable              = null;
	private ValueTerm valueTermClientPageable         = null;
	private ValueTerm valueTermExpandable             = null;
	private ValueTerm valueTermNavigable              = null;
	private ValueTerm valueTermIndexableByKey         = null;
	private ValueTerm valueTermBatchSupported         = null;
	private ValueTerm valueTermQueryFunctions         = null;
	private ValueTerm valueTermFilterRestrictions     = null;
	private ValueTerm valueTermSortRestrictions       = null;
	
	/**
	 * complex type instances
	 */
	private ComplexType complexTypeInsertRestrictions = null;
	private ComplexType complexTypeUpdateRestrictions = null;
	private ComplexType complexTypeDeleteRestrictions = null;
	private ComplexType complexTypeFilterRestrictions = null;
	private ComplexType complexTypeSortRestrictions   = null;
	
	/**
	 * term property instances
	 */
	private Property termPropertyInsertable;
	private Property termPropertyNonInsertableProperties;
	private Property termPropertyNonInsertableNavigationProperties;
	private Property termPropertyUpdatable;
	private Property termPropertyNonUpdatableProperties;
	private Property termPropertyNonUpdatableNavigationProperties;
	private Property termPropertyDeletable;
	private Property termPropertyNonDeletableNavigationProperties;
	private Property termPropertyFilterable;
	private Property termPropertyRequiredProperties;
	private Property termPropertyNonFilterableProperties;
	private Property termPropertySortable;
	private Property termPropertyAscendingOnlyProperties;
	private Property termPropertyDescendingOnlyProperties;
	private Property termPropertyUnsortableProperties;

	public CapabilityVocabularyAdapterImpl(IVocabularyContext context, Schema schema) {
		super(context, schema);
		ListIterator<ValueTerm> itTerm = schema.getValueTerms().listIterator();
		while(itTerm.hasNext()) {
			ValueTerm term = itTerm.next();
			String name = term.getName();
			if(ICapabilityVocabulary.VT_INSERT_RESTRICTIONS.equals(name)) {
				this.valueTermInsertRestrictions = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_UPDATE_RESTRICTIONS.equals(name)) {
				this.valueTermUpdateRestrictions = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_DELETE_RESTRICTIONS.equals(name)) {
				this.valueTermDeleteRestrictions = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_COUNTABLE.equals(name)) {
				this.valueTermCountable = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_QUERYABLE.equals(name)) {
				this.valueTermQueryable = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_CLIENT_PAGEABLE.equals(name)) {
				this.valueTermClientPageable = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_EXPANDABLE.equals(name)) {
				this.valueTermExpandable = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_NAVIGABLE.equals(name)) {
				this.valueTermNavigable = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_INDEXABLE_BY_KEY.equals(name)) {
				this.valueTermIndexableByKey = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_BATCH_SUPPORTED.equals(name)) {
				this.valueTermBatchSupported = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_QUERY_FUNCTIONS.equals(name)) {
				this.valueTermQueryFunctions = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_FILTER_RESTRICTIONS.equals(name)) {
				this.valueTermFilterRestrictions = term;
				continue;
			}
			if(ICapabilityVocabulary.VT_SORT_RESTRICTIONS.equals(name)) {
				this.valueTermSortRestrictions = term;
				continue;
			}
		}
		ListIterator<ComplexType> itType = schema.getComplexTypes().listIterator();
		while(itType.hasNext()) {
			ComplexType type = itType.next();
			String name = type.getName();
			if(ICapabilityVocabulary.CT_INSERT_RESTRICTIONS.equals(name)) {
				for(Property property : type.getProperties() ) {
					String pname = property.getName();
					if(ICapabilityVocabulary.CP_INSERTABLE.equals(pname)){
						this.termPropertyInsertable = property;
					}
					if(ICapabilityVocabulary.CP_NON_INSERTABLE_PROPERTIES.equals(pname)){
						this.termPropertyNonInsertableProperties = property;
					}
					if(ICapabilityVocabulary.CP_NON_INSERTABLE_NAVIGATION_PROPERTIES.equals(pname)){
						this.termPropertyNonInsertableNavigationProperties = property;
					}
				}
				this.complexTypeInsertRestrictions = type;
				continue;
			}
			if(ICapabilityVocabulary.CT_UPDATE_RESTRICTIONS.equals(name)) {
				for(Property property : type.getProperties() ) {
					String pname = property.getName(); 
					if(ICapabilityVocabulary.CP_UPDATABLE.equals(pname)){
						this.termPropertyUpdatable = property;
					}
					if(ICapabilityVocabulary.CP_NON_UPDATABLE_PROPERTIES.equals(pname)){
						this.termPropertyNonUpdatableProperties = property;
					}
					if(ICapabilityVocabulary.CP_NON_UPDATABLE_NAVIGATION_PROPERTIES.equals(pname)){
						this.termPropertyNonUpdatableNavigationProperties = property;
					}
				}
				this.complexTypeUpdateRestrictions = type;
				continue;
			}
			if(ICapabilityVocabulary.CT_DELETE_RESTRICTIONS.equals(name)) {
				for(Property property : type.getProperties() ) {
					String pname = property.getName();
					if(ICapabilityVocabulary.CP_DELETABLE.equals(pname)){
						this.termPropertyDeletable = property;
					}
					if(ICapabilityVocabulary.CP_NON_DELETABLE_NAVIGATION_PROPERTIES.equals(pname)){
						this.termPropertyNonDeletableNavigationProperties = property;
					}
				}
				this.complexTypeDeleteRestrictions = type;
				continue;
			}
			if(ICapabilityVocabulary.CT_FILTER_RESTRICTIONS.equals(name)) {
				for(Property property : type.getProperties() ) {
					String pname = property.getName();
					if(ICapabilityVocabulary.CP_FILTERABLE.equals(pname)){
						this.termPropertyFilterable = property;
					}
					if(ICapabilityVocabulary.CP_REQUIRED_PROPERTIES.equals(pname)){
						this.termPropertyRequiredProperties = property;
					}
					if(ICapabilityVocabulary.CP_NON_FILTERABLE_PROPERTIES.equals(pname)){
						this.termPropertyNonFilterableProperties = property;
					}
				}
				this.complexTypeFilterRestrictions = type;
				continue;
			}
			if(ICapabilityVocabulary.CT_SORT_RESTRICTIONS.equals(name)) {
				for(Property property : type.getProperties() ) {
					String pname = property.getName();
					if(ICapabilityVocabulary.CP_SORTABLE.equals(pname)){
						this.termPropertySortable = property;
					}
					if(ICapabilityVocabulary.CP_ASCENDING_ONLY_PROPERTIES.equals(pname)){
						this.termPropertyAscendingOnlyProperties = property;
					}
					if(ICapabilityVocabulary.CP_DESCENDING_ONLY_PROPERTIES.equals(pname)){
						this.termPropertyDescendingOnlyProperties = property;
					}
					if(ICapabilityVocabulary.CP_UNSORTABLE_PROPERTIES.equals(pname)){
						this.termPropertyUnsortableProperties = property;
					}
				}
				this.complexTypeSortRestrictions = type;
				continue;
			}
		}
	}
	
	@Override
	public ValueTerm getValueTermInsertRestrictions() {
		return this.valueTermInsertRestrictions;
	}

	@Override
	public ValueTerm getValueTermUpdateRestrictions() {
		return this.valueTermUpdateRestrictions;
	}

	@Override
	public ValueTerm getValueTermDeleteRestrictions() {
		return this.valueTermDeleteRestrictions;
	}

	@Override
	public ValueTerm getValueTermCountable() {
		return this.valueTermCountable;
	}

	@Override
	public ValueTerm getValueTermQueryable() {
		return this.valueTermQueryable;
	}

	@Override
	public ValueTerm getValueTermClientPageable() {
		return this.valueTermClientPageable;
	}

	@Override
	public ValueTerm getValueTermExpandable() {
		return this.valueTermExpandable;
	}

	@Override
	public ValueTerm getValueTermNavigable() {
		return this.valueTermNavigable;
	}

	@Override
	public ValueTerm getValueTermIndexableByKey() {
		return this.valueTermIndexableByKey;
	}

	@Override
	public ValueTerm getValueTermBatchSupported() {
		return this.valueTermBatchSupported;
	}

	@Override
	public ValueTerm getValueTermQueryFunctions() {
		return this.valueTermQueryFunctions;
	}

	@Override
	public ValueTerm getValueTermFilterRestrictions() {
		return this.valueTermFilterRestrictions;
	}

	@Override
	public ValueTerm getValueTermSortRestrictions() {
		return this.valueTermSortRestrictions;
	}

	@Override
	public ComplexType getComplexTypeInsertRestrictions() {
		return this.complexTypeInsertRestrictions;
	}

	@Override
	public ComplexType getComplexTypeUpdateRestrictions() {
		return this.complexTypeUpdateRestrictions;
	}

	@Override
	public ComplexType getComplexTypeDeleteRestrictions() {
		return this.complexTypeDeleteRestrictions;
	}

	@Override
	public ComplexType getComplexTypeFilterRestrictions() {
		return this.complexTypeFilterRestrictions;
	}

	@Override
	public ComplexType getComplexTypeSortRestrictions() {
		return this.complexTypeSortRestrictions;
	}

	public Property getTermPropertyInsertable() {
		return this.termPropertyInsertable;
	}

	public Property getTermPropertyNonInsertableProperties() {
		return this.termPropertyNonInsertableProperties;
	}

	public Property getTermPropertyNonInsertableNavigationProperties() {
		return this.termPropertyNonInsertableNavigationProperties;
	}

	public Property getTermPropertyUpdatable() {
		return this.termPropertyUpdatable;
	}

	public Property getTermPropertyNonUpdatableProperties() {
		return this.termPropertyNonUpdatableProperties;
	}

	public Property getTermPropertyNonUpdatableNavigationProperties() {
		return this.termPropertyNonUpdatableNavigationProperties;
	}

	public Property getTermPropertyDeletable() {
		return this.termPropertyDeletable;
	}

	public Property getTermPropertyNonDeletableNavigationProperties() {
		return this.termPropertyNonDeletableNavigationProperties;
	}

	public Property getTermPropertyFilterable() {
		return this.termPropertyFilterable;
	}

	public Property getTermPropertyRequiredProperties() {
		return this.termPropertyRequiredProperties;
	}

	public Property getTermPropertyNonFilterableProperties() {
		return this.termPropertyNonFilterableProperties;
	}

	public Property getTermPropertySortable() {
		return this.termPropertySortable;
	}

	public Property getTermPropertyAscendingOnlyProperties() {
		return this.termPropertyAscendingOnlyProperties;
	}

	public Property getTermPropertyDescendingOnlyProperties() {
		return this.termPropertyDescendingOnlyProperties;
	}

	public Property getTermPropertyUnsortableProperties() {
		return this.termPropertyUnsortableProperties;
	}

	
}
