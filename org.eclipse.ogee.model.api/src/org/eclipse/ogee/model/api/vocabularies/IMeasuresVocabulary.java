/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies;

import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.odata.ValueTerm;

public interface IMeasuresVocabulary extends IVocabulary {

	/**
	 *
	 */
	public static final String VC_NAMESPACE     = "Org.OData.Measures.V1";                                    //$NON-NLS-1$ 
	public static final String VC_DEFAULT_ALIAS = "Measures";                                                 //$NON-NLS-1$
	
	/**
	 * value term names
	 */
	public static final String VT_ISOCURRENCY   = "ISOCurrency";                                              //$NON-NLS-1$
	public static final String VT_SCALE         = "Scale";                                                    //$NON-NLS-1$
	public static final String VT_UNIT          = "Unit";                                                     //$NON-NLS-1$
	
	/**
	 * value term instances
	 */
	public ValueTerm getValueTermIsoCurrency();
	public ValueTerm getValueTermScale();
	public ValueTerm getValueTermUnit();
	
	
}
