/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies;

import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.odata.ValueTerm;

public interface ICoreVocabulary extends IVocabulary {

	/**
	 *
	 */
	public static final String VC_NAMESPACE                      = "Org.OData.Core.V1";              //$NON-NLS-1$
	public static final String VC_DEFAULT_ALIAS                  = "Core";                                    //$NON-NLS-1$
	
	/**
	 * value term names
	 */
	public static final String VT_APPLIES_TO_ASSOCIATION_SET     = "appliesToAssociationSet";                 //$NON-NLS-1$
	public static final String VT_APPLIES_TO_ENTITY_SET          = "appliesToEntitySet";                      //$NON-NLS-1$
	public static final String VT_APPLIES_TO_ENTITY_TYPE         = "appliesToEntityType";                     //$NON-NLS-1$
	public static final String VT_APPLIES_TO_COMPLEX_TYPE        = "appliesToComplexType";                    //$NON-NLS-1$
	public static final String VT_APPLIES_TO_PROPERTY            = "appliesToProperty";                       //$NON-NLS-1$
	public static final String VT_APPLIES_TO_NAVIGATION_PROPERTY = "appliesToNavigationProperty";             //$NON-NLS-1$
	public static final String VT_APPLIES_TO_PARAMETER           = "appliesToParameter";                      //$NON-NLS-1$
	public static final String VT_APPLIES_TO_VALUE_TERM          = "appliesToValueterm";                      //$NON-NLS-1$
	public static final String VT_APPLIES_TO_ENTITY_CONTAINER    = "appliesToEntityContainer";                //$NON-NLS-1$
	public static final String VT_REQUIRES_TYPE                  = "requiresType";                            //$NON-NLS-1$
	public static final String VT_IS_PROPERTY_PATH               = "isPropertyPath";                          //$NON-NLS-1$
	public static final String VT_IS_URI                         = "isURI";                                   //$NON-NLS-1$
	
	/**
	 * value term instances
	 */
	public ValueTerm getValueTermAppliesToAssociationSet();
	public ValueTerm getValueTermAppliesToEntitySet();
	public ValueTerm getValueTermAppliesToEntityType();
	public ValueTerm getValueTermAppliesToComplexType();
	public ValueTerm getValueTermAppliesToProperty();
	public ValueTerm getValueTermAppliesToNavigationProperty();
	public ValueTerm getValueTermAppliesToParameter();
	public ValueTerm getValueTermAppliesToValueTerm();
	public ValueTerm getValueTermAppliesToEntityContainer();
	public ValueTerm getValueTermRequiresType();
	public ValueTerm getValueTermIsPropertyPath();
	public ValueTerm getValueTermIsURI();

}
