/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies.impl;

import org.eclipse.ogee.model.api.ISchemaProvider;
import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.SchemaAdapterImpl;
import org.eclipse.ogee.model.api.vocabularies.ICapabilityVocabulary;
import org.eclipse.ogee.model.api.vocabularies.ICoreVocabulary;
import org.eclipse.ogee.model.odata.BooleanValue;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueTerm;

public class CapabilityVocabularyProviderImpl implements ISchemaProvider {

	@Override
	public String getNamespace() throws ModelAPIException {
		return ICapabilityVocabulary.VC_NAMESPACE;
	}

	@Override
	public SchemaAdapterImpl createAdapter(IVocabularyContext context, Schema schema) throws ModelAPIException {
		return new CapabilityVocabularyAdapterImpl(context, schema);
	}

	@Override
	public Schema createSchema(IVocabularyContext vocabularyContext) throws ModelAPIException {
		Schema           schema       = null;
		Property         property     = null;
		ValueAnnotation  annotation   = null;
		ComplexTypeUsage complexUsage = null;
		SimpleType       simpleType   = null;
		SimpleTypeUsage  simpleUsage  = null;
		BooleanValue     booleanValue = null;
		Documentation 	 doc 		  = null;
		
		// create schema instance
		schema = OdataFactory.eINSTANCE.createSchema();
		schema.setNamespace(ICapabilityVocabulary.VC_NAMESPACE);
		schema.getClassifiers().add(SchemaClassifier.VOCABULARY);

		// uses core vocabulary
		ICoreVocabulary coreVocabulary = (ICoreVocabulary)vocabularyContext.provideVocabulary(ICoreVocabulary.VC_NAMESPACE);
		Using using = OdataFactory.eINSTANCE.createUsing();
		using.setAlias(ICoreVocabulary.VC_DEFAULT_ALIAS);
		using.setUsedNamespace(coreVocabulary.getSchema());
		schema.getUsings().add(using);

		// complex type
		ComplexType ctInsertRestrictions = OdataFactory.eINSTANCE.createComplexType();
		ctInsertRestrictions.setName(ICapabilityVocabulary.CT_INSERT_RESTRICTIONS);
		schema.getComplexTypes().add(ctInsertRestrictions);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_INSERTABLE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		ctInsertRestrictions.getProperties().add(property);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_NON_INSERTABLE_PROPERTIES);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setCollection(true);
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		property.setNullable(true);
		ctInsertRestrictions.getProperties().add(property);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(property);
		annotation.setTerm(coreVocabulary.getValueTermIsPropertyPath());
		schema.getValueAnnotations().add(annotation);
		
		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_NON_INSERTABLE_NAVIGATION_PROPERTIES);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setCollection(true);
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		property.setNullable(true);
		ctInsertRestrictions.getProperties().add(property);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(property);
		annotation.setTerm(coreVocabulary.getValueTermIsPropertyPath());
		schema.getValueAnnotations().add(annotation);

		// complex type
		ComplexType ctUpdateRestrictions = OdataFactory.eINSTANCE.createComplexType();
		ctUpdateRestrictions.setName(ICapabilityVocabulary.CT_UPDATE_RESTRICTIONS);
		schema.getComplexTypes().add(ctUpdateRestrictions);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_UPDATABLE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		ctUpdateRestrictions.getProperties().add(property);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_NON_UPDATABLE_PROPERTIES);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setCollection(true);
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		property.setNullable(true);
		ctUpdateRestrictions.getProperties().add(property);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(property);
		annotation.setTerm(coreVocabulary.getValueTermIsPropertyPath());
		schema.getValueAnnotations().add(annotation);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_NON_UPDATABLE_NAVIGATION_PROPERTIES);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setCollection(true);
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		property.setNullable(true);
		ctUpdateRestrictions.getProperties().add(property);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(property);
		annotation.setTerm(coreVocabulary.getValueTermIsPropertyPath());
		schema.getValueAnnotations().add(annotation);
		
		// complex type
		ComplexType ctDeleteRestrictions = OdataFactory.eINSTANCE.createComplexType();
		ctDeleteRestrictions.setName(ICapabilityVocabulary.CT_DELETE_RESTRICTIONS);
		schema.getComplexTypes().add(ctDeleteRestrictions);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_DELETABLE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		ctDeleteRestrictions.getProperties().add(property);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_NON_DELETABLE_NAVIGATION_PROPERTIES);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setCollection(true);
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		property.setNullable(true);
		ctDeleteRestrictions.getProperties().add(property);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(property);
		annotation.setTerm(coreVocabulary.getValueTermIsPropertyPath());
		schema.getValueAnnotations().add(annotation);
		
		// complex type
		ComplexType ctFilterRestrictions = OdataFactory.eINSTANCE.createComplexType();
		ctFilterRestrictions.setName(ICapabilityVocabulary.CT_FILTER_RESTRICTIONS);
		schema.getComplexTypes().add(ctFilterRestrictions);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_FILTERABLE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		ctFilterRestrictions.getProperties().add(property);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_REQUIRED_PROPERTIES);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setCollection(true);
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		property.setNullable(true);
		ctFilterRestrictions.getProperties().add(property);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(property);
		annotation.setTerm(coreVocabulary.getValueTermIsPropertyPath());
		schema.getValueAnnotations().add(annotation);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_NON_FILTERABLE_PROPERTIES);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setCollection(true);
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		property.setNullable(true);
		ctFilterRestrictions.getProperties().add(property);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(property);
		annotation.setTerm(coreVocabulary.getValueTermIsPropertyPath());
		schema.getValueAnnotations().add(annotation);
		
		// complex type
		ComplexType ctSortRestrictions = OdataFactory.eINSTANCE.createComplexType();
		ctSortRestrictions.setName(ICapabilityVocabulary.CT_SORT_RESTRICTIONS);
		schema.getComplexTypes().add(ctSortRestrictions);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_SORTABLE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		ctSortRestrictions.getProperties().add(property);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_ASCENDING_ONLY_PROPERTIES);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setCollection(true);
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		property.setNullable(true);
		ctSortRestrictions.getProperties().add(property);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(property);
		annotation.setTerm(coreVocabulary.getValueTermIsPropertyPath());
		schema.getValueAnnotations().add(annotation);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_DESCENDING_ONLY_PROPERTIES);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setCollection(true);
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		property.setNullable(true);
		ctSortRestrictions.getProperties().add(property);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(property);
		annotation.setTerm(coreVocabulary.getValueTermIsPropertyPath());
		schema.getValueAnnotations().add(annotation);

		property = OdataFactory.eINSTANCE.createProperty();
		property.setName(ICapabilityVocabulary.CP_UNSORTABLE_PROPERTIES);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setCollection(true);
		simpleUsage.setSimpleType(simpleType);
		property.setType(simpleUsage);
		property.setNullable(true);
		ctSortRestrictions.getProperties().add(property);
		
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(property);
		annotation.setTerm(coreVocabulary.getValueTermIsPropertyPath());
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtInsertRestrictions = OdataFactory.eINSTANCE.createValueTerm();
		vtInsertRestrictions.setName(ICapabilityVocabulary.VT_INSERT_RESTRICTIONS);
		complexUsage = OdataFactory.eINSTANCE.createComplexTypeUsage();
		complexUsage.setComplexType(ctInsertRestrictions);
		vtInsertRestrictions.setType(complexUsage);	
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.InsertRestrictions); 
		vtInsertRestrictions.setDocumentation(doc);
		schema.getValueTerms().add(vtInsertRestrictions);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtInsertRestrictions);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntitySet());
		schema.getValueAnnotations().add(annotation);
		
		// value term
		ValueTerm vtUpdateRestrictions = OdataFactory.eINSTANCE.createValueTerm();
		vtUpdateRestrictions.setName(ICapabilityVocabulary.VT_UPDATE_RESTRICTIONS);
		complexUsage = OdataFactory.eINSTANCE.createComplexTypeUsage();
		complexUsage.setComplexType(ctUpdateRestrictions);
		vtUpdateRestrictions.setType(complexUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.UpdateRestrictions); 
		vtUpdateRestrictions.setDocumentation(doc);
		schema.getValueTerms().add(vtUpdateRestrictions);				
		
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtUpdateRestrictions);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntitySet());
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtDeleteRestrictions = OdataFactory.eINSTANCE.createValueTerm();
		vtDeleteRestrictions.setName(ICapabilityVocabulary.VT_DELETE_RESTRICTIONS);
		complexUsage = OdataFactory.eINSTANCE.createComplexTypeUsage();
		complexUsage.setComplexType(ctDeleteRestrictions);
		vtDeleteRestrictions.setType(complexUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.DeleteRestrictions); 
		vtDeleteRestrictions.setDocumentation(doc);
		schema.getValueTerms().add(vtDeleteRestrictions);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtDeleteRestrictions);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntitySet());
		schema.getValueAnnotations().add(annotation);
		
		// value term
		ValueTerm vtCountable = OdataFactory.eINSTANCE.createValueTerm();
		vtCountable.setName(ICapabilityVocabulary.VT_COUNTABLE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtCountable.setType(simpleUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.Countable); 
		vtCountable.setDocumentation(doc);
		schema.getValueTerms().add(vtCountable);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtCountable);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntitySet());
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtQueryable = OdataFactory.eINSTANCE.createValueTerm();
		vtQueryable.setName(ICapabilityVocabulary.VT_QUERYABLE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtQueryable.setType(simpleUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.Queryable); 
		vtQueryable.setDocumentation(doc);		
		schema.getValueTerms().add(vtQueryable);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtQueryable);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntitySet());
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtClientPageable = OdataFactory.eINSTANCE.createValueTerm();
		vtClientPageable.setName(ICapabilityVocabulary.VT_CLIENT_PAGEABLE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtClientPageable.setType(simpleUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.ClientPageable); 
		vtClientPageable.setDocumentation(doc);	
		schema.getValueTerms().add(vtClientPageable);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtClientPageable);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntitySet());
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtExpandable = OdataFactory.eINSTANCE.createValueTerm();
		vtExpandable.setName(ICapabilityVocabulary.VT_EXPANDABLE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtExpandable.setType(simpleUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.Expandable); 
		vtExpandable.setDocumentation(doc);	
		schema.getValueTerms().add(vtExpandable);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtExpandable);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToNavigationProperty());
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtNavigable = OdataFactory.eINSTANCE.createValueTerm();
		vtNavigable.setName(ICapabilityVocabulary.VT_NAVIGABLE);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtNavigable.setType(simpleUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.Navigable); 
		vtNavigable.setDocumentation(doc);	
		schema.getValueTerms().add(vtNavigable);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtNavigable);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToNavigationProperty());
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtIndexableByKey = OdataFactory.eINSTANCE.createValueTerm();
		vtIndexableByKey.setName(ICapabilityVocabulary.VT_INDEXABLE_BY_KEY);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtIndexableByKey.setType(simpleUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.IndexableByKey); 
		vtIndexableByKey.setDocumentation(doc);	
		schema.getValueTerms().add(vtIndexableByKey);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtIndexableByKey);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntitySet());
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtBatchSupported = OdataFactory.eINSTANCE.createValueTerm();
		vtBatchSupported.setName(ICapabilityVocabulary.VT_BATCH_SUPPORTED);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.BOOLEAN);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		vtBatchSupported.setType(simpleUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.BatchSupported); 
		vtBatchSupported.setDocumentation(doc);	
		schema.getValueTerms().add(vtBatchSupported);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtBatchSupported);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntityContainer());
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtQueryFunctions = OdataFactory.eINSTANCE.createValueTerm();
		vtQueryFunctions.setName(ICapabilityVocabulary.VT_QUERY_FUNCTIONS);
		simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(EDMTypes.STRING);
		booleanValue = OdataFactory.eINSTANCE.createBooleanValue();
		booleanValue.setValue(true);
		simpleType.setDefaultValue(booleanValue);
		simpleUsage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		simpleUsage.setSimpleType(simpleType);
		simpleUsage.setCollection(true);
		vtQueryFunctions.setType(simpleUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.QueryFunctions); 
		vtQueryFunctions.setDocumentation(doc);	
		schema.getValueTerms().add(vtQueryFunctions);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtQueryFunctions);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntityContainer());
		schema.getValueAnnotations().add(annotation);

		// value term
		ValueTerm vtFilterRestrictions = OdataFactory.eINSTANCE.createValueTerm();
		vtFilterRestrictions.setName(ICapabilityVocabulary.VT_FILTER_RESTRICTIONS);
		complexUsage = OdataFactory.eINSTANCE.createComplexTypeUsage();
		complexUsage.setComplexType(ctFilterRestrictions);
		vtFilterRestrictions.setType(complexUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.FilterRestrictions); 
		vtFilterRestrictions.setDocumentation(doc);	
		schema.getValueTerms().add(vtFilterRestrictions);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtFilterRestrictions);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntitySet());
		schema.getValueAnnotations().add(annotation);
		
		// value term
		ValueTerm vtSortRestrictions = OdataFactory.eINSTANCE.createValueTerm();
		vtSortRestrictions.setName(ICapabilityVocabulary.VT_SORT_RESTRICTIONS);
		complexUsage = OdataFactory.eINSTANCE.createComplexTypeUsage();
		complexUsage.setComplexType(ctSortRestrictions);
		vtSortRestrictions.setType(complexUsage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.SortRestrictions); 
		vtSortRestrictions.setDocumentation(doc);	
		schema.getValueTerms().add(vtSortRestrictions);

		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtSortRestrictions);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToEntitySet());
		schema.getValueAnnotations().add(annotation);
		
		return schema;
	}

}
