/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.SchemaAdapterImpl;
import org.eclipse.ogee.model.api.VocabularyException;
import org.eclipse.ogee.model.api.impl.Messages;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.CollectableExpression;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.RecordValue;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.model.odata.impl.PropertyToValueMapImpl;

public class VocabularyAdapterImpl extends SchemaAdapterImpl implements IVocabulary {

	public VocabularyAdapterImpl(IVocabularyContext context, Schema schema) {
		super(context, schema);
	}

	@Override
	public boolean isAdapterForType(Object type) {
		if(type==IVocabulary.class){
			return true;
		}
		return super.isAdapterForType(type);
	}

	@Override
	public IVocabularyContext getContext() throws ModelAPIException {
		return this.context;
	}

	@Override
	public ValueTerm getValueTerm(String name) throws ModelAPIException {
		if(name==null) throw new IllegalStateException(Messages.IllegalStateException_ArgumentsValueNotBound);
		for(ValueTerm term : this.schema.getValueTerms()){
			if(term==null || term.getName()==null){
				continue;
			}
			if(term.getName().equals(name)) {
				return term;
			}
		}
		return null;
	}

	@Override
	public List<ValueTerm> getApplicableValueTerms(EObject object) throws ModelAPIException {
		ICoreVocabulary core = (ICoreVocabulary)this.context.getVocabulary(ICoreVocabulary.VC_NAMESPACE);
		List<ValueTerm> filter = new ArrayList<ValueTerm>( );
		filter.add(core.getValueTermAppliesToAssociationSet());
		filter.add(core.getValueTermAppliesToEntitySet());
		filter.add(core.getValueTermAppliesToEntityType());
		filter.add(core.getValueTermAppliesToComplexType());
		filter.add(core.getValueTermAppliesToProperty());
		filter.add(core.getValueTermAppliesToNavigationProperty());
		filter.add(core.getValueTermAppliesToEntityContainer());
		filter.add(core.getValueTermAppliesToParameter());
		filter.add(core.getValueTermAppliesToValueTerm());

		ValueTerm select = null;
		if(object==null || !(object instanceof IAnnotationTarget)) {
			throw new VocabularyException(Messages.VocabularyException_InvalidObjectForApplicableTerms);
		}else if(object instanceof AssociationSet) {
			select = core.getValueTermAppliesToAssociationSet();
		}else if(object instanceof EntitySet) {
			select = core.getValueTermAppliesToEntitySet();
		}else if(object instanceof EntityType) {
			select = core.getValueTermAppliesToEntityType();
		}else if(object instanceof ComplexType) {
			select = core.getValueTermAppliesToComplexType();
		}else if(object instanceof Property) {
			select = core.getValueTermAppliesToProperty();
		}else if(object instanceof NavigationProperty) {
			select = core.getValueTermAppliesToNavigationProperty();
		}else if(object instanceof EntityContainer) {
			select = core.getValueTermAppliesToEntityContainer();
		}else if(object instanceof Parameter) {
			select = core.getValueTermAppliesToParameter();
		}else if(object instanceof ValueTerm) {
			select = core.getValueTermAppliesToValueTerm();
		}
		if(select!=null){
			filter.remove(select);
		}

		List<ValueTerm> result = new ArrayList<ValueTerm>();
		for(ValueTerm term : this.schema.getValueTerms()){
			if(isApplicableTerm(term, select, filter)) {
				result.add(term);
			}
		}
		return result;
	}

	private boolean isApplicableTerm(ValueTerm term, ValueTerm select, List<ValueTerm> filter) {
		ValueTerm valueTerm = null;
		boolean result = true;
		for(Annotation annotation : term.getAnnotations()){
			if(annotation instanceof ValueAnnotation) {
				valueTerm = ((ValueAnnotation)annotation).getTerm();
				if(valueTerm==select) {
					return true;
				}else if(filter.contains(valueTerm)){
					result = false;
				}else if(!isApplicableTerm(valueTerm, select, filter)){
					result = false;
				}
			}
		}
		return result;
	}

	@Override
	public ValueAnnotation createValueAnnotation(IAnnotationTarget target, ValueTerm term) throws ModelAPIException {
		ValueAnnotation annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(target);
		annotation.setTerm(term);
		Schema targetSchema = (Schema) getAncestorWithBehavior(target, Schema.class);
		if(targetSchema!=null) {
			targetSchema.getValueAnnotations().add(annotation);
		}
		annotation.setAnnotationValue(createAnnotationValue(term.getType()));
		return annotation;
	}

	@Override
	public CollectableExpression createCollectionValue(ValueCollection collection) throws ModelAPIException {
		try{
			IPropertyTypeUsage type = null;
			EObject parent = collection.eContainer();
			if(parent instanceof PropertyToValueMapImpl){
				type = ((PropertyToValueMapImpl)parent).getKey().getType();
			}else if(parent instanceof ValueAnnotation){
				type = ((ValueAnnotation)parent).getTerm().getType();
			}

			CollectableExpression value = null;
			if(type instanceof SimpleTypeUsage){
				value = createSimpleValue(((SimpleTypeUsage)type).getSimpleType());
			}else if(type instanceof ComplexTypeUsage){
				value = createRecordValue( ((ComplexTypeUsage)type).getComplexType());
			}else if(type instanceof EnumTypeUsage){
				value = this.createEnumValue((EnumTypeUsage)type);
			}

			if(value!=null) {
				return value;
			}
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput);
		}catch(NullPointerException e){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput, e);
		}
	}

	private AnnotationValue createAnnotationValue(IPropertyTypeUsage type) throws ModelAPIException {
		try{
			if(type instanceof SimpleTypeUsage) {
				SimpleTypeUsage usage = (SimpleTypeUsage)type;
				if(usage.isCollection()) {
					return OdataFactory.eINSTANCE.createValueCollection();
				}else{
					return createSimpleValue(usage.getSimpleType());
				}
			}

			if(type instanceof EnumTypeUsage) {
				EnumTypeUsage usage = (EnumTypeUsage)type;
				if(usage.isCollection()) {
					return OdataFactory.eINSTANCE.createValueCollection();
				}else{
					return this.createEnumValue(usage);
				}
			}

			if(type instanceof ComplexTypeUsage) {
				ComplexTypeUsage usage = (ComplexTypeUsage)type;
				if(usage.isCollection()) {
					return OdataFactory.eINSTANCE.createValueCollection();
				}else{
					return createRecordValue(usage.getComplexType());
				}
			}

			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput);
		}catch(NullPointerException e){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput, e);
		}
	}

	private SimpleValue createSimpleValue(SimpleType type) throws ModelAPIException {
		SimpleValue result = null;
		switch(type.getType().getValue()){
		case EDMTypes.GUID_VALUE:             result = OdataFactory.eINSTANCE.createGuidValue();            break;
		case EDMTypes.BINARY_VALUE:           result = OdataFactory.eINSTANCE.createBinaryValue();          break;
		case EDMTypes.BOOLEAN_VALUE:          result = OdataFactory.eINSTANCE.createBooleanValue();         break;
		case EDMTypes.STRING_VALUE:           result = OdataFactory.eINSTANCE.createStringValue();          break;
		case EDMTypes.SINGLE_VALUE:           result = OdataFactory.eINSTANCE.createSingleValue();          break;
		case EDMTypes.DOUBLE_VALUE:           result = OdataFactory.eINSTANCE.createDoubleValue();          break;
		case EDMTypes.DECIMAL_VALUE:          result = OdataFactory.eINSTANCE.createDecimalValue();         break;
		case EDMTypes.BYTE_VALUE:             result = OdataFactory.eINSTANCE.createByteValue();            break;
		case EDMTypes.SBYTE_VALUE:            result = OdataFactory.eINSTANCE.createSByteValue();           break;
		case EDMTypes.INT16_VALUE:            result = OdataFactory.eINSTANCE.createInt16Value();           break;
		case EDMTypes.INT32_VALUE:            result = OdataFactory.eINSTANCE.createInt32Value();           break;
		case EDMTypes.INT64_VALUE:            result = OdataFactory.eINSTANCE.createInt64Value();           break;
		case EDMTypes.TIME_VALUE:             result = OdataFactory.eINSTANCE.createTimeValue();            break;
		case EDMTypes.DATE_TIME_VALUE:        result = OdataFactory.eINSTANCE.createDateTimeValue();        break;
		case EDMTypes.DATE_TIME_OFFSET_VALUE: result = OdataFactory.eINSTANCE.createDateTimeOffsetValue();  break;
		default:                              throw new ModelAPIException(Messages.ModelAPIException_InvalidInput);
		}

		SimpleValue defValue = type.getDefaultValue();
		if(defValue!=null){
			result.setValueObject(defValue.getValueObject());
		}
		return result;
	}

	private RecordValue createRecordValue(ComplexType complexType) throws ModelAPIException {
		RecordValue recValue = OdataFactory.eINSTANCE.createRecordValue();
		for(Property property : complexType.getProperties()){
			recValue.getPropertyValues().put(property, createAnnotationValue(property.getType()));
		}
		return recValue;
	}
	
	private SimpleValue createEnumValue( EnumTypeUsage type ){
		SimpleValue value = null;
		
		EDMTypes underlyingType = type.getEnumType().getUnderlyingType();
		if(underlyingType == null){
			underlyingType = EDMTypes.INT32;
		}
		switch (underlyingType) {
		case SBYTE:
			value = OdataFactory.eINSTANCE.createSByteValue();
		case BYTE:
			value = OdataFactory.eINSTANCE.createByteValue();
		case INT16:
			value = OdataFactory.eINSTANCE.createInt16Value();
		case INT64:
			value = OdataFactory.eINSTANCE.createInt64Value();
		default:
			value = OdataFactory.eINSTANCE.createInt32Value();
		}
		return value;
	}

	private EObject getAncestorWithBehavior(EObject node, Class<? extends EObject> behavior) {
		EObject object = node;
		while(object!=null){
			if(behavior.isInstance(object)){
				break;
			}
			object = object.eContainer();
		}
		return object;
	}
}
