/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies.impl;

import java.util.ListIterator;

import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.vocabularies.IMeasuresVocabulary;
import org.eclipse.ogee.model.api.vocabularies.VocabularyAdapterImpl;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.ValueTerm;

public class MeasuresVocabularyAdapterImpl extends VocabularyAdapterImpl implements IMeasuresVocabulary {
	
	/**
	 * value term instances
	 */
	private ValueTerm valueTermIsoCurrency = null;
	private ValueTerm valueTermScale       = null;
	private ValueTerm valueTermUnit        = null;

	public MeasuresVocabularyAdapterImpl(IVocabularyContext context,
			Schema schema) {
		super(context, schema);
		ListIterator<ValueTerm> iterator = schema.getValueTerms().listIterator();
		while (iterator.hasNext()) {
			ValueTerm term = iterator.next();
			String name = term.getName();
			if(name==null) {
				continue;
			}
			if(name.equals(IMeasuresVocabulary.VT_ISOCURRENCY)) {
				this.valueTermIsoCurrency = term;
				continue;
			}
			if(name.equals(IMeasuresVocabulary.VT_SCALE)) {
				this.valueTermScale = term;
				continue;
			}
			if(name.equals(IMeasuresVocabulary.VT_UNIT)) {
				this.valueTermUnit = term;
				continue;
			}
		}
	}

	@Override
	public ValueTerm getValueTermIsoCurrency() {
		return this.valueTermIsoCurrency;
	}

	@Override
	public ValueTerm getValueTermScale() {
		return this.valueTermScale;
	}

	@Override
	public ValueTerm getValueTermUnit() {
		return this.valueTermUnit;
	}

}
