/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies.impl;

import org.eclipse.ogee.model.api.ISchemaProvider;
import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.SchemaAdapterImpl;
import org.eclipse.ogee.model.api.vocabularies.ICoreVocabulary;
import org.eclipse.ogee.model.api.vocabularies.IMeasuresVocabulary;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.StringValue;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueTerm;

public class MeasuresVocabularyProviderImpl implements ISchemaProvider {

	@Override
	public String getNamespace() throws ModelAPIException {
		return IMeasuresVocabulary.VC_NAMESPACE;
	}

	@Override
	public Schema createSchema(IVocabularyContext vocabularyContext) throws ModelAPIException {
		Schema          schema     = null;
		SimpleType      type       = null;
		SimpleTypeUsage usage      = null;
		ValueAnnotation annotation = null;
		Documentation 	doc 	   = null;
		StringValue     value      = null;
		
		// create schema instance
		schema = OdataFactory.eINSTANCE.createSchema();
		schema.setNamespace(IMeasuresVocabulary.VC_NAMESPACE);
		schema.getClassifiers().add(SchemaClassifier.VOCABULARY);

		// uses core vocabulary
		ICoreVocabulary coreVocabulary = (ICoreVocabulary)vocabularyContext.provideVocabulary(ICoreVocabulary.VC_NAMESPACE);
		Using using = OdataFactory.eINSTANCE.createUsing();
		using.setAlias(ICoreVocabulary.VC_DEFAULT_ALIAS);
		using.setUsedNamespace(coreVocabulary.getSchema());
		schema.getUsings().add(using);
		
		// value term
		ValueTerm vtIsoCurrency = OdataFactory.eINSTANCE.createValueTerm();
		vtIsoCurrency.setName(IMeasuresVocabulary.VT_ISOCURRENCY);
		type = OdataFactory.eINSTANCE.createSimpleType();
		type.setType(EDMTypes.STRING);
		usage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		usage.setSimpleType(type);
		vtIsoCurrency.setType(usage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.IsoCurrency); 
		vtIsoCurrency.setDocumentation(doc);
		schema.getValueTerms().add(vtIsoCurrency);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtIsoCurrency);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToProperty());
		schema.getValueAnnotations().add(annotation);
		
		// value term
		ValueTerm vtScale = OdataFactory.eINSTANCE.createValueTerm();
		vtScale.setName(IMeasuresVocabulary.VT_SCALE);
		type = OdataFactory.eINSTANCE.createSimpleType();
		type.setType(EDMTypes.BYTE);
		usage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		usage.setSimpleType(type);
		vtScale.setType(usage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.Scale); 
		vtScale.setDocumentation(doc);
		schema.getValueTerms().add(vtScale);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtScale);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToProperty());
		schema.getValueAnnotations().add(annotation);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtScale);
		annotation.setTerm(coreVocabulary.getValueTermRequiresType());
		value = OdataFactory.eINSTANCE.createStringValue();
		value.setValue("Edm.Decimal"); //$NON-NLS-1$
		annotation.setAnnotationValue(value);
		schema.getValueAnnotations().add(annotation);
		
		// value term
		ValueTerm vtUnit = OdataFactory.eINSTANCE.createValueTerm();
		vtUnit.setName(IMeasuresVocabulary.VT_UNIT);
		type = OdataFactory.eINSTANCE.createSimpleType();
		type.setType(EDMTypes.STRING);
		usage = OdataFactory.eINSTANCE.createSimpleTypeUsage();
		usage.setSimpleType(type);
		vtUnit.setType(usage);
		doc = OdataFactory.eINSTANCE.createDocumentation();
		doc.setSummary(Messages.Unit); 
		vtUnit.setDocumentation(doc);
		schema.getValueTerms().add(vtUnit);
		annotation = OdataFactory.eINSTANCE.createValueAnnotation();
		annotation.setTarget(vtUnit);
		annotation.setTerm(coreVocabulary.getValueTermAppliesToProperty());
		schema.getValueAnnotations().add(annotation);
	
		return schema;
	}

	@Override
	public SchemaAdapterImpl createAdapter(IVocabularyContext vocabularyContext, Schema schema) throws ModelAPIException {
		return new MeasuresVocabularyAdapterImpl(vocabularyContext, schema);
	}

}
