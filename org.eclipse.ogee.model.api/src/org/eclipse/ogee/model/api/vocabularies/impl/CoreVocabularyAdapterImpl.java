/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.vocabularies.impl;

import java.util.ListIterator;

import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.vocabularies.ICoreVocabulary;
import org.eclipse.ogee.model.api.vocabularies.VocabularyAdapterImpl;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.ValueTerm;

public class CoreVocabularyAdapterImpl extends VocabularyAdapterImpl implements ICoreVocabulary {

	/**
	 * value term instances
	 */
	private ValueTerm valueTermAppliesToAssociationSet     = null;
	private ValueTerm valueTermAppliesToEntitySet          = null;
	private ValueTerm valueTermAppliesToEntityType         = null;
	private ValueTerm valueTermAppliesToComplexType        = null;
	private ValueTerm valueTermAppliesToParameter          = null;
	private ValueTerm valueTermAppliesToProperty           = null;
	private ValueTerm valueTermAppliesToNavigationProperty = null;
	private ValueTerm valueTermAppliesToEntityContainer    = null;
	private ValueTerm valueTermAppliesToValueTerm          = null;
	private ValueTerm valueTermRequiresType                = null;
	private ValueTerm valueTermIsPropertyPath              = null;
	private ValueTerm valueTermIsURI                       = null;
	
	public CoreVocabularyAdapterImpl(IVocabularyContext context, Schema schema) {
		super(context, schema);
		ListIterator<ValueTerm> iterator = schema.getValueTerms().listIterator();
		while (iterator.hasNext()) {
			ValueTerm term = iterator.next();
			String name = term.getName();
			if(name==null) {
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_APPLIES_TO_ASSOCIATION_SET)) {
				this.valueTermAppliesToAssociationSet = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_APPLIES_TO_ENTITY_SET)) {
				this.valueTermAppliesToEntitySet = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_APPLIES_TO_ENTITY_TYPE)) {
				this.valueTermAppliesToEntityType = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_APPLIES_TO_COMPLEX_TYPE)) {
				this.valueTermAppliesToComplexType = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_APPLIES_TO_PROPERTY)) {
				this.valueTermAppliesToProperty = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_APPLIES_TO_NAVIGATION_PROPERTY)) {
				this.valueTermAppliesToNavigationProperty = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_APPLIES_TO_PARAMETER)) {
				this.valueTermAppliesToParameter = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_APPLIES_TO_VALUE_TERM)) {
				this.valueTermAppliesToValueTerm = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_APPLIES_TO_ENTITY_CONTAINER)) {
				this.valueTermAppliesToEntityContainer = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_REQUIRES_TYPE)) {
				this.valueTermRequiresType = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_IS_PROPERTY_PATH)) {
				this.valueTermIsPropertyPath = term;
				continue;
			}
			if(name.equals(ICoreVocabulary.VT_IS_URI)) {
				this.valueTermIsURI = term;
				continue;
			}
		}
	}
	
	@Override
	public ValueTerm getValueTermAppliesToAssociationSet() {
		return this.valueTermAppliesToAssociationSet;
	}

	@Override
	public ValueTerm getValueTermAppliesToEntitySet() {
		return this.valueTermAppliesToEntitySet;
	}

	@Override
	public ValueTerm getValueTermAppliesToEntityType() {
		return this.valueTermAppliesToEntityType;
	}

	@Override
	public ValueTerm getValueTermAppliesToComplexType() {
		return this.valueTermAppliesToComplexType;
	}

	@Override
	public ValueTerm getValueTermAppliesToProperty() {
		return this.valueTermAppliesToProperty;
	}

	@Override
	public ValueTerm getValueTermAppliesToNavigationProperty() {
		return this.valueTermAppliesToNavigationProperty;
	}

	@Override
	public ValueTerm getValueTermAppliesToParameter() {
		return this.valueTermAppliesToParameter;
	}

	@Override
	public ValueTerm getValueTermAppliesToValueTerm() {
		return this.valueTermAppliesToValueTerm;
	}

	@Override
	public ValueTerm getValueTermAppliesToEntityContainer() {
		return this.valueTermAppliesToEntityContainer;
	}

	@Override
	public ValueTerm getValueTermRequiresType() {
		return this.valueTermRequiresType;
	}

	@Override
	public ValueTerm getValueTermIsPropertyPath() {
		return this.valueTermIsPropertyPath;
	}

	@Override
	public ValueTerm getValueTermIsURI() {
		return this.valueTermIsURI;
	}

}
