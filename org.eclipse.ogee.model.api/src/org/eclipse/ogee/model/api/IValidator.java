/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Property;

/**
 * The OData validator validates an EDMXSet and name constraints on several OData artifacts.
 *
 */
public interface IValidator {

	/**
	 * Validates an OData EDMX set according to the OData 3.0 specification.
	 * For each constraint violation a corresponding problem marker is created.
	 * @param edmxSet The EDMX set which is going to be validated
	 */
	public Diagnostic validate(EDMXSet edmxSet);
	
	/**
	 * Validates the name of the entity type including uniqueness test as defined by the OData specification.
	 * @param entityType The target entity type
	 * @return
	 */
	public IStatus isValidName(EntityType entityType);
	
	/**
	 * Validates the name of the complex type including uniqueness test as defined by the OData specification.
	 * @param complexType The target complex type
	 * @return
	 */
	public IStatus isValidName(ComplexType complexType);
	
	/**
	 * Validates the name of the association including uniqueness test as defined by the OData specification.
	 * @param association The target association
	 * @return
	 */
	public IStatus isValidName(Association association);
	
	/**
	 * Validates the name of the entity set including uniqueness test as defined by the OData specification.
	 * @param entitySet The target entity set
	 * @return
	 */
	public IStatus isValidName(EntitySet entitySet);
	
	/**
	 * Validates the name of the navigation property including uniqueness test as defined by the OData specification.
	 * @param navigationProperty The target navigation property
	 * @return
	 */
	public IStatus isValidName(NavigationProperty navigationProperty);
	
	/**
	 * Validates the name of the property including uniqueness test as defined by the OData specification.
	 * @param property The target property
	 * @return
	 */
	public IStatus isValidName(Property property);
	
}
