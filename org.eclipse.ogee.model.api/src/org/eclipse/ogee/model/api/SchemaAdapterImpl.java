/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.ogee.model.api.impl.Messages;
import org.eclipse.ogee.model.odata.Schema;

public class SchemaAdapterImpl extends AdapterImpl implements ISchemaAdapter {

	protected final IVocabularyContext context;
	protected Schema schema;
	
	public SchemaAdapterImpl(IVocabularyContext context, Schema schema) {
		super();
		if(context==null) throw new IllegalStateException(Messages.IllegalStateException_ArgumentsValueNotBound);
		if(schema==null) throw new IllegalStateException(Messages.IllegalStateException_ArgumentsValueNotBound);
		this.context = context;
		this.schema = schema;
	}

	@Override
	public Schema getSchema() throws ModelAPIException {
		return this.schema;
	}

	@Override
	public boolean isAdapterForType(Object type) {
		if(type==ISchemaAdapter.class){
			return true;
		}
		return super.isAdapterForType(type);
	}

	@Override
	public Notifier getTarget() {
		return this.schema;
	}

	@Override
	public final void setTarget(Notifier newTarget) {
		if(newTarget!=this.schema){
			throw new IllegalStateException(Messages.IllegalStateException_AdapterTargetNotChangable);
		}
	}

	@Override
	public final void unsetTarget(Notifier oldTarget) {
		if(oldTarget == this.schema){
			this.schema = null;
		}
		else { 
			throw new IllegalStateException(Messages.IllegalStateException_AdapterTargetNotChangable);
		}
	}

}
