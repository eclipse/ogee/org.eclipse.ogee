/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.Schema;

/**
 * A resource context can be retrieved per EDMXSet using the IModelContext convencience API.
 * The resource context provides convenience in respect to:
 * <ul>
 * <li>retrieving a vocabulary context</li> 
 * <li>calculating all schemata in scope relative to the given schema</li> 
 * <li>adding an EDMX reference (for example in order to reference another vocabulary)</li> 
 * <li>remove an EDMX reference (for example in order to dereference another vocabulary)</li> 
 * </ul>
 */
public interface IResourceContext {
	
	/**
	 * Returns the corresponding vocabulary context
	 * @return IVocabularyContext
	 * @see IVocabularyContext
	 */
	public IVocabularyContext getVocabularyContext() throws ModelAPIException;
	
	/**
	 * Returns the schemata, which are in scope of the given schema.
	 * @param schema The schema which defines the scope
	 * @return The schemata which are in scope of the given schema
	 * @throws ModelAPIException
	 */
	public List<Schema> getSchemataInScope(Schema schema) throws ModelAPIException;
	
	/**
	 * Adds an EDMX to the actual EDMX set. EDMXs referenced by the new EDMX are added transitively.
	 * This method is not executed within an EMF command. The caller of this API method is responsible
	 * for wrapping into a corresponding command.
	 * @param edmx The EDMX going to be added
	 * @throws ModelAPIException
	 */
	public void addEDMX(EDMX edmx) throws ModelAPIException;
	
	/**
	 * Removes the EDMX from the actual EDMX set. EDMXs which are no longer in use are also removed transitively.
	 * This method is not executed within an EMF command. The caller of this API method is responsible for wrapping 
	 * into a corresponding command.
	 * @param edmx The EDMX going to be removed
	 * @throws ModelAPIException
	 */
	public void removeEDMX(EDMX edmx) throws ModelAPIException;
	
	/**
	 * Returns whether the given object is defined within the main scope.
	 * @param eObject The object going to be checked
	 * @return
	 */
	public boolean isInMainScope(EObject eObject) throws ModelAPIException;

	
}
