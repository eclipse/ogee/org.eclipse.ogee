/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.model.api.impl;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

/**
 *
 */
public class AutomateMapCleanupCommandImpl extends RecordingCommand {

	private final EMap<? extends EObject,? extends EObject> map;
	
	public AutomateMapCleanupCommandImpl(TransactionalEditingDomain domain, EMap<? extends EObject,? extends EObject> map) {
		super(domain);
		this.map = map;
	}

	public AutomateMapCleanupCommandImpl(TransactionalEditingDomain domain,	String label, EMap<? extends EObject,? extends EObject> map) {
		super(domain, label);
		this.map = map;
	}

	public AutomateMapCleanupCommandImpl(TransactionalEditingDomain domain, String label, String description, EMap<? extends EObject,? extends EObject> map) {
		super(domain, label, description);
		this.map = map;
	}

	@Override
	protected void doExecute() {
		while(this.map.containsKey(null)){
			this.map.removeKey(null);
		}
	}

}
