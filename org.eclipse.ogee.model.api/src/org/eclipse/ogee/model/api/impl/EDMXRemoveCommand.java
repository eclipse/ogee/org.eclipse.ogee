/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.ValueAnnotation;

public class EDMXRemoveCommand extends RecordingCommand {

	private HashMap<Schema, List<Using>> toBeDeletedUsings;
	private List<EDMX> toBeDeletedEdmx;
	private List<Schema> toBeDeletedSchemata;
	private EDMXSet edmxSet;
	private EDMX toBeDeletedEDMX;

	public EDMXRemoveCommand(TransactionalEditingDomain domain,
			EDMXSet edmxSet, EDMX toBeDeletedEDMX, List<EDMX> toBeDeletedEdmx,
			List<Schema> toBeDeletedSchemata,
			HashMap<Schema, List<Using>> toBeDeletedUsings) {
		super(domain);
		this.edmxSet = edmxSet;
		this.toBeDeletedUsings = toBeDeletedUsings;
		this.toBeDeletedEdmx = toBeDeletedEdmx;
		this.toBeDeletedSchemata = toBeDeletedSchemata;
		this.toBeDeletedEDMX = toBeDeletedEDMX;
	}

	@Override
	protected void doExecute() {
		Iterator<Schema> schemaIterator;
		Schema schema;
		
		Iterator<Schema> toBeDeletedSchemataIterator = toBeDeletedSchemata
				.iterator();
		while (toBeDeletedSchemataIterator.hasNext()) {
			Schema toBeDeletedSchema = toBeDeletedSchemataIterator.next();
			Iterator<ValueAnnotation> toBeDeletedValueAnnotationIterator = toBeDeletedSchema
					.getValueAnnotations().iterator();
			while (toBeDeletedValueAnnotationIterator.hasNext()) {
				ValueAnnotation toBeDeletedAnnotation = toBeDeletedValueAnnotationIterator
						.next();
				toBeDeletedAnnotation.setTerm(null);
				toBeDeletedAnnotation.setTarget(null);
			}
		}

		this.removeEDMXReference(this.toBeDeletedEDMX);
		this.edmxSet.getSchemata().removeAll(toBeDeletedSchemata);
		
		schemaIterator = toBeDeletedUsings.keySet().iterator();
		while (schemaIterator.hasNext()) {
			schema = schemaIterator.next();
			schema.getUsings().removeAll(toBeDeletedUsings.get(schema));
		}
		this.edmxSet.getFullScopeEDMX().removeAll(toBeDeletedEdmx);

	}

	private void removeEDMXReference(EDMX targetEDMX) {
		EDMXReference edmxReference = null;
		List<EDMXReference> toBeRemoved = new LinkedList<EDMXReference>();
		
		// Validate the input
		if (targetEDMX == null) {
			return;
		}

		Iterator<EDMXReference> edmxReferences = this.edmxSet.getMainEDMX()
				.getReferences().iterator();
		while (edmxReferences.hasNext()) {
			edmxReference = edmxReferences.next();
			if (edmxReference.getReferencedEDMX().equals(targetEDMX)) {
				toBeRemoved.add(edmxReference);
			}
		}
		this.edmxSet.getMainEDMX().getReferences().removeAll(toBeRemoved);

		Iterator<EDMX> edmxIterator = this.edmxSet.getFullScopeEDMX()
				.iterator();
		EDMX edmx;
		while (edmxIterator.hasNext()) {
			toBeRemoved = new LinkedList<EDMXReference>();
			edmx = edmxIterator.next();
			edmxReferences = edmx.getReferences().iterator();
			while (edmxReferences.hasNext()) {
				edmxReference = edmxReferences.next();
				if (edmxReference.getReferencedEDMX().equals(targetEDMX)) {
					toBeRemoved.add(edmxReference);
				}
			}
			edmx.getReferences().removeAll(toBeRemoved);
		}

	}

}
