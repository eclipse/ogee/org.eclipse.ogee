/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.impl;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME = "org.eclipse.ogee.model.api.impl.messages"; //$NON-NLS-1$

	public static String Validator_PropertyMapping_ValueMustExist;

	public static String Validator_EntityType_NameNotInitial;
	public static String Validator_ComplexType_NameNotInitial;
	public static String Validator_Property_NameNotInitial;
	public static String Validator_Property_TypeNotInitial;
	public static String Validator_EntityContainer_NameNotInitial;
	public static String Validator_Association_NameNotInitial;
	public static String Validator_NavigationProperty_NameNotInitial;
	public static String Validator_NavigationProperty_RelationshipNotInitial;
	public static String Validator_NavigationProperty_FromRoleNotInitial;
	public static String Validator_NavigationProperty_ToRoleNotInitial;
	public static String Validator_FunctionImport_NameNotInitial;
	public static String Validator_Parameter_NameNotInitial;
	public static String Validator_Parameter_TypeNotInitial;
	public static String Validator_EntitySet_NameNotInitial;
	public static String Validator_EntitySet_TypeNotInitial;
	public static String Validator_AssociationSet_NameNotInitial;
	public static String Validator_AssociationSet_AssociationNotInitial;
	public static String Validator_Role_NameNotInitial;
	public static String Validator_Role_MultiplicityNotInitial;
	public static String Validator_Role_TypeNotInitial;
	public static String Validator_AssociationSetEnd_RoleNotInitial;
	public static String Validator_AssociationSetEnd_EntitySetNotInitial;
	public static String Validator_EDMX_DataServiceNotInitial;
	public static String Validator_ReferentialConstraint_DependentRoleNotInitial;
	public static String Validator_ReferentialConstraint_PrincipalRoleNotInitial;
	public static String Validator_ReferentialConstraint_KeyMappingsMustExist;
	public static String Validator_PropertyMapping_DependentKeyPropertyMustExist;
	public static String Validator_PropertyMapping_PrincipalKeyPropertyMustExist;
	public static String Validator_SimpleType_TypeNotInitial;
	public static String Validator_Binding_TypeNotInitial;
	public static String Validator_DataService_VersionNotInitial;
	public static String Validator_ComplexTypeUsage_TypeNotInitial;
	public static String Validator_ReturnEntityTypeUsage_EntityTypeNotInitial;
	public static String Validator_EnumType_NameNotInitial;
	public static String Validator_EnumMember_NameNotInitial;
	public static String Validator_EnumTypeUsage_EnumTypeNotInitial;
	public static String Validator_ValueAnnotation_TargetNotInitial;
	public static String Validator_Using_UsedNamespaceNotInitial;
	public static String Validator_Using_DuplicateNamespace;

	public static String TransactionException_EMFObjectNotBound;
	public static String TransactionException_ResourceNotBound;
	public static String TransactionException_NoPlatformResource;

	public static String RegistryException_SchemaProviderNotFound;
	public static String RegistryException_InvalidSchemaClassifier;

	public static String VocabularyException_InvalidNamespace;
	public static String VocabularyException_InvalidObjectForApplicableTerms;

	public static String VocabularyException_DuplicateNamespace;
	public static String VocabularyException_NotSelfContaining;

	public static String ModelAPIException_InvalidInput;
	public static String ModelAPIException_InvalidSourceTargetForEDMXReference;
	public static String ModelAPIException_ErrorWhileRetrievingResourceContext;

	public static String IllegalStateException_ArgumentsValueNotBound;
	public static String IllegalStateException_AdapterTargetNotChangable;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
