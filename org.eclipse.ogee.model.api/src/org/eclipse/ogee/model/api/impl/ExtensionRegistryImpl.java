/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IRegistryEventListener;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ogee.model.api.ISchemaProvider;
import org.eclipse.ogee.model.api.RegistryException;
import org.eclipse.ogee.model.odata.SchemaClassifier;

public class ExtensionRegistryImpl implements IRegistryEventListener {

	private final static String EXSD_SCHEMATA_XP_PLUGIN = "org.eclipse.ogee.model.api"; //$NON-NLS-1$
	private final static String EXSD_SCHEMATA_XP_ID = "schemata"; //$NON-NLS-1$
	private final static String EXSD_SCHEMATA_XP_NAME = EXSD_SCHEMATA_XP_PLUGIN
			+ "." + EXSD_SCHEMATA_XP_ID; //$NON-NLS-1$
	private final static String EXSD_SCHEMATA_CE_SCHEMA = "schema"; //$NON-NLS-1$
	private final static String EXSD_SCHEMATA_CA_NAMESPACE = "namespace"; //$NON-NLS-1$
	private final static String EXSD_SCHEMATA_CA_URI = "uri"; //$NON-NLS-1$
	private final static String EXSD_SCHEMATA_CA_CLASSIFIER = "classifier"; //$NON-NLS-1$
	private final static String EXSD_SCHEMATA_CA_DESCRIPTION = "description"; //$NON-NLS-1$
	private final static String EXSD_SCHEMATA_CA_CLASS = "class"; //$NON-NLS-1$
	private final static String EXSD_SCHEMATA_CV_VOCABULARY = "Vocabulary"; //$NON-NLS-1$

	private final IExtensionRegistry registry;
	private final Map<String, IConfigurationElement> configurations;
	private final Map<String, ISchemaProvider> providers;
	private final List<String> vocabularies;

	public ExtensionRegistryImpl() {
		this.registry = Platform.getExtensionRegistry();
		this.configurations = new HashMap<String, IConfigurationElement>();
		this.providers = new HashMap<String, ISchemaProvider>();
		this.vocabularies = new ArrayList<String>();
	}

	/**
	 * Reads extension registry Registers as registry change listener
	 */
	public void init() {
		this.load();
		this.registry.addListener(this, EXSD_SCHEMATA_XP_NAME);
	}

	/**
	 * Free registry data Free registration as registry change listener
	 */
	public void dispose() {
		this.registry.removeListener(this);
		this.configurations.clear();
		this.providers.clear();
		this.vocabularies.clear();
	}

	@Override
	public void added(IExtensionPoint[] points) {
		return;
	}

	@Override
	public void removed(IExtensionPoint[] point) {
		return;
	}

	@Override
	public void added(IExtension[] extensions) {
		this.configurations.clear();
		this.providers.clear();
		this.vocabularies.clear();
		this.load();
	}

	@Override
	public void removed(IExtension[] extension) {
		this.configurations.clear();
		this.providers.clear();
		this.vocabularies.clear();
		this.load();
	}

	/**
	 * Returns the well-known URI for the namespace The value is taken from
	 * extension configuration data
	 */
	public String getSchemaURI(String namespace) throws RegistryException {
		IConfigurationElement element = this.configurations.get(namespace);
		if (element == null) {
			return new String();
		} else {
			return element.getAttribute(EXSD_SCHEMATA_CA_URI);
		}
	}

	/**
	 * Returns a user-readable description of the namespace The value is taken
	 * from extension configuration data
	 */
	public String getSchemaDescription(String namespace)
			throws RegistryException {
		IConfigurationElement element = this.configurations.get(namespace);
		if (element == null) {
			return new String();
		} else {
			return element.getAttribute(EXSD_SCHEMATA_CA_DESCRIPTION);
		}
	}

	/**
	 * Returns all registered namespaces that apply to the given schema
	 * classifier
	 */
	public List<String> getSchemaNamespaces(SchemaClassifier classifier)
			throws RegistryException {
		switch (classifier) {
		case VOCABULARY:
			return new ArrayList<String>(this.vocabularies);
		default:
			throw new RegistryException(Messages.bind(
					Messages.RegistryException_InvalidSchemaClassifier,
					classifier.toString()));
		}
	}

	/**
	 * Returns the schema provider associated with a given namespace
	 */
	public ISchemaProvider getSchemaProvider(String namespace)
			throws RegistryException {
		ISchemaProvider provider = this.providers.get(namespace);
		if (provider == null) {
			IConfigurationElement element = this.configurations.get(namespace);
			if (element != null) {
				try {
					provider = (ISchemaProvider) element
							.createExecutableExtension(EXSD_SCHEMATA_CA_CLASS);
				} catch (CoreException error) {
					throw new RegistryException(Messages.bind(
							Messages.RegistryException_SchemaProviderNotFound,
							namespace), error);
				}
			} else {
				throw new RegistryException(Messages.bind(
						Messages.RegistryException_SchemaProviderNotFound,
						namespace));
			}
			this.providers.put(namespace, provider);
		}
		return provider;
	}

	private void load() {
		IExtensionPoint point = this.registry.getExtensionPoint(
				EXSD_SCHEMATA_XP_PLUGIN, EXSD_SCHEMATA_XP_ID);
		IExtension[] extensions = point.getExtensions();
		for (int i = 0; i < extensions.length; i++) {
			IConfigurationElement[] elements = extensions[i]
					.getConfigurationElements();
			for (int j = 0; j < elements.length; j++) {
				String tagname = elements[j].getName();
				if (tagname == null || !tagname.equals(EXSD_SCHEMATA_CE_SCHEMA)) {
					continue;
				}
				String namespace = elements[j]
						.getAttribute(EXSD_SCHEMATA_CA_NAMESPACE);
				if (namespace == null) {
					continue;
				}
				String classifier = elements[j]
						.getAttribute(EXSD_SCHEMATA_CA_CLASSIFIER);
				if (classifier == null) {
					continue;
				}
				if (classifier.equals(EXSD_SCHEMATA_CV_VOCABULARY)) {
					this.vocabularies.add(namespace);
				}
				this.configurations.put(namespace, elements[j]);
			}
		}
	}

}
