/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.ogee.model.api.Activator;
import org.eclipse.ogee.utils.logger.Logger;

public class DomainSaveCommandImpl extends RecordingCommand {

	private static final String ENCODING_UTF8 = "UTF-8"; //$NON-NLS-1$
	
	private TransactionalEditingDomain domain = null;
	
	public DomainSaveCommandImpl(TransactionalEditingDomain domain) {
		super(domain);
		this.domain = domain;
	}

	public DomainSaveCommandImpl(TransactionalEditingDomain domain, String label) {
		super(domain, label);
		this.domain = domain;
	}

	public DomainSaveCommandImpl(TransactionalEditingDomain domain, String label, String description) {
		super(domain, label, description);
		this.domain = domain;
	}

	@Override
	protected void doExecute() {
		EList<Resource> resources = domain.getResourceSet().getResources();
		Map<String, String> options = new HashMap<String, String>();
		options.put(XMLResource.OPTION_ENCODING, ENCODING_UTF8);		
		for(Resource resource : resources){
			if (resource.isModified()) {
				try {
					resource.save(options);
				} catch (final IOException e) {
					Logger.getLogger(Activator.PLUGIN_ID).log(e.toString());
				}
			}
		}
	}

}
