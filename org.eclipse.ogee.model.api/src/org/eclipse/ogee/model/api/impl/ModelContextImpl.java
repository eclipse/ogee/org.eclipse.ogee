/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.impl;

import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.transaction.NotificationFilter;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.RollbackException;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.WorkspaceEditingDomainFactory;
import org.eclipse.ogee.model.api.Activator;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IResourceContext;
import org.eclipse.ogee.model.api.ISchemaAdapter;
import org.eclipse.ogee.model.api.ISchemaProvider;
import org.eclipse.ogee.model.api.IValidator;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.SchemaAdapterImpl;
import org.eclipse.ogee.model.api.TransactionException;
import org.eclipse.ogee.model.api.vocabularies.VocabularyAdapterImpl;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.utils.logger.Logger;

public final class ModelContextImpl extends AdapterFactoryImpl implements IModelContext, ResourceSetListener {

	public final static String ODATA_VERSION = "3.0";                              //$NON-NLS-1$
	public final static String DEFAULT_CONTAINER_NAME = "default";				   //$NON-NLS-1$

	public  final ExtensionRegistryImpl registry;
	
	public ModelContextImpl() {
		this.registry = new ExtensionRegistryImpl();
		this.registry.init();
	}

	@Override
	public boolean isFactoryForType(Object type) {
		if(type==IResourceContext.class){
			return true;
		}
		if(type==IVocabularyContext.class){
			return true;
		}
		if(type==ISchemaAdapter.class){
			return true;
		}
		if(type==IVocabulary.class){
			return true;
		}
		return super.isFactoryForType(type);
	}

	@Override
	protected Adapter createAdapter(Notifier target) {
		if(target==null) {
			return super.createAdapter(target);
		}else if(target instanceof EDMXSet){
			return new ResourceContextImpl(this, (EDMXSet) target);
		}else if(target instanceof Schema){
			Schema schema = (Schema) target;
			EDMXSet edmxSet = (EDMXSet) getAncestorWithBehavior(schema, EDMXSet.class);   
			IVocabularyContext context = (IVocabularyContext)this.adapt(edmxSet, IVocabularyContext.class);
			SchemaAdapterImpl adapter = null; 
			try{
				ISchemaProvider provider = this.registry.getSchemaProvider(schema.getNamespace());
				adapter = provider.createAdapter(context, schema);
			} catch (ModelAPIException error) {
				adapter = null;
			}
			if(adapter==null) {
				if(schema.getClassifiers().contains(SchemaClassifier.VOCABULARY)) {
					adapter = new VocabularyAdapterImpl(context, schema);
				}else{
					adapter = new SchemaAdapterImpl(context, schema);
				}
			}
			return adapter;
		}else{
			return super.createAdapter(target);
		}
	}

	@Override
	public IResourceContext getResourceContext(EDMXSet edmxSet) throws ModelAPIException {
		return (IResourceContext)this.adapt(edmxSet,IResourceContext.class);
	}
	
	@Override
	public IVocabularyContext getVocabularyContext(EDMXSet edmxSet) throws ModelAPIException {
		return (IVocabularyContext)this.adapt(edmxSet,IVocabularyContext.class);
	}

	@Override
	public IValidator createValidator() {
		return new Validator();
	}

	@Override
	public EDMXSet createServiceModel(String namespace) throws ModelAPIException {
		EDMXSet edmxSet = OdataFactory.eINSTANCE.createEDMXSet();
		EDMX mainEDMX = OdataFactory.eINSTANCE.createEDMX();
		edmxSet.setMainEDMX(mainEDMX);
		
		DataService service = OdataFactory.eINSTANCE.createDataService();
		service.setVersion(ODATA_VERSION);
		mainEDMX.setDataService(service);
		
		Schema schema = OdataFactory.eINSTANCE.createSchema();
		schema.setNamespace(namespace);
		schema.getClassifiers().add(SchemaClassifier.SERVICE);
		edmxSet.getSchemata().add(schema);
		service.getSchemata().add(schema);
		
		EntityContainer container = OdataFactory.eINSTANCE.createEntityContainer();
		container.setDefault(true);
		container.setName(DEFAULT_CONTAINER_NAME);
		schema.getContainers().add(container);
		return edmxSet;
	}
	
	@Override
	public IFile createModelFile(URI resourceURI, EDMXSet serviceModel, EObject viewModel) throws ModelAPIException {
		if(resourceURI==null) {
			throw new TransactionException(Messages.TransactionException_NoPlatformResource);
		}

		// create main file using resource URI
		Path path = new Path(resourceURI.toPlatformString(true));
		IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
		TransactionalEditingDomain editingDomain = getTransaction(file);
		ResourceSet resourceSet = editingDomain.getResourceSet();
		Resource main = resourceSet.createResource(resourceURI);

		// create root nodes in editing domain
		CompoundCommand command = new CompoundCommand();
		if(viewModel!=null) {
			command.append(new AddCommand(editingDomain,main.getContents(),viewModel));
		}
		if(serviceModel!=null) {
			command.append(new AddCommand(editingDomain,main.getContents(),serviceModel));
		}
		editingDomain.getCommandStack().execute(command.unwrap());
		
		main.setModified(true);

		return file;
	}
	
	@Override
	public void saveModelFile(IFile file) throws ModelAPIException {
		TransactionalEditingDomain domain = getTransaction(file);
		domain.getCommandStack().execute(new DomainSaveCommandImpl(domain));
		domain.getCommandStack().flush();
	}

	@Override
	public TransactionalEditingDomain getTransaction(EObject eObject) throws ModelAPIException {
		if(eObject == null){
			throw new TransactionException(Messages.TransactionException_EMFObjectNotBound);
		}
		Resource resource = eObject.eResource();
		if(resource == null){
			throw new TransactionException(Messages.TransactionException_ResourceNotBound);
		}
		URI uri = resource.getURI();
		if(!uri.isPlatform()){
			throw new TransactionException(Messages.TransactionException_NoPlatformResource);
		}
		String uriString = uri.trimFragment().toPlatformString(true);
		Path path = new Path(uriString);
		IFile odataModelFile = ResourcesPlugin.getWorkspace().getRoot().getFile(path.makeAbsolute());
		
		return this.getTransaction(odataModelFile);
	}

	@Override
	public TransactionalEditingDomain getTransaction(IFile odataModelFile) throws ModelAPIException {
		if(odataModelFile == null){
			throw new TransactionException(Messages.TransactionException_ResourceNotBound);
		}
		TransactionalEditingDomain transaction = null;
		String transactionId = odataModelFile.getFullPath().toString();
		transaction = TransactionalEditingDomain.Registry.INSTANCE.getEditingDomain(transactionId);
		if(transaction == null){
			transaction = WorkspaceEditingDomainFactory.INSTANCE.createEditingDomain();
			transaction.setID(transactionId);
			TransactionalEditingDomain.Registry.INSTANCE.add(transactionId, transaction);
			// Register the current instance of the model context as a resource set listener at the newly created transaction
			// We'd like to be informed once an EDMX set is added...
			transaction.addResourceSetListener(this);
		}
		return transaction;
	}	
	
	@Override
	public void removeTransaction(TransactionalEditingDomain transaction) throws ModelAPIException {
		if(transaction == null){
			return;
		}
		String transactionId = transaction.getID();
		this.disposeTransaction(transactionId);
	}

	@Override
	public NotificationFilter getFilter() {
		return NotificationFilter.NOT_TOUCH;
	}

	@Override
	public Command transactionAboutToCommit(ResourceSetChangeEvent event) throws RollbackException {
		CompoundCommand finalizer = new CompoundCommand();
		for(Notification notification : event.getNotifications()){
			// principal of key mapping underneath referential constraint with null key(s)
			if(notification.getFeature()==OdataPackage.eINSTANCE.getPropertyMapping_Key() && notification.getEventType()==Notification.SET && notification.getNewValue()==null && notification.getOldValue() instanceof Property){
				ReferentialConstraint parent = (ReferentialConstraint)((EObject)notification.getNotifier()).eContainer(); 
				finalizer.append(new AutomateMapCleanupCommandImpl(event.getEditingDomain(),parent.getKeyMappings()));
			}
			// TODO check whether it can be done for any map generically, find map from entry
//			if(notification.getNotifier() instanceof Map.Entry){
//				EObject parent = ((EObject)notification.getNotifier()).eContainer();
//				
//			}
		}
		if(finalizer.isEmpty()){
			return null;
		}else{
			return finalizer.unwrap();
		}
	}

	@Override
	public void resourceSetChanged(ResourceSetChangeEvent event) {
		Notification notification = null;
		Iterator<Notification> notifications = event.getNotifications().iterator();

		while(notifications.hasNext()){
			notification = notifications.next();
			// As soon as an EDMX set is added in the context of a transaction, where this instance is listening, load the corresponding
			// resource context. This is important to react on for example new vocabularies, removal of existing ones etc.
			if(notification.getEventType() == Notification.ADD && (notification.getNewValue() instanceof EDMXSet)){
				try {
					this.getResourceContext((EDMXSet)notification.getNewValue());
				} catch (ModelAPIException e) {
					Logger.getLogger(Activator.PLUGIN_ID).logError(Messages.ModelAPIException_ErrorWhileRetrievingResourceContext,e);
				}
			// The location of a resource set and therefore also the transaction ID has changed
			}else if(notification.getEventType() == Notification.SET && (notification.getNewValue() instanceof URI)	&& (notification.getNotifier() instanceof Resource)){
				updateTransactionRegistration((URI)notification.getOldValue(),(URI)notification.getNewValue());
			}
		}
		return;
	}

	@Override
	public boolean isAggregatePrecommitListener() {
		return false;
	}

	@Override
	public boolean isPrecommitOnly() {
		return false;
	}

	@Override
	public boolean isPostcommitOnly() {
		return false;
	}
	
	private void disposeTransaction(String transactionId){
		TransactionalEditingDomain transaction;

		transaction = TransactionalEditingDomain.Registry.INSTANCE.getEditingDomain(transactionId);
		TransactionalEditingDomain.Registry.INSTANCE.remove(transactionId);
		transaction.dispose();
	}
	
	private void updateTransactionRegistration(URI oldUri, URI newUri){
		TransactionalEditingDomain transaction;
		
		String oldTransactionId = calculateTransactionId(oldUri);
		String newTransactionId = calculateTransactionId(newUri);
		if(oldTransactionId == null || newTransactionId == null){
			return;
		}
		
		transaction = TransactionalEditingDomain.Registry.INSTANCE
				.getEditingDomain(oldTransactionId);
		if (transaction == null) {
			return;
		}
		TransactionalEditingDomain.Registry.INSTANCE.remove(oldTransactionId);
		TransactionalEditingDomain.Registry.INSTANCE.add(newTransactionId, transaction);
	}
	
	private String calculateTransactionId(URI uri){
		String transactionId = null;
		if(uri == null || !uri.isPlatform()){
			return null;
		}
		
		String uriString = uri.trimFragment().toPlatformString(true);
		Path path = new Path(uriString);
		IFile odataModelFile = ResourcesPlugin.getWorkspace().getRoot()
				.getFile(path.makeAbsolute());
		if (odataModelFile == null) {
			return null;
		}
		transactionId = odataModelFile.getFullPath().toString();
		return transactionId;
	}

	private EObject getAncestorWithBehavior(EObject node, Class<? extends EObject> behavior) {
		EObject object = node;
		while(object!=null) {
			if(behavior.isInstance(object)){
				break;
			}
			object = object.eContainer();
		}
		return object;
	}
	
}