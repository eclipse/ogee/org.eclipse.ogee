/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.model.api.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.ogee.model.api.IResourceContext;
import org.eclipse.ogee.model.api.ISchemaAdapter;
import org.eclipse.ogee.model.api.ISchemaProvider;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.ObjectInUseException;
import org.eclipse.ogee.model.api.TransactionException;
import org.eclipse.ogee.model.api.VocabularyException;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.Using;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.util.ModelException;
import org.eclipse.ogee.model.odata.util.OdataModelHelper;
import org.eclipse.ogee.model.odata.util.OdataSchemataReferencer;

public class ResourceContextImpl extends EContentAdapter implements IResourceContext, IVocabularyContext {

	private final ModelContextImpl parent;
	private final EDMXSet edmxSet;

	public ResourceContextImpl(ModelContextImpl parent, EDMXSet edmxSet) {
		this.parent = parent;
		this.edmxSet = edmxSet;
	}

	@Override
	public boolean isAdapterForType(Object type) {
		if(type==IResourceContext.class){
			return true;
		}
		if(type==IVocabularyContext.class){
			return true;
		}
		return super.isAdapterForType(type);
	}

	@Override
	public Notifier getTarget() {
		return this.edmxSet;
	}

	@Override
	public void notifyChanged(Notification notification) {
		super.notifyChanged(notification);

		// TODO special change handling
	}

	@Override
	public IVocabularyContext getVocabularyContext() {
		return this;
	}

	@Override
	public IVocabulary provideVocabulary(String namespace) throws ModelAPIException {
		Schema schema = this.addRegisteredSchema(namespace);
		return createVocabularyAdapter(schema);
	}

	@Override
	public IVocabulary getVocabulary(String namespace) throws ModelAPIException {
		Schema schema = this.findSchema(namespace);
		if(schema==null){
			throw new VocabularyException(Messages.bind(Messages.VocabularyException_InvalidNamespace, namespace));
		}
		return createVocabularyAdapter(schema);
	}

	@Override
	public String getVocabularyDescription(String namespace) throws ModelAPIException {
		return this.parent.registry.getSchemaDescription(namespace);
	}

	@Override
	public List<String> getRegisteredVocabularies() throws ModelAPIException {
		return this.parent.registry.getSchemaNamespaces(SchemaClassifier.VOCABULARY);
	}

	@Override
	public List<String> getAvailableVocabularies() throws ModelAPIException {
		List<String> result = new ArrayList<String>();
		if(this.edmxSet.getMainEDMX()!=null){
			for(EDMXReference reference : this.edmxSet.getMainEDMX().getReferences()){
				if(reference.getReferencedEDMX()!=null && reference.getReferencedEDMX().getDataService()!=null){
					for(Schema schema : reference.getReferencedEDMX().getDataService().getSchemata()){
						if(schema.getNamespace()!=null&&schema.getClassifiers().contains(SchemaClassifier.VOCABULARY)){
							result.add(schema.getNamespace());
						}
					}
				}
			}
		}
		return result;
	}

	@Override
	public List<Schema> getSchemataInScope(Schema schema) throws ModelAPIException {
		try{
			OdataModelHelper helper = new OdataModelHelper(this.edmxSet);
			return helper.getSchemataInScope(schema);
		}catch(ModelException e){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput,e);
		}
	}

	@Override
	public boolean isInMainScope(EObject eObject) throws ModelAPIException {
		if(eObject == null){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput);
		}
		Schema schema = null;
		if(eObject instanceof Schema){
			schema = (Schema)eObject;
		}
		while(eObject!=null && schema == null){
			if(eObject.eContainer() instanceof Schema){
				schema = (Schema)eObject.eContainer();
			}else{
				eObject = eObject.eContainer();
			}
		}
		
		if(schema == null){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput);
		}
		
		List<DataService> dataServices = schema.getDataServices();
		if(dataServices==null){
			return false;
		}
		Iterator<DataService> dataServiceIterator = dataServices.iterator();
		EDMX edmx;
		DataService dataService;
		while(dataServiceIterator.hasNext()){
			dataService = dataServiceIterator.next();
			edmx = (EDMX)dataService.eContainer();
			if(this.edmxSet.getMainEDMX() == edmx){
				return true;
			}
		}
		return false;
	}

	@Override
	public void addEDMX(EDMX edmx) throws ModelAPIException {
		//Check the input
		if(edmx == null || edmx.getDataService() == null){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput);
		}

		// Calculate transitive all EDMX and schemata
		List<EDMX> nextEdmx = new LinkedList<EDMX>();
		nextEdmx.add(edmx);
		List<EDMX> allEdmx = new LinkedList<EDMX>();
		List<Schema> allSchema = new LinkedList<Schema>();
		List<EDMX> blackList = new LinkedList<EDMX>();
		this.calculateEDMXTransitive(nextEdmx, blackList, allEdmx, allSchema);
		
		// Verify that new schemata don't contain external references
		OdataSchemataReferencer detector = new OdataSchemataReferencer();
		LinkedList<Schema> allSchemaTemp = new LinkedList<Schema>();
		allSchemaTemp.addAll(allSchema);
		allSchemaTemp.addAll(this.edmxSet.getSchemata());
		List<EObject> externalObjects = detector.execute(allSchemaTemp,null);
		
		if(externalObjects != null && externalObjects.size() != 0){
			throw new VocabularyException(Messages.VocabularyException_NotSelfContaining);
		}

		// Now, it seems OK to add the edmx and the schemata
		this.edmxSet.getFullScopeEDMX().addAll(allEdmx);
		this.edmxSet.getSchemata().addAll(allSchema);

		this.addEDMXReference(this.edmxSet.getMainEDMX(), edmx);
	}
	
	@Override
	public void removeEDMX(EDMX edmx) throws ModelAPIException {
		List<EObject> usingObjects;
		
		if(edmx == null || edmx.getDataService() == null){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput);
		}
		// Check, if EDMX is used in current EDMX set
		if(!this.edmxSet.getFullScopeEDMX().contains(edmx)){
			return;
		}
		
		// Check, if main edmx references the input edmx
		boolean isReferencedByMainEdmx = false;
		if(this.edmxSet.getMainEDMX() == null){
			throw new ModelAPIException(Messages.IllegalStateException_ArgumentsValueNotBound);	
		}
		Iterator<EDMXReference> mainEdmxRefIterator = this.edmxSet.getMainEDMX().getReferences().iterator();
		EDMXReference edmxRef;
		while(mainEdmxRefIterator.hasNext()){
			edmxRef = mainEdmxRefIterator.next();
			if(edmxRef.getReferencedEDMX() == edmx){
				isReferencedByMainEdmx = true;
				break;
			}
		}
		if(!isReferencedByMainEdmx){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput);
		}
		
		// Calculate all EDMX still used without edmx
		List<EDMX> nextEdmx = new LinkedList<EDMX>();
		nextEdmx.add(this.edmxSet.getMainEDMX());
		
		List<EDMX> edmxInScope = new LinkedList<EDMX>();
		List<Schema> schemataInScope = new LinkedList<Schema>();
		List<EDMX> blackList = new LinkedList<EDMX>();
		blackList.add(edmx);
		this.calculateEDMXTransitive(nextEdmx, blackList, edmxInScope, schemataInScope);
		
		// Therefore, the to be deleted EDMX and schemata 
		List<EDMX> toBeDeletedEdmx = new LinkedList<EDMX>();
		toBeDeletedEdmx.addAll(this.edmxSet.getFullScopeEDMX());
		toBeDeletedEdmx.removeAll(edmxInScope);
		List<Schema> toBeDeletedSchemata = new LinkedList<Schema>();
		toBeDeletedSchemata.addAll(this.edmxSet.getSchemata());
		toBeDeletedSchemata.removeAll(schemataInScope);
	
		// Check, if objects in other schemata - not belonging to the EDMX - have references
		OdataSchemataReferencer referencer = new OdataSchemataReferencer();
		usingObjects = referencer.execute(this.edmxSet.getSchemata(), toBeDeletedSchemata);
		
		// If used objects are only Using aliases from main schemata...remove them as well...	
		List<EObject> usingObjectsOfTypeUsing = new LinkedList<EObject>();
		Iterator<EObject> usingObjectIterator;
		Iterator<Schema> mainSchemaIterator = this.edmxSet.getMainEDMX().getDataService().getSchemata().iterator();
		Schema mainSchema;
		EObject usingObject;
		HashMap<Schema,List<Using>> toBeDeletedUsings = new HashMap<Schema,List<Using>>();
		Iterator<Using> usingIterator;
		while(mainSchemaIterator.hasNext()){
			mainSchema = mainSchemaIterator.next();
			usingObjectIterator = usingObjects.iterator();
			while(usingObjectIterator.hasNext()){
				usingObject = usingObjectIterator.next();
				usingIterator = mainSchema.getUsings().iterator();
				while (usingIterator.hasNext()) {
					Using using = usingIterator.next();
					if (using.getUsedNamespace() == usingObject) {
						usingObjectsOfTypeUsing.add(usingObject);
						List<Using> usings;
						if (toBeDeletedUsings.containsKey(mainSchema)) {
							usings = toBeDeletedUsings.get(mainSchema);
							usings.add(using);
						} else {
							usings = new LinkedList<Using>();
							usings.add(using);
							toBeDeletedUsings.put(mainSchema, usings);
						}
					}
				}
			}
		}
		usingObjects.removeAll(usingObjectsOfTypeUsing);
		
		if (usingObjects.size() == 0) {
			// No usage, remove all corresponding EDMX and schemata
			// Calculate or remove all EDMX References
			Iterator<Schema> toBeDeletedSchemataIterator = toBeDeletedSchemata
					.iterator();
			while (toBeDeletedSchemataIterator.hasNext()) {
				Schema toBeDeletedSchema = toBeDeletedSchemataIterator.next();
				Iterator<ValueAnnotation> toBeDeletedValueAnnotationIterator = toBeDeletedSchema.getValueAnnotations().iterator();
				while (toBeDeletedValueAnnotationIterator.hasNext()) {
					ValueAnnotation toBeDeletedAnnotation = toBeDeletedValueAnnotationIterator
							.next();
					toBeDeletedAnnotation.setTarget(null);
					toBeDeletedAnnotation.setTerm(null);
				}
			}
			this.removeEDMXReference(edmx);
			mainSchemaIterator = toBeDeletedUsings.keySet().iterator();
			while (mainSchemaIterator.hasNext()) {
				mainSchema = mainSchemaIterator.next();
				mainSchema.getUsings().removeAll(
						toBeDeletedUsings.get(mainSchema));
			}
			this.edmxSet.getFullScopeEDMX().removeAll(toBeDeletedEdmx);
			this.edmxSet.getSchemata().removeAll(toBeDeletedSchemata);
		} else {
			throw new ObjectInUseException(usingObjects);
		}
		
	}
	
	private void addEDMXReference(EDMX sourceEDMX, EDMX targetEDMX) throws ModelAPIException {
		List<EDMX> allEdmx = new LinkedList<EDMX>();
		allEdmx.add(this.edmxSet.getMainEDMX());
		allEdmx.addAll(this.edmxSet.getFullScopeEDMX());
		
		if(sourceEDMX == null || targetEDMX == null){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput);
		}
		// Verify, if source and target are known
		if(!allEdmx.contains(sourceEDMX) || !allEdmx.contains(targetEDMX)){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidSourceTargetForEDMXReference);
		}

		// Now, it seems OK to add a corresponding EDMX reference
		EDMXReference edmxReference = OdataFactory.eINSTANCE.createEDMXReference();
		edmxReference.setReferencedEDMX(targetEDMX);

		sourceEDMX.getReferences().add(edmxReference);
	}

	private void removeEDMXReference(EDMX targetEDMX) throws ModelAPIException {
		EDMXReference edmxReference = null;
		List<EDMXReference> toBeRemoved = new LinkedList<EDMXReference>();
		
		// Validate the input
		if(targetEDMX == null){
			throw new ModelAPIException(Messages.ModelAPIException_InvalidInput);
		}
		
		Iterator<EDMXReference> edmxReferences = this.edmxSet.getMainEDMX()
				.getReferences().iterator();
		while (edmxReferences.hasNext()) {
			edmxReference = edmxReferences.next();
			if (edmxReference.getReferencedEDMX().equals(targetEDMX)) {
				toBeRemoved.add(edmxReference);
			}
		}
		this.edmxSet.getMainEDMX().getReferences().removeAll(toBeRemoved);

		Iterator<EDMX> edmxIterator = this.edmxSet.getFullScopeEDMX()
				.iterator();
		EDMX edmx;
		while (edmxIterator.hasNext()) {
			toBeRemoved = new LinkedList<EDMXReference>();
			edmx = edmxIterator.next();
			edmxReferences = edmx.getReferences().iterator();
			while (edmxReferences.hasNext()) {
				edmxReference = edmxReferences.next();
				if (edmxReference.getReferencedEDMX().equals(targetEDMX)) {
					toBeRemoved.add(edmxReference);
				}
			}
			edmx.getReferences().removeAll(toBeRemoved);
		}
		
	}
	
	private void calculateEDMXTransitive(List<EDMX> nextEdmx, List<EDMX> blackList, List<EDMX> edmx, List<Schema> schema) throws ModelAPIException{
		EDMX tempEdmx;
		EDMXReference tempEdmxReference;
		Schema tempSchema;
		Schema registeredSchema;
		String namespace;
		Iterator<Schema> schemaIterator;
		Iterator<EDMXReference> edmxReferenceIterator;
		List<EDMXReference> edmxReferences;
		List<EDMX> newNextEdmx = new LinkedList<EDMX>();
		
		if(nextEdmx == null || nextEdmx.size() == 0){
			return;
		}
		
		Iterator<EDMX> edmxIterator = nextEdmx.iterator();
		while(edmxIterator.hasNext()){
			tempEdmx = edmxIterator.next();
			
			// Verify schemata input in more detail ...
			schemaIterator = tempEdmx.getDataService().getSchemata().iterator();
			while(schemaIterator.hasNext()){
				tempSchema = schemaIterator.next();
				// Verify that the schema is not already registered and if yes, then it is the same instance
				namespace = tempSchema.getNamespace();		
				try{
					registeredSchema = this.addRegisteredSchema(namespace);
					if(registeredSchema!=tempSchema){
						throw new VocabularyException(Messages.bind(Messages.VocabularyException_DuplicateNamespace, tempSchema.getNamespace()));
					}else{
						// Collect schema
						if(!schema.contains(tempSchema)){
							schema.add(tempSchema);
						}
					}
				}catch(ModelAPIException e){
					// Collect schema
					if(!schema.contains(tempSchema)){
						schema.add(tempSchema);
					}
				}
			}
			
			// Collect current edmx
			if(!edmx.contains(tempEdmx)){
				edmx.add(tempEdmx);
			}
			
			// Collect next edmx...
			edmxReferences = tempEdmx.getReferences();
			edmxReferenceIterator = edmxReferences.iterator();
			while(edmxReferenceIterator.hasNext()){
				tempEdmxReference = edmxReferenceIterator.next();
				if(!newNextEdmx.contains(tempEdmxReference.getReferencedEDMX()) && !edmx.contains(tempEdmxReference.getReferencedEDMX()) && !blackList.contains(tempEdmxReference.getReferencedEDMX())){
					newNextEdmx.add(tempEdmxReference.getReferencedEDMX());
				}
			}
			

		}
		
		this.calculateEDMXTransitive(newNextEdmx, blackList, edmx, schema);
	}

	private Schema addRegisteredSchema(String namespace) throws ModelAPIException {
		Schema schema = findSchema(namespace);
		if(schema==null) {
			// create missing schema instance
			ISchemaProvider provider = this.parent.registry.getSchemaProvider(namespace);
			schema = provider.createSchema(this);

			// create or re-use existing EDMX node with same URI
			String uri = this.parent.registry.getSchemaURI(namespace);
			EDMX edmx = findEDMX(uri);
			EDMXReference reference = null;
			if(edmx==null) {
				// create EDMX
				DataService service = OdataFactory.eINSTANCE.createDataService();
				service.setVersion(ModelContextImpl.ODATA_VERSION);
				service.getSchemata().add(schema);
				edmx = OdataFactory.eINSTANCE.createEDMX();
				edmx.setDataService(service);
				edmx.setURI(uri);
			}else{
				// found EDMX, now check reference from main EDMX
				for( EDMXReference ref : this.edmxSet.getMainEDMX().getReferences()){
					if(ref.getReferencedEDMX()==edmx){
						reference = ref;
					}
				}
			}

			// if reference from main EDMX is missing, create it
			if(reference==null){
				reference = OdataFactory.eINSTANCE.createEDMXReference();
				reference.setReferencedEDMX(edmx);
			}

			try {
				TransactionalEditingDomain domain = this.parent.getTransaction(this.edmxSet);
				CompoundCommand command = new CompoundCommand();
				command.append(new AddCommand(domain, this.edmxSet.getSchemata(), schema));
				if(edmx.eContainer()==null){
					command.append(new AddCommand(domain, this.edmxSet.getFullScopeEDMX(), edmx));
				}
				if(reference.eContainer()==null){
					command.append(new AddCommand(domain, this.edmxSet.getMainEDMX().getReferences(), reference));
				}
				domain.getCommandStack().execute(command.unwrap());
			}catch (TransactionException error) {
				this.edmxSet.getSchemata().add(schema);
				this.edmxSet.getFullScopeEDMX().add(edmx);
				this.edmxSet.getMainEDMX().getReferences().add(reference);
			}
		}
		return schema;
	}

	private Schema findSchema(String namespace) {
		if(namespace!=null){
			for(Schema schema : this.edmxSet.getSchemata()){
				if(namespace.equals(schema.getNamespace())){
					return schema;
				}
			}
		}
		return null;
	}

	private EDMX findEDMX(String uri) {
		if(uri!=null){
			for(EDMX edmx : this.edmxSet.getFullScopeEDMX()){
				if(uri.equals(edmx.getURI())){
					return edmx;
				}
			}
		}
		return null;
	}

	private IVocabulary createVocabularyAdapter(Schema schema) throws ModelAPIException {
		Adapter adapter = this.parent.adapt(schema, ISchemaAdapter.class);
		if(adapter==null) {
			throw new VocabularyException(Messages.bind(Messages.VocabularyException_InvalidNamespace, schema.getNamespace()));
		}else if(adapter instanceof IVocabulary){
			return (IVocabulary)adapter;
		}else{
			throw new VocabularyException(Messages.bind(Messages.VocabularyException_InvalidNamespace, schema.getNamespace()));
		}
	}

}
