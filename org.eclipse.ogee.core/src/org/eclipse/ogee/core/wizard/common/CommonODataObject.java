/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.wizard.common;

import org.eclipse.swt.graphics.Image;

/**
 * 
 * This class is used to populate the values in the new reusable
 * ODataModelProjectPage
 */
public class CommonODataObject
{
	private String displayName;
	private Image icon;
	private String description;
	private String id;


	public CommonODataObject(String displayName, Image icon, String description, String id)
	{
		this.id = id;
		this.displayName = displayName;
		this.description = description;
		this.icon = icon;

	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public Image getIcon()
	{
		return icon;
	}

	public void setIcon(Image icon)
	{
		this.icon = icon;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

}
