/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.wizard.common;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbench;

/**
 * 
 * The structure information which needs to be passed into common data object.
 */
public class CommonWizardStructureData
{
	// generic
	private String pageTitle = null;
	private String pageDescription = null;
	private IStructuredSelection selection = null;
	private String helpURL = null;
	private boolean isFinishEnabled = false;
	private boolean isSMPWizard = false;


	private boolean isNextEnabled = false;
	private IWorkbench workbench = null;

	// serviceImplPage
	private boolean servicePageShowServiceDetailsVersion = false;
	private boolean servicePageShowServiceDetailsServiceName = false;
	private String servicePageServiceType = null;
	private String servicePageServiceName = null;
	private boolean servicePageIsBrowseButtonEnabled = false;
	private String servicePageProjectPath = null;
	private boolean servicePagePathRequired = false;
	private List<CommonTargetRuntime> targetRuntimes = null;

	// odataModelPage
	private boolean odataPageIsBlankRequired = false;
	private boolean odataPageIsFromURLRequired = false;
	private boolean odataPageIsFromFileRequired = false;
	private boolean odataPageIsFromODataRequired = false;
	private boolean isBrowseButtonRequired = true;
	private String odataProjectPathFromSMP = null;

	// HCI for future use
	private boolean isHCI = false;

	public CommonWizardStructureData()
	{
		// empty
	}

	public String getPageTitle()
	{
		return pageTitle;
	}


	public void setPageTitle(String pageTitle)
	{
		this.pageTitle = pageTitle;
	}


	public String getPageDescription()
	{
		return pageDescription;
	}


	public void setPageDescription(String pageDescription)
	{
		this.pageDescription = pageDescription;
	}


	public String getServiceType()
	{
		return servicePageServiceType;
	}


	public void setServiceType(String serviceType)
	{
		this.servicePageServiceType = serviceType;
	}


	public String getServiceName()
	{
		return servicePageServiceName;
	}


	public void setServiceName(String serviceName)
	{
		this.servicePageServiceName = serviceName;
	}


	public String getHelpURL()
	{
		return helpURL;
	}


	public void setHelpURL(String helpURL)
	{
		this.helpURL = helpURL;
	}


	public boolean isFinishEnabled()
	{
		return isFinishEnabled;
	}


	public void setFinishEnabled(boolean isFinishEnabled)
	{
		this.isFinishEnabled = isFinishEnabled;
	}


	public boolean isBrowseButtonEnabled()
	{
		return servicePageIsBrowseButtonEnabled;
	}


	public void setBrowseButtonEnabled(boolean isBrowseButtonEnabled)
	{
		this.servicePageIsBrowseButtonEnabled = isBrowseButtonEnabled;
	}


	public String getProjectPath()
	{
		return servicePageProjectPath;
	}


	public void setProjectPath(String projectPath)
	{
		this.servicePageProjectPath = projectPath;
	}


	public IStructuredSelection getSelection()
	{
		return selection;
	}


	public void setSelection(IStructuredSelection selection)
	{
		this.selection = selection;
	}


	public boolean isNextEnabled()
	{
		return isNextEnabled;
	}


	public void setNextEnabled(boolean isNextEnabled)
	{
		this.isNextEnabled = isNextEnabled;
	}


	public boolean isHCI()
	{
		return isHCI;
	}


	public void setHCI(boolean isHCI)
	{
		this.isHCI = isHCI;
	}


	public boolean isOdataPageIsBlankRequired()
	{
		return odataPageIsBlankRequired;
	}


	public void setOdataPageIsBlankRequired(boolean odataPageIsBlankRequired)
	{
		this.odataPageIsBlankRequired = odataPageIsBlankRequired;
	}


	public boolean isOdataPageIsFromURLRequired()
	{
		return odataPageIsFromURLRequired;
	}


	public void setOdataPageIsFromURLRequired(boolean odataPageIsFromURLRequired)
	{
		this.odataPageIsFromURLRequired = odataPageIsFromURLRequired;
	}


	public boolean isOdataPageIsFromFileRequired()
	{
		return odataPageIsFromFileRequired;
	}


	public void setOdataPageIsFromFileRequired(boolean odataPageIsFromFileRequired)
	{
		this.odataPageIsFromFileRequired = odataPageIsFromFileRequired;
	}

	public boolean isServicePageShowServiceDetailsServiceName()
	{
		return servicePageShowServiceDetailsServiceName;
	}

	public void setServicePageShowServiceDetailsServiceName(boolean servicePageShowServiceDetailsServiceName)
	{
		this.servicePageShowServiceDetailsServiceName = servicePageShowServiceDetailsServiceName;
	}

	public boolean isServicePageShowServiceDetailsVersion()
	{
		return servicePageShowServiceDetailsVersion;
	}

	public void setServicePageShowServiceDetailsVersion(boolean servicePageShowServiceDetailsVersion)
	{
		this.servicePageShowServiceDetailsVersion = servicePageShowServiceDetailsVersion;
	}

	public List<CommonTargetRuntime> getTargetRuntimes()
	{
		return targetRuntimes;
	}

	public void setTargetRuntimes(List<CommonTargetRuntime> targetRuntimes)
	{
		this.targetRuntimes = targetRuntimes;
	}

	public boolean isBrowseButtonRequired()
	{
		return isBrowseButtonRequired;
	}

	public void setBrowseButtonRequired(boolean isBrowseButtonRequired)
	{
		this.isBrowseButtonRequired = isBrowseButtonRequired;
	}

	public String getOdataProjectPathFromSMP()
	{
		return odataProjectPathFromSMP;
	}

	public void setOdataProjectPathFromSMP(String odataProjectPathFromSMP)
	{
		this.odataProjectPathFromSMP = odataProjectPathFromSMP;
	}

	public boolean isServicePagePathRequired()
	{
		return servicePagePathRequired;
	}

	public void setServicePagePathRequired(boolean servicePageIsPathRequired)
	{
		this.servicePagePathRequired = servicePageIsPathRequired;
	}

	public boolean isOdataPageFromExistingODataRequired()
	{
		return odataPageIsFromODataRequired;
	}

	public void setOdataPageIsFromODataRequired(boolean odataPageIsFromODataRequired)
	{
		this.odataPageIsFromODataRequired = odataPageIsFromODataRequired;
	}

	public IWorkbench getWorkbench()
	{
		return workbench;
	}

	public void setWorkbench(IWorkbench workbench)
	{
		this.workbench = workbench;
	}

	public boolean isSMPWizard()
	{
		return isSMPWizard;
	}

	public void setSMPWizard(boolean isSMPWizard)
	{
		this.isSMPWizard = isSMPWizard;
	}
}
