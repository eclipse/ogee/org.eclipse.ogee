/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.wizard.common;

/**
 * 
 * The Common object which has been used to pass data between the reusable
 * wizard pages and wizard.
 */
public class CommonWizardObject
{
	private CommonWizardStructureData structureData;
	private CommonWizardBusinessData businessData;

	public CommonWizardStructureData getStructureData()
	{
		return structureData;
	}

	public CommonWizardBusinessData getBusinessData()
	{
		return businessData;
	}

	public CommonWizardObject(CommonWizardBusinessData businessData, CommonWizardStructureData structureData)
	{
		this.businessData = businessData;
		this.structureData = structureData;
	}

}
