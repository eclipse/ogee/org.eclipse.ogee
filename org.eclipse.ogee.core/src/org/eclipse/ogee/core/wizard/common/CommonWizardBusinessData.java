/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.wizard.common;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ogee.model.odata.EDMXSet;

/**
 * 
 * The business data which needs to be passed into common data object.
 */
public class CommonWizardBusinessData
{
	// servicePage
	private String projectName = null;
	private IPath odataFilePath = null;
	private String projectFilePath = null;
	private String smpVersionID = null;

	// odataPage
	private String odataPageFolderPath = null;
	private String odataPageServiceName = null;
	private CommonODataObject odataPageCommonODataObject = null;
	private IWizardPage serviceCatalogWizardPage = null;
	private EDMXSet odataPageEdmxSet = null;
	private static EDMXSet odataPageEdmxFromServiceCatalog = null;
	

	public CommonWizardBusinessData()
	{
		// empty
	}

	public static EDMXSet getEdmxfromservicecatalog()
	{
		return odataPageEdmxFromServiceCatalog;
	}

	public static void setEdmxfromservicecatalog(EDMXSet edmxfromservicecatalog)
	{
		CommonWizardBusinessData.odataPageEdmxFromServiceCatalog = edmxfromservicecatalog;
	}


	public IPath getOdataFilePath()
	{
		return odataFilePath;
	}

	public void setOdataFilePath(IPath odataFilePath)
	{
		this.odataFilePath = odataFilePath;
	}

	public String getProjectName()
	{
		return projectName;
	}

	public void setProjectName(String projectName)
	{
		this.projectName = projectName;
	}

	public String getProjectFilePath()
	{
		return projectFilePath;
	}

	public void setProjectFilePath(String projectFilePath)
	{
		this.projectFilePath = projectFilePath;
	}

	public String getOdataFolderPath()
	{
		return odataPageFolderPath;
	}

	public void setOdataFolderPath(String odataFolderPath)
	{
		this.odataPageFolderPath = odataFolderPath;
	}

	public String getOdataServiceName()
	{
		return odataPageServiceName;
	}

	public void setOdataServiceName(String odataServiceName)
	{
		this.odataPageServiceName = odataServiceName;
	}

	public CommonODataObject getCommonODataObject()
	{
		return odataPageCommonODataObject;
	}

	public void setCommonODataObject(CommonODataObject commonODataObject)
	{
		this.odataPageCommonODataObject = commonODataObject;
	}

	public EDMXSet getEdmxSet()
	{
		return odataPageEdmxSet;
	}

	public void setEdmxSet(EDMXSet edmxSet)
	{
		this.odataPageEdmxSet = edmxSet;
		odataPageEdmxFromServiceCatalog = edmxSet;
	}

	public String getVersionID()
	{
		return smpVersionID;
	}

	public void setVersionID(String versionID)
	{
		this.smpVersionID = versionID;
	}

	public IWizardPage getServiceCatalogWizardPage()
	{
		return serviceCatalogWizardPage;
	}

	public void setServiceCatalogWizardPage(IWizardPage serviceCatalogWizardPage)
	{
		this.serviceCatalogWizardPage = serviceCatalogWizardPage;
	}


}
