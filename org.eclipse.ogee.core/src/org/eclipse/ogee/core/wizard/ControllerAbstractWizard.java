/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.wizard;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.core.wizard.controller.WizardController;
import org.eclipse.ogee.core.wizard.controller.WizardControllerException;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ui.IWorkbench;

public abstract class ControllerAbstractWizard extends FrwkAbstractWizard
{
	/**
	 * wizard controller
	 */
	protected WizardController controller;

	// indicates if wizard should be closed
	private boolean closeWizard;

	/**
	 * selected forlder or file
	 */
	protected IStructuredSelection folderSelection;


	/**
	 * Constructs a new ControllerAbstractWizard.
	 */
	public ControllerAbstractWizard(String title)
	{
		super();

		setWindowTitle(title);
		setNeedsProgressMonitor(true);
	}

	/**
	 * wizard init method
	 * 
	 * @param workbench
	 * @param selection
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
		this.folderSelection = selection;
	}


	@Override
	public boolean performFinish()
	{
		try
		{
			this.closeWizard = true;

			getContainer().run(false, true, new IRunnableWithProgress()
			{
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException
				{
					try
					{
						controller.doFinish(monitor);
					}
					catch (WizardControllerException e)
					{
						showError(e);
						closeWizard = false;
					}
				}
			});
		}
		catch (InvocationTargetException e)
		{
			showError(e);
			closeWizard = false;
		}
		catch (InterruptedException e)
		{
			showError(e);
			closeWizard = false;
		}

		return this.closeWizard;
	}

	/**
	 * show error dialog and prints to the log
	 * 
	 * @param e
	 */
	protected void showError(Throwable e)
	{
		Logger.getFrameworkLogger().logError(FrameworkMessages.AbstractWizard_1, e);
		WizardPage currentPage = (WizardPage) getContainer().getCurrentPage();
		currentPage.setMessage(FrameworkMessages.AbstractWizard_3, IMessageProvider.ERROR);
	}
}
