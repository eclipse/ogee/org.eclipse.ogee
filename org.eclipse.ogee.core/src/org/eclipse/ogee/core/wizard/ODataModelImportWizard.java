/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.wizard;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.core.wizard.common.CommonWizardBusinessData;
import org.eclipse.ogee.core.wizard.common.CommonWizardObject;
import org.eclipse.ogee.core.wizard.common.CommonWizardStructureData;
import org.eclipse.ogee.core.wizard.pages.ODataModelProjectPage;
import org.eclipse.ogee.core.wizard.pages.ServiceFromFileWizardPage;
import org.eclipse.ogee.core.wizard.pages.ServiceFromUrlWizardPage;
import org.eclipse.ogee.core.wizard.pages.WizardPagesConstants;
import org.eclipse.ogee.designer.api.IODataDiagramCreator;
import org.eclipse.ogee.designer.api.IODataDiagramInput;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.vocabularies.ICapabilityVocabulary;
import org.eclipse.ogee.model.api.vocabularies.IMeasuresVocabulary;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

public class ODataModelImportWizard extends Wizard implements INewWizard
{
	public CommonWizardBusinessData businessData;
	private CommonWizardStructureData structureData;
	public ODataModelProjectPage oDataModelProjectPage;
	public ServiceFromFileWizardPage serviceFromFileWizardPage;
	public ServiceFromUrlWizardPage serviceFromUrlWizardPage;
	private IStructuredSelection folderSelection;
	private IConfigurationElement[] odataModelConfigElements;

	public ODataModelImportWizard()
	{
		super();
		setWindowTitle(FrameworkMessages.ODataServiceWizardTitle);
		setNeedsProgressMonitor(true);
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
		this.folderSelection = selection;
	}

	@Override
	public void addPages()
	{
		businessData = new CommonWizardBusinessData();
		structureData = new CommonWizardStructureData();
		structureData.setFinishEnabled(true);
		structureData.setPageDescription(FrameworkMessages.ImportODataServicePageDescription);
		structureData.setPageTitle(FrameworkMessages.ImportODataServicePageName);
		structureData.setSelection(folderSelection);
		structureData.setOdataPageIsBlankRequired(false);
		structureData.setOdataPageIsFromFileRequired(true);
		structureData.setOdataPageIsFromURLRequired(true);
		structureData.setBrowseButtonEnabled(true);
		CommonWizardObject wizardObject = new CommonWizardObject(businessData, structureData);
		try
		{
			oDataModelProjectPage = new ODataModelProjectPage(wizardObject);
			addPage(oDataModelProjectPage);

			serviceFromFileWizardPage = new ServiceFromFileWizardPage(wizardObject);
			addPage(serviceFromFileWizardPage);

			serviceFromUrlWizardPage = new ServiceFromUrlWizardPage(wizardObject);
			addPage(serviceFromUrlWizardPage);
			if (null != businessData && null != businessData.getServiceCatalogWizardPage())
			{
				addPage(businessData.getServiceCatalogWizardPage());
			}

		}
		catch (Exception e)
		{
			Logger.getFrameworkLogger().logError(e);
		}

	}


	@Override
	public boolean canFinish()
	{
		IWizardPage page = getContainer().getCurrentPage();
		if (!page.isPageComplete())
		{
			return false;
		}
		return true;
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page)
	{

		if (null != businessData.getCommonODataObject())
		{

			if (businessData.getCommonODataObject().getDisplayName()
					.equalsIgnoreCase(WizardPagesConstants.ODATA_METADATA_FILE))
			{
				return serviceFromFileWizardPage;
			}
			else if (businessData.getCommonODataObject().getDisplayName()
					.equalsIgnoreCase(WizardPagesConstants.ODATA_METADATA_URL))
			{
				return serviceFromUrlWizardPage;
			}
			else if (businessData.getCommonODataObject().getDisplayName()
					.equalsIgnoreCase(WizardPagesConstants.SERVICE_CATALOG)
					&& null != businessData && null != businessData.getServiceCatalogWizardPage())
			{
				return businessData.getServiceCatalogWizardPage();
			}
		}
		return null;
	}


	@Override
	public boolean performFinish()
	{
		try
		{
			this.getContainer().run(false, true, new IRunnableWithProgress()
			{
				@Override
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException
				{
					EDMXSet edmxSet = null;
					try
					{


						if (businessData.getCommonODataObject().getDisplayName()
								.equalsIgnoreCase(WizardPagesConstants.ODATA_METADATA_FILE)
								|| businessData.getCommonODataObject().getDisplayName()
										.equalsIgnoreCase(WizardPagesConstants.ODATA_METADATA_URL))
						{
							edmxSet = businessData.getEdmxSet();
						}
						else if (businessData.getCommonODataObject().getDisplayName()
								.equalsIgnoreCase(WizardPagesConstants.SERVICE_CATALOG))
						{
							edmxSet = businessData.getEdmxfromservicecatalog();
						}
						else
						{
							edmxSet = createModel();
						}


					}
					catch (BuilderException e1)
					{
						Logger.getFrameworkLogger().logError(e1);
					}
					if (edmxSet == null)
					{
						String extensionDisplayName = businessData.getOdataServiceName();
						String message = FrameworkMessages.ODataModelController_1 + extensionDisplayName
								+ FrameworkMessages.ODataModelController_2;
						throw new IllegalArgumentException(message);
					}

					// set namespace for the schemas if empty
					EList<Schema> schemas = edmxSet.getSchemata();
					if (schemas != null)
					{
						for (Schema schema : schemas)
						{
							String namespace = schema.getNamespace();
							if (namespace == null || namespace.isEmpty())
							{
								schema.setNamespace(businessData.getOdataServiceName());
							}
						}
					}

					// get workspace
					IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
					// get diagram file
					IFile diagramFile = root.getFile(new Path(businessData.getOdataFolderPath() + File.separator
							+ businessData.getOdataServiceName() + ".odata")); //$NON-NLS-1$
					if (diagramFile != null)
					{
						IODataDiagramCreator diagramCreator;
						diagramCreator = IODataDiagramCreator.INSTANCE;
						final IODataDiagramInput diagramInput = diagramCreator.createDiagramInput();

						diagramInput.setDiagramFile(diagramFile);
						diagramInput.setEDMXSet(edmxSet);
						diagramInput.setOpenEditor(true);
						diagramInput.setProgressMonitor(monitor);

						try
						{
							diagramCreator.createDiagram(diagramInput);
						}
						catch (CoreException e)
						{
							Logger.getFrameworkLogger().logError(e);
						}
					}
				}

			});
		}
		catch (InterruptedException e)
		{
			return false;
		}
		catch (InvocationTargetException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;


	}

	/**
	 * creates the model if it is blank model
	 * 
	 * @return
	 * @throws BuilderException
	 */
	private EDMXSet createModel() throws BuilderException
	{
		try
		{
			EDMXSet edmxSet = IModelContext.INSTANCE.createServiceModel(""); //$NON-NLS-1$

			// Implicitly create the vocabulary
			IModelContext.INSTANCE.getVocabularyContext(edmxSet).provideVocabulary(ICapabilityVocabulary.VC_NAMESPACE);
			IModelContext.INSTANCE.getVocabularyContext(edmxSet).provideVocabulary(IMeasuresVocabulary.VC_NAMESPACE);

			return edmxSet;
		}
		catch (ModelAPIException e)
		{
			throw new BuilderException(e);
		}
	}


	@Override
	public void dispose()
	{
		super.dispose();

		if (null != oDataModelProjectPage)
		{
			oDataModelProjectPage = null;
		}
		if (null != serviceFromFileWizardPage)
		{
			serviceFromFileWizardPage = null;
		}
		if (null != serviceFromUrlWizardPage)
		{
			serviceFromUrlWizardPage = null;
		}
		if (null != odataModelConfigElements)
		{
			odataModelConfigElements = null;
		}
		if (null != businessData)
		{
			businessData = null;
		}
		if (null != structureData)
		{
			structureData = null;
		}
	}

}
