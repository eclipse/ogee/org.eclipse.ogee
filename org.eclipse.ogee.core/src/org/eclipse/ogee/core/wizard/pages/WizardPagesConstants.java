package org.eclipse.ogee.core.wizard.pages;


public class WizardPagesConstants
{
	public static final String PAGE_NAME = "ODataModelProjectPage"; //$NON-NLS-1$
	public static final String ODATA_MODEL = ".serviceModel"; //$NON-NLS-1$
	public static final String WIZARD_PAGE_BANNER_ICON = "res/images/WIZARD_BANNER_ICON.gif"; //$NON-NLS-1$
	public static String ODATA_V2 = "odatav2";//$NON-NLS-1$
	public static String DOT_ODATA = ".odata";//$NON-NLS-1$
	public static String ODATA_METADATA_URL = "OData Service URL";//$NON-NLS-1$
	public static String ODATA_METADATA_FILE = "OData Metadata File";//$NON-NLS-1$
	public static String SERVICE_CATALOG = "Service Catalog";//$NON-NLS-1$
	public static String EXISTING_ODATA_MODEL = "Existing OData Model";//$NON-NLS-1$
	public static String EXISTING_ODATA_IMAGE = "res/images/existing_odata.png";//$NON-NLS-1$
	public static String BLANK_IMAGE = "res/images/blank.png";//$NON-NLS-1$
	public static String URL_IMAGE = "res/images/service url.png";//$NON-NLS-1$
	public static String FILE_IMAGE = "res/images/service file.png";//$NON-NLS-1$
	public static String BLANKID = "org.eclipse.ogee.wizard.blank";//$NON-NLS-1$
	public static String URLID = "org.eclipse.ogee.wizard.urldisplayName";//$NON-NLS-1$
	public static String FILEID = "org.eclipse.ogee.wizard.filedisplayName";//$NON-NLS-1$
	public static String EXISTINGODATAID = "org.eclipse.ogee.wizard.existingOdatadisplayName";//$NON-NLS-1$

}
