package org.eclipse.ogee.core.wizard.pages;

import java.io.File;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ogee.core.Activator;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.odata.model.internal.ODataModelExtension;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.core.wizard.common.CommonODataObject;
import org.eclipse.ogee.core.wizard.common.CommonWizardBusinessData;
import org.eclipse.ogee.core.wizard.common.CommonWizardObject;
import org.eclipse.ogee.core.wizard.common.CommonWizardStructureData;
import org.eclipse.ogee.core.wizard.controller.WizardControllerException;
import org.eclipse.ogee.help.IHelpConstants;
import org.eclipse.ogee.utils.ProjectUtils;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

/**
 * Represents the page for the OData Service wizard.
 */
public class ODataModelProjectPage extends WizardPage
{
	private IConfigurationElement[] odataModelConfigElements;
	private CommonODataObject selObject;
	private boolean isPageValidated = false;

	private Composite inputGroup;
	private Text folderText;
	private Button folderBrowseButton;
	private Label serviceNameLabel;
	private Text modelNameText;
	private CommonODataObject commonODataObject;
	private Set<CommonODataObject> commonData = new LinkedHashSet<CommonODataObject>();
	private IStructuredSelection folderSelection;
	private TableViewer tableViewer;
	private IWizardPage serviceCatalogWizardPage;


	public static final ImageDescriptor imageDescriptor;
	private Label tableLabel;
	private Composite container;
	private Label folderLabel;
	private Label separatorLabel;

	private ODataModelProjectPage me;
	private StyledText optionDescription;
	private CommonWizardBusinessData businessData = null;
	private CommonWizardStructureData structureData = null;
	private boolean firstTime;
	private static final String nonSpclCharacter = "[^a-zA-Z_0-9]"; //$NON-NLS-1$
	private static final String onlyCharacter = "[^a-z_]"; //$NON-NLS-1$

	private static final String ORG_ECLIPSE_OGEE_NEW_ODATA_MODEL = "org.eclipse.ogee.13e6d8bc3e304ac9901b069c7bb39a94"; //$NON-NLS-1$
	private static final String ORG_ECLIPSE_UI_HELP = "org.eclipse.ui.help"; //$NON-NLS-1$


	static
	{
		URL imageUrl = Activator.getDefault().getBundle().getEntry(WizardPagesConstants.WIZARD_PAGE_BANNER_ICON);
		imageDescriptor = ImageDescriptor.createFromURL(imageUrl);
	}

	/**
	 * Constructs a new page for the OData Service wizard.
	 * 
	 * @param extensions
	 * @throws WizardControllerException
	 * @throws org.eclipse.ogee.core.extensions.managers.ExtensionsManagerException
	 */
	public ODataModelProjectPage(CommonWizardObject wizardObject) throws WizardControllerException
	{
		super(WizardPagesConstants.PAGE_NAME);
		this.businessData = wizardObject.getBusinessData();
		this.structureData = wizardObject.getStructureData();
		this.firstTime = true;
		this.folderSelection = this.structureData.getSelection();
		try
		{
			loadModelExtensions();
		}
		catch (Exception e)
		{
			Logger.getFrameworkLogger().logError(e);
		}


		if (null != this.structureData.getPageTitle() && !this.structureData.isSMPWizard())
		{
			setTitle(this.structureData.getPageTitle());
		}
		else
		{
			setTitle(FrameworkMessages.NewODataServicePageName);
		}
		if (null != this.structureData.getPageDescription() && !this.structureData.isSMPWizard())
		{
			setDescription(this.structureData.getPageDescription());
		}
		else
		{
			setDescription(FrameworkMessages.NewODataServicePageDescription);
		}
		createCommonOdata();
		setPageComplete(false);
		setControl(this.container);


	}

	/**
	 * Retrieves the odata model extensions from the plugin.xml
	 * 
	 * @throws Exception
	 */
	private void loadModelExtensions() throws Exception
	{
		this.odataModelConfigElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
				Activator.PLUGIN_ID + WizardPagesConstants.ODATA_MODEL);

		for (IConfigurationElement odataModelConfigElement : this.odataModelConfigElements)
		{
			if (odataModelConfigElement == null)
			{
				throw new Exception(FrameworkMessages.OdataModelElementNull);
			}

			try
			{
				ODataModelExtension odataModelExtension = new ODataModelExtension(odataModelConfigElement);
				if (odataModelExtension.getDisplayName().equalsIgnoreCase(WizardPagesConstants.SERVICE_CATALOG))
				{
					List<IWizardPage> wizardPages = odataModelExtension.getODataModelProvider().getWizardPages();
					for (IWizardPage wizardPage : wizardPages)
					{
						serviceCatalogWizardPage = wizardPage;
						this.businessData.setServiceCatalogWizardPage(serviceCatalogWizardPage);
					}
				}
			}
			catch (ExtensionException e)
			{
				String id = odataModelConfigElement.getAttribute(ExtensionAttributes.id.name());
				Logger.getFrameworkLogger()
						.logWarning(
								e.getMessage()
										+ NLS.bind(FrameworkMessages.ODataModelExecutableExtensionIDCreationFailure, id));
			}
		}

	}


	/*
	 * Add the model group
	 */
	private void createCommonOdata()
	{

		// Blank OData Model
		if (this.structureData.isOdataPageIsBlankRequired())
		{
			Image image = createImage(Activator.getDefault().getBundle().getEntry(WizardPagesConstants.BLANK_IMAGE));
			commonODataObject = new CommonODataObject(FrameworkMessages.ODataModelProjectPage_BlankModel, image,
					FrameworkMessages.ODataModelProjectPage_BlankModelDescription, WizardPagesConstants.BLANKID);
			this.commonData.add(commonODataObject);
		}

		// OData Metadata File
		if (this.structureData.isOdataPageIsFromFileRequired())
		{
			Image image = createImage(Activator.getDefault().getBundle().getEntry(WizardPagesConstants.FILE_IMAGE));
			commonODataObject = new CommonODataObject(FrameworkMessages.ODataModelProjectPage_FileModel, image,
					FrameworkMessages.ODataModelProjectPage_FileModelDescription, WizardPagesConstants.FILEID);
			this.commonData.add(commonODataObject);
		}

		// OData Service URL
		if (this.structureData.isOdataPageIsFromURLRequired())
		{
			Image image = createImage(Activator.getDefault().getBundle().getEntry(WizardPagesConstants.URL_IMAGE));
			commonODataObject = new CommonODataObject(FrameworkMessages.ODataModelProjectPage_UrlModel, image,
					FrameworkMessages.ODataModelProjectPage_UrlModelDescription, WizardPagesConstants.URLID);
			this.commonData.add(commonODataObject);
		}


		loadExtensionToCommonObject();

		// Existing OData File
		if (this.structureData.isOdataPageFromExistingODataRequired())
		{
			Image image = createImage(Activator.getDefault().getBundle()
					.getEntry(WizardPagesConstants.EXISTING_ODATA_IMAGE));
			commonODataObject = new CommonODataObject(FrameworkMessages.ODataModelProjectPage_ExistingODataModel,
					image, FrameworkMessages.ODataModelProjectPage_ExistingODataModelDescription,
					WizardPagesConstants.FILEID);
			this.commonData.add(commonODataObject);
		}


	}

	private void loadExtensionToCommonObject()
	{
		// Retrieves the odata model extensions from the plugin.xml
		this.odataModelConfigElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
				Activator.PLUGIN_ID + WizardPagesConstants.ODATA_MODEL);

		for (IConfigurationElement odataModelConfigElement : this.odataModelConfigElements)
		{
			if (odataModelConfigElement == null)
			{
				try
				{
					throw new Exception(FrameworkMessages.OdataModelElementNull);
				}
				catch (Exception e)
				{
					Logger.getLogger(ODataModelProjectPage.class).logError(e);
				}
			}

			try
			{
				ODataModelExtension odataModelExtension = new ODataModelExtension(odataModelConfigElement);
				if (null != odataModelExtension)
				{

					Image image = createImage(odataModelExtension.getIcon());
					// Service Catalog
					commonODataObject = new CommonODataObject(odataModelExtension.getDisplayName(), image,
							odataModelExtension.getDescription(), odataModelExtension.getId());
					this.commonData.add(commonODataObject);
				}

			}
			catch (ExtensionException e)
			{
				String id = odataModelConfigElement.getAttribute(ExtensionAttributes.id.name());
				Logger.getFrameworkLogger()
						.logWarning(
								e.getMessage()
										+ NLS.bind(FrameworkMessages.ODataModelExecutableExtensionIDCreationFailure, id));
			}
		}


	}

	private Image createImage(URL icon)
	{
		ImageDescriptor createFromURL = ImageDescriptor.createFromURL(icon);
		Image image = createFromURL.createImage();
		return image;
	}

	@Override
	public void createControl(Composite parent)
	{
		try
		{
			this.container = new Composite(parent, SWT.NULL);
			GridLayout gl_container = new GridLayout(1, false);
			this.container.setLayout(gl_container);
			GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1);
			gridData.grabExcessHorizontalSpace = true;
			this.container.setLayoutData(gridData);

			createInputGroup(this.container);

			setControl(this.container);
		}
		catch (Exception e)
		{
			setMessage(e.getLocalizedMessage(), ERROR);
		}
	}


	/**
	 * Creates input group
	 * 
	 * @param parent
	 */
	private void createInputGroup(Composite parent)
	{
		// create files group
		this.inputGroup = new Composite(parent, SWT.NONE);
		GridLayout gl_inputGroup = new GridLayout(3, false);
		gl_inputGroup.marginWidth = 0;
		this.inputGroup.setLayout(gl_inputGroup);
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		gridData.grabExcessHorizontalSpace = true;
		this.inputGroup.setLayoutData(gridData);

		this.folderLabel = new Label(inputGroup, SWT.NONE);
		this.folderLabel.setText(FrameworkMessages.Folder);
		this.folderLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		// create source folder path text
		this.folderText = new Text(this.inputGroup, SWT.BORDER);
		this.folderText.setEditable(false);
		this.folderText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		if (structureData.isHCI())
		{
			this.folderText.setText(businessData.getProjectFilePath());
		}

		if (this.folderSelection != null)
		{
			String folderPath = extractFolderPathFromSelection(this.folderSelection);
			if (folderPath != null)
			{
				IStatus validFolderStatus = ResourcesPlugin.getWorkspace().validatePath(folderPath, IResource.FOLDER);
				IProject project = ProjectUtils.getProject(folderPath);
				if ((project != null && project.isOpen()) || validFolderStatus.getSeverity() == IStatus.OK)
				{
					this.businessData.setOdataFolderPath(folderPath);
					folderPath = removeHeadingFileSeparator(folderPath);
					this.folderText.setText(folderPath);
				}
			}
		}

		if (null != this.structureData.getOdataProjectPathFromSMP())
		{
			this.folderText.setText(this.structureData.getOdataProjectPathFromSMP());
		}

		this.folderText.addModifyListener(new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent arg0)
			{
				if (structureData.isBrowseButtonEnabled())
				{
					validatePage();
				}
				callIfFinish();
			}
		});

		// create source folder browse button
		this.folderBrowseButton = new Button(this.inputGroup, SWT.NONE);
		this.folderBrowseButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		this.folderBrowseButton.setText(FrameworkMessages.BrowseButton);
		if (this.structureData.isBrowseButtonEnabled())
		{
			this.folderBrowseButton.setEnabled(true);
			this.folderBrowseButton.addSelectionListener(new SelectionListener()
			{
				@Override
				public void widgetSelected(SelectionEvent arg0)
				{
					ContainerSelectionDialog dialog = new ContainerSelectionDialog(getShell(), ResourcesPlugin
							.getWorkspace().getRoot(), false, FrameworkMessages.SelectFolder);

					dialog.showClosedProjects(false);

					int returnCode = dialog.open();

					if (returnCode == ContainerSelectionDialog.OK)
					{
						Object[] result = dialog.getResult();
						if (result.length == 1)
						{
							String folderPath = ((Path) result[0]).toOSString();

							businessData.setOdataFolderPath(folderPath);

							folderPath = removeHeadingFileSeparator(folderPath);

							folderText.setText(folderPath);
						}
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent arg0)
				{
					// do nothing
				}
			});
		}
		else
		{
			this.folderBrowseButton.setEnabled(false);
		}

		// create service name label
		this.serviceNameLabel = new Label(this.inputGroup, SWT.NONE);
		this.serviceNameLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		this.serviceNameLabel.setText(FrameworkMessages.ServiceName);

		// create service name text
		this.modelNameText = new Text(this.inputGroup, SWT.BORDER);
		this.modelNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		this.modelNameText.setTextLimit(128);
		new Label(inputGroup, SWT.NONE);
		// setting the odata file path if odata model already selected
		if (this.folderSelection != null)
		{
			IFile odataFile = extractODataFilePathFromSelection(folderSelection);
			if (null != odataFile && structureData.isSMPWizard())
			{
				IPath odataFilePath = odataFile.getRawLocation();
				businessData.setOdataFilePath(odataFilePath);
				setODataFileName(odataFile);
			}
		}
		this.modelNameText.addModifyListener(new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent arg0)
			{
				String serviceName = getModelName();

				businessData.setOdataServiceName(serviceName);

				validatePage();

				callIfFinish();
			}
		});

		this.separatorLabel = new Label(this.container, SWT.SEPARATOR | SWT.HORIZONTAL);
		this.separatorLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		this.tableLabel = new Label(this.container, SWT.NONE);
		this.tableLabel.setText(FrameworkMessages.TypeOfModel);

		Composite container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.verticalSpacing = 10;
		container.setLayout(layout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		// add sash form, made from the groups and their descriptions
		SashForm sashForm = new SashForm(container, SWT.HORIZONTAL);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.widthHint = 300;
		gd.heightHint = 150;
		sashForm.setLayoutData(gd);

		// create the table viewer of the model groups
		this.tableViewer = new TableViewer(sashForm, SWT.BORDER);
		this.tableViewer.addSelectionChangedListener(new ISelectionChangedListener()
		{
			@Override
			public void selectionChanged(SelectionChangedEvent event)
			{
				StructuredSelection selection = (StructuredSelection) event.getSelection();
				onSelectionHandler(selection);
			}
		});

		this.tableViewer.addDoubleClickListener(new IDoubleClickListener()
		{
			@Override
			public void doubleClick(DoubleClickEvent event)
			{
				StructuredSelection selection = (StructuredSelection) event.getSelection();
				onSelectionHandler(selection);

				IWizardPage nextPage = getWizard().getNextPage(me);
				if (isPageComplete() && nextPage != null)
				{
					getContainer().showPage(nextPage);
				}
			}


		});

		this.tableViewer.setContentProvider(new IStructuredContentProvider()
		{
			@Override
			public Object[] getElements(Object inputElement)
			{
				Set<?> extensions = (Set<?>) inputElement;

				return extensions.toArray(new CommonODataObject[extensions.size()]);
			}

			@Override
			public void dispose()
			{
				// Do nothing
			}

			@Override
			public void inputChanged(Viewer arg0, Object arg1, Object arg2)
			{
				// Do nothing
			}
		});

		this.tableViewer.setLabelProvider(new ILabelProvider()
		{
			@Override
			public Image getImage(Object element)
			{
				CommonODataObject commonODataObject = (CommonODataObject) element;
				Image image = commonODataObject.getIcon();
				return image;
			}

			@Override
			public String getText(Object element)
			{
				CommonODataObject extension = (CommonODataObject) element;
				return extension.getDisplayName();
			}

			@Override
			public void addListener(ILabelProviderListener arg0)
			{
				// Do nothing
			}

			@Override
			public void dispose()
			{
				// Do nothing
			}

			@Override
			public boolean isLabelProperty(Object arg0, String arg1)
			{
				return false;
			}

			@Override
			public void removeListener(ILabelProviderListener arg0)
			{
				// Do nothing
			}
		});

		this.tableViewer.setInput(this.commonData);

		// for odata model import wizard so that thae first selected object is
		// url object
		if (!this.structureData.isOdataPageIsBlankRequired())
		{
			if (null != this.commonData.iterator().next())
			{
				selObject = this.commonData.iterator().next();
			}
		}


		// create the right hand side of the sash form that holds the
		// descriptions
		this.optionDescription = new StyledText(sashForm, SWT.BORDER | SWT.READ_ONLY | SWT.WRAP | SWT.V_SCROLL
				| SWT.H_SCROLL);
		this.optionDescription.setAlwaysShowScrollBars(false);

		// selecting the odata if model already selected
		if (this.folderSelection != null)
		{
			IFile odataFile = extractODataFilePathFromSelection(folderSelection);
			Object lastElement = null;
			if (null != odataFile && structureData.isSMPWizard())
			{
				if (null != this.commonData.iterator().next())
				{
					final Iterator<CommonODataObject> itr = commonData.iterator();
					lastElement = itr.next();
					while (itr.hasNext())
					{
						lastElement = itr.next();
					}
					selObject = (CommonODataObject) lastElement;
				}
			}
		}
		Dialog.applyDialogFont(container);
		setControl(container);
	}


	/**
	 * Set page complete to false as there are pages available for the below
	 * selected options
	 */
	protected void callIfFinish()
	{
		if ((null != selObject)
				&& (selObject.getDisplayName().equalsIgnoreCase(WizardPagesConstants.ODATA_METADATA_FILE)
						|| selObject.getDisplayName().equalsIgnoreCase(WizardPagesConstants.ODATA_METADATA_URL)
						|| selObject.getDisplayName().equalsIgnoreCase(WizardPagesConstants.SERVICE_CATALOG) || selObject
						.getDisplayName().equalsIgnoreCase(WizardPagesConstants.EXISTING_ODATA_MODEL)))
		{
			setPageComplete(false);
		}
	}

	private String removeHeadingFileSeparator(String folderPath)
	{
		int sepIndex = folderPath.indexOf(File.separator);

		folderPath = folderPath.substring(sepIndex + 1);

		return folderPath;
	}

	/**
	 * Validates the OData model name
	 * 
	 * @param modelName
	 * @return - true if the model name is valid, false otherwise.
	 */
	private boolean isValidModelName(String name)
	{

		boolean b = false;
		boolean b1 = false;

		Pattern nonSpclChar = Pattern.compile(nonSpclCharacter, Pattern.CASE_INSENSITIVE);
		Matcher m = nonSpclChar.matcher(name);
		b = m.find();

		Pattern onlyChar = Pattern.compile(onlyCharacter, Pattern.CASE_INSENSITIVE);
		if (name.trim().length() > 0)
		{
			Matcher m1 = onlyChar.matcher(name.substring(0, 1));
			b1 = m1.find();
		}
		if (b1)
		{
			setMessage(FrameworkMessages.ODataServiceWizard_ModelNameStartLetter, ERROR);
			return false;
		}
		else if (b)
		{
			setMessage(FrameworkMessages.ODataServiceWizard_ModelNameContainLettersDigits, ERROR);
			return false;
		}

		return true;
	}

	/*
	 * Setting the OData File path
	 */
	private void setODataFileName(IFile odataFile)
	{
		String odataFilePath = odataFile.getFullPath().toOSString();
		int sepIndex = odataFilePath.lastIndexOf(File.separator);
		String oDataNameFromPath = odataFilePath.substring(sepIndex + 1);
		String[] oDataName = oDataNameFromPath.split(".odata");
		if (null != oDataName && oDataName.length != 0)
		{
			businessData.setOdataServiceName(oDataName[0]);
			this.modelNameText.setText(oDataName[0]);
		}
	}

	/**
	 * Extracts the project's name from the selection made.
	 * 
	 * @param selection
	 * @return
	 */
	private IFile extractODataFilePathFromSelection(ISelection selection)
	{
		if (selection == null)
		{
			return null;
		}

		if (selection instanceof TreeSelection)
		{
			TreeSelection treeSelection = (TreeSelection) selection;
			Object firstElement = treeSelection.getFirstElement();
			if (firstElement == null)
			{
				return null;
			}

			IFile file = (IFile) Platform.getAdapterManager().getAdapter(firstElement, IFile.class);
			if (file == null)
			{
				if (firstElement instanceof IAdaptable)
				{
					file = (IFile) ((IAdaptable) firstElement).getAdapter(IFile.class);
				}
			}

			if (file != null && (file.getName().endsWith(WizardPagesConstants.DOT_ODATA))) //$NON-NLS-1$
			{
				return file;
			}
		}

		return null;
	}

	/**
	 * Extracts the project's name from the selection made.
	 * 
	 * @param selection
	 * @return
	 */
	private String extractFolderPathFromSelection(ISelection selection)
	{
		if (selection == null)
		{
			return null;
		}

		if (selection instanceof TreeSelection)
		{
			TreeSelection treeSelection = (TreeSelection) selection;
			Object firstElement = treeSelection.getFirstElement();
			if (firstElement == null)
			{
				return null;
			}

			IResource resource = (IResource) Platform.getAdapterManager().getAdapter(firstElement, IResource.class);
			if (resource == null)
			{
				if (firstElement instanceof IAdaptable)
				{
					resource = (IResource) ((IAdaptable) firstElement).getAdapter(IResource.class);
				}
			}

			if (resource != null && !IFile.class.isAssignableFrom(resource.getClass()))
			{
				String folderPath = resource.getFullPath().toOSString();

				return folderPath;
			}
		}

		return null;
	}


	private void onSelectionHandler(StructuredSelection selection)
	{
		try
		{
			selObject = (CommonODataObject) selection.getFirstElement();
			businessData.setCommonODataObject(selObject);
			this.optionDescription.setText(selObject.getDescription());
			validatePage();
			callIfFinish();
		}
		catch (Exception e)
		{
			Logger.getLogger(ODataModelProjectPage.class).logError(e);
			setMessage(e.getLocalizedMessage(), ERROR);
		}

	}

	@Override
	public boolean canFlipToNextPage()
	{
		// Set can flip to next page to true as there are pages available for
		// the below selected options
		if ((null != selObject)
				&& (selObject.getDisplayName().equalsIgnoreCase(WizardPagesConstants.ODATA_METADATA_FILE)
						|| selObject.getDisplayName().equalsIgnoreCase(WizardPagesConstants.ODATA_METADATA_URL)
						|| selObject.getDisplayName().equalsIgnoreCase(WizardPagesConstants.SERVICE_CATALOG) || selObject
						.getDisplayName().equalsIgnoreCase(WizardPagesConstants.EXISTING_ODATA_MODEL))
				&& isPageValidated)
		{
			return true;
		}
		return false;
	}


	/**
	 * @return - the source folder path.
	 */
	private String getFolderPath()
	{
		return this.folderText.getText().trim();
	}

	/**
	 * @return - the model name.
	 */
	private String getModelName()
	{
		return this.modelNameText.getText().trim();
	}

	private void validatePage()
	{
		setPageComplete(false);
		isPageValidated = false;
		String folderPath = getFolderPath();
		if ((folderPath == null || folderPath.isEmpty()) && this.structureData.isBrowseButtonEnabled())
		{
			setMessage(FrameworkMessages.PleaseSelectFolder, ERROR);
			return;
		}

		String modelName = getModelName();
		if (modelName == null || modelName.isEmpty())
		{
			setMessage(FrameworkMessages.SelectModelName, ERROR);
			return;
		}

		boolean validModelName = isValidModelName(modelName);
		if (!validModelName)
		{
			return;
		}

		String rootLocation = ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString();
		File odataFileFolder = new File(rootLocation + File.separator + folderPath);
		File[] listFiles = odataFileFolder.listFiles();
		if (listFiles != null)
		{
			for (File file : listFiles)
			{
				if (file.getName().equalsIgnoreCase(modelName + ".odata")) //$NON-NLS-1$
				{
					setMessage(FrameworkMessages.ODataModelPage_ODataAlreadyExists, ERROR);
					return;
				}
			}
		}

		setMessage(null);
		isPageValidated = true;
		setPageComplete(true);
	}


	@Override
	public void setVisible(boolean visible)
	{
		if (visible)
		{
			if (this.structureData.isOdataPageIsBlankRequired())
			{
				setDescription(FrameworkMessages.NewODataServicePageDescription);
			}
			else
			{
				setDescription(FrameworkMessages.ImportODataServicePageDescription);
			}
			// set the matching help context to the page when it's visible
			PlatformUI.getWorkbench().getHelpSystem().setHelp(getShell(), IHelpConstants.NEW_ODATA_MODEL);

			TableItem tableItem = null;
			final IFile odataFile = extractODataFilePathFromSelection(folderSelection);
			if (this.firstTime)
			{
				this.firstTime = false;
				Table table = this.tableViewer.getTable();
				if (table.getItemCount() > 0)
				{
					if (null != this.folderSelection && odataFile != null)
					{
						// select existing odata model
						if (table.getItemCount() == 5)
						{
							table.select(4);
							tableItem = table.getItem(4);
							validatePage();
							callIfFinish();
						}
						else
						{
							table.select(0);
							tableItem = table.getItem(0);
						}
					}
					else
					{
						table.select(0);
						tableItem = table.getItem(0);
					}

					CommonODataObject selObject = (CommonODataObject) tableItem.getData();
					try
					{
						businessData.setCommonODataObject(selObject);
						this.optionDescription.setText(selObject.getDescription());
					}
					catch (Exception e)
					{
						Logger.getLogger(ODataModelProjectPage.class).logError(e);
						setMessage(e.getLocalizedMessage(), ERROR);
					}
				}


				Display.getCurrent().asyncExec(new Runnable()
				{
					public void run()
					{
						// Disable the focus if the model is already selected
						if (null == folderSelection || null == odataFile)
						{
							modelNameText.setFocus();
						}

					}
				});

			}
		}

		super.setVisible(visible);
	}

	/**
	 * Sets the folder text for SMP
	 * 
	 * @param folderText
	 */
	public void setFolderTextForSMP(String folderText)
	{
		this.folderText.setText(folderText);
	}

	@Override
	public void dispose()
	{
		super.dispose();
		if (null != odataModelConfigElements)
		{
			odataModelConfigElements = null;
		}
		if (null != selObject)
		{
			selObject = null;
		}
		if (null != inputGroup)
		{
			inputGroup = null;
		}
		if (null != folderText)
		{
			folderText = null;
		}
		if (null != folderBrowseButton)
		{
			folderBrowseButton = null;
		}
		if (null != serviceNameLabel)
		{
			serviceNameLabel = null;
		}
		if (null != modelNameText)
		{
			modelNameText = null;
		}
		if (null != commonODataObject)
		{
			commonODataObject = null;
		}
		if (null != commonData)
		{
			commonData = null;
		}
		if (null != folderSelection)
		{
			folderSelection = null;
		}
		if (null != tableViewer)
		{
			tableViewer = null;
		}

		if (null != tableLabel)
		{
			tableLabel = null;
		}
		if (null != container)
		{
			container = null;
		}
		if (null != tableViewer)
		{
			tableViewer = null;
		}
		if (null != folderLabel)
		{
			folderLabel = null;
		}
		if (null != me)
		{
			me = null;
		}
		if (null != separatorLabel)
		{
			separatorLabel = null;
		}
		if (null != optionDescription)
		{
			optionDescription = null;
		}
	}

	@Override
	public void performHelp()
	{
		getShell().setData(IHelpConstants.ORG_ECLIPSE_UI_HELP, IHelpConstants.NEW_ODATA_MODEL);
	}
}
