/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.wizard.pages.help;

/**
 * Represent help constants for the context id inside the contextHelp.xml file
 */
public class HelpConstants
{
	/**
	 * context id for creating proxy.
	 */
	public static final String NEW_PROXY_HELP = ".eb8de224b8fd4dde95a25f6d856208f7"; //$NON-NLS-1$

	/**
	 * context id for creating new Starter Application Project
	 */
	public static final String NEW_PROJECT_HELP = ".99ba696b4bec44688be3df34db44e382"; //$NON-NLS-1$

	/**
	 * context id for templates
	 */
	public static final String TEMPLATES_HELP = ".99ba696b4bec44688be3df34db44e382tempaltes"; //$NON-NLS-1$

	/**
	 * context id for templates
	 */
	public static final String SERVICE_IMPLEMENTATION_HELP = ".serviceImplementationHelp"; //$NON-NLS-1$
}
