package org.eclipse.ogee.core.wizard.pages;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.core.Activator;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.core.wizard.common.CommonWizardBusinessData;
import org.eclipse.ogee.core.wizard.common.CommonWizardObject;
import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.exploration.tree.nodes.ServiceNode;
import org.eclipse.ogee.exploration.tree.viewer.ServiceTreeViewer;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.builders.ODataModelEDMXSetBuilder;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.service.validation.IServiceMetadataFileValidator;
import org.eclipse.ogee.utils.service.validation.Result;
import org.eclipse.ogee.utils.service.validation.Result.Status;
import org.eclipse.ogee.utils.service.validation.ServiceValidator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ogee.help.IHelpConstants;

public class ServiceFromFileWizardPage extends WizardPage
{
	private Logger logger = Logger.getLogger(ServiceFromFileWizardPage.class);

	private static final String EMPTY = ""; //$NON-NLS-1$	
	public static final String PAGE_NAME = "ServiceFromFileWizardPage";
	private Result resultServiceFromFile;
	private EDMXSet odataEdmxSet;
	private CommonWizardBusinessData businessData = null;

	// These filter names are displayed to the user in the file dialog.
	// Note that the inclusion of the actual extension in parentheses is
	// optional,
	// and doesn't have any effect on which files are displayed.
	private static final String[] FILTER_NAMES =
	{"All Files (*.*)"}; //$NON-NLS-1$

	// These filter extensions are used to filter which files are displayed.
	private static final String[] FILTER_EXTS =
	{"*.*"}; //$NON-NLS-1$	

	private Label metadataFileLabel;
	private Text serviceMetadataUriText;
	private Button browseMetadataButton;
	private Label serviceDetails;

	// service tree view part
	private ServiceTreeViewer serviceTreeViewer;
	public static final ImageDescriptor imageDescriptor;
	private Composite container;

	private GridLayout gl_filesGroup;
	private GridData gridData;
	private GridData gd_metadataFileLabel;
	private GridData gd_serviceMetadataUriText;
	private GridData gd_browseMetadataButton;

	private Set<String> unsupported;
	private ITreeNode serviceNode;
	private Edmx edmx;
	private IServiceMetadataFileValidator serviceMetadataValidator;

	private Result resultServiceMetadataFile;
	private Result result;

	private Status status;
	private String message;


	static
	{
		URL imageUrl = Activator.getDefault().getBundle().getEntry(WizardPagesConstants.WIZARD_PAGE_BANNER_ICON);
		imageDescriptor = ImageDescriptor.createFromURL(imageUrl);
	}

	/**
	 * Default constructor
	 * 
	 * @wbp.parser.constructor
	 */
	public ServiceFromFileWizardPage(CommonWizardObject wizardObject)
	{
		super(PAGE_NAME); //$NON-NLS-1$
		this.businessData = wizardObject.getBusinessData();
		setDescription(FrameworkMessages.ServiceFromFileWizardPage_2 + "\n" + FrameworkMessages.V2Support);
		setTitle(FrameworkMessages.ServiceFromFileWizardPage_3);
		setPageComplete(false);
	}


	@Override
	public void createControl(Composite parent)
	{
		// create files group
		container = new Composite(parent, SWT.NONE);
		gl_filesGroup = new GridLayout(3, false);
		gl_filesGroup.marginWidth = 10;
		container.setLayout(gl_filesGroup);
		gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		gridData.grabExcessHorizontalSpace = true;
		container.setLayoutData(gridData);
		// create metadata file label
		this.metadataFileLabel = new Label(container, SWT.NONE);
		gd_metadataFileLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_metadataFileLabel.verticalIndent = 20;
		metadataFileLabel.setLayoutData(gd_metadataFileLabel);
		this.metadataFileLabel.setText(FrameworkMessages.ServiceFromFileWizardPage_4);
		// create metadata file path text
		this.serviceMetadataUriText = new Text(container, SWT.BORDER);
		gd_serviceMetadataUriText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_serviceMetadataUriText.verticalIndent = 20;
		this.serviceMetadataUriText.setLayoutData(gd_serviceMetadataUriText);
		this.serviceMetadataUriText.addModifyListener(new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent event)
			{
				RunnableWithMonitor rwm = new RunnableWithMonitor()
				{
					@Override
					public void run()
					{
						// get page input metadata uri
						String metadataUri = getMetadataUri();
						// validate service metadata
						this.monitor.beginTask(FrameworkMessages.ServiceFromFileWizardPage_5, 14);
						result = validateServiceMetadataFile(metadataUri, this.monitor);
					}
				};

				syncExec(rwm, FrameworkMessages.ServiceFromFileWizardPage_6,
						FrameworkMessages.ServiceFromFileWizardPage_7);

				handleServiceValidationResult(result);
			}
		});
		// create metadata file browse button
		this.browseMetadataButton = new Button(container, SWT.NONE);
		gd_browseMetadataButton = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_browseMetadataButton.verticalIndent = 20;
		this.browseMetadataButton.setLayoutData(gd_browseMetadataButton);
		this.browseMetadataButton.setText(FrameworkMessages.ServiceFromFileWizardPage_8);
		this.browseMetadataButton.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				Shell shell = getShell();
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);

				dialog.setFilterNames(FILTER_NAMES);
				dialog.setFilterExtensions(FILTER_EXTS);

				String path = dialog.open();
				if (path != null)
				{
					File file = new File(path);
					if (file.isFile())
					{
						serviceMetadataUriText.setText(path);
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0)
			{
				// do nothing
			}
		});

		// set service details label
		serviceDetails = new Label(container, SWT.NONE);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1);
		serviceDetails.setLayoutData(gridData);
		serviceDetails.setText(FrameworkMessages.ServiceDetails);

		// adds service view part
		this.serviceTreeViewer = new ServiceTreeViewer(container);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		this.serviceTreeViewer.setLayoutData(gridData);

		setControl(container);
	}

	/**
	 * 
	 * @param result
	 * @throws BuilderException
	 */
	private void handleServiceValidationResult(Result result)
	{
		if (result == null)
		{
			return;
		}

		status = result.getStatus();
		message = result.getMessage();

		if (status == Status.OK)
		{
			setMessage(message, IMessageProvider.INFORMATION);
			setPageComplete(true);
		}
		else if (status == Status.ERROR || status == Status.CUSTOM_ERROR)
		{
			setPageComplete(false);

			setMessage(message, IMessageProvider.ERROR);

			Throwable exception = result.getException();
			if (exception != null)
			{
				logger.logError(message, exception);
			}
		}
		// updates service tree view part
		try
		{
			updateServiceExploration(result);
		}
		catch (BuilderException e)
		{
			setMessage(e.getMessage(), IMessageProvider.ERROR);
			setPageComplete(false);
		}
	}

	/**
	 * updates service exploration view part
	 * 
	 * @param result
	 * @throws BuilderException
	 */
	private void updateServiceExploration(Result result) throws BuilderException
	{
		if (this.serviceTreeViewer == null)
		{
			return;
		}

		if (result.getStatus() == Status.OK)
		{
			odataEdmxSet = getEDMXSet();
			this.businessData.setEdmxSet(odataEdmxSet);

			this.serviceNode = new ServiceNode(result.getServiceName(), odataEdmxSet);

			this.serviceTreeViewer.setService(serviceNode);

			// Non-supported elements will be logged( warning )
			unsupported = this.result.getEdmx1().getNotSupported();
			if (unsupported.size() > 0)
			{
				setMessage(FrameworkMessages.NotSupportedElements_wizard_message, IMessageProvider.INFORMATION);
				logger.log("During the import of [" + result.getServiceName() //$NON-NLS-1$
						+ "] the following elements were discarded: " //$NON-NLS-1$
						+ unsupported.toString());
			}
		}
		else
		// ERROR || CUSTOM_ERROR
		{
			this.serviceTreeViewer.clear();
		}
	}

	/**
	 * 
	 * @return
	 */
	private String getMetadataUri()
	{
		if (this.serviceMetadataUriText == null)
		{
			return EMPTY;
		}

		return this.serviceMetadataUriText.getText().trim();
	}

	/**
	 * Runnable with monitor
	 */
	private abstract class RunnableWithMonitor implements Runnable
	{
		protected IProgressMonitor monitor;

		public void setMonitor(IProgressMonitor monitor)
		{
			this.monitor = monitor;
		}
	}

	/**
	 * 
	 * @param runnable
	 */
	private void syncExec(final RunnableWithMonitor runnable, String taskName, String errorMessage)
	{
		try
		{
			getWizard().getContainer().run(false, true, new IRunnableWithProgress()
			{
				public void run(final IProgressMonitor monitor) throws InvocationTargetException, InterruptedException
				{
					try
					{
						runnable.setMonitor(monitor);
						Display.getDefault().syncExec(runnable);
					}
					finally
					{
						monitor.done();
					}
				}
			});
		}
		catch (InvocationTargetException ex)
		{
			logger.logError(taskName, ex);
			MessageDialog.openError(getShell(), errorMessage, ex.getMessage());
		}
		catch (InterruptedException ex)
		{
			logger.logError(taskName, ex);
			MessageDialog.openError(getShell(), errorMessage, ex.getMessage());
		}
	}

	@Override
	public void setVisible(boolean visible)
	{
		if (visible)
		{
			// set the matching help context to the page when it's visible
		      PlatformUI.getWorkbench().getHelpSystem().setHelp(getShell(), IHelpConstants.SVC_FROM_FILE); //$NON-NLS-1$
		}

		super.setVisible(visible);
	}

	/**
	 * Returns service EDMXSet object
	 * 
	 * @return EDMXSet
	 */
	public EDMXSet getEDMXSet() throws BuilderException
	{
		if (this.resultServiceFromFile == null)
		{
			return null;
		}

		if (this.odataEdmxSet == null)
		{
			edmx = this.resultServiceFromFile.getEdmx1();

			this.odataEdmxSet = ODataModelEDMXSetBuilder
					.build(edmx, this.resultServiceFromFile.getServiceMetadataUri());
		}

		return this.odataEdmxSet;
	}

	/**
	 * Validates the service metadata file and updates the progress monitor.
	 * 
	 * @param monitor
	 *            - progress monitor
	 * @param metadataUri
	 *            - the service's metadata uri
	 * @param serviceDocumentUri
	 *            - the service's service documentUri uri
	 * 
	 * @return the service's metadata
	 */
	public Result validateServiceMetadataFile(String serviceMetadataUri, IProgressMonitor monitor)
	{
		serviceMetadataValidator = ServiceValidator.getServiceMetadataValidator();

		resultServiceMetadataFile = serviceMetadataValidator.validate(serviceMetadataUri, monitor);
		if (resultServiceMetadataFile == null)
		{
			this.resultServiceFromFile = null;
			this.odataEdmxSet = null;
			return null;
		}

		if (this.resultServiceFromFile != null)
		{
			String currentServiceMetadataUri = this.resultServiceFromFile.getServiceMetadataUri();
			String newServiceMetadataUri = resultServiceMetadataFile.getServiceMetadataUri();
			if (currentServiceMetadataUri == null || !currentServiceMetadataUri.equals(newServiceMetadataUri))
			{
				this.odataEdmxSet = null;
			}
		}

		this.resultServiceFromFile = resultServiceMetadataFile;
		return this.resultServiceFromFile;
	}

	@Override
	public boolean canFlipToNextPage()
	{
		return false;
	}


	@Override
	public void dispose()
	{
		super.dispose();
		if (null != businessData)
		{
			businessData = null;
		}
		if (null != metadataFileLabel)
		{
			metadataFileLabel = null;
		}
		if (null != serviceMetadataUriText)
		{
			serviceMetadataUriText = null;
		}
		if (null != browseMetadataButton)
		{
			browseMetadataButton = null;
		}
		if (null != serviceTreeViewer)
		{
			serviceTreeViewer = null;
		}
		if (null != logger)
		{
			logger = null;
		}
		if (null != result)
		{
			result = null;
		}
		if (null != resultServiceMetadataFile)
		{
			resultServiceMetadataFile = null;
		}
		if (null != resultServiceFromFile)
		{
			resultServiceFromFile = null;
		}
		if (null != edmx)
		{
			edmx = null;
		}
		if (null != serviceNode)
		{
			serviceNode = null;
		}
		if (null != unsupported)
		{
			unsupported = null;
		}
		if (null != gd_browseMetadataButton)
		{
			gd_browseMetadataButton = null;
		}
		if (null != gd_serviceMetadataUriText)
		{
			gd_serviceMetadataUriText = null;
		}
		if (null != gd_metadataFileLabel)
		{
			gd_metadataFileLabel = null;
		}
		if (null != gridData)
		{
			gridData = null;
		}
		if (null != gl_filesGroup)
		{
			gl_filesGroup = null;
		}
		if (null != container)
		{
			container = null;
		}
		if (null != odataEdmxSet)
		{
			odataEdmxSet = null;
		}
		if (null != serviceDetails)
		{
			serviceDetails = null;
		}
		if (null != status)
		{
			status = null;
		}
		if (null != message)
		{
			message = null;
		}
	}
	/**
	 * Set the right help key for the context sensitive help
	 */
	@Override
	public void performHelp()
	{
		try
		{
			getShell().setData(IHelpConstants.ORG_ECLIPSE_UI_HELP, IHelpConstants.SVC_FROM_FILE); //$NON-NLS-1$ //$NON-NLS-2$	
		}
		catch (NullPointerException e)
		{
			logger.logError(e.getMessage(), e);
		}
	}
}
