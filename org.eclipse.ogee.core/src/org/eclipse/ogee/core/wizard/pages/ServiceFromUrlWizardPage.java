package org.eclipse.ogee.core.wizard.pages;

import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.core.Activator;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.core.wizard.common.CommonWizardBusinessData;
import org.eclipse.ogee.core.wizard.common.CommonWizardObject;
import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.exploration.tree.nodes.ServiceNode;
import org.eclipse.ogee.exploration.tree.viewer.ServiceTreeViewer;
import org.eclipse.ogee.help.IHelpConstants;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.builders.ODataModelEDMXSetBuilder;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.service.validation.GenericServiceUrlValidator;
import org.eclipse.ogee.utils.service.validation.IServiceUrlValidator;
import org.eclipse.ogee.utils.service.validation.Result;
import org.eclipse.ogee.utils.service.validation.Result.Status;
import org.eclipse.ogee.utils.service.validation.ServiceValidator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

/**
 * This class is for wizard page which handles the metadata url
 * 
 * @author I055871
 * 
 */
public class ServiceFromUrlWizardPage extends WizardPage
{
	private Logger logger = Logger.getLogger(ServiceFromUrlWizardPage.class);
	private static final String EMPTY = ""; //$NON-NLS-1$	
	public static final String PAGE_NAME = "ServiceFromUrlWizardPage";

	private Result result;
	private Result result1;
	private Result resultForServiceUrl;

	private EDMXSet odataEdmxSet;
	private Edmx edmx;
	private Set<String> unsupported;
	private IServiceUrlValidator serviceValidator;

	private Label serviceUrlLable;
	private Text serviceUrlText;

	// go button
	private Button goButton;
	// service tree view part
	private ServiceTreeViewer serviceTreeViewer;
	private CommonWizardBusinessData businessData = null;
	public static final ImageDescriptor imageDescriptor;

	private Composite container;
	private GridData gridData;
	private GridLayout gl_urlGroup;
	private GridData gd_serviceUrlLable;
	private GridData gd_serviceUrlText;
	private Label serviceDetails;
	private GridData gridDataForButton;

	private Status status;
	private String message;
	private ITreeNode serviceNode;


	private String currentServiceUrl;
	private String newServiceUrl;


	static
	{
		URL imageUrl = Activator.getDefault().getBundle().getEntry(WizardPagesConstants.WIZARD_PAGE_BANNER_ICON);
		imageDescriptor = ImageDescriptor.createFromURL(imageUrl);
	}

	/**
	 * Default constructor
	 * 
	 * @wbp.parser.constructor
	 */
	public ServiceFromUrlWizardPage(CommonWizardObject wizardObject)
	{
		super(PAGE_NAME); //$NON-NLS-1$
		this.businessData = wizardObject.getBusinessData();
		setDescription(FrameworkMessages.ServiceFromUrlWizardPage_0 + "\n" + FrameworkMessages.V2Support);
		setTitle(FrameworkMessages.ServiceFromUrlWizardPage_1);
		setPageComplete(false);
	}


	@Override
	public void createControl(Composite parent)
	{
		// create url group
		container = new Composite(parent, SWT.NONE);
		gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		gridData.grabExcessHorizontalSpace = true;
		container.setLayoutData(gridData);
		gl_urlGroup = new GridLayout(3, false);
		gl_urlGroup.marginWidth = 10;
		container.setLayout(gl_urlGroup);
		// create service url label
		this.serviceUrlLable = new Label(container, SWT.NONE);
		gd_serviceUrlLable = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_serviceUrlLable.verticalIndent = 20;
		serviceUrlLable.setLayoutData(gd_serviceUrlLable);
		this.serviceUrlLable.setText(FrameworkMessages.ServiceFromUrlWizardPage_2);
		// set service url text listener
		this.serviceUrlText = new Text(container, SWT.BORDER);
		gd_serviceUrlText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_serviceUrlText.verticalIndent = 20;
		this.serviceUrlText.setLayoutData(gd_serviceUrlText);
		this.serviceUrlText.addModifyListener(new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent arg0)
			{
				if (serviceUrlText.getEditable())
				{
					String serviceUrl = getServiceUrl();

					handleServiceUrlModification(serviceUrl);
				}
			}
		});

		// adds Go button
		createGoButton(container);

		// set service details label
		serviceDetails = new Label(container, SWT.NONE);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1);
		serviceDetails.setLayoutData(gridData);
		serviceDetails.setText(FrameworkMessages.ServiceDetails);
		// adds service view part
		this.serviceTreeViewer = new ServiceTreeViewer(container);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		this.serviceTreeViewer.setLayoutData(gridData);

		setControl(container);
	}

	/**
	 * returns service url
	 * 
	 * @return
	 */
	private String getServiceUrl()
	{
		if (this.serviceUrlText == null)
		{
			return EMPTY;
		}

		return this.serviceUrlText.getText().trim();
	}

	/**
	 * handeles service url modification
	 * 
	 * @param serviceUrl
	 */
	private void handleServiceUrlModification(String serviceUrl)
	{
		setPageComplete(false);

		if (serviceUrl == null || serviceUrl.isEmpty())
		{
			setGoButtonEnabled(false);
			setMessage(FrameworkMessages.ServiceFromUrlWizardPage_3, INFORMATION);
			return;
		}

		boolean validServiceUrl = isServiceUrlSyntaxValid(serviceUrl);
		if (validServiceUrl)
		{
			setGoButtonEnabled(true);
			setMessage(FrameworkMessages.ServiceFromUrlWizardPage_4, INFORMATION);
		}
		else
		{
			setMessage(FrameworkMessages.ServiceFromUrlWizardPage_5, ERROR);
			setGoButtonEnabled(false);
		}
		// clears service tree view part
		this.serviceTreeViewer.clear();
	}

	/**
	 * sets go button to be enabled or disabled
	 * 
	 * @param status
	 */
	private void setGoButtonEnabled(boolean status)
	{
		if (this.goButton == null)
		{
			return;
		}

		this.goButton.setEnabled(status);
	}

	private void createGoButton(Composite parent)
	{
		// create validate button
		this.goButton = new Button(parent, SWT.NONE);
		gridDataForButton = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gridDataForButton.verticalIndent = 20;
		gridDataForButton.widthHint = 80;
		this.goButton.setLayoutData(gridDataForButton);
		this.goButton.setEnabled(false);
		this.goButton.setText(FrameworkMessages.ServiceFromUrlWizardPage_6);
		// set validate button listener
		this.goButton.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				RunnableWithMonitor rwm = new RunnableWithMonitor()
				{
					@Override
					public void run()
					{
						// get page input url
						String serviceUrl = getServiceUrl();
						// validate service url
						this.monitor.beginTask(FrameworkMessages.ServiceFromUrlWizardPage_7, 14);
						result = validateServiceUrl(serviceUrl, this.monitor);
					}
				};

				syncExec(rwm, FrameworkMessages.ServiceFromUrlWizardPage_8,
						FrameworkMessages.ServiceFromUrlWizardPage_9);

				handleServiceValidationResult(result);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0)
			{
				// do nothing
			}
		});
	}

	/**
	 * 
	 * @param result
	 */
	private void handleServiceValidationResult(Result result)
	{
		if (result == null)
		{
			return;
		}

		status = result.getStatus();
		message = result.getMessage();

		if (status == Status.OK)
		{
			updateServiceInputFields(result);
			setGoButtonEnabled(false);
			setMessage(message, IMessageProvider.INFORMATION);
			setPageComplete(true);
		}
		else if (status == Status.ERROR || status == Status.CUSTOM_ERROR)
		{
			setPageComplete(false);

			setGoButtonEnabled(true);

			setMessage(message, IMessageProvider.ERROR);

			Throwable exception = result.getException();
			if (exception != null)
			{
				logger.logError(message, exception);
			}
		}
		// updates service tree view part
		try
		{
			updateServiceExploration(result);
		}
		catch (BuilderException e)
		{
			setGoButtonEnabled(true);
			setMessage(e.getMessage(), IMessageProvider.ERROR);
			setPageComplete(false);
		}
	}

	/**
	 * updates service exploration view part
	 * 
	 * @param result
	 * @throws BuilderException
	 */
	private void updateServiceExploration(Result result) throws BuilderException
	{
		if (this.serviceTreeViewer == null)
		{
			return;
		}

		if (result.getStatus() == Status.OK)
		{
			odataEdmxSet = getEDMXSet();
			this.businessData.setEdmxSet(odataEdmxSet);
			serviceNode = new ServiceNode(result.getServiceName(), odataEdmxSet);

			this.serviceTreeViewer.setService(serviceNode);

			// Non-supported elements will be logged( warning )
			unsupported = this.result.getEdmx1().getNotSupported();
			if (unsupported.size() > 0)
			{
				setMessage(FrameworkMessages.NotSupportedElements_wizard_message, IMessageProvider.INFORMATION);
				logger.log("During the import of [" + result.getServiceName() //$NON-NLS-1$
						+ "] the following elements were discarded: " //$NON-NLS-1$
						+ unsupported.toString());
			}
		}
		else
		// ERROR || CUSTOM_ERROR
		{
			this.serviceTreeViewer.clear();
		}
	}

	private void updateServiceInputFields(Result result)
	{
		if (this.serviceUrlText != null && result.isServiceUrlValidation())
		{
			setText(this.serviceUrlText, result.getServiceUrl(), false);
		}
	}

	/**
	 * Sets text control new value.
	 * 
	 * @param value
	 *            - new text value
	 * @param notifyTextListener
	 *            - notify or not text listener
	 */
	private void setText(Text text, String value, boolean notifyTextListener)
	{
		if (text == null)
		{
			return;
		}

		if (value == null)
		{
			return;
		}

		if (notifyTextListener)
		{
			text.setText(value.trim());
		}
		else
		{
			text.setEditable(false);
			text.setText(value.trim());
			text.setEditable(true);
		}
	}

	/**
	 * Runnable with monitor
	 */
	private abstract class RunnableWithMonitor implements Runnable
	{
		protected IProgressMonitor monitor;

		public void setMonitor(IProgressMonitor monitor)
		{
			this.monitor = monitor;
		}
	}

	/**
	 * 
	 * @param runnable
	 */
	private void syncExec(final RunnableWithMonitor runnable, String taskName, String errorMessage)
	{
		try
		{
			getWizard().getContainer().run(false, true, new IRunnableWithProgress()
			{
				public void run(final IProgressMonitor monitor) throws InvocationTargetException, InterruptedException
				{
					try
					{
						runnable.setMonitor(monitor);
						Display.getDefault().syncExec(runnable);
					}
					finally
					{
						monitor.done();
					}
				}
			});
		}
		catch (InvocationTargetException ex)
		{
			logger.logError(taskName, ex);
			MessageDialog.openError(getShell(), errorMessage, ex.getMessage());
		}
		catch (InterruptedException ex)
		{
			logger.logError(taskName, ex);
			MessageDialog.openError(getShell(), errorMessage, ex.getMessage());
		}
	}

	@Override
	public void setVisible(boolean visible)
	{
		if (visible)
		{
			// set the matching help context to the page when it's visible
			PlatformUI.getWorkbench().getHelpSystem().setHelp(getShell(), IHelpConstants.SVC_FROM_URL); //$NON-NLS-1$
		}

		super.setVisible(visible);
	}

	/**
	 * Returns service EDMXSet object
	 * 
	 * @return EDMXSet
	 * @throws BuilderException
	 */
	public EDMXSet getEDMXSet() throws BuilderException
	{
		if (this.result1 == null)
		{
			return null;
		}

		if (this.odataEdmxSet == null)
		{
			edmx = this.result1.getEdmx1();

			this.odataEdmxSet = ODataModelEDMXSetBuilder.build(edmx, this.result1.getServiceUrl());
		}

		return this.odataEdmxSet;
	}

	/**
	 * Validate syntax of a service url
	 * 
	 * @param serviceUrl
	 * @return
	 */
	public boolean isServiceUrlSyntaxValid(String serviceUrl)
	{
		return GenericServiceUrlValidator.isServiceUrlSyntaxValid(serviceUrl);
	}

	/**
	 * Validates the service url and updates the progress monitor.
	 * 
	 * @param monitor
	 *            - progress monitor
	 * @param serviceUri
	 *            - the service's uri
	 * 
	 * @return the service's metadata
	 */
	public Result validateServiceUrl(String serviceUrl, IProgressMonitor monitor)
	{
		serviceValidator = ServiceValidator.getServiceUrlValidator();

		if (monitor == null)
		{
			monitor = new NullProgressMonitor();
		}

		resultForServiceUrl = serviceValidator.validate(serviceUrl, new SubProgressMonitor(monitor, 8));
		if (resultForServiceUrl == null)
		{
			this.result1 = null;
			this.odataEdmxSet = null;
			return null;
		}

		if (this.result1 != null)
		{
			currentServiceUrl = this.result1.getServiceUrl();
			newServiceUrl = resultForServiceUrl.getServiceUrl();
			if (currentServiceUrl == null || !currentServiceUrl.equals(newServiceUrl))
			{
				this.odataEdmxSet = null;
			}
		}

		this.result1 = resultForServiceUrl;
		return this.result1;
	}

	@Override
	public boolean canFlipToNextPage()
	{
		return false;
	}

	@Override
	public void dispose()
	{
		super.dispose();
		if (null != businessData)
		{
			businessData = null;
		}
		if (null != serviceUrlLable)
		{
			serviceUrlLable = null;
		}
		if (null != serviceUrlText)
		{
			serviceUrlText = null;
		}
		if (null != goButton)
		{
			goButton = null;
		}
		if (null != logger)
		{
			logger = null;
		}
		if (null != result)
		{
			result = null;
		}
		if (null != result1)
		{
			result1 = null;
		}
		if (null != resultForServiceUrl)
		{
			resultForServiceUrl = null;
		}
		if (null != odataEdmxSet)
		{
			odataEdmxSet = null;
		}
		if (null != edmx)
		{
			edmx = null;
		}
		if (null != unsupported)
		{
			unsupported = null;
		}
		if (null != serviceValidator)
		{
			serviceValidator = null;
		}
		if (null != container)
		{
			container = null;
		}
		if (null != gridData)
		{
			gridData = null;
		}
		if (null != gl_urlGroup)
		{
			gl_urlGroup = null;
		}
		if (null != gd_serviceUrlLable)
		{
			gd_serviceUrlLable = null;
		}
		if (null != gd_serviceUrlText)
		{
			gd_serviceUrlText = null;
		}
		if (null != serviceDetails)
		{
			serviceDetails = null;
		}
		if (null != gridDataForButton)
		{
			gridDataForButton = null;
		}
		if (null != status)
		{
			status = null;
		}
		if (null != message)
		{
			message = null;
		}
		if (null != serviceNode)
		{
			serviceNode = null;
		}
		if (null != currentServiceUrl)
		{
			currentServiceUrl = null;
		}
		if (null != newServiceUrl)
		{
			newServiceUrl = null;
		}
		if (null != serviceTreeViewer)
		{
			serviceTreeViewer = null;
		}
	}


	/**
	 * Set the right help key for the context sensitive help
	 */
	@Override
	public void performHelp()
	{
		try
		{
			getShell().setData(IHelpConstants.ORG_ECLIPSE_UI_HELP, IHelpConstants.SVC_FROM_URL); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (NullPointerException e)
		{
			logger.logError(e.getMessage(), e);
		}
	}
}
