package org.eclipse.ogee.core.wizard.pages;


import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ogee.core.Activator;
import org.eclipse.ogee.core.extensions.service.implementation.ServiceImplementationException;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.core.wizard.common.CommonWizardBusinessData;
import org.eclipse.ogee.core.wizard.common.CommonWizardObject;
import org.eclipse.ogee.core.wizard.common.CommonWizardStructureData;
import org.eclipse.ogee.core.wizard.controller.WizardControllerException;
import org.eclipse.ogee.designer.utils.ArtifactUtil;
import org.eclipse.ogee.exploration.tree.api.ITreeNode;
import org.eclipse.ogee.exploration.tree.nodes.ServiceNode;
import org.eclipse.ogee.exploration.tree.viewer.ServiceTreeViewer;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.service.validation.IServiceMetadataFileValidator;
import org.eclipse.ogee.utils.service.validation.Result;
import org.eclipse.ogee.utils.service.validation.ServiceValidator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

/**
 * This class is for wizard page which handles the existing OData implementation
 * 
 * @author I055871
 * 
 */
public class ServiceFromODataWizardPage extends WizardPage
{
	private Logger logger = Logger.getLogger(ServiceFromODataWizardPage.class);
	public static final String PAGE_NAME = "ServiceFromODataWizardPage";
	private Result resultServiceFromFile;
	private EDMXSet odataEdmxSet;
	private CommonWizardBusinessData businessData = null;
	private IFile modelFile;
	private IFile file;
	private IStructuredSelection odataFileSelection;
	private CommonWizardStructureData structureData = null;


	private Label odataModelLabel;
	private Text odataFileText;
	private Button browseODataButton;

	// service tree view part
	private ServiceTreeViewer serviceTreeViewer;
	public static final ImageDescriptor imageDescriptor;

	private Composite container;
	private GridLayout gl_filesGroup;
	private GridData gridData;
	private GridData gd_metadataFileLabel;
	private GridData gd_serviceMetadataUriText;
	private GridData gd_browseMetadataButton;
	private ElementTreeSelectionDialog dialog;
	private IResource selectedResource;
	private Object[] resultObject;
	private IFile odataFile;
	private IPath odataFilePath;
	private Label serviceDetails;
	private IFile odataFileFromSelection;
	private IPath odataFilePathFromSelection;
	private TreeSelection treeSelection;
	private Object firstElement;

	private ITreeNode serviceNode = null;
	private List<org.eclipse.ogee.model.odata.EntityContainer> listEntity = null;
	private String path = null;
	private TransactionalEditingDomain editingDomain;
	private Resource model;
	private ResourceSet resourceSet = null;
	private URI uri;
	private IServiceMetadataFileValidator serviceMetadataValidator;
	private Result result;
	private String currentServiceMetadataUri;
	private String newServiceMetadataUri;

	static
	{
		URL imageUrl = Activator.getDefault().getBundle().getEntry(WizardPagesConstants.WIZARD_PAGE_BANNER_ICON);
		imageDescriptor = ImageDescriptor.createFromURL(imageUrl);
	}


	/**
	 * Default constructor
	 * 
	 * @wbp.parser.constructor
	 */
	public ServiceFromODataWizardPage(CommonWizardObject wizardObject)
	{
		super(PAGE_NAME); //$NON-NLS-1$
		this.businessData = wizardObject.getBusinessData();
		this.structureData = wizardObject.getStructureData();
		setDescription(FrameworkMessages.ServiceFromODataWizardPage_2 + "\n" + FrameworkMessages.V2Support);
		setTitle(FrameworkMessages.ServiceFromODataWizardPage_3);
		setPageComplete(false);
	}


	@Override
	public void createControl(Composite parent)
	{
		// create files group
		container = new Composite(parent, SWT.NONE);
		gl_filesGroup = new GridLayout(3, false);
		gl_filesGroup.marginWidth = 10;
		container.setLayout(gl_filesGroup);
		gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		gridData.grabExcessHorizontalSpace = true;
		container.setLayoutData(gridData);
		// create metadata file label
		this.odataModelLabel = new Label(container, SWT.NONE);
		gd_metadataFileLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_metadataFileLabel.verticalIndent = 20;
		odataModelLabel.setLayoutData(gd_metadataFileLabel);
		this.odataModelLabel.setText(FrameworkMessages.ServiceFromODataWizardPage_OData);
		// create metadata file path text
		this.odataFileText = new Text(container, SWT.BORDER);
		gd_serviceMetadataUriText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_serviceMetadataUriText.verticalIndent = 20;
		this.odataFileText.setLayoutData(gd_serviceMetadataUriText);
		this.odataFileSelection = this.structureData.getSelection();


		this.odataFileText.addModifyListener(new ModifyListener()
		{

			@Override
			public void modifyText(ModifyEvent event)
			{
				RunnableWithMonitor rwm = new RunnableWithMonitor()
				{

					@Override
					public void run()
					{
						this.monitor.beginTask(FrameworkMessages.ServiceFromFileWizardPage_5, 14);

					}
				};

				syncExec(rwm, FrameworkMessages.ServiceFromFileWizardPage_6,
						FrameworkMessages.ServiceFromFileWizardPage_7);

				handleServiceValidationResult();
			}
		});

		// create metadata file browse button
		this.browseODataButton = new Button(container, SWT.NONE);
		gd_browseMetadataButton = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_browseMetadataButton.verticalIndent = 20;
		this.browseODataButton.setLayoutData(gd_browseMetadataButton);
		this.browseODataButton.setText(FrameworkMessages.ServiceFromFileWizardPage_8);
		this.browseODataButton.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				dialog = new ElementTreeSelectionDialog(getShell(), new WorkbenchLabelProvider(),
						new BaseWorkbenchContentProvider());
				dialog.setTitle(FrameworkMessages.ServiceImplementationProjectPage_5);
				dialog.setMessage(FrameworkMessages.ServiceImplementationProjectPage_6);
				dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
				dialog.setHelpAvailable(false);
				dialog.setValidator(new ISelectionStatusValidator()
				{
					@Override
					public IStatus validate(Object[] args)
					{
						if (args.length == 0)
						{
							return new Status(IStatus.INFO, Activator.PLUGIN_ID,
									FrameworkMessages.ServiceImplementationProjectPage_7);
						}
						selectedResource = (IResource) args[0];
						if (selectedResource.toString().endsWith(WizardPagesConstants.DOT_ODATA)) //$NON-NLS-1$
						{
							return new Status(IStatus.OK, Activator.PLUGIN_ID, null);
						}
						return new Status(IStatus.ERROR, Activator.PLUGIN_ID,
								FrameworkMessages.ServiceImplementationProjectPage_7);
					}
				});

				int returnCode = dialog.open();

				if (returnCode == ElementTreeSelectionDialog.OK)
				{
					// clearing the previous instance on Browse OK
					odataEdmxSet = null;
					if (null != serviceTreeViewer)
					{
						serviceTreeViewer.clear();
					}
					resultObject = dialog.getResult();
					if (resultObject != null && resultObject.length >= 1)
					{
						odataFile = (IFile) resultObject[0];
						odataFilePath = odataFile.getRawLocation();
						businessData.setOdataFilePath(odataFilePath);
						setODataFilePath(odataFile);
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0)
			{
				// do nothing
			}
		});

		// set service details label
		serviceDetails = new Label(container, SWT.NONE);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1);
		serviceDetails.setLayoutData(gridData);
		serviceDetails.setText(FrameworkMessages.ServiceDetails);

		// adds service view part
		this.serviceTreeViewer = new ServiceTreeViewer(container);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		this.serviceTreeViewer.setLayoutData(gridData);

		if (odataFileSelection != null)
		{
			odataFileFromSelection = extractODataFilePathFromSelection(odataFileSelection);
			if (odataFileFromSelection != null)
			{
				odataFilePathFromSelection = odataFileFromSelection.getRawLocation();
				businessData.setOdataFilePath(odataFilePathFromSelection);
				setODataFilePath(odataFileFromSelection);
				RunnableWithMonitor rwm = new RunnableWithMonitor()
				{

					@Override
					public void run()
					{
						this.monitor.beginTask(FrameworkMessages.ServiceFromFileWizardPage_5, 14);
					}
				};

				syncExec(rwm, FrameworkMessages.ServiceFromFileWizardPage_6,
						FrameworkMessages.ServiceFromFileWizardPage_7);

				handleServiceValidationResult();

			}
		}

		setControl(container);
	}

	/**
	 * Extracts the project's name from the selection made.
	 * 
	 * @param selection
	 * @return
	 */
	private IFile extractODataFilePathFromSelection(ISelection selection)
	{
		if (selection == null)
		{
			return null;
		}

		if (selection instanceof TreeSelection)
		{
			treeSelection = (TreeSelection) selection;
			firstElement = treeSelection.getFirstElement();
			if (firstElement == null)
			{
				return null;
			}

			file = (IFile) Platform.getAdapterManager().getAdapter(firstElement, IFile.class);
			if (file == null)
			{
				if (firstElement instanceof IAdaptable)
				{
					file = (IFile) ((IAdaptable) firstElement).getAdapter(IFile.class);
				}
			}

			if (file != null && (file.getName().endsWith(WizardPagesConstants.DOT_ODATA))) //$NON-NLS-1$
			{
				return file;
			}
		}

		return null;
	}


	/**
	 * 
	 * @throws ServiceImplementationException
	 * @throws WizardControllerException
	 */
	private void validatePage() throws ServiceImplementationException, WizardControllerException
	{
		setPageComplete(false);

		if (null != odataFileText.getText())
		{
			setMessage(null);
			setPageComplete(true);
		}

	}

	/*
	 * Setting the OData File path
	 */
	private void setODataFilePath(IFile odataFile)
	{
		String odataFilePath = odataFile.getFullPath().toOSString();
		int sepIndex = odataFilePath.indexOf(File.separator);

		odataFilePath = odataFilePath.substring(sepIndex + 1);

		odataFileText.setText(odataFilePath);
	}

	/**
	 * 
	 * @param result
	 * @throws BuilderException
	 */

	private void handleServiceValidationResult()
	{

		// updates service tree view part
		try
		{
			updateServiceExploration();
			validatePage();
		}
		catch (BuilderException e)
		{
			setMessage(e.getMessage(), IMessageProvider.ERROR);
			setPageComplete(false);
		}
		catch (Exception e)
		{
			setMessage(e.getMessage(), IMessageProvider.ERROR);
			setPageComplete(false);
		}
	}

	/**
	 * updates service exploration view part
	 * 
	 * @param result
	 * @throws BuilderException
	 */
	private void updateServiceExploration() throws BuilderException
	{
		odataEdmxSet = getEDMXSet();
		this.businessData.setEdmxSet(odataEdmxSet);

		int schemaSize = 0;

		if (null != odataEdmxSet)
		{
			if (!odataEdmxSet.getMainEDMX().getDataService().getSchemata().isEmpty())
				schemaSize = odataEdmxSet.getMainEDMX().getDataService().getSchemata().size();

			// To populate the service name in the tree from existing model
			for (int i = 0; i < schemaSize; i++)
			{
				if (!odataEdmxSet.getMainEDMX().getDataService().getSchemata().get(i).getContainers().isEmpty())
					listEntity = odataEdmxSet.getMainEDMX().getDataService().getSchemata().get(i).getContainers();
			}

			if (listEntity.isEmpty() && null != listEntity)
			{
				serviceNode = new ServiceNode("default", odataEdmxSet);
			}
			else
			{
				for (int j = 0; j < listEntity.size(); j++)
				{
					if (!listEntity.isEmpty() && null != listEntity.get(j).getName())
					{
						serviceNode = new ServiceNode(listEntity.get(j).getName(), odataEdmxSet);
						break;
					}
					else
					{
						serviceNode = new ServiceNode("default", odataEdmxSet);
					}
				}
			}
			this.serviceTreeViewer.setService(serviceNode);
		}
		else
		{
			if (null != path)
			{
				throw new BuilderException("org.eclipse.core.internal.resources.ResourceException: Resource " + "\\"
						+ path + " does not exist.");
			}
			else
			{
				throw new BuilderException(
						"org.eclipse.core.internal.resources.ResourceException: Resource does not exist.");
			}
		}
	}

	/**
	 * Runnable with monitor
	 */
	private abstract class RunnableWithMonitor implements Runnable
	{
		protected IProgressMonitor monitor;

		public void setMonitor(IProgressMonitor monitor)
		{
			this.monitor = monitor;
		}
	}

	/**
	 * 
	 * @param runnable
	 */
	private void syncExec(final RunnableWithMonitor runnable, String taskName, String errorMessage)
	{
		try
		{
			getWizard().getContainer().run(false, true, new IRunnableWithProgress()
			{
				public void run(final IProgressMonitor monitor) throws InvocationTargetException, InterruptedException
				{
					try
					{
						runnable.setMonitor(monitor);
						Display.getDefault().syncExec(runnable);
					}
					finally
					{
						monitor.done();
					}
				}
			});
		}
		catch (InvocationTargetException ex)
		{
			logger.logError(taskName, ex);
			MessageDialog.openError(getShell(), errorMessage, ex.getMessage());
		}
		catch (InterruptedException ex)
		{
			logger.logError(taskName, ex);
			MessageDialog.openError(getShell(), errorMessage, ex.getMessage());
		}
	}

	@Override
	public void setVisible(boolean visible)
	{
		if (visible)
		{
			// set the matching help context to the page when it's visible
			/*
			 * PlatformUI.getWorkbench().getHelpSystem() .setHelp(getShell(),");
			 *///$NON-NLS-1$
		}

		super.setVisible(visible);
	}

	/**
	 * Returns service EDMXSet object
	 * 
	 * @return EDMXSet
	 */
	public EDMXSet getEDMXSet() throws BuilderException
	{

		try
		{
			path = this.odataFileText.getText().trim();
			this.modelFile = this.getModelFile(new Path(path));
			editingDomain = IModelContext.INSTANCE.getTransaction(this.modelFile);

			uri = URI.createPlatformResourceURI(path, true);

			if (editingDomain != null)
			{
				resourceSet = editingDomain.getResourceSet();
			}
			else
			{
				resourceSet = new ResourceSetImpl();
			}
			model = resourceSet.getResource(uri, true);
			if (model instanceof XMIResource)
			{
				odataEdmxSet = ArtifactUtil.getEDMXSetFromModel(model);
			}
		}
		catch (ModelAPIException e)
		{
			Logger.getLogger(Activator.PLUGIN_ID).logError(e);
		}


		return this.odataEdmxSet;
	}

	private IFile getModelFile(IPath fullPath)
	{
		return ResourcesPlugin.getWorkspace().getRoot().getFile(fullPath.makeAbsolute());
	}

	/**
	 * Validates the service metadata file and updates the progress monitor.
	 * 
	 * @param monitor
	 *            - progress monitor
	 * @param metadataUri
	 *            - the service's metadata uri
	 * @param serviceDocumentUri
	 *            - the service's service documentUri uri
	 * 
	 * @return the service's metadata
	 */
	public Result validateServiceMetadataFile(String serviceMetadataUri, IProgressMonitor monitor)
	{
		serviceMetadataValidator = ServiceValidator.getServiceMetadataValidator();

		result = serviceMetadataValidator.validate(serviceMetadataUri, monitor);
		if (result == null)
		{
			this.resultServiceFromFile = null;
			this.odataEdmxSet = null;
			return null;
		}

		if (this.resultServiceFromFile != null)
		{
			currentServiceMetadataUri = this.resultServiceFromFile.getServiceMetadataUri();
			newServiceMetadataUri = result.getServiceMetadataUri();
			if (currentServiceMetadataUri == null || !currentServiceMetadataUri.equals(newServiceMetadataUri))
			{
				this.odataEdmxSet = null;
			}
		}

		this.resultServiceFromFile = result;
		return this.resultServiceFromFile;
	}

	@Override
	public boolean canFlipToNextPage()
	{
		return false;
	}

	@Override
	public void dispose()
	{
		super.dispose();
		if (null != serviceTreeViewer)
		{
			serviceTreeViewer = null;
		}
		if (null != businessData)
		{
			businessData = null;
		}
		if (null != odataModelLabel)
		{
			odataModelLabel = null;
		}
		if (null != odataFileText)
		{
			odataFileText = null;
		}
		if (null != browseODataButton)
		{
			browseODataButton = null;
		}
		if (null != logger)
		{
			logger = null;
		}
		if (null != resultServiceFromFile)
		{
			resultServiceFromFile = null;
		}
		if (null != odataEdmxSet)
		{
			odataEdmxSet = null;
		}
		if (null != modelFile)
		{
			modelFile = null;
		}
		if (null != file)
		{
			file = null;
		}
		if (null != odataFileSelection)
		{
			odataFileSelection = null;
		}
		if (null != structureData)
		{
			structureData = null;
		}
		if (null != container)
		{
			container = null;
		}
		if (null != gl_filesGroup)
		{
			gl_filesGroup = null;
		}
		if (null != gridData)
		{
			gridData = null;
		}
		if (null != gd_metadataFileLabel)
		{
			gd_metadataFileLabel = null;
		}
		if (null != gd_serviceMetadataUriText)
		{
			gd_serviceMetadataUriText = null;
		}
		if (null != gd_browseMetadataButton)
		{
			gd_browseMetadataButton = null;
		}
		if (null != dialog)
		{
			dialog = null;
		}
		if (null != selectedResource)
		{
			selectedResource = null;
		}
		if (null != resultObject)
		{
			resultObject = null;
		}
		if (null != odataFile)
		{
			odataFile = null;
		}
		if (null != odataFilePath)
		{
			odataFilePath = null;
		}
		if (null != serviceDetails)
		{
			serviceDetails = null;
		}
		if (null != odataFileFromSelection)
		{
			odataFileFromSelection = null;
		}
		if (null != odataFilePathFromSelection)
		{
			odataFilePathFromSelection = null;
		}
		if (null != treeSelection)
		{
			treeSelection = null;
		}
		if (null != firstElement)
		{
			firstElement = null;
		}
		if (null != serviceNode)
		{
			serviceNode = null;
		}
		if (null != listEntity)
		{
			listEntity = null;
		}
		if (null != path)
		{
			path = null;
		}
		if (null != editingDomain)
		{
			editingDomain = null;
		}
		if (null != model)
		{
			model = null;
		}
		if (null != resourceSet)
		{
			resourceSet = null;
		}
		if (null != uri)
		{
			uri = null;
		}
		if (null != serviceMetadataValidator)
		{
			serviceMetadataValidator = null;
		}
		if (null != result)
		{
			result = null;
		}
		if (null != currentServiceMetadataUri)
		{
			currentServiceMetadataUri = null;
		}
		if (null != newServiceMetadataUri)
		{
			newServiceMetadataUri = null;
		}
	}


	/**
	 * Set the right help key for the context sensitive help
	 */
	@Override
	public void performHelp()
	{
		try
		{
			//getShell().setData("org.eclipse.ui.help", "org.eclipse.ogee.core.6d410ce9a48c46929ebe825258a1456b"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (NullPointerException e)
		{
			logger.logError(e.getMessage(), e);
		}
	}
}
