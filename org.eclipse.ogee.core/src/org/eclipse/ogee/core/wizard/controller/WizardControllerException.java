/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.wizard.controller;

public class WizardControllerException extends Exception
{
	private static final long serialVersionUID = -1164213506057770392L;

	public WizardControllerException(Throwable cause)
	{
		super(cause);
	}

	public WizardControllerException(String message)
	{
		super(message);
	}

	public WizardControllerException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
