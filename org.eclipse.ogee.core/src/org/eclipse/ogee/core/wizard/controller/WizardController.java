/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.wizard.controller;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.ogee.core.extensions.wizardpagesprovider.IWizardPagesProvider;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.core.wizard.FrwkAbstractWizard;

public abstract class WizardController
{
	/**
	 * selected visible extension
	 */
	protected IVisible selectedVisibleExtension;

	/**
	 * sets selected extension
	 * 
	 * @param visibleExtension
	 */
	public void setSelectedExtension(IVisible visibleExtension)
	{
		this.selectedVisibleExtension = visibleExtension;
	}

	/**
	 * executes a selected extension doFinish method
	 * 
	 * @param monitor
	 * @throws WizardControllerException
	 */
	public abstract void doFinish(IProgressMonitor monitor) throws WizardControllerException;

	/**
	 * adds selected extension pages to the wizard
	 * 
	 * @param wizard
	 * @throws WizardControllerException
	 */
	public void addSelectedExtensionWizardPages(FrwkAbstractWizard wizard) throws WizardControllerException
	{
		try
		{
			IWizardPagesProvider pagesProvider = getWizardPagesProvider();
			List<IWizardPage> wizardPages = getSelectedExtensionWizardPages(pagesProvider);
			// remove old pages from the specified page index
			wizard.removePages(1);
			// add extension wizard pages;
			for (IWizardPage wizardPage : wizardPages)
			{
				wizard.addPage(wizardPage);
			}
		}
		// CHECKSTYLE:OFF
		catch (Throwable e)
		// CHECKSTYLE:ON
		{
			throw new WizardControllerException(FrameworkMessages.ServiceImplementationController_1, e);
		}
	}

	/**
	 * retruns selected extension wizard pages provider
	 * 
	 * @return wizard pages provider
	 * @throws WizardControllerException
	 */
	protected abstract IWizardPagesProvider getWizardPagesProvider() throws WizardControllerException;

	/**
	 * returns page provider wizard pages
	 * 
	 * @param pagesProvider
	 * @return
	 */
	private List<IWizardPage> getSelectedExtensionWizardPages(IWizardPagesProvider pagesProvider)
	{
		if (pagesProvider == null)
		{
			return new ArrayList<IWizardPage>();
		}

		List<IWizardPage> wizardPages = pagesProvider.getWizardPages();
		if (wizardPages == null)
		{
			return new ArrayList<IWizardPage>();
		}

		return wizardPages;
	}
}
