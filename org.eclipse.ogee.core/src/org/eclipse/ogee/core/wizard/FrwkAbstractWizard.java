/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.wizard;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Composite;

/**
 * 
 * @deprecated
 * 
 */
public abstract class FrwkAbstractWizard extends Wizard
{ // filled with specific wizard pages
	protected List<IWizardPage> wizardPages = new ArrayList<IWizardPage>();


	/**
	 * Removes pages from the specified index
	 * 
	 * @param pageIndex
	 *            page to start the removal
	 */
	public void removePages(int pageIndex)
	{
		for (int i = this.wizardPages.size() - 1; i >= pageIndex; i--)
		{
			this.wizardPages.remove(i);
		}
	}


	@Override
	public void addPage(IWizardPage page)
	{
		this.wizardPages.add(page);

		page.setWizard(this);
	}


	@Override
	public boolean canFinish()
	{
		// Default implementation is to check if all pages are complete.
		for (IWizardPage wizardPage : this.wizardPages)
		{
			if (!wizardPage.isPageComplete())
			{
				return false;
			}
		}

		return true;
	}


	@Override
	public void createPageControls(Composite pageContainer)
	{
		// the default behavior is to create all the pages controls
		for (IWizardPage wizardPage : this.wizardPages)
		{
			wizardPage.createControl(pageContainer);
			// page is responsible for ensuring the created control is
			// accessable
			// via getControl.
			Assert.isNotNull(wizardPage.getControl());
		}
	}


	@Override
	public void dispose()
	{
		// notify pages
		for (IWizardPage wizardPage : this.wizardPages)
		{
			wizardPage.dispose();
		}

		super.dispose();
	}


	@Override
	public IWizardPage getNextPage(IWizardPage page)
	{
		int index = this.wizardPages.indexOf(page);
		if (index == this.wizardPages.size() - 1 || index == -1)
		{
			// last page or page not found
			return null;
		}

		return this.wizardPages.get(index + 1);
	}


	@Override
	public IWizardPage getPage(String name)
	{
		for (IWizardPage wizardPage : this.wizardPages)
		{
			String pageName = wizardPage.getName();
			if (pageName.equals(name))
			{
				return wizardPage;
			}
		}

		return null;
	}


	@Override
	public int getPageCount()
	{
		return this.wizardPages.size();
	}


	@Override
	public IWizardPage[] getPages()
	{
		return (IWizardPage[]) this.wizardPages.toArray(new IWizardPage[this.wizardPages.size()]);
	}


	@Override
	public IWizardPage getPreviousPage(IWizardPage page)
	{
		int index = this.wizardPages.indexOf(page);
		if (index == 0 || index == -1)
		{
			// first page or page not found
			return null;
		}

		return this.wizardPages.get(index - 1);
	}


	@Override
	public IWizardPage getStartingPage()
	{
		if (this.wizardPages.isEmpty())
		{
			return null;
		}

		return this.wizardPages.get(0);
	}


	@Override
	public boolean needsPreviousAndNextButtons()
	{
		return super.needsPreviousAndNextButtons() || this.wizardPages.size() >= 1;
	}
}
