/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core;

import java.util.Map;

import org.eclipse.core.internal.registry.ExtensionRegistry;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.equinox.p2.engine.spi.ProvisioningAction;
import org.eclipse.graphiti.ui.editor.DiagramEditorInput;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.designer.ODataEditorInput;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

@SuppressWarnings("restriction")
public class UninstallAction extends ProvisioningAction
{
	private static Logger logger = Logger.getUtilsLogger();

	private static final String ODATA_PERSPECTIVE_ID = "org.eclipse.ogee.utils.perspective"; //$NON-NLS-1$

	public UninstallAction()
	{
	}

	@Override
	public IStatus execute(Map<String, Object> parameters)
	{
		try
		{
			Display.getDefault().asyncExec(new Runnable()
			{
				@Override
				public void run()
				{
					//logger.log("Closing OData Perspective"); //$NON-NLS-1$
					try
					{
						closeODataEditors();
						logger.log("Closed OData Editors"); //$NON-NLS-1$
					}
					catch (PartInitException e)
					{
						logger.logError(e);
					}
					closeODataPerspective();
					logger.log("Closed OData Perspective"); //$NON-NLS-1$
				}
			});

			String operandObject = parameters.get("operand").toString(); //$NON-NLS-1$			
			if (operandObject != null && !operandObject.isEmpty() && operandObject.contains("--> null")) //$NON-NLS-1$
			{
				executeUninstall();
			}
		}
		catch (Exception e)
		{
			logger.logError(FrameworkMessages.UninstallAction_0, e);
		}

		return Status.OK_STATUS;
	}

	protected void closeODataEditors() throws PartInitException
	{
		boolean odataEditor = false;
		IWorkbenchWindow w = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (w != null)
		{
			IWorkbenchPage[] pages = w.getPages();
			for (IWorkbenchPage page : pages)
			{
				IEditorReference[] editorReferences = page.getEditorReferences();
				for (IEditorReference editorReference : editorReferences)
				{
					odataEditor = false;
					IEditorInput editorInput = editorReference.getEditorInput();
					if (editorInput instanceof ODataEditorInput)
					{
						final URI uri = ((ODataEditorInput) editorInput).getUri();
						final String uriString = uri.trimFragment().toPlatformString(true);
						IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(uriString));
						if (file != null && "odata".equals(file.getFileExtension()))
						{
							odataEditor = true;
						}
					}
					else if (editorInput instanceof IFileEditorInput)
					{
						IFile file = ((IFileEditorInput) editorInput).getFile();
						if (file != null && "odata".equals(file.getFileExtension()))
						{
							odataEditor = true;
						}
					}
					else if (editorInput instanceof DiagramEditorInput)
					{
						final URI uri = ((DiagramEditorInput) editorInput).getUri();
						final String uriString = uri.trimFragment().toPlatformString(true);
						IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(uriString));
						if (file != null && "odata".equals(file.getFileExtension()))
						{
							odataEditor = true;
						}
					}
					if (odataEditor)
					{
						IEditorPart editorPart = page.findEditor(editorInput);
						page.closeEditor(editorPart, true);
					}
				}
			}
		}
	}

	@Override
	public IStatus undo(Map<String, Object> parameters)
	{
		return null;
	}

	/**
	 * Removes all preferences configurations and encripted password
	 * 
	 */
	protected void executeUninstall()
	{
		logger.log(FrameworkMessages.UninstallAction_2);
		ExtensionRegistry extensionRegistry = (ExtensionRegistry) Platform.getExtensionRegistry();
		extensionRegistry.clearRegistryCache();
		logger.log(FrameworkMessages.UninstallAction_3);

	}

	protected void closeODataPerspective()
	{
		IWorkbenchWindow w = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (w != null)
		{
			IWorkbenchPage wp = w.getActivePage();
			if (wp != null)
			{
				IPerspectiveDescriptor[] pds = wp.getOpenPerspectives();
				if (pds != null && pds.length > 1)
				{
					for (IPerspectiveDescriptor pd : pds)
					{
						if (ODATA_PERSPECTIVE_ID.equals(pd.getId()))
						{
							wp.closePerspective(pd, true, true);
						}
					}
				}
			}
		}
	}

}
