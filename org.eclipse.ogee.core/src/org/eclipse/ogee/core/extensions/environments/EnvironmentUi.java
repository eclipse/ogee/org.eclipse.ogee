/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.environments;

import java.util.EnumMap;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;

/**
 * Represents an Environment UI of the framework.
 * 
 */
public abstract class EnvironmentUi
{
	/**
	 * the wizard page containing the environment UI
	 */
	protected WizardPage wizardPage;
	/**
	 * common environment model set by the framework
	 */
	protected EnumMap<EnvironmentConstants, Object> commonModel;


	/**
	 * Sets common environment model. This final method is called by the
	 * framework.
	 * 
	 * @param commonModel
	 *            common environment model
	 * @see EnvironmentConstants
	 */
	public final void setCommonModel(EnumMap<EnvironmentConstants, Object> commonModel)
	{
		this.commonModel = commonModel;
	}


	/**
	 * Returns common environment model.
	 * 
	 * @see EnvironmentConstants
	 * @return common environment model
	 */
	public final EnumMap<EnvironmentConstants, Object> getCommonModel()
	{
		return this.commonModel;
	}


	/**
	 * Sets the wizard page containing the environment UI. This final method is
	 * called by framework.
	 * 
	 * @param wizardPage
	 *            the wizard page containing the environment UI
	 */
	public final void setWizardPage(WizardPage wizardPage)
	{
		this.wizardPage = wizardPage;
	}


	/**
	 * Updates the status of the wizard page containing the environment UI
	 * control with the message.
	 * 
	 * @param message
	 *            to be displayed in wizard
	 */
	public final void updateStatus(Message message)
	{
		if (message == null)
		{
			this.wizardPage.setMessage(null);
			this.wizardPage.setPageComplete(true);
		}
		else if (message.getSeverity() <= 2)
		{
			this.wizardPage.setMessage(message.toString(), message.getSeverity());
			this.wizardPage.setPageComplete(true);
		}
		else
		{
			// since setMessage with error equals 3 but IStatus.ERROR equals 4 i
			// have to decrement 1
			this.wizardPage.setMessage(message.toString(), message.getSeverity() - 1);
			this.wizardPage.setPageComplete(false);
		}
	}


	/**
	 * Creates the composite UI control for the environment UI. Must use
	 * {@link #updateStatus(Message)} method to notify the page of any problems
	 * in the created composite UI control. Must use
	 * {@link #areEnvironmentParametersValid()} method to check the validity of
	 * the parameters of the composite UI control.
	 * 
	 * @param parent
	 *            the parent composite
	 * @return composite UI control for the environment UI
	 */
	public abstract Composite createCompositeUI(final Composite parent);


	/**
	 * Determines if the parameters of the environment UI and the specified
	 * project are valid or not. This needs to be implemented by the user since
	 * the framework doesn't know how this UI will look like.
	 * <p>
	 * Below is an example method:
	 * 
	 * <pre>
	 * public boolean areEnvironmentParametersValid()
	 * {
	 *  // check project name for errors
	 *          String projectName = (String) this.envUi.getCommonModel().get(EnvironmentConstants.PROJECT_NAME);
	 *  Message projectMessage = validateProject();
	 *   	 if (projectMessage.getSeverity() == IStatus.ERROR)
	 *  {
	 * 	updateStatus(projectMessage);
	 * 	return false;
	 *  }
	 *  // check package name for errors
	 *  String packageName = getPackageName();
	 *  Message packageMessage = validatePackage(packageName);
	 *  if (packageMessage.getSeverity() == IStatus.ERROR)
	 *  {
	 * 	updateStatus(packageMessage);
	 * 	return false;
	 *  }
	 * 		
	 *  // check project name for warning
	 *  if (projectMessage.getSeverity() == IStatus.WARNING)
	 *  {
	 * 	updateStatus(projectMessage);
	 * 	return true;
	 *  }
	 *  // check package name for warning
	 *  if (packageMessage.getSeverity() == IStatus.WARNING)
	 *  {
	 * 	updateStatus(packageMessage);
	 * 	return true;
	 *  }
	 * 
	 *  updateStatus(null);
	 *  return true;
	 * }
	 * 
	 * <pre>
	 * 
	 * {@link #updateStatus(Message)}
	 * @return true if the environment UI parameters and the specified project are valid, otherwise false
	 */
	public abstract boolean areEnvironmentParametersValid();
}
