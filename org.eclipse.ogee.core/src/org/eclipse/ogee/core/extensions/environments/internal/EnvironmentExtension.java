/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.environments.internal;

import java.net.URL;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.ogee.core.extensions.Extension;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.environments.Environment;
import org.eclipse.ogee.core.extensions.environments.EnvironmentException;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.ogee.core.extensions.visible.VisibleExtension;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;

/**
 * Represents an EnvironmentExtension.
 */
public class EnvironmentExtension extends Extension implements IVisible
{
	private Environment environment;
	private IVisible visibleExtension;

	/**
	 * Constructs a new EnvironmentExtension with the given
	 * IConfigurationElement.
	 * 
	 * @param configElement
	 *            - the data from Eclipse.
	 * @throws ExtensionException
	 */
	public EnvironmentExtension(IConfigurationElement configElement) throws ExtensionException
	{
		super(configElement);

		this.visibleExtension = new VisibleExtension(configElement, this.id);
	}

	/**
	 * @return - the Environment class of the environment extension, or an
	 *         exception is thrown (null is never returned).
	 * @throws EnvironmentException
	 *             - if the extension was null, or not an instance of an
	 *             Environment class, or something went wrong :)
	 */
	public Environment getEnvironment() throws ExtensionException
	{
		// only creates it once
		if (this.environment == null)
		{
			try
			{
				// creates and returns a new instance of the executable
				// extension
				// identified by the environment's name
				Object obj = this.configElement.createExecutableExtension(ExtensionAttributes.Environment.name());
				if (obj == null)
				{
					throw new ExtensionException(
							FrameworkMessages.EnvironmentExtension_EnvExecutableExtensionCreationFailure);
				}

				if (obj instanceof Environment)
				{
					this.environment = (Environment) obj;

					return this.environment;
				}

				throw new ExtensionException(
						FrameworkMessages.EnvironmentExtension_EnvExecutableExtensionCreationFailure);
			}
			catch (CoreException e)
			{
				// we want the exception to be shown in the UI,
				// so here we only send it upward
				throw new ExtensionException(e);
			}
		}

		return this.environment;
	}

	@Override
	public int compareTo(IVisible o)
	{
		return this.visibleExtension.compareTo(o);
	}

	@Override
	public String getDisplayName()
	{
		return this.visibleExtension.getDisplayName();
	}

	@Override
	public URL getIcon()
	{
		return this.visibleExtension.getIcon();
	}

	@Override
	public String getDescription()
	{
		return this.visibleExtension.getDescription();
	}

	@Override
	public String toString()
	{
		return super.toString() + this.visibleExtension.toString();
	}
}
