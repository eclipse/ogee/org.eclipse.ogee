/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.environments;

/**
 * Represents an environment UI message.
 * 
 */
public class Message
{
	private String message;
	private int severity;

	/**
	 * Creates new message.
	 * 
	 * @param message
	 *            text to display
	 * @param severity
	 *            level of severity
	 */
	public Message(String message, int severity)
	{
		this.message = message;

		this.severity = severity;
	}

	/**
	 * Returns level of severity
	 * 
	 * @return level of severity
	 */
	public int getSeverity()
	{
		return severity;
	}

	@Override
	public String toString()
	{
		return this.message;
	}
}
