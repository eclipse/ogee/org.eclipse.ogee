/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.environments;

import org.eclipse.ogee.core.extensions.templates.TemplateType;


/**
 * Represents the Environment extension of the framework.
 * 
 */
public abstract class Environment
{
	protected TemplateType templateType;


	/**
	 * Constructs a new environment class.
	 */
	public Environment()
	{
	}


	/**
	 * Sets the environment template type. This final method is called by the
	 * framework.
	 * 
	 * @param templateType
	 *            - the environment template type.
	 */
	public final void setTemplateType(TemplateType templateType)
	{
		this.templateType = templateType;
	}


	/**
	 * Each environment gets a template type from the framework. It decides
	 * which UI control to present according to the template type.
	 * <p>
	 * Below is an example method that creates the UI according to the template
	 * type:
	 * 
	 * <pre>
	 * <p>
	 * private EnvironmentUi envUi;
	 *
	 * public EnvironmentUi createEnvironmentUI(TemplateType templateType)
	 * {
	 *	  EnvironmentUi envUi = null;
	 *	
	 *	  switch(this.templateType)
	 *	  {
	 *		case Proxy: 
	 *			// an implementation of proxy environment UI
	 *			envUi = new ProxyEnvironmentUi();
	 *			break;
	 *		case Starter_Application:
	 *			// an implementation of starter application environment UI
	 *			envUi = new StarterAppEnvironmentUi();
	 *			break;
	 *	  }
	 *	
	 *	  this.envUi = envUi;
	 *	
	 *	  return envUi;
	 *}
	 * <pre>
	 * @return an environment UI control that corresponds to the given template type.
	 */
	public abstract EnvironmentUi createEnvironmentUI();
}
