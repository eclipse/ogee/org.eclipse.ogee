/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.visible;

import java.net.URL;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IContributor;
import org.eclipse.core.runtime.InvalidRegistryObjectException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ogee.core.Activator;
import org.eclipse.ogee.core.extensions.Extension;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;

/**
 * Represents a Visible Extension.
 */
public class VisibleExtension implements IVisible
{
	private String id;
	private String displayName;
	private URL icon;
	private String description;

	/**
	 * Constructs a new VisibleExtension.
	 * 
	 * @param configElement
	 *            - the data from Eclipse.
	 * @throws ExtensionException
	 */
	public VisibleExtension(IConfigurationElement configElement, String id) throws ExtensionException
	{
		try
		{
			this.id = id;

			this.displayName = configElement.getAttribute(ExtensionAttributes.displayName.name());
			Extension.validateStringInput(this.displayName, FrameworkMessages.VisibleExtension_ExtensionDisplayName);

			// icon contributor plugin
			IContributor contributor = configElement.getContributor();
			// get icon contributor plugin name
			String contributorName = contributor.getName();

			String iconPartialPath = configElement.getAttribute(ExtensionAttributes.icon.name());
			if (iconPartialPath == null || iconPartialPath.trim().isEmpty())
			{
				iconPartialPath = "res/images/collection.png"; //$NON-NLS-1$
				this.icon = Activator.getDefault().getBundle().getResource(iconPartialPath);
			}
			else
			{
				this.icon = Platform.getBundle(contributorName).getResource(iconPartialPath.trim());
			}

			this.description = configElement.getAttribute(ExtensionAttributes.description.name());
			Extension.validateStringInput(this.description, FrameworkMessages.VisibleExtension_ExtensionDescription);
		}
		catch (InvalidRegistryObjectException e)
		{
			throw new ExtensionException(e);
		}
	}


	@Override
	public int compareTo(IVisible o)
	{
		return this.displayName.compareToIgnoreCase(o.getDisplayName());
	}

	@Override
	public String getId()
	{
		return this.id;
	}

	@Override
	public String getDisplayName()
	{
		return this.displayName;
	}

	@Override
	public URL getIcon()
	{
		return this.icon;
	}

	@Override
	public String getDescription()
	{
		return this.description;
	}

	@Override
	public String toString()
	{
		String details = "displayName - " + this.displayName + "\ndescription - " + this.description + "\nicon - " + this.icon; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		return details;
	}
}
