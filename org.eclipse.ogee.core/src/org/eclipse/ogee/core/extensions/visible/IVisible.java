/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.visible;

import java.net.URL;

/**
 * Represents an interface of IVisible.
 */
public interface IVisible extends Comparable<IVisible>
{
	/**
	 * Gets the id of the extension.
	 * 
	 * @return the id of the extension.
	 */
	public String getId();

	/**
	 * Gets the display name of the extension.
	 * 
	 * @return the display name of the extension.
	 */
	public String getDisplayName();

	/**
	 * Gets the icon url of the extension.
	 * 
	 * @return the icon url of the extension.
	 */
	public URL getIcon();

	/**
	 * Gets the description of the extension.
	 * 
	 * @return the description of the extension.
	 */
	public String getDescription();
}
