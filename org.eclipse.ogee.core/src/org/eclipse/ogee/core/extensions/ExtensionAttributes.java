/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions;

/**
 * Represents an Extension Attributes.
 */
public enum ExtensionAttributes
{
	id,

	modelClass, inputModel, outputModelClass,

	displayName, icon, description,

	Environment, environmentId,

	Pattern, pattern, patternId,

	Template, proxy, starter_application,

	Component, observableComponent, componentId, component,

	outputLocation,

	ODataModelProvider, wizardType, sortCategory,

	ServiceImplementationProvider
}
