/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.templates;

import org.eclipse.ogee.core.extensions.environments.Environment;
import org.eclipse.ogee.core.extensions.patterns.Pattern;
import org.eclipse.swt.widgets.Shell;

/**
 * Represents a Template constants.
 * 
 */
public enum TemplateConstants
{
	/**
	 * Template environment.
	 * <p>
	 * The value object is {@link Environment}.
	 */
	ENVIRONMENT,

	/**
	 * Template pattern.
	 * <p>
	 * The value object is {@link Pattern}.
	 */
	PATTERN,

	/**
	 * Eclipse SWT shell.
	 * <p>
	 * The value object is {@link Shell}.
	 */
	SHELL
}
