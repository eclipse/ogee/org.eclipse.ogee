/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.templates.internal;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.ogee.core.extensions.Extension;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.templates.Template;
import org.eclipse.ogee.core.extensions.templates.TemplateException;
import org.eclipse.ogee.core.extensions.templates.TemplateType;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.osgi.util.NLS;

/**
 * Represents a Template Extension.
 */
public class TemplateExtension extends Extension
{
	protected String environmentId;
	protected Template template;
	protected TemplateType templateType;
	protected String patternId;
	protected OutputLocation outputLocation;

	/**
	 * Constructs a new Template Extension with a given IConfigurationElement.
	 * 
	 * @param configElement
	 * @throws ExtensionException
	 */
	public TemplateExtension(IConfigurationElement configElement) throws ExtensionException
	{
		super(configElement);

		this.environmentId = configElement.getAttribute(ExtensionAttributes.environmentId.name());
		validateStringInput(this.environmentId, FrameworkMessages.TemplateExtension_TemplateEnvId);

		initializeTemplateType();

		this.patternId = configElement.getAttribute(ExtensionAttributes.patternId.name());

		String outputLocation = configElement.getAttribute(ExtensionAttributes.outputLocation.name());
		if (outputLocation == null)
		{
			// if there's no outputLocation selected, the default is "project"
			this.outputLocation = OutputLocation.project;
		}
		else
		{
			this.outputLocation = OutputLocation.valueOf(outputLocation);
		}
	}

	/**
	 * Initialize the Template Type to be Proxy or Starter Application.
	 * 
	 * @throws ExtensionException
	 */
	private void initializeTemplateType() throws ExtensionException
	{
		String type = this.configElement.getName();

		if (type.equals(ExtensionAttributes.proxy.name()))
		{
			this.templateType = TemplateType.Proxy;
		}
		else if (type.equals(ExtensionAttributes.starter_application.name()))
		{
			this.templateType = TemplateType.Starter_Application;
		}
		else
		{
			throw new ExtensionException(NLS.bind(FrameworkMessages.Unknown_Template_Extension_Type, type));
		}
	}


	/**
	 * Get the Template type.
	 * 
	 * @return Template type.
	 */
	public TemplateType getType()
	{
		return this.templateType;
	}

	/**
	 * Get the Template Extension.
	 * 
	 * @return Template Extension always return not null value.
	 * @throws TemplateException
	 *             if the extension was null, or not an instance of an Template
	 *             class, or something went wrong
	 */
	public Template getTemplate() throws ExtensionException
	{
		if (this.template == null)
		{
			try
			{
				Object obj = this.configElement.createExecutableExtension(ExtensionAttributes.Template.name());
				if (obj == null)
				{
					throw new ExtensionException(
							FrameworkMessages.TemplateExtension_TemplateExecutableExtensionCreationFailure);
				}

				if (obj instanceof Template)
				{
					this.template = (Template) obj;

					return this.template;
				}

				throw new ExtensionException(
						FrameworkMessages.TemplateExtension_TemplateExecutableExtensionCreationFailure);
			}
			catch (CoreException e)
			{
				throw new ExtensionException(e);
			}
		}

		return this.template;
	}

	/**
	 * Get the Environment id.
	 * 
	 * @return the id of the environment extension.
	 */
	public String getEnvironmentId()
	{
		return this.environmentId;
	}

	/**
	 * Get Pattern id.
	 * 
	 * @return pattern id of the pattern extension.
	 */
	public String getPatternId()
	{
		return this.patternId;
	}

	/**
	 * Get whether the template uses a project or not.
	 * 
	 * @return true whether the template uses a project, false otherwise.
	 */
	public OutputLocation getOutputLocation()
	{
		return this.outputLocation;
	}

	@Override
	public String toString()
	{
		return super.toString() + "\n type - " + getType().name() + //$NON-NLS-1$
				"\nenvironmentId - " + this.environmentId + "\npatternId - " + this.patternId; //$NON-NLS-1$ //$NON-NLS-2$
	}
}
