/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.templates;

import java.util.EnumMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ogee.core.extensions.wizardpagesprovider.IWizardPagesProvider;

/**
 * Represent a Template extension of the framework.
 * 
 */
public abstract class Template implements IWizardPagesProvider
{
	protected EnumMap<TemplateConstants, Object> context;


	public EnumMap<TemplateConstants, Object> getContext()
	{
		return context;
	}

	/**
	 * Constructs a new template class.
	 */
	public Template()
	{
	}

	/**
	 * The template receives an environment and a pattern and a UI shell from
	 * the framework. <br>
	 * <br>
	 * Some templates don't have a pattern (can be null).<br>
	 * <br>
	 * Below is an example for a Java template:<br>
	 * <br>
	 * IJavaSeEnvironment javeSeEnvironment = (IJavaSeEnvironment)
	 * this.context.get(TemplateConstants.ENVIRONMENT)<br>
	 * Pattern pattern = (Pattern) this.context.get(TemplateConstants.PATTERN)<br>
	 * Shell shell = (Shell) this.context.get(TemplateConstants.PATTERN)<br>
	 * <br>
	 * 
	 * @param context
	 *            - the given context.
	 */
	public void setContext(EnumMap<TemplateConstants, Object> context)
	{
		this.context = context;
	}

	/**
	 * This method is responsible for the logic when Finish button is pressed.<br>
	 * Here one can use the abilities of the environment and pattern models.<br>
	 * <br>
	 * Below is an example for a Java template:<br>
	 * <br>
	 * environmentModel.updateProxyProject();<br>
	 * String projectName = environmentModel.getProject();<br>
	 * <br>
	 * 
	 * @throws TemplateException
	 *             if the method fails
	 */
	public abstract void onFinish(IProgressMonitor monitor) throws TemplateException;

	/**
	 * This method is responsible for the logic when any template error occurs.<br>
	 * Here one can revert any changes done till now.<br>
	 * The method is called by the framework.<br>
	 */
	public void onError()
	{
	}
}
