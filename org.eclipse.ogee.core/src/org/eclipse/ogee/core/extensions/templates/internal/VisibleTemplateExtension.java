/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.templates.internal;

import java.net.URL;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.ogee.core.extensions.visible.VisibleExtension;

/**
 * Represents a Visible Template Extension.
 */
public class VisibleTemplateExtension extends TemplateExtension implements IVisible
{
	private IVisible visibleExtension;

	/**
	 * Constructs a new Visible Template Extension with the given
	 * IConfigurationElement.
	 * 
	 * @param configElement
	 * @throws ExtensionException
	 */
	public VisibleTemplateExtension(IConfigurationElement configElement) throws ExtensionException
	{
		super(configElement);

		this.visibleExtension = new VisibleExtension(configElement, this.id);
	}

	@Override
	public String toString()
	{
		return super.toString() + this.visibleExtension.toString();
	}

	@Override
	public int compareTo(IVisible o)
	{
		return this.visibleExtension.compareTo(o);
	}

	@Override
	public String getDisplayName()
	{
		return this.visibleExtension.getDisplayName();
	}

	@Override
	public URL getIcon()
	{
		return this.visibleExtension.getIcon();
	}

	@Override
	public String getDescription()
	{
		return this.visibleExtension.getDescription();
	}
}
