/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.templates;

/**
 * Represents a Template extension exception.
 * 
 */
public class TemplateException extends Exception
{
	private static final long serialVersionUID = 3269898034453105082L;

	public enum State
	{
		ERROR, WARNING
	}

	private State state;


	public State getState()
	{
		return state;
	}

	public void setState(State state)
	{
		this.state = state;
	}

	/**
	 * Constructs a new template exception class.
	 * 
	 * @param message
	 *            displayed message
	 */
	public TemplateException(String message)
	{
		super(message);

		this.state = State.ERROR;
	}

	/**
	 * Constructs a new template exception class.
	 * 
	 * @param cause
	 *            what caused of the exception
	 */
	public TemplateException(Throwable cause)
	{
		super(cause);

		this.state = State.ERROR;
	}

}
