/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.patterns;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.ogee.core.extensions.components.Component;
import org.eclipse.ogee.core.extensions.wizardpagesprovider.IWizardPagesProvider;
import org.eclipse.ogee.utils.discovery.StatusUtils;

/**
 * Represents a Pattern.
 */
public abstract class Pattern implements IWizardPagesProvider, IExecutableExtension
{
	// the pattern's components
	protected Map<String, Component> components;

	/**
	 * Constructs a new pattern.
	 */
	public Pattern()
	{
		this.components = new HashMap<String, Component>();
	}

	/**
	 * Sets the initial data of this pattern. This method is invoked after
	 * instantiation with Map<String, Object> as input. It can be used to
	 * initialize this Pattern or its components with data.
	 * 
	 * Specific Pattern implementation can choose to override this method in
	 * order to initialize this Pattern or its components with the input data
	 * 
	 * @param data
	 */
	public void initialize(Map<String, Object> data)
	{

	}


	@Override
	public void setInitializationData(IConfigurationElement patternElement, String propertyName, Object data)
			throws CoreException
	{
		try
		{
			this.components = PatternUtil.createExecutableComponents(patternElement);
		}
		catch (PatternException e)
		{
			throw new CoreException(StatusUtils.statusError(e.getMessage(), e));
		}
	}
}
