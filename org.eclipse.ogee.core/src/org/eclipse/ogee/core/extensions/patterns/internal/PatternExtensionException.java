/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.patterns.internal;

public class PatternExtensionException extends Exception
{

	private static final long serialVersionUID = -4548889942497279742L;

	public PatternExtensionException()
	{

	}

	public PatternExtensionException(String message)
	{
		super(message);
	}

	public PatternExtensionException(Throwable cause)
	{
		super(cause);
	}

}
