/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.patterns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.InvalidRegistryObjectException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ogee.core.Activator;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.components.Component;
import org.eclipse.ogee.core.extensions.components.internal.ComponentExtension;
import org.eclipse.ogee.core.extensions.patterns.internal.PatternExtensionException;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.osgi.util.NLS;

/**
 * A utility class for the Pattern.
 */
public class PatternUtil
{
	private static final String COMPONENT = ".component"; //$NON-NLS-1$

	/**
	 * Returns component element ids of the given pattern element.
	 * 
	 * @param patternElement
	 *            pattern element
	 * @return component element ids of the given pattern element
	 * @throws PatternExtensionException
	 *             if patternElement is null
	 */
	public static List<String> getComponentIds(IConfigurationElement patternElement) throws PatternExtensionException
	{
		if (patternElement == null)
		{
			throw new PatternExtensionException(FrameworkMessages.PatternUtil_PatternElement_CouldNOtBeNull);
		}

		List<String> componentIds = new ArrayList<String>();

		IConfigurationElement[] componentIdElements = patternElement.getChildren(ExtensionAttributes.component.name());
		for (IConfigurationElement componentIdElement : componentIdElements)
		{
			componentIds.add(componentIdElement.getAttribute(ExtensionAttributes.componentId.name()));
		}

		return componentIds;
	}

	/**
	 * Returns the available pattern component elements.
	 * 
	 * @param patternElement
	 *            pattern element
	 * @return available pattern component elements
	 * @throws PatternException
	 * @throws ExtensionException
	 * @throws PatternExtensionException
	 *             if patternElement is null
	 */
	private static List<ComponentExtension> getPatternComponentExtensions(IConfigurationElement patternElement)
			throws PatternExtensionException
	{
		try
		{
			if (patternElement == null)
			{
				throw new PatternExtensionException(FrameworkMessages.PatternUtil_PatternElement_CouldNOtBeNull);
			}

			List<ComponentExtension> patternComponentExtensions = new ArrayList<ComponentExtension>();
			// get all available component elements
			IConfigurationElement[] allComponentElements = getAllComponentElements();

			// get pattern component id elements
			List<String> patternComponentIds = getComponentIds(patternElement);
			for (String patternComponentId : patternComponentIds)
			{
				for (IConfigurationElement componentElement : allComponentElements)
				{
					String componentElementId = componentElement.getAttribute(ExtensionAttributes.id.name());
					if (patternComponentId.equals(componentElementId))
					{
						patternComponentExtensions.add(new ComponentExtension(componentElement));
					}
				}
			}

			return patternComponentExtensions;
		}
		catch (InvalidRegistryObjectException e)
		{
			throw new PatternExtensionException(e);
		}
		catch (ExtensionException e)
		{
			throw new PatternExtensionException(e);
		}
	}

	/**
	 * Returns all available component elements.
	 * 
	 * @return all available component elements
	 */
	private static IConfigurationElement[] getAllComponentElements()
	{
		return Platform.getExtensionRegistry().getConfigurationElementsFor(Activator.PLUGIN_ID + COMPONENT);
	}

	/**
	 * Create executable components map where the key is a component id.
	 * Observable components get their observer components.
	 * 
	 * @param componentElements
	 *            component elements
	 * @throws PatternException
	 *             if any component instance cannot be created or observable
	 *             component is not found
	 */
	public static Map<String, Component> createExecutableComponents(IConfigurationElement patternElement)
			throws PatternException
	{
		Map<String, Component> components = new HashMap<String, Component>();

		try
		{
			// get all pattern component ids
			List<String> patternComponentIds = getComponentIds(patternElement);
			// get all available pattern component extensions
			List<ComponentExtension> patternComponentExtensions = getPatternComponentExtensions(patternElement);
			// if all needed components were found
			if (patternComponentExtensions.size() == patternComponentIds.size())
			{
				for (ComponentExtension patternComponentExtension : patternComponentExtensions)
				{
					Component component = createComponent(patternComponentExtension, patternComponentExtensions);
					// get component id
					String componentId = patternComponentExtension.getId();
					// save component in pattern
					components.put(componentId, component);
				}
			}
		}
		catch (PatternExtensionException e)
		{
			throw new PatternException(e);
		}

		return components;
	}

	/**
	 * Creates a component for this pattern.
	 * 
	 * @param componentExtensions
	 *            - list of component extensions.
	 * @param patternComponentExtension
	 *            - the pattern component extension.
	 * @return - a Component object.
	 * @throws ExtensionException
	 */
	private static Component createComponent(ComponentExtension patternComponentExtension,
			List<ComponentExtension> componentExtensions) throws PatternExtensionException
	{
		validateComponentOutputModel(patternComponentExtension);

		Component component;
		try
		{
			// get component executable
			component = patternComponentExtension.getComponent();
			// add observers to observable components if needed
			for (ComponentExtension componentExtension : componentExtensions)
			{
				if (patternComponentExtension == componentExtension)
				{
					continue;
				}

				// get output model class
				String outputModelClass = patternComponentExtension.getOutputModelClass();
				// get input models
				List<String> inputModels = componentExtension.getInputModels();
				if (inputModels.contains(outputModelClass))
				{
					Component observerComponent = componentExtension.getComponent();
					component.addObserverComponent(observerComponent);
				}
			}
		}
		catch (ExtensionException e)
		{
			throw new PatternExtensionException(e);
		}

		return component;
	}

	/**
	 * Validates the component's output model.
	 * 
	 * @param componentExtension
	 * @throws PatternExtensionException
	 */
	private static void validateComponentOutputModel(ComponentExtension componentExtension)
			throws PatternExtensionException
	{
		String outputModelClass = null;

		try
		{
			outputModelClass = componentExtension.getOutputModelClass();

			Component component = componentExtension.getComponent();
			Class<? extends Component> componentClass = component.getClass();

			boolean validComponentClass = validComponentClass(componentClass, outputModelClass);
			if (validComponentClass)
			{
				return;
			}
		}
		catch (ExtensionException e)
		{
			throw new PatternExtensionException(e);
		}

		throw new PatternExtensionException(NLS.bind(FrameworkMessages.PatternUtil_Component_MustBeOfType,
				componentExtension, outputModelClass));
	}


	private static boolean validComponentClass(Class<?> theClass, String theInterface)
	{
		Class<?>[] classInterfaces = theClass.getInterfaces();
		for (Class<?> componentInterface : classInterfaces)
		{
			if (theInterface.equals(componentInterface.getName()))
			{
				return true;
			}
		}

		Class<?> superClass = theClass.getSuperclass();
		boolean validClass = validComponentClass(superClass, theInterface);
		if (validClass)
		{
			return true;
		}

		return false;
	}
}
