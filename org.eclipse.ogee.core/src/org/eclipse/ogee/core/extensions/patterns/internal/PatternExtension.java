/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.patterns.internal;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.ogee.core.extensions.Extension;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.patterns.Pattern;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;

/**
 * Represents a Pattern Extension.
 */
public class PatternExtension extends Extension
{
	private Pattern pattern;
	private List<String> componentIds;

	/**
	 * Constructs a new Pattern Extension with the given IConfigurationElement.
	 * 
	 * @param configElement
	 * @throws ExtensionException
	 */
	public PatternExtension(IConfigurationElement configElement) throws ExtensionException
	{
		super(configElement);

		this.componentIds = new ArrayList<String>();

		IConfigurationElement[] components = configElement.getChildren(ExtensionAttributes.component.name());
		if (components == null || components.length == 0)
		{
			throw new ExtensionException(FrameworkMessages.PatternExtension_Pattern_Must_Contain_At_Least_One_Component);
		}

		for (IConfigurationElement observablePattern : components)
		{
			this.componentIds.add(observablePattern.getAttribute(ExtensionAttributes.componentId.name()));
		}
	}

	/**
	 * @return - a list of the component's ids.
	 */
	public List<String> getComponentIds()
	{
		return this.componentIds;
	}

	/**
	 * Get Pattern Extension.
	 * 
	 * @return - the Pattern class of the pattern extension, or an exception is
	 *         thrown (null is never returned).
	 * @throws ExtensionException
	 *             - if the extension was null, or not an instance of an Pattern
	 *             class, or something went wrong :)
	 */
	public Pattern getPattern() throws ExtensionException
	{
		try
		{
			Object obj = this.configElement.createExecutableExtension(ExtensionAttributes.Pattern.name());
			if (obj == null)
			{
				throw new ExtensionException(FrameworkMessages.PatternExecutableExtensionCreationFailure);
			}

			if (obj instanceof Pattern)
			{
				this.pattern = (Pattern) obj;

				return this.pattern;
			}

			throw new ExtensionException(FrameworkMessages.PatternExecutableExtensionCreationFailure);
		}
		catch (CoreException e)
		{
			// we want the exception to be shown in the UI,
			// so here we only send it upward
			throw new ExtensionException(e);
		}
	}
}
