/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.wizardpagesprovider;

import java.util.SortedSet;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ogee.core.extensions.visible.IVisible;

public class TemplateContentProvider implements IStructuredContentProvider
{
	private SortedSet<IVisible> templates;

	@SuppressWarnings("unchecked")
	public TemplateContentProvider(SortedSet<? extends IVisible> sortedSet)
	{
		this.templates = (SortedSet<IVisible>) sortedSet;
	}

	@Override
	public void dispose()
	{

	}

	@Override
	public void inputChanged(Viewer viewer, Object paramObject1, Object paramObject2)
	{

	}

	@Override
	public Object[] getElements(Object paramObject)
	{
		return this.templates.toArray();
	}
}
