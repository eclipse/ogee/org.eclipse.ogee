/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.wizardpagesprovider;


import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.swt.graphics.Image;

public class TemplateLabelProvider implements ITableLabelProvider
{

	private static final int TEXT_COLUMN_INDEX = 0;

	@Override
	public void addListener(ILabelProviderListener paramILabelProviderListener)
	{
		// do nothing
	}

	@Override
	public void dispose()
	{
		// do nothing

	}

	@Override
	public boolean isLabelProperty(Object paramObject, String paramString)
	{
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener paramILabelProviderListener)
	{
		// do nothing
	}

	@Override
	public Image getColumnImage(Object element, int columnIndex)
	{
		if (columnIndex == TEXT_COLUMN_INDEX)
		{
			URL imageUrl = ((IVisible) element).getIcon();
			ImageDescriptor imageDescriptor = ImageDescriptor.createFromURL(imageUrl);
			return imageDescriptor.createImage();

		}
		else
		{
			Logger.getUtilsLogger().logError("Invalid column index"); //$NON-NLS-1$
			return null;
		}
	}

	@Override
	public String getColumnText(Object element, int columnIndex)
	{
		assert element instanceof IVisible;

		if (columnIndex == TEXT_COLUMN_INDEX)
		{
			return "" + ((IVisible) element).getDisplayName(); //$NON-NLS-1$
		}

		else
		{
			Logger.getUtilsLogger().logError("Invalid column index"); //$NON-NLS-1$
			return null;
		}
	}


}
