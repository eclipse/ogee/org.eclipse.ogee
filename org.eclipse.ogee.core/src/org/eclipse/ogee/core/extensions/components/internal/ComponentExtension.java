/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.components.internal;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.ogee.core.extensions.Extension;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.components.Component;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;

/**
 * Represents a Component Extension.
 */
public class ComponentExtension extends Extension
{
	private Component component;
	private List<String> inputModels;
	private String outputModelClass;

	/**
	 * Constructs a new Component Extension with the given
	 * IConfigurationElement.
	 * 
	 * @param configElement
	 * @throws ExtensionException
	 */
	public ComponentExtension(IConfigurationElement configElement) throws ExtensionException
	{
		super(configElement);

		this.inputModels = new ArrayList<String>();

		IConfigurationElement[] inputModelElements = configElement.getChildren(ExtensionAttributes.inputModel.name());
		if (inputModelElements != null)
		{
			for (IConfigurationElement inputModelElement : inputModelElements)
			{
				this.inputModels.add(inputModelElement.getAttribute(ExtensionAttributes.modelClass.name()));
			}
		}

		this.outputModelClass = configElement.getAttribute(ExtensionAttributes.outputModelClass.name());
		validateStringInput(this.outputModelClass, "outputModelClass"); //$NON-NLS-1$
	}

	/**
	 * @return - the input models of this component extension.
	 */
	public List<String> getInputModels()
	{
		return this.inputModels;
	}

	/**
	 * @return - the output model class of this component extension.
	 */
	public String getOutputModelClass()
	{
		return this.outputModelClass;
	}


	/**
	 * @return - the Component class of the component extension, or an exception
	 *         is thrown (null is never returned).
	 * @throws ExtensionException
	 *             - if the extension was null, or not an instance of an
	 *             Component class, or something went wrong :)
	 */
	public Component getComponent() throws ExtensionException
	{
		if (this.component == null)
		{
			try
			{
				Object obj = this.configElement.createExecutableExtension(ExtensionAttributes.Component.name());
				if (obj == null)
				{
					throw new ExtensionException(FrameworkMessages.ComponentExecutableExtensionCreationFailure);
				}

				if (obj instanceof Component)
				{
					this.component = (Component) obj;

					return this.component;
				}

				throw new ExtensionException(FrameworkMessages.ComponentExecutableExtensionCreationFailure);
			}
			catch (CoreException e)
			{
				// we want the exception to be shown in the UI,
				// so here we only send it upward
				throw new ExtensionException(e);
			}
		}

		return this.component;
	}
}
