/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.components;

import java.util.Map;

import org.eclipse.ogee.core.extensions.wizardpagesprovider.IWizardPagesProvider;

/**
 * Represents a Component.
 */
public abstract class Component extends ObservableComponent implements ComponentObserver, IWizardPagesProvider
{
	/**
	 * Component constructor
	 */
	public Component()
	{
	}

	/**
	 * Sets the initial data of this component. This method is part of an
	 * external API to the wizard (for example the search console).
	 * 
	 * @param data
	 */
	public void initialize(Map<String, Object> data)
	{

	}
}
