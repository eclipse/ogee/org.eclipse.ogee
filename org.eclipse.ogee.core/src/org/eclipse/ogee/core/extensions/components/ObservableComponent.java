/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.components;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ogee.core.nls.messages.FrameworkMessages;


public class ObservableComponent
{
	private boolean observableChanged;
	private List<ComponentObserver> observerComponents;


	public ObservableComponent()
	{
		this.observableChanged = false;

		this.observerComponents = new ArrayList<ComponentObserver>();
	}


	public synchronized void addObserverComponent(ComponentObserver observer)
	{
		if (observer == null)
		{
			throw new IllegalArgumentException(FrameworkMessages.ObservableComponent_0);
		}

		if (this.observerComponents.contains(observer))
		{
			return;
		}

		this.observerComponents.add(observer);
	}


	public void notifyComponentObservers(Object object)
	{
		ComponentObserver[] observers;

		synchronized (this)
		{
			if (!this.observableChanged)
			{
				return;
			}

			observers = this.observerComponents.toArray(new ComponentObserver[this.observerComponents.size()]);
			clearObservableChanged();
		}

		for (int i = observers.length - 1; i >= 0; i--)
		{
			observers[i].update(object);
		}
	}


	public synchronized void setObservableChanged()
	{
		this.observableChanged = true;
	}


	public synchronized void clearObservableChanged()
	{
		this.observableChanged = false;
	}


	public synchronized boolean hasObservableChanged()
	{
		return this.observableChanged;
	}


	public synchronized int countComponentObservers()
	{
		return this.observerComponents.size();
	}
}
