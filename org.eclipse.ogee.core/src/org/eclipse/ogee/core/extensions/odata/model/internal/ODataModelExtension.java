/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.odata.model.internal;

import java.net.URL;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.ogee.core.extensions.Extension;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.odata.model.IODataModelProvider;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.ogee.core.extensions.visible.VisibleExtension;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;

/**
 * Represents a OData Model Extension.
 */
public class ODataModelExtension extends Extension implements IVisibleExtension
{
	private VisibleExtension visibleExtension;
	private IODataModelProvider odataModelProvider;
	private WizardType wizardType;
	private String sortCategory;

	/**
	 * Constructs a new OData Model Extension with a given
	 * IConfigurationElement.
	 * 
	 * @param odataModelConfigElement
	 * @throws ExtensionException
	 */
	public ODataModelExtension(IConfigurationElement odataModelConfigElement) throws ExtensionException
	{
		super(odataModelConfigElement);

		this.visibleExtension = new VisibleExtension(odataModelConfigElement, this.id);

		String wizardType = odataModelConfigElement.getAttribute(ExtensionAttributes.wizardType.name());
		this.wizardType = WizardType.valueOf(wizardType);
		this.sortCategory = odataModelConfigElement.getAttribute(ExtensionAttributes.sortCategory.name());
		if (this.sortCategory == null)
		{ // default category if nothing was selected
			this.sortCategory = "2"; //$NON-NLS-1$
		}
	}

	/**
	 * Get the OData Model Extension.
	 * 
	 * @return ODataModelCreator
	 * @throws ExtensionException
	 */
	public IODataModelProvider getODataModelProvider() throws ExtensionException
	{
		// only creates it once
		if (this.odataModelProvider == null)
		{
			try
			{
				// creates and returns a new instance of the executable
				// extension
				// identified by the odata model name
				Object obj = this.configElement
						.createExecutableExtension(ExtensionAttributes.ODataModelProvider.name());
				if (obj == null)
				{
					throw new ExtensionException(FrameworkMessages.ODataModelExecutableExtensionCreationFailure);
				}

				if (obj instanceof IODataModelProvider)
				{
					this.odataModelProvider = (IODataModelProvider) obj;
					return this.odataModelProvider;
				}

				throw new ExtensionException(FrameworkMessages.ODataModelExecutableExtensionCreationFailure);
			}
			catch (CoreException e)
			{
				throw new ExtensionException(e);
			}
		}

		return this.odataModelProvider;

	}

	@Override
	public int compareTo(IVisible o)
	{
		return this.visibleExtension.compareTo(o);
	}

	@Override
	public String getDisplayName()
	{
		return this.visibleExtension.getDisplayName();
	}

	@Override
	public URL getIcon()
	{
		return this.visibleExtension.getIcon();
	}

	@Override
	public String getDescription()
	{
		return this.visibleExtension.getDescription();
	}

	@Override
	public String toString()
	{
		return super.toString() + this.visibleExtension.toString();
	}

	public WizardType getWizardType()
	{
		return this.wizardType;
	}

	@Override
	public String getSortCategory()
	{
		return this.sortCategory;
	}


}
