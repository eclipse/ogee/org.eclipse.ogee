/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.odata.model;

import org.eclipse.ogee.core.extensions.wizardpagesprovider.IWizardPagesProvider;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.model.odata.EDMXSet;

/**
 * Represents the OData Model extension of the framework.
 */
public interface IODataModelProvider extends IWizardPagesProvider
{
	/**
	 * Creates OData model EDMXSet object.
	 * 
	 * @return EDMXSet of the service
	 */
	public EDMXSet createModel() throws BuilderException;
}
