/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.odata.model.internal;

import org.eclipse.ogee.core.extensions.visible.IVisible;

public interface IVisibleExtension extends IVisible
{
	/**
	 * Gets the sort category of the extension.
	 * 
	 * @return the sort category of the extension.
	 */
	public String getSortCategory();
}
