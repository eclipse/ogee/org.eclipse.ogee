/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions;

/**
 * Represents an Extension Exception
 */
public class ExtensionException extends Exception
{
	private static final long serialVersionUID = 5966036854470233830L;

	/**
	 * Empty constructor.
	 */
	public ExtensionException()
	{

	}

	/**
	 * Constructs a new ExtensionException.
	 * 
	 * @param message
	 *            - the message of this exception.
	 */
	public ExtensionException(String message)
	{
		super(message);
	}

	/**
	 * Constructs a new ExtensionException.
	 * 
	 * @param cause
	 *            - the cause of this exception.
	 */
	public ExtensionException(Throwable cause)
	{
		super(cause);
	}
	
}
