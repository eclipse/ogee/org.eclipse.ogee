/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.osgi.util.NLS;

/**
 * Represents an Extension.
 */
public abstract class Extension
{
	protected String id;
	protected IConfigurationElement configElement;

	/**
	 * Constructs a new Extension.
	 * 
	 * @param configElement
	 *            - the data from Eclipse.
	 * @throws ExtensionException
	 *             - if configElement is null.
	 */
	public Extension(IConfigurationElement configElement) throws ExtensionException
	{
		if (configElement == null)
		{
			throw new ExtensionException(FrameworkMessages.Extension_ConfigElementNotNull);
		}

		this.configElement = configElement;

		this.id = configElement.getAttribute(ExtensionAttributes.id.name());
		validateStringInput(this.id, FrameworkMessages.Extension_ExtensionId);
	}

	// Validates the user input value
	public static void validateStringInput(String inputValue, String inputName) throws ExtensionException
	{
		if (inputValue == null)
		{
			throw new ExtensionException(NLS.bind(FrameworkMessages.Extension_NotNull, inputName));
		}

		inputValue = inputValue.trim();
		if (inputValue.isEmpty())
		{
			throw new ExtensionException(NLS.bind(FrameworkMessages.Extension_NotEmpty, inputName));
		}
	}

	/**
	 * 
	 * @return the ConfigurationElement.
	 */
	public IConfigurationElement getConfigurationElement()
	{
		return this.configElement;
	}

	/**
	 * 
	 * @return the id of the extension
	 */
	public String getId()
	{
		return this.id;
	}

	@Override
	public String toString()
	{
		String details = "\nid - " + this.id; //$NON-NLS-1$

		return details;
	}

	/**
	 * @return a hash code value for the object.
	 */
	@Override
	public int hashCode()
	{
		return this.id.hashCode();
	}

	/**
	 * Indicates whether some other object is "equal to" this one.
	 * 
	 * @param obj
	 *            the reference object with which to compare.
	 * @return true if this object is the same as the obj argument; false
	 *         otherwise.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
		{
			return false;
		}

		if (this == obj)
		{
			return true;
		}

		if (obj instanceof Extension)
		{
			Extension other = (Extension) obj;

			return this.id.equals(other.getId());
		}

		return false;
	}
}
