/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.service.implementation;

public class ServiceImplementationException extends Exception
{
	private static final long serialVersionUID = 7495760549265637103L;

	public ServiceImplementationException(Throwable cause)
	{
		super(cause);
	}

	public ServiceImplementationException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
