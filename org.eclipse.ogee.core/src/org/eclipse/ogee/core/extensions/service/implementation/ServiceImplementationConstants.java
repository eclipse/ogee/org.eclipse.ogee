/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.service.implementation;

import org.eclipse.ogee.model.odata.EDMXSet;

public enum ServiceImplementationConstants
{
	/**
	 * Project name.
	 * <p>
	 * The value object is {@link String}.
	 */
	PROJECT_NAME,

	/**
	 * OData File path.
	 * <p>
	 * The value object is {@link String}.
	 */
	ODATA_FILE_PATH,

	/**
	 * OData model edmx set object.
	 * <p>
	 * The value object is {@link EDMXSet}.
	 */
	ODATA_MODEL_EDMX_SET
}
