/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.managers;

import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.spi.RegistryContributor;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.templates.TemplateType;
import org.eclipse.ogee.core.extensions.templates.internal.TemplateExtension;
import org.eclipse.ogee.core.extensions.templates.internal.VisibleTemplateExtension;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.swt.widgets.Shell;

/**
 * Represents an Extension Manager for Starter Application (Singleton).
 */
public class StarterAppExtensionsManager extends ExtensionsManager
{
	// private constructor
	public StarterAppExtensionsManager(Shell shell) throws ExtensionsManagerException
	{
		super(shell);

		this.type = TemplateType.Starter_Application;

		// loading the extensions from the plugin.xml
		loadExtenions();
	}

	@Override
	public void setSelectedTemplate(String templateId) throws ExtensionsManagerException
	{
		super.setSelectedTemplate(templateId);
	}

	/**
	 * Sorts the set of template extensions.
	 * 
	 * @return Template Extensions
	 * @throws ExtensionsManagerException
	 */
	public SortedSet<? extends IVisible> getTemplates() throws ExtensionsManagerException
	{
		try
		{
			// we sort the set only when the user requests it from the UI
			SortedSet<VisibleTemplateExtension> sortedSATemplates = new TreeSet<VisibleTemplateExtension>();

			for (TemplateExtension templateExtension : this.templateExtensions)
			{
				String environmentId = this.selectedEnvExtension.getId();
				if (templateExtension.getEnvironmentId().equals(environmentId))
				{
					VisibleTemplateExtension saTemplate = new VisibleTemplateExtension(
							templateExtension.getConfigurationElement());
					sortedSATemplates.add(saTemplate);
				}
			}

			return sortedSATemplates;
		}
		catch (ExtensionException e)
		{
			throw new ExtensionsManagerException(e);
		}
	}


	/**
	 * 
	 * @return
	 * @throws ExtensionsManagerException
	 */
	public IVisible getTemplate() throws ExtensionsManagerException
	{
		try
		{
			VisibleTemplateExtension saTemplate = new VisibleTemplateExtension(
					this.selectedTemplateExtension.getConfigurationElement());

			return saTemplate;
		}
		catch (ExtensionException e)
		{
			throw new ExtensionsManagerException(e);
		}
	}

	/**
	 * 
	 * @return the name of the selected environment plugin
	 */
	public String getSelectedEnvironmentContainingPlugin()
	{
		if (this.selectedEnvExtension == null)
		{
			return null;
		}

		IConfigurationElement configurationElement = this.selectedEnvExtension.getConfigurationElement();

		IExtension parent = (IExtension) configurationElement.getParent();
		RegistryContributor contributor = (RegistryContributor) parent.getContributor();
		String actualName = contributor.getActualName();

		return actualName;
	}
}
