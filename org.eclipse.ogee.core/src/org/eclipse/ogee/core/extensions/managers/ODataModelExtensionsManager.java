/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.managers;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ogee.core.Activator;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.odata.model.internal.ODataModelExtension;
import org.eclipse.ogee.core.extensions.odata.model.internal.WizardType;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.osgi.util.NLS;


public class ODataModelExtensionsManager
{
	private static final String ODATA_MODEL = ".serviceModel"; //$NON-NLS-1$
	private static final String NEW_LINE = "\n"; //$NON-NLS-1$
	private IConfigurationElement[] odataModelConfigElements;
	private Set<ODataModelExtension> allWizardExtensions;
	private Set<ODataModelExtension> newWizardExtensions;
	private Set<ODataModelExtension> importWizardExtensions;


	public ODataModelExtensionsManager() throws ExtensionsManagerException
	{
		this.allWizardExtensions = new HashSet<ODataModelExtension>();
		this.newWizardExtensions = new HashSet<ODataModelExtension>();
		this.importWizardExtensions = new HashSet<ODataModelExtension>();
		// loading the odata model extensions from the plugin.xml
		loadODataModelExtenions();
	}

	private void loadODataModelExtenions() throws ExtensionsManagerException
	{
		// Retrieves the odata model extensions from the plugin.xml
		this.odataModelConfigElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
				Activator.PLUGIN_ID + ODATA_MODEL);

		for (IConfigurationElement odataModelConfigElement : this.odataModelConfigElements)
		{
			if (odataModelConfigElement == null)
			{
				throw new ExtensionsManagerException(FrameworkMessages.OdataModelElementNull);
			}

			try
			{
				ODataModelExtension odataModelExtension = new ODataModelExtension(odataModelConfigElement);
				if (odataModelExtension.getWizardType().equals(WizardType.all))
				{
					this.allWizardExtensions.add(odataModelExtension);
					this.importWizardExtensions.add(odataModelExtension);
					this.newWizardExtensions.add(odataModelExtension);
				}
				else if (odataModelExtension.getWizardType().equals(WizardType.newWizard))
				{
					this.newWizardExtensions.add(odataModelExtension);
					this.allWizardExtensions.add(odataModelExtension);
				}
				else if (odataModelExtension.getWizardType().equals(WizardType.importWizard))
				{
					this.importWizardExtensions.add(odataModelExtension);
					this.allWizardExtensions.add(odataModelExtension);
				}
			}
			catch (ExtensionException e)
			{
				String id = odataModelConfigElement.getAttribute(ExtensionAttributes.id.name());
				Logger.getFrameworkLogger().logWarning(
						e.getMessage() + NEW_LINE
								+ NLS.bind(FrameworkMessages.ODataModelExecutableExtensionIDCreationFailure, id));
			}
		}
	}

	/**
	 * @return the odata model extensions which will appear in all the wizards
	 * @throws ExtensionsManagerException
	 */
	public Set<ODataModelExtension> getAllWizardExtensions() throws ExtensionsManagerException
	{
		return this.allWizardExtensions;
	}

	/**
	 * @return the odata model extensions which will appear in New Wizard
	 * @throws ExtensionsManagerException
	 */
	public Set<ODataModelExtension> getNewWizardExtensions() throws ExtensionsManagerException
	{
		return this.newWizardExtensions;
	}

	/**
	 * @return the odata model extensions which will appear in Import Wizard
	 * @throws ExtensionsManagerException
	 */
	public Set<ODataModelExtension> getImportWizardExtensions() throws ExtensionsManagerException
	{
		return this.importWizardExtensions;
	}
}
