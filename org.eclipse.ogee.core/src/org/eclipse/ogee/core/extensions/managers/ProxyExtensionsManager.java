/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.spi.RegistryContributor;
import org.eclipse.ogee.core.extensions.templates.TemplateType;
import org.eclipse.ogee.core.extensions.templates.internal.OutputLocation;
import org.eclipse.ogee.core.extensions.templates.internal.TemplateExtension;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;


/**
 * Represents an Extension Manager for Proxy (Singleton).
 */
public class ProxyExtensionsManager extends ExtensionsManager
{
	/**
	 * Constructor
	 * 
	 * @throws ExtensionsManagerException
	 */
	public ProxyExtensionsManager(Shell shell) throws ExtensionsManagerException
	{
		super(shell);

		this.type = TemplateType.Proxy;

		// loading the extensions from the plugin.xml
		loadExtenions();
	}


	/**
	 * Filters the loaded templates and environments and saves them.
	 * 
	 * @throws ExtensionsManagerException
	 */
	@Override
	protected void filterAndSave() throws ExtensionsManagerException
	{
		super.filterAndSave();

		removeDuplicateTemplates();
	}


	@Override
	public void setSelectedEnvironment(String environmentId) throws ExtensionsManagerException
	{
		super.setSelectedEnvironment(environmentId);

		for (TemplateExtension templateExtension : this.templateExtensions)
		{
			if (templateExtension.getEnvironmentId().equals(environmentId))
			{
				this.selectedTemplateExtension = templateExtension;
				setSelectedTemplateDependencies(); // TODO: try to refactor
				break;
			}
		}
	}


	/**
	 * removes the proxy templates that have the same environment (id)
	 */
	private void removeDuplicateTemplates()
	{
		Map<String, List<TemplateExtension>> duplicatesMap = createDuplicateEnvironmentMap();

		updateTemplateExtensions(duplicatesMap);
	}


	/**
	 * the removal itself...
	 * 
	 * @param duplicatesMap
	 */
	private void updateTemplateExtensions(Map<String, List<TemplateExtension>> duplicatesMap)
	{
		// now we really remove the duplicates:
		// going over all the keys in the map (all environmentIds)
		for (String environmentId : duplicatesMap.keySet())
		{
			List<TemplateExtension> temp = duplicatesMap.get(environmentId);
			if (temp.size() > 1)
			{
				// and remove the rest
				int size = temp.size();
				for (int i = 1; i < size; i++) // **starting at index = 1
				{
					TemplateExtension templateExtension = temp.get(i);
					this.templateExtensions.remove(templateExtension);
					Logger.getFrameworkLogger().logWarning(
							NLS.bind(FrameworkMessages.ProxyExtensionsManager_DuplicateProxyTemplateExt,
									templateExtension.getId(), environmentId));
				}
			}
		}
	}


	/**
	 * creates the duplicates hash map
	 * 
	 * @return
	 */
	private Map<String, List<TemplateExtension>> createDuplicateEnvironmentMap()
	{
		// creating a map of <environmentId,all TE with that environmentId>
		Map<String, List<TemplateExtension>> duplicatesMap = new HashMap<String, List<TemplateExtension>>();
		// going over each proxyTemplateExtension in the set
		for (TemplateExtension proxyTemplateExtension : this.templateExtensions)
		{
			// extracting its environmentId
			String environmentId = proxyTemplateExtension.getEnvironmentId();
			// getting all of this environmentId templates
			List<TemplateExtension> allEnvIdTemplates = duplicatesMap.get(environmentId);
			if (allEnvIdTemplates == null)
			{
				// the current environmentId doesn't exist in the map, need to
				// put it
				// with a new list of TE
				allEnvIdTemplates = new ArrayList<TemplateExtension>();
				duplicatesMap.put(environmentId, allEnvIdTemplates);
			}
			// add the current TE to this environmentId's list of templates
			allEnvIdTemplates.add(proxyTemplateExtension);
		}

		return duplicatesMap;
	}

	/**
	 * @return - the output location of the proxy.
	 */
	public OutputLocation getOutputLocation()
	{
		if (this.selectedTemplateExtension == null)
		{
			return null;
		}

		return this.selectedTemplateExtension.getOutputLocation();
	}

	/**
	 * 
	 * @return the name of the selected environment plugin
	 */
	public String getSelectedEnvironmentContainingPlugin()
	{
		if (this.selectedEnvExtension == null)
		{
			return null;
		}

		IConfigurationElement configurationElement = this.selectedEnvExtension.getConfigurationElement();

		IExtension parent = (IExtension) configurationElement.getParent();
		RegistryContributor contributor = (RegistryContributor) parent.getContributor();
		String actualName = contributor.getActualName();

		return actualName;
	}
}
