/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.extensions.managers;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ogee.core.Activator;
import org.eclipse.ogee.core.extensions.ExtensionAttributes;
import org.eclipse.ogee.core.extensions.ExtensionException;
import org.eclipse.ogee.core.extensions.environments.Environment;
import org.eclipse.ogee.core.extensions.environments.internal.EnvironmentExtension;
import org.eclipse.ogee.core.extensions.patterns.Pattern;
import org.eclipse.ogee.core.extensions.patterns.internal.PatternExtension;
import org.eclipse.ogee.core.extensions.templates.Template;
import org.eclipse.ogee.core.extensions.templates.TemplateConstants;
import org.eclipse.ogee.core.extensions.templates.TemplateType;
import org.eclipse.ogee.core.extensions.templates.internal.TemplateExtension;
import org.eclipse.ogee.core.extensions.visible.IVisible;
import org.eclipse.ogee.core.nls.messages.FrameworkMessages;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;


/**
 * Represents an Extension Manager, that loads the extensions, filters and saves
 * them.
 */
public abstract class ExtensionsManager
{
	// extension point scheme names
	private static final String ENVIRONMENT = ".environment"; //$NON-NLS-1$
	private static final String TEMPLATE = ".template"; //$NON-NLS-1$
	private static final String PATTERN = ".pattern"; //$NON-NLS-1$

	private static final String NEW_LINE = "\n"; //$NON-NLS-1$

	protected IConfigurationElement[] envConfigElements;
	protected IConfigurationElement[] templateConfigElements;
	protected IConfigurationElement[] patternConfigElements;

	// the environment and template selected by the user
	protected EnvironmentExtension selectedEnvExtension;
	protected TemplateExtension selectedTemplateExtension;
	protected Pattern selectedPattern;

	protected SortedSet<EnvironmentExtension> envExtensions;
	protected Set<TemplateExtension> templateExtensions;

	protected TemplateType type;
	protected Shell shell;


	/**
	 * Constructs a new ExtensionsManager.
	 */
	public ExtensionsManager(Shell shell)
	{
		this.shell = shell;

		this.envExtensions = new TreeSet<EnvironmentExtension>();
		this.templateExtensions = new HashSet<TemplateExtension>();
	}

	/**
	 * Sets the selected Environment Extension.
	 * 
	 * @param environmentId
	 *            - environment id, comes from the UI.
	 * @throws ExtensionsManagerException
	 */
	public void setSelectedEnvironment(String environmentId) throws ExtensionsManagerException
	{
		validateStringInput(environmentId, FrameworkMessages.ExtensionsManager_EnvironmentId);

		boolean envNotFound = true;
		for (EnvironmentExtension envExtension : this.envExtensions)
		{
			if (envExtension == null)
			{
				continue;
			}

			// check that such environment really exists
			if (envExtension.getId().equals(environmentId))
			{
				this.selectedEnvExtension = envExtension;
				envNotFound = false;
				break;
			}
		}

		if (envNotFound)
		{
			throw new ExtensionsManagerException(NLS.bind(
					FrameworkMessages.ExtensionsManager_Could_Not_Find_Environment, environmentId));
		}
	}

	/**
	 * @return - the template's wizard pages.
	 * @throws ExtensionsManagerException
	 */
	public List<IWizardPage> getPatternAndTemplatePages() throws ExtensionsManagerException
	{
		try
		{
			List<IWizardPage> wizardPages = new ArrayList<IWizardPage>();

			if (this.selectedTemplateExtension == null)
			{
				return wizardPages;
			}

			if (this.selectedPattern != null)
			{
				List<IWizardPage> patternWizardPages = this.selectedPattern.getWizardPages();
				if (patternWizardPages != null)
				{
					wizardPages.addAll(patternWizardPages);
				}
			}

			List<IWizardPage> templateWizardPages = this.selectedTemplateExtension.getTemplate().getWizardPages();
			if (templateWizardPages != null)
			{
				wizardPages.addAll(templateWizardPages);
			}

			return wizardPages;
		}
		catch (ExtensionException e)
		{
			throw new ExtensionsManagerException(e);
		}
	}

	/**
	 * @return - the template's pattern wizard pages.
	 * @throws ExtensionsManagerException
	 */
	public List<IWizardPage> getPatternPages()
	{
		List<IWizardPage> wizardPages = new ArrayList<IWizardPage>();

		if (this.selectedTemplateExtension == null)
		{
			return wizardPages;
		}

		if (this.selectedPattern != null)
		{
			List<IWizardPage> patternWizardPages = this.selectedPattern.getWizardPages();
			if (patternWizardPages != null)
			{
				wizardPages.addAll(patternWizardPages);
			}
		}

		return wizardPages;
	}


	/**
	 * @return - the selected environment extension.
	 */
	public IVisible getEnvironment()
	{
		return this.selectedEnvExtension;
	}

	/**
	 * Sets the selected template dependencies.
	 * 
	 * @throws ExtensionsManagerException
	 */
	protected void setSelectedTemplateDependencies() throws ExtensionsManagerException
	{
		try
		{
			Template template = this.selectedTemplateExtension.getTemplate();

			Environment environment = this.selectedEnvExtension.getEnvironment();

			this.selectedPattern = getSelectedPatternFromTemplate(this.selectedTemplateExtension);

			EnumMap<TemplateConstants, Object> context = new EnumMap<TemplateConstants, Object>(TemplateConstants.class);
			context.put(TemplateConstants.ENVIRONMENT, environment);
			context.put(TemplateConstants.PATTERN, this.selectedPattern);
			context.put(TemplateConstants.SHELL, this.shell);

			template.setContext(context);
		}
		catch (ExtensionException e)
		{
			throw new ExtensionsManagerException(e);
		}
	}


	/**
	 * @return - the selected template.
	 * @throws ExtensionsManagerException
	 */
	public Template getSelectedTemplate() throws ExtensionsManagerException
	{
		try
		{
			if (this.selectedTemplateExtension == null)
			{
				return null;
			}

			return this.selectedTemplateExtension.getTemplate();
		}
		catch (ExtensionException e)
		{
			throw new ExtensionsManagerException(e);
		}
	}


	/**
	 * @return the selected environment.
	 * @throws ExtensionsManagerException
	 */
	public Environment getSelectedEnvironment() throws ExtensionsManagerException
	{
		try
		{
			if (this.selectedEnvExtension == null)
			{
				return null;
			}

			return this.selectedEnvExtension.getEnvironment();
		}
		catch (ExtensionException e)
		{
			throw new ExtensionsManagerException(e);
		}
	}


	/**
	 * @return the selected Pattern.
	 * @throws ExtensionsManagerException
	 */
	public Pattern getSelectedPattern() throws ExtensionsManagerException
	{
		return this.selectedPattern;
	}

	/**
	 * @return the selected Pattern from TemplateExtension .
	 * 
	 *         If the pattern was not created yet, this method creates it.
	 * 
	 * @throws ExtensionsManagerException
	 */
	private Pattern getSelectedPatternFromTemplate(TemplateExtension templateExtention)
			throws ExtensionsManagerException
	{
		if (templateExtention == null)
		{
			return null;
		}
		String patternId = ""; //$NON-NLS-1$
		Pattern pattern = null;

		try
		{
			Template template = templateExtention.getTemplate();
			if (template != null)
			{
				EnumMap<TemplateConstants, Object> context = templateExtention.getTemplate().getContext();
				if (context != null)
				{
					pattern = (Pattern) context.get(TemplateConstants.PATTERN);
					return pattern;
				}
			}

			// pattern for this template was not created yet.
			patternId = templateExtention.getPatternId();
			if (patternId == null || patternId.isEmpty())
			{
				return null;
			}


			for (PatternExtension pExtension : createTemporaryPatternExtensionsList())
			{
				if (patternId.equals(pExtension.getId()))
				{
					return pExtension.getPattern();
				}
			}

		}
		catch (ExtensionException e)
		{
			throw new ExtensionsManagerException(NLS.bind(FrameworkMessages.ExtensionsManager_Could_Not_Find_Pattern,
					patternId, this.selectedTemplateExtension.getId()), e);
		}

		return pattern;
	}


	/**
	 * Validates the user input value
	 * 
	 * @param inputValue
	 * @param inputName
	 * @throws ExtensionsManagerException
	 */
	private void validateStringInput(String inputValue, String inputName) throws ExtensionsManagerException
	{
		if (inputValue == null)
		{
			throw new ExtensionsManagerException(NLS.bind(FrameworkMessages.Extension_NotNull, inputName));
		}

		inputValue = inputValue.trim();
		if (inputValue.isEmpty())
		{
			throw new ExtensionsManagerException(NLS.bind(FrameworkMessages.Extension_NotEmpty, inputName));
		}
	}

	/**
	 * @return Environment Extensions.
	 */
	public final SortedSet<? extends IVisible> getEnvironments()
	{
		// we sort the set only when the user requests it from the UI
		SortedSet<? extends IVisible> sortedEnvironments = new TreeSet<EnvironmentExtension>(this.envExtensions);

		return sortedEnvironments;
	}

	/**
	 * Sets selected Template Extension.
	 * 
	 * @param templateId
	 *            comes from UI.
	 * @throws ExtensionsManagerException
	 */
	protected void setSelectedTemplate(String templateId) throws ExtensionsManagerException
	{
		validateStringInput(templateId, FrameworkMessages.ExtensionsManager_TemplateId);

		boolean templateNotFound = true;
		for (TemplateExtension templateExtension : this.templateExtensions)
		{
			if (templateExtension == null)
			{
				continue;
			}

			if (templateExtension.getId().equals(templateId))
			{
				this.selectedTemplateExtension = templateExtension;
				templateNotFound = false;
				break;
			}
		}

		if (templateNotFound)
		{
			throw new ExtensionsManagerException(NLS.bind(FrameworkMessages.ExtensionsManager_Could_Not_Find_Template,
					templateId));
		}

		setSelectedTemplateDependencies();
	}

	/**
	 * Loads the Extensions, filters and saves them.
	 * 
	 * @throws ExtensionsManagerException
	 */
	protected final void loadExtenions() throws ExtensionsManagerException
	{
		loadSpecificExtensions();

		filterAndSave();
	}

	/**
	 * Loads Specific Environment, Template, Pattern and Component extensions.
	 */
	protected void loadSpecificExtensions()
	{
		// Retrieves the Template, Environment and Pattern extensions from the
		// plugin.xml
		this.envConfigElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
				Activator.PLUGIN_ID + ENVIRONMENT);

		this.templateConfigElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
				Activator.PLUGIN_ID + TEMPLATE);

		this.patternConfigElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
				Activator.PLUGIN_ID + PATTERN);
	}

	/**
	 * Creates temporary environment extensions list.
	 * 
	 * @param envConfigElements
	 *            existing environment extensions.
	 * @return temporary list of all existing environment extensions.
	 * @throws ExtensionsManagerException
	 *             - IConfigurationElement of environment extension is null.
	 */
	protected List<EnvironmentExtension> createTemporaryEnvironmentExtensionsList() throws ExtensionsManagerException
	{
		List<EnvironmentExtension> tempList = new ArrayList<EnvironmentExtension>();

		// going over the config elements and wrapping them to
		// EnvironmentExtension objects
		for (IConfigurationElement envConfigElement : this.envConfigElements)
		{
			if (envConfigElement == null)
			{
				throw new ExtensionsManagerException(FrameworkMessages.ExtensionsManager_NullEnvironmentExtension);
			}

			try
			{
				EnvironmentExtension envExtension = new EnvironmentExtension(envConfigElement);
				// add to the temporary list
				tempList.add(envExtension);
			}
			catch (ExtensionException e)
			{
				String id = envConfigElement.getAttribute(ExtensionAttributes.id.name());
				Logger.getFrameworkLogger().logWarning(
						e.getMessage() + NEW_LINE
								+ NLS.bind(FrameworkMessages.ExtensionsManager_EnvExtCreationFailed, id));
			}
		}

		return tempList;
	}

	/**
	 * Creates temporary pattern extensions list.
	 * 
	 * @param patterConfigElements
	 *            existing pattern extensions.
	 * @return temporary list of all existing pattern extensions.
	 * @throws ExtensionsManagerException
	 *             - when IConfigurationElement of pattern extension is null.
	 */
	protected List<PatternExtension> createTemporaryPatternExtensionsList() throws ExtensionsManagerException
	{
		List<PatternExtension> tempPatternList = new ArrayList<PatternExtension>();

		// going over the config elements and wrapping them to TemplateExtension
		// objects
		for (IConfigurationElement patternConfigElement : this.patternConfigElements)
		{
			if (patternConfigElement == null)
			{
				throw new ExtensionsManagerException(FrameworkMessages.ExtensionsManager_NullPatternExtension);
			}

			try
			{
				PatternExtension patternExtension = new PatternExtension(patternConfigElement);
				// for pattern testing purposes only
				// if it fails pattern will not be available and all it
				// dependent templates too
				patternExtension.getPattern();
				// add to the temporary list
				tempPatternList.add(patternExtension);
			}
			catch (ExtensionException e)
			{
				String id = patternConfigElement.getAttribute(ExtensionAttributes.id.name());
				Logger.getFrameworkLogger().logWarning(
						e.getMessage() + NEW_LINE
								+ NLS.bind(FrameworkMessages.ExtensionsManager_PatternExtCreationFailed, id));
			}
		}

		return tempPatternList;
	}

	/**
	 * Creates temporary template extensions list.
	 * 
	 * @param configElements
	 *            template extensions.
	 * @return temporary list of all specific template extensions.
	 * @throws ExtensionsManagerException
	 *             - when IConfigurationElement of template extension is null.
	 */
	protected List<TemplateExtension> createTemporaryTemplateExtensionsList(IConfigurationElement[] configElements,
			TemplateType type) throws ExtensionsManagerException
	{
		List<TemplateExtension> tempList = new ArrayList<TemplateExtension>();

		// going over the config elements and wrapping them to TemplateExtension
		// objects
		for (IConfigurationElement configElement : configElements)
		{
			if (configElement == null)
			{
				throw new ExtensionsManagerException(
						FrameworkMessages.ExtensionsManager_NullTemplateExtensionConfigElement);
			}

			try
			{
				TemplateExtension templateExtension = new TemplateExtension(configElement);
				// add to the temporary list
				if (templateExtension.getType() == type)
				{
					tempList.add(templateExtension);
				}
			}
			catch (ExtensionException e)
			{
				String id = configElement.getAttribute(ExtensionAttributes.id.name());
				Logger.getFrameworkLogger().logWarning(
						e.getMessage() + NEW_LINE
								+ NLS.bind(FrameworkMessages.ExtensionsManager_TemplateExtInitFailed, id));
			}
		}

		return tempList;
	}

	/**
	 * Filters through all environments, templates and patterns and saves the
	 * needed ones.
	 * 
	 * @throws ExtensionsManagerException
	 */
	protected void filterAndSave() throws ExtensionsManagerException
	{
		// temporary lists for saving the loaded templates, environments and
		// patterns
		List<EnvironmentExtension> tempEnvExtensions = createTemporaryEnvironmentExtensionsList();
		List<TemplateExtension> tempTemplateExtensions = createTemporaryTemplateExtensionsList();
		List<PatternExtension> tempPatternExtensions = createTemporaryPatternExtensionsList();

		// going over each Template and checks their templateType
		// (Proxy/StarterApp).
		for (TemplateExtension template : tempTemplateExtensions)
		{
			EnvironmentExtension neededEnvironment = getNeededEnvironment(template, tempEnvExtensions);
			if (neededEnvironment == null)
			{
				Logger.getFrameworkLogger().logWarning(
						NLS.bind(FrameworkMessages.StarterAppExtensionsManager_MissingEnvironmentId,
								template.getEnvironmentId(), template.getId()));
				continue;
			}
			// here we have the environment needed

			String patternId = template.getPatternId();
			if (patternId == null)
			{
				addEnvironmentAndTemplate(template, neededEnvironment);
				continue;
			}
			// here we also have the pattern needed

			PatternExtension foundPattern = findPattern(patternId, tempPatternExtensions);
			if (foundPattern == null)
			{
				Logger.getFrameworkLogger().logWarning(
						NLS.bind(FrameworkMessages.StarterAppExtensionsManager_PatternsNotFound, template.getId(),
								patternId));
				continue;
			}

			addEnvironmentAndTemplate(template, neededEnvironment);
		}
	}

	/**
	 * Creates temporary template extensions list.
	 * 
	 * @param templateConfigElements
	 *            existing template extensions .
	 * @return temporary list of all existing template extensions.
	 * @throws ExtensionsManagerException
	 *             - when IConfigurationElement of template extension is null.
	 */
	private List<TemplateExtension> createTemporaryTemplateExtensionsList() throws ExtensionsManagerException
	{
		List<TemplateExtension> tempList = createTemporaryTemplateExtensionsList(this.templateConfigElements, this.type);

		return tempList;
	}


	/**
	 * Finds and returns the needed environment of this proxy.
	 * 
	 * @param template
	 *            - the template extension.
	 * @param tempEnvExtensions
	 *            - the list of environment extensions.
	 * @return - the needed environment of this proxy.
	 */
	private EnvironmentExtension getNeededEnvironment(TemplateExtension template,
			List<EnvironmentExtension> tempEnvExtensions)
	{
		EnvironmentExtension neededEnvironment = null;

		// going over each environment
		for (EnvironmentExtension environment : tempEnvExtensions)
		{
			// checks for id equality.
			// if equal,
			if (environment.getId().equals(template.getEnvironmentId()))
			{
				neededEnvironment = environment;
				break;
			}
		}

		return neededEnvironment;
	}

	/**
	 * Finds and returns the needed pattern for this proxy.
	 * 
	 * @param neededPatternId
	 *            - the needed pattern id.
	 * @param tempPatternExtensions
	 *            - the list of pattern extensions.
	 * @return - the needed pattern for this proxy.
	 * @throws ExtensionsManagerException
	 * @throws ExtensionException
	 */
	private PatternExtension findPattern(String neededPatternId, List<PatternExtension> tempPatternExtensions)
			throws ExtensionsManagerException
	{
		try
		{
			for (PatternExtension patternExtension : tempPatternExtensions)
			{
				if (neededPatternId.equals(patternExtension.getId()))
				{ // test pattern could be created
					patternExtension.getPattern();
					return patternExtension;
				}
			}

			return null;
		}
		catch (ExtensionException e)
		{
			throw new ExtensionsManagerException(e);
		}
	}

	/**
	 * Adds environment and template to the parent's lists.
	 * 
	 * @param templateExtension
	 *            - the template extension.
	 * @param environmentExtension
	 *            - the environment extension.
	 */
	private void addEnvironmentAndTemplate(TemplateExtension templateExtension,
			EnvironmentExtension environmentExtension)
	{
		this.envExtensions.add(environmentExtension);
		this.templateExtensions.add(templateExtension);
	}
}
