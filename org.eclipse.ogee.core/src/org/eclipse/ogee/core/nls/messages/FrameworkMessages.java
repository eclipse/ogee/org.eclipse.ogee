/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core.nls.messages;

import org.eclipse.osgi.util.NLS;

public class FrameworkMessages extends NLS
{
	private static final String BUNDLE_NAME = "org.eclipse.ogee.core.nls.messages.framework"; //$NON-NLS-1$

	public static String Open_Graphical_Editor;
	public static String AbstractWizard_1;
	public static String AbstractWizard_3;

	public static String Unknown_Template_Extension_Type;

	public static String BrowseButton;

	// extensibility messages
	public static String EnvironmentExtension_EnvExecutableExtensionCreationFailure;
	public static String Extension_ConfigElementNotNull;
	public static String Extension_NotEmpty;
	public static String Extension_NotNull;
	public static String Extension_ExtensionId;
	public static String ExtensionsManager_EnvExtCreationFailed;
	public static String ExtensionsManager_EnvironmentId;
	public static String ExtensionsManager_NullEnvironmentExtension;
	public static String ExtensionsManager_NullPatternExtension;
	public static String ExtensionsManager_NullTemplateExtensionConfigElement;
	public static String ExtensionsManager_PatternExtCreationFailed;
	public static String ExtensionsManager_TemplateExtInitFailed;
	public static String ExtensionsManager_TemplateId;
	public static String PatternExecutableExtensionCreationFailure;
	public static String ProxyExtensionsManager_DuplicateProxyTemplateExt;
	public static String ComponentExecutableExtensionCreationFailure;

	public static String StarterAppExtensionsManager_MissingEnvironmentId;
	public static String StarterAppExtensionsManager_PatternsNotFound;
	public static String TemplateExtension_TemplateExecutableExtensionCreationFailure;
	public static String TemplateExtension_TemplateEnvId;
	public static String VisibleExtension_ExtensionDescription;
	public static String VisibleExtension_ExtensionDisplayName;
	public static String ExtensionsManager_Could_Not_Find_Pattern;
	public static String ExtensionsManager_Could_Not_Find_Environment;
	public static String ExtensionsManager_Could_Not_Find_Template;
	public static String ObservableComponent_0;
	public static String PatternExtension_Pattern_Must_Contain_At_Least_One_Component;
	public static String PatternUtil_PatternElement_CouldNOtBeNull;
	public static String PatternUtil_Component_MustBeOfType;

	public static String Folder;
	public static String SelectFolder;

	public static String ServiceImplementationExtension_0;
	public static String ServiceImplementationExtension_1;
	public static String ServiceImplementationExtensionsManager_0;
	public static String ServiceImplementationExtensionsManager_1;
	public static String ServiceImplementationProjectPage_0;
	public static String ServiceImplementationProjectPage_1;

	public static String ServiceImplementationProjectPage_13;
	public static String ServiceImplementationProjectPage_14;
	public static String ServiceImplementationProjectPage_15;
	public static String ServiceImplementationProjectPage_2;
	public static String ServiceImplementationProjectPage_3;
	public static String ServiceImplementationProjectPage_4;
	public static String ServiceImplementationProjectPage_5;
	public static String ServiceImplementationProjectPage_6;
	public static String ServiceImplementationProjectPage_7;
	public static String ServiceImplementationProjectPage_Path;
	public static String ServiceImplementationProjectPage_LocationDetails;
	public static String ServiceImplementationProjectPage_ServiceDetails;
	public static String ServiceImplementationProjectPage_ServiceType;
	public static String ServiceImplementationProjectPage_Server;
	public static String ServiceImplementationProjectPage_ODataVersion;
	public static String ServiceImplementationProjectPage_ServiceName;
	public static String ServiceImplementationProjectPage_ChooseODataFile;
	public static String ServiceImplementationProjectPage_CreateODataFile;
	public static String ServiceImplementationProjectPage_ODataDetails;
	public static String ServiceImplementationProjectPage_TargetRuntime;	
	public static String ServiceName;
	public static String PleaseSelectFolder;
	public static String SelectModelName;
	public static String PleaseSelectModelType;
	public static String NewODataServicePageDescription;
	public static String V2Support;
	public static String NewODataServicePageName;
	public static String ImportODataServicePageName;
	public static String ImportODataServicePageDescription;
	public static String TypeOfModel;
	public static String OdataModelElementNull;
	public static String ODataModelController_1;
	public static String ODataModelController_2;
	public static String ODataModelExecutableExtensionCreationFailure;
	public static String ODataModelExecutableExtensionIDCreationFailure;
	public static String ODataServiceWizardTitle;
	public static String ODataServiceWizard_NoExtenions;
	public static String ODataServiceWizard_ModelNameStartLetter;
	public static String ODataServiceWizard_ModelNameContainLettersDigits;
	public static String ODataServiceWizard_OptionalFile;
	public static String ODataServiceWizard_CreateNewOdataModel_Desc;


	public static String ODataModelPage_ODataAlreadyExists;

	public static String ServiceImplementationController_0;
	public static String ServiceImplementationController_1;
	public static String ServiceImplementationController_2;
	public static String ServiceImplementationController_3;
	public static String ServiceImplementationController_4;
	public static String ServiceImplementationController_5;
	public static String ServiceImplementationController_6;
	public static String ServiceImplementationController_7;
	public static String ServiceImplementationController_8;
	public static String ServiceImplementationController_9;
	public static String ServiceImplementationController_10;
	public static String ServiceImplementationController_11;
	public static String ServiceImplementationController_12;
	public static String ServiceImplementationController_13;
	public static String ServiceImplementationController_14;
	public static String ServiceImplementationController_15;


	public static String ServiceFromFileWizardPage_2;

	public static String ServiceFromFileWizardPage_3;

	public static String ServiceFromFileWizardPage_4;

	public static String ServiceFromFileWizardPage_5;

	public static String ServiceFromFileWizardPage_6;

	public static String ServiceFromFileWizardPage_7;

	public static String ServiceFromFileWizardPage_8;

	public static String ServiceFromUrlWizardPage_0;

	public static String ServiceFromUrlWizardPage_1;

	public static String ServiceFromUrlWizardPage_2;

	public static String ServiceFromUrlWizardPage_3;

	public static String ServiceFromUrlWizardPage_4;

	public static String ServiceFromUrlWizardPage_5;

	public static String ServiceFromUrlWizardPage_6;

	public static String ServiceFromUrlWizardPage_7;

	public static String ServiceFromUrlWizardPage_8;

	public static String ServiceFromUrlWizardPage_9;

	public static String ServiceFromODataWizardPage_OData;

	public static String ServiceDetails;

	public static String NotSupportedElements_wizard_message;

	public static String ODataModelProjectPage_BlankModel;

	public static String ODataModelProjectPage_UrlModel;

	public static String ODataModelProjectPage_FileModel;

	public static String ODataModelProjectPage_ExistingODataModel;

	public static String ODataModelProjectPage_ServiceCatalog;

	public static String ODataModelProjectPage_BlankModelDescription;

	public static String ODataModelProjectPage_UrlModelDescription;

	public static String ODataModelProjectPage_FileModelDescription;

	public static String ODataModelProjectPage_ExistingODataModelDescription;

	public static String ODataModelProjectPage_ServiceCatalogDescription;

	public static String ServiceFromODataWizardPage_2;

	public static String ServiceFromODataWizardPage_3;
	
	public static String UninstallAction_0;
	
	public static String UninstallAction_2;
	
	public static String UninstallAction_3;
	
	static
	{
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, FrameworkMessages.class);
	}

	private FrameworkMessages()
	{
	}
}
