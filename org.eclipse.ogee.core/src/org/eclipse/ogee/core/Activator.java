/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.core;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin
{

	// The plug-in ID
	public static final String PLUGIN_ID = "org.eclipse.ogee.core"; //$NON-NLS-1$

	public static final String CREATE_PROXY_WIZARD = "create_proxy_wizard"; //$NON-NLS-1$
	public static final String CREATE_STARTER_APP_WIZARD = "create_starter_app_wizard"; //$NON-NLS-1$
	public static final String IMPORT_SERVICE_WIZARD = "import_service_wizard"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator()
	{
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	public void start(BundleContext context) throws Exception // NOPMD
	{
		super.start(context);

		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	public void stop(BundleContext context) throws Exception // NOPMD
	{
		plugin = null;
		super.stop(context);

	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault()
	{
		return plugin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#initializeImageRegistry(org.eclipse
	 * .jface.resource.ImageRegistry)
	 */
	@Override
	protected void initializeImageRegistry(ImageRegistry reg)
	{
		super.initializeImageRegistry(reg);
		Bundle bundle = Platform.getBundle(PLUGIN_ID);

		ImageDescriptor myImage = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(
				"res/images/create_gateway_proxy.png"), null)); //$NON-NLS-1$
		reg.put(CREATE_PROXY_WIZARD, myImage);

		myImage = ImageDescriptor.createFromURL(FileLocator.find(bundle,
				new Path("res/images/new_gateway_project.png"), null)); //$NON-NLS-1$
		reg.put(CREATE_STARTER_APP_WIZARD, myImage);

		myImage = ImageDescriptor.createFromURL(FileLocator.find(bundle,
				new Path("res/images/create_service_model.png"), null)); //$NON-NLS-1$
		reg.put(IMPORT_SERVICE_WIZARD, myImage);

	}


}
