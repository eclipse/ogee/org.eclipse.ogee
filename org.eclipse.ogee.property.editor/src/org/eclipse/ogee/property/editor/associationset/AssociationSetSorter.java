/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.associationset;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.viewers.ViewerSorter;

public class AssociationSetSorter extends ViewerSorter {

	/**
	 * Constructor argument values that indicate to sort items by Name, end 1
	 * name or end 2 name.
	 */
	public final static int NAME = 1;
	public final static int END1 = 2;
	public final static int END2 = 3;

	// Criteria that the instance uses
	private int criteria;

	public AssociationSetSorter(int criteria) {
		super();
		this.criteria = criteria;
	}

	public int compare(Viewer viewer, Object o1, Object o2) {

		AssociationSetEntry associationSetEntry1 = (AssociationSetEntry) o1;
		AssociationSetEntry associationSetEntry2 = (AssociationSetEntry) o2;
		ViewerComparator viewerComparator = new ViewerComparator();
		switch (criteria) {
		case NAME:
			return viewerComparator.compare(viewer,
					associationSetEntry1.getName(),
					associationSetEntry2.getName());
		case END1:
			return viewerComparator.compare(viewer,
					associationSetEntry1.getEndName(0),
					associationSetEntry2.getEndName(0));
		case END2:
			return viewerComparator.compare(viewer,
					associationSetEntry1.getEndName(1),
					associationSetEntry2.getEndName(1));
		default:
			return 0;
		}
	}

}
