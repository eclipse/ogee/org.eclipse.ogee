/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.associationset;

import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.ui.elements.dialog.AssociationSetDialog;
import org.eclipse.swt.widgets.TableItem;

public class AssociationSetCellModifier implements ICellModifier {
	private AssociationSetDialog associationSetDialog;
	private AssociationSetModel associationSetModel;
	private boolean readOnly;

	public AssociationSetCellModifier(
			AssociationSetDialog associationSetDialog,
			AssociationSetModel associationSetModel, boolean readOnly) {
		this.associationSetDialog = associationSetDialog;
		this.associationSetModel = associationSetModel;
		this.readOnly = readOnly;
	}

	@Override
	public boolean canModify(Object element, String property) {
		return !readOnly;
	}

	@Override
	public Object getValue(Object element, String property) {
		// Find the index of the column
		int columnIndex = associationSetDialog.getColumnNames().indexOf(
				property);
		Object result = null;
		AssociationSetEntry associationSetEntry = (AssociationSetEntry) element;

		switch (columnIndex) {
		case 0: // Name
			result = associationSetEntry.getName();
			break;
		case 1: // End1
			result = getEndIndexValue(associationSetEntry, 0);
			break;
		case 2: // End2
			result = getEndIndexValue(associationSetEntry, 1);
			break;
		default:
			result = ""; //$NON-NLS-1$
		}
		return result;
	}

	@Override
	public void modify(Object element, String property, Object value) {
		// Find the index of the column
		int columnIndex = associationSetDialog.getColumnNames().indexOf(
				property);
		// flags for changes
		boolean name, end1, end2;
		name = end1 = end2 = false;

		TableItem item = (TableItem) element;
		AssociationSetEntry associationSetEntry = (AssociationSetEntry) item
				.getData();

		switch (columnIndex) {
		case 0: // Name
			name = modifyName(value, associationSetEntry);
			break;
		case 1: // End1
			end1 = modifyEnd(value, associationSetEntry, 0);
			break;
		case 2: // End2
			end2 = modifyEnd(value, associationSetEntry, 1);
			break;
		default:
			return;
		}

		if (name || end1 || end2) {
			validate();
			associationSetDialog.getTableViewer().update(associationSetEntry,
					null);
		}
	}

	private boolean modifyName(Object value,
			AssociationSetEntry associationSetEntry) {
		if (!associationSetEntry.getName().equals(value.toString())) {
			associationSetEntry.setName(value.toString());
			associationSetEntry.setChanged(true);
			return true;
		}

		return false;
	}

	private boolean modifyEnd(Object value,
			AssociationSetEntry associationSetEntry, int endIndex) {
		String valueString;
		EntitySet valueEntitySet;
		int comboIndex = ((Integer) value).intValue();
		String[] endoptions = associationSetModel.getEndOptions(endIndex);
		if (comboIndex < 0 || comboIndex >= endoptions.length) {
			return false;
		}
		valueString = associationSetModel.getEndOptions(endIndex)[comboIndex]
				.trim();
		valueEntitySet = associationSetModel.getEntitySetOptions(endIndex)[comboIndex];

		if (associationSetEntry.getEndName(endIndex) == null
				|| !associationSetEntry.getEndName(endIndex)
						.equals(valueString)) {
			associationSetEntry.setEndName(endIndex, valueString);
			associationSetEntry.setEndEntitySet(endIndex, valueEntitySet);
			associationSetEntry.setChanged(true);
			return true;
		}

		return false;
	}

	private Object getEndIndexValue(AssociationSetEntry associationSetEntry,
			int index) {
		int i;
		Object result;
		String stringValue = associationSetEntry.getEndName(index);
		String[] choices = associationSetModel.getEndOptions(index);
		if (stringValue == null)
			result = 0;
		else {
			i = choices.length - 1;

			while (i > 0 && !stringValue.equals(choices[i]))
				--i;
			result = i;
		}
		return result;
	}

	private void validate() {
		String message = associationSetModel.validate();
		if (message == null) {
			associationSetDialog.enableApplyButton(true, Messages.EMPTY_String);
		} else {
			associationSetDialog.enableApplyButton(false, message);
		}
	}

}
