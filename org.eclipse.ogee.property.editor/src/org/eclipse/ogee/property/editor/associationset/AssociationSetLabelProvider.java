/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.associationset;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class AssociationSetLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		AssociationSetEntry associationSetEntry = (AssociationSetEntry) element;
		String text;

		switch (columnIndex) {
		case 0: // Name
			text = associationSetEntry.getName();
			break;
		case 1: // End1
			text = associationSetEntry.getEndName(0);
			break;
		case 2: // End2
			text = associationSetEntry.getEndName(1);
			break;

		default:
			text = null;
			break;
		}
		return text;
	}

}
