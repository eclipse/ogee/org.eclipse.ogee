/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.associationset;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.ui.elements.dialog.IReturnValue;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ogee.utils.logger.Logger;

public class AssociationSetModel implements IReturnValue {
	private List<AssociationSetEntry> associationSetEntryList;
	private List<AssociationSetEntry> deletedAssociationSetEntryList;
	private int status;
	private Association association;
	private Logger logger = Logger.getLogger(Activator.PLUGIN_ID);
	private String associationName;

	public AssociationSetModel(Association association) {
		associationSetEntryList = new ArrayList<AssociationSetEntry>();
		deletedAssociationSetEntryList = new ArrayList<AssociationSetEntry>();

		if (association != null) {
			buildAssociationSetList(association);
			associationName = association.getName();
		} else {
			associationName = ""; //$NON-NLS-1$
			logger.logError(Messages.AssociationSetModel_Log_1);
		}
	}

	private void buildAssociationSetList(Association association) {
		String name;
		String end1Name;
		String end2Name;
		EntitySet entitySet1;
		EntitySet entitySet2;
		this.association = association;

		if (association.getAssociationSets() == null) {
			logger.logError(Messages.AssociationSetModel_0);
			return;
		}

		for (AssociationSet associationSet : association.getAssociationSets()) {
			end1Name = null;
			end2Name = null;
			entitySet1 = null;
			entitySet2 = null;

			name = associationSet.getName();
			if (associationSet.getEnds() != null
					&& !associationSet.getEnds().isEmpty()
					&& associationSet.getEnds().get(0).getEntitySet() != null) {
				entitySet1 = associationSet.getEnds().get(0).getEntitySet();
				end1Name = entitySet1.getName();
			} else {
				logger.trace(Messages.AssociationSetModel_Log_2, name);
			}

			if (associationSet.getEnds() != null
					&& associationSet.getEnds().size() > 1
					&& associationSet.getEnds().get(1).getEntitySet() != null) {
				entitySet2 = associationSet.getEnds().get(1).getEntitySet();
				end2Name = entitySet2.getName();
			} else {
				logger.trace(Messages.AssociationSetModel_Log_2, name);
			}

			associationSetEntryList.add(new AssociationSetEntry(associationSet,
					name, end1Name, end2Name, entitySet1, entitySet2, false));
		}

	}

	public List<AssociationSetEntry> getAssociationSets() {
		return associationSetEntryList;
	}

	public List<AssociationSetEntry> getDeletedAssociationSetEntryList() {
		return deletedAssociationSetEntryList;
	}

	public AssociationSetEntry addNewAssociationSetEntry() {

		String endOption0 = (getEndOptions(0) == null || getEndOptions(0).length == 0) ? "" : getEndOptions(0)[0]; //$NON-NLS-1$
		String endOption1 = (getEndOptions(1) == null || getEndOptions(1).length == 0) ? "" : getEndOptions(1)[0]; //$NON-NLS-1$
		EntitySet entitySet0 = (getEntitySetOptions(0) == null || getEntitySetOptions(0).length == 0) ? null
				: getEntitySetOptions(0)[0];
		EntitySet entitySet1 = (getEntitySetOptions(1) == null || getEntitySetOptions(1).length == 0) ? null
				: getEntitySetOptions(1)[0];

		AssociationSetEntry newAssociationSetEntry = new AssociationSetEntry(
				null, getAssociationSetInitialName(), endOption0, endOption1,
				entitySet0, entitySet1, true);

		associationSetEntryList.add(newAssociationSetEntry);

		return newAssociationSetEntry;
	}

	public void removeAssociationSet(AssociationSetEntry associationSetEntry) {
		deletedAssociationSetEntryList.add(associationSetEntry);
		associationSetEntryList.remove(associationSetEntry);
	}

	public String[] getEndOptions(int index) {

		if (association == null
				|| association.getEnds() == null
				|| association.getEnds().size() < index
				|| association.getEnds().get(index).getType() == null
				|| association.getEnds().get(index).getType().getEntitySets() == null) {
			logger.logError(String.format(Messages.AssociationSetModel_Log_3,
					associationName));
			return new String[] {};
		}

		List<String> endList = new ArrayList<String>();

		for (EntitySet entitySet : association.getEnds().get(index).getType()
				.getEntitySets()) {
			Schema schema = SchemaHandler.getSchema(association);
			if (schema != null
					&& schema.equals(SchemaHandler.getSchema(entitySet))) {
				endList.add(entitySet.getName());
			}
		}

		String[] ends = endList.toArray(new String[endList.size()]);
		return ends;
	}

	public EntitySet[] getEntitySetOptions(int index) {

		if (association == null
				|| association.getEnds() == null
				|| association.getEnds().get(index).getType() == null
				|| association.getEnds().get(index).getType().getEntitySets() == null) {
			logger.logError(String.format(Messages.AssociationSetModel_Log_3,
					associationName));
			return null;
		}

		List<EntitySet> entitySetList = new ArrayList<EntitySet>();

		for (EntitySet entitySet : association.getEnds().get(index).getType()
				.getEntitySets()) {
			Schema schema = SchemaHandler.getSchema(association);
			if (schema != null
					&& schema.equals(SchemaHandler.getSchema(entitySet))) {
				entitySetList.add(entitySet);
			}
		}

		EntitySet[] entitySets = entitySetList
				.toArray(new EntitySet[entitySetList.size()]);
		return entitySets;
	}

	private String getAssociationSetInitialName() {
		Set<String> nameSet = new HashSet<String>();
		String NewName = String.format("%sSet", associationName); //$NON-NLS-1$
		int i = 2;

		for (AssociationSetEntry associationSetEntry : associationSetEntryList) {
			if (!nameSet.contains(associationSetEntry.getName()))
				nameSet.add(associationSetEntry.getName());
		}

		while (nameSet.contains(NewName)) {
			NewName = String.format("%sSet%d", associationName, i++); //$NON-NLS-1$
		}

		return NewName;
	}

	public String getEndEntityName(int index, boolean isTitle) {

		if (association == null || association.getEnds() == null
				|| association.getEnds().get(index).getType() == null) {
			logger.logError(String.format(Messages.AssociationSetModel_Log_3,
					associationName));
			return null;
		}
		if (isTitle) {
			return String.format(Messages.AssociationSetModel_2, index + 1,
					association.getEnds().get(index).getType().getName());
		} else {
			return association.getEnds().get(index).getType().getName();
		}

	}

	@Override
	public String getText() {
		return Utilities.createAssociationSetString(association);
	}

	@Override
	public int getReturnStatus() {
		return status;

	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String validate() {
		String message = null;
		Set<String> nameSet = new HashSet<String>();

		for (AssociationSetEntry associationSetEntry : associationSetEntryList) {
			if (nameSet.contains(associationSetEntry.getName())) {
				return (String.format(Messages.AssociationSetModel_Validate_0,
						associationSetEntry.getName()));
			}

			if (associationSetEntry.getEndName(0) == null
					|| associationSetEntry.getEndName(0) == "" //$NON-NLS-1$
					|| associationSetEntry.getEndName(1) == null
					|| associationSetEntry.getEndName(1) == "") { //$NON-NLS-1$
				return (String
						.format(Messages.AssociationSetModel_AssociationSetMustContainValue,
								associationSetEntry.getName()));

			}

			nameSet.add(associationSetEntry.getName());
		}

		return message;
	}

}
