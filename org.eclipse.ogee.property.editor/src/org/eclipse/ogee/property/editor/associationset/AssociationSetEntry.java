/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.associationset;

import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.EntitySet;

public class AssociationSetEntry {
	private AssociationSet associationSet;
	private String name;
	private String end1Name;
	private String end2Name;
	private EntitySet end1EntitySet;
	private EntitySet end2EntitySet;
	private boolean isChanged;

	public AssociationSetEntry(AssociationSet associationSet, String name,
			String end1Name, String end2Name, EntitySet end1EntitySet,
			EntitySet end2EntitySet, boolean isChanged) {

		this.associationSet = associationSet;
		this.name = name;
		this.end1Name = end1Name;
		this.end2Name = end2Name;
		this.end1EntitySet = end1EntitySet;
		this.end2EntitySet = end2EntitySet;
		this.isChanged = isChanged;
	}

	public AssociationSet getAssociationSet() {
		return associationSet;
	}

	public void setAssociationSet(AssociationSet associationSet) {
		this.associationSet = associationSet;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isChanged() {
		return isChanged;
	}

	public void setChanged(boolean isChanged) {
		this.isChanged = isChanged;
	}

	public EntitySet getEndEntitySet(int index) {
		if (index == 0)
			return end1EntitySet;
		else
			return end2EntitySet;
	}

	public void setEndEntitySet(int index, EntitySet entitySet) {
		if (index == 0)
			end1EntitySet = entitySet;
		else
			end2EntitySet = entitySet;
	}

	public String getEndName(int index) {
		if (index == 0)
			return end1Name;
		else
			return end2Name;

	}

	public void setEndName(int index, String name) {
		if (index == 0)
			end1Name = name;
		else
			end2Name = name;
	}

}
