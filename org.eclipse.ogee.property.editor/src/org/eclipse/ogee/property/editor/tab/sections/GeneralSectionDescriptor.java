/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.tab.sections;

import org.eclipse.jface.viewers.AcceptAllFilter;
import org.eclipse.jface.viewers.IFilter;
import org.eclipse.ogee.property.editor.nls.messages.Constants;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.ui.elements.AdvancedPropertySection;
import org.eclipse.ui.views.properties.tabbed.AbstractSectionDescriptor;
import org.eclipse.ui.views.properties.tabbed.ISection;

public class GeneralSectionDescriptor extends AbstractSectionDescriptor {

	private IFilter filter;	

	private GeneralSectionDescriptor() {
		super();
		filter = new AcceptAllFilter();
	}

	@Override
	public String getId() {
		return Constants.GENERAL_SECTION_ID;
	}

	@Override
	public ISection getSectionClass() {
		return new AdvancedPropertySection(
				Messages.GeneralSection_first_column_name);
	}

	@Override
	public String getTargetTab() {
		return Constants.GENERAL_TAB_ID;
	}

	@Override
	public IFilter getFilter() {
		return filter;
	}

	private static GeneralSectionDescriptor instance;

	public static GeneralSectionDescriptor getInstance() {
		if (instance == null) {
			instance = new GeneralSectionDescriptor();
		}
		return instance;
	}

}
