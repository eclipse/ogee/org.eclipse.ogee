/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.tab.sections.filters;

import org.eclipse.jface.viewers.IFilter;
import org.eclipse.ogee.exploration.tree.api.IServiceDetails;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class GeneralFilter implements IFilter {

	@Override
	public boolean select(Object obj) {

		return accept(Utilities.getObject(obj));

	}

	protected boolean accept(Object obj) {
		if (obj instanceof IServiceDetails) {
			return true;
		}

		if (obj instanceof EntityType) {
			return true;
		}

		if (obj instanceof Property) {
			return true;
		}

		if (obj instanceof EntitySet) {
			return true;
		}

		if (obj instanceof FunctionImport) {
			return true;
		}

		if (obj instanceof NavigationProperty) {
			return true;
		}

		if (obj instanceof Association) {
			return true;
		}

		if (obj instanceof ComplexType) {
			return true;
		}

		if (obj instanceof Parameter) {
			return true;
		}

		if (obj instanceof Schema) {
			return true;
		}

		if (obj instanceof IFunctionReturnTypeUsage) {
			return true;
		}
		if (obj instanceof AssociationSet) {
			return true;
		}

		if (obj instanceof EntityContainer) {
			return true;
		}

		if (obj instanceof EnumType) {
			return true;
		}

		if (obj instanceof EnumMember) {
			return true;
		}

		if (obj instanceof ValueTerm) {
			return true;
		}

		return false;
	}

}
