/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.tab.sections;

import org.eclipse.jface.viewers.IFilter;
import org.eclipse.ogee.property.editor.nls.messages.Constants;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.tab.sections.filters.VocabularyFilter;
import org.eclipse.ogee.property.editor.ui.elements.AdvancedPropertySection;
import org.eclipse.ui.views.properties.tabbed.AbstractSectionDescriptor;
import org.eclipse.ui.views.properties.tabbed.ISection;

public class VocabulariesSectionDescriptor extends AbstractSectionDescriptor {
	private IFilter filter;

	private VocabulariesSectionDescriptor() {
		super();
		filter = new VocabularyFilter();
	}

	@Override
	public String getId() {
		return Constants.VOCABULARIES_SECTION_ID;
	}

	@Override
	public ISection getSectionClass() {
		return new AdvancedPropertySection(
				Messages.VocabulariesSection_first_column_name);
	}

	@Override
	public String getTargetTab() {
		return Constants.VOCABULARIES_TAB_ID;
	}

	@Override
	public IFilter getFilter() {
		return filter;
	}

	private static VocabulariesSectionDescriptor instance;

	public static VocabulariesSectionDescriptor getInstance() {
		if (instance == null) {
			instance = new VocabulariesSectionDescriptor();
		}
		return instance;
	}

}
