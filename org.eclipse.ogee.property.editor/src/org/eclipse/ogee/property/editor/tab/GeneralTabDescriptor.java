/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.tab;

import java.util.ArrayList;

import org.eclipse.ogee.property.editor.nls.messages.Constants;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.tab.sections.GeneralSectionDescriptor;
import org.eclipse.ui.views.properties.tabbed.AbstractTabDescriptor;
import org.eclipse.ui.views.properties.tabbed.ISectionDescriptor;

public class GeneralTabDescriptor extends AbstractTabDescriptor {

	private GeneralTabDescriptor() {
		super();
		ArrayList<ISectionDescriptor> sectionList = new ArrayList<ISectionDescriptor>();
		sectionList.add(GeneralSectionDescriptor.getInstance());
		setSectionDescriptors(sectionList);
	}

	@Override
	public String getLabel() {
		return Messages.GeneralTabDescriptor_Label;
	}

	@Override
	public String getId() {
		return Constants.GENERAL_TAB_ID;
	}

	@Override
	public String getCategory() {
		return Constants.CONTRIBUTER_PROPERTY_CATEGORY;
	}

	private static GeneralTabDescriptor instance;

	public static GeneralTabDescriptor getInstance() {
		if (instance == null) {
			instance = new GeneralTabDescriptor();
		}
		return instance;
	}
}
