/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.tab.sections.filters;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IFilter;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ogee.property.editor.utilities.WorkbenchUtilities;

public class VocabularyFilter implements IFilter {

	@Override
	public boolean select(Object obj) {
		return accept(obj);
	}

	static public boolean accept(Object obj) {
		Object object = Utilities.getObject(obj);
		boolean isIAnnotationTarget = (object instanceof IAnnotationTarget);
		if (Utilities.vocabularyButton != null)
			Utilities.vocabularyButton.setEnabled(isIAnnotationTarget
					&& !WorkbenchUtilities.isReadOnlyMode((EObject) object));
		return isIAnnotationTarget;
	}

}
