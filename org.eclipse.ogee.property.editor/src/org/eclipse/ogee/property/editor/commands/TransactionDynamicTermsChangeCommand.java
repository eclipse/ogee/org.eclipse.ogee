/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.CollectableExpression;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.RecordValue;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.property.editor.annotations.AnnotationValueAdapter;
import org.eclipse.ogee.property.editor.annotations.dynamic.DynamicTermsModel;
import org.eclipse.ogee.property.editor.annotations.dynamic.DynamicTermsTreeEntry;
import org.eclipse.ogee.property.editor.utilities.TypeConverter;

public class TransactionDynamicTermsChangeCommand extends
		TransactionChangeCommandAbstract {

	private DynamicTermsModel dynamicTermsModel;
	private ValueCollection baseValuecollection;

	public TransactionDynamicTermsChangeCommand(Object value,
			ValueCollection valueCollection) {
		super(null, value, valueCollection);
		this.dynamicTermsModel = (DynamicTermsModel) value;
		this.baseValuecollection = valueCollection;
	}

	@Override
	public void doExecute() {

		baseValuecollection.getValues().clear();

		// Creating the new annotation
		baseValuecollection.getValues().addAll(
				updateValueCollection(dynamicTermsModel.getRootElement()));
	}

	private List<CollectableExpression> updateValueCollection(
			DynamicTermsTreeEntry entry) {
		List<CollectableExpression> list = new ArrayList<CollectableExpression>();
		for (DynamicTermsTreeEntry treeEntry : entry.getChildrenList()) {
			CollectableExpression collectableExpression = (CollectableExpression) createCollectableExpression(treeEntry);
			if (collectableExpression != null) {
				list.add(collectableExpression);
			}
		}
		return list;

	}

	private AnnotationValue createCollectableExpression(
			DynamicTermsTreeEntry entry) {
		// SimpleValue
		if (!entry.isRecord() && !entry.isCollectionParent()) {
			return createSimpleValue(entry);
		}

		// Collection
		if (entry.isCollectionParent()) {
			ValueCollection valueCollection = OdataFactory.eINSTANCE
					.createValueCollection();
			// add children to the valueCollection
			valueCollection.getValues().addAll(updateValueCollection(entry));
			return valueCollection;
		}

		// RecordValue

		else if (entry.isRecord()
				&& entry.getTypeUsage() != null
				&& ((ComplexTypeUsage) entry.getTypeUsage()).getComplexType() != null) {
			ComplexType complexType = ((ComplexTypeUsage) entry.getTypeUsage())
					.getComplexType();
			RecordValue recordValue = OdataFactory.eINSTANCE
					.createRecordValue();
			recordValue.setType(complexType);

			// add properties
			for (DynamicTermsTreeEntry treeEntry : entry.getChildrenList()) {
				recordValue.getPropertyValues().put(
						(Property) treeEntry.getTermDefinition(),
						createCollectableExpression(treeEntry));
			}

			return recordValue;
		}

		return null;
	}

	private CollectableExpression createSimpleValue(DynamicTermsTreeEntry entry) {

		EDMTypes type = ((SimpleTypeUsage) entry.getTypeUsage())
				.getSimpleType().getType();

		if (dynamicTermsModel.isPath(entry.getDisplayValue())) {
			type = EDMTypes.STRING;
		}

		return AnnotationValueAdapter.getInstance().createSimpleValue(
				TypeConverter.getValue(entry.getDisplayValue(), type), type);

	}
}
