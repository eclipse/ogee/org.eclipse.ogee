/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.commands;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.utils.logger.Logger;

public class TransactionChangeCommand extends TransactionChangeCommandAbstract {
	private Logger logger = Logger.getLogger(Activator.PLUGIN_ID);

	private String methodName;
	private Class<?> parameterDecleredClass;

	public TransactionChangeCommand(Object id, Object value, EObject eObject) {
		super(id, value, eObject);
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Class<?> getParameterDecleredClass() {
		return parameterDecleredClass;
	}

	public void setParameterDecleredClass(Class<?> parameterDecleredClass) {
		this.parameterDecleredClass = parameterDecleredClass;
	}

	@Override
	public void doExecute() {
		invoke(eObject, value, getMethodName(), getParameterDecleredClass());
	}

	private void invoke(Object object, Object value, String methodName,
			Class<?> valueClazz) {
		try {
			Method method = object.getClass().getDeclaredMethod(methodName,
					valueClazz);
			method.invoke(object, value);

		} catch (NoSuchMethodException e) {
			logger.logError(e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			logger.logError(e.getLocalizedMessage());
		} catch (InvocationTargetException e) {
			logger.logError(e.getLocalizedMessage());
		}
	}
}
