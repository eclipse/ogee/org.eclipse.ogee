/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.Multiplicity;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class TransactionAssociationMultiplicityChangeCommand extends
		TransactionChangeCommand {

	public TransactionAssociationMultiplicityChangeCommand(Object id,
			Object value, EObject eObject) {
		super(id, value, eObject);
	}

	@Override
	public void doExecute() {
		if (!(eObject instanceof Role) || !(value instanceof Multiplicity)
				|| !(eObject.eContainer() instanceof Association))
			return;
		Role role = (Role) eObject;
		Multiplicity multiplicity = (Multiplicity) value;
		Association association = (Association) role.eContainer();
		role.setMultiplicity(multiplicity);
		if (Utilities.isManyToMany(association) || isRCNeedDelete(association)) {
			association.setReferentialConstraint(null);
		}
	}

	private boolean isRCNeedDelete(Association association) {
		ReferentialConstraint referentialConstraint = association
				.getReferentialConstraint();
		return (referentialConstraint != null)
				&& referentialConstraint.getPrincipal().equals(eObject)
				&& (Multiplicity.MANY == value);
	}

}
