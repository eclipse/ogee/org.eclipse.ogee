/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.associationset.AssociationSetEntry;
import org.eclipse.ogee.property.editor.associationset.AssociationSetModel;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.utils.logger.Logger;

public class TransactionAssociationSetChangeCommand extends
		TransactionChangeCommandAbstract {
	private AssociationSetModel associationModel;
	private Association association;

	public TransactionAssociationSetChangeCommand(Object id, Object value,
			EObject eObject) {
		super(id, value, eObject);
		associationModel = (AssociationSetModel) value;
		association = (Association) eObject;
	}

	@Override
	public void doExecute() {
		Logger.getLogger(Activator.PLUGIN_ID).trace(
				Messages.TransactionAssociationSetChangeCommand_Log_0);

		AssociationSet associationSet;

		for (AssociationSetEntry associationSetEntry : associationModel
				.getAssociationSets()) {
			if (!associationSetEntry.isChanged()) // No change
				continue;

			associationSet = associationSetEntry.getAssociationSet();
			if (associationSet != null) {
				// Modify existing association set
				deleteAssociationSet(associationSet, associationSetEntry);
				AddNewAssociationSet(associationSetEntry);
			} else {
				// New AssociationSet
				AddNewAssociationSet(associationSetEntry);
			}
		}

		for (AssociationSetEntry associationSetEntry : associationModel
				.getDeletedAssociationSetEntryList()) {
			associationSet = associationSetEntry.getAssociationSet();

			if (associationSet == null) // Added and deleted only in Dialog UI
			{
				continue;
			}

			deleteAssociationSet(associationSet, associationSetEntry);
		}

		Logger.getLogger(Activator.PLUGIN_ID).trace(
				Messages.TransactionAssociationSetChangeCommand_Log_1);
	}

	private void AddNewAssociationSet(AssociationSetEntry associationSetEntry) {
		AssociationSet associationSet = OdataFactory.eINSTANCE
				.createAssociationSet();

		associationSet.setName(associationSetEntry.getName());
		associationSet.setAssociation(association);

		createAssociationSetEnd(associationSetEntry, associationSet, 0);
		createAssociationSetEnd(associationSetEntry, associationSet, 1);

		association.getAssociationSets().add(associationSet);
		getEntitycontainer(associationSet,
				associationSetEntry.getEndEntitySet(0)).getAssociationSets()
				.add(associationSet);
	}

	private void createAssociationSetEnd(
			AssociationSetEntry associationSetEntry,
			AssociationSet associationSet, int index) {
		AssociationSetEnd associationSetEnd = OdataFactory.eINSTANCE
				.createAssociationSetEnd();
		associationSetEnd.setEntitySet(associationSetEntry
				.getEndEntitySet(index));
		associationSetEnd.setRole(association.getEnds().get(index));
		associationSet.getEnds().add(associationSetEnd);
	}

	// Delete existing AssociationSet
	private void deleteAssociationSet(AssociationSet associationSet,
			AssociationSetEntry associationSetEntry) {
		associationSet.getEnds().clear();
		associationSet.getAssociation().getAssociationSets()
				.remove(associationSet);
		EntityContainer entityContainer = getEntitycontainer(associationSet,
				associationSetEntry.getEndEntitySet(0));
		if (entityContainer == null
				|| entityContainer.getAssociationSets() == null) {
			return;
		}
		entityContainer.getAssociationSets().remove(associationSet);
	}

	private EntityContainer getEntitycontainer(AssociationSet associationSet,
			EntitySet entitySet) {
		if (entitySet != null && entitySet.eContainer() != null
				&& (entitySet.eContainer() instanceof EntityContainer)) {
			return (EntityContainer) entitySet.eContainer();
		}

		return null;
	}

}
