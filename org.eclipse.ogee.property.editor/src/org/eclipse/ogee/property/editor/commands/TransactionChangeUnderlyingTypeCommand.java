/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.IntegerValue;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.property.editor.annotations.AnnotationValueAdapter;
import org.eclipse.ogee.property.editor.utilities.TypeConverter;
import org.eclipse.ogee.property.editor.utilities.Validator;

public class TransactionChangeUnderlyingTypeCommand extends
		TransactionChangeCommand {

	public TransactionChangeUnderlyingTypeCommand(Object id, Object value,
			EObject eObject) {
		super(id, value, eObject);
	}

	@Override
	public void doExecute() {
		if (!(eObject instanceof EnumType) || !(value instanceof EDMTypes))
			return;

		EnumType enumType = (EnumType) eObject;
		enumType.setUnderlyingType((EDMTypes) value);

		for (EnumMember enumMember : enumType.getMembers()) {
			if (isValid(enumMember)) {
				SimpleValue simpleValue = AnnotationValueAdapter.getInstance()
						.createSimpleValue(
								TypeConverter.getValue(enumMember.getValue()
										.getValueObject().toString(),
										(EDMTypes) value), (EDMTypes) value);
				if (simpleValue instanceof IntegerValue)
					enumMember.setValue((IntegerValue) simpleValue);
			}
		}
	}

	private boolean isValid(EnumMember enumMember) {
		boolean isValid = false;
		if (enumMember.getValue() != null
				&& enumMember.getValue().getValueObject() != null) {
			isValid = Validator.validate(enumMember.getValue().getValueObject()
					.toString(), (EDMTypes) value) == null;
		}
		return isValid;
	}

}
