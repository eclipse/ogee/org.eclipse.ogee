/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.PathValue;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.property.editor.annotations.AnnotationTabRecord;
import org.eclipse.ogee.property.editor.annotations.AnnotationValueAdapter;

public class TransactionAnnotationValueCommand extends TransactionChangeCommand {
	boolean isPathValue;
	AnnotationTabRecord record;

	public TransactionAnnotationValueCommand(Object id, Object value,
			EObject eObject, boolean isPathValue) {
		super(id, value, eObject);
		this.isPathValue = isPathValue;
		this.record = (AnnotationTabRecord) id;
	}

	@Override
	public void doExecute() {
		if (isPathValue) {
			if (record.getAnnotationValue() instanceof PathValue) {
				((PathValue) record.getAnnotationValue())
						.setPath((String) value);
			} else {
				record.setAnnotationValue(AnnotationValueAdapter.getInstance()
						.createPathValue((String) value));
			}
		} else {
			if (record.getAnnotationValue() instanceof SimpleValue) {
				((SimpleValue) record.getAnnotationValue())
						.setValueObject(value);
			} else {
				record.setAnnotationValue(AnnotationValueAdapter.getInstance()
						.createSimpleValue(
								value,
								((SimpleTypeUsage) record.getTypeUsage())
										.getSimpleType().getType()));
			}
		}
	}
}
