/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.designer.api.IODataDiagramCreator;
import org.eclipse.ogee.model.odata.IParameterTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.property.editor.type.handler.AbstractTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.ParameterTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.PropertyTypeHandler;

public class TransactionTypeUsageChangeCommand extends TransactionChangeCommand {
	private AbstractTypeHandler typeHandler;
	private boolean isProperty;

	public TransactionTypeUsageChangeCommand(Object id, Object value,
			EObject eObject, AbstractTypeHandler typeHandler) {
		super(id, value, eObject);
		this.typeHandler = typeHandler;
		this.isProperty = getEObject() instanceof Property;

	}

	@Override
	public void doExecute() {
		EObject oldValue = null;
		EObject newValue = null;

		if (isProperty) {
			oldValue = ((Property) getEObject()).getType();
			IPropertyTypeUsage ptu = (IPropertyTypeUsage) ((PropertyTypeHandler) typeHandler)
					.getType((Integer) value);
			if ((Property) getEObject() != null
					&& ((Property) getEObject()).getType() != null) {
				ptu.setCollection(((Property) getEObject()).getType()
						.isCollection());
			}
			newValue = ptu;
			((Property) getEObject()).setType(ptu);
		} else // isParameter
		{
			oldValue = ((Parameter) getEObject()).getType();
			IParameterTypeUsage ptu = ((ParameterTypeHandler) typeHandler)
					.getType((Integer) value);
			ptu.setCollection(((Parameter) getEObject()).getType()
					.isCollection());
			newValue = ptu;
			((Parameter) getEObject()).setType(ptu);
		}

		IODataDiagramCreator.INSTANCE.refreshEditor(getEObject(), oldValue,
				newValue);

	}

}
