/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.commands;

import org.eclipse.emf.ecore.EObject;

/**
 * 
 * Abstract class to enable transactional edit
 * 
 */
public abstract class TransactionChangeCommandAbstract {
	final protected Object id;
	protected Object value;
	final protected EObject eObject;

	public TransactionChangeCommandAbstract(Object id, Object value,
			EObject eObject) {
		this.id = id;
		this.value = value;
		this.eObject = eObject;
	}

	/**
	 * Implements transactional executable code (set property value)
	 */
	public abstract void doExecute();

	public EObject getEObject() {
		return eObject;
	}

}
