/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.Schema;

public class TransactionSchemaChangeCommand extends TransactionChangeCommand {
	private Schema from;
	private Schema to;

	public TransactionSchemaChangeCommand(Object id, Object value,
			EObject eObject) {
		super(id, value, eObject);
	}

	public void setFrom(Schema from) {
		this.from = from;
	}

	public void setTo(Schema to) {
		this.to = to;
	}

	@Override
	public void doExecute() {
		if (eObject instanceof EntityType) {
			from.getEntityTypes().remove(eObject);
			to.getEntityTypes().add((EntityType) eObject);
		} else if (eObject instanceof EntityContainer) {
			from.getContainers().remove(eObject);
			to.getContainers().add((EntityContainer) eObject);
		} else if (eObject instanceof ComplexType) {
			from.getComplexTypes().remove(eObject);
			to.getComplexTypes().add((ComplexType) eObject);
		} else if (eObject instanceof Association) {
			from.getAssociations().remove(eObject);
			to.getAssociations().add((Association) eObject);
		} else if (eObject instanceof EnumType) {
			from.getEnumTypes().remove(eObject);
			to.getEnumTypes().add((EnumType) eObject);
		}
	}

}
