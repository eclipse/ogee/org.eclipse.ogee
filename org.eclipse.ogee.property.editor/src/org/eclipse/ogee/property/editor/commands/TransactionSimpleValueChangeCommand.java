/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.property.editor.annotations.AnnotationValueAdapter;
import org.eclipse.ogee.property.editor.utilities.TypeConverter;

public class TransactionSimpleValueChangeCommand extends
		TransactionChangeCommand {

	private EDMTypes edmType;

	public TransactionSimpleValueChangeCommand(Object id, Object value,
			EObject eObject, EDMTypes edmType) {
		super(id, value, eObject);
		this.edmType = edmType;
	}

	@Override
	public void doExecute() {
		Object converteredValue = TypeConverter.getValue(value, edmType);

		setValue(AnnotationValueAdapter.getInstance().createSimpleValue(
				converteredValue, edmType));
		super.doExecute();
	}

}
