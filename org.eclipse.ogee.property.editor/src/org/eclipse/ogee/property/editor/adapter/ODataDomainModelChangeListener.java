/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.adapter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.NotificationFilter;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.RollbackException;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.ogee.property.editor.nls.messages.Constants;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.IPage;
import org.eclipse.ui.views.properties.PropertySheet;
import org.eclipse.ui.views.properties.tabbed.TabContents;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

/**
 * Listener for BO changes in an EMF model linked to a diagram. This class is to
 * refresh the properties view on model changes.
 * 
 * 
 */
public class ODataDomainModelChangeListener implements ResourceSetListener {

	/**
	 * Default Constructor.
	 */
	public ODataDomainModelChangeListener() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.transaction.ResourceSetListener#getFilter()
	 */
	@Override
	public NotificationFilter getFilter() {
		return NotificationFilter.NOT_TOUCH;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.transaction.ResourceSetListener#isAggregatePrecommitListener
	 * ()
	 */
	@Override
	public boolean isAggregatePrecommitListener() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.transaction.ResourceSetListener#isPostcommitOnly()
	 */
	@Override
	public boolean isPostcommitOnly() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.transaction.ResourceSetListener#isPrecommitOnly()
	 */
	@Override
	public boolean isPrecommitOnly() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.transaction.ResourceSetListener#resourceSetChanged(org
	 * .eclipse.emf.transaction.ResourceSetChangeEvent)
	 */
	@Override
	public void resourceSetChanged(ResourceSetChangeEvent event) {
		// To update properties view
		IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		if (activeWorkbenchWindow == null) {
			return;
		}
		final IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();

		// Compute changed BOs.
		final Set<EObject> changedBOs = new HashSet<EObject>();
		List<Notification> notifications = event.getNotifications();
		for (Notification notification : notifications) {
			Object notifier = notification.getNotifier();
			if (notifier instanceof EObject /*
											 * || notifier instanceof
											 * PropertyDescriptor
											 */) {
				changedBOs.add((EObject) notifier);
			}

		}

		// Do nothing if no BO linked to the diagram changed.
		if (changedBOs.size() == 0) {
			return;
		}

		// Do an asynchronous update in the UI thread.
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				IViewPart view = activePage
						.findView(Constants.PROPERTY_SHEET_ID);
				if (view != null && view instanceof PropertySheet) {
					IPage currentPage = ((PropertySheet) view).getCurrentPage();
					if (currentPage instanceof TabbedPropertySheetPage) {
						TabContents currentTab = ((TabbedPropertySheetPage) currentPage)
								.getCurrentTab();
						if (currentTab != null) {
							currentTab.refresh();
						}
						//refresh the title bar of property editor to show correct value after edit for any artifact
						((TabbedPropertySheetPage) currentPage).labelProviderChanged(new LabelProviderChangedEvent(new BaseLabelProvider()));
					}
				}

			}

		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.transaction.ResourceSetListener#transactionAboutToCommit
	 * (org.eclipse.emf.transaction.ResourceSetChangeEvent)
	 */
	@Override
	public Command transactionAboutToCommit(ResourceSetChangeEvent event)
			throws RollbackException {
		return null;
	}

}
