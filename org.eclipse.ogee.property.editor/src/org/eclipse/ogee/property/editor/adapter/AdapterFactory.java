/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.adapter;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.exploration.tree.api.IServiceDetails;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.property.editor.nls.messages.Constants;
import org.eclipse.ogee.property.editor.property.sources.AnnotationTargetODataSource;
import org.eclipse.ogee.property.editor.property.sources.AssociationODataSource;
import org.eclipse.ogee.property.editor.property.sources.ComplexTypeODataSource;
import org.eclipse.ogee.property.editor.property.sources.EntityContainerODataSource;
import org.eclipse.ogee.property.editor.property.sources.EntitySetODataSource;
import org.eclipse.ogee.property.editor.property.sources.EntityTypeODataSource;
import org.eclipse.ogee.property.editor.property.sources.EnumMembersODataSource;
import org.eclipse.ogee.property.editor.property.sources.EnumTypeODataSource;
import org.eclipse.ogee.property.editor.property.sources.FunctionImportODataSource;
import org.eclipse.ogee.property.editor.property.sources.FunctionImportParameterODataSource;
import org.eclipse.ogee.property.editor.property.sources.FunctionImportReturnTypeODataSource;
import org.eclipse.ogee.property.editor.property.sources.NavigationPropertyODataSource;
import org.eclipse.ogee.property.editor.property.sources.PropertyODataSource;
import org.eclipse.ogee.property.editor.property.sources.SchemaODataSource;
import org.eclipse.ogee.property.editor.property.sources.ServiceOdataExplorationTreeSource;
import org.eclipse.ogee.property.editor.property.sources.ValueTermODataSource;
import org.eclipse.ogee.property.editor.tab.sections.filters.VocabularyFilter;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ogee.property.editor.utilities.WorkbenchUtilities;
import org.eclipse.ui.views.properties.IPropertySource;

public class AdapterFactory implements IAdapterFactory {

	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {

		if (adapterType != IPropertySource.class) {
			return null;
		}

		Object obj = Utilities.getObject(adaptableObject);

		if (obj instanceof IServiceDetails) {
			return new ServiceOdataExplorationTreeSource(
					(IServiceDetails) adaptableObject);
		}		
		boolean readOnlyMode = WorkbenchUtilities.isReadOnlyMode((EObject) obj);
		String tabId = WorkbenchUtilities.getCurrentTabId();
		if (tabId != null && tabId.equals(Constants.VOCABULARIES_TAB_ID)
				&& VocabularyFilter.accept(obj)) {
			return new AnnotationTargetODataSource((IAnnotationTarget) obj,
					readOnlyMode);
		}

		return createPropertySource(obj, readOnlyMode);
	}

	private IPropertySource createPropertySource(Object obj,
			boolean isReadOnlyMode) {
		if (obj instanceof EntityType) {
			return new EntityTypeODataSource((EntityType) obj, isReadOnlyMode);
		}
		if (obj instanceof Property) {
			return new PropertyODataSource((Property) obj, isReadOnlyMode);
		}
		if (obj instanceof EntitySet) {
			return new EntitySetODataSource((EntitySet) obj, isReadOnlyMode);
		}
		if (obj instanceof NavigationProperty) {
			return new NavigationPropertyODataSource((NavigationProperty) obj,
					isReadOnlyMode);
		}
		if (obj instanceof FunctionImport) {
			return new FunctionImportODataSource((FunctionImport) obj,
					isReadOnlyMode);
		}
		if (obj instanceof Association) {
			return new AssociationODataSource((Association) obj, isReadOnlyMode);
		}
		if (obj instanceof AssociationSet) {
			return new AssociationODataSource(
					((AssociationSet) obj).getAssociation(), isReadOnlyMode);
		}
		if (obj instanceof ComplexType) {
			return new ComplexTypeODataSource((ComplexType) obj, isReadOnlyMode);
		}
		if (obj instanceof Parameter) {
			return new FunctionImportParameterODataSource((Parameter) obj,
					isReadOnlyMode);
		}
		if (obj instanceof Schema) {
			return new SchemaODataSource((Schema) obj, isReadOnlyMode);
		}
		if (obj instanceof IFunctionReturnTypeUsage) {
			return new FunctionImportReturnTypeODataSource(
					(IFunctionReturnTypeUsage) obj, isReadOnlyMode);
		}
		if (obj instanceof EntityContainer) {
			return new EntityContainerODataSource((EntityContainer) obj,
					isReadOnlyMode);
		}
		if (obj instanceof EnumType) {
			return new EnumTypeODataSource((EnumType) obj, isReadOnlyMode);
		}
		if (obj instanceof EnumMember) {
			return new EnumMembersODataSource((EnumMember) obj, isReadOnlyMode);
		}
		if (obj instanceof ValueTerm) {
			return new ValueTermODataSource((ValueTerm) obj, isReadOnlyMode);
		}

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class[] getAdapterList() {
		return new Class[] { IPropertySource.class };
	}

}
