/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.adapter;

import java.net.URL;
import java.util.MissingResourceException;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ogee.model.edit.odata.provider.OdataItemProviderAdapterFactory;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.swt.graphics.Image;

public class TabbedPropertyHeaderProvider extends AdapterFactoryLabelProvider {
	private Logger logger = Logger.getLogger(Activator.PLUGIN_ID);

	public TabbedPropertyHeaderProvider() {
		super(new OdataItemProviderAdapterFactory());
	}

	@Override
	public Image getImage(Object obj) {
		StructuredSelection selection = (StructuredSelection) obj;

		Object element = Utilities.getObject(selection.getFirstElement());

		try {
			IItemLabelProvider labelProvider = (IItemLabelProvider) this.adapterFactory
					.adapt(element, IItemLabelProvider.class);

			// if association set is changed then selection will be null, hence
			// not showing any image
			if (element instanceof AssociationSet) {
				Association association = ((AssociationSet) element)
						.getAssociation();
				if (null == association) {
					return null;
				}
			}
			// return parents image if label provider is null
			if (labelProvider == null) {
				return super.getImage(element);
			}
			// return parents image if image info object is null
			Object imageInfo = labelProvider.getImage(element);
			if (imageInfo == null) {
				return super.getImage(element);
			}
			// return image info object if it is of type Image
			if (imageInfo instanceof Image) {
				return (Image) imageInfo;
			}
			// get image from the image info object if it is of type URL
			if (imageInfo instanceof URL) {
				URL url = (URL) imageInfo;
				return getImage(url);
			}

			return super.getImage(element);
		} catch (MissingResourceException e) {
			logger.logError(e.getLocalizedMessage());
			return null;
		}
	}

	private Image getImage(URL imageURL) {
		Image image = null;
		ImageRegistry registry = Activator.getDefault().getImageRegistry();
		image = registry.get(imageURL.getPath());
		if (image != null) {
			return image;
		}
		ImageDescriptor desc = ImageDescriptor.createFromURL(imageURL);
		registry.put(imageURL.getPath(), desc);
		image = registry.get(imageURL.getPath());
		return image;
	}

	@Override
	public String getText(Object obj) {

		StructuredSelection selection = (StructuredSelection) obj;

		Object element = Utilities.getObject(selection.getFirstElement());

		IItemLabelProvider labelProvider = (IItemLabelProvider) this.adapterFactory
				.adapt(element, IItemLabelProvider.class);
		if (labelProvider != null) {
			if (element instanceof EntityType) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_EntityType);
			}
			if (element instanceof EntitySet) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_EntitySet);
			}
			if (element instanceof Property) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_Property);
			}
			if (element instanceof Association) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_Association);
			}
			if (element instanceof AssociationSet) {
				Association association = ((AssociationSet) element)
						.getAssociation();
				// if association set is changed then selection will be null
				// hence not showing any text
				if (null == association) {
					return "";
				} else {
					labelProvider = (IItemLabelProvider) this.adapterFactory
							.adapt(association, IItemLabelProvider.class);

					return createHeaderText(labelProvider.getText(association),
							Messages.TabbedPropertyHeaderProvider_Association);
				}
			}
			if (element instanceof NavigationProperty) {
				return createHeaderText(
						labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_NavigationProperty);
			}
			if (element instanceof ComplexType) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_ComplexType);
			}
			if (element instanceof FunctionImport) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_FunctionImport);
			}
			if (element instanceof Parameter) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_Parameter);
			}
			if (element instanceof Schema) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_Schema);
			}
			if (element instanceof EnumType) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_EnumType);
			}
			if (element instanceof EnumMember) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_EnumMember);
			}
			if (element instanceof IFunctionReturnTypeUsage) {
				return Messages.TabbedPropertyHeaderProvider_Function_Import_Return_Type;
			}
			if (element instanceof ValueTerm) {
				return createHeaderText(labelProvider.getText(element),
						Messages.TabbedPropertyHeaderProvider_ValueTerm);
			}

		}
		return super.getText(element);

	}

	private String createHeaderText(String elementLabel, String typeLabel) {
		return elementLabel + "   " + typeLabel; //$NON-NLS-1$
	}

	@Override
	public void dispose() {
		super.dispose();
		AdapterFactory factory = getAdapterFactory();
		if (factory instanceof IDisposable) {
			((IDisposable) factory).dispose();
		}
	}
}
