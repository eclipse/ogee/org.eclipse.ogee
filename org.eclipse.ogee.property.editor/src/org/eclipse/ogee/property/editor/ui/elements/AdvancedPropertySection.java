/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements;

import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.swt.accessibility.AccessibleAdapter;
import org.eclipse.swt.accessibility.AccessibleEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

public class AdvancedPropertySection extends
		org.eclipse.ui.views.properties.tabbed.AdvancedPropertySection {

	private String firstColumnName;

	public AdvancedPropertySection(String firstCoulmName) {
		this.firstColumnName = firstCoulmName;
	}

	@Override
	public void createControls(Composite parent,
			final TabbedPropertySheetPage atabbedPropertySheetPage) {
		super.createControls(parent, atabbedPropertySheetPage);

		final Tree tree = (Tree) page.getControl();
		TreeColumn[] columns = tree.getColumns();
		columns[0].setText(firstColumnName);

		tree.getAccessible().addAccessibleListener(new AccessibleAdapter() {
			@Override
			public void getName(AccessibleEvent e) {
				e.result = tree.getSelection()[0].getText();
			}
		});

		setActionBars(atabbedPropertySheetPage.getSite().getActionBars());

	}

	public void setActionBars(IActionBars actionBars) {
		actionBars.getMenuManager().removeAll();
		actionBars.getToolBarManager().removeAll();		
		actionBars.getToolBarManager().update(true);
		actionBars.getMenuManager().update(true);

		Utilities.setStatusLineManager(actionBars.getStatusLineManager());

	}

	public String getFirstColumnName() {
		return firstColumnName;
	}

}
