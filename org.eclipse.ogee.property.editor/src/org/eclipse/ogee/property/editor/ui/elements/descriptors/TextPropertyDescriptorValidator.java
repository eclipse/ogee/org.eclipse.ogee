/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.descriptors;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;

public class TextPropertyDescriptorValidator extends
		AbstractPropertyDescriptorValidator {

	protected TextPropertyDescriptor textPropertyDescriptor;

	public TextPropertyDescriptorValidator(Object id, String displayName,
			EDMTypes edmType) {
		super(id, displayName, edmType);
		textPropertyDescriptor = new TextPropertyDescriptor(id, displayName);

		switch (edmType) {
		case DATE_TIME:
		case DATE_TIME_OFFSET:
		case TIME:
		case GUID:
			textPropertyDescriptor.setValidator(getCellEditorDateValidator());
			break;
		default:
			textPropertyDescriptor.setValidator(getCellEditorValidator());
			break;
		}

	}

	@Override
	public CellEditor getCellEditor(final Composite parent) {
		return textPropertyDescriptor.createPropertyEditor(parent);
	}

	@Override
	public String getCategory() {
		return textPropertyDescriptor.getCategory();
	}

	@Override
	public String getDescription() {
		return textPropertyDescriptor.getDescription();
	}

	@Override
	public String getDisplayName() {
		return textPropertyDescriptor.getDisplayName();
	}

	@Override
	public String[] getFilterFlags() {
		return textPropertyDescriptor.getFilterFlags();
	}

	@Override
	public Object getHelpContextIds() {
		return textPropertyDescriptor.getHelpContextIds();
	}

	@Override
	public Object getId() {
		return textPropertyDescriptor.getId();
	}

	@Override
	public ILabelProvider getLabelProvider() {
		return textPropertyDescriptor.getLabelProvider();
	}

	@Override
	public boolean isCompatibleWith(IPropertyDescriptor anotherProperty) {
		return textPropertyDescriptor.isCompatibleWith(anotherProperty);
	}

	@Override
	public boolean isLabelProviderSet() {
		return textPropertyDescriptor.isLabelProviderSet();
	}

	@Override
	public void setAlwaysIncompatible(boolean flag) {
		textPropertyDescriptor.setAlwaysIncompatible(flag);
	}

	@Override
	public void setCategory(String category) {
		textPropertyDescriptor.setCategory(category);
	}

	@Override
	public void setDescription(String description) {
		textPropertyDescriptor.setDescription(description);
	}

	@Override
	public void setFilterFlags(String[] value) {
		textPropertyDescriptor.setFilterFlags(value);
	}

	@Override
	public void setHelpContextIds(Object contextIds) {
		textPropertyDescriptor.setHelpContextIds(contextIds);
	}

	@Override
	public void setLabelProvider(ILabelProvider provider) {
		textPropertyDescriptor.setLabelProvider(provider);
	}

	@Override
	public void setValidator(ICellEditorValidator validator) {
		textPropertyDescriptor.setValidator(validator);
	}

}
