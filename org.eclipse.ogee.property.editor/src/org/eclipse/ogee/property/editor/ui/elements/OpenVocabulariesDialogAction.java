/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ogee.property.editor.ui.elements.dialog.VocabulariesModelDialog;
import org.eclipse.ogee.property.editor.utilities.WorkbenchUtilities;
import org.eclipse.ui.PlatformUI;

public class OpenVocabulariesDialogAction extends Action {

	private EObject eObject;

	public OpenVocabulariesDialogAction(String text) {
		super(text);
	}

	@Override
	public void run() {

		VocabulariesModelDialog dialog = new VocabulariesModelDialog(PlatformUI
				.getWorkbench().getActiveWorkbenchWindow().getShell(), eObject,
				WorkbenchUtilities.isReadOnlyMode(eObject));

		dialog.open();

	}

	public void setSelectedEObject(EObject eObject) {
		this.eObject = eObject;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return super.getImageDescriptor();
	}

}
