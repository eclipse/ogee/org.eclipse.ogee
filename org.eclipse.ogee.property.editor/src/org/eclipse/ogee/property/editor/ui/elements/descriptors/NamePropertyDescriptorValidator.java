/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.descriptors;

import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.property.editor.utilities.Validator;

public class NamePropertyDescriptorValidator extends
		TextPropertyDescriptorValidator {

	private Object id = null;

	public NamePropertyDescriptorValidator(Object id, String displayName) {
		super(id, displayName, EDMTypes.STRING);
		this.id = id;
	}

	@Override
	protected String validate(String string) {
		// For alias the name check should not include empty check as the alias
		// name can be empty
		if (null != id && id.toString().equalsIgnoreCase("ALIAS")) {
			return Validator.validateAliasName(string);
		}
		if (null != id && id.toString().equalsIgnoreCase("NAMESPACE")) {
			return Validator.validateNamespaceName(string);
		} else {
			return Validator.validateName(string);
		}
	}

}
