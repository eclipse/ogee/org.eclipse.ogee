/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.descriptors;

import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DialogContext;
import org.eclipse.ogee.property.editor.ui.elements.dialog.IReturnValue;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * a new Font dialog cell editor.
 * 
 */
public class ButtonCellEditor extends DialogCellEditor {

	private DialogContext context;

	protected ButtonCellEditor(Composite parent, DialogContext context) {
		super(parent);
		this.context = context;
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		context.openDialog();

		return context.getReturnValue();
	}

	@Override
	protected void updateContents(Object value) {
		String message = Messages.EMPTY_String;

		if (value instanceof String) {
			message = (String) value;
		} else if (value instanceof IReturnValue) {
			message = ((IReturnValue) value).getText();
		}

		getDefaultLabel().setText(message);

	}
}
