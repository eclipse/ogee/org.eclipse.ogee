/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.dialog;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.WorkbenchUtilities;
import org.eclipse.ogee.property.editor.vocabulary.HandleValueAnnotation;
import org.eclipse.ogee.property.editor.vocabulary.ValueTermExt;
import org.eclipse.ogee.property.editor.vocabulary.VocabulariesTreeModel;
import org.eclipse.ogee.property.editor.vocabulary.VocabularyTreeContentProvider;
import org.eclipse.ogee.property.editor.vocabulary.VocabularyTreeNodeLabelProvider;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class VocabulariesModelDialog extends CommonStatusDialog {
	private Logger logger = Logger.getLogger(Activator.PLUGIN_ID);

	private static final int LEVEL_OF_EXPAND = 1;
	private CheckboxTreeViewer treeViewer;
	private EObject eObject;
	private VocabularyTreeContentProvider contentProvider;
	private VocabulariesTreeModel vocabulariesTreeModel;
	private Set<ValueTermExt> changedElements = new HashSet<ValueTermExt>();

	public VocabulariesModelDialog(Shell parent, EObject eobject,
			boolean isReadOnly) {
		super(parent, Messages.VocabulariesDialog_Title, true, isReadOnly);
		this.eObject = eobject;
		contentProvider = new VocabularyTreeContentProvider(eObject);
		vocabulariesTreeModel = new VocabulariesTreeModel(
				SchemaHandler.getEDMXSet(eobject), eobject);
		setTopExplenation(Messages.VocabulariesModelDialog_Top_Explanation);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);

		// change the Dialog Icon
		setImage(Activator.IMG_VOCABULARY.createImage(true));

		CreateVocabularyContainer(getMainContainer());

		return composite;
	}

	private void CreateVocabularyContainer(Composite composite) {
		composite.setLayout(new GridLayout(1, false));

		if (!isVocabulariesApplic()) {
			createInfoMessage(composite);
		} else {
			createTreeViewer(composite);
		}

	}

	private void createInfoMessage(Composite composite) {
		GridData gd = new GridData(getDialog_width(), 300);
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;
		gd.grabExcessVerticalSpace = true; // Layout vertically, too!
		gd.verticalAlignment = GridData.FILL;

		Label messageLabel = new Label(composite, SWT.WRAP | SWT.BORDER);
		messageLabel.setText(addMessageText());
		messageLabel.setLayoutData(gd);
	}

	private String addMessageText() {
		List<IVocabulary> vocabularies = vocabulariesTreeModel
				.getVocabularies();

		StringBuilder stringBuild = new StringBuilder();

		for (IVocabulary iVocabulary : vocabularies) {
			try {
				stringBuild.append(iVocabulary.getSchema().getNamespace())
						.append(System.getProperty("line.separator")); //$NON-NLS-1$

			} catch (ModelAPIException e) {
				logger.logError(e.getLocalizedMessage());
			}
		}

		return (String
				.format("%s %n%s ", Messages.VocabulariesDialog_No_applicable_terms_found, stringBuild.toString())); //$NON-NLS-1$

	}

	private void createTreeViewer(Composite composite) {
		GridData gdTree = new GridData(getDialog_width(), 300);
		gdTree.grabExcessHorizontalSpace = true;
		gdTree.horizontalAlignment = GridData.FILL;
		gdTree.grabExcessVerticalSpace = true; // Layout vertically, too!
		gdTree.verticalAlignment = GridData.FILL;

		treeViewer = new CheckboxTreeViewer(composite);
		treeViewer.getTree().setLayoutData(gdTree);

		treeViewer.setLabelProvider(new VocabularyTreeNodeLabelProvider());
		// Expand the tree
		treeViewer.setAutoExpandLevel(LEVEL_OF_EXPAND);
		// Provide the input to the ContentProvider

		treeViewer.setContentProvider(contentProvider);
		treeViewer.setInput(vocabulariesTreeModel);

		checkVocabularyElelemts();

		// Add listeners to treeViewer
		addTreeViewerListener();
		addTreeViewerCheckStateListener();
		addTreeViewerSelectionChangedListener();

	}

	private void checkVocabularyElelemts() {
		// if all children are checked when loading than check parent
		for (IVocabulary vocabulary : contentProvider.getVocabularyList()) {
			if (contentProvider.getCheckedValueTermsExt().get(vocabulary)
					.size() == contentProvider.getChildren(vocabulary).length) {
				treeViewer.setChecked(vocabulary, true);
			} else if (!contentProvider.getCheckedValueTermsExt()
					.get(vocabulary).isEmpty()) {
				treeViewer.setGrayChecked(vocabulary, true);
			}
		}

	}

	private void addTreeViewerSelectionChangedListener() {
		// when an element is choose it's documentation will be display
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				// if selection is empty clear label
				if (event.getSelection().isEmpty()) {
					getDetailsText().setText(Messages.EMPTY_String);
					return;
				}
				if (event.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) event
							.getSelection();

					if (selection.getFirstElement() instanceof ValueTermExt) {
						ValueTerm valueTerm = ((ValueTermExt) (selection
								.getFirstElement())).getValueTerm();
						Documentation document = valueTerm.getDocumentation();

						if (document != null) {
							getDetailsText().setText(document.getSummary());

						} else {
							getDetailsText()
									.setText(
											Messages.VocabulariesDialog_No_description_available); // Documentation not available
							
						}
					} else {
						getDetailsText().setText(Messages.EMPTY_String);
					}
				}

			}
		});

	}

	private void addTreeViewerCheckStateListener() {

		// When an element is checked - checked is children
		treeViewer.addCheckStateListener(new ICheckStateListener() {

			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				if (event.getElement() instanceof IVocabulary) {
					treeViewer.setGrayed(event.getElement(), false);
					Object[] children = contentProvider.getChildren(event
							.getElement());
					for (int i = 0; i < children.length; i++) {
						((ValueTermExt) (children[i])).setChecked(event
								.getChecked());
						if (!changedElements
								.contains((ValueTermExt) children[i])) {

							changedElements.add((ValueTermExt) children[i]);
						}
					}

					treeViewer.setSubtreeChecked(event.getElement(),
							event.getChecked());

					updateStatus(new Status(IStatus.OK, Activator.PLUGIN_ID,
							Messages.EMPTY_String));

				} else if (event.getElement() instanceof ValueTermExt) {

					((ValueTermExt) (event.getElement())).setChecked(event
							.getChecked());
					treeViewer.setGrayChecked(((ValueTermExt) (event
							.getElement())).getVocabulary(), true);

					if (!changedElements.contains((ValueTermExt) event
							.getElement())) {
						changedElements.add((ValueTermExt) event.getElement());
					}
					updateStatus(new Status(IStatus.OK, Activator.PLUGIN_ID,
							Messages.EMPTY_String));

					if (!event.getChecked()) {
						boolean isOneChecked = false;

						Object[] children = contentProvider
								.getChildren(((ValueTermExt) (event
										.getElement())).getVocabulary());
						for (int i = 0; i < children.length; i++) {
							if (((ValueTermExt) (children[i])).isChecked() == true) {
								isOneChecked = true;
								break;
							}
						}
						if (!isOneChecked) {
							treeViewer.setGrayChecked(((ValueTermExt) event
									.getElement()).getVocabulary(), false);
						}

					}

				}

			}
		});

	}

	private void addTreeViewerListener() {
		// When the Tree is expended set the checked children
		treeViewer.addTreeListener(new ITreeViewerListener() {

			@Override
			public void treeExpanded(TreeExpansionEvent arg0) {
				if (arg0.getElement() instanceof IVocabulary) {

					List<ValueTermExt> checkedelements = contentProvider
							.getCheckedValueTermsExt().get(arg0.getElement());
					if (checkedelements.size() == 0
							|| contentProvider
									.isAllCheckedValueTermsExtUnChecked((IVocabulary) arg0
											.getElement())) {
						return;
					}

					Object[] prevChecked = treeViewer.getCheckedElements();
					int tmpSize = prevChecked.length;
					prevChecked = Arrays.copyOf(prevChecked, prevChecked.length
							+ checkedelements.size());

					System.arraycopy(checkedelements.toArray(), 0, prevChecked,
							tmpSize, checkedelements.size());

					((CheckboxTreeViewer) arg0.getTreeViewer())
							.setCheckedElements(prevChecked);
				}

			}

			@Override
			public void treeCollapsed(TreeExpansionEvent arg0) {

			}
		});

	}

	private boolean isVocabulariesApplic() {

		contentProvider.getElements(vocabulariesTreeModel);

		return (!contentProvider.getVocabularyList().isEmpty());

	}

	@Override
	protected void okPressed() {
		Map<IVocabulary, List<ValueTermExt>> checkedValueTermsExt = ((VocabularyTreeContentProvider) treeViewer
				.getContentProvider()).getCheckedValueTermsExt();

		HandleValueAnnotation.updateValueAnnotation(eObject,
				treeViewer.getCheckedElements(), checkedValueTermsExt,
				changedElements);
		WorkbenchUtilities.refreshAndSetAnnotationsTab();
		this.close();
	}

}
