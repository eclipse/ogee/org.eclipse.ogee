/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.dialog;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.associationset.AssociationSetCellModifier;
import org.eclipse.ogee.property.editor.associationset.AssociationSetContentProvider;
import org.eclipse.ogee.property.editor.associationset.AssociationSetEntry;
import org.eclipse.ogee.property.editor.associationset.AssociationSetLabelProvider;
import org.eclipse.ogee.property.editor.associationset.AssociationSetModel;
import org.eclipse.ogee.property.editor.associationset.AssociationSetSorter;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

public class AssociationSetDialog extends CommonStatusDialog implements
		IPropertyDescriptorDialog {
	// Set the table column property names
	private final String NAME_COLUMN = Messages.AssociationSetDialog_NAME;
	private final String END1_COLUMN = Messages.AssociationSetDialog_END1;
	private final String END2_COLUMN = Messages.AssociationSetDialog_END2;

	// Set column names
	private String[] columnNames = new String[] { NAME_COLUMN, END1_COLUMN,
			END2_COLUMN, };
	private Table table;
	private TableViewer tableViewer;
	private Button addButton;
	private Button deleteButton;
	Association association;
	private AssociationSetModel associationSetModel;
	private ViewerSorter nameSorter = new AssociationSetSorter(
			AssociationSetSorter.NAME);
	private ViewerSorter end1Sorter = new AssociationSetSorter(
			AssociationSetSorter.END1);
	private ViewerSorter end2Sorter = new AssociationSetSorter(
			AssociationSetSorter.END2);

	public AssociationSetDialog(Shell parent, Association association,
			boolean isReadOnly) {
		super(parent, Messages.AssociationSetDialog_TITLE, false, isReadOnly);
		this.association = association;
		associationSetModel = new AssociationSetModel(association);
		setTopExplenation(Messages.AssociationSetDialog_TOP_EXPLENATION);
		setDialog_width(500);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		// change the Dialog Icon
		setImage(Activator.IMG_ASSOCIATIONSET.createImage(true));
		createAssociationContainer(getMainContainer());
		updateTableButtonStatus();
		return composite;
	}

	private void createAssociationContainer(Composite composite) {

		composite.setLayout(new GridLayout(3, false));

		// Create the table
		createTable(composite);

		// Add the buttons
		createTableButtons(composite);

		// Create and setup the TableViewer
		createTableViewer();

		// Set TableViewer Data providers
		setTableDataProvider();

	}

	private void createTable(Composite parent) {
		int style = SWT.SINGLE | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION | SWT.HIDE_SELECTION;

		table = new Table(parent, style);

		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalSpan = 3;
		table.setLayoutData(gridData);

		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		// 1st column with task Description
		TableColumn column = new TableColumn(table, SWT.LEFT, 0);
		column.setText(NAME_COLUMN);
		column.setWidth(150);
		column.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				tableViewer.setSorter(nameSorter);
			}
		});

		// 2nd column with End1
		column = new TableColumn(table, SWT.LEFT, 1);
		column.setText(associationSetModel.getEndEntityName(0, true));
		column.setWidth(150);
		column.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				tableViewer.setSorter(end1Sorter);
			}
		});

		// 3rd column column with End2
		column = new TableColumn(table, SWT.LEFT, 2);
		column.setText(associationSetModel.getEndEntityName(1, true));
		column.setWidth(150);
		column.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				tableViewer.setSorter(end2Sorter);
			}
		});

	}

	private void createTableButtons(Composite parent) {
		// Create and configure the "Add" button
		addButton = new Button(parent, SWT.PUSH | SWT.CENTER);
		addButton.setText(Messages.AssociationSetDialog_ADD_BUTTON_TEXT);

		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_END);
		gridData.widthHint = 80;
		addButton.setLayoutData(gridData);

		addButton.addSelectionListener(new SelectionAdapter() {

			// Add association and refresh the view
			public void widgetSelected(SelectionEvent e) {
				addAssociationSet();
			}
		});

		// Create and configure the "Delete" button
		deleteButton = new Button(parent, SWT.PUSH | SWT.CENTER);
		deleteButton.setText(Messages.AssociationSetDialog_REMOVE_BUTTON_TEXT);
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		gridData.widthHint = 80;
		deleteButton.setLayoutData(gridData);

		deleteButton.addSelectionListener(new SelectionAdapter() {

			// Remove the selection and refresh the view
			public void widgetSelected(SelectionEvent e) {
				AssociationSetEntry associationSetEntry = (AssociationSetEntry) ((IStructuredSelection) tableViewer
						.getSelection()).getFirstElement();
				if (associationSetEntry != null) {
					removeAssociationSet(associationSetEntry);
				}
			}

		});

	}

	private void createTableViewer() {
		tableViewer = new TableViewer(table);
		tableViewer.setUseHashlookup(true);

		tableViewer.setColumnProperties(columnNames);

		// Create the cell editors
		CellEditor[] editors = new CellEditor[columnNames.length];

		// Column 1 : Name (Free text)
		TextCellEditor textEditor = new TextCellEditor(table);
		((Text) textEditor.getControl()).setTextLimit(128);
		((Text) textEditor.getControl())
				.addVerifyListener(new VerifyListener() {
					@Override
					public void verifyText(VerifyEvent e) {
						if (e.text.length() > 0)
							e.doit = true;
					}
				});
		editors[0] = textEditor;

		// Column 2 : End1 (Combo Box)
		editors[1] = new ComboBoxCellEditor(table,
				associationSetModel.getEndOptions(0), SWT.READ_ONLY);

		// Column 3 : End2 (Combo Box)
		editors[2] = new ComboBoxCellEditor(table,
				associationSetModel.getEndOptions(1), SWT.READ_ONLY);

		// Assign the cell editors to the viewer
		tableViewer.setCellEditors(editors);

		// Set the cell modifier for the viewer
		tableViewer.setCellModifier(new AssociationSetCellModifier(this,
				associationSetModel, isReadOnly()));

		// Set the default sorter for the viewer - by name
		tableViewer.setSorter(nameSorter);

	}

	private void addAssociationSet() {

		associationSetModel.addNewAssociationSetEntry();
		tableViewer.refresh();
		if (!deleteButton.getEnabled()) {
			deleteButton.setEnabled(true);
		}
		String message = associationSetModel.validate();
		if (message == null) {
			enableApplyButton(true, Messages.EMPTY_String);
		} else {
			enableApplyButton(false, message);
		}

	}

	private void removeAssociationSet(AssociationSetEntry associationSetEntry) {
		associationSetModel.removeAssociationSet(associationSetEntry);
		tableViewer.refresh();

		if (associationSetModel.getAssociationSets().size() == 0) {
			deleteButton.setEnabled(false);
		}
		String message = associationSetModel.validate();
		if (message == null) {
			enableApplyButton(true, Messages.EMPTY_String);
		} else {
			enableApplyButton(false, message);
		}
	}

	private void setTableDataProvider() {
		tableViewer.setContentProvider(new AssociationSetContentProvider());
		tableViewer.setLabelProvider(new AssociationSetLabelProvider());
		tableViewer.setInput(associationSetModel);
	}

	private void updateTableButtonStatus() {
		addButton.setEnabled(!isReadOnly());
		deleteButton.setEnabled(!isReadOnly());
	}

	@Override
	public IReturnValue getReturnValue() {
		return associationSetModel;
	}

	public List<String> getColumnNames() {
		return Arrays.asList(columnNames);
	}

	public TableViewer getTableViewer() {
		return tableViewer;
	}

	@Override
	protected void cancelPressed() {
		associationSetModel.setStatus(CANCEL);
		super.cancelPressed();
	}

	@Override
	protected Control createButtonBar(Composite parent) {
		super.createButtonBar(parent);
		// Set focus on Cancel
		getButton(CANCEL).setFocus();
		return parent;
	}

}
