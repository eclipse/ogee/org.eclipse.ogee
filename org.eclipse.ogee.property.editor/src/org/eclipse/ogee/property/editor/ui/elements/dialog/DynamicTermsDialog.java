/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.dialog;

import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.FocusCellOwnerDrawHighlighter;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.TreeViewerEditor;
import org.eclipse.jface.viewers.TreeViewerFocusCellManager;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.annotations.AnnotationTabRecord;
import org.eclipse.ogee.property.editor.annotations.dynamic.AnnotationLabelProvider;
import org.eclipse.ogee.property.editor.annotations.dynamic.AnnotationValueLabelProvider;
import org.eclipse.ogee.property.editor.annotations.dynamic.DynamicAnnotationValueEditingSupport;
import org.eclipse.ogee.property.editor.annotations.dynamic.DynamicTermsModel;
import org.eclipse.ogee.property.editor.annotations.dynamic.DynamicTermsTreeContentProvider;
import org.eclipse.ogee.property.editor.annotations.dynamic.DynamicTermsTreeEntry;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class DynamicTermsDialog extends CommonStatusDialog implements
		IPropertyDescriptorDialog {

	private AnnotationTabRecord annotationTabRecord;
	private static final int DIALOG_WIDTH = 600;
	private TreeViewer termsTree;
	private DynamicTermsModel dynamicTermsModel;
	private Button addButton;
	private Button deleteButton;

	public DynamicTermsDialog(Shell parent,
			AnnotationTabRecord annotationTabRecord, boolean isReadOnly) {
		super(parent, Messages.DynamicTermsDialog_MaintainDynamicAnnotations,
				true, isReadOnly);
		this.annotationTabRecord = annotationTabRecord;
		setTopExplenation(Messages.DynamicTermsDialog_AddRemoveMaintainTermValues);
		setDialog_width(DIALOG_WIDTH);
		dynamicTermsModel = new DynamicTermsModel(annotationTabRecord);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		getDetailsText().setText(annotationTabRecord.getDocumentationSummery());
		setImage(Activator.IMG_VOCABULARY.createImage(true)); // change the  dialog Icon																
		createTermsContainer(getMainContainer());
		return composite;
	}

	private void createTermsContainer(Composite mainContainer) {
		mainContainer.setLayout(new GridLayout(3, false));
		createTermsTree(mainContainer);
		createTreeButtons(mainContainer);
	}

	private void createTreeButtons(Composite parent) {
		// Create and configure the "Add" button
		addButton = new Button(parent, SWT.PUSH | SWT.CENTER);
		addButton.setText(Messages.DynamicTermsDialog_AddItem);

		addButtonSelectionListener(addButton);

		// Create and configure the "Delete" button
		deleteButton = new Button(parent, SWT.PUSH | SWT.CENTER);
		deleteButton.setText(Messages.DynamicTermsDialog_RemoveItem);
		deleteButtonSelectionListener(deleteButton);

		GridData gridData = new GridData();
		gridData.widthHint = 80;
		addButton.setLayoutData(gridData);
		deleteButton.setLayoutData(gridData);

		// Load the dialog with disabled buttons
		addButton.setEnabled(false);
		deleteButton.setEnabled(false);

	}

	private void deleteButtonSelectionListener(Button deleteButton) {
		deleteButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {

				TreeSelection selection = (TreeSelection) termsTree
						.getSelection();
				if (!selection.isEmpty()) {
					dynamicTermsModel.remove((DynamicTermsTreeEntry) selection
							.getFirstElement());
					enableApplyButton(true, ""); //$NON-NLS-1$
					((Button) arg0.getSource()).setEnabled(false);
					termsTree.refresh();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {

			}
		});

	}

	private void addButtonSelectionListener(Button addButton) {
		addButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				DynamicTermsTreeEntry firstElement = (DynamicTermsTreeEntry) ((TreeSelection) termsTree
						.getSelection()).getFirstElement();

				dynamicTermsModel.addChild(firstElement);
				termsTree.refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// ((DynamicTermsTreeContentProvider)
				// termsTree.getContentProvider()).addElement(annotationTabRecord);
			}
		});

	}

	private void createTermsTree(Composite mainContainer) {

		termsTree = new TreeViewer(mainContainer, SWT.BORDER
				| SWT.FULL_SELECTION);
		termsTree.getTree().setLinesVisible(true);
		termsTree.getTree().setHeaderVisible(true);

		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.horizontalSpan = 3;
		termsTree.getTree().setLayoutData(gridData);

		createTreeColumns();
		termsTree.setContentProvider(new DynamicTermsTreeContentProvider(
				dynamicTermsModel, isReadOnly()));
		termsTree.setInput(dynamicTermsModel);
		// Support of disable and Enable add/remove buttons
		termsTree.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				DynamicTermsTreeEntry entry = (DynamicTermsTreeEntry) ((TreeSelection) arg0
						.getSelection()).getFirstElement();
				if (isReadOnly() || entry == null) {
					return;
				}

				if (entry.isCollectionParent()) {
					addButton.setEnabled(true);
					deleteButton.setEnabled(false);
				} else if (entry.getParent() != null
						&& entry.getParent().isCollectionParent()
						&& entry.getDisplayValue() != "") { //$NON-NLS-1$
					addButton.setEnabled(false);
					deleteButton.setEnabled(true);
				} else {
					addButton.setEnabled(false);
					deleteButton.setEnabled(false);
				}
			}
		});

		termsTree.expandAll();

	}

	private void createTreeColumns() {
		TreeViewerFocusCellManager focusCellManager = new TreeViewerFocusCellManager(
				termsTree, new FocusCellOwnerDrawHighlighter(termsTree));
		ColumnViewerEditorActivationStrategy actSupport = new ColumnViewerEditorActivationStrategy(
				termsTree) {
			protected boolean isEditorActivationEvent(
					ColumnViewerEditorActivationEvent event) {
				return event.eventType == ColumnViewerEditorActivationEvent.MOUSE_CLICK_SELECTION
						|| event.eventType == ColumnViewerEditorActivationEvent.MOUSE_DOUBLE_CLICK_SELECTION
						|| (event.eventType == ColumnViewerEditorActivationEvent.KEY_PRESSED && event.keyCode == SWT.CR)
						|| event.eventType == ColumnViewerEditorActivationEvent.PROGRAMMATIC;
			}
		};
		//
		TreeViewerEditor.create(termsTree, focusCellManager, actSupport,
				ColumnViewerEditor.TABBING_HORIZONTAL
						| ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR
						| ColumnViewerEditor.TABBING_VERTICAL
						| ColumnViewerEditor.KEYBOARD_ACTIVATION);

		// Create the first Column

		TreeViewerColumn column = new TreeViewerColumn(termsTree, SWT.NONE);
		column.getColumn().setWidth(350);
		column.getColumn().setText(createFirsColunmText());

		column.setLabelProvider(new AnnotationLabelProvider());

		// Create the Value column

		column = new TreeViewerColumn(termsTree, SWT.NONE);
		column.getColumn().setText(Messages.DynamicTermsDialog_ValueHeader);
		column.getColumn().setWidth(200);
		column.setEditingSupport(new DynamicAnnotationValueEditingSupport(
				column.getViewer(), annotationTabRecord, this));
		column.setLabelProvider(new AnnotationValueLabelProvider());

	}

	private String createFirsColunmText() {
		return (annotationTabRecord.getName());
	}

	@Override
	public IReturnValue getReturnValue() {
		return new IReturnValue() {

			@Override
			public String getText() {

				int size = ((ValueCollection) annotationTabRecord
						.getAnnotationValue()).getValues().size();

				return Utilities.createAnnotationCollectionString(size);

			}

			@Override
			public int getReturnStatus() {
				return 0;
			}
		};

	}

	@Override
	protected void okPressed() {
		dynamicTermsModel.updateTerms(annotationTabRecord);
		this.close();
	}

	@Override
	protected Control createButtonBar(Composite parent) {
		super.createButtonBar(parent);
		// Set focus on Cancel
		getButton(CANCEL).setFocus();
		return parent;
	}
}
