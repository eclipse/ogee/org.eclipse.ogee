/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.descriptors;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.swt.widgets.Composite;

public class TextComboBoxPropertyDescriptor extends
		AbstractPropertyDescriptorValidator {

	private String[] values;

	public TextComboBoxPropertyDescriptor(Object id, String displayName,
			String[] values, EDMTypes edmType) {
		super(id, displayName, edmType);
		this.values = values;

		switch (edmType) {
		case DATE_TIME:
		case DATE_TIME_OFFSET:
		case TIME:
		case GUID:
			setValidator(getCellEditorDateValidator());
			break;
		default:
			setValidator(getCellEditorValidator());
			break;
		}

	}

	public TextComboBoxPropertyDescriptor(Object id, String displayName,
			List<String> values, EDMTypes edmType) {
		this(id, displayName, values.toArray(new String[values.size()]),
				edmType);
	}

	@Override
	public CellEditor getCellEditor(Composite parent) {
		return new TextComboCellEditor(parent, values);
	}

	@Override
	protected String validate(String string) {
		if (Arrays.asList(values).contains(string)) {
			return null;
		}
		return super.validate(string);
	}

}
