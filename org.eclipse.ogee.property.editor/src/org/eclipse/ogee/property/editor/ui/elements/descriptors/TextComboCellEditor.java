/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.descriptors;

import java.text.MessageFormat;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Listener;

public class TextComboCellEditor extends ComboBoxCellEditor {

	private String value;

	private CCombo comboBox;

	public TextComboCellEditor(Composite parent, String[] items) {
		super(parent, items);
		setItems(items);
	}

	@Override
	protected Control createControl(Composite parent) {
		comboBox = (CCombo) super.createControl(parent);

		for (Listener listener : comboBox.getListeners(SWT.KeyDown)) {
			comboBox.removeListener(SWT.KeyDown, listener);
		}
		comboBox.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				keyReleaseOccured(e);
			}
		});

		for (Listener listener : comboBox.getListeners(SWT.DefaultSelection)) {
			comboBox.removeListener(SWT.DefaultSelection, listener);

		}
		for (Listener listener : comboBox.getListeners(SWT.Selection)) {
			comboBox.removeListener(SWT.Selection, listener);

		}
		comboBox.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent event) {
				applyValueAndDeactivate();
			}

			public void widgetSelected(SelectionEvent event) {
				value = comboBox.getText();
			}
		});

		return comboBox;

	}

	@Override
	protected Object doGetValue() {
		return value;
	}

	@Override
	protected void doSetValue(Object value) {
		Assert.isTrue(comboBox != null && value instanceof String);
		if (value instanceof String) {
			this.value = (String) value;
			comboBox.setText((String) value);
		}
	}

	@Override
	protected void focusLost() {
		if (isActivated()) {
			applyValueAndDeactivate();
		}
	}

	@Override
	protected void keyReleaseOccured(KeyEvent keyEvent) {
		if (keyEvent.character == '\u001b') {
			fireCancelEditor();
		} else if (keyEvent.character == '\t' || keyEvent.character == '\n') {
			applyValueAndDeactivate();
		} else {
			editOccured();
		}
	}

	private void applyValueAndDeactivate() {
		value = comboBox.getText();
		markDirty();
		boolean isValid = true;
		if (comboBox.getSelectionIndex() == -1) {
			isValid = isCorrect(value);
		}
		setValueValid(isValid);

		if (!isValid) {
			setErrorMessage(MessageFormat.format(getErrorMessage(),
					new Object[] { Messages.TextComboCellEditor_WrongValue }));
		}

		fireApplyEditorValue();

		deactivate();
	}

	protected void editOccured() {
		value = comboBox.getText();
		if (value == null) {
			value = "";//$NON-NLS-1$
		}
		Object typedValue = value;
		boolean oldValidState = isValueValid();
		boolean newValidState = isCorrect(typedValue);
		if (!newValidState) {
			setErrorMessage(MessageFormat.format(getErrorMessage(),
					new Object[] { value }));
		}
		valueChanged(oldValidState, newValidState);
	}

}