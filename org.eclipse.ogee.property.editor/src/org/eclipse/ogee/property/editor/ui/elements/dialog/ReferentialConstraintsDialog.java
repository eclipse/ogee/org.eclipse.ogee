/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.dialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.StatusDialog;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Multiplicity;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.utilities.RefConstraintHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class ReferentialConstraintsDialog extends StatusDialog implements
		IPropertyDescriptorDialog {

	private static final int DELETE_BUTTON_ID = IDialogConstants.CLIENT_ID;
	private static final int ROW_NUMBER = 10;
	private static final int EDITABLECOLUMN = 1;
	private static final int UNEDITABLECOLUMN = 0;

	private boolean isReadOnlyMode;
	private EntityType[] entityTypesArr;
	private Association association;
	private Text dependentNameText;
	private Combo combo;
	private Table table;

	private TableEditor editor;
	private SelectionListener tableListener;
	private SelectionListener comboListener;

	private int principleIndex;
	private int dependentIndex;
	private HashMap<String, String> refConstraintMap = new HashMap<String, String>();
	private RefConstraintHandler refConstraintHandler;

	public ReferentialConstraintsDialog(Shell parent, Association association,
			boolean isReadOnly) {
		super(parent);
		this.association = association;
		this.entityTypesArr = new EntityType[] {
				association.getEnds().get(0).getType(),
				association.getEnds().get(1).getType() };
		this.isReadOnlyMode = isReadOnly;

		setShellStyle(SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL
				| SWT.CLOSE);
		setHelpAvailable(false);
		setDialogHelpAvailable(false);
		setStatusLineAboveButtons(true);
		updateStatus(new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				Messages.EMPTY_String));
		if (this.isReadOnlyMode) {
			setTitle(Messages.RefConstraintsDialog_Title
					+ Messages.CommonStatusDialog_ReadOnly);
		} else {
			setTitle(Messages.RefConstraintsDialog_Title);
		}
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		Button deleteButton = createButton(parent, DELETE_BUTTON_ID,
				Messages.ReferentialConstraintsDialog_Delete, false);
		if (isReadOnlyMode && deleteButton != null) {
			deleteButton.setEnabled(false);
		}
		super.createButtonsForButtonBar(parent);

		getButton(CANCEL).setFocus();
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (DELETE_BUTTON_ID == buttonId) {
			deletePressed();
		} else {
			super.buttonPressed(buttonId);
		}
	}

	@Override
	protected void cancelPressed() {
		refConstraintMap.clear();
		refConstraintHandler = new RefConstraintHandler(refConstraintMap, null,
				null, association);
		refConstraintHandler.setStatus(CANCEL);
		super.cancelPressed();

	}

	@Override
	public void okPressed() {
		createMap();
		refConstraintHandler = new RefConstraintHandler(refConstraintMap,
				entityTypesArr[dependentIndex], entityTypesArr[principleIndex],
				association);
		refConstraintHandler.setStatus(OK);
		close();
	}

	private void deletePressed() {
		refConstraintMap.clear();
		refConstraintHandler = new RefConstraintHandler(refConstraintMap, null,
				null, association);
		refConstraintHandler.setStatus(OK);
		close();
	}

	private void createMap() {
		refConstraintMap.clear();
		for (TableItem item : table.getItems()) {
			if (item.getText(UNEDITABLECOLUMN).equals(Messages.EMPTY_String)) {
				break;
			}
			refConstraintMap.put(item.getText(UNEDITABLECOLUMN),
					item.getText(EDITABLECOLUMN));
		}
	}

	@Override
	public RefConstraintHandler getReturnValue() {
		return refConstraintHandler;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		setImage(Activator.IMG_ASSOCIATION.createImage(true));

		final Composite composite = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = (GridLayout) composite.getLayout();
		gridLayout.marginTop = 10;
		gridLayout.marginLeft = 15;
		gridLayout.marginRight = 15;
		gridLayout.horizontalSpacing = 5;
		gridLayout.verticalSpacing = 10;
		gridLayout.marginWidth = 5;
		gridLayout.marginHeight = 5;
		gridLayout.numColumns = 2;

		// host label
		Label lblHost = new Label(composite, SWT.NONE);
		lblHost.setText(Messages.RefConstraintsDialog_HostText);
		GridData gd_lblHost = new GridData(SWT.FILL, SWT.CENTER, true, false,
				2, 1);
		lblHost.setLayoutData(gd_lblHost);

		// Principle label
		createLabel(composite,
				String.format(Messages.ReferentialConstraintsDialog_Principle));

		// Dependent label
		createLabel(composite,
				String.format(Messages.ReferentialConstraintsDialog_Dependent));

		// Principle combo box
		createPrincipleCombo(composite);

		// Dependent Text
		this.dependentNameText = new Text(composite, SWT.BORDER | SWT.SINGLE
				| SWT.LEFT | SWT.READ_ONLY);
		GridData gd_dependenTtext = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		dependentNameText.setLayoutData(gd_dependenTtext);

		createTable(composite);

		composite.pack();

		checkIfRefConstraintSet();
		checkIfReadOnly();

		return composite;
	}

	private void createPrincipleCombo(final Composite composite) {
		this.combo = new Combo(composite, SWT.DROP_DOWN | SWT.BORDER
				| SWT.READ_ONLY);
		combo.add(entityTypesArr[0].getName());
		combo.add(entityTypesArr[1].getName());
		combo.setFocus();

		GridData gd_compo = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1);
		combo.setLayoutData(gd_compo);

		comboListener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateControls((Combo) e.getSource());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		};
		combo.addSelectionListener(comboListener);
	}

	private void createTable(final Composite composite) {
		this.table = new Table(composite, SWT.SINGLE | SWT.BORDER
				| SWT.FULL_SELECTION | SWT.V_SCROLL);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		// Create columns
		TableColumn columnPrinciple = new TableColumn(table, SWT.LEFT);
		columnPrinciple
				.setText(Messages.RefConstraintsDialog_PrincipleColumnTitle);
		columnPrinciple.setWidth(300);

		TableColumn columnDependent = new TableColumn(table, SWT.LEFT);
		columnDependent
				.setText(Messages.RefConstraintsDialog_DependentColumnTitle);
		columnDependent.setWidth(300);
		// columnDependent.pack();

		// create 10 rows
		for (int i = 0; i < ROW_NUMBER; i++) {
			new TableItem(table, SWT.NONE);
		}

		// Create an editor object to use for text editing
		editor = new TableEditor(table);
		// The editor must have the same size as the cell and must
		// not be any smaller than 50 pixels.
		editor.horizontalAlignment = SWT.LEFT;
		editor.grabHorizontal = true;
		editor.minimumWidth = 50;

		GridData gd_table = new GridData(SWT.FILL, SWT.CENTER, true, true, 2, 1);
		table.setLayoutData(gd_table);
		table.pack();
	}

	private void createLabel(final Composite composite, String labelName) {
		Label lblPrincipleLabel = new Label(composite, SWT.NONE);
		lblPrincipleLabel.setText(String.format(labelName));
		lblPrincipleLabel.setToolTipText(String.format(labelName));

		GridData gd_lblPrincipleLabel = new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1);
		lblPrincipleLabel.setLayoutData(gd_lblPrincipleLabel);
	}

	protected void updateControls(Combo source) {
		principleIndex = source.getSelectionIndex();
		dependentIndex = (principleIndex + 1) % 2;

		// update the Dependent Text field
		dependentNameText.setText(entityTypesArr[dependentIndex].getName());

		// update the rows
		final List<Property> principleKeys = getProperties(true,
				entityTypesArr[principleIndex]);
		updateTableRows(principleKeys);

		updateStatusOfDialog();

		if (table.isListening(SWT.Selection)) {
			table.removeSelectionListener(tableListener);
		}
		tableListener = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) { // Clean up any
															// previous editor
															// control
				Control oldEditor = editor.getEditor();
				if (oldEditor != null)
					oldEditor.dispose();

				// Identify the selected row
				final TableItem item = (TableItem) e.item;
				if (item == null
						|| table.indexOf(item) > principleKeys.size() - 1)
					return;

				// create the combo
				final CCombo co = new CCombo(table, SWT.DROP_DOWN
						| SWT.READ_ONLY);

				List<Property> dependentProperties = getProperties(false,
						entityTypesArr[dependentIndex]);
				for (Property property : dependentProperties) {
					co.add(property.getName());
				}
				co.select(co.indexOf(item.getText(EDITABLECOLUMN)));
				co.setFocus();
				co.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						item.setText(EDITABLECOLUMN,
								((CCombo) e.getSource()).getText());

						// The user selected an item; end the editing session
						co.dispose();
						updateStatusOfDialog();
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {

					}
				});
				editor.setEditor(co, item, EDITABLECOLUMN);
			}
		};

		table.addSelectionListener(tableListener);
		table.setFocus();

	}

	private List<Property> getProperties(boolean isKey, EntityType entity) {
		List<Property> keys = new ArrayList<Property>(entity.getKeys());
		List<Property> properties = new ArrayList<Property>(
				entity.getProperties());

		EntityType baseEntity = entity.getBaseType();
		while (baseEntity != null) {
			keys.addAll(baseEntity.getKeys());
			properties.addAll(baseEntity.getProperties());
			baseEntity = baseEntity.getBaseType();
		}

		if (isKey) {
			return keys;
		}
		properties.addAll(keys);
		return properties;
	}

	private void updateTableRows(List<Property> principleKeys) {
		// if the entity has more then the number of rows in the table
		while (principleKeys.size() > table.getItemCount()) {
			new TableItem(table, SWT.NONE);
		}

		// if in the previous selection , the entity had more then 10 keys we
		// need to reduce the table
		if (principleKeys.size() < ROW_NUMBER
				&& table.getItemCount() > ROW_NUMBER) {
			while (table.getItemCount() > ROW_NUMBER) {
				table.remove(0);
			}
		}

		// update the table with the new principle keys
		for (int i = 0; i < table.getItemCount(); i++) {
			TableItem tableItem = table.getItem(i);
			tableItem.setText(EDITABLECOLUMN, Messages.EMPTY_String);

			// if the combo in the table is open and then changing the principle
			// entity, the combo need to be disposed
			Control oldEditor = editor.getEditor();
			if (oldEditor != null)
				oldEditor.dispose();

			if (i < principleKeys.size()) {
				tableItem.setText(UNEDITABLECOLUMN, principleKeys.get(i)
						.getName());
			} else {
				tableItem.setText(UNEDITABLECOLUMN, Messages.EMPTY_String);
			}

		}
	}

	protected void updateStatusOfDialog() {

		boolean isDone = true;
		HashSet<String> dependentProperties = new HashSet<String>();
		String errMes = "";//$NON-NLS-1$

		for (int i = 0; i < getProperties(true, entityTypesArr[principleIndex])
				.size(); i++) {
			if (table.getItem(i).getText(EDITABLECOLUMN)
					.equals(Messages.EMPTY_String)) {
				isDone = false;
				break;
			}

			if (!dependentProperties.add(table.getItem(i).getText(
					EDITABLECOLUMN))) {
				isDone = false;
				errMes = Messages.ReferentialConstraintsDialog_theSameDependentProperty;
				break;
			}
		}

		if (isDone && !isReadOnlyMode) {
			updateStatus(new Status(IStatus.INFO, Activator.PLUGIN_ID, errMes));
		} else {
			updateStatus(new Status(IStatus.ERROR, Activator.PLUGIN_ID, errMes));
		}

	}

	private void checkIfRefConstraintSet() {
		ReferentialConstraint rConstraint = association
				.getReferentialConstraint();
		if (rConstraint != null) {
			int principleIndex = entityTypesArr[1].equals(rConstraint
					.getPrincipal().getType()) ? 1 : 0;
			// In case there were a change in Multiplicity after creating
			// Referential Constraint
			if (Multiplicity.MANY == rConstraint.getPrincipal()
					.getMultiplicity()) {
				return;
			}

			combo.select(principleIndex);
			updateControls(combo);

			EMap<Property, Property> keyMappings = rConstraint.getKeyMappings();

			int i = 0;
			for (Entry<Property, Property> mapEntry : keyMappings) {
				List<Property> keys = getProperties(true,
						entityTypesArr[principleIndex]);
				if (mapEntry.getKey() == null
						|| !keys.contains(mapEntry.getKey())) {
					continue;
				}

				if (table.getItemCount() < i) {
					new TableItem(table, SWT.NONE);
				}

				table.getItem(i).setText(UNEDITABLECOLUMN,
						mapEntry.getKey().getName());

				String dependentName = ""; //$NON-NLS-1$
				if (mapEntry.getValue() != null) {
					dependentName = mapEntry.getValue().getName();

				}

				table.getItem(i).setText(EDITABLECOLUMN, dependentName);
				i++;
			}

			updateStatusOfDialog();
			createMap();
		}
	}

	private void checkIfReadOnly() {
		if (isReadOnlyMode) {
			combo.removeSelectionListener(comboListener);
			combo.setEnabled(false);
			if (table.isListening(SWT.Selection)) {
				table.removeSelectionListener(tableListener);
			}
		}

		// in case of 1:m association
		else if (isAssociationOneToMany()) {
			combo.setEnabled(false);
			combo.removeSelectionListener(comboListener);

			EntityType principalEntity = association.getEnds().get(0)
					.getMultiplicity().equals(Multiplicity.ONE) ? association
					.getEnds().get(0).getType() : association.getEnds().get(1)
					.getType();
			int index = entityTypesArr[1].equals(principalEntity) ? 1 : 0;
			combo.select(index);

			// if the RC were already defined, the table was updated in the
			// checkIfRefConstraintSet().
			// if the user created RC, but then updated the turn the
			// Multiplicity of the roles, the current RC are not valid
			ReferentialConstraint rConstraint = association
					.getReferentialConstraint();
			if (rConstraint == null
					|| !rConstraint.getPrincipal().getType()
							.equals(principalEntity)) {
				updateControls(combo);
			}
		}

	}

	private boolean isAssociationOneToMany() {
		return (association.getEnds().get(0).getMultiplicity()
				.equals(Multiplicity.ONE) && association.getEnds().get(1)
				.getMultiplicity().equals(Multiplicity.MANY))
				|| (association.getEnds().get(1).getMultiplicity()
						.equals(Multiplicity.ONE) && association.getEnds()
						.get(0).getMultiplicity().equals(Multiplicity.MANY));
	}

}
