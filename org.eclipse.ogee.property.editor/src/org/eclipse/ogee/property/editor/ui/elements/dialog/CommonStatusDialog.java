/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.dialog;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.StatusDialog;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class CommonStatusDialog extends StatusDialog {

	private int dialogWidth = 400;
	private String topExplenation;
	private Text detailsText;
	private Composite mainContainer;
	private boolean details;
	private boolean isReadOnly;

	public CommonStatusDialog(Shell parent, String title, boolean HasDetails,
			boolean isReadOnly) {
		super(parent);
		details = HasDetails;
		this.isReadOnly = isReadOnly;
		setShellStyle(SWT.RESIZE | SWT.TITLE | SWT.ICON | SWT.APPLICATION_MODAL
				| SWT.MIN | SWT.MAX | SWT.CLOSE);
		setHelpAvailable(false);
		setDialogHelpAvailable(false);
		updateStatus(new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				Messages.EMPTY_String));

		if (this.isReadOnly()) {
			setTitle(title + " " + Messages.CommonStatusDialog_ReadOnly);//$NON-NLS-1$
		} else {
			setTitle(title);
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 5;
		gridLayout.marginTop = 10;
		gridLayout.marginWidth = 10; // left and right edges of the layout
		gridLayout.verticalSpacing = 10; // between vertical cells
		gridLayout.numColumns = 1; // number of cell columns in the layout
		composite.setLayout(gridLayout);

		createTopExplenation(composite);
		createMainComponent(composite);
		if (details) {
			createDeatilsBox(composite);
		}

		return composite;
	}

	protected void createTopExplenation(Composite composite) {
		String text = getTopExplenation();

		if (text == null || text == "") //$NON-NLS-1$
			return;

		Label topLabel = new Label(composite, SWT.WRAP);
		topLabel.setText(text);
	}

	protected void createMainComponent(Composite composite) {
		GridData gdcontainer = new GridData(dialogWidth, 300);
		gdcontainer.grabExcessHorizontalSpace = true;
		gdcontainer.horizontalAlignment = GridData.FILL;
		gdcontainer.grabExcessVerticalSpace = true; // Layout vertically, too!
		gdcontainer.verticalAlignment = GridData.FILL;

		mainContainer = new Composite(composite, SWT.NONE);
		mainContainer.setLayoutData(gdcontainer);
	}

	protected void createDeatilsBox(Composite composite) {
		// Separate line before description details section
		GridData gdSeperator = new GridData();
		gdSeperator.grabExcessHorizontalSpace = true;
		gdSeperator.horizontalAlignment = GridData.FILL;
		gdSeperator.grabExcessVerticalSpace = true; // Layout vertically, too!
		gdSeperator.verticalAlignment = GridData.FILL;

		Label seperatorLabel = new Label(composite, SWT.SEPARATOR
				| SWT.HORIZONTAL | SWT.CENTER);
		seperatorLabel.setLayoutData(gdSeperator);

		// Group Description control
		GridData gdGroupescription = new GridData(dialogWidth, 50);
		gdGroupescription.grabExcessHorizontalSpace = true;
		gdGroupescription.horizontalAlignment = GridData.FILL;
		gdGroupescription.grabExcessVerticalSpace = true;

		gdGroupescription.verticalAlignment = GridData.FILL;

		Group descriptionGroup = new Group(composite, SWT.V_SCROLL | SWT.WRAP);
		descriptionGroup.setText(Messages.CommonStatusDialog_Details);
		descriptionGroup.setLayout(new GridLayout(1, false));
		descriptionGroup.setLayoutData(gdGroupescription);

		// Textbox inside group to hold Term's details text
		GridData gdTextBox = new GridData();
		gdTextBox.grabExcessHorizontalSpace = true;
		gdTextBox.horizontalAlignment = GridData.FILL;
		gdTextBox.grabExcessVerticalSpace = true; // Layout vertically, too!
		gdTextBox.verticalAlignment = GridData.FILL;

		detailsText = new Text(descriptionGroup, SWT.READ_ONLY | SWT.V_SCROLL
				| SWT.WRAP);
		detailsText.setLayoutData(gdTextBox);

	}

	// Getters and Setters

	protected String getTopExplenation() {
		return topExplenation;
	}

	protected void setTopExplenation(String topExplenation) {
		this.topExplenation = topExplenation;
	}

	protected Text getDetailsText() {
		return detailsText;
	}

	protected void setDetailsText(Text detailsText) {
		this.detailsText = detailsText;
	}

	protected Composite getMainContainer() {
		return mainContainer;
	}

	protected void setMainContainer(Composite mainContainer) {
		this.mainContainer = mainContainer;
	}

	protected int getDialog_width() {
		return dialogWidth;
	}

	protected void setDialog_width(int dialog_width) {
		this.dialogWidth = dialog_width;
	}

	public void enableApplyButton(boolean toEnable, String message) {
		if (toEnable)
			updateStatus(new Status(IStatus.OK, Activator.PLUGIN_ID, message));
		else
			updateStatus(new Status(IStatus.ERROR, Activator.PLUGIN_ID, message));
	}

	/**
	 * @return the isReadOnly
	 */
	public boolean isReadOnly() {
		return isReadOnly;
	}

}
