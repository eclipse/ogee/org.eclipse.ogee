/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.descriptors;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellEditorListener;
import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ogee.property.editor.utilities.Validator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.PropertyDescriptor;

public abstract class AbstractPropertyDescriptorValidator extends
		PropertyDescriptor implements ICellEditorListener {
	protected Validator validator = new Validator();
	protected ICellEditorValidator cellEditorValidator = null;
	protected CellEditor cellEditor;
	protected EDMTypes edmType;

	public AbstractPropertyDescriptorValidator(Object id, String displayName,
			EDMTypes edmType) {
		super(id, displayName);
		this.edmType = edmType;
	}

	@Override
	public CellEditor createPropertyEditor(Composite parent) {
		cellEditor = getCellEditor(parent);
		cellEditor.addListener(this);
		if (getValidator() != null) {
			cellEditor.setValidator(getValidator());
		}
		return cellEditor;
	}

	public abstract CellEditor getCellEditor(Composite parent);

	protected ICellEditorValidator getCellEditorValidator() {
		if (cellEditorValidator == null) {
			cellEditorValidator = new ICellEditorValidator() {
				@Override
				public String isValid(Object value) {
					if (cellEditor == null)
						return null;

					String errMessage = validate(value.toString());
					if (errMessage != null) {
						Color redColor = cellEditor.getControl().getDisplay()
								.getSystemColor(SWT.COLOR_RED);
						cellEditor.getControl().setToolTipText(errMessage);
						cellEditor.getControl().setForeground(redColor);
					} else {
						Color defaultColor = cellEditor.getControl()
								.getDisplay().getSystemColor(SWT.DEFAULT);
						cellEditor.getControl().setToolTipText(null);
						cellEditor.getControl().setForeground(defaultColor);
					}

					return errMessage;
				}
			};
		}
		return cellEditorValidator;
	}

	protected ICellEditorValidator getCellEditorDateValidator() {
		if (cellEditorValidator == null) {
			cellEditorValidator = new ICellEditorValidator() {
				@Override
				public String isValid(Object value) {
					if (cellEditor == null)
						return null;

					String errMessage = validate(value.toString());
					if (errMessage != null) {
						cellEditor.getControl().setToolTipText(errMessage);

					} else {
						Color defaultColor = cellEditor.getControl()
								.getDisplay().getSystemColor(SWT.DEFAULT);
						cellEditor.getControl().setToolTipText(null);
						cellEditor.getControl().setForeground(defaultColor);
					}

					return errMessage;
				}
			};
		}
		return cellEditorValidator;
	}

	protected String validate(String string) {
		return Validator.validate(string, edmType);

	}

	@Override
	public void editorValueChanged(boolean oldValidState, boolean newValidState) {
		Utilities.updateStatusLineManager(cellEditor.getErrorMessage());
	}

	@Override
	public void cancelEditor() {
		Utilities.updateStatusLineManager(null);

	}

	@Override
	public void applyEditorValue() {
		Utilities.updateStatusLineManager(null);

	}

}
