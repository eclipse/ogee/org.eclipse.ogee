/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.ui.elements.descriptors;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.ogee.property.editor.property.descriptor.DialogContext;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.PropertyDescriptor;

public class ButtonPropertyDescriptor extends PropertyDescriptor {
	private DialogContext context;

	/**
	 * Creates an property descriptor with the given id and display name.
	 * 
	 * @param id
	 *            the id of the property
	 * @param displayName
	 *            the name to display for the property
	 */
	public ButtonPropertyDescriptor(Object id, String displayName,
			DialogContext context) {
		super(id, displayName);
		this.context = context;

	}

	@Override
	public CellEditor createPropertyEditor(Composite parent) {
		CellEditor editor = new ButtonCellEditor(parent, context);
		if (getValidator() != null)
			editor.setValidator(getValidator());
		return editor;
	}

}
