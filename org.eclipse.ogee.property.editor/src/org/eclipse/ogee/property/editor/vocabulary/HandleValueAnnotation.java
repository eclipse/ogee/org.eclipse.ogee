/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.vocabulary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.commands.TransactionChangeCommandAbstract;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ogee.utils.logger.Logger;

public class HandleValueAnnotation {
	static private EObject eObject;

	public static void updateValueAnnotation(EObject eObject,
			Object[] checkedElem,
			Map<IVocabulary, List<ValueTermExt>> createdMapVocTerms,
			Set<ValueTermExt> changedElements) {
		HandleValueAnnotation.eObject = eObject;
		List<Object> checkedElemList = Arrays.asList(checkedElem);

		List<ValueTermExt> toCreateList = new ArrayList<ValueTermExt>();
		List<ValueTermExt> toRemoveList = new ArrayList<ValueTermExt>();
		for (ValueTermExt valueTermExt : changedElements) {
			if (valueTermExt.isChecked()) {
				if (!createdMapVocTerms.get((valueTermExt).getVocabulary())
						.contains(valueTermExt)) {
					toCreateList.add(valueTermExt);
				}
			} else {
				if (!checkedElemList.contains(valueTermExt)
						|| createdMapVocTerms.get(
								(valueTermExt).getVocabulary()).contains(
								valueTermExt)) {
					toRemoveList.add(valueTermExt);
				}
			}
		}

		updateObjectTerms(toCreateList, toRemoveList);

	}

	private static void updateObjectTerms(
			final List<ValueTermExt> toCreateList,
			final List<ValueTermExt> toRemoveList) {

		TransactionChangeCommandAbstract transactionChangeCommand = new TransactionChangeCommandAbstract(
				null, null, HandleValueAnnotation.eObject) {

			@Override
			public void doExecute() {
				createTerms(toCreateList);
				removeTerms(toRemoveList);

			}

			private void removeTerms(List<ValueTermExt> removeList) {
				List<Annotation> delAnnotationList = new ArrayList<Annotation>();

				for (ValueTermExt valueTerm : removeList) {

					for (Annotation annotation : ((IAnnotationTarget) eObject)
							.getAnnotations()) {

						if (annotation instanceof ValueAnnotation) {						
							if (valueTerm.getValueTerm().getValueAnnotations()
									.indexOf(annotation) != -1) {
								delAnnotationList.add(annotation);
							}

						}
					}

					valueTerm.getValueTerm().getValueAnnotations()
							.removeAll(delAnnotationList);
				}
				if (SchemaHandler.getSchema(eObject) != null) {

					SchemaHandler.getSchema(eObject).getValueAnnotations()
							.removeAll(delAnnotationList);
				}
				((IAnnotationTarget) eObject).getAnnotations().removeAll(
						delAnnotationList);

			}

			private void createTerms(List<ValueTermExt> toCreateList) {
				for (ValueTermExt valueTermExt : toCreateList) {
					try {
						ValueAnnotation val = valueTermExt.getVocabulary()
								.createValueAnnotation(
										((IAnnotationTarget) eObject),
										valueTermExt.getValueTerm());
						createDefaultValues(valueTermExt.getValueTerm()
								.getType(), val);

					} catch (ModelAPIException e) {
						Logger.getLogger(Activator.PLUGIN_ID).logError(
								e.getLocalizedMessage());
					}
				}

			}

			private void createDefaultValues(IPropertyTypeUsage type,
					ValueAnnotation val) {
				if (type instanceof SimpleTypeUsage) {
					// set default value for simple value
					SimpleType simTypeTerm = (SimpleType) ((SimpleTypeUsage) type)
							.getSimpleType();
					if (simTypeTerm.getDefaultValue() == null) {
						return;
					}
					if (val.getAnnotationValue() instanceof SimpleValue) {

						SimpleValue simpleValue = (SimpleValue) val
								.getAnnotationValue();
						simpleValue.setValueObject(((SimpleValue) simTypeTerm
								.getDefaultValue()).getValueObject());
					}

				} else if (type instanceof ComplexTypeUsage) {
					EList<Property> properties = ((ComplexTypeUsage) type)
							.getComplexType().getProperties();
					for (Property property : properties) {
						createDefaultValues(property.getType(), val);
					}

				}
			}

		};
		Utilities.executeTransaction(transactionChangeCommand);
	}

}
