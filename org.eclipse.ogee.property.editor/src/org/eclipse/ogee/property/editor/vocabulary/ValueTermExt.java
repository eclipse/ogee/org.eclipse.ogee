/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.vocabulary;

import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.odata.ValueTerm;

public class ValueTermExt {
	private IVocabulary Vocabulary;
	private ValueTerm valueTerm;
	private boolean checked;

	public ValueTermExt(IVocabulary vocabulary, ValueTerm valueTerm) {
		super();
		Vocabulary = vocabulary;
		this.valueTerm = valueTerm;
	}

	public String getName() {
		return valueTerm.getName();
	}

	public ValueTerm getValueTerm() {
		return valueTerm;
	}

	public IVocabulary getVocabulary() {
		return Vocabulary;
	}

	public void setVocabulary(IVocabulary vocabulary) {
		Vocabulary = vocabulary;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

}
