/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.vocabulary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.utils.logger.Logger;

public class VocabularyTreeContentProvider implements ITreeContentProvider {
	private Logger logger = Logger.getLogger(Activator.PLUGIN_ID);

	private Map<IVocabulary, List<ValueTermExt>> checkedValueTermsMap;

	private Map<IVocabulary, List<ValueTermExt>> vocabularyTermsMap;

	private List<IVocabulary> vocabularyList;

	private EObject eObject;

	public VocabularyTreeContentProvider(EObject eObject) {

		this.eObject = eObject;
		vocabularyTermsMap = new HashMap<IVocabulary, List<ValueTermExt>>();
		checkedValueTermsMap = new HashMap<IVocabulary, List<ValueTermExt>>();
		vocabularyList = new ArrayList<IVocabulary>();
	}

	public List<IVocabulary> getVocabularyList() {
		return vocabularyList;
	}

	public Map<IVocabulary, List<ValueTermExt>> getCheckedValueTermsExt() {

		return checkedValueTermsMap;

	}

	public boolean isAllCheckedValueTermsExtUnChecked(IVocabulary vocabulary) {
		int checked = 0;

		for (ValueTermExt valueTermExt : checkedValueTermsMap.get(vocabulary)) {
			if (valueTermExt.isChecked() == false) {
				checked++;
			}
		}

		return (checked == checkedValueTermsMap.get(vocabulary).size());

	}

	@Override
	public Object[] getChildren(Object arg0) {

		if (!(arg0 instanceof IVocabulary)) {
			return null;
		}

		IVocabulary vocabulary = (IVocabulary) arg0;

		if (vocabularyTermsMap.containsKey(vocabulary)) // return the children from Map														
		{
			return vocabularyTermsMap.get(vocabulary).toArray();
		}

		List<ValueTermExt> applyValueTermExt = new LinkedList<ValueTermExt>();

		try {

			List<ValueTerm> applyTerms = vocabulary
					.getApplicableValueTerms(eObject);

			for (ValueTerm valueTerm : applyTerms) // Create valueTermExt list													
			{
				applyValueTermExt.add(new ValueTermExt(vocabulary, valueTerm));
			}
			// add the Voca and ValuTerms to map
			vocabularyTermsMap.put(vocabulary, applyValueTermExt);

			return applyValueTermExt.toArray();

		} catch (ModelAPIException e) {
			logger.logError(e.getLocalizedMessage());
		}

		return null;

	}

	@Override
	public Object[] getElements(Object arg0) {

		if (vocabularyList.size() != 0) {
			return vocabularyList.toArray();
		}
		if (arg0 instanceof VocabulariesTreeModel) {
			List<IVocabulary> listVocabulary = ((VocabulariesTreeModel) arg0)
					.getVocabularies();
			for (IVocabulary iVocabulary : listVocabulary) {
				if (hasChildren(iVocabulary)) {
					vocabularyList.add(iVocabulary);
					// Get Checked elements for vocabulary
					setCheckedValueTermsExt(iVocabulary);

				}
			}
		}

		return vocabularyList.toArray();

	}

	@Override
	public Object getParent(Object arg0) {
		if (arg0 instanceof ValueTermExt) {
			return ((ValueTermExt) arg0).getVocabulary();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object arg0) {
		if (arg0 instanceof IVocabulary) {

			try {

				List<ValueTerm> terms = ((IVocabulary) arg0)
						.getApplicableValueTerms(eObject);
				return (terms.size() > 0);

			} catch (ModelAPIException e) {
				logger.logError(e.getLocalizedMessage());
			}
		}

		return false;
	}

	public void setCheckedValueTermsExt(IVocabulary element) {

		if (checkedValueTermsMap.get(element) != null) {
			return;
		}

		if (vocabularyTermsMap.get(element) == null) {
			getChildren(element);
		}

		IAnnotationTarget annotationTarget = ((IAnnotationTarget) eObject);

		List<ValueTermExt> tmpCheckedValueTerms = new ArrayList<ValueTermExt>();

		List<Annotation> annotations = annotationTarget.getAnnotations();
		for (Annotation annotation : annotations) {

			if (annotation instanceof ValueAnnotation) {

				ValueTerm term = ((ValueAnnotation) annotation).getTerm();

				for (ValueTermExt valueTermExt : vocabularyTermsMap
						.get(element)) {
					if (valueTermExt.getValueTerm().getName() == term.getName()) {
						ValueTermExt checkedValueTermExt = vocabularyTermsMap
								.get(element).get(
										vocabularyTermsMap.get(element)
												.indexOf(valueTermExt));
						checkedValueTermExt.setChecked(true);
						tmpCheckedValueTerms.add(checkedValueTermExt);

					}
				}
			}
		}
		checkedValueTermsMap.put(element, tmpCheckedValueTerms);
	}

	@Override
	public void dispose() {

	}

	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {

	}
}
