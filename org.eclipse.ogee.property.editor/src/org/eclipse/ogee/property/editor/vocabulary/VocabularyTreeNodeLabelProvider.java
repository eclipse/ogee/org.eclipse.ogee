/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.vocabulary;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.swt.graphics.Image;

public class VocabularyTreeNodeLabelProvider extends LabelProvider {

	private Logger logger = Logger.getLogger(Activator.PLUGIN_ID);

	@Override
	public Image getImage(Object element) {
		return super.getImage(element);
	}

	@Override
	public String getText(Object element) {
		SimpleTypeUsage simpleTypeUsage;
		ComplexTypeUsage complexType;
		EnumTypeUsage enumType;
		if (element instanceof IVocabulary) {
			try {
				return ((IVocabulary) element).getSchema().getNamespace();
			} catch (ModelAPIException e) {
				logger.logError(e.getLocalizedMessage());
				return null;
			}
		}

		if (element instanceof ValueTermExt) {

			if (((ValueTermExt) element).getValueTerm().getType() instanceof SimpleTypeUsage) {

				simpleTypeUsage = (SimpleTypeUsage) ((ValueTermExt) element)
						.getValueTerm().getType();

				return String
						.format("%s (%s) ", ((ValueTermExt) element).getName(), simpleTypeUsage.getSimpleType().getType().getLiteral()); //$NON-NLS-1$

			}

			else if (((ValueTermExt) element).getValueTerm().getType() instanceof ComplexTypeUsage) {

				complexType = (ComplexTypeUsage) ((ValueTermExt) element)
						.getValueTerm().getType();

				return String
						.format("%s (%s) ", ((ValueTermExt) element).getName(), complexType.getComplexType().getName()); //$NON-NLS-1$
			}

			else if (((ValueTermExt) element).getValueTerm().getType() instanceof EnumTypeUsage) {

				enumType = (EnumTypeUsage) ((ValueTermExt) element)
						.getValueTerm().getType();

				return String
						.format("%s (%s) ", ((ValueTermExt) element).getName(), enumType.getEnumType().getName()); //$NON-NLS-1$
			}

			return ((ValueTermExt) element).getName();
		}
		return super.getText(element);
	}

}
