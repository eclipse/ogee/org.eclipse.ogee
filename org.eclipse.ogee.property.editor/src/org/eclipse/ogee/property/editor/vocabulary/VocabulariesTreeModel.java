/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.vocabulary;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.vocabularies.ICoreVocabulary;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.utils.logger.Logger;

public class VocabulariesTreeModel {
	private Logger logger = Logger.getLogger(Activator.PLUGIN_ID);

	private EDMXSet edmxSet;
	private EObject eObject;

	public VocabulariesTreeModel(EDMXSet edmxset, EObject eObject) {
		this.edmxSet = edmxset;
		this.eObject = eObject;
	}

	public EObject getSelectedEObject() {
		return eObject;
	}

	public List<IVocabulary> getVocabularies() {

		List<IVocabulary> vocabList = new ArrayList<IVocabulary>();

		try {
			IVocabularyContext vocabularyContext = IModelContext.INSTANCE
					.getVocabularyContext(edmxSet);
			List<String> vocabulariesNames = vocabularyContext
					.getAvailableVocabularies();
			vocabulariesNames.remove(ICoreVocabulary.VC_NAMESPACE);
			for (String namespace : vocabulariesNames) {
				if (!namespace.equals(ICoreVocabulary.VC_NAMESPACE)) // do not add Core Vocabulary 																		
				{
					vocabList.add(vocabularyContext
							.provideVocabulary(namespace));
				}
			}
			return vocabList;

		} catch (ModelAPIException e) {
			logger.logError(e.getLocalizedMessage());
		}

		return null;
	}

}
