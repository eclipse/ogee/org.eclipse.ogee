/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.nls.messages;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.ogee.property.editor.nls.messages.messages"; //$NON-NLS-1$

	public static final String EMPTY_String = "";//$NON-NLS-1$

	public static String AnnotationLabelProvider_0;

	public static String AnnotationLabelProvider_1;

	public static String AssociationSetDialog_ADD_BUTTON_TEXT;

	public static String AssociationSetDialog_button_Apply;

	public static String AssociationSetDialog_END1;

	public static String AssociationSetDialog_END2;

	public static String AssociationSetDialog_NAME;

	public static String AssociationSetDialog_REMOVE_BUTTON_TEXT;

	public static String AssociationSetDialog_TITLE;

	public static String AssociationSetDialog_TOP_EXPLENATION;

	public static String AssociationSetModel_0;

	public static String AssociationSetModel_2;

	public static String AssociationSetModel_AssociationSetMustContainValue;

	public static String AssociationSetModel_Log_1;

	public static String AssociationSetModel_Log_2;

	public static String AssociationSetModel_Log_3;

	public static String AssociationSetModel_Validate_0;

	public static String CommonStatusDialog_Details;

	public static String CommonStatusDialog_ReadOnly;

	public static String EMPTY_ENTER;

	public static String DescriptorId_Abstract;
	public static String DescriptorId_Alias;
	public static String DescriptorId_Association;
	public static String DescriptorId_AssociationSetName;

	public static String DescriptorId_AssociationSets;
	public static String DescriptorId_Author;
	public static String DescriptorId_BaseType;
	public static String DescriptorId_Bindto;
	public static String DescriptorId_Collection;
	public static String DescriptorId_ContainsTarget;
	public static String DescriptorId_Default;
	public static String DescriptorId_Description;
	public static String DescriptorId_Documentation;
	public static String DescriptorId_END1;
	public static String DescriptorId_END2;
	public static String DescriptorId_EntitySet;
	public static String DescriptorId_EntityType;

	public static String DescriptorId_EnumValue;
	public static String DescriptorId_Facets;
	public static String DescriptorId_FixedLength;

	public static String DescriptorId_Flags;
	public static String DescriptorId_FromRole;
	public static String DescriptorId_HasSideeffect;
	public static String DescriptorId_Id;
	public static String DescriptorId_ImageURL;
	public static String DescriptorId_Key;
	public static String DescriptorId_LongDescriptor;
	public static String DescriptorId_MaxLength;
	public static String DescriptorId_Media;
	public static String DescriptorId_MetadataURL;
	public static String DescriptorId_Multiplicity;
	public static String DescriptorId_Name;
	public static String DescriptorId_Namespace;
	public static String DescriptorId_Nullable;
	public static String DescriptorId_Precision;
	public static String DescriptorId_ReferentialConstraint;
	public static String DescriptorId_Relationship;
	public static String DescriptorId_ReturnType;
	public static String DescriptorId_Role;
	public static String DescriptorId_Scale;
	public static String DescriptorId_ServiceURL;
	public static String DescriptorId_Summary;
	public static String DescriptorId_TechnicalServiceName;
	public static String DescriptorId_TechnicalServiceVersion;
	public static String DescriptorId_Title;
	public static String DescriptorId_ToRole;
	public static String DescriptorId_Type;
	public static String DescriptorId_UnderlyingType;

	public static String DescriptorId_UpdatedDate;
	public static String DescriptorId_UseForEtag;
	public static String DescriptorId_Schema;

	public static String DynamicTermsDialog_AddItem;

	public static String DynamicTermsDialog_AddRemoveMaintainTermValues;

	public static String DynamicTermsDialog_MaintainDynamicAnnotations;

	public static String DynamicTermsDialog_RemoveItem;

	public static String DynamicTermsDialog_ValueHeader;

	// TODO Start - Enable with OData V4 support
	public static String FunctionImport_HttpMethod_GET;
	public static String FunctionImport_HttpMethod_POST;
	// End - Enable with OData V4 support

	public static String GeneralSection_first_column_name;

	public static String GeneralTabDescriptor_Label;

	public static String RefConstraintsDialog_DependentColumnTitle;
	public static String RefConstraintsDialog_HostText;
	public static String RefConstraintsDialog_PrincipleColumnTitle;
	public static String RefConstraintsDialog_Title;
	public static String ReferentialConstraintsDialog_Delete;
	public static String ReferentialConstraintsDialog_Dependent;
	public static String ReferentialConstraintsDialog_Principle;
	public static String ReferentialConstraintsDialog_theSameDependentProperty;

	public static String TabbedPropertyHeaderProvider_Association;
	public static String TabbedPropertyHeaderProvider_AssociationSet;
	public static String TabbedPropertyHeaderProvider_ComplexType;
	public static String TabbedPropertyHeaderProvider_EntitySet;
	public static String TabbedPropertyHeaderProvider_EntityType;
	public static String TabbedPropertyHeaderProvider_EnumMember;

	public static String TabbedPropertyHeaderProvider_EnumType;

	public static String TabbedPropertyHeaderProvider_Function_Import_Return_Type;
	public static String TabbedPropertyHeaderProvider_FunctionImport;
	public static String TabbedPropertyHeaderProvider_NavigationProperty;
	public static String TabbedPropertyHeaderProvider_Parameter;
	public static String TabbedPropertyHeaderProvider_Property;
	public static String TabbedPropertyHeaderProvider_Schema;
	public static String TabbedPropertyHeaderProvider_ValueTerm;

	public static String TextComboCellEditor_WrongValue;

	public static String TransactionAssociationSetChangeCommand_Log_0;

	public static String TransactionAssociationSetChangeCommand_Log_1;

	public static String TypeConverter_Parse_Error1;

	public static String Utilities_AnnotationCollectionStringFormat;

	public static String Utilities_AssociationSetStringFotmat;

	public static String Utilities_RCStringFormat;

	public static String validation_contain_spaces;

	public static String validation_first_char;

	public static String validation_Incorrect_floating_point_format;
	public static String validation_name_empty;
	public static String validation_no_special_char;
	public static String validation_only_digits;
	public static String validation_out_of_boundary;
	public static String validation_name_keywords;

	public static String Validator_3;

	public static String Validator_Exceed_Length;

	public static String Validator_Exceed_Length_Namespace;

	public static String Validator_Exceed_Length_Namespace_PartName;

	public static String Validator_Invalid_Characters_Namespace;

	public static String Validator_Invalid_PartName_Namespace;

	public static String Validator_ParseError1;

	public static String Validator_UUID;

	public static String VocabulariesDialog_Apply;

	public static String VocabulariesDialog_No_applicable_terms_found;

	public static String VocabulariesDialog_No_description_available;

	public static String VocabulariesDialog_Title;

	public static String VocabulariesModelDialog_Top_Explanation;

	public static String VocabulariesSection_first_column_name;

	public static String VocabulariesTabDescriptor_Label;

	public static String VocabularyButton_Title;

	public static String VocabularyButton_ToolTip;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
