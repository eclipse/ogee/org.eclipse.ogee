/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.nls.messages;

public class Constants {
	
	public static final String PROPERTY_SHEET_ID = "org.eclipse.ui.views.PropertySheet";//$NON-NLS-1$
	public static final String EDITOR_ID = "org.eclipse.ogee.designer.ODataEditor"; //$NON-NLS-1$

	public static final String CONTRIBUTER_PROPERTY_CATEGORY = "Simple"; //$NON-NLS-1$
	public static final String VOCABULARIES_TAB_ID = "org.eclipse.ogee.property.editor.Extensions"; //$NON-NLS-1$
	public static final String GENERAL_TAB_ID = "org.eclipse.ogee.property.editor.General"; //$NON-NLS-1$

	public static final String GENERAL_SECTION_ID = "org.eclipse.ogee.property.editor.view.GeneralSection"; //$NON-NLS-1$
	public static final String VOCABULARIES_SECTION_ID = "org.eclipse.ogee.property.editor.view.VocabulariesSection"; //$NON-NLS-1$

	public static final String ACTION_BAR_BUTTON_ID = "org.eclipse.ogee.property.editor.ui.elements.OpenVocabulariesDialogAction";//$NON-NLS-1$

}
