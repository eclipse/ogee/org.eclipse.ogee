/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.type.handler;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.designer.api.IODataDiagramCreator;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;

public class ReturnTypeHandler extends AbstractTypeHandler {
	private IFunctionReturnTypeUsage typeUsage;

	public ReturnTypeHandler(IFunctionReturnTypeUsage returnTypeUsage) {
		this.typeUsage = returnTypeUsage;
		createMaps(SchemaHandler.getSchema(typeUsage));
		keysList.addAll(edmTypesMap.keySet());
		keysList.addAll(complexTypesMap.keySet());
		keysList.addAll(entityTypesMap.keySet());
		keysList.addAll(enumTypesMap.keySet());
	}

	@Override
	protected void createMaps(Schema schema, String prefix) {

		if (typeUsage instanceof SimpleTypeUsage) {
			createSimpleTypesMap();
		}

		else if (typeUsage instanceof ComplexTypeUsage) {
			createComplexTypeMap(schema, prefix);
		}

		else if (typeUsage instanceof ReturnEntityTypeUsage) {
			createEntityTypeMap(schema, prefix);
		}

		else if (typeUsage instanceof EnumTypeUsage) {
			createEnumTypeMap(schema, prefix);
		}

	}

	public int getTypeIndex(IFunctionReturnTypeUsage typeUsage) {

		if (typeUsage instanceof ReturnEntityTypeUsage) {
			if (((ReturnEntityTypeUsage) typeUsage).getEntityType() != null) {
				String name = ((ReturnEntityTypeUsage) typeUsage)
						.getEntityType().getName();
				return keysList
						.indexOf(SchemaHandler
								.getNamespaceOrAlias(((ReturnEntityTypeUsage) typeUsage)
										.getEntityType())
								+ NAMESPACE_SEPARATOR + name);
			} else
				return -1;
		} else
			return getTypeIndex((IPropertyTypeUsage) typeUsage);

	}

	public void updateType(Integer index, IFunctionReturnTypeUsage typeUsage) {
		if (typeUsage == null)
			return;
		EObject prevType = null;
		EObject newType = null;
		String key = keysList.get(index);

		if (edmTypesMap.containsKey(key)) {
			prevType = ((SimpleTypeUsage) typeUsage).getSimpleType();
			SimpleType simpleType = OdataFactory.eINSTANCE.createSimpleType();
			simpleType.setType(edmTypesMap.get(key));
			newType = simpleType;
			((SimpleTypeUsage) typeUsage).setSimpleType(simpleType);
		} else if (complexTypesMap.containsKey(key)) {
			prevType = ((ComplexTypeUsage) typeUsage).getComplexType();
			ComplexType complexType = complexTypesMap.get(key);
			((ComplexTypeUsage) typeUsage).setComplexType(complexType);
			newType = complexType;
		} else if (entityTypesMap.containsKey(key)) {
			prevType = ((ReturnEntityTypeUsage) typeUsage).getEntityType();
			EntityType entityType = entityTypesMap.get(key);
			((ReturnEntityTypeUsage) typeUsage).setEntityType(entityType);
			newType = entityType;
		} else if (enumTypesMap.containsKey(key)) {
			prevType = ((EnumTypeUsage) typeUsage).getEnumType();
			EnumType enumType = enumTypesMap.get(key);
			((EnumTypeUsage) typeUsage).setEnumType(enumType);
			newType = enumType;
		}
		if (prevType != null && newType != null) {
			IODataDiagramCreator.INSTANCE.refreshEditor(typeUsage, prevType,
					newType);
		}
	}
}
