/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.type.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.api.vocabularies.ICapabilityVocabulary;
import org.eclipse.ogee.model.api.vocabularies.ICoreVocabulary;
import org.eclipse.ogee.model.api.vocabularies.IMeasuresVocabulary;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;

public abstract class AbstractTypeHandler {
	public static final String NAMESPACE_SEPARATOR = "."; //$NON-NLS-1$
	protected Map<String, EDMTypes> edmTypesMap = new TreeMap<String, EDMTypes>();
	protected Map<String, ComplexType> complexTypesMap = new TreeMap<String, ComplexType>();
	protected Map<String, EnumType> enumTypesMap = new TreeMap<String, EnumType>();
	protected Map<String, EntityType> entityTypesMap = new TreeMap<String, EntityType>();
	protected List<String> keysList = new ArrayList<String>();

	protected abstract void createMaps(Schema schema, String prefix);

	protected void createMaps(Schema schema) {
		if (schema == null)
			return;
		List<Schema> schemata = SchemaHandler.getAllSchemata(schema);
		if (schemata != null) {
			for (Schema s : schemata) {

				String prefix = SchemaHandler.getNamespaceOrAlias(s)
						+ NAMESPACE_SEPARATOR;
				createMaps(s, prefix);
			}
			removeSelectedEObject(SchemaHandler.getNamespaceOrAlias(schema)
					+ NAMESPACE_SEPARATOR);
		}
	}

	/**
	 * updating the eDMTypesMap with the set of EDMX types declared in the
	 * EDMTypes enum
	 */
	protected void createSimpleTypesMap() {
		if (edmTypesMap.isEmpty()) {
			for (EDMTypes type : EDMTypes.values()) {
				edmTypesMap.put(type.getLiteral(), type);
			}
		}
	}

	protected void createComplexTypeMap(Schema schema, String prefix) {		
		if (!prefix.equals(ICoreVocabulary.VC_NAMESPACE + ".") //$NON-NLS-1$
				&& !prefix.equals(ICapabilityVocabulary.VC_NAMESPACE + ".") //$NON-NLS-1$
				&& !prefix.equals(IMeasuresVocabulary.VC_NAMESPACE + ".")) { //$NON-NLS-1$			
			if (schema.getComplexTypes() != null) {
				for (ComplexType complexType : schema.getComplexTypes()) {
					complexTypesMap.put(prefix + complexType.getName(),
							complexType);
				}
			}
		}
	}

	protected void createEnumTypeMap(Schema schema, String prefix) {
		if (schema.getEnumTypes() != null) {
			for (EnumType enumType : schema.getEnumTypes()) {
				enumTypesMap.put(prefix + enumType.getName(), enumType);
			}
		}
	}

	protected void createEntityTypeMap(Schema schema, String prefix) {
		if (schema.getEntityTypes() != null) {
			for (EntityType entityType : schema.getEntityTypes()) {
				entityTypesMap.put(prefix + entityType.getName(), entityType);
			}
		}
	}

	/**
	 * 
	 * @param property
	 * @return the index of the property type in the array. If the property
	 *         doesn't have type nor the type doesn't exists in the schema any
	 *         more, the return value is -1.
	 */
	public int getTypeIndex(IPropertyTypeUsage propertyTypeUsage) {
		String name = null;

		if (propertyTypeUsage instanceof SimpleTypeUsage) {
			SimpleTypeUsage simpleTypeUsage = (SimpleTypeUsage) propertyTypeUsage;
			if (simpleTypeUsage.getSimpleType() != null
					&& simpleTypeUsage.getSimpleType().getType() != null)
				name = simpleTypeUsage.getSimpleType().getType().getLiteral();
		} else if (propertyTypeUsage instanceof ComplexTypeUsage) {
			ComplexTypeUsage complexTypeUsage = (ComplexTypeUsage) propertyTypeUsage;
			if (complexTypeUsage.getComplexType() != null)
				name = SchemaHandler.getNamespaceOrAlias(complexTypeUsage
						.getComplexType())
						+ NAMESPACE_SEPARATOR
						+ complexTypeUsage.getComplexType().getName();
		} else if (propertyTypeUsage instanceof EnumTypeUsage) {
			EnumTypeUsage enumTypeUsage = (EnumTypeUsage) propertyTypeUsage;
			if (enumTypeUsage.getEnumType() != null)
				name = SchemaHandler.getNamespaceOrAlias(enumTypeUsage
						.getEnumType())
						+ NAMESPACE_SEPARATOR
						+ enumTypeUsage.getEnumType().getName();
		}
		return keysList.indexOf(name);
	}

	public String[] getTypesArray() {
		return keysList.toArray(new String[keysList.size()]);
	}

	/**
	 * Remove the selected {@link EObject} from the map<BR>
	 * The list should not contain the selected {@link EObject} This method is
	 * not abstract since it is not mandotory.
	 * 
	 * @param prefix
	 *            (name space or alias of the schema + separator)
	 */
	protected void removeSelectedEObject(String prefix) {

	}

}
