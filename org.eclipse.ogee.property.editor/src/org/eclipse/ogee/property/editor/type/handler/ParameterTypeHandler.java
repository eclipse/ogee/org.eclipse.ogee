/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.type.handler;

import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.IParameterTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.property.editor.utilities.TypeUsageFactory;

public class ParameterTypeHandler extends AbstractTypeHandler {
	public ParameterTypeHandler(Parameter parameter) {
		createMaps(SchemaHandler.getSchema(parameter));
		keysList.addAll(edmTypesMap.keySet());
		keysList.addAll(complexTypesMap.keySet());
		// TODO Start - Enable with OData V4 support
		// keysList.addAll(entityTypesMap.keySet());
		// End - Enable with OData V4 support
		keysList.addAll(enumTypesMap.keySet());
	}

	@Override
	protected void createMaps(Schema schema, String prefix) {
		createSimpleTypesMap();
		createEntityTypeMap(schema, prefix);
		// TODO Start - Enable with OData V4 support
		// createComplexTypeMap(schema, prefix);
		// End - Enable with OData V4 support
		createEnumTypeMap(schema, prefix);
	}

	public int getTypeIndex(IParameterTypeUsage typeUsage) {

		if (typeUsage instanceof EntityTypeUsage) {
			if (((EntityTypeUsage) typeUsage).getEntityType() != null) {
				String name = ((EntityTypeUsage) typeUsage).getEntityType()
						.getName();
				return keysList.indexOf(SchemaHandler
						.getNamespaceOrAlias(((EntityTypeUsage) typeUsage)
								.getEntityType())
						+ NAMESPACE_SEPARATOR + name);
			} else
				return -1;
		}
		return getTypeIndex((IPropertyTypeUsage) typeUsage);

	}

	public IParameterTypeUsage getType(Integer index) {
		String key = keysList.get(index);

		if (edmTypesMap.containsKey(key)) {
			return TypeUsageFactory.createSimpleTypeUsage(edmTypesMap.get(key));
		}

		else if (complexTypesMap.containsKey(key)) {
			return TypeUsageFactory.createComplexTypeUsage(complexTypesMap
					.get(key));
		}

		else if (entityTypesMap.containsKey(key)) {
			return TypeUsageFactory.createEntityTypeUsage(entityTypesMap
					.get(key));
		}

		else if (enumTypesMap.containsKey(key)) {
			return TypeUsageFactory.createEnumTypeUsage(enumTypesMap.get(key));
		}
		return null;

	}

}
