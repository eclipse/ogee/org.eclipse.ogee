/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.type.handler;

import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.property.editor.utilities.TypeUsageFactory;

public class PropertyTypeHandler extends AbstractTypeHandler {
	private Property property;

	public PropertyTypeHandler(Property property) {
		this.property = property;
		createMaps(SchemaHandler.getSchema(property));
		keysList.addAll(edmTypesMap.keySet());
		keysList.addAll(complexTypesMap.keySet());
		keysList.addAll(enumTypesMap.keySet());
	}

	@Override
	protected void createMaps(Schema schema, String prefix) {

		if (property.getType() instanceof SimpleTypeUsage
				|| this.property.getType() == null) {
			createSimpleTypesMap();
		} else if (property.getType() instanceof ComplexTypeUsage) {
			createComplexTypeMap(schema, prefix);
		} else if (property.getType() instanceof EnumTypeUsage) {
			createEnumTypeMap(schema, prefix);
		}
	}

	@Override
	protected void removeSelectedEObject(String prefix) {
		if (property.eContainer() instanceof ComplexType) {
			complexTypesMap.remove(prefix
					+ ((ComplexType) property.eContainer()).getName());
		}
	}

	public IPropertyTypeUsage getType(Integer index) {
		String key = keysList.get(index);

		if (edmTypesMap.containsKey(key)) {
			return TypeUsageFactory.createSimpleTypeUsage(edmTypesMap.get(key));
		}

		else if (complexTypesMap.containsKey(key)) {
			return TypeUsageFactory.createComplexTypeUsage(complexTypesMap
					.get(key));
		}

		else if (enumTypesMap.containsKey(key)) {
			return TypeUsageFactory.createEnumTypeUsage(enumTypesMap.get(key));
		}

		return null;

	}

}
