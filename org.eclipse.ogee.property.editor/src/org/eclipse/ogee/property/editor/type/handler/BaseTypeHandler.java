/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.type.handler;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.property.editor.nls.messages.Messages;


public class BaseTypeHandler extends AbstractTypeHandler {
	private boolean isEntityType = false;
	private EObject eObject = null;

	public BaseTypeHandler(EntityType entityType) {
		this((EObject) entityType, true);
	}

	public BaseTypeHandler(FunctionImport functionImport) {
		this((EObject) functionImport, true);
	}

	public BaseTypeHandler(ComplexType complexType) {
		this((EObject) complexType, false);
	}

	private BaseTypeHandler(EObject eObject, boolean isEntityType) {
		this.eObject = eObject;
		this.isEntityType = isEntityType;
		createMaps(SchemaHandler.getSchema(eObject));
		keysList.addAll(complexTypesMap.keySet());
		keysList.addAll(entityTypesMap.keySet());
		keysList.add(Messages.EMPTY_ENTER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ogee.property.editor.type.handler.TypeHandler
	 * #createMaps(org.eclipse.ogee.model.odata.Schema, java.lang.String)
	 */
	@Override
	protected void createMaps(Schema schema, String prefix) {
		if (isEntityType) {
			createEntityTypeMap(schema, prefix);
		} else {
			createComplexTypeMap(schema, prefix);
		}
	}

	@Override
	protected void removeSelectedEObject(String prefix) {
		if (isEntityType) {
			if (eObject instanceof EntityType) {
				entityTypesMap
						.remove(prefix + ((EntityType) eObject).getName());
			} else if (eObject instanceof FunctionImport) {
				entityTypesMap.remove(prefix
						+ ((FunctionImport) eObject).getName());
			}
		} else {
			complexTypesMap.remove(prefix + ((ComplexType) eObject).getName());
		}
	}

	public int getIndex(String name) {
		return keysList.indexOf(name);
	}

	public String getName(int index) {
		return keysList.get(index);
	}

	public EObject getEobject(int index) {
		if (isEntityType)
			return entityTypesMap.get(keysList.get(index));
		else
			return complexTypesMap.get(keysList.get(index));
	}
}
