/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.type.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;

public class SchemaHandler {
	private Map<String, Schema> namespaceSchemaMap;
	private List<String> namespaces;

	public SchemaHandler() {
		namespaceSchemaMap = new HashMap<String, Schema>();
		namespaces = new ArrayList<String>();
	}

	/**
	 * 
	 * @param eObject
	 * @return Array of String with all the schemata under the same Data Service.
	 */
	public String[] getAllSchemata(EObject eObject) {
		EDMX mainEDMX = getMainEDMX(getSchema(eObject));
		if (mainEDMX != null && mainEDMX.getDataService() != null
				&& mainEDMX.getDataService().getSchemata() != null) {
			for (Schema s : mainEDMX.getDataService().getSchemata()) {
				// To populate the schema field with alias if present, else with
				// namespace
				if (null != s.getAlias() && !s.getAlias().isEmpty()) {
					namespaceSchemaMap.put(s.getAlias(), s);
				} else {
					namespaceSchemaMap.put(s.getNamespace(), s);
				}
			}
			namespaces.addAll(namespaceSchemaMap.keySet());
		}
		return namespaces.toArray(new String[namespaces.size()]);

	}

	public Schema getSchema(String schemaName) {
		return namespaceSchemaMap.get(schemaName);
	}

	public Schema getSchema(int index) {
		if (namespaces == null)
			return null;
		return namespaceSchemaMap.get(namespaces.get(index));
	}

	/**
	 * Extracts the schema from the EObject<br>
	 * <b>Do not</b> use this method in case your EObject is not part of the
	 * schema<br>
	 * 
	 * @param eObject
	 * @return
	 */
	public static Schema getSchema(EObject eObject) {
		EObject schema = eObject;
		while (schema != null && !(schema instanceof Schema)) {
			schema = schema.eContainer();
		}
		if (schema instanceof Schema)
			return (Schema) schema;
		else
			return null;

	}

	public static List<Schema> getAllSchemata(Schema schema) {
		List<Schema> schemata = new ArrayList<Schema>();
		schemata.addAll(getSchemata(schema));
		EDMX mainEDMX = getMainEDMX(schema);
		EList<EDMXReference> references = null;

		if (mainEDMX != null) {
			references = mainEDMX.getReferences();
		}

		if (references != null) {
			for (EDMXReference edmxReference : references) {
				if (edmxReference.getReferencedEDMX() != null
						&& edmxReference.getReferencedEDMX().getDataService() != null
						&& edmxReference.getReferencedEDMX().getDataService()
								.getSchemata() != null)
					schemata.addAll(edmxReference.getReferencedEDMX()
							.getDataService().getSchemata());
			}
		}
		return schemata;
	}

	public static List<Schema> getSchemata(Schema schema) {
		List<Schema> schemata = new ArrayList<Schema>();
		EDMX mainEDMX = getMainEDMX(schema);
		if (mainEDMX != null && mainEDMX.getDataService() != null) {
			schemata = mainEDMX.getDataService().getSchemata();
		}
		return schemata;
	}

	private static EDMX getMainEDMX(Schema schema) {
		EDMX mainEDMX = null;
		EDMXSet edmxSet = getEDMXSet(schema);
		if (edmxSet != null) {
			mainEDMX = edmxSet.getMainEDMX();
		}
		return mainEDMX;
	}

	public static EDMXSet getEDMXSet(EObject eObject) {
		return getEDMXSet(getSchema(eObject));
	}

	private static EDMXSet getEDMXSet(Schema schema) {
		EDMXSet edmxSet = null;
		if (schema != null && schema.eContainer() instanceof EDMXSet) {
			edmxSet = (EDMXSet) schema.eContainer();
		}
		return edmxSet;
	}

	/**
	 * 
	 * @param eObject
	 * @return
	 */
	public int getIndex(EObject eObject) {
		Schema schema = getSchema(eObject);
		String string = null;
		// To populate the schema field with alias if present, else with
		// namespace
		if (null != getSchema(eObject).getAlias()
				&& !getSchema(eObject).getAlias().isEmpty()) {
			string = schema != null ? schema.getAlias() : null;
			return namespaces.indexOf(string);
		} else {
			string = schema != null ? schema.getNamespace() : null;
			return namespaces.indexOf(string);

		}
	}

	public Object getInitialDescriptorValue(boolean isReadOnly, EObject eObject) {
		if (isReadOnly) {
			// To populate the schema field with alias if present, else with
			// namespace
			if (null != getSchema(eObject).getAlias()
					&& !getSchema(eObject).getAlias().isEmpty()) {
				return getSchema(eObject).getAlias();
			} else {
				return getSchema(eObject).getNamespace();
			}
		} else {
			return getIndex(eObject);
		}
	}

	public static String getNamespaceOrAlias(Schema schema) {
		/*
		 * Place holder method in order to handle the alias if exist.
		 */
		if (null != schema.getAlias() && !schema.getAlias().isEmpty()) {
			return schema.getAlias();
		} else {
			return schema.getNamespace();
		}
	}

	public static String getNamespaceOrAlias(EObject eObject) {
		return getNamespaceOrAlias(getSchema(eObject));
	}
}
