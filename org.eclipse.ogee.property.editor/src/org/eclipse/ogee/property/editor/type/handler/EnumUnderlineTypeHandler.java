/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.type.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.model.odata.EDMTypes;

public class EnumUnderlineTypeHandler {
	private Map<String, EDMTypes> edmTypesMap;
	private List<String> keyList;

	public EnumUnderlineTypeHandler(EList<EDMTypes> list) {
		edmTypesMap = new TreeMap<String, EDMTypes>();
		if (list != null) {
			createUnderlineTypesMap(list);
		}

		keyList = new ArrayList<String>(edmTypesMap.keySet());
	}

	private void createUnderlineTypesMap(EList<EDMTypes> list) {
		for (EDMTypes edmTypes : list) {
			edmTypesMap.put(edmTypes.getName(), edmTypes);
		}

	}

	public String[] getEDMTypesNames() {
		return keyList.toArray(new String[keyList.size()]);
	}

	public int getIndex(String name) {
		return keyList.indexOf(name);
	}

	public EDMTypes getUnderlineType(int index) {
		return edmTypesMap.get(keyList.get(index));
	}

}
