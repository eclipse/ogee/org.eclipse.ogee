/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations;

import java.util.ArrayList;
import java.util.Map.Entry;

import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.PropertyDescriptorType;

public class AnnotationTabRecord {

	private Entry<Property, AnnotationValue> entry;
	private ValueAnnotation valueAnnotation;
	TermResolver resolver;

	private PropertyDescriptorType propertyDesc;
	private ArrayList<String> comboBoxOptionsList;
	private ArrayList<Object> newValuesList;

	private Object currentValue;
	private IAnnotationTarget annotationTarget;

	public AnnotationTabRecord(ValueAnnotation valueAnnotation,
			IAnnotationTarget annotationTarget) {
		this.valueAnnotation = valueAnnotation;
		this.resolver = new TermResolver(valueAnnotation.getTerm());
		this.annotationTarget = annotationTarget;
		this.newValuesList = new ArrayList<Object>();
	}

	public AnnotationTabRecord(Entry<Property, AnnotationValue> entry,
			IAnnotationTarget annotationTarget) {
		this.entry = entry;
		this.resolver = new TermResolver(entry.getKey());
		this.annotationTarget = annotationTarget;
		this.newValuesList = new ArrayList<Object>();
	}

	/**
	 * 
	 * @return the object that holds the AnnotationValue
	 */
	public Object getObjectId() {
		if (entry != null) {
			return getEntry();
		} else {
			return getValueAnnotation();
		}

	}

	public IAnnotationTarget getAnnotationTarget() {
		return annotationTarget;
	}

	public void setPropertyDesc(PropertyDescriptorType propertyDesc) {
		this.propertyDesc = propertyDesc;
	}

	private Entry<Property, AnnotationValue> getEntry() {
		return entry;
	}

	public ArrayList<Object> getNewValuesList() {
		return newValuesList;
	}

	public void setNewValuesList(ArrayList<Object> newValuesList) {
		this.newValuesList = newValuesList;
	}

	private ValueAnnotation getValueAnnotation() {
		return valueAnnotation;
	}

	public ArrayList<String> getComboBoxOptionsList() {
		return comboBoxOptionsList;
	}

	public void setComboBoxOptionsList(ArrayList<String> comboBoxOptionsList) {
		this.comboBoxOptionsList = comboBoxOptionsList;
	}

	public Object getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(Object currentValue) {
		this.currentValue = currentValue;
	}

	public PropertyDescriptorType getPropertyDesc() {
		return propertyDesc;
	}

	public String getName() {
		return resolver.getName();
	}

	public IPropertyTypeUsage getTypeUsage() {
		return resolver.getTypeUsage();

	}

	public IAnnotationTarget getTerm() {
		return resolver.getEObject();
	}

	public AnnotationValue getAnnotationValue() {
		if (entry != null) {
			return entry.getValue();
		} else {
			return valueAnnotation.getAnnotationValue();
		}
	}

	public void setAnnotationValue(AnnotationValue annotationValue) {
		if (entry != null) {
			entry.setValue(annotationValue);
		} else {
			valueAnnotation.setAnnotationValue(annotationValue);
		}
	}

	public void setPropertyDescriptorData(PropertyDescriptorType propertyDesc,
			Object currentValue) {
		setPropertyDescriptorData(propertyDesc, currentValue, null);
	}

	public void setPropertyDescriptorData(PropertyDescriptorType propertyDesc,
			Object currentValue, ArrayList<String> List) {
		setPropertyDesc(propertyDesc);
		setCurrentValue(currentValue);
		setComboBoxOptionsList(List);
	}

	public String getDocumentationSummery() {
		if (entry != null) {
			if (entry.getKey().getDocumentation() != null) {
				return entry.getKey().getDocumentation().getSummary();
			}
		} else {
			if (valueAnnotation.getTerm().getDocumentation() != null) {
				return valueAnnotation.getTerm().getDocumentation()
						.getSummary();
			}
		}
		return Messages.VocabulariesDialog_No_description_available;
	}
}
