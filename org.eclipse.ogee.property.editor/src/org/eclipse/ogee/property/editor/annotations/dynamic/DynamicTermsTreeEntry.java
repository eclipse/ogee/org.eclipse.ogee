/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations.dynamic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.property.editor.nls.messages.Messages;

public class DynamicTermsTreeEntry {
	private AnnotationValue annotationValue;
	private String displayValue;
	private IPropertyTypeUsage typeUsage;
	private boolean record;
	private List<DynamicTermsTreeEntry> childrenList;
	private IAnnotationTarget termDefinition;
	private boolean isCollectionParent;
	private DynamicTermsTreeEntry parent;

	private boolean isEmpty;

	public DynamicTermsTreeEntry(AnnotationValue annotationValue,
			String displayValue, IAnnotationTarget termDefinition) {
		this.annotationValue = annotationValue;
		this.displayValue = displayValue;
		this.termDefinition = termDefinition;

		if (termDefinition instanceof Property) {
			this.typeUsage = ((Property) termDefinition).getType();
		} else if (termDefinition instanceof ValueTerm) {
			this.typeUsage = ((ValueTerm) termDefinition).getType();
		}

		this.record = (typeUsage instanceof ComplexTypeUsage);
		this.isCollectionParent = false;
		this.childrenList = new ArrayList<DynamicTermsTreeEntry>();

		updateStatus();
	}

	public AnnotationValue getAnnotationValue() {
		return annotationValue;
	}

	public void setAnnotationValue(AnnotationValue annotationValue) {
		this.annotationValue = annotationValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String value) {
		displayValue = value;
		updateStatus();
	}

	public IPropertyTypeUsage getTypeUsage() {
		return typeUsage;
	}

	public void setTypeUsage(IPropertyTypeUsage typeUsage) {
		this.typeUsage = typeUsage;
	}

	public boolean isRecord() {
		return record;
	}

	public void setRecord(boolean record) {
		this.record = record;
	}

	public List<DynamicTermsTreeEntry> getChildrenList() {
		return childrenList;
	}

	public void addChild(DynamicTermsTreeEntry child) {
		childrenList.add(child);
		updateStatus();
	}

	public void removeChild(DynamicTermsTreeEntry child) {
		childrenList.remove(child);
		updateStatus();
	}

	protected void updateStatus() {
		boolean newIsEmpty = Messages.EMPTY_String.equals(displayValue);

		if (newIsEmpty) {
			for (DynamicTermsTreeEntry child : getChildrenList()) {
				newIsEmpty = (child.isEmpty() && newIsEmpty);
			}
		}

		boolean changed = isEmpty ^ newIsEmpty;
		isEmpty = newIsEmpty;

		if (changed && getParent() != null) {
			getParent().updateStatus();
		}

	}

	public void addChild() {
		DynamicTermsTreeEntry child = new DynamicTermsTreeEntry(null,
				Messages.EMPTY_String, getTermDefinition());
		child.setParent(this);
		childrenList.add(child);
	}

	public IAnnotationTarget getTermDefinition() {
		return termDefinition;
	}

	public void setTermDefinition(IAnnotationTarget termDefinition) {
		this.termDefinition = termDefinition;
	}

	public boolean isCollectionParent() {
		return isCollectionParent;
	}

	public void setIsCollectionParent(boolean isCollectionParent) {
		this.isCollectionParent = isCollectionParent;
	}

	public DynamicTermsTreeEntry getParent() {
		return parent;
	}

	public void setParent(DynamicTermsTreeEntry parent) {
		this.parent = parent;
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	public void clean() {
		for (DynamicTermsTreeEntry child : getChildrenList()) {
			child.clean();
		}

		if (isCollectionParent()) {
			Set<DynamicTermsTreeEntry> childrenSet = new HashSet<DynamicTermsTreeEntry>(
					getChildrenList());
			for (DynamicTermsTreeEntry child : childrenSet) {
				if (child.isEmpty()) {
					removeChild(child);
				}
			}
		}
	}

}
