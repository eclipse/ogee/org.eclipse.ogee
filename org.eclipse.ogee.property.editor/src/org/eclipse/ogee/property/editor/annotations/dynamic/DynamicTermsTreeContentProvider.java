/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations.dynamic;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class DynamicTermsTreeContentProvider implements ITreeContentProvider {
	private DynamicTermsModel dynamicTermsModel;
	private boolean isReadOnly;

	public DynamicTermsTreeContentProvider(DynamicTermsModel dynamicTermsModel,
			boolean isReadOnly) {
		super();
		this.isReadOnly = isReadOnly;
		this.dynamicTermsModel = dynamicTermsModel;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {

	}

	@Override
	public Object[] getChildren(Object arg0) {
		return (dynamicTermsModel.getChildren(((DynamicTermsTreeEntry) arg0),
				isReadOnly)).toArray();
	}

	@Override
	public Object[] getElements(Object arg0) {
		return new Object[] { ((DynamicTermsModel) arg0).getRootElement() };

	}

	@Override
	public Object getParent(Object arg0) {
		return (((DynamicTermsTreeEntry) arg0)).getParent();
	}

	@Override
	public boolean hasChildren(Object arg0) {
		return (((DynamicTermsTreeEntry) arg0).isRecord() || ((DynamicTermsTreeEntry) arg0)
				.isCollectionParent());
	}

}
