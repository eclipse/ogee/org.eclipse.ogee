/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.RecordValue;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.property.editor.property.descriptor.PropertyDescriptorFactory;
import org.eclipse.ogee.property.editor.property.descriptor.PropertyDescriptorType;
import org.eclipse.ogee.property.editor.property.sources.AnnotationODataSource;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.PropertyDescriptor;

/**
 * This class is used to create propertyDescriptores set dynamically for
 * annotations. </br> There are two enter points to the class and to the
 * process:</br> 1. createTargetAnnotationPropertyDescriptors - When creating
 * the propertyDescriptores set from the AnnotationTarget itself. </br> 2.
 * createAnnotationPropertyDescriptors - When creating the propertyDescriptores
 * set not from the AnnotationTarget. In that case this is a second level of
 * propertyDescriptores and the.
 * 
 * 
 */
public class AnnotationHelper {

	/**
	 * The first level of the annotations. Starts to investigate the annotations
	 * from the annotationTarget. goes over all the annotation of this
	 * AnnotationTarget
	 * 
	 * 
	 * @param annotationTarget
	 * @param annotationHelperMap
	 *            key: the id of the propertyDescriptores, usually the
	 *            annotation.
	 * @param isReadOnly
	 * @return a set of the IPropertyDescriptor
	 */
	public Set<IPropertyDescriptor> createTargetAnnotationPropertyDescriptors(
			IAnnotationTarget annotationTarget,
			Map<Object, AnnotationTabRecord> annotationHelperMap,
			boolean isReadOnly) {

		Set<IPropertyDescriptor> propertyDescriptorSet = new HashSet<IPropertyDescriptor>();
		List<Annotation> annotations = annotationTarget.getAnnotations();

		for (Annotation annotation : annotations) {

			if (annotation instanceof ValueAnnotation) {

				AnnotationTabRecord record = new AnnotationTabRecord(
						(ValueAnnotation) annotation, annotationTarget);
				annotationHelperMap.put(annotation, record);

				propertyDescriptorSet.add(createPropertyDescriptor(record,
						isReadOnly));

			}

		}

		propertyDescriptorSet.remove(null);
		return propertyDescriptorSet;
	}

	/**
	 * The second level and beyond of the annotations. starts from certain
	 * annotaionValue, which must be of type RecordValue.
	 * 
	 * @param annotaionValue
	 * @param annotationTarget
	 * @param annotationHelperMap
	 *            key: the id of the propertyDescriptores, usually the
	 *            annotation.
	 * @return a set of the IPropertyDescriptor
	 */
	public Set<IPropertyDescriptor> createAnnotationPropertyDescriptors(
			AnnotationValue annotaionValue, IAnnotationTarget annotationTarget,
			Map<Object, AnnotationTabRecord> annotationHelperMap,
			boolean isReadOnly) {

		Set<IPropertyDescriptor> propertyDescriptorSet = new HashSet<IPropertyDescriptor>();
		if (annotaionValue instanceof RecordValue) {
			EMap<Property, AnnotationValue> propertyValues = ((RecordValue) annotaionValue)
					.getPropertyValues();

			for (Entry<Property, AnnotationValue> entry : propertyValues) {
				AnnotationTabRecord record = new AnnotationTabRecord(entry,
						annotationTarget);
				annotationHelperMap.put(entry, record);

				propertyDescriptorSet.add(createPropertyDescriptor(record,
						isReadOnly));
			}

		}

		propertyDescriptorSet.remove(null);
		return propertyDescriptorSet;
	}

	/**
	 * Creates the PropertyDescriptor according to the different types of
	 * annotations.
	 * 
	 * @param record
	 * @param annotationValue
	 * @param isReadOnly
	 * @return
	 */
	private IPropertyDescriptor createPropertyDescriptor(
			AnnotationTabRecord record, boolean isReadOnly) {
		IPropertyTypeUsage typeUsage = record.getTypeUsage();
		AnnotationValue annotationValue = record.getAnnotationValue();
		if (typeUsage.isCollection()) {
			int numberOfCollection = 0;
			if (annotationValue != null) {
				numberOfCollection = ((ValueCollection) annotationValue)
						.getValues().size();
			}
			record.setPropertyDescriptorData(
					PropertyDescriptorType.ButtonPropertyDescriptor,
					Utilities
							.createAnnotationCollectionString(numberOfCollection));

			return PropertyDescriptorFactory.getInstance()
					.createButtonPropertyDescriptor(record.getObjectId(),
							record.getName(), record, isReadOnly);

		} else if (typeUsage instanceof SimpleTypeUsage) {
			return SimplePathValuePropertyDescriptorResolver
					.createPropertyDescriptor(record, annotationValue,
							isReadOnly);
		} else if (typeUsage instanceof ComplexTypeUsage) {
			record.setCurrentValue(new AnnotationODataSource(annotationValue,
					record.getAnnotationTarget(), isReadOnly));
			record.setPropertyDesc(PropertyDescriptorType.PropertyDescriptor);

			return new PropertyDescriptor(record.getObjectId(),
					record.getName());
		}

		return null;
	}

	private static AnnotationHelper instance;

	private AnnotationHelper() {
		// empty constructor
	}

	public static AnnotationHelper getInstance() {
		if (instance == null) {
			instance = new AnnotationHelper();
		}
		return instance;
	}

}
