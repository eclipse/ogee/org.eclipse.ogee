/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations.dynamic;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.property.editor.nls.messages.Messages;

public class AnnotationLabelProvider extends ColumnLabelProvider {

	public String getText(Object element) {
		String name = Messages.EMPTY_String;
		String type = Messages.EMPTY_String;
		if (element instanceof DynamicTermsTreeEntry) {
			if (((DynamicTermsTreeEntry) element).getTermDefinition() instanceof Property) {
				name = ((Property) ((DynamicTermsTreeEntry) element)
						.getTermDefinition()).getName();
			} else {
				name = ((ValueTerm) ((DynamicTermsTreeEntry) element)
						.getTermDefinition()).getName();
			}

			IPropertyTypeUsage typeUsage = ((DynamicTermsTreeEntry) element)
					.getTypeUsage();

			if (typeUsage instanceof SimpleTypeUsage) {
				type = ((SimpleTypeUsage) typeUsage).getSimpleType().getType()
						.getLiteral();

			} else if (typeUsage instanceof ComplexTypeUsage) {
				type = ((ComplexTypeUsage) typeUsage).getComplexType()
						.getName();
			}

			if (((DynamicTermsTreeEntry) element).isCollectionParent()) {
				return String.format(Messages.AnnotationLabelProvider_0, name,
						type);
			} else
				return String.format(Messages.AnnotationLabelProvider_1, name,
						type);
		}

		return name;
	}

}
