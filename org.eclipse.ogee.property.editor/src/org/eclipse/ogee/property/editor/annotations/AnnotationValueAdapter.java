/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations;

import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.PathValue;
import org.eclipse.ogee.model.odata.SimpleValue;

public class AnnotationValueAdapter {

	private static AnnotationValueAdapter instance;

	private AnnotationValueAdapter() {
		// default constructor
	}

	public PathValue createPathValue(String value) {
		PathValue pathValue = OdataFactory.eINSTANCE.createPathValue();
		pathValue.setPath(value);
		return pathValue;
	}

	public SimpleValue createSimpleValue(Object value, EDMTypes edmType) {
		SimpleValue simpleValue = null;
		switch (edmType) {
		case BOOLEAN:
			simpleValue = OdataFactory.eINSTANCE.createBooleanValue();
			break;
		case BYTE:
			simpleValue = OdataFactory.eINSTANCE.createByteValue();
			break;
		case DECIMAL:
			simpleValue = OdataFactory.eINSTANCE.createDecimalValue();
			break;
		case DOUBLE:
			simpleValue = OdataFactory.eINSTANCE.createDoubleValue();
			break;
		case INT16:
			simpleValue = OdataFactory.eINSTANCE.createInt16Value();
			break;
		case INT32:
			simpleValue = OdataFactory.eINSTANCE.createInt32Value();
			break;
		case INT64:
			simpleValue = OdataFactory.eINSTANCE.createInt64Value();
			break;
		case SINGLE:
			simpleValue = OdataFactory.eINSTANCE.createSingleValue();
			break;
		case STRING:
			simpleValue = OdataFactory.eINSTANCE.createStringValue();
			break;
		case BINARY:
			simpleValue = OdataFactory.eINSTANCE.createBinaryValue();
			break;
		case DATE_TIME:
			simpleValue = OdataFactory.eINSTANCE.createDateTimeValue();
			break;
		case DATE_TIME_OFFSET:
			simpleValue = OdataFactory.eINSTANCE.createDateTimeOffsetValue();
			break;
		case GUID:
			simpleValue = OdataFactory.eINSTANCE.createGuidValue();
			break;
		case SBYTE:
			simpleValue = OdataFactory.eINSTANCE.createSByteValue();
			break;
		case TIME:
			simpleValue = OdataFactory.eINSTANCE.createTimeValue();
			break;
		default:
			break;
		}

		if (simpleValue != null) {
			simpleValue.setValueObject(value);
		}
		return simpleValue;
	}

	public static AnnotationValueAdapter getInstance() {
		if (instance == null) {
			instance = new AnnotationValueAdapter();
		}
		return instance;
	}
}
