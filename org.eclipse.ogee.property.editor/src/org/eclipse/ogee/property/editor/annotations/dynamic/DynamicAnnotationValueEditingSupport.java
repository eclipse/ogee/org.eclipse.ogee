/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations.dynamic;

import java.util.ArrayList;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.property.editor.annotations.AnnotationTabRecord;
import org.eclipse.ogee.property.editor.annotations.PathValueResolver;
import org.eclipse.ogee.property.editor.ui.elements.dialog.DynamicTermsDialog;
import org.eclipse.ogee.property.editor.utilities.Validator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

public class DynamicAnnotationValueEditingSupport extends EditingSupport {
	private AnnotationTabRecord annotationTabRecord;
	private DynamicTermsDialog dynamicTermsDialog;
	private ComboBoxCellEditor textComboCellEditor;
	private ArrayList<String> pathList;

	public DynamicAnnotationValueEditingSupport(ColumnViewer viewer,
			AnnotationTabRecord annotationTabRecord,
			DynamicTermsDialog dynamicTermsDialog) {

		super(viewer);
		this.annotationTabRecord = annotationTabRecord;

		this.dynamicTermsDialog = dynamicTermsDialog;
	}

	@Override
	protected boolean canEdit(Object arg0) {
		if (dynamicTermsDialog.isReadOnly()
				|| ((DynamicTermsTreeEntry) arg0).isCollectionParent()
				|| ((DynamicTermsTreeEntry) arg0).isRecord()) {
			return false;
		}
		return true;
	}

	@Override
	protected CellEditor getCellEditor(Object arg0) {

		PathValueResolver pathValueResolver = new PathValueResolver(
				annotationTabRecord.getAnnotationTarget(),
				((DynamicTermsTreeEntry) arg0).getTermDefinition(),
				((DynamicTermsTreeEntry) arg0).getTypeUsage());
		pathList = pathValueResolver.getValues();

		if (pathValueResolver.isPropertyPathFlag()) {
			textComboCellEditor = new ComboBoxCellEditor(
					(Composite) getViewer().getControl(),
					pathList.toArray(new String[pathList.size()]),
					SWT.READ_ONLY);
			return textComboCellEditor;

		} else {
			return new TextCellEditor((Composite) getViewer().getControl());

		}

	}

	@Override
	protected Object getValue(Object arg0) {
		if (textComboCellEditor == null) {
			return ((DynamicTermsTreeEntry) arg0).getDisplayValue();
		} else {
			return 0;
		}
	}

	@Override
	protected void setValue(Object arg0, Object arg1) {
		String selectedValue;
		if (arg1 != null) {
			if (textComboCellEditor == null) {
				selectedValue = (String) arg1;
				IPropertyTypeUsage typeUsage = ((DynamicTermsTreeEntry) arg0)
						.getTypeUsage();
				if (typeUsage instanceof SimpleTypeUsage) {
					EDMTypes type = ((SimpleTypeUsage) typeUsage)
							.getSimpleType().getType();
					String validate = Validator.validate(selectedValue, type);
					if (validate != null) {
						dynamicTermsDialog.enableApplyButton(false, validate);
						return;
					}
				}
			} else {
				// Get the selected value from comboBox
				selectedValue = pathList.get((Integer) arg1);
			}

			((DynamicTermsTreeEntry) arg0).setDisplayValue(selectedValue);
			dynamicTermsDialog.enableApplyButton(true, ""); //$NON-NLS-1$

			getViewer().refresh();
		}
	}

}
