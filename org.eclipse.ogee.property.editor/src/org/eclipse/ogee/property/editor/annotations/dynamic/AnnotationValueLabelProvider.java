/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations.dynamic;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.ogee.property.editor.nls.messages.Messages;

public class AnnotationValueLabelProvider extends ColumnLabelProvider {

	@Override
	public String getText(Object element) {

		if (element instanceof DynamicTermsTreeEntry) {
			return (((DynamicTermsTreeEntry) element).getDisplayValue())
					.toString();
		}
		return Messages.EMPTY_String;
	}
}
