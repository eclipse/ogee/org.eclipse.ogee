/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations;

import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ValueTerm;

public class TermResolver {
	private Property property;
	private ValueTerm term;

	public TermResolver(Property property) {
		this.property = property;
	}

	public TermResolver(ValueTerm valueTerm) {
		this.term = valueTerm;
	}

	public String getName() {
		if (property != null) {
			return property.getName();
		} else {
			return term.getName();
		}
	}

	public IPropertyTypeUsage getTypeUsage() {
		if (property != null) {
			return property.getType();
		} else {
			return term.getType();
		}

	}

	public IAnnotationTarget getEObject() {
		if (property != null) {
			return property;
		} else {
			return term;
		}

	}

}
