/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.PathValue;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.PropertyDescriptorFactory;
import org.eclipse.ogee.property.editor.property.descriptor.PropertyDescriptorType;
import org.eclipse.ogee.property.editor.utilities.TypeConverter;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

public class SimplePathValuePropertyDescriptorResolver {

	public static IPropertyDescriptor createPropertyDescriptor(
			AnnotationTabRecord record, AnnotationValue annotationValue,
			boolean isReadOnly) {
		// if there is no current value, then it is an empty string
		Object currentValue = Messages.EMPTY_String;
		if (annotationValue instanceof SimpleValue
				&& (((SimpleValue) annotationValue).getValueObject() != null)) {
			currentValue = ((SimpleValue) annotationValue).getValueObject();
		}
		if (annotationValue instanceof PathValue
				&& (((PathValue) annotationValue).getPath() != null)) {
			currentValue = ((PathValue) annotationValue).getPath();
		}

		if (isReadOnly) {
			record.setPropertyDescriptorData(
					PropertyDescriptorType.PropertyDescriptor,
					currentValue.toString());
			return PropertyDescriptorFactory.getInstance()
					.createPropertyDescriptor(record.getObjectId(),
							record.getName());
		}

		PathValueResolver annotationValueResolver = new PathValueResolver(
				record);

		ArrayList<String> pathList = annotationValueResolver.getValues();
		if (annotationValueResolver.isPropertyPathFlag()) {
			return createPathPropertyDescriptor(record, currentValue, pathList);
		}
		return createSimplePropertyDescriptor(record, currentValue,
				((SimpleTypeUsage) record.getTypeUsage()).getSimpleType(),
				pathList);

	}

	private static IPropertyDescriptor createSimplePropertyDescriptor(
			AnnotationTabRecord record, Object currentValue,
			SimpleType simpleType, ArrayList<String> pathList) {
		String fieldFormatted = null;

		switch (simpleType.getType()) {
		case BOOLEAN:
			// The list of options in the Boolean case contains the values
			// false and true in the first two places
			pathList.addAll(0, Arrays.asList(Utilities.boolValues));
			int index = pathList.indexOf(currentValue);
			if (index == -1 && currentValue != Messages.EMPTY_String) {
				currentValue = (Boolean) (currentValue) ? 1 : 0;
			} else {
				currentValue = index;
			}

			record.setPropertyDescriptorData(
					PropertyDescriptorType.ComboBoxPropertyDescriptor,
					currentValue, pathList);
			return PropertyDescriptorFactory.getInstance()
					.createComboBoxPropertyDescriptor(record.getObjectId(),
							record.getName(),
							pathList.toArray(new String[pathList.size()]));

		case SINGLE:
			fieldFormatted = TypeConverter
					.formatNumberOrEmptyString(currentValue);
			record.setPropertyDescriptorData(
					PropertyDescriptorType.FloatTextComboBoxPropertyDescriptor,
					fieldFormatted, pathList);
			return PropertyDescriptorFactory.getInstance()
					.createFloatTextComboBoxPropertyDescriptor(
							record.getObjectId(), record.getName(), pathList);
		case DOUBLE:
			fieldFormatted = TypeConverter
					.formatNumberOrEmptyString(currentValue);
			record.setPropertyDescriptorData(
					PropertyDescriptorType.DoubleTextComboBoxPropertyDescriptor,
					fieldFormatted, pathList);
			return PropertyDescriptorFactory.getInstance()
					.createDoubleTextComboBoxPropertyDescriptor(
							record.getObjectId(), record.getName(), pathList);
		case DECIMAL:
			fieldFormatted = TypeConverter
					.formatNumberOrEmptyString(currentValue);
			record.setPropertyDescriptorData(
					PropertyDescriptorType.DecimalTextComboBoxPropertyDescriptor,
					fieldFormatted, pathList);
			return PropertyDescriptorFactory.getInstance()
					.createDecimalTextComboBoxPropertyDescriptor(
							record.getObjectId(), record.getName(), pathList);
		case INT16:
			fieldFormatted = TypeConverter
					.formatNumberOrEmptyString(currentValue);
			record.setPropertyDescriptorData(
					PropertyDescriptorType.ShortTextComboBoxPropertyDescriptor,
					fieldFormatted, pathList);
			return PropertyDescriptorFactory.getInstance()
					.createShortTextComboBoxPropertyDescriptor(
							record.getObjectId(), record.getName(), pathList);
		case INT32:
			fieldFormatted = TypeConverter
					.formatNumberOrEmptyString(currentValue);
			record.setPropertyDescriptorData(
					PropertyDescriptorType.Int32PropertyDescriptorValidator,
					fieldFormatted, pathList);
			return PropertyDescriptorFactory.getInstance()
					.createIntegerTextComboBoxPropertyDescriptor(
							record.getObjectId(), record.getName(), pathList);
		case INT64:
			fieldFormatted = TypeConverter
					.formatNumberOrEmptyString(currentValue);
			record.setPropertyDescriptorData(
					PropertyDescriptorType.LongTextComboBoxPropertyDescriptor,
					fieldFormatted, pathList);
			return PropertyDescriptorFactory.getInstance()
					.createLongTextComboBoxPropertyDescriptor(
							record.getObjectId(), record.getName(), pathList);
		case STRING:
			record.setPropertyDescriptorData(
					PropertyDescriptorType.TextPropertyDescriptor,
					currentValue, pathList);
			return PropertyDescriptorFactory.getInstance()
					.createStringTextComboBoxPropertyDescriptor(
							record.getObjectId(), record.getName(), pathList);
		case SBYTE:
		case BYTE:
			record.setPropertyDescriptorData(
					PropertyDescriptorType.ByteTextComboBoxPropertyDescriptor,
					currentValue.toString(), pathList);
			return PropertyDescriptorFactory.getInstance()
					.createByteTextComboBoxPropertyDescriptor(
							record.getObjectId(), record.getName(), pathList);
		case DATE_TIME:
			fieldFormatted = TypeConverter.formatDateTime(currentValue);
			record.setPropertyDescriptorData(
					PropertyDescriptorType.DateTimeTextComboBoxPropertyDescriptorValidator,
					fieldFormatted, pathList);
			return PropertyDescriptorFactory.getInstance()
					.createDateTimeTextComboBoxPropertyDescriptorValidator(
							record.getObjectId(), record.getName(), pathList);
		case TIME:
			fieldFormatted = TypeConverter.formatTime(currentValue);
			record.setPropertyDescriptorData(
					PropertyDescriptorType.TimeTextComboBoxPropertyDescriptorValidator,
					fieldFormatted, pathList);
			return PropertyDescriptorFactory.getInstance()
					.createTimeTextComboBoxPropertyDescriptorValidator(
							record.getObjectId(), record.getName(), pathList);
		case DATE_TIME_OFFSET:
			fieldFormatted = TypeConverter.formatDateTimeOffset(currentValue);
			record.setPropertyDescriptorData(
					PropertyDescriptorType.DateTimeOffsetTextComboBoxPropertyDescriptorValidator,
					fieldFormatted, pathList);
			return PropertyDescriptorFactory
					.getInstance()
					.createDateTimeOffsetTextComboBoxPropertyDescriptorValidator(
							record.getObjectId(), record.getName(), pathList);

		case GUID:
			record.setPropertyDescriptorData(
					PropertyDescriptorType.GUIDTextComboBoxPropertyDescriptor,
					currentValue.toString(), pathList);
			return PropertyDescriptorFactory.getInstance()
					.GUIDTextComboBoxPropertyDescriptor(record.getObjectId(),
							record.getName(), pathList);
		case BINARY:		
		default:
			record.setPropertyDescriptorData(
					PropertyDescriptorType.PropertyDescriptor,
					currentValue.toString(), pathList);
			return PropertyDescriptorFactory.getInstance()
					.createPropertyDescriptor(record.getObjectId(),
							record.getName());
		}
	}

	private static IPropertyDescriptor createPathPropertyDescriptor(
			AnnotationTabRecord record, Object currentValue,
			ArrayList<String> pathList) {

		record.setPropertyDescriptorData(
				PropertyDescriptorType.ComboBoxPropertyDescriptor,
				currentValue, pathList);
		return PropertyDescriptorFactory.getInstance()
				.createComboBoxPropertyDescriptor(record.getObjectId(),
						record.getName(),
						pathList.toArray(new String[pathList.size()]));
	}

}
