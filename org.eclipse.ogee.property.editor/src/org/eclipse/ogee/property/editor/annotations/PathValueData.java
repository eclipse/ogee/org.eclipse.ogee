/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations;

import org.eclipse.ogee.model.odata.EDMTypes;

public class PathValueData {
	private String pathValueString;
	private EDMTypes edmType;
	private boolean structured;

	public PathValueData(String pathValueStr, EDMTypes edmType,
			boolean isConcatenated) {
		this.pathValueString = pathValueStr;
		this.edmType = edmType;
		this.structured = isConcatenated;
	}

	public String getPathValueString() {
		return pathValueString;
	}

	public EDMTypes getEdmType() {
		return edmType;
	}

	public boolean isStructured() {
		return structured;
	}

}
