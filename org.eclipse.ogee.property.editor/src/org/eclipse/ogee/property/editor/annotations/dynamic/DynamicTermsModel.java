/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations.dynamic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.CollectableExpression;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.ConstantExpression;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.RecordValue;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.property.editor.annotations.AnnotationTabRecord;
import org.eclipse.ogee.property.editor.annotations.PathValueResolver;
import org.eclipse.ogee.property.editor.commands.TransactionDynamicTermsChangeCommand;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class DynamicTermsModel {

	private DynamicTermsTreeEntry rootEntry;
	private AnnotationTabRecord annotationTabRecord;

	public DynamicTermsModel(AnnotationTabRecord annotationTabRecord) {
		this.annotationTabRecord = annotationTabRecord;
		buildModel();
	}

	private void buildModel() {
		rootEntry = new DynamicTermsTreeEntry(
				(ValueCollection) annotationTabRecord.getAnnotationValue(),
				Messages.EMPTY_String, annotationTabRecord.getTerm());
		rootEntry.setIsCollectionParent(true);

		extructCollectionValues(rootEntry);
	}

	// These 2 methods works together in order to extract the annotations
	// values
	private void extructCollectionValues(DynamicTermsTreeEntry entry) {
		EList<CollectableExpression> existingAnnotation = ((ValueCollection) entry
				.getAnnotationValue()).getValues();

		if (!existingAnnotation.isEmpty()) {
			for (CollectableExpression collectableExpression : existingAnnotation) {
				DynamicTermsTreeEntry childEntry = new DynamicTermsTreeEntry(
						collectableExpression, Messages.EMPTY_String,
						entry.getTermDefinition());
				childEntry.setParent(entry);
				entry.addChild(childEntry);

				if (collectableExpression instanceof ConstantExpression) {
					if (((SimpleValue) collectableExpression).getValueObject() != null) {
						childEntry
								.setDisplayValue(((SimpleValue) collectableExpression)
										.getValueObject().toString());
					}
				} else {
					extructPropertyValues(childEntry);
				}
			}
		}
	}

	private void extructPropertyValues(DynamicTermsTreeEntry parentEntry) {
		EMap<Property, AnnotationValue> propertyValues = ((RecordValue) parentEntry
				.getAnnotationValue()).getPropertyValues();

		for (Entry<Property, AnnotationValue> entry : propertyValues) {
			DynamicTermsTreeEntry childEntry = new DynamicTermsTreeEntry(
					(AnnotationValue) entry.getValue(), Messages.EMPTY_String,
					entry.getKey());

			childEntry.setParent(parentEntry);
			parentEntry.addChild(childEntry);

			// Update the value in case of simpleValue
			if (entry.getValue() instanceof SimpleValue
					&& ((SimpleValue) entry.getValue()).getValueObject() != null) {
				childEntry.setDisplayValue(((SimpleValue) entry.getValue())
						.getValueObject().toString());
			}

			// Update the children in case of record
			else if (entry.getValue() instanceof RecordValue) {
				extructPropertyValues(childEntry);
			}

			// Update the children in case of collection
			else if (entry.getValue() instanceof ValueCollection) {
				childEntry.setIsCollectionParent(true);
				extructCollectionValues(childEntry);
			}

		}
	}

	public DynamicTermsTreeEntry getRootElement() {
		return rootEntry;
	}

	public List<DynamicTermsTreeEntry> getChildren(DynamicTermsTreeEntry entry,
			boolean isReadOnly) {

		if (!isReadOnly) {
			// In case there are no children, create new child
			// add first child to collection
			if (entry.isCollectionParent() && entry.getChildrenList().isEmpty()) {
				addChild(entry);
			} else if (entry.isRecord() && entry.getChildrenList().isEmpty()) {
				// new record children row
				EList<Property> properties = ((ComplexTypeUsage) entry
						.getTypeUsage()).getComplexType().getProperties();
				for (Property property : properties) {
					DynamicTermsTreeEntry dynamicTermsTreeEntry = new DynamicTermsTreeEntry(
							null, Messages.EMPTY_String, property);
					dynamicTermsTreeEntry.setIsCollectionParent(property
							.getType().isCollection());
					dynamicTermsTreeEntry.setParent(entry);
					entry.addChild(dynamicTermsTreeEntry);
				}
			}
		}

		return entry.getChildrenList();
	}

	public void addChild(DynamicTermsTreeEntry entry) {
		entry.addChild();
	}

	public void remove(DynamicTermsTreeEntry child) {
		child.getParent().removeChild(child);
	}

	public boolean isPath(String value) {
		PathValueResolver annotationValueResolver = new PathValueResolver(
				annotationTabRecord);
		ArrayList<String> pathList = annotationValueResolver.getValues();

		return pathList.contains(value);
	}

	public void updateTerms(AnnotationTabRecord annotationTabRecord) {
		rootEntry.clean();
		TransactionDynamicTermsChangeCommand transactionDynamicTermsChangeCommand = new TransactionDynamicTermsChangeCommand(
				this,
				(ValueCollection) annotationTabRecord.getAnnotationValue());
		Utilities.executeTransaction(transactionDynamicTermsChangeCommand);
	}

}
