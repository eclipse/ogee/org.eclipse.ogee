/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.annotations;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.vocabularies.ICoreVocabulary;
import org.eclipse.ogee.model.odata.Annotation;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.IParameterTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.ValueAnnotation;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.utils.logger.Logger;

public class PathValueResolver {
	public static final String SEPARATOR = "/"; //$NON-NLS-1$

	private Logger logger = Logger.getLogger(Activator.PLUGIN_ID);
	private IAnnotationTarget annotationTarget;
	private EDMTypes edmType;
	private List<PathValueData> list = new ArrayList<PathValueData>();
	private boolean isPropertyFlag;
	private IAnnotationTarget term;

	public PathValueResolver(AnnotationTabRecord record) {
		this(record.getAnnotationTarget(), record.getTerm(), record
				.getTypeUsage());
	}

	/**
	 * 
	 * @param target
	 * @param term
	 * @param typeUsage
	 */
	public PathValueResolver(IAnnotationTarget target, IAnnotationTarget term,
			IPropertyTypeUsage typeUsage) {
		this.annotationTarget = target;
		this.term = term;
		if (typeUsage != null && typeUsage instanceof SimpleTypeUsage) {
			this.edmType = ((SimpleTypeUsage) typeUsage).getSimpleType()
					.getType();
		}

		setIsPropertyFlag();
		getAvailablePathValues();

	}

	public boolean isPropertyPathFlag() {
		return isPropertyFlag;
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<String> getValues() {
		ArrayList<String> returnList = new ArrayList<String>();

		for (PathValueData pathValueData : list) {
			if ((isPropertyFlag && !pathValueData.isStructured())
					|| (!isPropertyFlag && edmType == pathValueData
							.getEdmType())) {
				returnList.add(pathValueData.getPathValueString());
			}
		}

		return returnList;

	}

	/**
     * 
     */
	private void setIsPropertyFlag() {
		// Get the schema for the artifact
		EDMXSet edmxSet = SchemaHandler.getEDMXSet(annotationTarget);

		if (edmxSet == null) {
			isPropertyFlag = false;
			return;
		}

		ValueTerm isPropertyTerm = getPropertyPathTermName(edmxSet);

		// Check if term the user is on, had Value annotation named
		// isPropertyPath
		if (term != null && term.getAnnotations() != null) {
			for (Annotation annotation : term.getAnnotations()) {
				if (((ValueAnnotation) annotation).getTerm() != null
						&& ((ValueAnnotation) annotation).getTerm().equals(
								isPropertyTerm)) {
					isPropertyFlag = true;
					return;
				}
			}
		}

		isPropertyFlag = false;
	}

	/**
	 * 
	 */
	private void getAvailablePathValues() {

		// If we are in one of the leafs display the parent level
		if (annotationTarget instanceof Property
				|| annotationTarget instanceof Parameter
				|| annotationTarget instanceof NavigationProperty) {
			if (annotationTarget.eContainer() != null) {
				if (annotationTarget.eContainer() instanceof EntityType) {
					createEntityTypeValuePathString(
							(EntityType) annotationTarget.eContainer(),
							Messages.EMPTY_String);
				} else if (annotationTarget.eContainer() instanceof ComplexType) {
					createComplexTypeValuePathString(
							(ComplexType) annotationTarget.eContainer(),
							Messages.EMPTY_String);
				} else if (annotationTarget.eContainer() instanceof FunctionImport) {
					createFuntionImportValuePathString(
							(FunctionImport) annotationTarget.eContainer(),
							Messages.EMPTY_String);
				}
			}
		} else if (annotationTarget instanceof ComplexType) {
			createComplexTypeValuePathString((ComplexType) annotationTarget,
					Messages.EMPTY_String);
		} else if (annotationTarget instanceof EntitySet) {
			createEntitySetValuePathString((EntitySet) annotationTarget,
					Messages.EMPTY_String);
		} else if (annotationTarget instanceof EntityType) {
			createEntityTypeValuePathString((EntityType) annotationTarget,
					Messages.EMPTY_String);
		} else if (annotationTarget instanceof FunctionImport) {
			createFuntionImportValuePathString(
					(FunctionImport) annotationTarget, Messages.EMPTY_String);
		}

	}

	/**
	 * 
	 * @param entityType
	 * @param pathString
	 * @param edmType
	 * @return
	 */
	private void createEntityTypeValuePathString(EntityType entityType,
			String pathString) {
		if (entityType.getKeys() != null) {
			for (Property property : entityType.getKeys()) {
				createPropertyValuePathString(property, pathString);
			}
		}

		if (entityType.getProperties() != null) {
			for (Property property : entityType.getProperties()) {
				createPropertyValuePathString(property, pathString);
			}
		}

		for (NavigationProperty navigationProperty : entityType
				.getNavigationProperties()) {
			createNavigationPropertyValuePathString(navigationProperty,
					pathString);
		}
	}

	/**
	 * 
	 * @param entitySet
	 * @param pathString
	 * @return
	 */
	private void createEntitySetValuePathString(EntitySet entitySet,
			String pathString) {
		createEntityTypeValuePathString(entitySet.getType(), pathString);
	}

	/**
	 * get all the ValuePath strings related to property
	 * 
	 * @param property
	 * @param pathString
	 * @return
	 */
	private void createPropertyValuePathString(Property property,
			String pathString) {
		IPropertyTypeUsage propertyTypeUsage = property.getType();

		if (propertyTypeUsage instanceof SimpleTypeUsage) {
			if (((SimpleTypeUsage) propertyTypeUsage).getSimpleType() != null) {
				list.add(new PathValueData(pathString + property.getName(),
						((SimpleTypeUsage) propertyTypeUsage).getSimpleType()
								.getType(), !Messages.EMPTY_String
								.equals(pathString)));
			}
		}

		if (propertyTypeUsage instanceof ComplexTypeUsage) {
			createComplexTypeValuePathString(
					((ComplexTypeUsage) propertyTypeUsage).getComplexType(),
					pathString + property.getName() + SEPARATOR);
		}

		if (propertyTypeUsage instanceof EnumTypeUsage) {
			EDMTypes edmTypes = (((EnumTypeUsage) propertyTypeUsage)
					.getEnumType() == null) ? null
					: ((EnumTypeUsage) propertyTypeUsage).getEnumType()
							.getUnderlyingType();
			list.add(new PathValueData(pathString + property.getName(),
					edmTypes, !Messages.EMPTY_String.equals(pathString)));
		}
	}

	/**
	 * 
	 * @param complexType
	 * @param pathString
	 * @return
	 */
	private void createComplexTypeValuePathString(ComplexType complexType,
			String pathString) {
		if (complexType == null) {
			return;
		}

		if (complexType.getProperties() != null) {
			for (Property property : complexType.getProperties()) {
				createPropertyValuePathString(property, pathString);
			}
		}
	}

	private void createFuntionImportValuePathString(
			FunctionImport functionImport, String pathString) {
		if (functionImport.getParameters() != null) {
			for (Parameter parameter : functionImport.getParameters()) {
				createParameterValuePathString(parameter, pathString);
			}
		}

	}

	/**
	 * 
	 * @param parameter
	 * @param pathString
	 * @return
	 */
	private void createParameterValuePathString(Parameter parameter,
			String pathString) {
		IParameterTypeUsage parameterTypeUsage = parameter.getType();

		if (parameterTypeUsage instanceof SimpleTypeUsage) {
			list.add(new PathValueData(pathString + parameter.getName(),
					((SimpleTypeUsage) parameterTypeUsage).getSimpleType()
							.getType(), !Messages.EMPTY_String
							.equals(pathString)));
		}

		if (parameterTypeUsage instanceof ComplexTypeUsage) {
			createComplexTypeValuePathString(
					((ComplexTypeUsage) parameterTypeUsage).getComplexType(),
					pathString + parameter.getName() + SEPARATOR);
		}

		if (parameterTypeUsage instanceof EnumTypeUsage) {
			EDMTypes edmTypes = (((EnumTypeUsage) parameterTypeUsage)
					.getEnumType() == null) ? null
					: ((EnumTypeUsage) parameterTypeUsage).getEnumType()
							.getUnderlyingType();
			list.add(new PathValueData(pathString + parameter.getName(),
					edmTypes, !Messages.EMPTY_String.equals(pathString)));
		}
	}

	/**
	 * 
	 * @param navigationProperty
	 * @param pathString
	 * @return
	 */
	private void createNavigationPropertyValuePathString(
			NavigationProperty navigationProperty, String pathString) {
		list.add(new PathValueData(pathString + navigationProperty.getName(),
				null, !Messages.EMPTY_String.equals(pathString)));
	}

	private ValueTerm getPropertyPathTermName(EDMXSet edmxSet) {
		try {
			// Get the isPropertyPath value term from core vocabulary
			IVocabulary vocabulary = IModelContext.INSTANCE
					.getVocabularyContext(edmxSet).provideVocabulary(
							ICoreVocabulary.VC_NAMESPACE);
			return vocabulary.getValueTerm(ICoreVocabulary.VT_IS_PROPERTY_PATH);

		} catch (ModelAPIException e) {
			logger.logError(e.getLocalizedMessage());
			return null;
		}

	}

}
