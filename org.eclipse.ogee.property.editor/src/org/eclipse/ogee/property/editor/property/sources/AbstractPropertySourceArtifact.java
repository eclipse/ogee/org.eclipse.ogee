/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.designer.api.IODataDiagramCreator;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IDocumentable;
import org.eclipse.ogee.model.odata.IntegerValue;
import org.eclipse.ogee.model.odata.Multiplicity;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.property.editor.commands.TransactionAssociationMultiplicityChangeCommand;
import org.eclipse.ogee.property.editor.commands.TransactionChangeCommand;
import org.eclipse.ogee.property.editor.commands.TransactionChangeUnderlyingTypeCommand;
import org.eclipse.ogee.property.editor.commands.TransactionSchemaChangeCommand;
import org.eclipse.ogee.property.editor.commands.TransactionSimpleValueChangeCommand;
import org.eclipse.ogee.property.editor.commands.TransactionTypeUsageChangeCommand;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.property.descriptor.PropertyDescriptorFactory;
import org.eclipse.ogee.property.editor.type.handler.AbstractTypeHandler;
import org.eclipse.ogee.property.editor.ui.elements.dialog.IReturnValue;
import org.eclipse.ogee.property.editor.utilities.TypeConverter;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ogee.property.editor.utilities.Validator;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;

/**
 * An abstract class to create and handel {@link IPropertySource} from an
 * {@link EObject} (usually an OData artifact)
 * 
 * 
 */
public abstract class AbstractPropertySourceArtifact extends
		AbstractPropertySource {
	// Array with all the relevant descriptors; should be initalize in the
	// constuctor of the extended class
	protected DescriptorId[] descriptorIds;
	// map that holds the value for each descriptor; should be initalize in
	// initializeDescriptorsValuesMap() mathod.
	protected Map<DescriptorId, Object> descriptorValueMap = new HashMap<DescriptorId, Object>();

	/*
* 
*/
	private boolean isNeedToRefresh = false;
	protected EObject oldValue = null;
	protected EObject newValue = null;

	public AbstractPropertySourceArtifact(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

	/**
	 * 
	 * @return {@link EObject} The OData artifact
	 */
	abstract protected EObject getEObject();

	/**
	 * Initialize map with the descriptors and their values.<br>
	 * The key of the map is {@link DescriptorId} and value is an {@link Object}
	 * .<br>
	 * This method used in order to enable returning values from the extended
	 * classes<br>
	 * The value is returning by
	 * {@link AbstractPropertySourceArtifact#getPropertyValue(Object)} method
	 * according to the key.
	 * 
	 */
	abstract protected void initializeDescriptorsValuesMap();

	/**
	 * This method should be override if the property descriptor type is
	 * {@link PropertyDescriptorType#ComboBoxPropertyDescriptor}.<br>
	 * This method called by
	 * {@link AbstractPropertySourceArtifact#getPropertyDescriptors()}.
	 * 
	 * @param id
	 *            The {@link DescriptorId} in order to determine which values to
	 *            return in case of multiple combo box in the same property
	 *            source.
	 * @return {@link String[]} with the values for the created combo box.
	 */
	protected String[] getComboBoxValues(DescriptorId id) {
		return null;
	}

	/**
	 * This method should be override if the setter of the property descriptor
	 * requiers additional object.
	 * 
	 * @param object
	 *            The value in order to create the additional object. if no
	 *            additional value requries to create the object, provude
	 *            <code>null</code>.
	 * @return {@link Object} the additional object.
	 */
	protected Object getAdditionalValue(DescriptorId descriptorId, Object object) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#getPropertyDescriptors()
	 */
	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		ArrayList<IPropertyDescriptor> propertyDescriptorList = new ArrayList<IPropertyDescriptor>();
		// null check to prevent null pointer exceptions when sometimes
		// associations or any object are not populated
		if (null != descriptorIds) {
			for (DescriptorId descriptorId : descriptorIds) {
				IPropertyDescriptor propertyDescriptor = null;
				if (isReadOnly
						&& descriptorId != DescriptorId.REFERENTIAL_CONSTRAINT
						&& descriptorId != DescriptorId.ASSOCIATIONSET) {
					propertyDescriptor = PropertyDescriptorFactory
							.getInstance().createPropertyDescriptor(
									descriptorId);
				} else {
					switch (descriptorId.getDescriptorType()) {
					case ComboBoxPropertyDescriptor:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance()
								.createComboBoxPropertyDescriptor(descriptorId,
										getComboBoxValues(descriptorId));
						break;
					case BytePropertyDescriptorValidator:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance()
								.createBytePropertyDescriptorValidator(
										descriptorId);
						break;
					case SBytePropertyDescriptorValidator:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance()
								.createSBytePropertyDescriptorValidator(
										descriptorId);
						break;
					case Int16PropertyDescriptorValidator:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance()
								.createInt16PropertyDescriptorValidator(
										descriptorId);
						break;
					case Int32PropertyDescriptorValidator:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance()
								.createIntegerPropertyDescriptorValidator(
										descriptorId);
						break;
					case Int64PropertyDescriptorValidator:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance()
								.createLongPropertyDescriptorValidator(
										descriptorId);
						break;
					case PropertyDescriptor:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance().createPropertyDescriptor(
										descriptorId);
						break;
					case TextPropertyDescriptor:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance().createTextPropertyDescriptor(
										descriptorId);
						break;
					case TextPropertyDescriptorValidator:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance()
								.createTextPropertyDescriptorValidator(
										descriptorId);
						break;
					case ButtonPropertyDescriptor:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance().createButtonPropertyDescriptor(
										descriptorId,
										(Association) getEObject(), isReadOnly);
						break;
					case EDMTypesPropertyDescriptorValidator:
						if ((SimpleType) getEObject() != null) {
							propertyDescriptor = PropertyDescriptorFactory
									.getInstance()
									.createEDMTypesPropertyDescriptor(
											descriptorId,
											((SimpleType) getEObject())
													.getType());
						}
						break;
					case NamePropertyDescriptorValidator:
						propertyDescriptor = PropertyDescriptorFactory
								.getInstance().createNamePropertyDescriptor(
										descriptorId);
						break;
					default:
						break;
					}
				}
				propertyDescriptorList.add(propertyDescriptor);
			}
		}
		return propertyDescriptorList
				.toArray(new IPropertyDescriptor[propertyDescriptorList.size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.properties.IPropertySource#getPropertyValue(java
	 * .lang.Object)
	 */
	@Override
	public Object getPropertyValue(Object id) {

		Object descriptorValue = null;

		if (descriptorValueMap.isEmpty())
			initializeDescriptorsValuesMap();
		DescriptorId descriptor = (DescriptorId) id;
		switch (descriptor) {
		case DOCUMENTATION:
			return new DocumentationODataSource((IDocumentable) getEObject(),
					isReadOnly);
		case ISBINDING:
			return new BindingODataSource((FunctionImport) getEObject(),
					isReadOnly);
		case END1:
		case END2:
			return new RoleODataSource(
					(Role) descriptorValueMap.get(descriptor), isReadOnly);
		case FACETS:
			Object[] object = (Object[]) descriptorValueMap.get(descriptor);
			if (object != null)
				return new FacetsSimpleTypeODataSource(object[0],
						(SimpleTypeUsage) object[1], isReadOnly);
		case ASSOCIATION_ENTITYSET:
			break;
		case ISCOLLECTION:
			if (descriptorValueMap.get(descriptor).getClass() == String.class)
				return descriptorValueMap.get(descriptor);
		case ISABSTRACT:
		case ISDEFAULT:
		case SIDEEFFECT:
		case ISMEDIA:
		case ISNULLABLECOMBO:
		case IS_USE_FOR_ETAG:
		case ISKEY:
		case ISFLAGS:
		case CONTAINS_TARGET:
			if (!isReadOnly) { // if read only cont. to deault
				descriptorValue = this.descriptorValueMap.get(descriptor);
				if (descriptorValue != null)
					return (Boolean) descriptorValue ? 1 : 0;
			}
			/*
			 * all the following are under the default case ISKEY ,
			 * RELATIONSHIP,FROM_ROLE,TO_ROLE, ENTIRY_SET,
			 * RETURN_TYPE,ASSOCIATION, DEFAULT, ENTITY_TYPE, ISCOLLECTION,
			 * FIXED_LENGTH, LONG_DESCRIPTION NAME, NAMESPACE, MULTIPLICITY,
			 * REFERENTIAL_CONSTRAINT, ROLE, TYPE, TYPECOMBO, SUMMARY,
			 * MAX_LENGTH ,PRECISION, SCALE, CONTAINS_TARGET, BASE, VALUE
			 */
		default:
			return descriptorValueMap.get(descriptor);

		}
		return null;
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if (isReadOnly)
			return;
		TransactionChangeCommand command = new TransactionChangeCommand(id,
				value, getEObject());
		DescriptorId descriptor = (DescriptorId) id;
		switch (descriptor) {
		case ALIAS:
			setupCommand(command, ((String) value), "setAlias", String.class); //$NON-NLS-1$
			break;
		case BASE:
			// Base type is only for EntityType and ComplexType
			Class<?> clazz = EntityType.class;
			if (getEObject() instanceof ComplexType) {
				clazz = ComplexType.class;
			}
			setupCommandWithAdditionalValue(descriptor, command, value,
					"setBaseType", clazz); //$NON-NLS-1$
			isNeedToRefresh = true;
			break;
		case CONTAINS_TARGET:
			setupCommandWithBooleanValue(command, value, "setContainsTarget"); //$NON-NLS-1$
			break;
		case DEFAULT:
			command = new TransactionSimpleValueChangeCommand(id, value,
					getEObject(), (EDMTypes) getAdditionalValue(descriptor,
							value));
			setupCommand(command, value, "setDefaultValue", SimpleValue.class); //$NON-NLS-1$
			break;
		case ASSOCIATION_ENTITYSET:
			setupCommandWithAdditionalValue(descriptor, command, value,
					"setEntitySet", EntitySet.class); //$NON-NLS-1$
			break;
		case UNDERLYING_TYPE:
			command = new TransactionChangeUnderlyingTypeCommand(id,
					getAdditionalValue(descriptor, value), getEObject());
			break;
		case FIXED_LENGTH:
			setupCommandWithIntegerValue(command, value, "setFixedLength"); //$NON-NLS-1$
			break;
		case VALUE_BYTE:
		case VALUE_SBYTE:
		case VALUE_INT16:
		case VALUE_INT32:
		case VALUE_INT64:
			setupCommandWithAdditionalValue(descriptor, command, value,
					"setValue", IntegerValue.class); //$NON-NLS-1$
			break;
		case ISABSTRACT:
			setupCommandWithBooleanValue(command, value, "setAbstract"); //$NON-NLS-1$
			break;
		case ISDEFAULT:
			setupCommandWithBooleanValue(command, value, "setDefault"); //$NON-NLS-1$
			break;
		case ISFLAGS:
			setupCommandWithBooleanValue(command, value, "setFlags"); //$NON-NLS-1$
			break;
		case ISMEDIA:
			setupCommandWithBooleanValue(command, value, "setMedia"); //$NON-NLS-1$
			break;
		case ISNULLABLECOMBO:
			setupCommandWithBooleanValue(command, value, "setNullable"); //$NON-NLS-1$
			break;
		case IS_USE_FOR_ETAG:
			setupCommandWithBooleanValue(command, value, "setForEtag"); //$NON-NLS-1$
			break;
		case LONG_DESCRIPTION:
			setupCommand(command, (String) value,
					"setLongDescription", String.class); //$NON-NLS-1$
			break;
		case MAX_LENGTH:
			setupCommandWithIntegerValue(command, value, "setMaxLength"); //$NON-NLS-1$
			break;
		case MULTIPLICITY:
			command = new TransactionAssociationMultiplicityChangeCommand(
					descriptor, Multiplicity.get((Integer) value), getEObject());
			break;
		case NAME:
		case ROLE:
			if (Validator.validateName(value.toString()) != null)
				return;
			setupCommand(command, value.toString(), "setName", String.class); //$NON-NLS-1$
			break;
		case NAMESPACE:
			setupCommand(command, value.toString(),
					"setNamespace", String.class); //$NON-NLS-1$
			break;
		case PRECISION:
			setupCommandWithIntegerValue(command, value, "setPrecision"); //$NON-NLS-1$
			break;
		case REFERENTIAL_CONSTRAINT:
			if (((IReturnValue) value).getReturnStatus() != org.eclipse.jface.window.Window.CANCEL) {
				setupCommandWithAdditionalValue(descriptor, command, value,
						"setReferentialConstraint", ReferentialConstraint.class); //$NON-NLS-1$
				break;
			} else {
				return;
			}
		case SCALE:
			setupCommandWithIntegerValue(command, value, "setScale"); //$NON-NLS-1$
			break;
		case SCHEMA:
			command = new TransactionSchemaChangeCommand(id, value,
					getEObject());
			((TransactionSchemaChangeCommand) command)
					.setFrom((Schema) getAdditionalValue(descriptor, null));
			((TransactionSchemaChangeCommand) command)
					.setTo((Schema) getAdditionalValue(descriptor, value));
			break;
		case SIDEEFFECT:
			setupCommandWithBooleanValue(command, value, "setSideEffecting"); //$NON-NLS-1$
			break;
		case SUMMARY:
			setupCommand(command, value, "setSummary", String.class); //$NON-NLS-1$
			// command.setValue((String) value);
			break;
		case TYPECOMBO:
			command = new TransactionTypeUsageChangeCommand(descriptor, value,
					getEObject(), (AbstractTypeHandler) getAdditionalValue(
							descriptor, value));
			break;
		/*
		 * all the following are under the default case ALIAS, ASSOCIATION,
		 * DOCUMENTATION, END1, END2, TO_ROLE, ENTITY_TYPE, FACETS, ISBINDING,
		 * FROM_ROLEFACETS2, ENTIRY_SET, RETURN_TYPE , ISCOLLECTION,
		 * RELATIONSHIP ,ISKEY, TYPE
		 */
		default:
			return;
		}
		Utilities.executeTransaction(command);
		if (isNeedToRefresh) {
			IODataDiagramCreator.INSTANCE.refreshEditor(getEObject(), oldValue,
					newValue);
			isNeedToRefresh = false;
		}
	}

	private void setupCommand(TransactionChangeCommand command, Object value,
			String methodName, Class<?> parameterDecleredClass) {
		command.setValue(value);
		command.setMethodName(methodName);
		command.setParameterDecleredClass(parameterDecleredClass);
	}

	private void setupCommandWithAdditionalValue(DescriptorId descriptorId,
			TransactionChangeCommand command, Object value, String methodName,
			Class<?> parameterDecleredClass) {
		setupCommand(command, getAdditionalValue(descriptorId, value),
				methodName, parameterDecleredClass);
	}

	private void setupCommandWithBooleanValue(TransactionChangeCommand command,
			Object value, String methodName) {
		setupCommand(command, ((Integer) value).equals(1), methodName,
				Boolean.TYPE);
	}

	private void setupCommandWithIntegerValue(TransactionChangeCommand command,
			Object value, String methodName) {
		int iValue;
		if (TypeConverter.getIntegerValue(value) == null)
			iValue = 0;
		else
			iValue = TypeConverter.getIntegerValue(value).intValue();

		setupCommand(command, iValue, methodName, Integer.TYPE);
	}

}
