/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.exploration.tree.api.IServiceDetails;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;

public class ServiceOdataExplorationTreeSource extends
		AbstractPropertySourceArtifact {
	private IServiceDetails iServiceDetails;

	public ServiceOdataExplorationTreeSource(IServiceDetails iServiceDetails) {
		super(true);
		this.iServiceDetails = iServiceDetails;

		descriptorIds = new DescriptorId[] { DescriptorId.DESCRIPTION,
				DescriptorId.TITLE, DescriptorId.AUTHOR,
				DescriptorId.TECHNICAL_SERVICE_VERSION, DescriptorId.ID,
				DescriptorId.METADATAURL, DescriptorId.TECHNICAL_SERVICE_NAME,
				DescriptorId.SERVICEURL, DescriptorId.UPDATEDDATE, };
		initializeDescriptorsValuesMap();
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.DESCRIPTION,
				iServiceDetails.getDescription());
		descriptorValueMap.put(DescriptorId.TITLE, iServiceDetails.getTitle());
		descriptorValueMap
				.put(DescriptorId.AUTHOR, iServiceDetails.getAuthor());
		descriptorValueMap.put(DescriptorId.TECHNICAL_SERVICE_VERSION,
				iServiceDetails.getTechnicalServiceVersion());
		descriptorValueMap.put(DescriptorId.ID, iServiceDetails.getID());
		descriptorValueMap.put(DescriptorId.METADATAURL,
				iServiceDetails.getMetadataUrl());
		descriptorValueMap.put(DescriptorId.TECHNICAL_SERVICE_NAME,
				iServiceDetails.getTechnicalServiceName());
		descriptorValueMap.put(DescriptorId.SERVICEURL,
				iServiceDetails.getServiceUrl());
		descriptorValueMap.put(DescriptorId.UPDATEDDATE,
				iServiceDetails.getUpdatedDate());
	}

	@Override
	protected EObject getEObject() {
		/*
		 * this PropertySource is read only.
		 */
		return null;
	}

}
