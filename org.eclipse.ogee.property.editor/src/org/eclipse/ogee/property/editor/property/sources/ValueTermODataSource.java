/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;

public class ValueTermODataSource extends AbstractPropertySourceArtifact {
	protected ValueTerm valueTerm;

	public ValueTermODataSource(ValueTerm valueTerm, boolean isReadOnly) {
		super(isReadOnly);
		this.valueTerm = valueTerm;

		descriptorIds = new DescriptorId[] { DescriptorId.NAME,
				DescriptorId.TYPE };
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, valueTerm.getName());

		String type = valueTerm.getType() != null ? getValueTermTypeName(valueTerm
				.getType()) : Messages.EMPTY_String;
		descriptorValueMap.put(DescriptorId.TYPE, type);
	}

	@Override
	protected EObject getEObject() {
		return valueTerm;
	}

	private String getValueTermTypeName(IPropertyTypeUsage iPropertyTypeUsage) {
		if (iPropertyTypeUsage instanceof SimpleTypeUsage) {
			return ((SimpleTypeUsage) iPropertyTypeUsage).getSimpleType() != null ? ((SimpleTypeUsage) iPropertyTypeUsage)
					.getSimpleType().getType().getName()
					: Messages.EMPTY_String;
		} else if (iPropertyTypeUsage instanceof ComplexTypeUsage) {
			return ((ComplexTypeUsage) iPropertyTypeUsage).getComplexType()
					.getName();
		} else if (iPropertyTypeUsage instanceof EnumTypeUsage) {
			return ((EnumTypeUsage) iPropertyTypeUsage).getEnumType().getName();
		}
		return Messages.EMPTY_String;

	}
}
