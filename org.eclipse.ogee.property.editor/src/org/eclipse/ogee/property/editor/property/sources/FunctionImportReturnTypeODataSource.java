/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.IParameterTypeUsage;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.property.editor.commands.TransactionChangeCommandAbstract;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.type.handler.AbstractTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.ReturnTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class FunctionImportReturnTypeODataSource extends
		AbstractPropertySourceArtifact {

	protected IFunctionReturnTypeUsage functionReturnTypeUsage;
	protected ReturnTypeHandler typeHandler;

	// in case of Entity Type
	private Hashtable<String, EntitySet> entitySetMap = new Hashtable<String, EntitySet>();
	private List<String> keysList = new ArrayList<String>();
	private boolean isWithEntrySet;

	public FunctionImportReturnTypeODataSource(
			IFunctionReturnTypeUsage functionReturnTypeUsage, boolean isReadOnly) {
		super(isReadOnly);
		this.functionReturnTypeUsage = functionReturnTypeUsage;
		typeHandler = new ReturnTypeHandler(functionReturnTypeUsage);

		if (functionReturnTypeUsage instanceof ReturnEntityTypeUsage) {
			descriptorIds = new DescriptorId[] { DescriptorId.RETURN_TYPE,
					DescriptorId.ISCOLLECTION, DescriptorId.ENTIRY_SET };
			isWithEntrySet = true;
		} else {
			descriptorIds = new DescriptorId[] { DescriptorId.RETURN_TYPE,
					DescriptorId.ISCOLLECTION };
			isWithEntrySet = false;
		}
	}

	/**
	 * creates the map and the list of entity sets for the Entity set
	 * PropertyDescriptor
	 * 
	 * @return
	 */
	private String[] createEntitySetKeys() {
		if (((ReturnEntityTypeUsage) functionReturnTypeUsage).getEntityType() == null) {
			return new String[0];
		}
		EList<EntitySet> eList = ((ReturnEntityTypeUsage) functionReturnTypeUsage)
				.getEntityType().getEntitySets();
		if (!eList.isEmpty()) {
			for (EntitySet entitySet : eList) {
				entitySetMap.put(entitySet.getName(), entitySet);
			}
		}
		keysList.addAll(entitySetMap.keySet());
		return keysList.toArray(new String[keysList.size()]);
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		Object value = typeHandler.getTypeIndex(functionReturnTypeUsage);
		if (isReadOnly) {
			value = Messages.EMPTY_String;
			if (functionReturnTypeUsage instanceof ReturnEntityTypeUsage) {
				EntityType entityType = ((ReturnEntityTypeUsage) functionReturnTypeUsage)
						.getEntityType();
				if (entityType != null) {
					value = SchemaHandler.getNamespaceOrAlias(entityType)
							+ AbstractTypeHandler.NAMESPACE_SEPARATOR
							+ entityType.getName();
				}
			} else if (functionReturnTypeUsage instanceof SimpleTypeUsage) {
				value = ((SimpleTypeUsage) functionReturnTypeUsage)
						.getSimpleType().getType().getName();
			} else if (functionReturnTypeUsage instanceof ComplexTypeUsage) {
				ComplexType complexType = ((ComplexTypeUsage) functionReturnTypeUsage)
						.getComplexType();
				if (complexType != null) {
					value = SchemaHandler.getNamespaceOrAlias(complexType)
							+ AbstractTypeHandler.NAMESPACE_SEPARATOR
							+ complexType.getName();
				}
			} else if (functionReturnTypeUsage instanceof EnumTypeUsage) {
				EnumType enumType = ((EnumTypeUsage) functionReturnTypeUsage)
						.getEnumType();
				if (enumType != null) {
					value = SchemaHandler.getNamespaceOrAlias(enumType)
							+ AbstractTypeHandler.NAMESPACE_SEPARATOR
							+ enumType.getName();
				}
			}

		}
		descriptorValueMap.put(DescriptorId.RETURN_TYPE, value);

		if (functionReturnTypeUsage instanceof ReturnEntityTypeUsage) {
			value = ((ReturnEntityTypeUsage) functionReturnTypeUsage)
					.isCollection();
		} else {
			value = ((IParameterTypeUsage) functionReturnTypeUsage)
					.isCollection();

		}
		descriptorValueMap.put(DescriptorId.ISCOLLECTION, value);
		if (isWithEntrySet) {
			if (((ReturnEntityTypeUsage) functionReturnTypeUsage)
					.getEntitySet() != null) {
				if (isReadOnly)
					value = ((ReturnEntityTypeUsage) functionReturnTypeUsage)
							.getEntitySet().getName();
				else
					value = keysList
							.indexOf(((ReturnEntityTypeUsage) functionReturnTypeUsage)
									.getEntitySet().getName());
			} else {
				if (isReadOnly)
					value = Messages.EMPTY_String;
				else
					value = -1;
			}
			descriptorValueMap.put(DescriptorId.ENTIRY_SET, value);
		}
	}

	protected String[] getComboBoxValues(DescriptorId id) {
		if (id == DescriptorId.RETURN_TYPE)
			return typeHandler.getTypesArray();
		if (id == DescriptorId.ISCOLLECTION)
			return Utilities.boolValues;
		if (id == DescriptorId.ENTIRY_SET)
			return createEntitySetKeys();
		return null;
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if (isReadOnly)
			return;
		TransactionChangeCommandAbstract transactionChangeCommand = new TransactionChangeCommandAbstract(
				id, value, functionReturnTypeUsage) {

			@Override
			public void doExecute() {
				DescriptorId descriptor = (DescriptorId) id;
				switch (descriptor) {
				case RETURN_TYPE:
					typeHandler.updateType((Integer) value,
							functionReturnTypeUsage);
					break;
				case ISCOLLECTION:
					if (functionReturnTypeUsage instanceof ReturnEntityTypeUsage) {
						((ReturnEntityTypeUsage) functionReturnTypeUsage)
								.setCollection(((Integer) value).equals(1));
					}

					else {
						((IParameterTypeUsage) functionReturnTypeUsage)
								.setCollection(((Integer) value).equals(1));
					}

					break;
				case ENTIRY_SET:
					((ReturnEntityTypeUsage) functionReturnTypeUsage)
							.setEntitySet(entitySetMap.get(keysList
									.get((Integer) value)));
					break;
				default:
					break;
				}
			}
		};

		Utilities.executeTransaction(transactionChangeCommand);
	}

	@Override
	protected EObject getEObject() {
		return functionReturnTypeUsage;
	}

}
