/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.descriptor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.property.editor.annotations.AnnotationTabRecord;
import org.eclipse.ogee.property.editor.ui.elements.descriptors.ButtonPropertyDescriptor;
import org.eclipse.ogee.property.editor.ui.elements.descriptors.NamePropertyDescriptorValidator;
import org.eclipse.ogee.property.editor.ui.elements.descriptors.TextComboBoxPropertyDescriptor;
import org.eclipse.ogee.property.editor.ui.elements.descriptors.TextPropertyDescriptorValidator;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ui.views.properties.ComboBoxPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;

public class PropertyDescriptorFactory {

	public IPropertyDescriptor createPropertyDescriptor(Object id,
			String displayName) {
		PropertyDescriptor descriptor = new PropertyDescriptor(id, displayName);
		return descriptor;
	}

	public IPropertyDescriptor createTextPropertyDescriptor(Object id,
			String displayName) {
		PropertyDescriptor descriptor = new TextPropertyDescriptor(id,
				displayName);
		return descriptor;
	}

	public IPropertyDescriptor createComboBoxPropertyDescriptor(Object id,
			String displayName, String[] values) {
		PropertyDescriptor descriptor = new ComboBoxPropertyDescriptor(id,
				displayName, values);
		return descriptor;
	}

	public IPropertyDescriptor createSBytePropertyDescriptorValidator(
			DescriptorId descriptorId) {
		return new TextPropertyDescriptorValidator(descriptorId,
				descriptorId.getDisplayName(), EDMTypes.SBYTE);
	}

	public IPropertyDescriptor createDecimalPropertyDescriptorValidator(
			DescriptorId descriptorId) {
		return new TextPropertyDescriptorValidator(descriptorId,
				descriptorId.getDisplayName(), EDMTypes.DECIMAL);
	}

	public IPropertyDescriptor createBytePropertyDescriptorValidator(
			DescriptorId descriptorId) {
		return new TextPropertyDescriptorValidator(descriptorId,
				descriptorId.getDisplayName(), EDMTypes.BYTE);
	}

	public IPropertyDescriptor createInt16PropertyDescriptorValidator(
			DescriptorId descriptorId) {
		return new TextPropertyDescriptorValidator(descriptorId,
				descriptorId.getDisplayName(), EDMTypes.INT16);
	}

	public IPropertyDescriptor createIntegerPropertyDescriptorValidator(
			Object id, String displayName) {
		return new TextPropertyDescriptorValidator(id, displayName,
				EDMTypes.INT32);
	}

	public IPropertyDescriptor createLongPropertyDescriptorValidator(Object id,
			String displayName) {
		return new TextPropertyDescriptorValidator(id, displayName,
				EDMTypes.INT64);
	}

	public IPropertyDescriptor createTextPropertyDescriptorValidator(Object id,
			String displayName) {
		return new TextPropertyDescriptorValidator(id, displayName,
				EDMTypes.STRING);
	}

	public IPropertyDescriptor createFloatPropertyDescriptorValidator(
			Object id, String displayName) {
		return new TextPropertyDescriptorValidator(id, displayName,
				EDMTypes.DECIMAL);
	}

	public IPropertyDescriptor createDoublePropertyDescriptorValidator(
			Object id, String displayName) {
		return new TextPropertyDescriptorValidator(id, displayName,
				EDMTypes.DOUBLE);
	}

	public IPropertyDescriptor createStringTextComboBoxPropertyDescriptor(
			Object id, String displayName, List<String> values) {
		return new TextComboBoxPropertyDescriptor(id, displayName, values,
				EDMTypes.STRING);
	}

	public IPropertyDescriptor createIntegerTextComboBoxPropertyDescriptor(
			Object id, String displayName, List<String> values) {
		return new TextComboBoxPropertyDescriptor(id, displayName, values,
				EDMTypes.INT32);
	}

	public IPropertyDescriptor createLongTextComboBoxPropertyDescriptor(
			Object id, String displayName, List<String> values) {
		return new TextComboBoxPropertyDescriptor(id, displayName, values,
				EDMTypes.INT64);
	}

	public IPropertyDescriptor createDecimalTextComboBoxPropertyDescriptor(
			Object id, String displayName, ArrayList<String> values) {
		return new TextComboBoxPropertyDescriptor(id, displayName, values,
				EDMTypes.DECIMAL);
	}

	public IPropertyDescriptor createFloatTextComboBoxPropertyDescriptor(
			Object id, String displayName, List<String> values) {
		return new TextComboBoxPropertyDescriptor(id, displayName, values,
				EDMTypes.SINGLE);
	}

	public IPropertyDescriptor createDoubleTextComboBoxPropertyDescriptor(
			Object id, String displayName, List<String> values) {
		return new TextComboBoxPropertyDescriptor(id, displayName, values,
				EDMTypes.DOUBLE);
	}

	public IPropertyDescriptor createByteTextComboBoxPropertyDescriptor(
			Object id, String displayName, List<String> values) {
		return new TextComboBoxPropertyDescriptor(id, displayName, values,
				EDMTypes.BYTE);
	}

	public IPropertyDescriptor GUIDTextComboBoxPropertyDescriptor(Object id,
			String displayName, List<String> values) {
		return new TextComboBoxPropertyDescriptor(id, displayName, values,
				EDMTypes.GUID);
	}

	public IPropertyDescriptor createShortTextComboBoxPropertyDescriptor(
			Object id, String displayName, List<String> values) {
		return new TextComboBoxPropertyDescriptor(id, displayName, values,
				EDMTypes.INT16);
	}

	public IPropertyDescriptor createButtonPropertyDescriptor(Object id,
			String displayName, AnnotationTabRecord record, boolean isReadOnly) {
		return createButtonPropertyDescriptor(id, displayName,
				new DialogContext(record, DialogTypesEnum.DynamicTerms,
						isReadOnly));
	}

	private IPropertyDescriptor createButtonPropertyDescriptor(Object id,
			String displayName, DialogContext context) {
		return new ButtonPropertyDescriptor(id, displayName, context);
	}

	public IPropertyDescriptor createIntegerPropertyDescriptorValidator(
			DescriptorId descriptorId) {
		return createIntegerPropertyDescriptorValidator(descriptorId,
				descriptorId.getDisplayName());
	}

	public IPropertyDescriptor createLongPropertyDescriptorValidator(
			DescriptorId descriptorId) {
		return createLongPropertyDescriptorValidator(descriptorId,
				descriptorId.getDisplayName());
	}

	public IPropertyDescriptor createPropertyDescriptor(
			DescriptorId descriptorId) {
		return createPropertyDescriptor(descriptorId,
				descriptorId.getDisplayName());
	}

	public IPropertyDescriptor createTextPropertyDescriptor(
			DescriptorId descriptorId) {
		return createTextPropertyDescriptor(descriptorId,
				descriptorId.getDisplayName());
	}

	public IPropertyDescriptor createComboBoxPropertyDescriptor(
			DescriptorId descriptorId, String[] values) {
		return createComboBoxPropertyDescriptor(descriptorId,
				descriptorId.getDisplayName(), values);
	}

	public IPropertyDescriptor createButtonPropertyDescriptor(
			DescriptorId descriptorId, Association association,
			boolean isReadOnly) {
		if (descriptorId == DescriptorId.REFERENTIAL_CONSTRAINT) {
			return createButtonPropertyDescriptor(descriptorId,
					descriptorId.getDisplayName(), new DialogContext(
							association, DialogTypesEnum.ReferentialConstraint,
							isReadOnly));
		} else if (descriptorId == DescriptorId.ASSOCIATIONSET) {
			return createButtonPropertyDescriptor(descriptorId,
					descriptorId.getDisplayName(), new DialogContext(
							association, DialogTypesEnum.AssociationsSet,
							isReadOnly));
		}
		return null;
	}

	public IPropertyDescriptor createTextPropertyDescriptorValidator(
			DescriptorId descriptorId) {
		return createTextPropertyDescriptorValidator(descriptorId,
				descriptorId.getDisplayName());
	}

	public IPropertyDescriptor createDateTimeTextComboBoxPropertyDescriptorValidator(
			Object objectId, String name, ArrayList<String> pathList) {
		TextComboBoxPropertyDescriptor textComboBoxPropertyDescriptor = new TextComboBoxPropertyDescriptor(
				objectId, name, pathList, EDMTypes.DATE_TIME);

		return textComboBoxPropertyDescriptor;
	}

	public IPropertyDescriptor createTimeTextComboBoxPropertyDescriptorValidator(
			Object objectId, String name, ArrayList<String> pathList) {
		return new TextComboBoxPropertyDescriptor(objectId, name, pathList,
				EDMTypes.TIME);
	}

	public IPropertyDescriptor createDateTimeOffsetTextComboBoxPropertyDescriptorValidator(
			Object objectId, String name, ArrayList<String> pathList) {
		return new TextComboBoxPropertyDescriptor(objectId, name, pathList,
				EDMTypes.DATE_TIME_OFFSET);
	}

	public IPropertyDescriptor createEDMTypesPropertyDescriptor(
			DescriptorId descriptorId, EDMTypes type) {
		switch (type) {
		case BOOLEAN:
			return createComboBoxPropertyDescriptor(descriptorId,
					Utilities.boolValues);
		case BYTE:
			return createBytePropertyDescriptorValidator(descriptorId);
		case TIME:
			return new TextPropertyDescriptorValidator(descriptorId,
					descriptorId.getDisplayName(), EDMTypes.TIME);
		case DATE_TIME:
			return new TextPropertyDescriptorValidator(descriptorId,
					descriptorId.getDisplayName(), EDMTypes.DATE_TIME);
		case DATE_TIME_OFFSET:
			return new TextPropertyDescriptorValidator(descriptorId,
					descriptorId.getDisplayName(), EDMTypes.DATE_TIME_OFFSET);
		case DECIMAL:
			return createDecimalPropertyDescriptorValidator(descriptorId);
		case DOUBLE:
			return createDoublePropertyDescriptorValidator(descriptorId,
					descriptorId.getDisplayName());
		case GUID:
			return createGUIDPropertyDescriptorValidator(descriptorId);
		case INT16:
			return createInt16PropertyDescriptorValidator(descriptorId);
		case INT32:
			return createIntegerPropertyDescriptorValidator(descriptorId);
		case INT64:
			return createLongPropertyDescriptorValidator(descriptorId);
		case SBYTE:
			return createSBytePropertyDescriptorValidator(descriptorId);
		case SINGLE:
			return createFloatPropertyDescriptorValidator(descriptorId,
					descriptorId.getDisplayName());
		case STRING:
			return createTextPropertyDescriptor(descriptorId);
		default:
			return createPropertyDescriptor(descriptorId);
		}
	}

	public IPropertyDescriptor createNamePropertyDescriptor(
			DescriptorId descriptorId) {
		return new NamePropertyDescriptorValidator(descriptorId,
				descriptorId.getDisplayName());
	}

	private IPropertyDescriptor createGUIDPropertyDescriptorValidator(
			DescriptorId descriptorId) {
		return new TextPropertyDescriptorValidator(descriptorId,
				descriptorId.getDisplayName(), EDMTypes.GUID);
	}

	private static PropertyDescriptorFactory instance;

	private PropertyDescriptorFactory() {

	}

	public static PropertyDescriptorFactory getInstance() {
		if (instance == null) {
			instance = new PropertyDescriptorFactory();
		}
		return instance;
	}

}
