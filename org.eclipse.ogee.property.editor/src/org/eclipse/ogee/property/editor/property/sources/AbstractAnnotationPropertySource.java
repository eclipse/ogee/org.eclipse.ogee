/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.ogee.property.editor.annotations.AnnotationTabRecord;
import org.eclipse.ogee.property.editor.commands.TransactionAnnotationValueCommand;
import org.eclipse.ogee.property.editor.ui.elements.dialog.IReturnValue;
import org.eclipse.ogee.property.editor.utilities.TypeConverter;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ogee.property.editor.utilities.Validator;

public abstract class AbstractAnnotationPropertySource extends
		AbstractPropertySource {
	protected Map<Object, AnnotationTabRecord> annotationHelperMap = new HashMap<Object, AnnotationTabRecord>();

	@Override
	public Object getPropertyValue(Object id) {
		return annotationHelperMap.get(id).getCurrentValue();
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if (isReadOnly)
			return;

		AnnotationTabRecord record = annotationHelperMap.get(id);
		ArrayList<String> comboBoxOptionsList = record.getComboBoxOptionsList();
		boolean isPathValue = false;
		Object newValue = value;
		String message = null;

		if (value instanceof String
				&& comboBoxOptionsList.contains(((String) value).trim())) {
			isPathValue = true;
			newValue = ((String) value).trim();
		}

		else {
			switch (record.getPropertyDesc()) {
			case ComboBoxPropertyDescriptor: // Boolean
				newValue = record.getComboBoxOptionsList().get((Integer) value);

				// The list of options in the Boolean case contains the values
				// false and true in the first two places
				if ((Integer) value > -1 && (Integer) value < 2) {
					newValue = newValue.equals(Boolean.TRUE.toString());
				} else {
					isPathValue = true;
				}

				break;
			case IntegerTextComboBoxPropertyDescriptor:
				if (Validator.validateInteger((String) value) == null) {
					newValue = TypeConverter.getIntegerValue(value);
				}
				break;
			case TextPropertyDescriptor:
			case TextPropertyDescriptorValidator:
			case StringTextComboBoxPropertyDescriptor:
				break;
			case ByteTextComboBoxPropertyDescriptor:
				if (Validator.validateByte((String) value) == null) {
					newValue = TypeConverter.getByteValue(value);
				}
				break;
			case DoubleTextComboBoxPropertyDescriptor:
				if (Validator.validateDecimalNumber((String) value) == null) {
					newValue = TypeConverter.getDoubleValue(value);
				}
				break;
			case FloatTextComboBoxPropertyDescriptor:
				if (Validator.validateDecimalNumber((String) value) == null) {
					newValue = TypeConverter.getFloatValue(value);
				}
				break;
			case LongTextComboBoxPropertyDescriptor:
				if (Validator.validateLong((String) value) == null) {
					newValue = TypeConverter.getLongValue(value);
				}
				break;
			case ShortTextComboBoxPropertyDescriptor:
				if (Validator.validateShort((String) value) == null) {
					newValue = TypeConverter.getShortValue(value);
				}
				break;
			case DecimalTextComboBoxPropertyDescriptor:
				if (Validator.validateDecimalNumber((String) value) == null) {
					newValue = TypeConverter.getBigDecimalValue(value);
				}
				break;
			case ButtonPropertyDescriptor:
				record.setCurrentValue(((IReturnValue) value).getText());
				return;
			case TimeTextComboBoxPropertyDescriptorValidator:
				message = Validator.validateTime((String) value);
				if (message == null) {
					newValue = TypeConverter.getTimeValue(value);
				} else {
					return;
				}
				break;
			case DateTimeTextComboBoxPropertyDescriptorValidator:
				message = Validator.validateDateTime((String) value);
				if (message == null) {
					newValue = TypeConverter.getDateTimeValue(value);
				} else {
					return;
				}
				break;
			case DateTimeOffsetTextComboBoxPropertyDescriptorValidator:
				message = Validator.validateDateTimeOffset((String) value);
				if (message == null) {
					newValue = TypeConverter.getDateTimeOffsetValue(value);
				} else {
					return;
				}
				break;
			case GUIDTextComboBoxPropertyDescriptor:
				if (Validator.validateGUID((String) value) == null) {
					newValue = TypeConverter.getGUIDValue(value);
				}
				break;
			default:
				return;

			}
		}

		TransactionAnnotationValueCommand command = new TransactionAnnotationValueCommand(
				record, newValue, record.getAnnotationTarget(), isPathValue);
		Utilities.executeTransaction(command);

		record.setCurrentValue(value);

	}

}
