/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import java.util.Set;

import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.property.editor.annotations.AnnotationHelper;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

public class AnnotationODataSource extends AbstractAnnotationPropertySource {

	private AnnotationValue annotaionValue;
	private IAnnotationTarget annotationTarget;

	public AnnotationODataSource(AnnotationValue annotationValue,
			IAnnotationTarget annotationTarget, boolean isReadOnly) {
		this.annotaionValue = annotationValue;
		this.isReadOnly = isReadOnly;
		this.annotationTarget = annotationTarget;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		Set<IPropertyDescriptor> propertyDescriptors = AnnotationHelper
				.getInstance().createAnnotationPropertyDescriptors(
						annotaionValue, annotationTarget, annotationHelperMap,
						isReadOnly);

		return propertyDescriptors
				.toArray(new IPropertyDescriptor[propertyDescriptors.size()]);

	}

	@Override
	public String toString() {
		return Messages.EMPTY_String;
	}

}
