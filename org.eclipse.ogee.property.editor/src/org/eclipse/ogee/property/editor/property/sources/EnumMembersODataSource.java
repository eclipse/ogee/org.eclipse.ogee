/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.ByteValue;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.Int16Value;
import org.eclipse.ogee.model.odata.Int32Value;
import org.eclipse.ogee.model.odata.Int64Value;
import org.eclipse.ogee.model.odata.IntegerValue;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.SByteValue;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.utilities.TypeConverter;

public class EnumMembersODataSource extends AbstractPropertySourceArtifact {
	private EnumMember enumMember;

	public EnumMembersODataSource(EnumMember enumMember, boolean isReadOnly) {
		super(isReadOnly);
		this.enumMember = enumMember;

		List<DescriptorId> list = new ArrayList<DescriptorId>();
		list.add(DescriptorId.NAME);

		switch (((EnumType) enumMember.eContainer()).getUnderlyingType()) {
		case BYTE:
			list.add(DescriptorId.VALUE_BYTE);
			break;
		case INT16:
			list.add(DescriptorId.VALUE_INT16);
			break;
		case INT32:
			list.add(DescriptorId.VALUE_INT32);
			break;
		case INT64:
			list.add(DescriptorId.VALUE_INT64);
			break;
		case SBYTE:
			list.add(DescriptorId.VALUE_SBYTE);
			break;
		default:
			break;
		}
		descriptorIds = list.toArray(new DescriptorId[list.size()]);
	}

	@Override
	protected EObject getEObject() {
		return enumMember;
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, enumMember.getName());
		Object value = enumMember.getValue() != null
				&& enumMember.getValue().getValueObject() != null ? enumMember
				.getValue().getValueObject().toString() : ""; //$NON-NLS-1$

		switch (((EnumType) enumMember.eContainer()).getUnderlyingType()) {
		case BYTE:
			descriptorValueMap.put(DescriptorId.VALUE_BYTE, value);
			break;
		case INT16:
			descriptorValueMap.put(DescriptorId.VALUE_INT16, value);
			break;
		case INT32:
			descriptorValueMap.put(DescriptorId.VALUE_INT32, value);
			break;
		case INT64:
			descriptorValueMap.put(DescriptorId.VALUE_INT64, value);
			break;
		case SBYTE:
			descriptorValueMap.put(DescriptorId.VALUE_SBYTE, value);
			break;
		default:
			break;
		}

	}

	@Override
	protected Object getAdditionalValue(DescriptorId descriptorId, Object object) {
		IntegerValue value = null;
		switch (descriptorId) {
		case VALUE_BYTE:
			value = OdataFactory.eINSTANCE.createByteValue();
			((ByteValue) value).setValue(TypeConverter.getByteValue(object));
			break;
		case VALUE_SBYTE:
			value = OdataFactory.eINSTANCE.createSByteValue();
			((SByteValue) value).setValue(TypeConverter.getByteValue(object));
			break;
		case VALUE_INT16:
			value = OdataFactory.eINSTANCE.createInt16Value();
			((Int16Value) value).setValue(TypeConverter.getShortValue(object));
			break;
		case VALUE_INT32:
			value = OdataFactory.eINSTANCE.createInt32Value();
			((Int32Value) value)
					.setValue(TypeConverter.getIntegerValue(object));
			break;
		case VALUE_INT64:
			value = OdataFactory.eINSTANCE.createInt64Value();
			((Int64Value) value).setValue(TypeConverter.getLongValue(object));
			break;
		default:
			break;

		}
		return value;

	}
}
