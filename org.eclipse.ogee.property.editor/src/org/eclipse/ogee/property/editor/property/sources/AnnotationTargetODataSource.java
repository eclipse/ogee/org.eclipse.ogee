/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import java.util.Set;

import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.property.editor.annotations.AnnotationHelper;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

public class AnnotationTargetODataSource extends
		AbstractAnnotationPropertySource {

	private IAnnotationTarget annotationTarget;

	public AnnotationTargetODataSource(IAnnotationTarget obj, boolean isReadOnly) {
		this.annotationTarget = obj;
		this.isReadOnly = isReadOnly;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		Set<IPropertyDescriptor> propertyDescriptors = AnnotationHelper
				.getInstance().createTargetAnnotationPropertyDescriptors(
						annotationTarget, annotationHelperMap, isReadOnly);

		return propertyDescriptors
				.toArray(new IPropertyDescriptor[propertyDescriptors.size()]);
	}

}
