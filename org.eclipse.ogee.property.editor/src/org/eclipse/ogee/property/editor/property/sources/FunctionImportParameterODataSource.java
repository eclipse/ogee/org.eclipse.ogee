/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.property.editor.commands.TransactionChangeCommand;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.type.handler.ParameterTypeHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class FunctionImportParameterODataSource extends
		AbstractPropertySourceArtifact {
	protected Parameter parameter;
	protected ParameterTypeHandler typeHandler;

	public FunctionImportParameterODataSource(Parameter parameter,
			boolean isReadOnly) {
		super(isReadOnly);
		this.parameter = parameter;
		typeHandler = new ParameterTypeHandler(parameter);
		descriptorIds = getDescriptorIds();
	}

	private DescriptorId[] getDescriptorIds() {
		List<DescriptorId> propertyDescriptorList = new ArrayList<DescriptorId>();
		propertyDescriptorList.add(DescriptorId.NAME);
		propertyDescriptorList.add(DescriptorId.TYPECOMBO);
		propertyDescriptorList.add(DescriptorId.DOCUMENTATION);		
		if (parameter.getType() instanceof SimpleTypeUsage) {
			propertyDescriptorList.add(DescriptorId.FACETS);
		}
		return propertyDescriptorList
				.toArray(new DescriptorId[propertyDescriptorList.size()]);
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, parameter.getName());
		Object value = typeHandler.getTypeIndex(parameter.getType());
		if (isReadOnly) {

			if (parameter.getType() instanceof SimpleTypeUsage) {
				value = ((SimpleTypeUsage) parameter.getType()).getSimpleType()
						.getType().getName();
			}

			else if (parameter.getType() instanceof ComplexTypeUsage) {
				value = ((ComplexTypeUsage) parameter.getType())
						.getComplexType().getName();
			}

			else if (parameter.getType() instanceof EntityTypeUsage) {
				value = ((EntityTypeUsage) parameter.getType()).getEntityType()
						.getName();
			} else if (parameter.getType() instanceof EnumTypeUsage) {
				value = ((EnumTypeUsage) parameter.getType()).getEnumType()
						.getName();
			}
		}
		descriptorValueMap.put(DescriptorId.TYPECOMBO, value);
		Object[] o = new Object[] { Parameter.class, parameter.getType() };
		descriptorValueMap.put(DescriptorId.FACETS, o);
		descriptorValueMap.put(DescriptorId.ISCOLLECTION, parameter.getType()
				.isCollection());
	}

	@Override
	protected String[] getComboBoxValues(DescriptorId id) {
		if (id == DescriptorId.ISCOLLECTION)
			return Utilities.boolValues;
		return typeHandler.getTypesArray();
	}

	@Override
	protected Object getAdditionalValue(DescriptorId descriptorId, Object object) {
		if (descriptorId == DescriptorId.TYPECOMBO)
			return typeHandler;
		return null;
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if (id == DescriptorId.ISCOLLECTION && !isReadOnly) {
			TransactionChangeCommand command = new TransactionChangeCommand(id,
					((Integer) value).equals(1), parameter.getType());
			command.setMethodName("setCollection"); //$NON-NLS-1$
			command.setParameterDecleredClass(Boolean.TYPE);
			Utilities.executeTransaction(command);
		} else
			super.setPropertyValue(id, value);
	}

	@Override
	protected EObject getEObject() {
		return parameter;
	}
}
