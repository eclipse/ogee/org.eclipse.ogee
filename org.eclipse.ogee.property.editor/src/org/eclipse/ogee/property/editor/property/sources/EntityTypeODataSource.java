/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.type.handler.AbstractTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.BaseTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class EntityTypeODataSource extends AbstractPropertySourceArtifact {
	protected EntityType entityType;
	private SchemaHandler schemaHandler;
	private BaseTypeHandler baseTypeHandler;

	public EntityTypeODataSource(EntityType entityType, boolean isReadOnly) {
		super(isReadOnly);
		this.entityType = entityType;
		descriptorIds = new DescriptorId[] { DescriptorId.NAME,
				DescriptorId.ISABSTRACT, DescriptorId.ISMEDIA,
				DescriptorId.BASE, DescriptorId.DOCUMENTATION,
				DescriptorId.SCHEMA };
		schemaHandler = new SchemaHandler();
		baseTypeHandler = new BaseTypeHandler(entityType);
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, entityType.getName());
		descriptorValueMap
				.put(DescriptorId.ISABSTRACT, entityType.isAbstract());
		descriptorValueMap.put(DescriptorId.ISMEDIA, entityType.isMedia());
		Object value = Messages.EMPTY_ENTER;
		if (entityType.getBaseType() != null) {
			value = SchemaHandler.getNamespaceOrAlias(entityType.getBaseType())
					+ AbstractTypeHandler.NAMESPACE_SEPARATOR
					+ entityType.getBaseType().getName();
		}
		if (!isReadOnly) {
			value = baseTypeHandler.getIndex((String) value);
		}

		descriptorValueMap.put(DescriptorId.BASE, value);
		descriptorValueMap.put(DescriptorId.SCHEMA, schemaHandler
				.getInitialDescriptorValue(isReadOnly, getEObject()));
	}

	@Override
	protected String[] getComboBoxValues(DescriptorId id) {
		if (id == DescriptorId.BASE)
			return baseTypeHandler.getTypesArray();
		else if (id == DescriptorId.ISMEDIA || id == DescriptorId.ISABSTRACT)
			return Utilities.boolValues;
		if (id == DescriptorId.SCHEMA)
			return schemaHandler.getAllSchemata(entityType);
		return null;
	}

	@Override
	protected Object getAdditionalValue(DescriptorId descriptorId, Object object) {
		if (descriptorId == DescriptorId.SCHEMA) {
			if (object == null)
				return SchemaHandler.getSchema(getEObject());
			return schemaHandler.getSchema((Integer) object);
		} else if (descriptorId == DescriptorId.BASE) {
			EObject newBaseType = null;
			oldValue = entityType.getBaseType();
			if (!Messages.EMPTY_ENTER.equals(baseTypeHandler
					.getName((Integer) object))) {
				newBaseType = baseTypeHandler.getEobject((Integer) object);
			}
			newValue = newBaseType;
			return newBaseType;
		}
		return null;
	}

	@Override
	protected EObject getEObject() {
		return entityType;
	}

}
