/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;

public class SchemaODataSource extends AbstractPropertySourceArtifact {
	protected Schema schema;

	public SchemaODataSource(Schema schema, boolean isReadOnly) {
		super(isReadOnly);
		this.schema = schema;
		descriptorIds = new DescriptorId[] { DescriptorId.NAMESPACE,
				DescriptorId.ALIAS };
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAMESPACE, schema.getNamespace());
		String alias = schema.getAlias();
		if (alias == null)
			alias = Messages.EMPTY_String;
		descriptorValueMap.put(DescriptorId.ALIAS, alias);
	}

	@Override
	protected EObject getEObject() {
		return schema;
	}
}