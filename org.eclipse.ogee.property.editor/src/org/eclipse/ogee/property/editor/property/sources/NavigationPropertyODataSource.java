/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.designer.api.IODataDiagramCreator;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.Multiplicity;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.property.editor.commands.TransactionChangeCommandAbstract;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.type.handler.AbstractTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class NavigationPropertyODataSource extends
		AbstractPropertySourceArtifact {
	protected NavigationProperty navigationProperty;

	protected Hashtable<String, Association> associationtMap = new Hashtable<String, Association>();
	protected List<String> keysList = new ArrayList<String>();

	public NavigationPropertyODataSource(NavigationProperty navigationProperty,
			boolean isReadOnly) {
		super(isReadOnly);
		this.navigationProperty = navigationProperty;
		descriptorIds = new DescriptorId[] { DescriptorId.NAME,
				DescriptorId.FROM_ROLE,				
				DescriptorId.RELATIONSHIP, DescriptorId.TO_ROLE,
				DescriptorId.MULTIPLICITY, DescriptorId.DOCUMENTATION };
	}

	protected String[] getComboBoxValues(DescriptorId id) {
		if (id == DescriptorId.RELATIONSHIP)
			return createAssociationKeys();
		if (id == DescriptorId.MULTIPLICITY)
			return Utilities.createMultiplicityArray();
		if (id == DescriptorId.CONTAINS_TARGET)
			return Utilities.boolValues;
		return null;
	}

	/**
	 * creates the map and the list of entity sets for the Entity set
	 * PropertyDescriptor
	 * 
	 * @return
	 */
	private String[] createAssociationKeys() {
		List<Association> associations = new ArrayList<Association>();
		Schema schema = SchemaHandler.getSchema(navigationProperty);
		if (schema != null) {
			for (Schema s : SchemaHandler.getSchemata(schema)) {
				associations.addAll(s.getAssociations());
			}

			for (Association association : associations) {
				for (Role role : association.getEnds()) {
					if (role.getType().equals(navigationProperty.eContainer())) {
						associationtMap
								.put(SchemaHandler
										.getNamespaceOrAlias(association)
										+ AbstractTypeHandler.NAMESPACE_SEPARATOR
										+ association.getName(), association);
					}
				}

			}
		}
		keysList.addAll(associationtMap.keySet());
		return keysList.toArray(new String[keysList.size()]);
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		Object value = -1;
		descriptorValueMap.put(DescriptorId.NAME, navigationProperty.getName());
		if (navigationProperty.getRelationship() != null) {
			if (isReadOnly)
				value = navigationProperty.getRelationship().getName();
			else if (navigationProperty.getRelationship() != null)
				value = keysList.indexOf(SchemaHandler
						.getNamespaceOrAlias(navigationProperty
								.getRelationship())
						+ AbstractTypeHandler.NAMESPACE_SEPARATOR
						+ navigationProperty.getRelationship().getName());
		} else {
			if (isReadOnly)
				value = Messages.EMPTY_String;
		}
		descriptorValueMap.put(DescriptorId.RELATIONSHIP, value);
		if (navigationProperty.getFromRole() != null) {
			descriptorValueMap.put(DescriptorId.FROM_ROLE, navigationProperty
					.getFromRole().getName());
		} else {
			descriptorValueMap.put(DescriptorId.FROM_ROLE,
					Messages.EMPTY_String);
		}

		if (navigationProperty.getToRole() != null) {
			descriptorValueMap.put(DescriptorId.TO_ROLE, navigationProperty
					.getToRole().getName());
		} else {
			descriptorValueMap.put(DescriptorId.TO_ROLE, Messages.EMPTY_String);
		}
		descriptorValueMap.put(DescriptorId.CONTAINS_TARGET,
				navigationProperty.isContainsTarget());

		value = -1;
		if (navigationProperty.getToRole() != null) {
			if (isReadOnly)
				value = navigationProperty.getToRole().getMultiplicity()
						.getLiteral();
			else
				value = navigationProperty.getToRole().getMultiplicity()
						.getValue();
		} else {
			if (isReadOnly)
				value = Messages.EMPTY_String;
		}
		descriptorValueMap.put(DescriptorId.MULTIPLICITY, value);
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if (isReadOnly)
			return;
		final DescriptorId descriptor = (DescriptorId) id;
		if (descriptor == DescriptorId.RELATIONSHIP
				|| descriptor == DescriptorId.MULTIPLICITY) {

			TransactionChangeCommandAbstract transactionChangeCommand = new TransactionChangeCommandAbstract(
					id, value, navigationProperty) {
				@Override
				public void doExecute() {
					if (descriptor == DescriptorId.MULTIPLICITY) {
						if (navigationProperty.getToRole() != null) {
							navigationProperty.getToRole().setMultiplicity(
									Multiplicity.get((Integer) value));
						}
					} else {
						Association previousNssociation = navigationProperty
								.getRelationship();
						Association association = associationtMap.get(keysList
								.get((Integer) value));
						navigationProperty.setRelationship(association);

						Role fromRole = null;
						Role toRole = null;
						if (association.getEnds().get(0).getType()
								.equals(navigationProperty.eContainer())) {
							fromRole = association.getEnds().get(0);
							toRole = association.getEnds().get(1);

						} else {
							fromRole = association.getEnds().get(1);
							toRole = association.getEnds().get(0);
						}

						navigationProperty.setToRole(toRole);
						navigationProperty.setFromRole(fromRole);

						// in order to refresh the Grphiti view
						IODataDiagramCreator.INSTANCE.refreshEditor(
								navigationProperty, previousNssociation,
								association);
					}
				}
			};
			Utilities.executeTransaction(transactionChangeCommand);
		} else {
			super.setPropertyValue(id, value);
		}
	}

	@Override
	protected EObject getEObject() {
		return navigationProperty;
	}
}
