/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.IDocumentable;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.property.editor.commands.TransactionChangeCommand;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class DocumentationODataSource extends AbstractPropertySourceArtifact {

	protected Documentation documentation;
	protected IDocumentable documentable;

	public DocumentationODataSource(IDocumentable documentable,
			boolean isReadOnly) {
		super(isReadOnly);
		this.documentable = documentable;
		this.documentation = documentable.getDocumentation();
		descriptorIds = new DescriptorId[] { DescriptorId.SUMMARY,
				DescriptorId.LONG_DESCRIPTION };
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		String summary;
		String description;
		if (documentation == null) {
			summary = description = Messages.EMPTY_String;
		} else {
			if (documentation.getLongDescription() == null) {
				description = Messages.EMPTY_String;
			} else {
				description = documentation.getLongDescription();
			}
			if (documentation.getSummary() == null) {
				summary = Messages.EMPTY_String;
			} else {
				summary = documentation.getSummary();
			}
		}
		descriptorValueMap.clear();
		descriptorValueMap.put(DescriptorId.LONG_DESCRIPTION, description);
		descriptorValueMap.put(DescriptorId.SUMMARY, summary);

	}

	@Override
	public String toString() {
		return Messages.EMPTY_String;
	}

	@Override
	protected EObject getEObject() {
		if (documentation == null) {
			documentation = OdataFactory.eINSTANCE.createDocumentation();
			TransactionChangeCommand command = new TransactionChangeCommand(
					null, documentation, documentable);
			command.setMethodName("setDocumentation"); //$NON-NLS-1$
			command.setParameterDecleredClass(Documentation.class);
			Utilities.executeTransaction(command);
			// documentable.setDocumentation(documentation);
		}
		return documentation;
	}
}
