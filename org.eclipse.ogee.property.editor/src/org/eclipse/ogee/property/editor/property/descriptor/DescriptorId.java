/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.descriptor;

import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Abstract;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Alias;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Association;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Author;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_BaseType;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Bindto;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Collection;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_ContainsTarget;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Default;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Description;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Documentation;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_END1;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_END2;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_EntitySet;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_EntityType;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Facets;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_FixedLength;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_FromRole;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_HasSideeffect;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Id;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Key;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_LongDescriptor;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_MaxLength;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Media;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_MetadataURL;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Multiplicity;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Name;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Namespace;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Nullable;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Precision;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_ReferentialConstraint;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Relationship;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_ReturnType;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Role;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Scale;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Schema;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_ServiceURL;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Summary;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_TechnicalServiceName;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_TechnicalServiceVersion;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Title;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_ToRole;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_Type;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_UpdatedDate;
import static org.eclipse.ogee.property.editor.nls.messages.Messages.DescriptorId_UseForEtag;

import org.eclipse.ogee.property.editor.nls.messages.Messages;

public enum DescriptorId {
	NAME(DescriptorId_Name,
			PropertyDescriptorType.NamePropertyDescriptorValidator), ISABSTRACT(
			DescriptorId_Abstract,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), ISMEDIA(
			DescriptorId_Media,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), BASE(
			DescriptorId_BaseType,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), END1(
			DescriptorId_END1), END2(DescriptorId_END2), DOCUMENTATION(
			DescriptorId_Documentation), REFERENTIAL_CONSTRAINT(
			DescriptorId_ReferentialConstraint,
			PropertyDescriptorType.ButtonPropertyDescriptor), SCHEMA(
			DescriptorId_Schema,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), ASSOCIATION_ENTITYSET(
			DescriptorId_EntitySet,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), ROLE(
			DescriptorId_Role,
			PropertyDescriptorType.TextPropertyDescriptorValidator), ASSOCIATION(
			DescriptorId_Association), TYPE(DescriptorId_Type), TYPECOMBO(
			DescriptorId_Type,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), ISCOLLECTION(
			DescriptorId_Collection,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), SUMMARY(
			DescriptorId_Summary, PropertyDescriptorType.TextPropertyDescriptor), LONG_DESCRIPTION(
			DescriptorId_LongDescriptor,
			PropertyDescriptorType.TextPropertyDescriptor), ISDEFAULT(
			DescriptorId_Default,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), DEFAULT(
			DescriptorId_Default,
			PropertyDescriptorType.EDMTypesPropertyDescriptorValidator), PRECISION(
			DescriptorId_Precision,
			PropertyDescriptorType.Int32PropertyDescriptorValidator), MAX_LENGTH(
			DescriptorId_MaxLength,
			PropertyDescriptorType.Int32PropertyDescriptorValidator), SCALE(
			DescriptorId_Scale,
			PropertyDescriptorType.Int32PropertyDescriptorValidator), FIXED_LENGTH(
			DescriptorId_FixedLength,
			PropertyDescriptorType.Int32PropertyDescriptorValidator), SIDEEFFECT(
			DescriptorId_HasSideeffect,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), ISBINDING(
			DescriptorId_Bindto), FACETS(DescriptorId_Facets), FROM_ROLE(
			DescriptorId_FromRole), CONTAINS_TARGET(
			DescriptorId_ContainsTarget,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), RELATIONSHIP(
			DescriptorId_Relationship,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), TO_ROLE(
			DescriptorId_ToRole), MULTIPLICITY(DescriptorId_Multiplicity,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), ISNULLABLE(
			DescriptorId_Nullable), ISNULLABLECOMBO(DescriptorId_Nullable,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), IS_USE_FOR_ETAG(
			DescriptorId_UseForEtag), ISKEY(DescriptorId_Key), ENTITY_TYPE(
			DescriptorId_EntityType), NAMESPACE(DescriptorId_Namespace,
			PropertyDescriptorType.NamePropertyDescriptorValidator), RETURN_TYPE(
			DescriptorId_ReturnType,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), ENTIRY_SET(
			DescriptorId_EntitySet,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), DESCRIPTION(
			DescriptorId_Description), TITLE(DescriptorId_Title), AUTHOR(
			DescriptorId_Author), TECHNICAL_SERVICE_VERSION(
			DescriptorId_TechnicalServiceVersion), ID(DescriptorId_Id), METADATAURL(
			DescriptorId_MetadataURL), TECHNICAL_SERVICE_NAME(
			DescriptorId_TechnicalServiceName), SERVICEURL(
			DescriptorId_ServiceURL), UPDATEDDATE(DescriptorId_UpdatedDate), ALIAS(
			DescriptorId_Alias,
			PropertyDescriptorType.NamePropertyDescriptorValidator), ASSOCIATIONSET(
			Messages.DescriptorId_AssociationSets,
			PropertyDescriptorType.ButtonPropertyDescriptor), ISFLAGS(
			Messages.DescriptorId_Flags,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), UNDERLYING_TYPE(
			Messages.DescriptorId_UnderlyingType,
			PropertyDescriptorType.ComboBoxPropertyDescriptor), VALUE_INT64(
			Messages.DescriptorId_EnumValue,
			PropertyDescriptorType.Int64PropertyDescriptorValidator), VALUE_INT32(
			Messages.DescriptorId_EnumValue,
			PropertyDescriptorType.Int32PropertyDescriptorValidator), VALUE_INT16(
			Messages.DescriptorId_EnumValue,
			PropertyDescriptorType.Int16PropertyDescriptorValidator), VALUE_BYTE(
			Messages.DescriptorId_EnumValue,
			PropertyDescriptorType.BytePropertyDescriptorValidator), VALUE_SBYTE(
			Messages.DescriptorId_EnumValue,
			PropertyDescriptorType.SBytePropertyDescriptorValidator);

	private final String displayName;
	private final PropertyDescriptorType descriptorType;

	private DescriptorId(String displayName) {
		this.displayName = displayName;
		descriptorType = PropertyDescriptorType.PropertyDescriptor;
	}

	private DescriptorId(String displayName,
			PropertyDescriptorType descriptorType) {
		this.displayName = displayName;
		this.descriptorType = descriptorType;
	}

	public String getDisplayName() {
		return displayName;
	}

	public PropertyDescriptorType getDescriptorType() {
		return descriptorType;
	}

}
