/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.Binding;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.property.editor.commands.TransactionChangeCommandAbstract;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.property.descriptor.PropertyDescriptorFactory;
import org.eclipse.ogee.property.editor.type.handler.AbstractTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.BaseTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

public class BindingODataSource extends AbstractPropertySourceArtifact {
	protected Binding binding;
	protected FunctionImport functionImport;
	private BaseTypeHandler typeHandler = null;

	public BindingODataSource(FunctionImport functionImport, boolean isReadOnly) {
		super(isReadOnly);
		this.functionImport = functionImport;
		this.binding = functionImport.getBinding();
		descriptorIds = new DescriptorId[] { DescriptorId.TYPECOMBO,
				DescriptorId.ISCOLLECTION };
		typeHandler = new BaseTypeHandler(functionImport);

	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		if (binding == null) {
			descriptorValueMap.put(DescriptorId.ISCOLLECTION,
					Messages.EMPTY_String);
			Object value = Messages.EMPTY_ENTER;
			if (!isReadOnly)
				value = typeHandler.getIndex(Messages.EMPTY_ENTER);
			descriptorValueMap.put(DescriptorId.TYPECOMBO, value);
		} else {
			descriptorValueMap.put(DescriptorId.ISCOLLECTION,
					binding.isCollection());
			if (isReadOnly) {
				descriptorValueMap.put(DescriptorId.TYPECOMBO, binding
						.getType().getName());
			} else {
				if (binding.getType() != null)
					descriptorValueMap.put(DescriptorId.TYPECOMBO, typeHandler
							.getIndex(SchemaHandler.getNamespaceOrAlias(binding
									.getType())
									+ AbstractTypeHandler.NAMESPACE_SEPARATOR
									+ binding.getType().getName()));
			}
		}
	}

	@Override
	protected String[] getComboBoxValues(DescriptorId id) {
		if (id == DescriptorId.ISCOLLECTION)
			return Utilities.boolValues;
		return typeHandler.getTypesArray();
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		IPropertyDescriptor[] propertyDescriptors;

		if (binding == null) {
			if (!isReadOnly) {
				propertyDescriptors = new IPropertyDescriptor[] {
						PropertyDescriptorFactory
								.getInstance()
								.createComboBoxPropertyDescriptor(
										DescriptorId.TYPECOMBO,
										getComboBoxValues(DescriptorId.TYPECOMBO)),
						PropertyDescriptorFactory.getInstance()
								.createPropertyDescriptor(
										DescriptorId.ISCOLLECTION) };
			} else {
				propertyDescriptors = new IPropertyDescriptor[] {
						PropertyDescriptorFactory.getInstance()
								.createPropertyDescriptor(
										DescriptorId.TYPECOMBO),
						PropertyDescriptorFactory.getInstance()
								.createPropertyDescriptor(
										DescriptorId.ISCOLLECTION) };
			}
			return propertyDescriptors;
		} else {
			propertyDescriptors = super.getPropertyDescriptors();
		}
		return propertyDescriptors;
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if (isReadOnly)
			return;
		TransactionChangeCommandAbstract transactionChangeCommand = new TransactionChangeCommandAbstract(
				id, value, functionImport) {

			@Override
			public void doExecute() {
				DescriptorId descriptor = (DescriptorId) id;
				switch (descriptor) {
				case ISCOLLECTION:
					if (binding == null) {
						binding = OdataFactory.eINSTANCE.createBinding();
						functionImport.setBinding(binding);
					}
					binding.setCollection(((Integer) value).equals(1));
					break;
				case TYPECOMBO:
					if (typeHandler.getName((Integer) value).equals(
							Messages.EMPTY_ENTER)) {
						binding = null;
						functionImport.setBinding(null);
						break;
					}

					if (binding == null) {
						binding = OdataFactory.eINSTANCE.createBinding();
						binding.setCollection(false);
						functionImport.setBinding(binding);
					}
					binding.setType((EntityType) typeHandler
							.getEobject((Integer) value));
					break;
				default:
					break;
				}
			}
		};

		Utilities.executeTransaction(transactionChangeCommand);

	}

	@Override
	public String toString() {
		return Messages.EMPTY_String;
	}

	@Override
	protected EObject getEObject() {
		return functionImport;
	}

}
