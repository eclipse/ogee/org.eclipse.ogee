/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.Role;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.type.handler.AbstractTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class RoleODataSource extends AbstractPropertySourceArtifact {

	protected Role role;

	public RoleODataSource(Role role, boolean isReadOnly) {
		super(isReadOnly);
		this.role = role;
		descriptorIds = new DescriptorId[] { DescriptorId.ROLE,
				DescriptorId.ENTITY_TYPE, DescriptorId.MULTIPLICITY,
				DescriptorId.DOCUMENTATION };
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.ROLE, role.getName());

		Object value = Messages.EMPTY_String;
		if (role.getType() != null && role.getType().getName() != null)
			value = SchemaHandler.getNamespaceOrAlias(role.getType())
					+ AbstractTypeHandler.NAMESPACE_SEPARATOR
					+ role.getType().getName();
		descriptorValueMap.put(DescriptorId.ENTITY_TYPE, value);

		value = role.getMultiplicity().getValue();
		if (isReadOnly)
			value = role.getMultiplicity().getLiteral();
		descriptorValueMap.put(DescriptorId.MULTIPLICITY, value);
	}

	@Override
	protected String[] getComboBoxValues(DescriptorId id) {
		return Utilities.createMultiplicityArray();
	}

	@Override
	public String toString() {
		return role.getName();
	}

	@Override
	protected EObject getEObject() {
		return role;
	}

}
