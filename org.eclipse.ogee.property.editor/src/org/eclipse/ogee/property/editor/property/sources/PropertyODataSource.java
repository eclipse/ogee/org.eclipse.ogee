/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.property.editor.commands.TransactionChangeCommandAbstract;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.property.descriptor.PropertyDescriptorFactory;
import org.eclipse.ogee.property.editor.type.handler.PropertyTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.PropertyDescriptor;

public class PropertyODataSource extends AbstractPropertySourceArtifact {

	protected Property property;
	private PropertyTypeHandler typeHandler;

	public PropertyODataSource(Property property, boolean isReadOnly) {
		super(isReadOnly);
		this.property = property;
		typeHandler = new PropertyTypeHandler(property);
		descriptorIds = new DescriptorId[] { DescriptorId.NAME,
				// TODO Start - Enable with OData V4 support
				// DescriptorId.ISCOLLECTION,
				// End - Enable with OData V4 support
				DescriptorId.DOCUMENTATION };
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, property.getName());
		EObject container = property.eContainer();
		descriptorValueMap.put(DescriptorId.ISNULLABLE, property.isNullable());
		descriptorValueMap.put(DescriptorId.ISNULLABLECOMBO,
				property.isNullable());
		descriptorValueMap.put(DescriptorId.IS_USE_FOR_ETAG,
				property.isForEtag());
		Object value = typeHandler.getTypeIndex(property.getType());
		if (isReadOnly) {
			IPropertyTypeUsage pUsage = property.getType();

			if (pUsage instanceof SimpleTypeUsage
					&& ((SimpleTypeUsage) pUsage).getSimpleType() != null
					&& ((SimpleTypeUsage) pUsage).getSimpleType().getType() != null) {
				value = ((SimpleTypeUsage) pUsage).getSimpleType().getType()
						.getLiteral();
			} else if (pUsage instanceof ComplexTypeUsage
					&& ((ComplexTypeUsage) pUsage).getComplexType() != null) {
				value = ((ComplexTypeUsage) pUsage).getComplexType().getName();
			} else if (pUsage instanceof EnumTypeUsage
					&& ((EnumTypeUsage) pUsage).getEnumType() != null) {
				value = ((EnumTypeUsage) pUsage).getEnumType().getName();
			} else
				value = Messages.EMPTY_String;
		}
		descriptorValueMap.put(DescriptorId.TYPECOMBO, value);
		value = false;
		IPropertyTypeUsage pTypeUsage = property.getType();
		if (pTypeUsage != null && pTypeUsage.isCollection()) {
			value = true;
		}
		descriptorValueMap.put(DescriptorId.ISCOLLECTION, value);
		if (pTypeUsage != null) {
			if (container instanceof EntityType)
				descriptorValueMap.put(DescriptorId.ISKEY,
						(((EntityType) property.eContainer()).getKeys()
								.contains(property)));
			Object[] o = new Object[] { Property.class, property.getType() };
			descriptorValueMap.put(DescriptorId.FACETS, o);
		}
	}

	@Override
	protected String[] getComboBoxValues(DescriptorId id) {
		if (id == DescriptorId.ISCOLLECTION
				|| id == DescriptorId.ISNULLABLECOMBO
				|| id == DescriptorId.IS_USE_FOR_ETAG
				|| id == DescriptorId.ISKEY)
			return Utilities.boolValues;
		if (id == DescriptorId.TYPECOMBO)
			return typeHandler.getTypesArray();
		return null;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		ArrayList<IPropertyDescriptor> propertyDescriptorList = new ArrayList<IPropertyDescriptor>();
		if (isReadOnly) {
			if (property.getType() instanceof SimpleTypeUsage) {
				propertyDescriptorList.add(new PropertyDescriptor(
						DescriptorId.ISNULLABLE, DescriptorId.ISNULLABLE
								.getDisplayName()));
				propertyDescriptorList.add(new PropertyDescriptor(
						DescriptorId.FACETS, DescriptorId.FACETS
								.getDisplayName()));
				propertyDescriptorList.add(new PropertyDescriptor(
						DescriptorId.TYPECOMBO, DescriptorId.TYPECOMBO
								.getDisplayName()));

				if (property.eContainer() instanceof EntityType) {
					propertyDescriptorList.add(new PropertyDescriptor(
							DescriptorId.IS_USE_FOR_ETAG,
							DescriptorId.IS_USE_FOR_ETAG.getDisplayName()));
					propertyDescriptorList.add(new PropertyDescriptor(
							DescriptorId.ISKEY, DescriptorId.ISKEY
									.getDisplayName()));
				}

			} else if (property.getType() instanceof EnumTypeUsage
					&& property.eContainer() instanceof EntityType) {
				propertyDescriptorList.add(PropertyDescriptorFactory
						.getInstance().createPropertyDescriptor(
								DescriptorId.ISNULLABLE));
				propertyDescriptorList.add(PropertyDescriptorFactory
						.getInstance().createPropertyDescriptor(
								DescriptorId.ISKEY));
				propertyDescriptorList.add(PropertyDescriptorFactory
						.getInstance().createPropertyDescriptor(
								DescriptorId.TYPECOMBO));
			} else {
				propertyDescriptorList.add(new PropertyDescriptor(
						DescriptorId.TYPECOMBO, DescriptorId.TYPECOMBO
								.getDisplayName()));
			}

		} else {
			if (property.getType() instanceof SimpleTypeUsage
					|| this.property.getType() == null) {
				IPropertyDescriptor nullabelDescriptor = PropertyDescriptorFactory
						.getInstance()
						.createComboBoxPropertyDescriptor(
								DescriptorId.ISNULLABLECOMBO,
								getComboBoxValues(DescriptorId.ISNULLABLECOMBO));
				propertyDescriptorList.add(nullabelDescriptor);
				propertyDescriptorList.add(PropertyDescriptorFactory
						.getInstance().createPropertyDescriptor(
								DescriptorId.FACETS));
				propertyDescriptorList.add(PropertyDescriptorFactory
						.getInstance().createComboBoxPropertyDescriptor(
								DescriptorId.TYPECOMBO,
								getComboBoxValues(DescriptorId.TYPECOMBO)));

				if (property.eContainer() instanceof EntityType) {

					propertyDescriptorList
							.add(PropertyDescriptorFactory
									.getInstance()
									.createComboBoxPropertyDescriptor(
											DescriptorId.IS_USE_FOR_ETAG,
											getComboBoxValues(DescriptorId.IS_USE_FOR_ETAG)));
					propertyDescriptorList.add(PropertyDescriptorFactory
							.getInstance().createComboBoxPropertyDescriptor(
									DescriptorId.ISKEY,
									getComboBoxValues(DescriptorId.ISKEY)));

					if (((EntityType) property.eContainer()).getKeys()
							.contains(property)) {
						propertyDescriptorList.remove(nullabelDescriptor);
						propertyDescriptorList.add(PropertyDescriptorFactory
								.getInstance().createPropertyDescriptor(
										DescriptorId.ISNULLABLE));
					}
				}

			} else if (property.getType() instanceof ComplexTypeUsage
					|| property.getType() instanceof EnumTypeUsage) {
				propertyDescriptorList.add(PropertyDescriptorFactory
						.getInstance().createComboBoxPropertyDescriptor(
								DescriptorId.TYPECOMBO,
								typeHandler.getTypesArray()));
			}
			if (property.getType() instanceof EnumTypeUsage
					&& property.eContainer() instanceof EntityType) {
				propertyDescriptorList.add(PropertyDescriptorFactory
						.getInstance().createComboBoxPropertyDescriptor(
								DescriptorId.ISKEY,
								getComboBoxValues(DescriptorId.ISKEY)));
				if (((EntityType) property.eContainer()).getKeys().contains(
						property)) {
					propertyDescriptorList.add(PropertyDescriptorFactory
							.getInstance().createPropertyDescriptor(
									DescriptorId.ISNULLABLE));
				} else {
					propertyDescriptorList
							.add(PropertyDescriptorFactory
									.getInstance()
									.createComboBoxPropertyDescriptor(
											DescriptorId.ISNULLABLECOMBO,
											getComboBoxValues(DescriptorId.ISNULLABLECOMBO)));
				}

			}
		}
		propertyDescriptorList.addAll(Arrays.asList(super
				.getPropertyDescriptors()));
		return propertyDescriptorList
				.toArray(new IPropertyDescriptor[propertyDescriptorList.size()]);
	}

	@Override
	protected Object getAdditionalValue(DescriptorId descriptorId, Object object) {
		if (descriptorId == DescriptorId.TYPECOMBO)
			return typeHandler;
		return null;
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if (isReadOnly)
			return;
		if (id != DescriptorId.ISCOLLECTION && id != DescriptorId.ISKEY) {
			super.setPropertyValue(id, value);
			return;
		}

		TransactionChangeCommandAbstract transactionChangeCommand = new TransactionChangeCommandAbstract(
				id, value, property) {

			@Override
			public void doExecute() {
				if (id == DescriptorId.ISCOLLECTION)
					property.getType().setCollection(
							((Integer) value).equals(1));
				else if (id == DescriptorId.ISKEY) {
					EntityType entityType = (EntityType) property.eContainer();
					if (((Integer) value).equals(1))// check to key
					{
						entityType.getProperties().remove(property);
						entityType.getKeys().add(property);
						property.setNullable(false);

					} else
					// remove from key
					{
						modifyReferentialConstraint(entityType);
						entityType.getProperties().add(property);
						entityType.getKeys().remove(property);
					}
				}

			}

			/**
			 * 
			 * @param entityType
			 */
			private void modifyReferentialConstraint(EntityType entityType) {
				Schema schema = SchemaHandler.getSchema(entityType);
				if (schema == null || schema.getAssociations() == null)
					return;
				for (Association association : schema.getAssociations()) {
					ReferentialConstraint referentialConstraint = association
							.getReferentialConstraint();
					if (referentialConstraint != null
							&& referentialConstraint.getPrincipal() != null
							&& referentialConstraint.getPrincipal().getType() != null
							&& referentialConstraint.getKeyMappings() != null
							&& referentialConstraint.getPrincipal().getType() == entityType)
						referentialConstraint.getKeyMappings().remove(property);
				}
			}
		};

		Utilities.executeTransaction(transactionChangeCommand);
	}

	@Override
	protected EObject getEObject() {
		return property;
	}
}
