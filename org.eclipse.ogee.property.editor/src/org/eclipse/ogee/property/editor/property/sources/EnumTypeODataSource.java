/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.type.handler.EnumUnderlineTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class EnumTypeODataSource extends AbstractPropertySourceArtifact {
	protected EnumType enumType;
	private SchemaHandler schemaHandler;
	private EnumUnderlineTypeHandler enumUnderlineTypeHandler;

	public EnumTypeODataSource(EnumType enumType, boolean isReadOnly) {
		super(isReadOnly);

		this.enumType = enumType;
		descriptorIds = new DescriptorId[] { DescriptorId.NAME,
				DescriptorId.ISFLAGS, DescriptorId.UNDERLYING_TYPE,
				DescriptorId.SCHEMA };
		schemaHandler = new SchemaHandler();
		enumUnderlineTypeHandler = new EnumUnderlineTypeHandler(
				enumType.getApplicableUnderlyingTypes());
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, enumType.getName());
		descriptorValueMap.put(DescriptorId.ISFLAGS, enumType.isFlags());
		descriptorValueMap.put(DescriptorId.UNDERLYING_TYPE,
				enumUnderlineTypeHandler.getIndex(enumType.getUnderlyingType()
						.getName()));
		descriptorValueMap.put(DescriptorId.SCHEMA, schemaHandler
				.getInitialDescriptorValue(isReadOnly, getEObject()));
	}

	@Override
	protected String[] getComboBoxValues(DescriptorId id) {
		if (id == DescriptorId.ISFLAGS)
			return Utilities.boolValues;
		if (id == DescriptorId.UNDERLYING_TYPE)
			return enumUnderlineTypeHandler.getEDMTypesNames();
		if (id == DescriptorId.SCHEMA)
			return schemaHandler.getAllSchemata(enumType);
		return null;
	}

	@Override
	protected Object getAdditionalValue(DescriptorId descriptorId, Object object) {
		if (descriptorId == DescriptorId.SCHEMA) {
			if (object == null)
				return SchemaHandler.getSchema(getEObject());
			return schemaHandler.getSchema((Integer) object);
		}
		if (descriptorId == DescriptorId.UNDERLYING_TYPE) {
			return enumUnderlineTypeHandler.getUnderlineType((Integer) object);
		}
		return null;
	}

	@Override
	protected EObject getEObject() {
		return enumType;
	}

}
