/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.descriptor;

import org.eclipse.jface.dialogs.StatusDialog;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.property.editor.annotations.AnnotationTabRecord;
import org.eclipse.ogee.property.editor.ui.elements.dialog.AssociationSetDialog;
import org.eclipse.ogee.property.editor.ui.elements.dialog.DynamicTermsDialog;
import org.eclipse.ogee.property.editor.ui.elements.dialog.IPropertyDescriptorDialog;
import org.eclipse.ogee.property.editor.ui.elements.dialog.IReturnValue;
import org.eclipse.ogee.property.editor.ui.elements.dialog.ReferentialConstraintsDialog;
import org.eclipse.ui.PlatformUI;

public class DialogContext {
	private Object eObject;
	private StatusDialog dialog;
	private DialogTypesEnum type;
	private boolean isReadOnly;

	public DialogContext(Object eObject, DialogTypesEnum type,
			boolean isReadOnly) {
		this.eObject = eObject;
		this.type = type;
		this.isReadOnly = isReadOnly;
	}

	private void createDialog() {
		switch (type) {
		case AssociationsSet:
			dialog = new AssociationSetDialog(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell(),
					(Association) eObject, isReadOnly);
			break;
		case DynamicTerms:
			dialog = new DynamicTermsDialog(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell(),
					(AnnotationTabRecord) eObject, isReadOnly);
			break;
		case ReferentialConstraint:
			dialog = new ReferentialConstraintsDialog(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell(),
					(Association) eObject, isReadOnly);
			break;
		}
	}

	public StatusDialog getDialog() {
		if (dialog == null) {
			createDialog();
		}

		return dialog;
	}

	public void openDialog() {
		getDialog().open();
	}

	public IReturnValue getReturnValue() {
		return ((IPropertyDescriptorDialog) dialog).getReturnValue();
	}
}
