/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.utilities.TypeConverter;

public class FacetsSimpleTypeODataSource extends AbstractPropertySourceArtifact {

	protected SimpleType simpleType;
	private boolean isParameter;

	public FacetsSimpleTypeODataSource(Object clazz, SimpleTypeUsage type,
			boolean isReadOnly) {
		super(isReadOnly);
		if (type != null) {
			this.simpleType = type.getSimpleType();
		}
		isParameter = (clazz == Parameter.class);
		descriptorIds = getDescriptorIds();
	}

	private DescriptorId[] getDescriptorIds() {
		ArrayList<DescriptorId> propertyDescriptorList = new ArrayList<DescriptorId>();

		if (this.simpleType != null) {
			switch (simpleType.getType()) {
			case BINARY:
			case STRING:
				propertyDescriptorList.add(DescriptorId.MAX_LENGTH);
				propertyDescriptorList.add(DescriptorId.FIXED_LENGTH);
				break;
			case DECIMAL:
				propertyDescriptorList.add(DescriptorId.SCALE);
				propertyDescriptorList.add(DescriptorId.PRECISION);
				break;
			case DATE_TIME:
			case DATE_TIME_OFFSET:
			case TIME:
				propertyDescriptorList.add(DescriptorId.PRECISION);
				break;
			default:
				break;
			}
		}
		if (isParameter) {
			propertyDescriptorList.remove(DescriptorId.FIXED_LENGTH);
		} else {
			propertyDescriptorList.add(DescriptorId.DEFAULT);
		}
		return propertyDescriptorList
				.toArray(new DescriptorId[propertyDescriptorList.size()]);
	}

	@Override
	protected void initializeDescriptorsValuesMap() {

		Object value = Messages.EMPTY_String;

		if (simpleType.getDefaultValue() != null
				&& simpleType.getDefaultValue().getValueObject() != null) {
			switch (simpleType.getType()) {
			case TIME:
				value = TypeConverter.formatTime(simpleType.getDefaultValue()
						.getValueObject());
				break;
			case DATE_TIME:
				value = TypeConverter.formatDateTime(simpleType
						.getDefaultValue().getValueObject());
				break;
			case DATE_TIME_OFFSET:
				value = TypeConverter.formatDateTimeOffset(simpleType
						.getDefaultValue().getValueObject());
				break;
			case BINARY:
				// TODO Start - Enable with OData V4 support
				// case STREAM:
				// End - Enable with OData V4 support
			case GUID:
			case STRING:
				value = simpleType.getDefaultValue().getValueObject()
						.toString();
				break;
			case BYTE:
			case DECIMAL:
			case DOUBLE:
			case INT16:
			case INT32:
			case INT64:
			case SBYTE:
			case SINGLE:
				value = TypeConverter.formatNumberOrEmptyString(simpleType
						.getDefaultValue().getValueObject());
				break;
			default:
				break;
			}
		}
		if (simpleType.getType() == EDMTypes.BOOLEAN) {
			if (!isReadOnly) {
				if (simpleType.getDefaultValue() != null
						&& simpleType.getDefaultValue().getValueObject() != null) {
					value = (Boolean) ((SimpleValue) (simpleType
							.getDefaultValue())).getValueObject() ? 1 : 0;
				} else
					value = -1;
			} else {
				if (simpleType.getDefaultValue() != null
						&& simpleType.getDefaultValue().getValueObject() != null)
					value = (Boolean) ((SimpleValue) (simpleType
							.getDefaultValue())).getValueObject();
				else
					value = Messages.EMPTY_String;
			}
		}
		descriptorValueMap.put(DescriptorId.DEFAULT, value);
		descriptorValueMap.put(DescriptorId.FIXED_LENGTH, TypeConverter
				.formatNumberOrEmptyString(simpleType.getFixedLength()));
		descriptorValueMap.put(DescriptorId.MAX_LENGTH, TypeConverter
				.formatNumberOrEmptyString(simpleType.getMaxLength()));
		descriptorValueMap.put(DescriptorId.PRECISION, TypeConverter
				.formatNumberOrEmptyString(simpleType.getPrecision()));
		descriptorValueMap.put(DescriptorId.SCALE,
				TypeConverter.formatNumberOrEmptyString(simpleType.getScale()));
	}

	@Override
	protected Object getAdditionalValue(DescriptorId descriptorId, Object object) {
		if (descriptorId == DescriptorId.DEFAULT) {
			return simpleType.getType();
		}
		return null;
	}

	@Override
	public String toString() {
		return Messages.EMPTY_String;
	}

	@Override
	protected EObject getEObject() {
		return simpleType;
	}
}
