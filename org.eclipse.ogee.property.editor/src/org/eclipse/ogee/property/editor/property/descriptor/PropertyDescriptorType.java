/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.descriptor;

public enum PropertyDescriptorType
{
    ComboBoxPropertyDescriptor, PropertyDescriptor, TextPropertyDescriptor, ButtonPropertyDescriptor,TextPropertyDescriptorValidator, 
    Int32PropertyDescriptorValidator, FloatPropertyDescriptorValidator, DoublePropertyDescriptorValidator, 
    Int64PropertyDescriptorValidator, StringTextComboBoxPropertyDescriptor, IntegerTextComboBoxPropertyDescriptor,
    LongTextComboBoxPropertyDescriptor,FloatTextComboBoxPropertyDescriptor,DoubleTextComboBoxPropertyDescriptor, 
    DecimalTextComboBoxPropertyDescriptor,
    ShortTextComboBoxPropertyDescriptor, ByteTextComboBoxPropertyDescriptor, Int16PropertyDescriptorValidator, 
    BytePropertyDescriptorValidator, SBytePropertyDescriptorValidator, EDMTypesPropertyDescriptorValidator, 
    DateTimeTextComboBoxPropertyDescriptorValidator, TimeTextComboBoxPropertyDescriptorValidator, DateTimeOffsetTextComboBoxPropertyDescriptorValidator,
    GUIDTextComboBoxPropertyDescriptor,NamePropertyDescriptorValidator;
}
