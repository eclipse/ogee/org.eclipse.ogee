/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.type.handler.AbstractTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.BaseTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class ComplexTypeODataSource extends AbstractPropertySourceArtifact {

	protected ComplexType complexType;
	private SchemaHandler schemaHandler;
	private BaseTypeHandler baseTypeHandler;

	public ComplexTypeODataSource(ComplexType complexType, boolean isReadOnly) {
		super(isReadOnly);
		this.complexType = complexType;
		descriptorIds = new DescriptorId[] { DescriptorId.NAME,
				DescriptorId.ISABSTRACT, DescriptorId.BASE,
				DescriptorId.DOCUMENTATION, DescriptorId.SCHEMA };
		schemaHandler = new SchemaHandler();
		baseTypeHandler = new BaseTypeHandler(complexType);

	}

	@Override
	protected String[] getComboBoxValues(DescriptorId id) {
		if (id == DescriptorId.ISABSTRACT)
			return Utilities.boolValues;
		if (id == DescriptorId.BASE)
			return baseTypeHandler.getTypesArray();
		if (id == DescriptorId.SCHEMA)
			return schemaHandler.getAllSchemata(getEObject());
		return null;
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, complexType.getName());
		descriptorValueMap.put(DescriptorId.ISABSTRACT,
				complexType.isAbstract());

		Object value = Messages.EMPTY_ENTER;
		if (complexType.getBaseType() != null) {
			value = SchemaHandler
					.getNamespaceOrAlias(complexType.getBaseType())
					+ AbstractTypeHandler.NAMESPACE_SEPARATOR
					+ complexType.getBaseType().getName();
		}
		if (!isReadOnly) {
			value = baseTypeHandler.getIndex((String) value);
		}
		descriptorValueMap.put(DescriptorId.BASE, value);
		descriptorValueMap.put(DescriptorId.SCHEMA, schemaHandler
				.getInitialDescriptorValue(isReadOnly, getEObject()));
	}

	@Override
	protected Object getAdditionalValue(DescriptorId descriptorId, Object object) {
		if (descriptorId == DescriptorId.SCHEMA) {
			if (object == null)
				return SchemaHandler.getSchema(getEObject());
			return schemaHandler.getSchema((Integer) object);
		} else if (descriptorId == DescriptorId.BASE) {
			EObject newBaseType = null;
			oldValue = complexType.getBaseType();
			if (!Messages.EMPTY_ENTER.equals(baseTypeHandler
					.getName((Integer) object))) {
				newBaseType = baseTypeHandler.getEobject((Integer) object);
			}
			newValue = newBaseType;
			return newBaseType;
		}
		return null;
	}

	@Override
	protected EObject getEObject() {
		return complexType;
	}
}
