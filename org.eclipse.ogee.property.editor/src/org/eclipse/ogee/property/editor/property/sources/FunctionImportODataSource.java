/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;

//import org.eclipse.ogee.property.editor.utilities.Utilities;

public class FunctionImportODataSource extends AbstractPropertySourceArtifact {

	protected FunctionImport functionImport;

	public FunctionImportODataSource(FunctionImport functionImport,
			boolean isReadOnly) {
		super(isReadOnly);
		this.functionImport = functionImport;
		descriptorIds = new DescriptorId[] { DescriptorId.NAME,
				DescriptorId.SIDEEFFECT,				
				DescriptorId.DOCUMENTATION };
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, functionImport.getName());
		descriptorValueMap.put(DescriptorId.SIDEEFFECT,
				functionImport.isSideEffecting());

	}

	@Override
	protected String[] getComboBoxValues(DescriptorId id) {		
		String[] comboBoxValues = { Messages.FunctionImport_HttpMethod_GET,
				Messages.FunctionImport_HttpMethod_POST };
		return comboBoxValues;
	}

	@Override
	protected EObject getEObject() {
		return functionImport;
	}
}