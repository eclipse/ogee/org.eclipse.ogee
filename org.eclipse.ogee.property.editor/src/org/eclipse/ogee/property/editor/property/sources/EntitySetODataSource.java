/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.type.handler.AbstractTypeHandler;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;

public class EntitySetODataSource extends AbstractPropertySourceArtifact {
	protected EntitySet entitySet;

	public EntitySetODataSource(EntitySet entitySet, boolean isReadOnly) {
		super(isReadOnly);
		this.entitySet = entitySet;

		descriptorIds = new DescriptorId[] { DescriptorId.NAME,
				DescriptorId.TYPE, DescriptorId.DOCUMENTATION };
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, entitySet.getName());
		String type = entitySet.getType() != null ? SchemaHandler
				.getNamespaceOrAlias(entitySet.getType())
				+ AbstractTypeHandler.NAMESPACE_SEPARATOR
				+ entitySet.getType().getName() : Messages.EMPTY_String;
		descriptorValueMap.put(DescriptorId.TYPE, type);
	}

	@Override
	protected EObject getEObject() {
		return entitySet;
	}
}