/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class EntityContainerODataSource extends AbstractPropertySourceArtifact {

	protected EntityContainer entityContainer;
	private SchemaHandler schemaHandler;

	public EntityContainerODataSource(EntityContainer entityContainer,
			boolean isReadOnly) {
		super(isReadOnly);
		this.entityContainer = entityContainer;
		descriptorIds = new DescriptorId[] { DescriptorId.NAME,
				DescriptorId.ISDEFAULT, DescriptorId.DOCUMENTATION,
				DescriptorId.SCHEMA };
		schemaHandler = new SchemaHandler();
	}

	@Override
	protected String[] getComboBoxValues(DescriptorId id) {
		if (id == DescriptorId.SCHEMA)
			return schemaHandler.getAllSchemata(getEObject());
		return Utilities.boolValues;
	}

	@Override
	protected EObject getEObject() {
		return entityContainer;
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, entityContainer.getName());
		descriptorValueMap.put(DescriptorId.ISDEFAULT,
				entityContainer.isDefault());
		descriptorValueMap.put(DescriptorId.SCHEMA, schemaHandler
				.getInitialDescriptorValue(isReadOnly, getEObject()));
	}

	@Override
	protected Object getAdditionalValue(DescriptorId descriptorId, Object object) {
		if (descriptorId == DescriptorId.SCHEMA) {
			if (object == null)
				return SchemaHandler.getSchema(getEObject());
			return schemaHandler.getSchema((Integer) object);
		}
		return null;
	}

}