/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.property.sources;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.property.editor.commands.TransactionAssociationSetChangeCommand;
import org.eclipse.ogee.property.editor.commands.TransactionChangeCommandAbstract;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.property.descriptor.DescriptorId;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ogee.property.editor.ui.elements.dialog.IReturnValue;
import org.eclipse.ogee.property.editor.utilities.RefConstraintHandler;
import org.eclipse.ogee.property.editor.utilities.Utilities;

public class AssociationODataSource extends AbstractPropertySourceArtifact {
	protected Association association;
	private SchemaHandler schemaHandler;

	public AssociationODataSource(Association association, boolean isReadOnly) {
		super(isReadOnly);
		this.association = association;
		// null check to prevent null pointer exceptions when sometimes
		// associations or any object are not populated
		if (null != association) {
			if (Utilities.isManyToMany(association)) {
				descriptorIds = new DescriptorId[] { DescriptorId.NAME,
						DescriptorId.END1, DescriptorId.END2,
						DescriptorId.DOCUMENTATION, DescriptorId.SCHEMA,
						DescriptorId.ASSOCIATIONSET };
			} else {
				descriptorIds = new DescriptorId[] { DescriptorId.NAME,
						DescriptorId.END1, DescriptorId.END2,
						DescriptorId.DOCUMENTATION, DescriptorId.SCHEMA,
						DescriptorId.REFERENTIAL_CONSTRAINT,
						DescriptorId.ASSOCIATIONSET };
			}

			schemaHandler = new SchemaHandler();
		}
	}

	@Override
	protected void initializeDescriptorsValuesMap() {
		descriptorValueMap.put(DescriptorId.NAME, association.getName());
		descriptorValueMap.put(DescriptorId.END1, association.getEnds().get(0));
		descriptorValueMap.put(DescriptorId.END2, association.getEnds().get(1));

		ReferentialConstraint rConstraint = (ReferentialConstraint) association
				.getReferentialConstraint();

		String message;
		if (rConstraint == null) {
			message = Messages.EMPTY_String;
		} else {
			message = Utilities.createRCValueString(rConstraint.getPrincipal()
					.getName(), rConstraint.getDependent().getName());
		}

		descriptorValueMap.put(DescriptorId.REFERENTIAL_CONSTRAINT, message);
		descriptorValueMap.put(DescriptorId.ASSOCIATIONSET,
				Utilities.createAssociationSetString(association));
		descriptorValueMap.put(DescriptorId.SCHEMA, schemaHandler
				.getInitialDescriptorValue(isReadOnly, getEObject()));
	}

	@Override
	protected String[] getComboBoxValues(DescriptorId id) {
		if (id == DescriptorId.SCHEMA)
			return schemaHandler.getAllSchemata(getEObject());
		return null;
	}

	@Override
	protected EObject getEObject() {
		return association;
	}

	@Override
	protected Object getAdditionalValue(DescriptorId descriptorId, Object object) {
		if (descriptorId == DescriptorId.SCHEMA) {
			if (object == null)
				return SchemaHandler.getSchema(getEObject());
			return schemaHandler.getSchema((Integer) object);
		}
		return createReferentialConstraint((RefConstraintHandler) object);
	}

	@Override
	public void setPropertyValue(Object id, Object value) {
		if (id == DescriptorId.ASSOCIATIONSET) {
			if (((IReturnValue) value).getReturnStatus() != org.eclipse.jface.window.Window.CANCEL) {
				TransactionChangeCommandAbstract transactionChangeCommandAssociationSet = new TransactionAssociationSetChangeCommand(
						id, value, association);
				Utilities
						.executeTransaction(transactionChangeCommandAssociationSet);
			}
		} else {
			super.setPropertyValue(id, value);
		}
	}

	private ReferentialConstraint createReferentialConstraint(
			RefConstraintHandler newRefConstraint) {
		if (newRefConstraint.getDependent() == null) {
			return null;
		}

		ReferentialConstraint rConstraint = OdataFactory.eINSTANCE
				.createReferentialConstraint();

		if (association.getEnds().get(0).getType()
				.equals(newRefConstraint.getPrincipal())) {
			rConstraint.setPrincipal(association.getEnds().get(0));
			rConstraint.setDependent(association.getEnds().get(1));
		} else {
			rConstraint.setPrincipal(association.getEnds().get(1));
			rConstraint.setDependent(association.getEnds().get(0));
		}

		rConstraint.getKeyMappings().putAll(newRefConstraint.createMapping());

		return rConstraint;
	}

}
