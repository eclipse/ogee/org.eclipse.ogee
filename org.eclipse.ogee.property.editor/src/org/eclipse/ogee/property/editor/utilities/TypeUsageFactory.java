/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.utilities;

import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.IParameterTypeUsage;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;

public class TypeUsageFactory {

	public static SimpleTypeUsage createSimpleTypeUsage(EDMTypes edmType) {
		SimpleType simpleType = OdataFactory.eINSTANCE.createSimpleType();
		simpleType.setType(edmType);

		SimpleTypeUsage simpleTypeUsage = OdataFactory.eINSTANCE
				.createSimpleTypeUsage();
		simpleTypeUsage.setSimpleType(simpleType);

		return simpleTypeUsage;
	}

	public static ComplexTypeUsage createComplexTypeUsage(
			ComplexType complexType) {
		ComplexTypeUsage complexTypeUsage = OdataFactory.eINSTANCE
				.createComplexTypeUsage();
		complexTypeUsage.setComplexType(complexType);

		return complexTypeUsage;
	}

	public static EnumTypeUsage createEnumTypeUsage(EnumType enumType) {

		EnumTypeUsage enumTypeUsage = OdataFactory.eINSTANCE
				.createEnumTypeUsage();
		enumTypeUsage.setEnumType(enumType);

		return enumTypeUsage;
	}

	public static IParameterTypeUsage createEntityTypeUsage(
			EntityType entityType) {
		EntityTypeUsage entityTypeUsage = OdataFactory.eINSTANCE
				.createEntityTypeUsage();
		entityTypeUsage.setEntityType(entityType);

		return entityTypeUsage;
	}

	public static ReturnEntityTypeUsage createEntityTypeReturnUsage(
			EntityType entityType) {
		ReturnEntityTypeUsage entityTypeUsage = OdataFactory.eINSTANCE
				.createReturnEntityTypeUsage();
		entityTypeUsage.setEntityType(entityType);

		return entityTypeUsage;
	}
}
