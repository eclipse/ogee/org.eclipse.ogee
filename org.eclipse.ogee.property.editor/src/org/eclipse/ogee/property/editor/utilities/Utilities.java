/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.utilities;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.TransactionalCommandStack;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.platform.GraphitiConnectionEditPart;
import org.eclipse.graphiti.ui.platform.GraphitiShapeEditPart;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.Multiplicity;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.adapter.ODataDomainModelChangeListener;
import org.eclipse.ogee.property.editor.commands.TransactionChangeCommandAbstract;
import org.eclipse.ogee.property.editor.nls.messages.Constants;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.ui.elements.OpenVocabulariesDialogAction;
import org.eclipse.ogee.utils.logger.Logger;

public class Utilities {

	private static final String ORG_ECLIPSE_OGEE_PROPERTY_EDITOR_VOCABULARY = "org.eclipse.ogee.property.editor.vocabulary"; //$NON-NLS-1$
	public static Action vocabularyButton = getAction();
	private static IStatusLineManager statusLineManager;
	private static ResourceSetListener resourceListener = new ODataDomainModelChangeListener();
	public static final String[] boolValues = { Boolean.FALSE.toString(),
			Boolean.TRUE.toString() };

	private static Action getAction() {
		OpenVocabulariesDialogAction openVocabulariesDialog = new OpenVocabulariesDialogAction(
				Messages.VocabularyButton_Title);
		openVocabulariesDialog
				.setToolTipText(Messages.VocabularyButton_ToolTip);
		openVocabulariesDialog.setImageDescriptor(Activator.IMG_VOCABULARY);
		openVocabulariesDialog.setId(Constants.ACTION_BAR_BUTTON_ID);
		openVocabulariesDialog
				.setActionDefinitionId(ORG_ECLIPSE_OGEE_PROPERTY_EDITOR_VOCABULARY);
		return openVocabulariesDialog;
	}

	/**
	 * 
	 * This method is responsible to return an object which the tabbed property
	 * know to handle: domain object (for selected shape in graphical editor or
	 * in navigation tree) IServiceDetails (for service catalog).
	 * 
	 * GraphitiShapeEditPart--> Marker class for all shape edit parts.
	 * GraphitiConnectionEditPart--> Marker class for all connection edit parts.
	 * 
	 * @param obj
	 *            selected object
	 * @return the relevant object
	 */

	public static Object getObject(Object obj) {
		if (obj instanceof GraphitiShapeEditPart) {
			GraphitiShapeEditPart shapeEditpart = (GraphitiShapeEditPart) obj;
			return Graphiti.getLinkService()
					.getBusinessObjectForLinkedPictogramElement(
							shapeEditpart.getPictogramElement());
		}
		if (obj instanceof GraphitiConnectionEditPart) {
			GraphitiConnectionEditPart connectionEditpart = (GraphitiConnectionEditPart) obj;
			return Graphiti.getLinkService()
					.getBusinessObjectForLinkedPictogramElement(
							connectionEditpart.getPictogramElement());
		}
		if (obj instanceof PictogramElement) {
			PictogramElement pe = (PictogramElement) obj;
			return Graphiti.getLinkService()
					.getBusinessObjectForLinkedPictogramElement(pe);
		}

		return obj;

	}

	public static void executeTransaction(
			final TransactionChangeCommandAbstract transactionChangeCommand) {
		TransactionalEditingDomain domain = null;
		TransactionalCommandStack stack = null;

		try {
			domain = IModelContext.INSTANCE
					.getTransaction(transactionChangeCommand.getEObject());
			domain.addResourceSetListener(resourceListener);
			stack = (TransactionalCommandStack) domain.getCommandStack();
		} catch (ModelAPIException e) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(
					e.getLocalizedMessage());
			return;
		}

		stack.execute(new RecordingCommand(domain) {
			@Override
			protected void doExecute() {
				transactionChangeCommand.doExecute();
			}
		});

	}

	public static String createRCValueString(String principal, String dependent) {
		return String.format(Messages.Utilities_RCStringFormat, principal,
				dependent);
	}

	public static String createAnnotationCollectionString(int numberOfCollection) {
		return String.format(
				Messages.Utilities_AnnotationCollectionStringFormat,
				numberOfCollection);
	}

	public static String createAssociationSetString(Association association) {
		int count = 0;

		if (association != null && association.getAssociationSets() != null)
			count = association.getAssociationSets().size();

		return String.format(Messages.Utilities_AssociationSetStringFotmat,
				count);
	}

	public static String[] createMultiplicityArray() {
		String[] multiplicityArray = new String[Multiplicity.VALUES.size()];

		for (Multiplicity type : Multiplicity.values()) {
			multiplicityArray[type.getValue()] = type.getLiteral();
		}
		return multiplicityArray;
	}

	public static void updateStatusLineManager(String message) {
		if (Utilities.statusLineManager == null)
			return;
		Utilities.statusLineManager.setErrorMessage(message);
		Utilities.statusLineManager.update(true);
	}

	public static void setStatusLineManager(IStatusLineManager statusLineManager) {
		Utilities.statusLineManager = statusLineManager;
	}

	public static boolean isManyToMany(Association association) {
		return association.getEnds().get(0).getMultiplicity()
				.equals(Multiplicity.MANY)
				&& association.getEnds().get(1).getMultiplicity()
						.equals(Multiplicity.MANY);
	}
}
