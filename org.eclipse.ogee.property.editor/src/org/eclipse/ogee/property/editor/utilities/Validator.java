/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.utilities;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.eclipse.ogee.client.parser.impl.util.DateUtils;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.util.DateTime;
import org.eclipse.ogee.model.odata.util.DateTimeOffset;
import org.eclipse.ogee.property.editor.nls.messages.Messages;

public class Validator {
	private static final String nonSpclCharacter = "[^a-zA-Z_0-9]"; //$NON-NLS-1$
	private static final String onlyCharacter = "[^a-z]"; //$NON-NLS-1$
	private static final String onlyCharacterAlias = "[^a-z_]"; //$NON-NLS-1$
	public static final String[] FORMAT_TIME_LIST = { "HH:mm:ssZ", "HH:mm:ss" }; //$NON-NLS-1$ //$NON-NLS-2$
	private static final int maximumLengthOfSimpleIdentifier = 128;
	private static final int maximumLengthOfNameSpace = 511;
	private static final String onlyCharacterNamespace = "[a-zA-Z_]"; //$NON-NLS-1$
	private static final String nonSpclCharacterNamespace = "\\w+"; //$NON-NLS-1$
	private static final String system = "system"; //$NON-NLS-1$
	private static final String odata = "odata"; //$NON-NLS-1$
	private static final String transientkey = "transient"; //$NON-NLS-1$
	private static final String edm = "edm"; //$NON-NLS-1$

	public static String validate(String value, EDMTypes edmType) {
		if (TypeConverter.isNullOrEmpty(value))
			return null;

		switch (edmType) {
		case SBYTE:
		case BYTE:
			return validateByte(value);
		case DECIMAL:// BigDecimal validation is equal to DOUBLE
		case DOUBLE:
			return validateDouble(value);
		case GUID:
			return validateGUID(value);
		case INT16:
			return validateShort(value);
		case INT32:
			return validateInteger(value);
		case INT64:
			return validateLong(value);
		case SINGLE:
			return validateFloat(value);
		case STRING:
			return validateString(value);
		case TIME:
			return validateTime(value);
		case DATE_TIME:
			return validateDateTime(value);
		case DATE_TIME_OFFSET:
			return validateDateTimeOffset(value);
		default:
			return null;
		}
	}

	public static String validateString(String name) {
		String errorMsg = null;
		if (name.length() > 128) {
			errorMsg = Messages.Validator_Exceed_Length;
		}

		return errorMsg;

	}

	/**
	 * @param name
	 * @return the error message if the validation of the entity name fails
	 */
	public static String validateName(String name) {
		String errorMsg = null;
		boolean b = false;
		boolean b1 = false;

		Pattern nonSpclChar = Pattern.compile(nonSpclCharacter,
				Pattern.CASE_INSENSITIVE);
		Matcher m = nonSpclChar.matcher(name);
		b = m.find();

		Pattern onlyChar = Pattern.compile(onlyCharacter,
				Pattern.CASE_INSENSITIVE);
		if (name.trim().length() > 0) {
			Matcher m1 = onlyChar.matcher(name.substring(0, 1));
			b1 = m1.find();
		}
		if (name.trim().length() < 1) {
			errorMsg = Messages.validation_name_empty;
		}
		if (b) {
			errorMsg = Messages.validation_no_special_char;

		} else if (b1) {
			errorMsg = Messages.validation_first_char;
		} else if (name.length() > 128) {
			errorMsg = Messages.Validator_Exceed_Length;
		}

		return errorMsg;
	}

	/**
	 * @param name
	 * @return the error message if the validation of the alias name fails
	 */
	public static String validateAliasName(String name) {
		String errorMsg = null;
		boolean b = false;
		boolean b1 = false;

		Pattern nonSpclChar = Pattern.compile(nonSpclCharacter,
				Pattern.CASE_INSENSITIVE);
		Matcher m = nonSpclChar.matcher(name);
		b = m.find();

		Pattern onlyChar = Pattern.compile(onlyCharacterAlias,
				Pattern.CASE_INSENSITIVE);
		if (name.trim().length() > 0) {
			Matcher m1 = onlyChar.matcher(name.substring(0, 1));
			b1 = m1.find();
		}
		if (b) {
			errorMsg = Messages.validation_no_special_char;

		} else if (b1) {
			errorMsg = Messages.validation_first_char;
		} else if (name.length() > 128) {
			errorMsg = Messages.Validator_Exceed_Length;
		}
		if (name.equalsIgnoreCase(system)
				|| name.equalsIgnoreCase(transientkey)
				|| name.equalsIgnoreCase(odata) || name.equalsIgnoreCase(edm)) {
			errorMsg = Messages.validation_name_keywords;
		}

		return errorMsg;
	}

	/**
	 * @param name
	 * @return the error message if the validation of the namespace name fails
	 */
	public static String validateNamespaceName(String name) {
		String errorMsg = null;
		// System", "Transient",”odata” or "Edm".
		if (name.equalsIgnoreCase(system)
				|| name.equalsIgnoreCase(transientkey)
				|| name.equalsIgnoreCase(odata) || name.equalsIgnoreCase(edm)) {
			errorMsg = Messages.validation_name_keywords;
			return errorMsg;
		}
		if (name.trim().length() < 1) {
			errorMsg = Messages.validation_name_empty;
			return errorMsg;
		}
		if (name.length() > maximumLengthOfNameSpace) {
			errorMsg = Messages.Validator_Exceed_Length_Namespace;
			return errorMsg;
		}
		if (containsInvalidCharactersInNamespace(name)) {
			errorMsg = Messages.Validator_Invalid_Characters_Namespace;
		}
		String[] subStrings = name.split("\\.");
		for (String str : subStrings) {
			boolean statusLength = checkValidNamespacePartNameLength(str);
			if (!statusLength) {
				errorMsg = Messages.Validator_Exceed_Length_Namespace_PartName;
				break;
			}
			boolean status = checkValidNamespacePartName(str);
			if (!status) {
				errorMsg = Messages.Validator_Invalid_PartName_Namespace;
				break;
			}
		}
		return errorMsg;
	}

	public static boolean checkValidNamespacePartNameLength(String name) {
		if (hasInvalidLength(name)) {
			return false;
		}
		return true;
	}

	public static boolean checkValidNamespacePartName(String name) {
		if (containsInvalidCharacters(name)) {
			return false;
		}

		return true;
	}

	protected static boolean hasInvalidLength(String name) {
		if (name != null && name.length() > maximumLengthOfSimpleIdentifier) {
			return true;
		}
		return false;
	}

	protected static boolean containsInvalidCharacters(String name) {
		boolean noSpecialCharacters = false;
		boolean firstLetterOnlyCharacters = false;

		if (name == null) {
			return false;
		}

		noSpecialCharacters = Pattern.matches(nonSpclCharacterNamespace, name);

		if (name.trim().length() > 0) {
			firstLetterOnlyCharacters = Pattern.matches(onlyCharacterNamespace,
					name.substring(0, 1));
		}
		return (!noSpecialCharacters || !firstLetterOnlyCharacters);
	}

	protected static boolean containsInvalidCharactersInNamespace(String name) {
		boolean firstLetterOnlyCharacters = false;
		if (name == null) {
			return false;
		}

		if (name.trim().length() > 0) {
			firstLetterOnlyCharacters = Pattern.matches(onlyCharacterNamespace,
					name.substring(0, 1));
		}
		return (!firstLetterOnlyCharacters);
	}

	public static String validateNumber(String nString, Number[] number) {
		if (TypeConverter.isNullOrEmpty(nString))
			return null;

		ParsePosition pos = new ParsePosition(0);

		number[0] = NumberFormat.getInstance(Locale.getDefault()).parse(
				nString, pos);
		if (pos.getIndex() != nString.length()) {
			return String.format(Messages.Validator_ParseError1,
					pos.getIndex() + 1);
		}

		return null;
	}

	public static String validateLong(String nString) {
		String errorMsg = validateNumber(nString, new Number[1]);

		if (errorMsg != null)
			return errorMsg;

		int maxLength = (nString.contains(",") == true) ? 26 : 20; //Max value with or without commas -9,223,372,036,854,775,808 //$NON-NLS-1$

		if (nString.length() > maxLength)
			errorMsg = Messages.validation_out_of_boundary;

		return errorMsg;
	}

	/**
	 * @param name
	 * @return the error message if the number string is not a valid integer
	 */
	public static String validateInteger(String nString) {
		Number[] number = new Number[] { null };
		String errorMsg = validateNumber(nString, number);
		long l;

		if (errorMsg != null || number[0] == null)
			return errorMsg;

		l = number[0].longValue();

		if (l > Integer.MAX_VALUE || l < Integer.MIN_VALUE)
			errorMsg = Messages.validation_out_of_boundary;

		return errorMsg;
	}

	public static String validateDecimalNumber(String nString) {
		Number[] number = new Number[] { null };
		return validateNumber(nString, number);
	}

	public static String validateDouble(String value) {
		try {
			Double.parseDouble(value);
		} catch (NumberFormatException e) {
			return validateDecimalNumber(value);
		}

		return null;
	}

	public static String validateFloat(String value) {
		try {
			Float.parseFloat(value);
		} catch (NumberFormatException e) {
			return validateDecimalNumber(value);
		}

		return null;
	}

	public static String validateShort(String nString) {
		Number[] number = new Number[] { null };
		String errorMsg = validateNumber(nString, number);
		long l;

		if (errorMsg != null || number[0] == null)
			return errorMsg;

		l = number[0].longValue();

		if (l > Short.MAX_VALUE || l < Short.MIN_VALUE)
			errorMsg = Messages.validation_out_of_boundary;

		return errorMsg;
	}

	public static String validateByte(String nString) {
		Number[] number = new Number[] { null };
		String errorMsg = validateNumber(nString, number);
		long l;

		if (errorMsg != null || number[0] == null)
			return errorMsg;

		l = number[0].longValue();

		if (l > Byte.MAX_VALUE || l < Byte.MIN_VALUE)
			errorMsg = Messages.validation_out_of_boundary;

		return errorMsg;
	}

	public static String validateTime(String value) {
		if (TypeConverter.isNullOrEmpty(value))
			return null;

		try {
			DatatypeConverter.parseTime(value);
			return null;
		} catch (IllegalArgumentException e) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
					FORMAT_TIME_LIST[1]);
			return String.format(Messages.Validator_3,
					simpleDateFormat.format(new Date()));
		}

	}

	public static String validateDateTime(String value) {
		if (TypeConverter.isNullOrEmpty(value))
			return null;

		try {
			DateTime.fromXMLFormat(value);
			if (DateUtils.parse(value) == null)
				throw new IllegalArgumentException();
		} catch (IllegalArgumentException e) {
			DateTime dateTime = new DateTime(new Date());
			return String.format(Messages.Validator_3, dateTime.toXMLFormat());
		}

		return null;
	}

	public static String validateDateTimeOffset(String value) {
		if (TypeConverter.isNullOrEmpty(value))
			return null;

		try {
			DateTimeOffset.fromXMLFormat(value);
			if (DateUtils.parse(value) == null)
				throw new IllegalArgumentException();
		} catch (IllegalArgumentException e) {
			DateTimeOffset dateTimeOffset = new DateTimeOffset(new Date(),
					Calendar.getInstance().getTimeZone());
			return String.format(Messages.Validator_3,
					dateTimeOffset.toXMLFormat());
		}

		return null;
	}

	public static String validateGUID(String value) {
		try {
			java.util.UUID.fromString(value);
		} catch (IllegalArgumentException e) {
			return Messages.Validator_UUID;
		}

		return null;
	}

}
