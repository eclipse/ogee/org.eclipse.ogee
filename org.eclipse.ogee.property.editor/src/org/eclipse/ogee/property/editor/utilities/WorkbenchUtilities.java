/*******************************************************************************
 * Copyright (c) 2012-2014 SAP SE. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: SAP SE - initial API and implementation and/or initial
 * documentation
 * 
 *******************************************************************************/
package org.eclipse.ogee.property.editor.utilities;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.ui.editor.DiagramEditorInput;
import org.eclipse.ogee.designer.ODataEditorInput;
import org.eclipse.ogee.designer.ODataMultiPageEditor;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.property.editor.nls.messages.Constants;
import org.eclipse.ogee.property.editor.type.handler.SchemaHandler;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.IPage;
import org.eclipse.ui.views.properties.PropertySheet;
import org.eclipse.ui.views.properties.tabbed.ITabDescriptor;
import org.eclipse.ui.views.properties.tabbed.TabContents;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

public class WorkbenchUtilities {

	private static boolean previousReadOnlyStatus = false;

	private static IWorkbenchPage getActivePage() {
		IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		if (activeWorkbenchWindow == null) {
			return null;
		}
		return activeWorkbenchWindow.getActivePage();
	}

	public static String getCurrentTabId() {
		IWorkbenchPage activePage = getActivePage();
		IViewPart view = activePage.findView(Constants.PROPERTY_SHEET_ID);
		if (view == null) {
			return null;
		}
		IPage currentPage = ((PropertySheet) view).getCurrentPage();
		if (!(currentPage instanceof TabbedPropertySheetPage)) {
			return null;
		}
		ITabDescriptor currentTab = ((TabbedPropertySheetPage) currentPage)
				.getSelectedTab();
		if (currentTab == null) {
			return null;
		}

		return currentTab.getId();
	}

	public static String getActivePartId() {
		IWorkbenchPage activePage = getActivePage();
		if (activePage == null || activePage.getActivePartReference() == null
				|| activePage.getActivePartReference().getId() == null) {
			return null;
		}
		return activePage.getActivePartReference().getId();
	}

	public static boolean isEditorOpen(String editorId) {
		IEditorReference[] editorReferences = getActivePage()
				.getEditorReferences();
		for (IEditorReference iEditorReference : editorReferences) {
			if (iEditorReference.getId().equals(editorId)) {
				return true;
			}
		}
		return false;
	}

	public static void refreshAndSetAnnotationsTab() {
		IViewPart view = getActivePage().findView(Constants.PROPERTY_SHEET_ID);
		if (view != null && view instanceof PropertySheet) {
			IPage currentPage = ((PropertySheet) view).getCurrentPage();
			if (currentPage instanceof TabbedPropertySheetPage) {
				((TabbedPropertySheetPage) currentPage)
						.setSelectedTab(Constants.VOCABULARIES_TAB_ID);
				TabContents currentTab = ((TabbedPropertySheetPage) currentPage)
						.getCurrentTab();
				if (currentTab != null) {
					currentTab.refresh();
				}
			}
		}
	}

	@SuppressWarnings("restriction")
	public static boolean isReadOnlyMode(EObject obj) {
		String activePartId = getActivePartId();
		if (activePartId == null) {// there is no active part
			return true;
		}
		if (Constants.PROPERTY_SHEET_ID.equals(activePartId)) {
			// in case of changing section in the property sheet, we need to
			// know from where we have started
			return previousReadOnlyStatus;
		}

		Schema schema = SchemaHandler.getSchema(obj);

		boolean readonly = false;
		/*
		 * if (Constants.SERVICE_CATALOG_ID.equals(activePartId)) { // the
		 * active part is service catalog readonly = true; } else
		 */if (schema != null
				&& !SchemaHandler.getSchemata(schema).contains(schema)) {
			// the selected artifact is from referenced schema
			readonly = true;
		} else if (isEditorOpen(Constants.EDITOR_ID)) {
			// the readOnlyMode is set by graphiti editor
			// readonly = IODataDiagramCreator.INSTANCE.isReadOnlyModel();
			final IEditorPart editor = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.getActiveEditor();
			if (editor instanceof ODataMultiPageEditor) {
				final IEditorInput editorInput = editor.getEditorInput();
				// Editor is invoked from IODataDiagramCreator#createDiagram()
				// API.
				if (editorInput instanceof ODataEditorInput) {

					final URI uri = ((ODataEditorInput) editorInput).getUri();
					final String uriString = uri.trimFragment()
							.toPlatformString(true);
					IFile file = ResourcesPlugin.getWorkspace().getRoot()
							.getFile(new Path(uriString));
					if (file.isReadOnly()) {
						readonly = true;
					} else {
						readonly = ((ODataEditorInput) editorInput)
								.getDiagramInput().isReadOnlyMode();
					}
				}
				// Editor is invoked from Project Explorer.
				else if (editorInput instanceof IFileEditorInput) {
					IFile file = ((IFileEditorInput) editorInput).getFile();
					if (file.isReadOnly()) {
						readonly = true;
					} else {
						readonly = ((IFileEditorInput) editorInput).getFile()
								.isReadOnly();
					}
				}
				// Editor is invoked when eclipse IDE is launched.
				else if (editorInput instanceof DiagramEditorInput) {
					final URI uri = ((DiagramEditorInput) editorInput).getUri();
					final String uriString = uri.trimFragment()
							.toPlatformString(true);
					IFile file = ResourcesPlugin.getWorkspace().getRoot()
							.getFile(new Path(uriString));
					if (file.isReadOnly()) {
						readonly = true;
					} else {
						readonly = file.isReadOnly();
					}
				}
			}

		}
		previousReadOnlyStatus = readonly;
		return readonly;

	}

}
