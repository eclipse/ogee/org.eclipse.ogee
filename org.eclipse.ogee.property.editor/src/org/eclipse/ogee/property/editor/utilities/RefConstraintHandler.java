/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.property.editor.ui.elements.dialog.IReturnValue;

public class RefConstraintHandler implements IReturnValue {
	private HashMap<String, String> refConstraintMap;
	private EntityType dependent;
	private EntityType principle;
	private Association association;
	private int status;

	public RefConstraintHandler(HashMap<String, String> refConstraintMap,
			EntityType dependent, EntityType principle, Association association) {
		this.dependent = dependent;
		this.principle = principle;
		this.refConstraintMap = refConstraintMap;
		this.association = association;
	}

	public EntityType getDependent() {
		return dependent;
	}

	public EntityType getPrincipal() {
		return principle;
	}

	public Map<Property, Property> createMapping() {
		List<Property> principleKeys = getProperties(true);
		List<Property> dependentProperties = new LinkedList<Property>(
				getProperties(false));

		Map<String, Property> principleKeysMap = new HashMap<String, Property>();
		for (Property property : principleKeys) {
			principleKeysMap.put(property.getName(), property);
		}

		Map<String, Property> dependentKeysMap = new HashMap<String, Property>();
		for (Property property : dependentProperties) {
			dependentKeysMap.put(property.getName(), property);
		}

		Map<Property, Property> refConsMapping = new HashMap<Property, Property>();

		for (String principleKey : refConstraintMap.keySet()) {
			refConsMapping.put(principleKeysMap.get(principleKey),
					dependentKeysMap.get(refConstraintMap.get(principleKey)));
		}

		return refConsMapping;
	}

	@Override
	public String getText() {
		if (principle == null || dependent == null) {
			return Messages.EMPTY_String;
		}
		if (association.getEnds().get(0).getType().equals(principle)) {
			return Utilities.createRCValueString(association.getEnds().get(0)
					.getName(), association.getEnds().get(1).getName());
		}
		return Utilities.createRCValueString(association.getEnds().get(1)
				.getName(), association.getEnds().get(0).getName());

	}

	@Override
	public int getReturnStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	private List<Property> getProperties(boolean isPrinciple) {
		EntityType entity = dependent;
		if (isPrinciple) {
			entity = principle;
		}
		List<Property> keys = new ArrayList<Property>(entity.getKeys());
		List<Property> properties = new ArrayList<Property>(
				entity.getProperties());

		EntityType baseEntity = entity.getBaseType();
		while (baseEntity != null) {
			keys.addAll(baseEntity.getKeys());
			properties.addAll(baseEntity.getProperties());
			baseEntity = baseEntity.getBaseType();
		}

		if (isPrinciple) {
			return keys;
		}
		properties.addAll(keys);
		return properties;
	}

}
