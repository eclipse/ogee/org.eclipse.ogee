/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.property.editor.utilities;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import javax.xml.bind.DatatypeConverter;

import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.util.DateTime;
import org.eclipse.ogee.model.odata.util.DateTimeOffset;
import org.eclipse.ogee.property.editor.adapter.Activator;
import org.eclipse.ogee.property.editor.nls.messages.Messages;
import org.eclipse.ogee.utils.logger.Logger;

public class TypeConverter {

	public static Object getValue(Object value, EDMTypes type) {
		switch (type) {
		case BOOLEAN:
			if (value instanceof Integer)
				return ((Integer) value).equals(1);
			break;
		case BYTE:
			return getByteValue(value);
		case DATE_TIME:
			return getDateTimeValue(value);
		case DATE_TIME_OFFSET:
			return getDateTimeOffsetValue(value);
		case TIME:
			return getTimeValue(value);
		case DECIMAL:
			return getBigDecimalValue(value);
		case DOUBLE:
			return getDoubleValue(value);
		case INT16:
			return getShortValue(value);
		case INT32:
			return getIntegerValue(value);
		case INT64:
			return getLongValue(value);
		case SBYTE:
			return getByteValue(value);
		case SINGLE:
			return getFloatValue(value);
		case GUID:
			return getGUIDValue(value);
		default:
			return value;
		}
		return null;
	}

	public static UUID getGUIDValue(Object value) {

		if (isNullOrEmpty(value))
			return null;

		return java.util.UUID.fromString(value.toString());
	}

	public static Integer getIntegerValue(Object value) {
		if (isNullOrEmpty(value))
			return null;

		Number number = parseStringToNumber(value.toString());
		if (number == null)
			return null;

		return number.intValue();
	}

	public static Byte getByteValue(Object value) {
		if (isNullOrEmpty(value))
			return null;

		return parseStringToNumber(value.toString()).byteValue();
	}

	public static Double getDoubleValue(Object value) {
		if (isNullOrEmpty(value))
			return null;

		return parseStringToNumber(value.toString()).doubleValue();
	}

	public static Float getFloatValue(Object value) {
		if (isNullOrEmpty(value))
			return null;

		return parseStringToNumber(value.toString()).floatValue();
	}

	public static Short getShortValue(Object value) {
		if (isNullOrEmpty(value))
			return null;

		return parseStringToNumber(value.toString()).shortValue();
	}

	public static Long getLongValue(Object value) {
		if (isNullOrEmpty(value))
			return null;
		return parseStringToNumber(value.toString()).longValue();
	}

	public static BigDecimal getBigDecimalValue(Object value) {
		if (isNullOrEmpty(value))
			return null;

		return BigDecimal.valueOf(parseStringToNumber(value.toString())
				.doubleValue());
	}

	public static Object getTimeValue(Object value) {
		if (isNullOrEmpty(value))
			return null;

		return DateTime.fromXMLFormat(value.toString()).getDateValue();
	}

	public static Object getDateTimeValue(Object value) {
		if (isNullOrEmpty(value))
			return null;

		return DateTime.fromXMLFormat(value.toString());
	}

	public static Object getDateTimeOffsetValue(Object value) {
		if (isNullOrEmpty(value))
			return null;

		return DateTimeOffset.fromXMLFormat(value.toString());
	}

	public static String formatNumberOrEmptyString(Object object) {
		if (isNullOrEmpty(object))
			return ""; //$NON-NLS-1$

		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale
				.getDefault());
		numberFormat.setMaximumFractionDigits(8);
		return numberFormat.format(object);
	}

	public static String formatTime(Object sDate) {
		if (sDate == null || !(sDate instanceof Date))
			return ""; //$NON-NLS-1$

		Calendar cal = Calendar.getInstance();
		cal.setTime((Date) sDate);
		cal.setTimeZone(java.util.TimeZone.getTimeZone("GMT")); //$NON-NLS-1$
		return DatatypeConverter.printTime(cal);
	}

	public static String formatDateTime(Object sDate) {
		if (sDate == null || !(sDate instanceof DateTime))
			return ""; //$NON-NLS-1$

		return ((DateTime) sDate).toXMLFormat();
	}

	public static String formatDateTimeOffset(Object sDate) {
		if (sDate == null || !(sDate instanceof DateTimeOffset))
			return ""; //$NON-NLS-1$

		return ((DateTimeOffset) sDate).toXMLFormat();
	}

	public static Number parseStringToNumber(String nString) {
		ParsePosition pos = new ParsePosition(0);
		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale
				.getDefault());
		numberFormat.setGroupingUsed(true);

		Number parsedNumber = numberFormat.parse(nString, pos);

		// pos is set AFTER the last parsed position
		if (pos.getIndex() != nString.length()) {
			Logger.getLogger(Activator.PLUGIN_ID).logError(
					String.format(Messages.TypeConverter_Parse_Error1, nString,
							pos.getIndex() + 1));
		}

		return parsedNumber;
	}

	public static boolean isNullOrEmpty(Object o) {
		return (o == null || o.toString().equals("")); //$NON-NLS-1$

	}
}