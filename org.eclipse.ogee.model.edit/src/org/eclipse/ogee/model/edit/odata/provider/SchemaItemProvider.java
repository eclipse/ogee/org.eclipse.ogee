/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.edit.odata.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.ogee.model.edit.OdataEditPlugin;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.OdataPackage;
import org.eclipse.ogee.model.odata.Schema;

/**
 * This is the item provider adapter for a {@link org.eclipse.ogee.model.odata.Schema} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SchemaItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SchemaItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamespacePropertyDescriptor(object);
			addAliasPropertyDescriptor(object);
			addClassifiersPropertyDescriptor(object);
			addDataServicesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Namespace feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamespacePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Schema_namespace_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Schema_namespace_feature", "_UI_Schema_type"),
				 OdataPackage.Literals.SCHEMA__NAMESPACE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Alias feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAliasPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Schema_alias_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Schema_alias_feature", "_UI_Schema_type"),
				 OdataPackage.Literals.SCHEMA__ALIAS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Classifiers feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClassifiersPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Schema_classifiers_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Schema_classifiers_feature", "_UI_Schema_type"),
				 OdataPackage.Literals.SCHEMA__CLASSIFIERS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Data Services feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDataServicesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Schema_dataServices_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Schema_dataServices_feature", "_UI_Schema_type"),
				 OdataPackage.Literals.SCHEMA__DATA_SERVICES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OdataPackage.Literals.IDOCUMENTABLE__DOCUMENTATION);
			childrenFeatures.add(OdataPackage.Literals.SCHEMA__CONTAINERS);
			childrenFeatures.add(OdataPackage.Literals.SCHEMA__ENTITY_TYPES);
			childrenFeatures.add(OdataPackage.Literals.SCHEMA__ASSOCIATIONS);
			childrenFeatures.add(OdataPackage.Literals.SCHEMA__ENUM_TYPES);
			childrenFeatures.add(OdataPackage.Literals.SCHEMA__COMPLEX_TYPES);
			childrenFeatures.add(OdataPackage.Literals.SCHEMA__USINGS);
			childrenFeatures.add(OdataPackage.Literals.SCHEMA__VALUE_TERMS);
			childrenFeatures.add(OdataPackage.Literals.SCHEMA__VALUE_ANNOTATIONS);
			childrenFeatures.add(OdataPackage.Literals.SCHEMA__TYPE_ANNOTATIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Schema.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/schema.png"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	@Override
	public String getText(Object object) {
		String label = ((Schema)object).getNamespace();
		return label == null || label.length() == 0 ?
			getString("_UI_Schema_type") :
			label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Schema.class)) {
			case OdataPackage.SCHEMA__NAMESPACE:
			case OdataPackage.SCHEMA__ALIAS:
			case OdataPackage.SCHEMA__CLASSIFIERS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case OdataPackage.SCHEMA__DOCUMENTATION:
			case OdataPackage.SCHEMA__CONTAINERS:
			case OdataPackage.SCHEMA__ENTITY_TYPES:
			case OdataPackage.SCHEMA__ASSOCIATIONS:
			case OdataPackage.SCHEMA__ENUM_TYPES:
			case OdataPackage.SCHEMA__COMPLEX_TYPES:
			case OdataPackage.SCHEMA__USINGS:
			case OdataPackage.SCHEMA__VALUE_TERMS:
			case OdataPackage.SCHEMA__VALUE_ANNOTATIONS:
			case OdataPackage.SCHEMA__TYPE_ANNOTATIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.IDOCUMENTABLE__DOCUMENTATION,
				 OdataFactory.eINSTANCE.createDocumentation()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.SCHEMA__CONTAINERS,
				 OdataFactory.eINSTANCE.createEntityContainer()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.SCHEMA__ENTITY_TYPES,
				 OdataFactory.eINSTANCE.createEntityType()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.SCHEMA__ASSOCIATIONS,
				 OdataFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.SCHEMA__ENUM_TYPES,
				 OdataFactory.eINSTANCE.createEnumType()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.SCHEMA__COMPLEX_TYPES,
				 OdataFactory.eINSTANCE.createComplexType()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.SCHEMA__USINGS,
				 OdataFactory.eINSTANCE.createUsing()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.SCHEMA__VALUE_TERMS,
				 OdataFactory.eINSTANCE.createValueTerm()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.SCHEMA__VALUE_ANNOTATIONS,
				 OdataFactory.eINSTANCE.createValueAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.SCHEMA__TYPE_ANNOTATIONS,
				 OdataFactory.eINSTANCE.createTypeAnnotation()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return OdataEditPlugin.INSTANCE;
	}

}
