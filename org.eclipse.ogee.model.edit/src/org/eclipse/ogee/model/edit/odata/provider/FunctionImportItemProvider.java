/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 */
package org.eclipse.ogee.model.edit.odata.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.ogee.model.edit.OdataEditPlugin;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.OdataPackage;

/**
 * This is the item provider adapter for a {@link org.eclipse.ogee.model.odata.FunctionImport} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionImportItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionImportItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAnnotationsPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addSideEffectingPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Annotations feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAnnotationsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IAnnotationTarget_annotations_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IAnnotationTarget_annotations_feature", "_UI_IAnnotationTarget_type"),
				 OdataPackage.Literals.IANNOTATION_TARGET__ANNOTATIONS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FunctionImport_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FunctionImport_name_feature", "_UI_FunctionImport_type"),
				 OdataPackage.Literals.FUNCTION_IMPORT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Side Effecting feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSideEffectingPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FunctionImport_sideEffecting_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FunctionImport_sideEffecting_feature", "_UI_FunctionImport_type"),
				 OdataPackage.Literals.FUNCTION_IMPORT__SIDE_EFFECTING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OdataPackage.Literals.IDOCUMENTABLE__DOCUMENTATION);
			childrenFeatures.add(OdataPackage.Literals.FUNCTION_IMPORT__PARAMETERS);
			childrenFeatures.add(OdataPackage.Literals.FUNCTION_IMPORT__BINDING);
			childrenFeatures.add(OdataPackage.Literals.FUNCTION_IMPORT__RETURN_TYPE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns FunctionImport.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/function.png"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	@Override
	public String getText(Object object) {
		String label = ((FunctionImport)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_FunctionImport_type") :
			label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FunctionImport.class)) {
			case OdataPackage.FUNCTION_IMPORT__NAME:
			case OdataPackage.FUNCTION_IMPORT__SIDE_EFFECTING:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case OdataPackage.FUNCTION_IMPORT__DOCUMENTATION:
			case OdataPackage.FUNCTION_IMPORT__PARAMETERS:
			case OdataPackage.FUNCTION_IMPORT__BINDING:
			case OdataPackage.FUNCTION_IMPORT__RETURN_TYPE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.IDOCUMENTABLE__DOCUMENTATION,
				 OdataFactory.eINSTANCE.createDocumentation()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.FUNCTION_IMPORT__PARAMETERS,
				 OdataFactory.eINSTANCE.createParameter()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.FUNCTION_IMPORT__BINDING,
				 OdataFactory.eINSTANCE.createBinding()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.FUNCTION_IMPORT__RETURN_TYPE,
				 OdataFactory.eINSTANCE.createComplexTypeUsage()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.FUNCTION_IMPORT__RETURN_TYPE,
				 OdataFactory.eINSTANCE.createEnumTypeUsage()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.FUNCTION_IMPORT__RETURN_TYPE,
				 OdataFactory.eINSTANCE.createSimpleTypeUsage()));

		newChildDescriptors.add
			(createChildParameter
				(OdataPackage.Literals.FUNCTION_IMPORT__RETURN_TYPE,
				 OdataFactory.eINSTANCE.createReturnEntityTypeUsage()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return OdataEditPlugin.INSTANCE;
	}

}
