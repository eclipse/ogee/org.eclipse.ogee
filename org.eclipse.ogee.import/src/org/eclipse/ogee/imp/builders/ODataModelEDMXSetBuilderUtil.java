/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.ogee.utils.service.validation.GenericServiceUrlValidator;
import org.eclipse.ogee.utils.service.validation.IServiceMetadataFileValidator;
import org.eclipse.ogee.utils.service.validation.IServiceUrlValidator;
import org.eclipse.ogee.utils.service.validation.Result;
import org.eclipse.ogee.utils.service.validation.ServiceValidator;

/**
 * Utility to build an OData Model EDMXSet object from URL or File path.
 */
public class ODataModelEDMXSetBuilderUtil {
	/**
	 * Builds an OData Model EDMXSet object from a URI.
	 * 
	 * @param uri
	 *            String
	 * @return EdmxSet
	 * @throws BuilderException
	 */
	public static EDMXSet buildFromUri(EDMXSet edmxSet, String uri,
			EDMX parentOdataEdmx) throws BuilderException {
		boolean valid = GenericServiceUrlValidator.isServiceUrlSyntaxValid(uri);

		if (valid)
			return buildFromUrl(edmxSet, uri, parentOdataEdmx);
		else
			return buildFromFilePath(edmxSet, uri, parentOdataEdmx);
	}

	/**
	 * Builds an OData Model EDMXSet object from a URL.
	 * 
	 * @param edmxSet
	 * @param url
	 *            String
	 * @return EdmxSet
	 * @throws BuilderException
	 */
	private static EDMXSet buildFromUrl(EDMXSet edmxSet, String url,
			EDMX parentOdataEdmx) throws BuilderException {
		IServiceUrlValidator serviceValidator = ServiceValidator
				.getServiceUrlValidator();

		Result validationResult = serviceValidator.validateSimple(url, null);

		return buildFromValidationResult(edmxSet, validationResult,
				parentOdataEdmx);
	}

	/**
	 * Builds an OData Model EDMXSet object from File path.
	 * 
	 * @param filePath
	 *            String
	 * @return EdmxSet
	 * @throws BuilderException
	 */
	private static EDMXSet buildFromFilePath(EDMXSet edmxSet, String filePath,
			EDMX parentOdataEdmx) throws BuilderException {
		IServiceMetadataFileValidator serviceFileValidator = ServiceValidator
				.getServiceMetadataValidator();

		Result validationResult = serviceFileValidator.validate(filePath, null);

		return buildFromValidationResult(edmxSet, validationResult,
				parentOdataEdmx);
	}

	private static EDMXSet buildFromValidationResult(EDMXSet edmxSet,
			Result validationResult, EDMX parentOdataEdmx)
			throws BuilderException {
		if (validationResult.getStatus() != Result.Status.OK) {
			Logger.getUtilsLogger().logError(validationResult.getMessage());
			Throwable cause = validationResult.getException();
			if (cause != null) {
				Logger.getUtilsLogger().logError(cause);
			}
		} else {
			Edmx edmx = validationResult.getEdmx1();

			String serviceUrl = validationResult.getServiceUrl();
			if (serviceUrl.isEmpty()) {
				serviceUrl = validationResult.getServiceMetadataUri();
			}

			ODataModelEDMXSetBuilder.updateEdmxSet(edmxSet, edmx,
					parentOdataEdmx, serviceUrl);
		}

		return edmxSet;
	}
}
