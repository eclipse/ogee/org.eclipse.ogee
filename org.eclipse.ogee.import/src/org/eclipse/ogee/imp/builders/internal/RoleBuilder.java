/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Multiplicity;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Role;

/**
 * Builds an OData Role object.
 */
public class RoleBuilder {
	/**
	 * Builds an OData Role object from the given Edmx End object.
	 * 
	 * @param edmxEnd
	 * @param objects
	 * @return - OData Role object
	 */
	public static Role build(org.eclipse.ogee.client.model.edmx.End edmxEnd,
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		// V4 support
		if (edmxEnd == null || edmxAssociation == null) {
			return null;
		}

		validateInputParameters(edmxEnd, edmxAssociation, namespace, edmx,
				objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		Role role = (Role) nsObject.get(edmxAssociation.toString() + edmxEnd);
		if (role == null) {
			// create role
			role = OdataFactory.eINSTANCE.createRole();
			// save role
			nsObject.put(edmxAssociation.toString() + edmxEnd, role);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxEnd
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				role.setDocumentation(documentation);
			}

			// set role name
			String roleName = edmxEnd.getRole();
			role.setName(roleName);

			// set multiplicity
			String edmxMultiplicity = edmxEnd.getMultiplicity();
			int intMultiplicity = 0;
			if (edmxMultiplicity.equals("*")) //$NON-NLS-1$
			{
				intMultiplicity = 2;
			} else if (edmxMultiplicity.equals("1")) //$NON-NLS-1$
			{
				intMultiplicity = 1;
			}

			Multiplicity multiplicity = Multiplicity.get(intMultiplicity);
			role.setMultiplicity(multiplicity);

			// set entity type
			String edmxTypeName = edmxEnd.getType();
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType = EdmxUtilities
					.getEntityTypeByName(edmxTypeName, edmx);
			String entityTypeNamespace = EdmxUtilities.getEntityTypeNamespace(
					edmxTypeName, edmx);

			if (entityTypeNamespace != null) {
				EntityType entityType = EntityTypeBuilder.build(edmxEntityType,
						entityTypeNamespace, edmx, objects);
				role.setType(entityType);
			}
		}

		return role;
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.End edmxEnd,
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxEnd(edmxEnd);

		AssociationBuilder.validateEdmxAssociation(edmxAssociation);

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	private static void validateEdmxEnd(
			org.eclipse.ogee.client.model.edmx.End edmxEnd)
			throws BuilderException {
		String edmxRoleName = edmxEnd.getRole();
		if (edmxRoleName == null) {
			throw new BuilderException(FrameworkImportMessages.RoleBuilder_0);
		}

		String edmxRoleType = edmxEnd.getType();
		if (edmxRoleType == null) {
			throw new BuilderException(FrameworkImportMessages.RoleBuilder_5);
		}

		String edmxRoleMultiplicity = edmxEnd.getMultiplicity();
		if (edmxRoleMultiplicity == null) {
			throw new BuilderException(FrameworkImportMessages.RoleBuilder_10);
		}
	}
}
