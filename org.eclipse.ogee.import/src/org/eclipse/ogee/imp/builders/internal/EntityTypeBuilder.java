/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.PropertyRef;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Property;

/**
 * Builds an OData EntityType object.
 */
public class EntityTypeBuilder {
	public static EntityType build(
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxEntityType, namespace, edmx, objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		EntityType entityType = (EntityType) nsObject.get(edmxEntityType);
		if (entityType == null) {
			// create entity type
			entityType = OdataFactory.eINSTANCE.createEntityType();
			// save entity type
			nsObject.put(edmxEntityType, entityType);

			// set name
			String name = edmxEntityType.getName();
			entityType.setName(name);

			// set hasStream/media
			boolean edmxHasStream = edmxEntityType.getMHasStream();
			entityType.setMedia(edmxHasStream);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxEntityType
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				entityType.setDocumentation(documentation);
			}

			// set is abstract
			boolean isAbstract = edmxEntityType.isAbstract();
			entityType.setAbstract(isAbstract);

			// set base type
			setBaseType(entityType, edmxEntityType, namespace, edmx, objects);

			// sets properties
			setProperties(entityType, edmxEntityType, namespace, edmx, objects);

			// set navigation properties
			setNavigationProperties(entityType, edmxEntityType, namespace,
					edmx, objects);
		}

		return entityType;
	}

	/**
	 * Sets properties.
	 * 
	 * @param entityType
	 * @param edmxEntityType
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setProperties(EntityType entityType,
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		List<String> keysList = new ArrayList<String>();

		org.eclipse.ogee.client.model.edmx.Key edmxKey = edmxEntityType
				.getKey();
		if (edmxKey != null) {
			PropertyRef[] propertyRefs = edmxKey.getPropertyRefs();
			for (PropertyRef propertyRef : propertyRefs) {
				String propertyRefName = propertyRef.getName();
				keysList.add(propertyRefName);
			}
		}

		org.eclipse.ogee.client.model.edmx.Property[] edmxProperties = edmxEntityType
				.getProperties();
		for (org.eclipse.ogee.client.model.edmx.Property edmxProperty : edmxProperties) {
			Property property = PropertyBuilder.build(edmxProperty,
					edmxEntityType, namespace, edmx, objects);

			if (keysList.contains(property.getName())) {
				// a key must be set as nullable false
				property.setNullable(false);

				// set key
				entityType.getKeys().add(property);
			} else { // set property
				entityType.getProperties().add(property);
			}
		}
	}

	/**
	 * Sets navigation properties.
	 * 
	 * @param entityType
	 * @param edmxEntityType
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setNavigationProperties(EntityType entityType,
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		org.eclipse.ogee.client.model.edmx.NavigationProperty[] edmxNavigationProperties = edmxEntityType
				.getNavigationProperties();
		for (org.eclipse.ogee.client.model.edmx.NavigationProperty edmxNavigationProperty : edmxNavigationProperties) {
			NavigationProperty navigationProperty = NavigationPropertyBuilder
					.build(edmxNavigationProperty, edmxEntityType, namespace,
							edmx, objects);
			entityType.getNavigationProperties().add(navigationProperty);
		}
	}

	/**
	 * Sets base type.
	 * 
	 * @param entityType
	 * @param edmxEntityType
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setBaseType(EntityType entityType,
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		String baseType = edmxEntityType.getBaseType();
		if (baseType != null) {
			String entityTypeNamespace = EdmxUtilities.getEntityTypeNamespace(
					baseType, edmx);

			if (entityTypeNamespace != null) // may be null due to edmx
												// reference
			{
				org.eclipse.ogee.client.model.edmx.EntityType edmxBaseEntityType = EdmxUtilities
						.getEntityTypeByName(baseType, edmx);

				if (edmxBaseEntityType != null) {
					EntityType baseEntityType = EntityTypeBuilder.build(
							edmxBaseEntityType, entityTypeNamespace, edmx,
							objects);
					entityType.setBaseType(baseEntityType);
				}
			}
		}
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	public static void validateEntityType(
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			String namespace) throws BuilderException {
		if (edmxEntityType == null) {
			throw new BuilderException(
					FrameworkImportMessages.EntityTypeBuilder_1);
		}

		String name = edmxEntityType.getName();
		if (name == null) {
			throw new BuilderException(
					FrameworkImportMessages.EntityTypeBuilder_0);
		}
	}
}
