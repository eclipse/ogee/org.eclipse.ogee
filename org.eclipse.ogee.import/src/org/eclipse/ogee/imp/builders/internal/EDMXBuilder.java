/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.EdmxDataServices;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.DataService;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Schema;

/**
 * Builds an OData EDMX object.
 */
public class EDMXBuilder {
	/**
	 * Builds an OData EDMX object given the Edmx object.
	 * 
	 * @param edmx
	 * @return
	 * @throws BuilderException
	 */
	public static EDMX build(org.eclipse.ogee.client.model.edmx.Edmx edmx,
			String uri, Map<String, Map<Object, Object>> odataObjectsMap)
			throws BuilderException {
		validateInputParameters(edmx);

		EDMX odataEDMX = OdataFactory.eINSTANCE.createEDMX();
		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();

		// set uri
		if (uri != null) {
			uri = uri.trim();
		}
		if (uri == null) {
			uri = ""; //$NON-NLS-1$
		}
		odataEDMX.setURI(uri);

		DataService dataService = createDataService(odataEDMX, edmxDataServices);

		setSchemata(edmx, edmxDataServices, dataService, odataObjectsMap);

		return odataEDMX;
	}

	/**
	 * Sets schemata.
	 * 
	 * @param edmx
	 * @param edmxDataServices
	 * @param dataService
	 * @param odataObjects
	 * @throws BuilderException
	 */
	private static void setSchemata(
			org.eclipse.ogee.client.model.edmx.Edmx edmx,
			EdmxDataServices edmxDataServices, DataService dataService,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		org.eclipse.ogee.client.model.edmx.Schema[] edmxSchemas = edmxDataServices
				.getSchemas();
		for (org.eclipse.ogee.client.model.edmx.Schema edmxSchema : edmxSchemas) {
			Schema schema = SchemaBuilder.build(edmxSchema, edmx, objects);
			dataService.getSchemata().add(schema);
		}
	}

	/**
	 * Creates data service
	 * 
	 * @param odataEDMX
	 * @param edmxDataServices
	 * @return
	 */
	private static DataService createDataService(EDMX odataEDMX,
			EdmxDataServices edmxDataServices) {
		DataService dataService = OdataFactory.eINSTANCE.createDataService();

		dataService.setVersion(edmxDataServices.getmDataServiceVersion());
		odataEDMX.setDataService(dataService);

		return dataService;
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.Edmx edmx)
			throws BuilderException {
		if (edmx == null) {
			throw new BuilderException(FrameworkImportMessages.Converter_0);
		}

		EdmxDataServices edmxDataServices = edmx.getEdmxDataServices();
		if (edmxDataServices == null) {
			throw new BuilderException(FrameworkImportMessages.EDMXBuilder_0);
		}
	}

	/**
	 * validates the given parameters.
	 * 
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	public static void validateCommonParameters(
			org.eclipse.ogee.client.model.edmx.Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		if (edmx == null) {
			throw new BuilderException(FrameworkImportMessages.Converter_0);
		}

		if (objects == null) {
			throw new BuilderException(FrameworkImportMessages.EDMXBuilder_3);
		}
	}
}
