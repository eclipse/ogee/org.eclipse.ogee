/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.List;
import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.PropertyRef;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Role;

/**
 * Builds an OData ReferentialConstraint object.
 */
public class ReferentialConstraintBuilder {
	/**
	 * Converts Edmx ReferentialConstraint to OData ReferentialConstraint
	 * 
	 * @param edmxReferentialConstraint
	 * @param edmx
	 * @return - OData ReferentialConstraint
	 */
	public static ReferentialConstraint build(
			org.eclipse.ogee.client.model.edmx.ReferentialConstraint edmxReferentialConstraint,
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxReferentialConstraint, edmxAssociation,
				namespace, edmx, objects);

		Map<Object, Object> nsObjects = objects.get(namespace);

		ReferentialConstraint referentialConstraint = (ReferentialConstraint) nsObjects
				.get(edmxReferentialConstraint);
		if (referentialConstraint == null) {
			// create referential constraint
			referentialConstraint = OdataFactory.eINSTANCE
					.createReferentialConstraint();
			// save referential constraint
			nsObjects.put(edmxReferentialConstraint, referentialConstraint);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxReferentialConstraint
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				referentialConstraint.setDocumentation(documentation);
			}

			org.eclipse.ogee.client.model.edmx.Dependent edmxDependent = edmxReferentialConstraint
					.getDependent();
			org.eclipse.ogee.client.model.edmx.Principal edmxPrincipal = edmxReferentialConstraint
					.getPrincipal();

			// set dependent
			org.eclipse.ogee.client.model.edmx.End edmxDependentEnd = EdmxUtilities
					.getEnd(edmxDependent.getRole(), edmxAssociation);

			if (edmxDependentEnd == null) {
				// assume the second end is the dependent
				edmxDependentEnd = edmxAssociation.getEnds()[1];
			}

			if (edmxDependentEnd != null) {
				Role dependentRole = RoleBuilder.build(edmxDependentEnd,
						edmxAssociation, namespace, edmx, objects);
				referentialConstraint.setDependent(dependentRole);
			}

			// set principal
			org.eclipse.ogee.client.model.edmx.End edmxPrincipalEnd = EdmxUtilities
					.getEnd(edmxPrincipal.getRole(), edmxAssociation);
			if (edmxPrincipalEnd == null) {
				// assume the first end is the principal
				edmxPrincipalEnd = edmxAssociation.getEnds()[0];
			}
			if (edmxPrincipalEnd != null) {
				Role principalRole = RoleBuilder.build(edmxPrincipalEnd,
						edmxAssociation, namespace, edmx, objects);
				referentialConstraint.setPrincipal(principalRole);
			}

			if (referentialConstraint != null && edmxDependent != null
					&& edmxPrincipal != null && edmxDependentEnd != null
					&& edmxPrincipalEnd != null) {
				// build property mappings
				buildPropertyMappings(referentialConstraint, edmxDependent,
						edmxPrincipal, edmxDependentEnd, edmxPrincipalEnd,
						namespace, edmx, objects);
			}
		}

		return referentialConstraint;
	}

	/**
	 * Builds property mappings
	 * 
	 * @param edmxPrincipal
	 * @param edmxDependent
	 * @param edmxDependent
	 * @param edmxPrincipal
	 * @param dependent
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void buildPropertyMappings(
			ReferentialConstraint referentialConstraint,
			org.eclipse.ogee.client.model.edmx.Dependent edmxDependent,
			org.eclipse.ogee.client.model.edmx.Principal edmxPrincipal,
			org.eclipse.ogee.client.model.edmx.End edmxDependentEnd,
			org.eclipse.ogee.client.model.edmx.End edmxPrincipalEnd,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		// get the Dependent entity type
		String dependentEntityTypeName = edmxDependentEnd.getType();
		EntityType dependentEntityType = EdmxUtilities.getEntityTypeByName(
				dependentEntityTypeName, edmx);
		List<org.eclipse.ogee.client.model.edmx.Property> edmxDependentProperties = EdmxUtilities
				.getProperties(dependentEntityTypeName, edmx);

		// get the Principal entity type
		String principalEntityTypeName = edmxPrincipalEnd.getType();
		EntityType principalEntityType = EdmxUtilities.getEntityTypeByName(
				principalEntityTypeName, edmx);
		List<org.eclipse.ogee.client.model.edmx.Property> edmxPrincipalProperties = EdmxUtilities
				.getProperties(principalEntityTypeName, edmx);

		// get property refs from both Principal and Dependent
		PropertyRef[] dependentPropertyRefs = edmxDependent.getPropertyRefs();
		PropertyRef[] principalPropertyRefs = edmxPrincipal.getPropertyRefs();

		// get their sizes
		int principalPropertyRefsLength = principalPropertyRefs.length;
		int dependentPropertyRefsLength = dependentPropertyRefs.length;

		// take the smaller size
		int size = principalPropertyRefsLength < dependentPropertyRefsLength ? principalPropertyRefsLength
				: dependentPropertyRefsLength;

		// going over all dependent property refs
		for (int i = 0; i < size; i++) {
			// get dependent key name
			String dependentKeyName = dependentPropertyRefs[i].getName();
			// build the dependent key property
			org.eclipse.ogee.client.model.edmx.Property edmxDependentProperty = findProperty(
					dependentKeyName, edmxDependentProperties);
			Property dependentProperty = PropertyBuilder.build(
					edmxDependentProperty, dependentEntityType, namespace,
					edmx, objects);

			// get principal key name
			String principalKeyName = principalPropertyRefs[i].getName();
			// build the principal key property
			org.eclipse.ogee.client.model.edmx.Property edmxPrincipalProperty = findProperty(
					principalKeyName, edmxPrincipalProperties);
			Property principalProperty = PropertyBuilder.build(
					edmxPrincipalProperty, principalEntityType, namespace,
					edmx, objects);

			// create property mapping
			referentialConstraint.getKeyMappings().put(principalProperty,
					dependentProperty);
		}
	}

	/**
	 * Find edmx property
	 * 
	 * @param edmxPropertyName
	 * @param edmxProperties
	 * @return
	 * @throws BuilderException
	 */
	private static org.eclipse.ogee.client.model.edmx.Property findProperty(
			String edmxPropertyName,
			List<org.eclipse.ogee.client.model.edmx.Property> edmxProperties)
			throws BuilderException {
		for (org.eclipse.ogee.client.model.edmx.Property edmxProperty : edmxProperties) {
			if (edmxProperty.getName().equalsIgnoreCase(edmxPropertyName)) {
				return edmxProperty;
			}
		}
		return new org.eclipse.ogee.client.model.edmx.Property();

		// throw new
		// BuilderException(FrameworkImportMessages.ReferentialConstraintBuilder_8
		// + edmxPropertyName +
		// FrameworkImportMessages.ReferentialConstraintBuilder_9);
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.ReferentialConstraint edmxReferentialConstraint,
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxReferentialConstraint(edmxReferentialConstraint);

		AssociationBuilder.validateEdmxAssociation(edmxAssociation);

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	private static void validateEdmxReferentialConstraint(
			org.eclipse.ogee.client.model.edmx.ReferentialConstraint edmxReferentialConstraint)
			throws BuilderException {
		if (edmxReferentialConstraint == null) {
			throw new BuilderException(
					FrameworkImportMessages.ReferentialConstraintBuilder_1);
		}

		org.eclipse.ogee.client.model.edmx.Dependent edmxDependent = edmxReferentialConstraint
				.getDependent();
		org.eclipse.ogee.client.model.edmx.Principal edmxPrincipal = edmxReferentialConstraint
				.getPrincipal();
		if (edmxDependent == null || edmxPrincipal == null) {
			throw new BuilderException(
					FrameworkImportMessages.ReferentialConstraintBuilder_4);
		}

		PropertyRef[] edmxDependentPropertyRefs = edmxDependent
				.getPropertyRefs();
		if (edmxDependentPropertyRefs == null) {
			throw new BuilderException(
					FrameworkImportMessages.ReferentialConstraintBuilder_5);
		}

		PropertyRef[] edmxPrincipalPropertyRefs = edmxPrincipal
				.getPropertyRefs();
		if (edmxPrincipalPropertyRefs == null) {
			throw new BuilderException(
					FrameworkImportMessages.ReferentialConstraintBuilder_6);
		}

		int dependentLength = edmxDependentPropertyRefs.length;
		int principalLength = edmxPrincipalPropertyRefs.length;
		if (dependentLength != principalLength) {
			throw new BuilderException(
					FrameworkImportMessages.ReferentialConstraintBuilder_7);
		}
	}
}
