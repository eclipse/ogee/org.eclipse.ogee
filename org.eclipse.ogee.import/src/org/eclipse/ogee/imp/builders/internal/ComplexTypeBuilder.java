/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Property;

/**
 * Builds an OData ComplexType object.
 */
public class ComplexTypeBuilder {
	/**
	 * Converts Edmx ComplexType to OData ComplexType
	 * 
	 * @param edmxComplexType
	 * @return - OData ComplexType
	 */
	public static ComplexType build(
			org.eclipse.ogee.client.model.edmx.ComplexType edmxComplexType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxComplexType, namespace, edmx, objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		ComplexType complexType = (ComplexType) nsObject.get(edmxComplexType);
		if (complexType == null) {
			// create complex type
			complexType = OdataFactory.eINSTANCE.createComplexType();
			// set name
			String edmxComplexTypeName = edmxComplexType.getName();
			complexType.setName(edmxComplexTypeName);
			// save complex typeO
			nsObject.put(edmxComplexType, complexType);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxComplexType
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				complexType.setDocumentation(documentation);
			}

			// set properties
			org.eclipse.ogee.client.model.edmx.Property[] edmxProperties = edmxComplexType
					.getProperties();
			for (org.eclipse.ogee.client.model.edmx.Property edmxProperty : edmxProperties) {

				Property property = PropertyBuilder.build(edmxProperty,
						edmxComplexType, namespace, edmx, objects);
				complexType.getProperties().add(property);
			}

			// set abstract
			boolean isAbstract = edmxComplexType.isAbstract();
			complexType.setAbstract(isAbstract);

			// set base type
			String edmxComplexBaseTypeName = edmxComplexType.getBaseType();
			if (edmxComplexBaseTypeName != null) {
				org.eclipse.ogee.client.model.edmx.ComplexType edmxBaseComplexType = EdmxUtilities
						.getComplexTypeByName(edmxComplexBaseTypeName, edmx);
				if (edmxBaseComplexType != null) {
					ComplexType baseComplexType = ComplexTypeBuilder.build(
							edmxBaseComplexType, namespace, edmx, objects);
					complexType.setBaseType(baseComplexType);
				}
			}
		}

		return complexType;
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.ComplexType edmxComplexType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxComplexType(edmxComplexType);

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	public static void validateEdmxComplexType(
			org.eclipse.ogee.client.model.edmx.ComplexType edmxComplexType)
			throws BuilderException {
		if (edmxComplexType == null) {
			throw new BuilderException(
					FrameworkImportMessages.ComplexTypeBuilder_1);
		}

		String edmxComplexTypeName = edmxComplexType.getName();
		if (edmxComplexTypeName == null) {
			throw new BuilderException(
					FrameworkImportMessages.ComplexTypeBuilder_0);
		}
	}
}
