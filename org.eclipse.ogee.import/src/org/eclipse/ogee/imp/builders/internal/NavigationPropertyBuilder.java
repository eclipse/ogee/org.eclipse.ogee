/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.NavigationProperty;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Role;

/**
 * Builds an OData NavigationProperty object.
 */
public class NavigationPropertyBuilder {
	/**
	 * Builds an OData NavigationProperty object from the given Edmx
	 * NavigationProperty object.
	 * 
	 * @param edmxNavigationProperty
	 * @param edmx
	 * @param objects
	 * @return
	 * @throws BuilderException
	 */
	public static NavigationProperty build(
			org.eclipse.ogee.client.model.edmx.NavigationProperty edmxNavigationProperty,
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxNavigationProperty, edmxEntityType,
				namespace, edmx, objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		NavigationProperty navigationProperty = (NavigationProperty) nsObject
				.get(edmxNavigationProperty);
		if (navigationProperty == null) {
			// create navigation property
			navigationProperty = OdataFactory.eINSTANCE
					.createNavigationProperty();
			// save navigation property
			nsObject.put(edmxNavigationProperty, navigationProperty);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxNavigationProperty
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				navigationProperty.setDocumentation(documentation);
			}

			// set name
			String edmxNavigationPropertyName = edmxNavigationProperty
					.getName();
			navigationProperty.setName(edmxNavigationPropertyName);

			// set relationship
			String edmxRelationship = edmxNavigationProperty.getRelationship();
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation = EdmxUtilities
					.getAssociation(edmxRelationship, edmx);
			String associationNamespace = EdmxUtilities
					.getAssociationNamespace(edmxRelationship, edmx);
			Association association = AssociationBuilder.build(edmxAssociation,
					associationNamespace, edmx, objects);
			navigationProperty.setRelationship(association);

			// set from role
			String edmxFromRole = edmxNavigationProperty.getFromRole();
			org.eclipse.ogee.client.model.edmx.End fromRoleEnd = EdmxUtilities
					.getEnd(edmxFromRole, edmxAssociation);
			Role fromRole = RoleBuilder.build(fromRoleEnd, edmxAssociation,
					associationNamespace, edmx, objects);
			navigationProperty.setFromRole(fromRole);

			// set to role
			String edmxToRole = edmxNavigationProperty.getToRole();
			org.eclipse.ogee.client.model.edmx.End toRoleEnd = EdmxUtilities
					.getEnd(edmxToRole, edmxAssociation);
			Role toRole = RoleBuilder.build(toRoleEnd, edmxAssociation,
					associationNamespace, edmx, objects);
			navigationProperty.setToRole(toRole);
		}

		return navigationProperty;
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.NavigationProperty edmxNavigationProperty,
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxNavigationProperty(edmxNavigationProperty);

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	private static void validateEdmxNavigationProperty(
			org.eclipse.ogee.client.model.edmx.NavigationProperty edmxNavigationProperty)
			throws BuilderException {
		if (edmxNavigationProperty == null) {
			throw new BuilderException(
					FrameworkImportMessages.NavigationPropertyBuilder_1);
		}

		String name = edmxNavigationProperty.getName();
		if (name == null) {
			throw new BuilderException(
					FrameworkImportMessages.NavigationPropertyBuilder_0);
		}

	}
}
