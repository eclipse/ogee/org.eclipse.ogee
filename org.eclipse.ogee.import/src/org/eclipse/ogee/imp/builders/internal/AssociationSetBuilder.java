/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.End;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.OdataFactory;

/**
 * Builds an OData AssociationSet object.
 */
public class AssociationSetBuilder {
	/**
	 * Builds an OData AssociationSet object from the given Edmx AssociationSet
	 * object.
	 * 
	 * @param edmxAssociationSet
	 * @return
	 */
	public static AssociationSet build(
			org.eclipse.ogee.client.model.edmx.AssociationSet edmxAssociationSet,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxAssociationSet, namespace, edmx, objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		if (nsObject == null) {
			Schema edmxSchema = EdmxUtilities.getSchema(namespace, edmx);
			SchemaBuilder.build(edmxSchema, edmx, objects);
			nsObject = objects.get(namespace);
		}

		AssociationSet associationSet = (AssociationSet) nsObject
				.get(edmxAssociationSet);
		if (associationSet == null) {
			// create association set
			associationSet = OdataFactory.eINSTANCE.createAssociationSet();
			// save association end
			nsObject.put(edmxAssociationSet, associationSet);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxAssociationSet
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				associationSet.setDocumentation(documentation);
			}

			// set name
			String associationSetName = edmxAssociationSet.getName();
			associationSet.setName(associationSetName);

			// set association
			String associationName = edmxAssociationSet.getAssociation();
			String associationNamespace = EdmxUtilities
					.getAssociationNamespace(associationName, edmx);

			if (associationNamespace != null) {
				org.eclipse.ogee.client.model.edmx.Association edmxAssociation = EdmxUtilities
						.getAssociation(associationName, edmx);
				// might come from a reference
				if (edmxAssociation != null) {
					Association association = AssociationBuilder.build(
							edmxAssociation, associationNamespace, edmx,
							objects);
					associationSet.setAssociation(association);
				}

				// set ends
				org.eclipse.ogee.client.model.edmx.End[] edmxEnds = edmxAssociationSet
						.getEnds();
				for (org.eclipse.ogee.client.model.edmx.End edmxAssociationSetEnd : edmxEnds) {
					String entitySetName = edmxAssociationSetEnd.getEntitySet();

					// get the role of this AssociationSetEnd
					String edmxAssociationSetEndRoleName = edmxAssociationSetEnd
							.getRole();
					// get the end from the association by its role
					edmxAssociationSetEnd = getEndFromAssociation(
							edmxAssociationSetEndRoleName, edmxAssociation);

					if (edmxAssociationSetEnd != null) {
						AssociationSetEnd associationSetEnd = AssociationSetEndBuilder
								.build(edmxAssociationSetEnd,
										associationSetName, entitySetName,
										edmxAssociation, associationNamespace,
										namespace, edmx, objects);
						associationSet.getEnds().add(associationSetEnd);
					}
				}
			}
		}

		return associationSet;
	}

	private static org.eclipse.ogee.client.model.edmx.End getEndFromAssociation(
			String edmxAssociationSetEndRoleName,
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation)
			throws BuilderException {
		if (edmxAssociation == null) {
			return null;
		}

		// get the end from the association
		End edmxEnd = EdmxUtilities.getEnd(edmxAssociationSetEndRoleName,
				edmxAssociation);

		return edmxEnd;
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.AssociationSet edmxAssociationSet,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxAssociationSet(edmxAssociationSet);

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	private static void validateEdmxAssociationSet(
			org.eclipse.ogee.client.model.edmx.AssociationSet edmxAssociationSet)
			throws BuilderException {
		if (edmxAssociationSet == null) {
			throw new BuilderException(
					FrameworkImportMessages.AssociationSetBuilder_1);
		}

		String name = edmxAssociationSet.getName();
		if (name == null) {
			throw new BuilderException(
					FrameworkImportMessages.AssociationSetBuilder_0);
		}

		String associationName = edmxAssociationSet.getAssociation();
		if (associationName == null) {
			throw new BuilderException(
					FrameworkImportMessages.AssociationSetBuilder_5);
		}
	}
}
