/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Parameter;

/**
 * Builds an OData Function object.
 */
public class FunctionImportBuilder {
	private static final String POST = "POST"; //$NON-NLS-1$

	/**
	 * Converts the given Edmx function import to OData Function object.
	 * 
	 * @param edmxFunctionImport
	 * @return - OData Function object.
	 * @throws BuilderException
	 */
	public static FunctionImport build(
			org.eclipse.ogee.client.model.edmx.FunctionImport edmxFunctionImport,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxFunctionImport, namespace, edmx, objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		FunctionImport function = (FunctionImport) nsObject
				.get(edmxFunctionImport);
		if (function == null) {
			// create function
			function = OdataFactory.eINSTANCE.createFunctionImport();
			// set name
			String name = edmxFunctionImport.getName();
			function.setName(name);
			// save function
			nsObject.put(edmxFunctionImport, function);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxFunctionImport
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				function.setDocumentation(documentation);
			}

			IFunctionReturnTypeUsage returnEntityTypeUsage = ReturnEntityTypeUsageBuilder
					.build(edmxFunctionImport, namespace, edmx, objects);

			function.setReturnType(returnEntityTypeUsage);

			// set parameters
			org.eclipse.ogee.client.model.edmx.Parameter[] edmxParameters = edmxFunctionImport
					.getParameters();
			for (org.eclipse.ogee.client.model.edmx.Parameter edmxParameter : edmxParameters) {
				Parameter parameter = ParameterBuilder.build(edmxParameter,
						namespace, edmx, objects, name);
				function.getParameters().add(parameter);
			}

			String edmxHttpMethod = edmxFunctionImport.getmHttpMethod();
			if (edmxHttpMethod != null && edmxHttpMethod.equals(POST)) {
				function.setSideEffecting(true);
			}
		}

		return function;
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.FunctionImport edmxFunctionImport,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxFunctionImport(edmxFunctionImport);

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	private static void validateEdmxFunctionImport(
			org.eclipse.ogee.client.model.edmx.FunctionImport edmxFunctionImport)
			throws BuilderException {
		if (edmxFunctionImport == null) {
			throw new BuilderException(
					FrameworkImportMessages.FunctionBuilder_1);
		}

		String edmxFunctionImportName = edmxFunctionImport.getName();
		if (edmxFunctionImportName == null) {
			throw new BuilderException(
					FrameworkImportMessages.FunctionImportBuilder_0);
		}
	}
}
