/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.ReferentialConstraint;
import org.eclipse.ogee.model.odata.Role;

/**
 * Builds an OData Association object.
 */
public class AssociationBuilder {
	/**
	 * Converts edmx Association to OData Association
	 * 
	 * @param edmxAssociation
	 * @param edmxSchema
	 * @param edmx
	 * @param objects
	 * @return
	 * @throws BuilderException
	 */
	public static Association build(
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		// V4 support
		if (edmxAssociation == null) {
			return null;
		}

		validateInputParameters(edmxAssociation, namespace, edmx, objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		if (nsObject == null) {
			Schema edmxSchema = EdmxUtilities.getSchema(namespace, edmx);
			SchemaBuilder.build(edmxSchema, edmx, objects);
			nsObject = objects.get(namespace);
		}

		Association association = (Association) nsObject.get(edmxAssociation);
		if (association == null) {
			// create association
			association = OdataFactory.eINSTANCE.createAssociation();
			// save association
			nsObject.put(edmxAssociation, association);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxAssociation
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				association.setDocumentation(documentation);
			}

			// set name
			String edmxAssociationName = edmxAssociation.getName();
			association.setName(edmxAssociationName);

			// set ends
			org.eclipse.ogee.client.model.edmx.End[] edmxEnds = edmxAssociation
					.getEnds();
			for (org.eclipse.ogee.client.model.edmx.End edmxEnd : edmxEnds) {
				Role end = RoleBuilder.build(edmxEnd, edmxAssociation,
						namespace, edmx, objects);
				association.getEnds().add(end);
			}

			// set referential constraint
			org.eclipse.ogee.client.model.edmx.ReferentialConstraint edmxReferentialConstraint = edmxAssociation
					.getReferentialConstraint();
			if (edmxReferentialConstraint != null) {
				ReferentialConstraint referentialConstraint = ReferentialConstraintBuilder
						.build(edmxReferentialConstraint, edmxAssociation,
								namespace, edmx, objects);
				association.setReferentialConstraint(referentialConstraint);
			}
		}

		return association;
	}

	public static void validateEdmxAssociation(
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation)
			throws BuilderException {
		String edmxAssociationName = edmxAssociation.getName();
		if (edmxAssociationName == null) {
			throw new BuilderException(
					FrameworkImportMessages.AssociationBuilder_0);
		}
	}

	public static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxAssociation(edmxAssociation);

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}
}
