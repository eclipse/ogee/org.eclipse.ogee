/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.List;
import java.util.Map;

import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;

/**
 * Return type of a function import builder.
 */
public class ReturnEntityTypeUsageBuilder {
	private enum Type {
		SIMPLE, ENTITY, COMPLEX
	}

	private static final String KEY_SUFFIX = "ReturnEntityTypeUsage="; //$NON-NLS-1$

	public static IFunctionReturnTypeUsage build(
			FunctionImport edmxFunctionImport, String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		String key = KEY_SUFFIX + edmxFunctionImport.getName();

		// get the function's return type from our Edmx object
		String edmxFunctionImportReturnType = edmxFunctionImport
				.getReturnType();

		IFunctionReturnTypeUsage returnTypeUsage = OdataFactory.eINSTANCE
				.createReturnEntityTypeUsage();

		// check the type of the return type (simple, entity, complex or entity
		// set)
		Type type = getType((ReturnEntityTypeUsage) returnTypeUsage,
				edmxFunctionImportReturnType, edmx);

		if (type == null) {
			return null; // this function import has no return type, or it comes
							// from a reference.
		}

		switch (type) {
		case ENTITY:

			returnTypeUsage = OdataFactory.eINSTANCE
					.createReturnEntityTypeUsage();

			// check if needs to be setCollection
			returnTypeUsage = checkIfCollection(
					(ReturnEntityTypeUsage) returnTypeUsage,
					edmxFunctionImportReturnType, edmx);

			returnTypeUsage = setEntitySetAndEntityType(edmxFunctionImport,
					namespace, edmx, objects,
					(ReturnEntityTypeUsage) returnTypeUsage,
					edmxFunctionImportReturnType, type);

			break;

		case COMPLEX:

			ComplexType edmxComplexType = EdmxUtilities.getComplexTypeByName(
					edmxFunctionImportReturnType, edmx);
			org.eclipse.ogee.model.odata.ComplexType complexType = ComplexTypeBuilder
					.build(edmxComplexType, namespace, edmx, objects);

			ComplexTypeUsage complexTypeUsage = OdataFactory.eINSTANCE
					.createComplexTypeUsage();
			complexTypeUsage.setComplexType(complexType);

			if (isCollection(edmxFunctionImportReturnType)) {
				complexTypeUsage.setCollection(true);
			}

			returnTypeUsage = complexTypeUsage;
			break;

		case SIMPLE:

			SimpleType simpleType = OdataFactory.eINSTANCE.createSimpleType();
			SimpleTypeUsage simpleTypeUsage = OdataFactory.eINSTANCE
					.createSimpleTypeUsage();

			// check if collection
			if (isCollection(edmxFunctionImportReturnType)) {
				simpleTypeUsage.setCollection(true);
				edmxFunctionImportReturnType = extractReturnTypeFromCollectionType(edmxFunctionImportReturnType);
			}

			// get rid of the "Edm." prefix
			edmxFunctionImportReturnType = edmxFunctionImportReturnType
					.substring(edmxFunctionImportReturnType.indexOf(".") + 1); //$NON-NLS-1$

			// going over all simple types
			List<EDMTypes> EDMTypesValues = EDMTypes.VALUES;
			for (EDMTypes edmType : EDMTypesValues) {
				String edmTypeName = edmType.name();
				edmTypeName = edmTypeName.replaceAll("_", ""); //$NON-NLS-1$ //$NON-NLS-2$
				if (edmTypeName.equalsIgnoreCase(edmxFunctionImportReturnType)) {
					simpleType.setType(edmType);
				}
			}

			simpleTypeUsage.setSimpleType(simpleType);
			returnTypeUsage = simpleTypeUsage;

			// no need to set entity type and entity set to a simple type
			break;

		default:
			// do something?
			break;
		}

		Map<Object, Object> nsObject = objects.get(namespace);
		nsObject.put(key, returnTypeUsage);

		return returnTypeUsage;
	}

	/**
	 * Checks if the given returnTypeUsage is a collection, if so, it sets
	 * collection = true.
	 */
	private static ReturnEntityTypeUsage checkIfCollection(
			ReturnEntityTypeUsage returnTypeUsage,
			String edmxFunctionImportReturnType, Edmx edmx) {
		if (isCollection(edmxFunctionImportReturnType)) {
			returnTypeUsage.setCollection(true);
			return returnTypeUsage;
		}

		if (edmxFunctionImportReturnType.contains(".")) //$NON-NLS-1$
		{
			int indexOfDot = edmxFunctionImportReturnType.lastIndexOf('.');
			edmxFunctionImportReturnType = edmxFunctionImportReturnType
					.substring(indexOfDot + 1);

			Schema[] schemas = edmx.getEdmxDataServices().getSchemas();

			for (Schema schema : schemas) {
				// going over all entity sets to check if the return type is an
				// entity set,
				// if so, it also needs to be set as a collection
				EntityContainer[] edmxEntityContainers = schema
						.getEntityContainers();
				for (EntityContainer entityContainer : edmxEntityContainers) {
					org.eclipse.ogee.client.model.edmx.EntitySet[] edmxEntitySets = entityContainer
							.getEntitySets();
					for (org.eclipse.ogee.client.model.edmx.EntitySet entitySet : edmxEntitySets) {
						if (entitySet.getName().equals(
								edmxFunctionImportReturnType)) {
							returnTypeUsage.setCollection(true);
							return returnTypeUsage;
						}
					}
				}
			}
		}

		return returnTypeUsage;
	}

	private static ReturnEntityTypeUsage setEntitySetAndEntityType(
			FunctionImport edmxFunctionImport, String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects,
			ReturnEntityTypeUsage returnTypeUsage,
			String edmxFunctionImportReturnType, Type type)
			throws BuilderException {
		// set entity set

		// According to CSDL:
		// 1. If the return type of FunctionImport is a collection of entities,
		// the EntitySet attribute MUST be defined.
		// 2. If the return type of FunctionImport is of ComplexType or scalar
		// type, the EntitySet attribute MUST NOT be defined.
		// if (returnTypeUsage.isCollection()) {
		String edmxEntitySetName = edmxFunctionImport.getEntitySet();
		if (edmxEntitySetName != null) {
			org.eclipse.ogee.client.model.edmx.EntitySet edmxEntitySet = EdmxUtilities
					.getEntitySetByName(edmxEntitySetName, edmx);

			if (edmxEntitySet != null) {
				EntitySet entitySet = EntitySetBuilder.build(edmxEntitySet,
						namespace, edmx, objects);

				((ReturnEntityTypeUsage) returnTypeUsage)
						.setEntitySet(entitySet);
			}
		}
		// }

		if (edmxFunctionImportReturnType != null) {
			// set entity type
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType = EdmxUtilities
					.getEntityTypeByName(edmxFunctionImportReturnType, edmx);

			if (edmxEntityType == null) {
				// perhaps the return type is an entity set, then use its entity
				// type

				org.eclipse.ogee.client.model.edmx.EntitySet edmxEntitySet = EdmxUtilities
						.getEntitySetByName(edmxFunctionImportReturnType, edmx);

				if (edmxEntitySet != null) {
					// get the (String) entity type of the entity set
					String entitySetType = edmxEntitySet.getEntityType();

					String entityTypeNamespace = EdmxUtilities
							.getEntityTypeNamespace(entitySetType, edmx);

					if (entityTypeNamespace != null) // may be null due to edmx
														// reference
					{
						// get the edmx entity type
						org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType2 = EdmxUtilities
								.getEntityTypeByName(entitySetType, edmx);

						// build an entity type
						EntityType entityType = EntityTypeBuilder.build(
								edmxEntityType2, entityTypeNamespace, edmx,
								objects);

						// set it in the return type
						((ReturnEntityTypeUsage) returnTypeUsage)
								.setEntityType(entityType);
					}
				}
			} else // entity type
			{
				String entityTypeNamespace = EdmxUtilities
						.getEntityTypeNamespace(edmxFunctionImportReturnType,
								edmx);

				if (entityTypeNamespace != null) {
					EntityType entityType = EntityTypeBuilder.build(
							edmxEntityType, entityTypeNamespace, edmx, objects);

					((ReturnEntityTypeUsage) returnTypeUsage)
							.setEntityType(entityType);
				}
			}
		}

		return returnTypeUsage;
	}

	/**
	 * Gets a return type of a function import as a string, and returns its
	 * object representation (SimpleTypeUsage, ComplexTypeUsage,
	 * EntityTypeUsage).
	 */
	private static Type getType(ReturnEntityTypeUsage returnTypeUsage,
			String edmxReturnType, Edmx edmx) {
		String namespace = ""; //$NON-NLS-1$

		if (edmxReturnType == null) {
			return null;
		}

		// check if collection
		returnTypeUsage = checkIfCollection(returnTypeUsage, edmxReturnType,
				edmx);
		if (returnTypeUsage.isCollection()) {
			edmxReturnType = extractReturnTypeFromCollectionType(edmxReturnType);
		}

		if (edmxReturnType.contains("Edm.")) //$NON-NLS-1$
		{
			return Type.SIMPLE;
		}

		if (edmxReturnType.contains(".")) //$NON-NLS-1$
		{
			int indexOfLastDot = edmxReturnType.lastIndexOf('.');

			namespace = edmxReturnType.substring(0, indexOfLastDot);
			edmxReturnType = edmxReturnType.substring(indexOfLastDot + 1);
		}

		Type type = null;

		boolean match = false;

		Schema[] schemas = edmx.getEdmxDataServices().getSchemas();

		// going over all schemas namespaces
		for (Schema schema : schemas) {
			String schemaNamespace = schema.getNamespace();

			if (schemaNamespace.equals(namespace)) {
				match = true;
			}
		}

		if (!match) {
			// the return type comes from a reference
			// or doesn't exist so it needs to be set as null here.
			return null;
		}

		for (Schema schema : schemas) {
			// going over all entity types
			org.eclipse.ogee.client.model.edmx.EntityType[] edmxEntityTypes = schema
					.getEntityTypes();
			for (org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType : edmxEntityTypes) {
				if (edmxEntityType.getName().equals(edmxReturnType)) {
					return Type.ENTITY;
				}
			}

			// going over all complex types
			ComplexType[] edmxComplexTypes = schema.getComplexTypes();
			for (ComplexType edmxComplexType : edmxComplexTypes) {
				if (edmxComplexType.getName().equals(edmxReturnType)) {
					return Type.COMPLEX;
				}
			}

			// going over all entity sets
			org.eclipse.ogee.client.model.edmx.EntityContainer[] edmxEntityContainers = schema
					.getEntityContainers();
			for (EntityContainer entityContainer : edmxEntityContainers) {
				org.eclipse.ogee.client.model.edmx.EntitySet[] edmxEntitySets = entityContainer
						.getEntitySets();

				for (org.eclipse.ogee.client.model.edmx.EntitySet edmxEntitySet : edmxEntitySets) {
					if (edmxEntitySet.getName().equals(edmxReturnType)) {
						return Type.ENTITY;
					}
				}
			}
		}

		return type;
	}

	/**
	 * Extracts the actual return type with no prefixes.
	 * 
	 * @param returnType
	 * @return - the actual return type with no prefixes.
	 */
	private static String extractReturnTypeFromCollectionType(String returnType) {
		if (isCollection(returnType)) {
			int start = returnType.indexOf('(');
			int end = returnType.indexOf(')');
			returnType = returnType.substring(start + 1, end);
			return returnType;
		}

		if (returnType.contains(".")) //$NON-NLS-1$
		{
			int indexOfDot = returnType.lastIndexOf('.');
			returnType = returnType.substring(indexOfDot + 1);
		}

		return returnType;
	}

	/**
	 * Checks whether the given returnType starts with "Collection".
	 * 
	 * @param returnType
	 * @return - true if the given returnType starts with "Collection", false
	 *         otherwise.
	 */
	private static boolean isCollection(String returnType) {
		if (returnType == null) {
			return false;
		}

		return returnType.startsWith("Collection"); //$NON-NLS-1$
	}
}
