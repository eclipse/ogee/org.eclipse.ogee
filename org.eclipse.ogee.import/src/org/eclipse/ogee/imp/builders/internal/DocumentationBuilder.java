/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import org.eclipse.ogee.client.model.edmx.LongDescription;
import org.eclipse.ogee.client.model.edmx.Summary;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.OdataFactory;

/**
 * Builds an OData Documentation object.
 */
public class DocumentationBuilder {
	/**
	 * Converts the given Edmx Documentation object to OData Documentation
	 * object.
	 * 
	 * @param edmxDocumentation
	 * @return - OData Documentation object.
	 * @throws BuilderException
	 */
	public static Documentation build(
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation)
			throws BuilderException {
		if (edmxDocumentation == null) {
			throw new BuilderException(
					FrameworkImportMessages.DocumentationBuilder_0);
		}

		Documentation documentation = OdataFactory.eINSTANCE
				.createDocumentation();

		LongDescription edmxLongDescription = edmxDocumentation
				.getLongDescription();
		if (edmxLongDescription != null) {
			documentation.setLongDescription(edmxLongDescription.getValue());
		}

		Summary edmxSummary = edmxDocumentation.getSummary();
		if (edmxSummary != null) {
			documentation.setSummary(edmxSummary.getValue());
		}

		return documentation;
	}
}
