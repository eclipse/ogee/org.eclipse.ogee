/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;

/**
 * Builds an OData Using object.
 */
public class UsingBuilder {
	/**
	 * Converts the given Edmx Using object to OData Using object.
	 * 
	 * @param edmxUsing
	 * @return - OData Using object.
	 * @throws BuilderException
	 */
	public static Using build(
			org.eclipse.ogee.client.model.edmx.Using edmxUsing,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxUsing, namespace, edmx, objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		Using using = (Using) nsObject.get(edmxUsing);
		if (using == null) {
			// create using
			using = OdataFactory.eINSTANCE.createUsing();
			// save using
			nsObject.put(edmxUsing, using);

			// set alias
			String alias = edmxUsing.getAlias();
			using.setAlias(alias);

			org.eclipse.ogee.client.model.edmx.Schema edmxSchema = EdmxUtilities
					.getSchema(edmxUsing.getNamespace(), edmx);
			if (edmxSchema != null) {
				Schema schema = SchemaBuilder.build(edmxSchema, edmx, objects);
				using.setUsedNamespace(schema);
			}
		}

		return using;
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.Using edmxUsing,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateUsing(edmxUsing);

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	private static void validateUsing(
			org.eclipse.ogee.client.model.edmx.Using edmxUsing)
			throws BuilderException {
		if (edmxUsing == null) {
			throw new BuilderException(FrameworkImportMessages.UsingBuilder_0);
		}

		String namespace = edmxUsing.getNamespace();
		if (namespace == null) {
			throw new BuilderException(FrameworkImportMessages.UsingBuilder_1);
		}

		String alias = edmxUsing.getAlias();
		if (alias == null) {
			throw new BuilderException(FrameworkImportMessages.UsingBuilder_2);
		}
	}
}
