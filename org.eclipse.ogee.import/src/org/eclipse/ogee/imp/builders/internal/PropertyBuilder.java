/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.parser.impl.EdmTypes;
import org.eclipse.ogee.client.parser.impl.util.DateUtils;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.model.odata.util.DateTime;
import org.eclipse.ogee.model.odata.util.DateTimeOffset;
import org.eclipse.ogee.utils.logger.Logger;

/**
 * Builds an OData Property object.
 */
public class PropertyBuilder {
	/**
	 * Builds OData property.
	 * 
	 * @param edmxProperty
	 *            - the given property to be converted.
	 * @param edmxEntityTypeName
	 * @param edmxEntityType
	 * @return - OData property
	 */
	public static Property build(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty,
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxProperty, edmxEntityType, namespace, edmx,
				objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		if (edmxEntityType == null) {
			edmxEntityType = new EntityType();
		}
		Property property = (Property) nsObject.get(edmxEntityType.toString()
				+ edmxProperty);
		if (property == null) {
			// create property
			property = OdataFactory.eINSTANCE.createProperty();
			// save property
			nsObject.put(edmxEntityType.toString() + edmxProperty, property);

			// set property name
			String name = edmxProperty.getName();
			property.setName(name);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxProperty
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				property.setDocumentation(documentation);
			}

			// set is nullable
			boolean edmxNullable = edmxProperty.isNullable();
			property.setNullable(edmxNullable);

			// "Use for Etag" translates to "concurrencyMode=Fixed"
			String concurrencyMode = edmxProperty
					.getAttribute("ConcurrencyMode"); //$NON-NLS-1$
			if ((concurrencyMode != null) && (concurrencyMode.equals("Fixed"))) //$NON-NLS-1$
			{
				property.setForEtag(true);
			}

			EdmTypes edmTypes = EdmTypes.toEdmTypes(edmxProperty.getType());
			// set property type
			String edmxPropertyType = (edmTypes.equals(EdmTypes.NOVALUE) ? edmxProperty
					.getType() : edmTypes.toString());

			if (edmxPropertyType != null) {
				setPropertyType(property, edmxProperty, edmxPropertyType,
						namespace, edmx, objects);
			}

		}

		return property;
	}

	/**
	 * Sets the type of the property.
	 * 
	 * @param property
	 * @param edmxProperty
	 * @param edmxPropertyType
	 * @param namespace
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setPropertyType(Property property,
			org.eclipse.ogee.client.model.edmx.Property edmxProperty,
			String edmxPropertyType, String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		boolean collection = false;

		if (edmxPropertyType.contains("Edm")) //$NON-NLS-1$
		{
			if (isCollection(edmxPropertyType)) {
				collection = true;
				edmxPropertyType = extractTypeFromCollectionType(edmxPropertyType);
			}

			// handle simple type
			SimpleTypeUsage stu = getSimpleTypeUsage(edmxProperty,
					edmxPropertyType);

			if (stu != null && collection) {
				stu.setCollection(true);
			}

			// might be null
			property.setType(stu);
		} else {
			if (edmxPropertyType.contains(".")) //$NON-NLS-1$
			{
				if (isCollection(edmxPropertyType)) {
					collection = true;
					edmxPropertyType = extractTypeFromCollectionType(edmxPropertyType);
				}

				// going over all schemas
				Schema[] schemas = edmx.getEdmxDataServices().getSchemas();
				for (Schema schema : schemas) {
					// going over all complex types
					org.eclipse.ogee.client.model.edmx.ComplexType[] complexTypes = schema
							.getComplexTypes();
					for (org.eclipse.ogee.client.model.edmx.ComplexType complexType : complexTypes) {
						if (complexType.getName().equals(
								edmxPropertyType.substring(edmxPropertyType
										.lastIndexOf('.') + 1)))
						{
							// handle complex type
							ComplexTypeUsage ctu = getComplexTypeUsage(
									complexType, namespace, edmx, objects);

							if (collection) {
								ctu.setCollection(true);
							}

							// might be null
							property.setType(ctu);
						}
					}
				}
			}
		}
	}

	/**
	 * Extracts the type from the collection type
	 * 
	 * @param type
	 * @return
	 */
	private static String extractTypeFromCollectionType(String type) {
		int start = type.indexOf('('); 
		int end = type.indexOf(')'); 
		type = type.substring(start + 1, end);

		return type;
	}

	/**
	 * @param type
	 * @return - true whether the given type is a collection, false otherwise.
	 */
	private static boolean isCollection(String type) {
		if (type == null) {
			return false;
		}

		return type.startsWith("Collection"); //$NON-NLS-1$
	}

	/**
	 * Builds OData property.
	 * 
	 * @param edmxProperty
	 * @param edmxComplexType
	 * @param edmxSchema
	 * @param edmx
	 * @param objects
	 * @return
	 * @throws BuilderException
	 */
	public static Property build(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty,
			org.eclipse.ogee.client.model.edmx.ComplexType edmxComplexType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxProperty, edmxComplexType, namespace, edmx,
				objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		Property property = (Property) nsObject.get(edmxComplexType.toString()
				+ edmxProperty);
		if (property == null) {
			// create property
			property = OdataFactory.eINSTANCE.createProperty();
			// set property name
			String name = edmxProperty.getName();
			property.setName(name);
			// save property
			nsObject.put(edmxComplexType.toString() + edmxProperty, property);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxProperty
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				property.setDocumentation(documentation);
			}

			// set is nullable
			boolean edmxNullable = edmxProperty.isNullable();
			property.setNullable(edmxNullable);

			// set property type
			EdmTypes edmTypes = EdmTypes.toEdmTypes(edmxProperty.getType());

			String edmxPropertyType = (edmTypes.equals(EdmTypes.NOVALUE) ? edmxProperty
					.getType() : edmTypes.toString());

			if (edmxPropertyType != null) {
				setPropertyType(property, edmxProperty, edmxPropertyType,
						namespace, edmx, objects);
			}
		}

		return property;
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty,
			org.eclipse.ogee.client.model.edmx.ComplexType edmxComplexType,
			String namepsace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxProperty(edmxProperty);

		ComplexTypeBuilder.validateEdmxComplexType(edmxComplexType);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	/**
	 * Handles the type of the property - complex type.
	 * 
	 * @param edmxProperty
	 * @param edmxPropertyType
	 * @return ComplexTypeUsage
	 * @throws BuilderException
	 */
	private static ComplexTypeUsage getComplexTypeUsage(
			org.eclipse.ogee.client.model.edmx.ComplexType edmxComplexType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		ComplexType complexType = ComplexTypeBuilder.build(edmxComplexType,
				namespace, edmx, objects);

		ComplexTypeUsage ctu = OdataFactory.eINSTANCE.createComplexTypeUsage();

		ctu.setComplexType(complexType);

		return ctu;
	}

	/**
	 * Handles the type of the property - simple type.
	 * 
	 * @param edmxProperty
	 * @param edmxPropertyType
	 * @return SimpleTypeUsage
	 */
	private static SimpleTypeUsage getSimpleTypeUsage(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty,
			String edmxPropertyType) {

		edmxPropertyType = edmxPropertyType.replace("Edm.", ""); //$NON-NLS-1$ //$NON-NLS-2$
		EDMTypes edmType = EDMTypes.getByName(edmxPropertyType);

		SimpleType simpleType = OdataFactory.eINSTANCE.createSimpleType();

		String defaultValue = edmxProperty.getAttribute("DefaultValue"); //$NON-NLS-1$
		if (defaultValue != null) {
			SimpleValue simpleValue = null;
			switch (edmType) {
			case BOOLEAN:
				simpleValue = OdataFactory.eINSTANCE.createBooleanValue();
				simpleValue.setValueObject(new Boolean(defaultValue));
				break;
			case BYTE:
				simpleValue = OdataFactory.eINSTANCE.createByteValue();
				simpleValue.setValueObject(Byte.parseByte(defaultValue));
				break;
			case DECIMAL:
				simpleValue = OdataFactory.eINSTANCE.createDecimalValue();
				simpleValue.setValueObject(new BigDecimal(defaultValue));
				break;
			case DOUBLE:
				simpleValue = OdataFactory.eINSTANCE.createDoubleValue();
				simpleValue.setValueObject(new Double(defaultValue));
				break;
			case INT16:
				simpleValue = OdataFactory.eINSTANCE.createInt16Value();
				simpleValue.setValueObject(new Short(defaultValue));
				break;
			case INT32:
				simpleValue = OdataFactory.eINSTANCE.createInt32Value();
				simpleValue.setValueObject(new Integer(defaultValue));
				break;
			case INT64:
				simpleValue = OdataFactory.eINSTANCE.createInt64Value();
				simpleValue.setValueObject(new Long(defaultValue));
				break;
			case SINGLE:
				simpleValue = OdataFactory.eINSTANCE.createSingleValue();
				simpleValue.setValueObject(new Float(defaultValue));
				break;
			case STRING:
				simpleValue = OdataFactory.eINSTANCE.createStringValue();
				simpleValue.setValueObject(defaultValue);
				break;
			case BINARY:
				simpleValue = OdataFactory.eINSTANCE.createBinaryValue();
				simpleValue.setValueObject(defaultValue.getBytes());
				break;
			case DATE_TIME:
				simpleValue = OdataFactory.eINSTANCE.createDateTimeValue();
				simpleValue.setValueObject(new DateTime(DateUtils
						.parse(defaultValue)));
				break;
			case DATE_TIME_OFFSET:
				simpleValue = OdataFactory.eINSTANCE
						.createDateTimeOffsetValue();
				simpleValue.setValueObject(DateTimeOffset
						.fromXMLFormat(defaultValue));
				break;
			case GUID:
				simpleValue = OdataFactory.eINSTANCE.createGuidValue();
				simpleValue.setValueObject(UUID.fromString(defaultValue));
				break;
			case SBYTE:
				simpleValue = OdataFactory.eINSTANCE.createSByteValue();
				simpleValue.setValueObject(Byte.parseByte(defaultValue));
				break;
			case TIME:
				simpleValue = OdataFactory.eINSTANCE.createTimeValue();
				simpleValue.setValueObject(DateTime.fromXMLFormat(defaultValue)
						.getDateValue());
				break;
			default:
				break;
			}
			simpleType.setDefaultValue(simpleValue);
		}

		// Since the type "Float" is only supported in OData V3,
		// we make it as "Single" for now
		if (edmType == null && edmxPropertyType.equals("Float")) //$NON-NLS-1$
		{
			edmType = EDMTypes.SINGLE;
		}

		if (edmType != null) {
			// set type
			simpleType.setType(edmType);

			// set max length
			int maxLength = getMaxLength(edmxProperty);
			simpleType.setMaxLength(maxLength);

			// set fixed length
			int fixedLength = getFixedLength(edmxProperty, maxLength);
			if (fixedLength != -1) {
				simpleType.setFixedLength(fixedLength);
			}

			// set scale
			int scale = getScale(edmxProperty);
			simpleType.setScale(scale);

			// set precision
			int precision = getPrecision(edmxProperty);
			simpleType.setPrecision(precision);

			// set property simple type
			SimpleTypeUsage stu = OdataFactory.eINSTANCE
					.createSimpleTypeUsage();
			stu.setSimpleType(simpleType);

			return stu;
		}

		return null;
	}

	/**
	 * @param edmxProperty
	 * @return - the fixed length of this property
	 */
	private static int getFixedLength(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty,
			int maxLength) {

		// FixedLength can be boolean in 2.0 and int in 3.0,4.0 versions of
		// odata.
		// if the value is "true", then the fixed length is set as the max
		// length.
		// if the value is "false", then no value is set.
		// if the value is an int number, then this number is set as the fixed
		// length.

		int fixedLength = -1;
		// get the fixed length string from the property
		String fixedLengthStr = edmxProperty.getFixedLength();
		if (fixedLengthStr != null) {
			if (fixedLengthStr.equalsIgnoreCase(Boolean.TRUE.toString())) {
				fixedLength = maxLength;
			} else if (fixedLengthStr
					.equalsIgnoreCase(Boolean.FALSE.toString())) {
				return fixedLength; // -1
			} else // int value
			{
				try {
					fixedLength = Integer.valueOf(fixedLengthStr).intValue();
				} catch (NumberFormatException e) {
					Logger.getUtilsLogger().logError(e);
					// continue the build process
				}
			}
		}

		return fixedLength;
	}

	/**
	 * @param edmxProperty
	 * @return - the max length of this property
	 */
	private static int getMaxLength(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty) {
		int maxLength = 0;
		String maxLengthStr = edmxProperty.getMaxLength();
		if (maxLengthStr != null) {
			maxLength = Integer.MAX_VALUE;
			if (!maxLengthStr.equalsIgnoreCase("Max")) //$NON-NLS-1$
			{
				maxLength = Integer.valueOf(maxLengthStr).intValue();
			}
		}

		return maxLength;
	}

	/**
	 * @param edmxProperty
	 * @return - the scale of this property
	 */
	private static int getScale(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty) {
		int scale = 0;
		String scaleStr = edmxProperty.getAttribute("Scale"); //$NON-NLS-1$
		if (scaleStr != null) {
			scale = Integer.valueOf(scaleStr).intValue();
		}

		return scale;
	}

	/**
	 * @param edmxProperty
	 * @return - the precision of this property
	 */
	private static int getPrecision(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty) {
		int precision = 0;
		String precisionStr = edmxProperty.getAttribute("Precision"); //$NON-NLS-1$
		if (precisionStr != null) {
			precision = Integer.valueOf(precisionStr).intValue();
		}

		return precision;
	}

	private static void validateEdmxProperty(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty)
			throws BuilderException {
		if (edmxProperty == null) {
			throw new BuilderException(
					FrameworkImportMessages.PropertyBuilder_1);
		}
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty,
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxProperty(edmxProperty);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}
}
