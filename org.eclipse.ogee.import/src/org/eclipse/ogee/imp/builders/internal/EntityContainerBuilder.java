/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.AssociationSet;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.OdataFactory;

/**
 * Builds an OData EntityContainer object.
 */
public class EntityContainerBuilder {
	/**
	 * Convert Edmx EntityContainer to OData EntityContainer
	 * 
	 * @param edmxEntityContainer
	 * @return - OData EntityContainer
	 * @throws BuilderException
	 */
	public static EntityContainer build(
			org.eclipse.ogee.client.model.edmx.EntityContainer edmxEntityContainer,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxEntityContainer, namespace, edmx, objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		EntityContainer entityContainer = (EntityContainer) nsObject
				.get(edmxEntityContainer);
		if (entityContainer == null) {
			// create entity container
			entityContainer = OdataFactory.eINSTANCE.createEntityContainer();
			// save entity container
			nsObject.put(edmxEntityContainer, entityContainer);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxEntityContainer
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				entityContainer.setDocumentation(documentation);
			}

			// set name
			String name = edmxEntityContainer.getName();
			entityContainer.setName(name);

			// set is default
			boolean isDefault = edmxEntityContainer.isDefault();
			entityContainer.setDefault(isDefault);

			// set association sets
			setAssociationSets(entityContainer, edmxEntityContainer, namespace,
					edmx, objects);

			// set entity sets
			setEntitySets(entityContainer, edmxEntityContainer, namespace,
					edmx, objects);

			// set function imports
			setFunctionImports(entityContainer, edmxEntityContainer, namespace,
					edmx, objects);
		}

		return entityContainer;
	}

	/**
	 * Sets function imports.
	 * 
	 * @param entityContainer
	 * @param edmxEntityContainer
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setFunctionImports(
			EntityContainer entityContainer,
			org.eclipse.ogee.client.model.edmx.EntityContainer edmxEntityContainer,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		org.eclipse.ogee.client.model.edmx.FunctionImport[] edmxFunctionImports = edmxEntityContainer
				.getFunctionImports();
		for (org.eclipse.ogee.client.model.edmx.FunctionImport edmxFunctionImport : edmxFunctionImports) {
			FunctionImport function = (FunctionImport) FunctionImportBuilder
					.build(edmxFunctionImport, namespace, edmx, objects);
			entityContainer.getFunctionImports().add(function);
		}
	}

	/**
	 * Sets entity sets.
	 * 
	 * @param entityContainer
	 * @param edmxEntityContainer
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setEntitySets(
			EntityContainer entityContainer,
			org.eclipse.ogee.client.model.edmx.EntityContainer edmxEntityContainer,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		org.eclipse.ogee.client.model.edmx.EntitySet[] edmxEntitySets = edmxEntityContainer
				.getEntitySets();
		for (org.eclipse.ogee.client.model.edmx.EntitySet edmxEntitySet : edmxEntitySets) {
			EntitySet entitySet = EntitySetBuilder.build(edmxEntitySet,
					namespace, edmx, objects);
			entityContainer.getEntitySets().add(entitySet);
		}
	}

	/**
	 * Sets association sets.
	 * 
	 * @param entityContainer
	 * @param edmxEntityContainer
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setAssociationSets(
			EntityContainer entityContainer,
			org.eclipse.ogee.client.model.edmx.EntityContainer edmxEntityContainer,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		org.eclipse.ogee.client.model.edmx.AssociationSet[] edmxAssociationSets = edmxEntityContainer
				.getAssociationSets();
		for (org.eclipse.ogee.client.model.edmx.AssociationSet edmxAssociationSet : edmxAssociationSets) {
			AssociationSet associationSet = AssociationSetBuilder.build(
					edmxAssociationSet, namespace, edmx, objects);
			entityContainer.getAssociationSets().add(associationSet);
		}
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.EntityContainer edmxEntityContainer,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxEntityContainer(edmxEntityContainer);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	private static void validateEdmxEntityContainer(
			org.eclipse.ogee.client.model.edmx.EntityContainer edmxEntityContainer)
			throws BuilderException {
		if (edmxEntityContainer == null) {
			throw new BuilderException(
					FrameworkImportMessages.EntityContainerBuilder_1);
		}

		String edmxEntityContainerName = edmxEntityContainer.getName();
		if (edmxEntityContainerName == null) {
			throw new BuilderException(
					FrameworkImportMessages.EntityContainerBuilder_0);
		}
	}
}
