/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.OdataFactory;

/**
 * Builds an OData EntitySet object.
 */
public class EntitySetBuilder {
	/**
	 * Converts the given Edmx Entity Set to OData Entity Set.
	 * 
	 * @param edmxEntitySet
	 * @return - OData Entity Set.
	 */
	public static EntitySet build(
			org.eclipse.ogee.client.model.edmx.EntitySet edmxEntitySet,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxEntitySet, namespace, edmx, objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		EntitySet entitySet = (EntitySet) nsObject.get(edmxEntitySet);
		if (entitySet == null) {
			// create entity set
			entitySet = OdataFactory.eINSTANCE.createEntitySet();
			// save entity set
			nsObject.put(edmxEntitySet, entitySet);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxEntitySet
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				entitySet.setDocumentation(documentation);
			}

			// set name
			String name = edmxEntitySet.getName();
			entitySet.setName(name);

			// set type
			String entityTypeName = edmxEntitySet.getEntityType();
			String entityTypeNamespace = EdmxUtilities.getEntityTypeNamespace(
					entityTypeName, edmx);

			if (entityTypeNamespace != null) // may be null because of edmx
												// reference
			{
				org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType = EdmxUtilities
						.getEntityTypeByName(entityTypeName, edmx);
				if (edmxEntityType != null) {
					EntityType entityType = EntityTypeBuilder.build(
							edmxEntityType, entityTypeNamespace, edmx, objects);
					entitySet.setType(entityType);
				}
			}
		}

		return entitySet;
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.EntitySet edmxEntitySet,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxEntitySet(edmxEntitySet);

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	private static void validateEdmxEntitySet(
			org.eclipse.ogee.client.model.edmx.EntitySet edmxEntitySet)
			throws BuilderException {
		if (edmxEntitySet == null) {
			throw new BuilderException(
					FrameworkImportMessages.EntitySetBuilder_1);
		}

		String edmxEntitySetName = edmxEntitySet.getName();
		if (edmxEntitySetName == null) {
			throw new BuilderException(
					FrameworkImportMessages.EntitySetBuilder_0);
		}

		String entityTypeName = edmxEntitySet.getEntityType();
		if (entityTypeName == null) {
			throw new BuilderException(
					FrameworkImportMessages.EntitySetBuilder_5);
		}
	}
}
