/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.Documentation;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;

/**
 * Builds an OData Parameter object.
 */
public class ParameterBuilder {
	private static final String EDM = "Edm."; //$NON-NLS-1$
	private static final String FLOAT = "Float"; //$NON-NLS-1$

	public static Parameter build(
			org.eclipse.ogee.client.model.edmx.Parameter edmxParameter,
			String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects, String functionName)
			throws BuilderException {
		validateInputParameters(edmxParameter, objects);

		Map<Object, Object> nsObject = objects.get(namespace);

		Parameter parameter = (Parameter) nsObject.get(functionName
				+ edmxParameter);
		if (parameter == null) {
			// create parameter
			parameter = OdataFactory.eINSTANCE.createParameter();
			// save parameter
			nsObject.put(functionName + edmxParameter, parameter);

			// set name
			String name = edmxParameter.getName();
			parameter.setName(name);

			// set documentation
			org.eclipse.ogee.client.model.edmx.Documentation edmxDocumentation = edmxParameter
					.getDocumentation();
			if (edmxDocumentation != null) {
				Documentation documentation = DocumentationBuilder
						.build(edmxDocumentation);
				parameter.setDocumentation(documentation);
			}

			// set type
			String edmxParameterType = edmxParameter.getType();
			setParameterType(edmx, edmxParameter, objects, parameter,
					edmxParameterType);
		}

		return parameter;
	}

	/**
	 * Sets the type of the parameter.
	 * 
	 * @param edmx
	 * @param objects
	 * @param parameter
	 * @param edmxParameterType
	 * @throws BuilderException
	 */
	private static void setParameterType(Edmx edmx,
			org.eclipse.ogee.client.model.edmx.Parameter edmxParameter,
			Map<String, Map<Object, Object>> objects, Parameter parameter,
			String edmxParameterType) throws BuilderException {
		if (edmxParameterType != null) {
			if (edmxParameterType.contains(EDM)) {
				if (isCollection(edmxParameterType)) {
					edmxParameterType = extractTypeFromCollectionType(edmxParameterType);
				}

				// simple type
				SimpleType simpleType = OdataFactory.eINSTANCE
						.createSimpleType();
				edmxParameterType = edmxParameterType.replace(EDM, "").trim(); //$NON-NLS-1$
				EDMTypes edmType = EDMTypes.getByName(edmxParameterType);

				// Since the type "Float" is only supported in OData V3,
				// we make it as "Single" for now
				if (edmType == null && edmxParameterType.equals(FLOAT)) {
					edmType = EDMTypes.SINGLE;
				}

				if (edmType != null) {
					simpleType.setType(edmType);

					// set max length
					int maxLength = getMaxLength(edmxParameter);
					simpleType.setMaxLength(maxLength);

					// Fix for CSN 0000162339 2014
					// set scale
					int scale = getScale(edmxParameter);
					simpleType.setScale(scale);

					// set precision
					int precision = getPrecision(edmxParameter);
					simpleType.setPrecision(precision);

					SimpleTypeUsage stu = OdataFactory.eINSTANCE
							.createSimpleTypeUsage();
					stu.setSimpleType(simpleType);

					parameter.setType(stu);
				}
			} else if (edmxParameterType.contains(".")) //$NON-NLS-1$
			{
				// the type can be either EntityType/ComplexType/EnumType
				// or collection of any of them.
				// EnumType is being taken care of in a different class (V3).
				// Also the type can come from a reference, in this case it will
				// be set as null here.

				int indexOfFirstDot = edmxParameterType.indexOf('.');
				// get the parameter type's namespace
				String parameterTypeNamespace = edmxParameterType.substring(0,
						indexOfFirstDot);

				boolean match = false;
				// get all the namespaces in the edmx
				Schema[] schemas = edmx.getEdmxDataServices().getSchemas();
				// go over them
				for (Schema schema : schemas) {
					String schemaNamespace = schema.getNamespace();
					if (schemaNamespace.equals(parameterTypeNamespace)) {
						match = true;
					}
				}

				if (!match) {
					// the type of this parameter comes from a reference
					// or doesn't exist so it needs to be set as null here.
					parameter.setType(null);
					return;
				}

				if (isCollection(edmxParameterType)) {
					edmxParameterType = extractTypeFromCollectionType(edmxParameterType);
				}

				String entityTypeNamespace = EdmxUtilities
						.getEntityTypeNamespace(edmxParameterType, edmx);

				if (entityTypeNamespace != null) // may be null due to edmx
													// reference
				{
					// try to build an entity type
					org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType = EdmxUtilities
							.getEntityTypeByName(edmxParameterType, edmx);

					if (edmxEntityType != null) // entity type
					{
						EntityTypeUsage entityTypeUsage = OdataFactory.eINSTANCE
								.createEntityTypeUsage();

						EntityType entityType = EntityTypeBuilder.build(
								edmxEntityType, entityTypeNamespace, edmx,
								objects);

						entityTypeUsage.setEntityType(entityType);

						parameter.setType(entityTypeUsage);
					} else {
						// try to build a complex type
						org.eclipse.ogee.client.model.edmx.ComplexType edmxComplexType = EdmxUtilities
								.getComplexTypeByName(edmxParameterType, edmx);

						if (edmxComplexType != null) // complex type
						{
							ComplexTypeUsage complexTypeUsage = OdataFactory.eINSTANCE
									.createComplexTypeUsage();

							String complexTypeNamespace = EdmxUtilities
									.getComplexTypeNamespace(edmxParameterType,
											edmx);
							ComplexType complexType = ComplexTypeBuilder.build(
									edmxComplexType, complexTypeNamespace,
									edmx, objects);

							complexTypeUsage.setComplexType(complexType);

							parameter.setType(complexTypeUsage);
						}
					}
				}
			}
		}
	}

	/**
	 * @param edmxParameter
	 * @return - the max length of this property
	 */
	private static int getMaxLength(
			org.eclipse.ogee.client.model.edmx.Parameter edmxParameter) {
		int maxLength = 0;
		String maxLengthStr = edmxParameter.getMaxLength();
		if (maxLengthStr != null) {
			maxLength = Integer.MAX_VALUE;
			if (!maxLengthStr.equalsIgnoreCase("Max")) //$NON-NLS-1$
			{
				maxLength = Integer.valueOf(maxLengthStr).intValue();
			}
		}

		return maxLength;
	}

	/**
	 * @param edmxParameter
	 * @return - the scale of this parameter
	 */
	private static int getScale(
			org.eclipse.ogee.client.model.edmx.Parameter edmxParameter) {
		int scale = 0;
		String scaleStr = edmxParameter.getAttribute("Scale"); //$NON-NLS-1$
		if (scaleStr != null) {
			scale = Integer.valueOf(scaleStr).intValue();
		}

		return scale;
	}

	/**
	 * @param edmxParameter
	 * @return - the precision of this parameter
	 */
	private static int getPrecision(
			org.eclipse.ogee.client.model.edmx.Parameter edmxParameter) {
		int precision = 0;
		String precisionStr = edmxParameter.getAttribute("Precision"); //$NON-NLS-1$
		if (precisionStr != null) {
			precision = Integer.valueOf(precisionStr).intValue();
		}

		return precision;
	}

	/**
	 * @param type
	 * @return - true whether the given type is a collection, false otherwise.
	 */
	private static boolean isCollection(String type) {
		if (type == null) {
			return false;
		}

		return type.startsWith("Collection"); //$NON-NLS-1$
	}

	/**
	 * Extracts the type from the collection type
	 * 
	 * @param type
	 * @return
	 */
	private static String extractTypeFromCollectionType(String type) {
		int start = type.indexOf('(');
		int end = type.indexOf(')');
		type = type.substring(start + 1, end);

		return type;
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.Parameter edmxParameter,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		if (edmxParameter == null) {
			throw new BuilderException(
					FrameworkImportMessages.ParameterBuilder_1);
		} else if (objects == null) {
			throw new BuilderException(
					FrameworkImportMessages.ParameterBuilder_2);
		}

		String edmxParameterName = edmxParameter.getName();
		if (edmxParameterName == null) {
			throw new BuilderException(
					FrameworkImportMessages.ParameterBuilder_0);
		}
	}
}
