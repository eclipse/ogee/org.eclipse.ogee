/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.client.parser.impl.util.EdmxUtilities;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.AssociationSetEnd;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Role;

/**
 * Builds an OData AssociationSetEnd object.
 */
public class AssociationSetEndBuilder {
	/**
	 * Converts the given Edmx End object to AssociationSetEnd object.
	 * 
	 * @param edmxAssociationSetEnd
	 * @param namespace
	 * @return - AssociationSetEnd object.
	 */
	public static AssociationSetEnd build(
			org.eclipse.ogee.client.model.edmx.End edmxAssociationSetEnd,
			String associationSetName, String entitySetName,
			org.eclipse.ogee.client.model.edmx.Association edmxAssociation,
			String associationNamespace, String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxAssociationSetEnd, entitySetName,
				associationNamespace, edmx, objects);

		String associationName = edmxAssociation.getName();
		String key = getKey(edmxAssociationSetEnd, entitySetName, namespace,
				associationSetName, associationName);

		Map<Object, Object> nsObjects = objects.get(namespace);
		AssociationSetEnd associationSetEnd = (AssociationSetEnd) nsObjects
				.get(key);
		if (associationSetEnd == null) {
			// create association set end
			associationSetEnd = OdataFactory.eINSTANCE
					.createAssociationSetEnd();
			// save association set end
			nsObjects.put(key, associationSetEnd);

			// set role
			Role role = RoleBuilder.build(edmxAssociationSetEnd,
					edmxAssociation, associationNamespace, edmx, objects);
			associationSetEnd.setRole(role);

			// set entity set
			org.eclipse.ogee.client.model.edmx.EntitySet edmxEntitySet = EdmxUtilities
					.getEntitySetByName(entitySetName, edmx);
			if (edmxEntitySet != null) {
				EntitySet entitySet = EntitySetBuilder.build(edmxEntitySet,
						namespace, edmx, objects);
				associationSetEnd.setEntitySet(entitySet);
			}
		}

		return associationSetEnd;
	}

	private static String getKey(
			org.eclipse.ogee.client.model.edmx.End edmxEnd,
			String entitySetName, String namespace, String associationSetName,
			String associationName) {
		return associationSetName
				+ "/" + associationName + "/" + edmxEnd.getRole() + "/" + entitySetName + "/" + namespace; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.End edmxEnd,
			String entitySetName, String namespace, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxAssociationSetEnd(edmxEnd, entitySetName);

		SchemaBuilder.validateNamespace(namespace);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	private static void validateEdmxAssociationSetEnd(
			org.eclipse.ogee.client.model.edmx.End edmxEnd,
			String entitySetName) throws BuilderException {
		if (edmxEnd == null) {
			throw new BuilderException(
					FrameworkImportMessages.AssociationSetEndBuilder_1);
		} else if (entitySetName == null) {
			throw new BuilderException(
					FrameworkImportMessages.AssociationSetEndBuilder_2);
		}

		String roleName = edmxEnd.getRole();
		if (roleName == null) {
			throw new BuilderException(
					FrameworkImportMessages.AssociationSetEndBuilder_3);
		}
	}
}
