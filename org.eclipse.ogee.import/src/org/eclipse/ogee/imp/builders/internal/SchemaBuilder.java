/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders.internal;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.EntityContainer;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.Using;

/**
 * Builds an OData Service object.
 */
public class SchemaBuilder {
	/**
	 * Converts an Edmx to OData Service.
	 * 
	 * @param edmxSchema
	 * @param edmx
	 * @param objects
	 * @return
	 * @throws BuilderException
	 */
	public static Schema build(
			org.eclipse.ogee.client.model.edmx.Schema edmxSchema, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateInputParameters(edmxSchema, edmx, objects);

		String namespace = edmxSchema.getNamespace();
		Map<Object, Object> nsObjects = objects.get(namespace);
		if (nsObjects == null) {
			nsObjects = new HashMap<Object, Object>();
			objects.put(namespace, nsObjects);
		}

		Schema schema = (Schema) nsObjects.get(edmxSchema);
		if (schema == null) {
			schema = OdataFactory.eINSTANCE.createSchema();
			schema.getClassifiers().add(SchemaClassifier.SERVICE);

			// save schema
			nsObjects.put(edmxSchema, schema);

			// set usings
			setUsings(schema, edmxSchema, edmx, objects);

			// set namespace
			schema.setNamespace(namespace);

			// set alias
			String alias = edmxSchema.getAlias();
			if (alias != null) {
				schema.setAlias(alias);
			}
			// set associations
			setAssociations(schema, edmxSchema, edmx, objects);

			// set entity types
			setEntityTypes(schema, edmxSchema, edmx, objects);

			// set complex types
			setComplexTypes(schema, edmxSchema, edmx, objects);

			// set entity containers
			setEntityContainers(schema, edmxSchema, edmx, objects);
		}

		return schema;
	}

	/**
	 * Set usings.
	 * 
	 * @param schema
	 * @param edmxSchema
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setUsings(Schema schema,
			org.eclipse.ogee.client.model.edmx.Schema edmxSchema, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		String namespace = edmxSchema.getNamespace();

		org.eclipse.ogee.client.model.edmx.Using[] edmxUsings = edmxSchema
				.getUsings();
		for (org.eclipse.ogee.client.model.edmx.Using edmxUsing : edmxUsings) {
			Using using = UsingBuilder.build(edmxUsing, namespace, edmx,
					objects);
			schema.getUsings().add(using);
		}
	}

	/**
	 * Sets entity types.
	 * 
	 * @param schema
	 * @param edmxSchema
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setEntityTypes(Schema schema,
			org.eclipse.ogee.client.model.edmx.Schema edmxSchema, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		org.eclipse.ogee.client.model.edmx.EntityType[] edmxEntityTypes = edmxSchema
				.getEntityTypes();
		for (org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType : edmxEntityTypes) {
			EntityType entityType = EntityTypeBuilder.build(edmxEntityType,
					edmxSchema.getNamespace(), edmx, objects);
			schema.getEntityTypes().add(entityType);
		}
	}

	/**
	 * Sets entity containers.
	 * 
	 * @param schema
	 * @param edmxSchema
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setEntityContainers(Schema schema,
			org.eclipse.ogee.client.model.edmx.Schema edmxSchema, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		String namespace = edmxSchema.getNamespace();

		org.eclipse.ogee.client.model.edmx.EntityContainer[] edmxEntityContainers = edmxSchema
				.getEntityContainers();
		for (org.eclipse.ogee.client.model.edmx.EntityContainer edmxEntityContainer : edmxEntityContainers) {
			EntityContainer entityContainer = EntityContainerBuilder.build(
					edmxEntityContainer, namespace, edmx, objects);
			schema.getContainers().add(entityContainer);
		}
	}

	/**
	 * Sets complex types.
	 * 
	 * @param schema
	 * @param edmxSchema
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setComplexTypes(Schema schema,
			org.eclipse.ogee.client.model.edmx.Schema edmxSchema, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		org.eclipse.ogee.client.model.edmx.ComplexType[] edmxComplexTypes = edmxSchema
				.getComplexTypes();
		for (org.eclipse.ogee.client.model.edmx.ComplexType edmxComplexType : edmxComplexTypes) {
			ComplexType complexType = ComplexTypeBuilder.build(edmxComplexType,
					edmxSchema.getNamespace(), edmx, objects);
			schema.getComplexTypes().add(complexType);
		}
	}

	/**
	 * Sets associations.
	 * 
	 * @param schema
	 * @param edmxSchema
	 * @param edmx
	 * @param objects
	 * @throws BuilderException
	 */
	private static void setAssociations(Schema service,
			org.eclipse.ogee.client.model.edmx.Schema edmxSchema, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		String namespace = edmxSchema.getNamespace();

		org.eclipse.ogee.client.model.edmx.Association[] edmxAssociations = edmxSchema
				.getAssociations();
		for (org.eclipse.ogee.client.model.edmx.Association edmxAssociation : edmxAssociations) {
			Association association = AssociationBuilder.build(edmxAssociation,
					namespace, edmx, objects);
			service.getAssociations().add(association);
		}
	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.Schema edmxSchema, Edmx edmx,
			Map<String, Map<Object, Object>> objects) throws BuilderException {
		validateEdmxSchema(edmxSchema);

		EDMXBuilder.validateCommonParameters(edmx, objects);
	}

	private static void validateEdmxSchema(
			org.eclipse.ogee.client.model.edmx.Schema edmxSchema)
			throws BuilderException {
		if (edmxSchema == null) {
			throw new BuilderException(FrameworkImportMessages.SchemaBuilder_4);
		}

		String namespace = edmxSchema.getNamespace();
		validateNamespace(namespace);
	}

	public static void validateNamespace(String namespace)
			throws BuilderException {
		if (namespace == null) {
			throw new BuilderException(FrameworkImportMessages.SchemaBuilder_5);
		}
	}
}
