/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.builders;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.ogee.client.model.edmx.v3.EdmxV3;
import org.eclipse.ogee.imp.builders.internal.EDMXBuilder;
import org.eclipse.ogee.imp.buildersV3.AddReferenceUtil;
import org.eclipse.ogee.imp.buildersV3.BuilderV3Util;
import org.eclipse.ogee.imp.buildersV3.EDMXSetBuilderV3;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IResourceContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.vocabularies.ICapabilityVocabulary;
import org.eclipse.ogee.model.api.vocabularies.IMeasuresVocabulary;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXReference;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.osgi.util.NLS;

/**
 * Builds an OData Model EDMXSet object.
 */
public class ODataModelEDMXSetBuilder {
	/**
	 * Builds an OData Model EDMXSet object by the given Edmx object, that is in
	 * version 2.0.
	 * 
	 * @param edmxV2
	 * @return - EDMXSet
	 * @throws BuilderException
	 */
	public static EDMXSet build(org.eclipse.ogee.client.model.edmx.Edmx edmxV2,
			String uri) throws BuilderException {
		EDMXSet odataEdmxSet = OdataFactory.eINSTANCE.createEDMXSet();

		updateEdmxSet(odataEdmxSet, edmxV2, null, uri);
		return odataEdmxSet;
	}

	/**
	 * Builds an OData Model EDMXSet object by the given Edmx object, that is in
	 * version 2.0.
	 * 
	 * @param edmxV2
	 * @return - EDMXSet
	 * @throws BuilderException
	 */
	public static EDMXSet updateEdmxSet(EDMXSet odataEdmxSet,
			org.eclipse.ogee.client.model.edmx.Edmx edmxV2,
			EDMX parentOdataEdmx, String uri) throws BuilderException {
		try {
			Map<String, Map<Object, Object>> odataObjectsMap = new ConcurrentHashMap<String, Map<Object, Object>>();

			AddReferenceUtil addReferenceUtil = new AddReferenceUtil();
			String dupNamspace = addReferenceUtil
					.checkMainNamespaceDuplication(edmxV2, odataEdmxSet);
			if (dupNamspace != null) {
				String message = NLS.bind(BuilderV3Messages.addReference01,
						dupNamspace);
				throw new BuilderException(message);

			}
			if (addReferenceUtil.skipRefNamespaceDuplication(edmxV2,
					odataEdmxSet)) {
				return odataEdmxSet;
			}
			EDMX odataEdmx = EDMXBuilder.build(edmxV2, uri, odataObjectsMap);

			boolean mainEdmx = false;
			if (odataEdmxSet.getMainEDMX() == null) {
				odataEdmxSet.setMainEDMX(odataEdmx);
				odataEdmxSet.getSchemata().addAll(
						odataEdmx.getDataService().getSchemata());

				IModelContext.INSTANCE.getVocabularyContext(odataEdmxSet)
						.provideVocabulary(ICapabilityVocabulary.VC_NAMESPACE);
				IModelContext.INSTANCE.getVocabularyContext(odataEdmxSet)
						.provideVocabulary(IMeasuresVocabulary.VC_NAMESPACE);
				mainEdmx = true;
			}

			// Checks if XML version is 3.0 and build the relevant EDMXSet
			// elements for OData V3
			String getmDataServiceVersion = edmxV2.getEdmxDataServices()
					.getmDataServiceVersion();
			BuilderV3Util builderV3Util = new BuilderV3Util();
			if (builderV3Util.checkVersion(getmDataServiceVersion)) {
				if (mainEdmx) {
					odataEdmxSet = EDMXSetBuilderV3.build(odataEdmxSet,
							(EdmxV3) edmxV2, null, odataObjectsMap);
				} else {
					odataEdmxSet = EDMXSetBuilderV3.build(odataEdmxSet,
							(EdmxV3) edmxV2, odataEdmx, odataObjectsMap);
				}

			}

			if (!mainEdmx) {

				dupNamspace = addReferenceUtil.checkNamespaceDuplication(
						odataEdmx, odataEdmxSet);
				if (dupNamspace != null) {
					String message = NLS.bind(BuilderV3Messages.addReference01,
							dupNamspace);
					throw new BuilderException(message);

				}
				if (parentOdataEdmx == null) {
					IResourceContext resourceContextImpl = IModelContext.INSTANCE
							.getResourceContext(odataEdmxSet);
					resourceContextImpl.addEDMX(odataEdmx);
				} else {
					EDMXReference edmxReference = OdataFactory.eINSTANCE
							.createEDMXReference();
					edmxReference.setReferencedEDMX(odataEdmx);
					parentOdataEdmx.getReferences().add(edmxReference);
					IResourceContext resourceContextImpl = IModelContext.INSTANCE
							.getResourceContext(odataEdmxSet);
					resourceContextImpl.addEDMX(odataEdmx);
				}

			}

			return odataEdmxSet;
		} catch (ModelAPIException e) {
			// wrap ModelAPIException in a BuilderException and throw
			throw new BuilderException(e);
		}
	}

}
