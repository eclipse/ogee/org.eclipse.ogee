/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.nls.messages;

import org.eclipse.osgi.util.NLS;

public class FrameworkImportMessages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.ogee.imp.nls.messages.frameworkimport"; //$NON-NLS-1$

	public static String AssociationBuilder_0;

	public static String AssociationSetBuilder_0;

	public static String AssociationSetBuilder_1;

	public static String AssociationSetBuilder_5;

	public static String AssociationSetEndBuilder_1;

	public static String AssociationSetEndBuilder_2;

	public static String AssociationSetEndBuilder_3;

	public static String ComplexTypeBuilder_0;

	public static String ComplexTypeBuilder_1;

	public static String Converter_0;

	public static String DocumentationBuilder_0;

	public static String EDMXBuilder_0;

	public static String EDMXBuilder_3;

	public static String EntityContainerBuilder_0;

	public static String EntityContainerBuilder_1;

	public static String EntitySetBuilder_0;

	public static String EntitySetBuilder_1;

	public static String EntitySetBuilder_5;

	public static String EntityTypeBuilder_0;

	public static String EntityTypeBuilder_1;


	public static String FunctionBuilder_1;

	public static String FunctionImportBuilder_0;

	public static String NavigationPropertyBuilder_0;

	public static String NavigationPropertyBuilder_1;

	public static String ParameterBuilder_0;

	public static String ParameterBuilder_1;

	public static String ParameterBuilder_2;


	public static String PropertyBuilder_1;

	public static String ReferentialConstraintBuilder_1;

	public static String ReferentialConstraintBuilder_4;

	public static String ReferentialConstraintBuilder_5;

	public static String ReferentialConstraintBuilder_6;

	public static String ReferentialConstraintBuilder_7;

	
	public static String RoleBuilder_0;

	public static String RoleBuilder_10;

	public static String RoleBuilder_5;

	

	public static String SchemaBuilder_4;

	public static String SchemaBuilder_5;

	public static String UsingBuilder_0;

	public static String UsingBuilder_1;

	public static String UsingBuilder_2;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, FrameworkImportMessages.class);
	}

	private FrameworkImportMessages() {
		// Default constructor
	}
}
