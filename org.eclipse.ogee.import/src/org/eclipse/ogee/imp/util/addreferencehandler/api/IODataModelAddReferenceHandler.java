/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.util.addreferencehandler.api;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ogee.imp.buildersV3.ODataModelAddReferenceHandler;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.service.validation.Result;

public interface IODataModelAddReferenceHandler {
	static final IODataModelAddReferenceHandler INSTANCE = new ODataModelAddReferenceHandler();

	/**
	 * The method adds a reference to the main EDMXSet object
	 * 
	 * @param Result
	 *            - Reference EdmxSet object
	 * @param currEdmxSet
	 *            - The main EdmxSet object
	 * @return - Status of the process
	 */
	public IStatus addReference(Result referencedEdmxResult, EDMXSet currEdmxSet);

}
