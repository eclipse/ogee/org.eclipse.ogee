/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.util.builderV3.nls.messages;

import org.eclipse.osgi.util.NLS;

public class BuilderV3Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3"; //$NON-NLS-1$
	public static String AddAnnotation2EDMXSetObects_annotationsTargetNotExist;
	public static String addReference01;
	public static String addAnnotationToObjectError01;
	public static String EnumBuilderV3_0;
	public static String EnumBuilderV3_1;
	public static String EnumBuilderV3_2;
	public static String ValueTermV3_1;
	public static String ValueTermV3_2;
	public static String ValueTermV3_3;
	public static String TypeError_0;
	public static String TypeError_2;
	public static String addAnnotationToObjectError02;
	public static String addAnnotationToObjectError03;
	public static String addAnnotationToObjectError04;
	public static String addReference02;
	public static String addReference03;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, BuilderV3Messages.class);
	}

	private BuilderV3Messages() {
		// Default constructor
	}
}
