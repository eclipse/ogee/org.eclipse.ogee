/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.client.model.edmx.Parameter;
import org.eclipse.ogee.client.model.edmx.Property;
import org.eclipse.ogee.client.model.edmx.Schema;
import org.eclipse.ogee.client.model.edmx.v3.Annotations;
import org.eclipse.ogee.client.model.edmx.v3.BaseExpressionElement;
import org.eclipse.ogee.client.model.edmx.v3.ComplexTypeV3;
import org.eclipse.ogee.client.model.edmx.v3.EdmxV3;
import org.eclipse.ogee.client.model.edmx.v3.EntityContainerV3;
import org.eclipse.ogee.client.model.edmx.v3.EntitySetV3;
import org.eclipse.ogee.client.model.edmx.v3.EntityTypeV3;
import org.eclipse.ogee.client.model.edmx.v3.EnumType;
import org.eclipse.ogee.client.model.edmx.v3.FunctionImportV3;
import org.eclipse.ogee.client.model.edmx.v3.NavigationPropertyV3;
import org.eclipse.ogee.client.model.edmx.v3.ParameterV3;
import org.eclipse.ogee.client.model.edmx.v3.PropertyV3;
import org.eclipse.ogee.client.model.edmx.v3.PropertyValue;
import org.eclipse.ogee.client.model.edmx.v3.SchemaV3;
import org.eclipse.ogee.client.model.edmx.v3.ValueAnnotation;
import org.eclipse.ogee.client.model.edmx.v3.expressions.Record;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.buildersV3.api.IAddAnnotation2EDMXSetObects;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.IVocabulary;
import org.eclipse.ogee.model.api.IVocabularyContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.AnnotationValue;
import org.eclipse.ogee.model.odata.BinaryValue;
import org.eclipse.ogee.model.odata.BooleanValue;
import org.eclipse.ogee.model.odata.ByteValue;
import org.eclipse.ogee.model.odata.CollectableExpression;
import org.eclipse.ogee.model.odata.ConstantExpression;
import org.eclipse.ogee.model.odata.DateTimeOffsetValue;
import org.eclipse.ogee.model.odata.DateTimeValue;
import org.eclipse.ogee.model.odata.DecimalValue;
import org.eclipse.ogee.model.odata.DoubleValue;
import org.eclipse.ogee.model.odata.DynamicExpression;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.GuidValue;
import org.eclipse.ogee.model.odata.IAnnotationTarget;
import org.eclipse.ogee.model.odata.Int16Value;
import org.eclipse.ogee.model.odata.Int32Value;
import org.eclipse.ogee.model.odata.Int64Value;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.PathValue;
import org.eclipse.ogee.model.odata.RecordValue;
import org.eclipse.ogee.model.odata.SimpleValue;
import org.eclipse.ogee.model.odata.SingleValue;
import org.eclipse.ogee.model.odata.StringValue;
import org.eclipse.ogee.model.odata.ValueCollection;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.ogee.model.odata.impl.PropertyToValueMapImpl;
import org.eclipse.ogee.model.odata.impl.ValueCollectionImpl;
import org.eclipse.ogee.model.odata.util.DateTime;
import org.eclipse.ogee.model.odata.util.DateTimeOffset;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.osgi.util.NLS;

public class AddAnnotation2EDMXSetObects implements
		IAddAnnotation2EDMXSetObects {

	// Private class parameters
	private Map<String, Map<Object, Object>> objectsList;
	private String vocabularyName;
	private String termName;
	private Map<String, IVocabulary> usedVocabularies = new HashMap<String, IVocabulary>();
	private EDMXSet edmxSet;
	private String schemaNameSpace;
	private org.eclipse.ogee.model.odata.Schema schema;
	private IVocabulary vocabulary;
	private BuilderV3Util builderV3Util;
	private String entityType;
	private String complexType;
	private Logger logger = Logger.getLogger(AddAnnotation2EDMXSetObects.class
			.getName());

	@Override
	public void annotateEDMXSetobjects(
			Map<String, Map<Object, Object>> objects, EdmxV3 xmlEdmx,
			EDMXSet edmxSet) throws BuilderException {
		// This method adds, if needed, annotations to relevant EDMXSet object
		// Local declaration
		ValueAnnotation[] xmlValueAnnotationsList;

		this.builderV3Util = new BuilderV3Util();
		this.objectsList = objects;
		this.edmxSet = edmxSet;

		// Loop on the schema children that potentially can be annotated
		Schema[] xmlSchemas = xmlEdmx.getEdmxDataServices().getSchemas();

		for (Schema xmlSchemaEntry : xmlSchemas) {
			SchemaV3 xmlSchemaV3 = ((SchemaV3) xmlSchemaEntry);
			this.schemaNameSpace = xmlSchemaV3.getNamespace();
			this.schema = (org.eclipse.ogee.model.odata.Schema) this.objectsList
					.get(this.schemaNameSpace).get(xmlSchemaEntry);

			// Annotations
			Annotations[] annotationList = xmlSchemaV3.getAnnotations();
			for (Annotations currAnnotations : annotationList) {
				// Get target EdmxSet object
				String target = currAnnotations.getTarget();
				if (target != null) {
					IAnnotationTarget targetElement = getTargetElement(edmxSet,
							currAnnotations.getTarget(), xmlSchemaV3);
					// Add ValueAnnotations to EDMXSet Object
					addValueAnnotations2Object(targetElement,
							currAnnotations.getValueAnnotations());
				}
			}

			// EntityType
			EntityType[] xmlEntityTypes = xmlSchemaV3.getEntityTypes();
			for (EntityType xmlEntityTypeEntry : xmlEntityTypes) {
				EntityTypeV3 xmlEntityTypeV3 = ((EntityTypeV3) xmlEntityTypeEntry);
				// Get Annotations
				xmlValueAnnotationsList = xmlEntityTypeV3.getValueAnnotations();
				if (xmlValueAnnotationsList.length > 0) {
					// Get respective EDMXSet EntityType object
					org.eclipse.ogee.model.odata.EntityType entityTypeObject = (org.eclipse.ogee.model.odata.EntityType) this.objectsList
							.get(this.schemaNameSpace).get(xmlEntityTypeEntry);
					// Add ValueAnnotations to EntityType EDMXSet Object
					addValueAnnotations2Object(entityTypeObject,
							xmlValueAnnotationsList);
				}

				this.entityType = xmlEntityTypeEntry.toString();

				// Property
				Property[] xmlProperties = xmlEntityTypeV3.getProperties();
				for (Property xmlPropertyEntry : xmlProperties) {
					PropertyV3 xmlPropertyV3 = ((PropertyV3) xmlPropertyEntry);
					// Get Annotations
					xmlValueAnnotationsList = xmlPropertyV3
							.getValueAnnotations();
					if (xmlValueAnnotationsList.length > 0) {

						// Get respective EDMXSet Property object
						org.eclipse.ogee.model.odata.Property propertyObject = (org.eclipse.ogee.model.odata.Property) this.objectsList
								.get(this.schemaNameSpace).get(
										this.entityType + xmlPropertyEntry);
						// Add valueAnnotations to Property object
						addValueAnnotations2Object(propertyObject,
								xmlValueAnnotationsList);
					}
				}

				// NavigationProperty
				NavigationProperty[] xmlNavigationProperties = xmlEntityTypeV3
						.getNavigationProperties();
				for (NavigationProperty xmlNavigationPropertyEntry : xmlNavigationProperties) {
					NavigationPropertyV3 xmlNavigationPropertyV3 = ((NavigationPropertyV3) xmlNavigationPropertyEntry);
					// Get Annotations
					xmlValueAnnotationsList = xmlNavigationPropertyV3
							.getValueAnnotations();

					if (xmlValueAnnotationsList.length > 0) {
						// Get respective EDMXSet NavigationProperty object
						org.eclipse.ogee.model.odata.NavigationProperty navigationProperty = (org.eclipse.ogee.model.odata.NavigationProperty) this.objectsList
								.get(this.schemaNameSpace).get(
										xmlNavigationPropertyEntry);

						// Add valueAnnotations to NavigationProperty object
						addValueAnnotations2Object(navigationProperty,
								xmlValueAnnotationsList);
					}
				}
			}

			// ComplexType
			ComplexType[] xmlComplexTypes = xmlSchemaV3.getComplexTypes();
			for (ComplexType xmlComplexTypeEntry : xmlComplexTypes) {
				ComplexTypeV3 xmlComplexTypeV3 = ((ComplexTypeV3) xmlComplexTypeEntry);
				// Get Annotations
				xmlValueAnnotationsList = xmlComplexTypeV3
						.getValueAnnotations();

				if (xmlValueAnnotationsList.length > 0) {
					// Get respective EDMXSet ComplexType object
					org.eclipse.ogee.model.odata.ComplexType complexTypeObject = (org.eclipse.ogee.model.odata.ComplexType) this.objectsList
							.get(this.schemaNameSpace).get(xmlComplexTypeEntry);
					// Add valueAnnotations to NavigationProperty object
					addValueAnnotations2Object(complexTypeObject,
							xmlValueAnnotationsList);
				}

				this.complexType = xmlComplexTypeEntry.toString();

				// Property
				Property[] xmlProperties = xmlComplexTypeV3.getProperties();
				for (Property xmlPropertyEntry : xmlProperties) {
					PropertyV3 xmlPropertyV3 = ((PropertyV3) xmlPropertyEntry);
					// Get Annotations
					xmlValueAnnotationsList = xmlPropertyV3
							.getValueAnnotations();
					if (xmlValueAnnotationsList.length > 0) {
						// Get respective EDMXSet Property object
						org.eclipse.ogee.model.odata.Property propertyObject = (org.eclipse.ogee.model.odata.Property) this.objectsList
								.get(this.schemaNameSpace).get(
										this.complexType + xmlPropertyEntry);
						// Add valueAnnotations to Property object
						addValueAnnotations2Object(propertyObject,
								xmlValueAnnotationsList);
					}
				}
			}

			// EnumType
			EnumType[] xmlEnumTypes = xmlSchemaV3.getEnumTypes();
			for (EnumType xmlEnumTypeEntry : xmlEnumTypes) {
				// Get Annotations
				xmlValueAnnotationsList = xmlEnumTypeEntry
						.getValueAnnotations();
				if (xmlValueAnnotationsList.length > 0) {
					// Get respective EDMXSet EnumType object
					org.eclipse.ogee.model.odata.EnumType enumTypeObject = (org.eclipse.ogee.model.odata.EnumType) this.objectsList
							.get(this.schemaNameSpace).get(xmlEnumTypeEntry);
					// Add valueAnnotations to EnumType object
					addValueAnnotations2Object(
							(IAnnotationTarget) enumTypeObject,
							xmlValueAnnotationsList);
				}
			}

			// ValueTerm
			org.eclipse.ogee.client.model.edmx.v3.ValueTerm[] xmlValueTerms = xmlSchemaV3
					.getValueTerms();
			for (org.eclipse.ogee.client.model.edmx.v3.ValueTerm xmlValueTermEntry : xmlValueTerms) {
				// Get Annotations
				xmlValueAnnotationsList = xmlValueTermEntry
						.getValueAnnotations();
				if (xmlValueAnnotationsList.length > 0) {
					// Get respective EDMXSet ValueTerm object
					org.eclipse.ogee.model.odata.ValueTerm valueTermObject = (org.eclipse.ogee.model.odata.ValueTerm) this.objectsList
							.get(this.schemaNameSpace).get(xmlValueTermEntry);
					// Add valueAnnotations to ValueTerm object
					addValueAnnotations2Object(valueTermObject,
							xmlValueAnnotationsList);
				}
			}

			// EntityContainer
			EntityContainer[] xmlEntityContainers = xmlSchemaV3
					.getEntityContainers();
			for (EntityContainer xmlEntityContainerEntry : xmlEntityContainers) {

				EntityContainerV3 xmlEntityContainerV3 = (EntityContainerV3) xmlEntityContainerEntry;
				ValueAnnotation[] xmlValueAnnotations = xmlEntityContainerV3
						.getValueAnnotations();
				if (xmlValueAnnotations.length > 0) {
					// Get respective EDMXSet EntityContainer object
					org.eclipse.ogee.model.odata.EntityContainer entityContainerObject = (org.eclipse.ogee.model.odata.EntityContainer) this.objectsList
							.get(this.schemaNameSpace).get(
									xmlEntityContainerEntry);
					// Add valueAnnotations to EntityContainer object
					addValueAnnotations2Object(entityContainerObject,
							xmlValueAnnotations);
				}

				// EntitySet
				EntitySet[] xmlEntitySets = xmlEntityContainerV3
						.getEntitySets();
				for (EntitySet xmlentitySetEntry : xmlEntitySets) {
					EntitySetV3 xmlEntitySetV3 = ((EntitySetV3) xmlentitySetEntry);
					// Get Annotations
					xmlValueAnnotationsList = xmlEntitySetV3
							.getValueAnnotations();
					if (xmlValueAnnotationsList.length > 0) {
						// Get respective EDMXSet to EntitySet object
						org.eclipse.ogee.model.odata.EntitySet entitySetObject = (org.eclipse.ogee.model.odata.EntitySet) this.objectsList
								.get(this.schemaNameSpace).get(
										xmlentitySetEntry);
						// Add valueAnnotations EntitySet object
						addValueAnnotations2Object(entitySetObject,
								xmlValueAnnotationsList);
					}
				}

				// FunctionImport
				FunctionImport[] xmlFunctionImports = xmlEntityContainerV3
						.getFunctionImports();
				for (FunctionImport xmlFunctionImportEntry : xmlFunctionImports) {
					FunctionImportV3 xmlFunctionImportV3 = ((FunctionImportV3) xmlFunctionImportEntry);
					// Get Annotations
					xmlValueAnnotationsList = xmlFunctionImportV3
							.getValueAnnotations();
					if (xmlValueAnnotationsList.length > 0) {
						// Get respective EDMXSet FunctionImport object
						org.eclipse.ogee.model.odata.FunctionImport functionImportObject = (org.eclipse.ogee.model.odata.FunctionImport) this.objectsList
								.get(this.schemaNameSpace).get(
										xmlFunctionImportEntry);
						// Add valueAnnotations to FunctionImport object
						addValueAnnotations2Object(functionImportObject,
								xmlValueAnnotationsList);
					}

					// Parameter
					String functionImportName = xmlFunctionImportV3.getName();
					Parameter[] xmlParameters = xmlFunctionImportV3
							.getParameters();
					for (Parameter xmlParameterEntry : xmlParameters) {
						ParameterV3 xmlParameterV3 = ((ParameterV3) xmlParameterEntry);
						// Get Annotations
						xmlValueAnnotationsList = xmlParameterV3
								.getValueAnnotations();
						if (xmlValueAnnotationsList.length > 0) {
							// Get respective EDMXSet FunctionImportParameter
							// object
							org.eclipse.ogee.model.odata.Parameter functionImportParameterObject = (org.eclipse.ogee.model.odata.Parameter) this.objectsList
									.get(this.schemaNameSpace).get(
											functionImportName
													+ xmlParameterEntry);
							// Add valueAnnotations to FunctionImportParameter
							// object
							addValueAnnotations2Object(
									functionImportParameterObject,
									xmlValueAnnotationsList);
						}
					}
				}
			}

		}

	}

	/**
	 * This method adds ValueAnnotations to Domain model object
	 * 
	 * @param target
	 *            - Domain model object
	 * @param xmlValueAnnotations
	 *            - XML Object
	 * @throws BuilderException
	 */
	private void addValueAnnotations2Object(IAnnotationTarget target,
			ValueAnnotation[] xmlValueAnnotations) throws BuilderException {
		// Loop on XML ValueAnnotation list and add them to target model object
		for (ValueAnnotation xmlValueAnnotationEntry : xmlValueAnnotations) {
			try {
				this.vocabulary = null;

				// Get Term name and Vocabulary name from the XML
				// ValueAnnotation
				String xmlValueAnnotationTerm = xmlValueAnnotationEntry
						.getTerm();
				if (xmlValueAnnotationTerm == null) {
					throw new BuilderException(NLS.bind(
							BuilderV3Messages.addAnnotationToObjectError04,
							null));
				}
				splitVocabularyTerm(xmlValueAnnotationTerm);

				// Get vocabulary object from the EDMX references
				this.vocabulary = getVocabularyObject();

				if (this.vocabulary == null) {
					logger.logWarning(NLS.bind(
							BuilderV3Messages.addAnnotationToObjectError02,
							this.vocabularyName, this.termName));
					return;
				}

				// Get ValueTerm from the Vocabulary
				ValueTerm valueTerm = vocabulary.getValueTerm(this.termName);
				if (valueTerm == null) {
					logger.logWarning(NLS.bind(
							BuilderV3Messages.addAnnotationToObjectError02,
							this.vocabularyName, this.termName));
					return;
				}
				// Create ValueAnnotation
				org.eclipse.ogee.model.odata.ValueAnnotation valueAnnotation = vocabulary
						.createValueAnnotation(target, valueTerm);

				// Assign the XML annotation content (a.k.a. annotation value)
				// to the created ValueAnnotation
				AnnotationValue annotationValue = valueAnnotation
						.getAnnotationValue();
				setAnnotationValue(valueAnnotation, annotationValue,
						xmlValueAnnotationEntry);
			} catch (ModelAPIException e) {
				throw new BuilderException(e.getMessage());
			}

		}
	}

	/**
	 * This method gets a vocabulary object by a vocabulary NameSpace from the
	 * attached EDMX references
	 * 
	 * @return - vocabulary object
	 * @throws BuilderException
	 * @throws ModelAPIException
	 */
	private IVocabulary getVocabularyObject() throws BuilderException,
			ModelAPIException {

		IVocabulary vocabulary = null;

		// Get vocabulary from already used vocabularies
		if (this.usedVocabularies != null && !this.usedVocabularies.isEmpty()) {
			vocabulary = this.usedVocabularies.get(this.vocabularyName);
		}

		if (vocabulary == null) {
			// Get Vocabulary edmxSet object
			EDMXSet vocabularyEdmxSet = getVocabularyEDMXSet(this.vocabularyName);
			if (vocabularyEdmxSet == null) {
				logger.logWarning(NLS.bind(
						BuilderV3Messages.addAnnotationToObjectError01,
						this.vocabularyName));
				return null;
			}
			// Get Vocabulary from edmxSet
			IVocabularyContext contextModel;
			try {
				contextModel = IModelContext.INSTANCE
						.getVocabularyContext(vocabularyEdmxSet);
				vocabulary = contextModel
						.provideVocabulary(this.vocabularyName);
				// Save new found vocabulary in the used vocabulary list
				this.usedVocabularies.put(this.vocabularyName, vocabulary);
				return vocabulary;
			} catch (ModelAPIException e) {
				throw new BuilderException(e.getMessage());
			}
		} else {
			return vocabulary;
		}
	}

	/**
	 * This method sets an XML annotation content to the created ValueAnnotation
	 * 
	 * @param valueAnnotation
	 * 
	 * @param annotationValue
	 *            - the created annotation value
	 * @param xmlValueAnnotation
	 *            - current iteration XML annotation object
	 * @throws ModelAPIException
	 */
	private void setAnnotationValue(
			org.eclipse.ogee.model.odata.ValueAnnotation valueAnnotation,
			AnnotationValue annotationValue, ValueAnnotation xmlValueAnnotation)
			throws ModelAPIException {
		if (annotationValue instanceof ConstantExpression) {
			setConstantExpressionValue(valueAnnotation, annotationValue,
					xmlValueAnnotation);
		} else if (annotationValue instanceof DynamicExpression) {
			setDynamicExpressionValue(valueAnnotation, annotationValue,
					xmlValueAnnotation);
		} else if (annotationValue instanceof CollectableExpression) {
			setCollectableExpression(valueAnnotation, annotationValue,
					xmlValueAnnotation);
		}
	}

	/**
	 * This method managing the DynamicExpression fork, either the created
	 * annotation is a RecordValue expression or PathValue expression or
	 * ValueCollection expression
	 * 
	 * @param valueAnnotation
	 * 
	 * @param annotationValue
	 *            - the created annotation value
	 * @param xmlValueAnnotation
	 *            - current iteration XML annotation object
	 * @throws ModelAPIException
	 */
	private void setDynamicExpressionValue(
			org.eclipse.ogee.model.odata.ValueAnnotation valueAnnotation,
			AnnotationValue annotationValue, ValueAnnotation xmlValueAnnotation)
			throws ModelAPIException {
		if (annotationValue instanceof RecordValue) {
			Record xmlRecord = xmlValueAnnotation.getRecord();
			setRecordValue(valueAnnotation, annotationValue, xmlRecord);
		} else if (annotationValue instanceof PathValue) {
			setPathValue(annotationValue, xmlValueAnnotation);
		} else if (annotationValue instanceof ValueCollection) {
			setCollectableExpression(valueAnnotation, annotationValue,
					xmlValueAnnotation);
		}
	}

	/**
	 * This method managing the RecordValue expression. It gets the Record
	 * property values and calls to property value mapping implementation flow
	 * 
	 * @param valueAnnotation
	 * 
	 * @param annotationValue
	 *            - the created annotation value
	 * @param xmlRecord
	 *            - xml record object
	 * @throws ModelAPIException
	 */
	private void setRecordValue(
			org.eclipse.ogee.model.odata.ValueAnnotation valueAnnotation,
			AnnotationValue annotationValue, Record xmlRecord)
			throws ModelAPIException {
		// Get annotation's PropertyValues
		RecordValue recordValue = (RecordValue) annotationValue;
		EMap<org.eclipse.ogee.model.odata.Property, AnnotationValue> propertyValues = recordValue
				.getPropertyValues();
		// Get XML's PropertyValues
		PropertyValue[] xmlPropertyValuesList = xmlRecord.getPropertyValues();

		// Loop on annotation's PropertyValues and on XML's PropertyValues with
		// the same property name
		for (Entry<org.eclipse.ogee.model.odata.Property, AnnotationValue> propertyValuesEntry : propertyValues) {
			String propertyName = propertyValuesEntry.getKey().getName();
			for (PropertyValue xmlPropertyValueEntry : xmlPropertyValuesList) {
				if (propertyName.equalsIgnoreCase(xmlPropertyValueEntry
						.getProperty())) {
					implmentPropertyValueMap(valueAnnotation,
							propertyValuesEntry, xmlPropertyValueEntry);
					break;
				}
			}
		}
	}

	/**
	 * This method dispatches the property value according to property type
	 * 
	 * @param valueAnnotation
	 * 
	 * @param propertyValue
	 *            - Domain model property value
	 * @param xmlPropertyValue
	 *            - XML object property value content
	 * @throws ModelAPIException
	 */
	private void implmentPropertyValueMap(
			org.eclipse.ogee.model.odata.ValueAnnotation valueAnnotation,
			Entry<org.eclipse.ogee.model.odata.Property, AnnotationValue> propertyValue,
			PropertyValue xmlPropertyValue) throws ModelAPIException {
		AnnotationValue annotationValue = propertyValue.getValue();
		if (annotationValue instanceof ConstantExpression) {
			setConstantExpressionValue(null, annotationValue, xmlPropertyValue);
		} else if (annotationValue instanceof PathValue) {
			setPathValue(annotationValue, xmlPropertyValue);
		} else if (annotationValue instanceof ValueCollectionImpl) {
			setCollectableExpression(valueAnnotation, annotationValue,
					xmlPropertyValue);
		}
	}

	/**
	 * This method sets XML object content as a children of the collection
	 * expression
	 * 
	 * @param valueAnnotation
	 * 
	 * @param annotationValue
	 *            - The PropertyValue annotation value
	 * @param xmlValueAnnotation
	 *            - XML object content
	 * @throws ModelAPIException
	 */
	private void setCollectableExpression(
			org.eclipse.ogee.model.odata.ValueAnnotation valueAnnotation,
			AnnotationValue annotationValue,
			BaseExpressionElement xmlValueAnnotation) throws ModelAPIException {
		CollectableExpression createCollectionValue = null;
		Object[] childExpressions = xmlValueAnnotation.getCollection()
				.getChildExpressions();
		ValueCollection valueCollection = (ValueCollection) annotationValue;
		createCollectionValue = this.vocabulary
				.createCollectionValue(valueCollection);
		for (Object xmlChildExpressionEntry : childExpressions) {
			if (createCollectionValue instanceof ConstantExpression) {
				SimpleValue simpleValue = (SimpleValue) createCollectionValue;
				// String
				if (simpleValue instanceof StringValue) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String) {
						StringValue value = OdataFactory.eINSTANCE
								.createStringValue();
						value.setValue(((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.String) xmlChildExpressionEntry)
								.getValue());
						valueCollection.getValues().add(value);
					}
				}
				// Boolean
				else if (simpleValue instanceof BooleanValue) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool) {
						BooleanValue value = OdataFactory.eINSTANCE
								.createBooleanValue();
						String boolValue = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Bool) xmlChildExpressionEntry)
								.getValue();
						value.setValue(Boolean.valueOf(boolValue));
						valueCollection.getValues().add(value);
					}
				}
				// Byte
				else if (simpleValue instanceof ByteValue) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte) {
						ByteValue value = OdataFactory.eINSTANCE
								.createByteValue();
						String stringValue = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte) xmlChildExpressionEntry)
								.getValue();
						value.setValue(Byte.valueOf(stringValue));
						valueCollection.getValues().add(value);
					}
				}
				// Binary
				else if (simpleValue instanceof BinaryValue) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Binary) {
						BinaryValue value = OdataFactory.eINSTANCE
								.createBinaryValue();
						byte[] byteValue = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Binary) xmlChildExpressionEntry)
								.getValue().getBytes();
						value.setValue(byteValue);
						valueCollection.getValues().add(value);
					}
				}
				// DateTime
				else if (simpleValue instanceof DateTimeValue) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTime) {
						DateTimeValue value = OdataFactory.eINSTANCE
								.createDateTimeValue();
						String dateTimeString = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTime) xmlChildExpressionEntry)
								.getValue();
						value.setValue(DateTime.fromXMLFormat(dateTimeString));
						valueCollection.getValues().add(value);
					}
				}
				// DateTimeOffset
				else if (simpleValue instanceof DateTimeOffsetValue) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTimeOffset) {
						DateTimeOffsetValue value = OdataFactory.eINSTANCE
								.createDateTimeOffsetValue();
						String dateTimeString = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.DateTimeOffset) xmlChildExpressionEntry)
								.getValue();
						value.setValue(DateTimeOffset
								.fromXMLFormat(dateTimeString));
						valueCollection.getValues().add(value);
					}
				}
				// Decimal
				else if (simpleValue instanceof DecimalValue) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal) {
						DecimalValue value = OdataFactory.eINSTANCE
								.createDecimalValue();
						String decimalString = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Decimal) xmlChildExpressionEntry)
								.getValue();
						value.setValue(new BigDecimal(decimalString));
						valueCollection.getValues().add(value);
					}
				}
				// Float
				else if (simpleValue instanceof SingleValue) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float) {
						SingleValue floatValue = OdataFactory.eINSTANCE
								.createSingleValue();
						String floatString = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float) xmlChildExpressionEntry)
								.getValue();
						floatValue.setValue(Float.parseFloat(floatString));
						valueCollection.getValues().add(floatValue);
					}
				} else if (simpleValue instanceof DoubleValue) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float) {
						DoubleValue doubleValue = OdataFactory.eINSTANCE
								.createDoubleValue();
						String floatString = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Float) xmlChildExpressionEntry)
								.getValue();
						doubleValue.setValue(Double.valueOf(floatString));
						valueCollection.getValues().add(doubleValue);
					}
				}
				// Guid
				else if (simpleValue instanceof GuidValue) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Guid) {
						GuidValue value = OdataFactory.eINSTANCE
								.createGuidValue();
						String guidString = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Guid) xmlChildExpressionEntry)
								.getValue();
						UUID uuid = UUID.fromString(guidString);
						value.setValue(uuid);
						valueCollection.getValues().add(value);
					}
				}
				// Int
				else if (simpleValue instanceof Int16Value) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int) {
						Int16Value value = OdataFactory.eINSTANCE
								.createInt16Value();
						String intString = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int) xmlChildExpressionEntry)
								.getValue();
						value.setValue(Short.valueOf(intString));
						valueCollection.getValues().add(value);
					}
				}
				// Int32
				else if (simpleValue instanceof Int32Value) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int) {
						String intString = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int) xmlChildExpressionEntry)
								.getValue();
						Int32Value intValue = OdataFactory.eINSTANCE
								.createInt32Value();
						intValue.setValue(Integer.valueOf(intString));
						valueCollection.getValues().add(intValue);
					}
				}
				// Int64
				else if (simpleValue instanceof Int64Value) {
					if (xmlChildExpressionEntry instanceof org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int) {
						String intString = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Int) xmlChildExpressionEntry)
								.getValue();
						Int64Value intValue = OdataFactory.eINSTANCE
								.createInt64Value();
						intValue.setValue(Long.valueOf(intString));
						valueCollection.getValues().add(intValue);
					}
				}
			} else {
				if (xmlChildExpressionEntry instanceof Record) {
					Record recordExpressionEntry = (Record) xmlChildExpressionEntry;
					setRecordValue(valueAnnotation, createCollectionValue,
							recordExpressionEntry);
					valueCollection.getValues().add(createCollectionValue);
				}
			}
		}
	}

	/**
	 * This method sets XML object content as a PathValue expression
	 * 
	 * @param annotationValue
	 *            - The PropertyValue annotation value
	 * @param xmlValueAnnotation
	 *            - XML object content
	 */
	private void setPathValue(AnnotationValue annotationValue,
			BaseExpressionElement xmlValueAnnotation) {
		PathValue pathValue = (PathValue) annotationValue;
		pathValue.setPath(xmlValueAnnotation.getPath().getValue());
	}

	/**
	 * This method sets XML object content as a constant expression
	 * 
	 * @param valueAnnotation
	 * 
	 * @param annotationValue
	 *            - The PropertyValue annotation value
	 * @param xmlValueAnnotation
	 *            - XML object content
	 */
	private void setConstantExpressionValue(
			org.eclipse.ogee.model.odata.ValueAnnotation valueAnnotation,
			AnnotationValue annotationValue,
			BaseExpressionElement xmlValueAnnotation) {
		SimpleValue simpleValue = (SimpleValue) annotationValue;
		// Boolean
		if (simpleValue instanceof BooleanValue) {
			if (xmlValueAnnotation.getBool() != null) {
				BooleanValue booleanValue = (BooleanValue) simpleValue;
				String boolValue = xmlValueAnnotation.getBool().getValue();
				booleanValue.setValue(Boolean.valueOf(boolValue));
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// String
		else if (simpleValue instanceof StringValue) {
			if (xmlValueAnnotation.getString() != null) {
				StringValue stringValue = (StringValue) simpleValue;
				stringValue.setValue(xmlValueAnnotation.getString().getValue());
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// Byte
		else if (simpleValue instanceof ByteValue) {
			if (xmlValueAnnotation.getByte() != null) {
				ByteValue byteValue = (ByteValue) simpleValue;
				String stringValue = ((org.eclipse.ogee.client.model.edmx.v3.expressions.primitive.Byte) xmlValueAnnotation
						.getByte()).getValue();
				byteValue.setValue(Byte.valueOf(stringValue));
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// Binary
		else if (simpleValue instanceof BinaryValue) {
			if (xmlValueAnnotation.getBinary() != null) {
				BinaryValue binaryValue = (BinaryValue) simpleValue;
				byte[] value = xmlValueAnnotation.getBinary().getValue()
						.getBytes();
				binaryValue.setValue(value);
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// DateTime
		else if (simpleValue instanceof DateTimeValue) {
			if (xmlValueAnnotation.getDateTime() != null) {
				DateTimeValue dateTimeValue = (DateTimeValue) simpleValue;
				String dateString = xmlValueAnnotation.getDateTime().getValue();
				dateTimeValue.setValue(DateTime.fromXMLFormat(dateString));
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// DateTimeOffset
		else if (simpleValue instanceof DateTimeOffsetValue) {
			if (xmlValueAnnotation.getDateTimeOffset() != null) {
				DateTimeOffsetValue dateTimeOffsetValue = (DateTimeOffsetValue) simpleValue;
				String dateString = xmlValueAnnotation.getDateTimeOffset()
						.getValue();
				dateTimeOffsetValue.setValue(DateTimeOffset
						.fromXMLFormat(dateString));
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// Decimal
		else if (simpleValue instanceof DecimalValue) {
			if (xmlValueAnnotation.getDecimal() != null) {
				DecimalValue decimalValue = (DecimalValue) simpleValue;
				String decimalString = xmlValueAnnotation.getDecimal()
						.getValue();
				decimalValue.setValue(new BigDecimal(decimalString));
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// Float
		else if (simpleValue instanceof SingleValue) {
			if (xmlValueAnnotation.getFloat() != null) {
				SingleValue floatValue = (SingleValue) simpleValue;
				floatValue.setValue(Float.parseFloat(xmlValueAnnotation
						.getFloat().getValue()));
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		} else if (simpleValue instanceof DoubleValue) {
			if (xmlValueAnnotation.getFloat() != null) {
				DoubleValue floatValue = (DoubleValue) simpleValue;
				floatValue.setValue(Double.valueOf(xmlValueAnnotation
						.getFloat().getValue()));
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// Guid
		else if (simpleValue instanceof GuidValue) {
			if (xmlValueAnnotation.getGuid() != null) {
				GuidValue guidValue = (GuidValue) simpleValue;
				UUID uuid = UUID.fromString(xmlValueAnnotation.getGuid()
						.getValue());
				guidValue.setValue(uuid);
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// Int16
		else if (simpleValue instanceof Int16Value) {
			if (xmlValueAnnotation.getInt() != null) {
				Int16Value intValue = (Int16Value) simpleValue;
				String intString = xmlValueAnnotation.getInt().getValue();
				intValue.setValue(Short.valueOf(intString));
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// Int32
		else if (simpleValue instanceof Int32Value) {
			if (xmlValueAnnotation.getInt() != null) {
				Int32Value intValue = (Int32Value) simpleValue;
				String intString = xmlValueAnnotation.getInt().getValue();
				intValue.setValue(Integer.valueOf(intString));
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}
		// Int64
		else if (simpleValue instanceof Int64Value) {
			if (xmlValueAnnotation.getInt() != null) {
				Int64Value intValue = (Int64Value) simpleValue;
				String intString = xmlValueAnnotation.getInt().getValue();
				intValue.setValue(Long.valueOf(intString));
			} else if (xmlValueAnnotation.getPath() != null) {
				createPathValue(xmlValueAnnotation.getPath().getValue(),
						annotationValue, valueAnnotation);
			}
		}

	}

	/**
	 * This method creates the domain model PathValue object
	 * 
	 * @param xmlPathValueContent
	 *            - PathValue content from the XML file
	 * @param valueAnnotation
	 * @param annotationValue
	 */
	private void createPathValue(String xmlPathValueContent,
			AnnotationValue annotationValue,
			org.eclipse.ogee.model.odata.ValueAnnotation valueAnnotation) {
		PathValue pathValue = OdataFactory.eINSTANCE.createPathValue();
		pathValue.setPath(xmlPathValueContent);

		try {
			// PropertyValue
			@SuppressWarnings("unchecked")
			Entry<org.eclipse.ogee.model.odata.Property, AnnotationValue> propertyValue = (Entry<org.eclipse.ogee.model.odata.Property, AnnotationValue>) annotationValue
					.eContainer();
			if (propertyValue instanceof PropertyToValueMapImpl) {
				if (annotationValue instanceof ConstantExpression) {
					// SimpleType PropertyValue
					propertyValue.setValue(pathValue);
				} else if (annotationValue instanceof ValueCollection) {
					// Collection PropertyValue
					ValueCollection valueCollection = (ValueCollection) annotationValue;
					valueCollection.getValues().add(
							(CollectableExpression) pathValue);
				}
			}
		} catch (ClassCastException e) {
			if (annotationValue instanceof ConstantExpression) {
				// ConstantExpression
				valueAnnotation.setAnnotationValue(pathValue);
			}
		}
	}

	/**
	 * This method gets domain model EDMXSet object in which the vocabulary was
	 * found
	 * 
	 * @param vocabularyName
	 *            - vocabulary name
	 * @return - EDMXSet object in which the vocabulary was found
	 * @throws ModelAPIException
	 */
	private EDMXSet getVocabularyEDMXSet(String vocabularyName)
			throws ModelAPIException {
		EDMXSet edmxSet = null;
		edmxSet = this.builderV3Util.getEdmxSetFromNamespace(
				this.vocabularyName, this.edmxSet, this.schema);
		return edmxSet;
	}

	/**
	 * This method splits the given term name to NameSpace and actual term name.
	 * After the last dot we can find the term name, before will be the
	 * NameSpace
	 * 
	 * @param term
	 *            - term name, NameSpace qualified
	 * @throws ModelAPIException
	 */
	private void splitVocabularyTerm(String term) throws ModelAPIException {
		int lastIndexOf = term.lastIndexOf("."); //$NON-NLS-1$
		if (lastIndexOf != -1) {
			this.termName = term.substring(lastIndexOf + 1);
			String identifier = term.substring(0, lastIndexOf);

			// Check if the used vocabulary defined as an alias in the main
			// schema in the using part. If yes, convert the vocabulary name
			// from alias name to an appropriate namespace name

			this.vocabularyName = this.builderV3Util.getSchemaNamespace(
					this.schema, identifier, this.edmxSet);
			// Fallback - if vocabulary name not found take the identifier
			if (this.vocabularyName == null) {
				this.vocabularyName = identifier;
			}
		}
	}

	private IAnnotationTarget getTargetElement(EDMXSet edmxSet,
			String targetPath, SchemaV3 xmlCurrentSchema)
			throws BuilderException {
		/*
		 * ------------------------------------------------------------------
		 * Get the target element object
		 * ------------------------------------------------------------------
		 * Step1 - Get the target schema Step2 - Get the target element for
		 * value annotations
		 * ------------------------------------------------------------------
		 */

		final String SEPERATOR_DOT = "."; //$NON-NLS-1$
		final String SEPERATOR_SLASH = "/"; //$NON-NLS-1$

		IAnnotationTarget targetElement = null;
		String elementPath = null;
		org.eclipse.ogee.model.odata.Schema targetSchema = null;

		// ------------------------------------------------------------------
		// Get the target schema object
		// ------------------------------------------------------------------

		if (!targetPath.contains(SEPERATOR_DOT)) // In case the annotation is
													// local
		{
			targetSchema = this.schema;
			elementPath = targetPath;
		} else {
			String targetPathTemp = targetPath;
			while (targetPathTemp.contains(SEPERATOR_DOT)) {
				String str = targetPathTemp.substring(0,
						targetPathTemp.lastIndexOf(SEPERATOR_DOT));
				try {
					targetSchema = this.builderV3Util
							.getTargetSchemaFromNamespace(str, edmxSet,
									this.schema);
				} catch (ModelAPIException e) {
					throw new BuilderException(e.getMessage());
				}
				if (targetSchema != null) {
					elementPath = targetPathTemp;
					break;
				}
				targetPathTemp = str;
			}

			// in case of the same schema but with '.' e.g
			// container.functionImport
			if ((targetPath.contains(SEPERATOR_DOT)) && (targetSchema == null)) {
				targetSchema = this.schema;
				elementPath = targetPath;
			}
		}

		if (targetSchema == null) {
			throw new BuilderException(
					BuilderV3Messages.AddAnnotation2EDMXSetObects_annotationsTargetNotExist);
		}

		/*
		 * ------------------------------------------------------------------
		 * Get the target element object
		 * ------------------------------------------------------------------
		 * Case1 - target elementPath with no "." or"/" e.g. entityType1 Case2 -
		 * target elementPath with "." e.g. container.functionImport1 Case3 -
		 * target elementPath with "." and "/" e.g.
		 * container.functionImport1/parameter Case4 - target elementPath with
		 * "/" e.g. entityType/property1
		 * ------------------------------------------------------------------
		 */

		// Case1 - target elementPath with no "." or"/" e.g. entityType1
		// ---------------------------------------------------------------
		if ((!elementPath.contains(SEPERATOR_DOT))
				&& (!elementPath.contains(SEPERATOR_SLASH))) {

			// Check ComplexType
			EList<org.eclipse.ogee.model.odata.ComplexType> ComplexTypeList = targetSchema
					.getComplexTypes();
			for (org.eclipse.ogee.model.odata.ComplexType currComplexType : ComplexTypeList) {
				if (elementPath.equals(currComplexType.getName())) {
					return currComplexType;
				}
			}

			// Check EntityType
			EList<org.eclipse.ogee.model.odata.EntityType> EntityTypeList = targetSchema
					.getEntityTypes();
			for (org.eclipse.ogee.model.odata.EntityType currEntityType : EntityTypeList) {
				if (elementPath.equals(currEntityType.getName())) {
					return currEntityType;
				}
			}

			// Check ValueTerm
			EList<org.eclipse.ogee.model.odata.ValueTerm> ValueTermList = targetSchema
					.getValueTerms();
			for (org.eclipse.ogee.model.odata.ValueTerm currValueTerm : ValueTermList) {
				if (elementPath.equals(currValueTerm.getName())) {
					return currValueTerm;
				}
			}

		}

		// Case2 - target elementPath with "." e.g. container.functionImport1
		// ---------------------------------------------------------------
		if (elementPath.contains(SEPERATOR_DOT)) {
			String containerName = elementPath.substring(0,
					elementPath.lastIndexOf(SEPERATOR_DOT));
			String containerSubelementName = null;
			String containerElementName = null;
			if (elementPath.contains(SEPERATOR_SLASH)) {
				containerSubelementName = elementPath.substring(
						elementPath.indexOf(SEPERATOR_DOT),
						elementPath.lastIndexOf(SEPERATOR_SLASH));
				containerElementName = elementPath.substring(elementPath
						.lastIndexOf(SEPERATOR_SLASH) + 1);
			} else {
				containerSubelementName = elementPath.substring(elementPath
						.indexOf(SEPERATOR_DOT) + 1);
			}

			// Get container
			EList<org.eclipse.ogee.model.odata.EntityContainer> entityContainerList = targetSchema
					.getContainers();
			for (org.eclipse.ogee.model.odata.EntityContainer currEntityContainer : entityContainerList) {
				if (containerName.equals(currEntityContainer.getName())) {

					// Check if its Function import
					EList<org.eclipse.ogee.model.odata.FunctionImport> functionImportList = currEntityContainer
							.getFunctionImports();
					for (org.eclipse.ogee.model.odata.FunctionImport currFunctionImport : functionImportList) {

						if (containerSubelementName.equals(currFunctionImport
								.getName())) {
							// Case3 - target elementPath with "." and "/" e.g.
							// container.functionImport1/parameter
							// ---------------------------------------------------------------
							if (elementPath.contains(SEPERATOR_SLASH)) {
								// check if its parameter
								EList<org.eclipse.ogee.model.odata.Parameter> parameterList = currFunctionImport
										.getParameters();
								for (org.eclipse.ogee.model.odata.Parameter currParameter : parameterList) {
									if (containerElementName
											.equals(currParameter.getName())) {
										targetElement = currParameter;
									}
								}
							} else {
								targetElement = currFunctionImport;
							}
							return targetElement;
						}

					}
					// Check if its EntitySet
					if (!elementPath.contains(SEPERATOR_SLASH)) {

						EList<org.eclipse.ogee.model.odata.EntitySet> entitySetList = currEntityContainer
								.getEntitySets();
						for (org.eclipse.ogee.model.odata.EntitySet currEntitySet : entitySetList) {

							if (containerSubelementName.equals(currEntitySet
									.getName())) {
								return currEntitySet;
							}
						}
					}
				}
			}
		}
		// Case4 - target elementPath with "/" e.g. entityType/property1
		// ---------------------------------------------------------------
		if (elementPath.contains(SEPERATOR_SLASH)) // type/property
		{
			String type = elementPath.substring(0,
					elementPath.indexOf(SEPERATOR_SLASH));

			// check property in entity Type
			EList<org.eclipse.ogee.model.odata.EntityType> entitytypeList = targetSchema
					.getEntityTypes();
			for (org.eclipse.ogee.model.odata.EntityType currEntitytype : entitytypeList) {
				if (type.equals(currEntitytype.getName())) {
					// Properties
					EList<org.eclipse.ogee.model.odata.Property> propertyList = currEntitytype
							.getProperties();
					for (org.eclipse.ogee.model.odata.Property currProperty : propertyList) {
						String propertyName = elementPath.substring(elementPath
								.indexOf(SEPERATOR_SLASH) + 1);
						if (propertyName.equals(currProperty.getName())) {
							return currProperty;
						}
					}

					// Properties
					EList<org.eclipse.ogee.model.odata.NavigationProperty> NavigationPropertyList = currEntitytype
							.getNavigationProperties();
					for (org.eclipse.ogee.model.odata.NavigationProperty currNavigationProperty : NavigationPropertyList) {
						String NavigationPropertyName = elementPath
								.substring(elementPath.indexOf(SEPERATOR_SLASH) + 1);
						if (NavigationPropertyName
								.equals(currNavigationProperty.getName())) {
							return currNavigationProperty;
						}
					}

				}
			}

			// check property in complex Type
			EList<org.eclipse.ogee.model.odata.ComplexType> ComplextypeList = targetSchema
					.getComplexTypes();
			for (org.eclipse.ogee.model.odata.ComplexType currComplextype : ComplextypeList) {
				if (type.equals(currComplextype.getName())) {
					EList<org.eclipse.ogee.model.odata.Property> CTpropertyList = currComplextype
							.getProperties();
					for (org.eclipse.ogee.model.odata.Property currProperty : CTpropertyList) {
						String propertyName = elementPath.substring(elementPath
								.indexOf(SEPERATOR_SLASH) + 1);
						if (propertyName.equals(currProperty.getName())) {
							return currProperty;
						}
					}
				}
			}
		}

		return targetElement;

	}
}
