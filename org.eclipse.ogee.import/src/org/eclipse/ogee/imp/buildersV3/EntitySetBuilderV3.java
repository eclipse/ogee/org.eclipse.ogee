/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.util.Map;

import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntitySet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.osgi.util.NLS;

public class EntitySetBuilderV3 {
	public void build(
			org.eclipse.ogee.client.model.edmx.EntitySet edmxEntitySet,
			String namespace, Map<String, Map<Object, Object>> objects,
			Schema schema, EDMXSet edmxSet) throws BuilderException,
			ModelAPIException {
		// get EntitySet emf model object
		Map<Object, Object> nsObject = objects.get(namespace);

		EntitySet entitySet = (EntitySet) nsObject.get(edmxEntitySet);

		BuilderV3Util builderV3Utility = new BuilderV3Util();

		// get EntitySet EntityType
		String edmxEntitySetType = edmxEntitySet.getEntityType();
		if (edmxEntitySetType != null && entitySet.getType() == null) {
			Object objectType = null;

			String objTypeShortName = builderV3Utility
					.extractTypeShortName(edmxEntitySetType);
			String objIdentifier = builderV3Utility
					.extractTypeNameSpace(edmxEntitySetType);
			if (objIdentifier == null || objIdentifier.equals("Edm"))//$NON-NLS-1$
			{
				objIdentifier = namespace;
			}

			String targetSchema = builderV3Utility.getSchemaNamespace(schema,
					objIdentifier, edmxSet);

			// target schema not found
			if (targetSchema == null || targetSchema.isEmpty()) {
				throw new BuilderException(NLS.bind(
						BuilderV3Messages.addReference02, objIdentifier));
			}

			objectType = builderV3Utility.getObjectTypeFromTargetSchema(
					targetSchema, edmxSet, schema, objTypeShortName);

			if (objectType == null) {
				throw new BuilderException(NLS.bind(
						BuilderV3Messages.TypeError_0, edmxEntitySetType,
						"EntitySet")); //$NON-NLS-1$
			}

			if (objectType instanceof EntityType) {
				entitySet.setType((EntityType) objectType);
			} else {
				throw new BuilderException(NLS.bind(
						BuilderV3Messages.TypeError_0, edmxEntitySetType,
						"EntitySet")); //$NON-NLS-1$
			}

		}

	}
}
