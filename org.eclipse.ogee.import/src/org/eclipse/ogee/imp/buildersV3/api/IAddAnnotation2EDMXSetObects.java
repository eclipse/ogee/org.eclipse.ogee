/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3.api;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.v3.EdmxV3;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.model.odata.EDMXSet;

public interface IAddAnnotation2EDMXSetObects {
	/**
	 * This method adds annotations to all EDMXSet objects that can have the
	 * vocabulary ValueAnnotation term
	 * 
	 * @param objects
	 *            - XML object lookup table
	 * @param edmx
	 *            - edmx in parsed XML object format
	 * @param edmxSet
	 *            - edmxSet in EMF object format
	 * @throws BuilderException
	 */
	public void annotateEDMXSetobjects(
			Map<String, Map<Object, Object>> objects, EdmxV3 edmx,
			EDMXSet edmxSet) throws BuilderException;

}
