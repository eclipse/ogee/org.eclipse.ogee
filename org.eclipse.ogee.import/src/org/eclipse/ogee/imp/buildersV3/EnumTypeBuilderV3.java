/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.v3.Member;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EnumMember;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.IntegerValue;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.osgi.util.NLS;

public class EnumTypeBuilderV3 {
	public void build(
			org.eclipse.ogee.client.model.edmx.v3.EnumType edmxEnumType,
			Map<String, Map<Object, Object>> odataObjectsMap, Schema schema)
			throws BuilderException {

		// get enumType form the lockup table
		Map<Object, Object> nsObject = odataObjectsMap.get(schema
				.getNamespace());
		EnumType enumType = (EnumType) nsObject.get(edmxEnumType);

		if (enumType == null) {
			String enumTypeName = edmxEnumType.getName();
			if (enumTypeName == null) {
				throw new BuilderException(BuilderV3Messages.EnumBuilderV3_0);
			}
			// create enumType
			enumType = OdataFactory.eINSTANCE.createEnumType();
			// save enumType
			nsObject.put(edmxEnumType, enumType);

			// set enumType name
			enumType.setName(enumTypeName);

			// set is Flags
			enumType.setFlags(edmxEnumType.isFlags());

			// set underlying type
			String underlyingType = edmxEnumType.getUnderlyingType();
			String eType = underlyingType.replace("Edm.", ""); //$NON-NLS-1$ //$NON-NLS-2$
			EDMTypes edmType = EDMTypes.getByName(eType);
			if (edmType == null) {
				// EnumType UnderlyingType cannot be (invalid value)
				throw new BuilderException(NLS.bind(
						BuilderV3Messages.EnumBuilderV3_2, enumTypeName,
						underlyingType));
			}
			enumType.setUnderlyingType(edmType);

			// set members
			Member[] members = edmxEnumType.getMembers();
			for (Member currMember : members) {
				String enumMemberName = currMember.getName();
				if (enumMemberName == null) {
					throw new BuilderException(
							BuilderV3Messages.EnumBuilderV3_1);
				}
				EnumMember enumMember = OdataFactory.eINSTANCE
						.createEnumMember();
				enumMember.setName(enumMemberName);
				Long longMemberValue = (Long) currMember.getValue();
				if (longMemberValue != null) {
					IntegerValue integerMemberValue = createMemberValue(
							longMemberValue, edmType);
					enumMember.setValue(integerMemberValue);
				}
				enumType.getMembers().add(enumMember);
			}

			// add the enum to the Schema
			schema.getEnumTypes().add(enumType);
		}

	}

	private IntegerValue createMemberValue(Long value, EDMTypes edmType) {
		IntegerValue integerValue = null;
		switch (edmType) {
		case BYTE:
			integerValue = OdataFactory.eINSTANCE.createByteValue();
			integerValue.setValueObject((Byte) value.byteValue());
			break;
		case INT16:
			integerValue = OdataFactory.eINSTANCE.createInt16Value();
			integerValue.setValueObject((Short) value.shortValue());
			break;
		case INT32:
			integerValue = OdataFactory.eINSTANCE.createInt32Value();
			integerValue.setValueObject((Integer) value.intValue());
			break;
		case INT64:
			integerValue = OdataFactory.eINSTANCE.createInt64Value();
			integerValue.setValueObject((Long) value);
			break;
		case SBYTE:
			integerValue = OdataFactory.eINSTANCE.createSByteValue();
			integerValue.setValueObject((Byte) value.byteValue());
			break;
		default:
			break;
		}

		return integerValue;
	}

}
