/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.util.Map;

import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.IParameterTypeUsage;
import org.eclipse.ogee.model.odata.Parameter;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.osgi.util.NLS;

public class FunctionImportParameterBuilderV3 {

	public void build(
			org.eclipse.ogee.client.model.edmx.Parameter edmxFunctionImportParameter,
			String namespace, Map<String, Map<Object, Object>> objects,
			Schema schema, EDMXSet edmxSet, String functionName)
			throws BuilderException, ModelAPIException {

		validateInputParameters(edmxFunctionImportParameter, namespace, objects);

		// get Parameter emf model object
		Map<Object, Object> nsObject = objects.get(namespace);

		Parameter functionImportParameter = (Parameter) nsObject
				.get(functionName + edmxFunctionImportParameter);

		BuilderV3Util builderV3Utility = new BuilderV3Util();

		// get edmx parameter type
		String edmxParameterType = edmxFunctionImportParameter.getType();

		// update Function Import parameter type with ENUM:
		// when in edmx file parameter has type, but in EMF model it doesn't
		// have any type
		if (edmxParameterType != null
				&& functionImportParameter.getType() == null) {
			Object objectType = null;

			String objTypeShortName = builderV3Utility
					.extractTypeShortName(edmxParameterType);
			String objIdentifier = builderV3Utility
					.extractTypeNameSpace(edmxParameterType);

			if (objIdentifier == null || objIdentifier.equals("Edm"))//$NON-NLS-1$
			{
				objIdentifier = namespace;
			}

			String targetSchema = builderV3Utility.getSchemaNamespace(schema,
					objIdentifier, edmxSet);
			// target schema not found
			if (targetSchema == null || targetSchema.isEmpty()) {
				throw new BuilderException(NLS.bind(
						BuilderV3Messages.addReference02, objIdentifier));
			}

			objectType = builderV3Utility.getObjectTypeFromTargetSchema(
					targetSchema, edmxSet, schema, objTypeShortName);

			if (objectType == null) {
				throw new BuilderException(NLS.bind(
						BuilderV3Messages.TypeError_0, edmxParameterType,
						"Parameter")); //$NON-NLS-1$
			}

			Object typeUsage = builderV3Utility.createTypeUsage(objectType);

			if (typeUsage != null && typeUsage instanceof IParameterTypeUsage) {
				functionImportParameter
						.setType((IParameterTypeUsage) typeUsage);
			}

		}

		// update Function Import Parameter type with collection, if applicable
		if (edmxParameterType != null
				&& builderV3Utility.isCollection(edmxParameterType)) {
			IParameterTypeUsage paramTypeUsage = functionImportParameter
					.getType();
			paramTypeUsage.setCollection(true);
		}

	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.Parameter edmxParameter,
			String namespace, Map<String, Map<Object, Object>> objects)
			throws BuilderException {
		if (edmxParameter == null) {
			throw new BuilderException(
					FrameworkImportMessages.ParameterBuilder_1);
		} else if (objects == null) {
			throw new BuilderException(
					FrameworkImportMessages.ParameterBuilder_2);
		}

		if (namespace == null) {
			throw new BuilderException(FrameworkImportMessages.SchemaBuilder_5);
		}
	}

}
