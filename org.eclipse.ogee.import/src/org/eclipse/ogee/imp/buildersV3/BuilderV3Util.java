/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.ComplexType;
import org.eclipse.ogee.model.odata.ComplexTypeUsage;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.EnumType;
import org.eclipse.ogee.model.odata.EnumTypeUsage;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.Using;

public class BuilderV3Util {

	public String getSchemaNamespace(Schema schema, String identifier,
			EDMXSet currentEdmxSet) throws ModelAPIException {
		String namespace = null;
		if (schema.getNamespace().equals(identifier)) {
			namespace = schema.getNamespace();
		} else {
			String alias = schema.getAlias();
			if (alias != null && alias.equals(identifier)) {
				namespace = schema.getNamespace();
			}
		}

		if (namespace == null) {
			EList<Using> usingsList = schema.getUsings();
			for (Using usingEntry : usingsList) {
				if (usingEntry.getAlias().equals(identifier)) {
					namespace = usingEntry.getUsedNamespace().getNamespace();
					break;
				}
			}
		}

		Schema targetSchema = getTargetSchemaFromNamespace(identifier,
				currentEdmxSet, schema);
		if (targetSchema != null) {
			return targetSchema.getNamespace();
		}

		return namespace;
	}

	public EDMXSet getEdmxSetFromNamespace(String targetNamespace,
			EDMXSet currentEdmxSet, Schema currentSchema)
			throws ModelAPIException {
		EDMXSet targetEdmxSet = null;
		List<org.eclipse.ogee.model.odata.Schema> schemataInScope = currentEdmxSet
				.getSchemata();
		for (Schema targetSchemaEntry : schemataInScope) {
			if (targetSchemaEntry.getNamespace().equals(targetNamespace)
					|| (targetSchemaEntry.getAlias() != null && targetSchemaEntry
							.getAlias().equals(targetNamespace))) {
				targetEdmxSet = ((EDMXSet) targetSchemaEntry.eContainer());
				break;
			}

		}
		return targetEdmxSet;
	}

	public Schema getTargetSchemaFromNamespace(String targetNamespace,
			EDMXSet currentEdmxSet, Schema currentSchema)
			throws ModelAPIException {
		Schema targetSchema = null;
		List<org.eclipse.ogee.model.odata.Schema> schemataInScope = currentEdmxSet
				.getSchemata();
		for (Schema targetSchemaEntry : schemataInScope) {
			if (targetSchemaEntry.getNamespace().equals(targetNamespace)
					|| (targetSchemaEntry.getAlias() != null && targetSchemaEntry
							.getAlias().equals(targetNamespace))) {
				targetSchema = targetSchemaEntry;
				break;
			}

		}
		return targetSchema;
	}

	public Object getObjectTypeFromTargetSchema(String targetNamespace,
			EDMXSet currentEdmxSet, Schema currentSchema, String typeName)
			throws ModelAPIException {
		Schema targetSchema = null;
		Object objectType = null;
		if (targetNamespace == null) {
			return null;
		}

		if (currentSchema.getNamespace() == targetNamespace) {
			targetSchema = currentSchema;
		} else {
			targetSchema = getTargetSchemaFromNamespace(targetNamespace,
					currentEdmxSet, currentSchema);
		}

		if (targetSchema == null) {
			return null;
		}

		// Check the type between ENUM
		EList<EnumType> enumTypes = targetSchema.getEnumTypes();
		for (EnumType currEnumType : enumTypes) {
			if (currEnumType.getName().equals(typeName)) {
				return currEnumType;
			}
		}

		// Check the type between EntityType
		EList<EntityType> entityTypes = targetSchema.getEntityTypes();
		for (EntityType currEntityType : entityTypes) {
			if (currEntityType.getName().equals(typeName)) {
				return currEntityType;
			}
		}

		// Check the type between ComplexType
		EList<ComplexType> complexTypes = targetSchema.getComplexTypes();
		for (ComplexType currComplexType : complexTypes) {
			if (currComplexType.getName().equals(typeName)) {
				return currComplexType;
			}
		}

		// Check the type between Associations
		EList<Association> associationList = targetSchema.getAssociations();
		for (Association currAssociation : associationList) {
			if (currAssociation.getName().equals(typeName)) {
				return currAssociation;
			}
		}

		return objectType;
	}

	public Object createTypeUsage(Object objectType) {
		Object typeUsage = null;

		if (objectType instanceof EnumType) {
			EnumTypeUsage enumTypeUsage = OdataFactory.eINSTANCE
					.createEnumTypeUsage();
			enumTypeUsage.setEnumType((EnumType) objectType);
			return enumTypeUsage;
		}

		if (objectType instanceof EntityType) {
			EntityTypeUsage entityTypeUsage = OdataFactory.eINSTANCE
					.createEntityTypeUsage();
			entityTypeUsage.setEntityType((EntityType) objectType);
			return entityTypeUsage;
		}

		if (objectType instanceof ComplexType) {
			ComplexTypeUsage complexTypeUsage = OdataFactory.eINSTANCE
					.createComplexTypeUsage();
			complexTypeUsage.setComplexType((ComplexType) objectType);
			return complexTypeUsage;
		}

		return typeUsage;
	}

	/**
	 * Checks whether the given returnType starts with "Collection".
	 * 
	 * @param returnType
	 * @return - true if the given returnType starts with "Collection", false
	 *         otherwise.
	 */
	public boolean isCollection(String edmxType) {
		if (edmxType == null) {
			return false;
		}

		return edmxType.startsWith("Collection"); //$NON-NLS-1$
	}

	public String extractTypeShortName(String edmxType) {
		int start = edmxType.lastIndexOf(".") + 1; //$NON-NLS-1$
		int end;
		if (edmxType.contains(")")) { //$NON-NLS-1$
			end = edmxType.indexOf(")"); //$NON-NLS-1$
		} else {
			end = edmxType.length();
		}
		return edmxType.substring(start, end);
	}

	public String extractTypeNameSpace(String edmxType) {
		if (edmxType.contains(".")) { //$NON-NLS-1$
			int end = edmxType.lastIndexOf("."); //$NON-NLS-1$
			int start;
			if (edmxType.contains("(")) { //$NON-NLS-1$
				start = edmxType.indexOf("("); //$NON-NLS-1$
			} else {
				start = -1;
			}
			return edmxType.substring(start + 1, end);
		}
		return null;

	}

	/**
	 * Checks if the given OData version is 3.0
	 * 
	 * @param version
	 * @return true if version is 3.0, false otherwise
	 * @throws BuilderException
	 */
	public boolean checkVersion(String version) throws BuilderException {
		if (version != null) {
			Float floatVersion = Float.valueOf(version);
			if (floatVersion == 3.0) {
				return true;
			}
		}
		return false;
	}

}
