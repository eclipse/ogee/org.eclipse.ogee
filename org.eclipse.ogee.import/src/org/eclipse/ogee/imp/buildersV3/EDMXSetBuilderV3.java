/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.ogee.imp.buildersV3;

import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ogee.client.model.edmx.Association;
import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.client.model.edmx.End;
import org.eclipse.ogee.client.model.edmx.EntityContainer;
import org.eclipse.ogee.client.model.edmx.EntitySet;
import org.eclipse.ogee.client.model.edmx.EntityType;
import org.eclipse.ogee.client.model.edmx.FunctionImport;
import org.eclipse.ogee.client.model.edmx.Property;
import org.eclipse.ogee.client.model.edmx.Using;
import org.eclipse.ogee.client.model.edmx.v3.EdmxV3;
import org.eclipse.ogee.client.model.edmx.v3.EnumType;
import org.eclipse.ogee.client.model.edmx.v3.FunctionImportV3;
import org.eclipse.ogee.client.model.edmx.v3.Reference;
import org.eclipse.ogee.client.model.edmx.v3.SchemaV3;
import org.eclipse.ogee.client.model.edmx.v3.ValueTerm;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.SchemaClassifier;

/**
 * Update EDMXSet object for V3 elements .
 */
public class EDMXSetBuilderV3 {
	/**
	 * Update EDMXSet object with V3 elements
	 * 
	 * @param edmx
	 *            - Object which represents the imported XML file
	 * @param edmxSet
	 *            - Object which represents the domain model object
	 * @return - Updated Object which represents the domain model object
	 * @throws Throwable
	 */
	public static EDMXSet build(EDMXSet edmxSet, EdmxV3 edmx, EDMX odataEdmx,
			Map<String, Map<Object, Object>> odataObjectsMap)
			throws BuilderException {

		// if EDMX reference exist call Add reference( recursive call )
		Reference[] uriList = edmx.getReferences();

		if (uriList != null) {
			for (Reference uri : uriList) {
				String stringUri = uri.getUrl();
				ODataModelAddReferenceHandlerInternal addReferenceHandlerInternal = new ODataModelAddReferenceHandlerInternal();
				IStatus status = addReferenceHandlerInternal.addReference(
						stringUri, edmxSet, odataEdmx);

				if (!status.isOK()) {
					Throwable BuilderException = status.getException();
					throw (BuilderException) BuilderException;
				}

			}

		}

		// Update ODataV3 elements in the edmxSet
		try {
			updateEDMXSetElements(edmxSet, edmx, odataObjectsMap);
		} catch (ModelAPIException e) {

			throw new BuilderException(e.getMessage());
		}

		// Update ODataV3 annotation
		AddAnnotation2EDMXSetObects annotation2edmxSetObjects = new AddAnnotation2EDMXSetObects();
		annotation2edmxSetObjects.annotateEDMXSetobjects(odataObjectsMap, edmx,
				edmxSet);

		return edmxSet;
	}

	private static void updateEDMXSetElements(EDMXSet edmxSet, EdmxV3 edmx,
			Map<String, Map<Object, Object>> odataObjectsMap)
			throws BuilderException, ModelAPIException {
		// update each schema with the new elements and the updated elements
		org.eclipse.ogee.client.model.edmx.Schema[] schemata = edmx
				.getEdmxDataServices().getSchemas();
		for (org.eclipse.ogee.client.model.edmx.Schema currSchema : schemata) {
			SchemaV3 currSchemaV3 = (SchemaV3) currSchema;
			String schemaNamespace = currSchemaV3.getNamespace();
			Map<Object, Object> nsObject = odataObjectsMap.get(currSchemaV3
					.getNamespace());
			org.eclipse.ogee.model.odata.Schema schema = (org.eclipse.ogee.model.odata.Schema) nsObject
					.get(currSchemaV3);
			// update using used namespace reference
			Using[] usings = currSchemaV3.getUsings();
			for (Using usingEntry : usings) {
				if (!usingEntry.getNamespace().equals(
						currSchemaV3.getNamespace())) {
					UsingBuilderV3 usingBuilderV3 = new UsingBuilderV3();
					usingBuilderV3.build(usingEntry, odataObjectsMap, schema,
							edmxSet);
				}
			}

			// add enums
			EnumType[] enumTypes = currSchemaV3.getEnumTypes();
			for (EnumType currEnumType : enumTypes) {
				EnumTypeBuilderV3 enumTypeBuilderV3 = new EnumTypeBuilderV3();
				enumTypeBuilderV3.build(currEnumType, odataObjectsMap, schema);
			}

			// add value term
			ValueTerm[] valueTerms = currSchemaV3.getValueTerms();
			for (ValueTerm currValueTerm : valueTerms) {
				ValueTermBuilderV3 valueTermBuilderV3 = new ValueTermBuilderV3();
				valueTermBuilderV3.build(currValueTerm, odataObjectsMap,
						schema, edmxSet);
			}

			// update entity type properties
			for (EntityType currEntityType : currSchemaV3.getEntityTypes()) {
				// Update entityType base type
				EntityTypeBuilderV3 entityTypeBuilderV3 = new EntityTypeBuilderV3();
				entityTypeBuilderV3.build(currEntityType, odataObjectsMap,
						schema, edmxSet);

				for (Property currProprty : currEntityType.getProperties()) {
					PropertyBuilderV3 propertyBuilderV3 = new PropertyBuilderV3();
					propertyBuilderV3.build(currProprty, null, currEntityType,
							odataObjectsMap, schema, edmxSet);
				}

				// update NavigationProperty relationship attribute
				for (org.eclipse.ogee.client.model.edmx.NavigationProperty currNavigationProperty : currEntityType
						.getNavigationProperties()) {
					NavigationPropertyBuilderV3 navigationPropertyBuilderV3 = new NavigationPropertyBuilderV3();
					navigationPropertyBuilderV3.build(currNavigationProperty,
							odataObjectsMap, schema, edmxSet);
				}
			}

			// update complex type properties
			for (ComplexType currComplexType : currSchemaV3.getComplexTypes()) {
				// Update entityType base type
				ComplexTypeBuilderV3 complexTypeBuilderV3 = new ComplexTypeBuilderV3();
				complexTypeBuilderV3.build(currComplexType, odataObjectsMap,
						schema, edmxSet);

				for (Property currProprty : currComplexType.getProperties()) {
					PropertyBuilderV3 propertyBuilderV3 = new PropertyBuilderV3();
					propertyBuilderV3.build(currProprty, currComplexType, null,
							odataObjectsMap, schema, edmxSet);
				}
			}

			// update Association Ends
			for (Association currAssociation : currSchemaV3.getAssociations()) {
				for (End currEnd : currAssociation.getEnds()) {
					AssociationEndBuilderV3 associationEndBuilderV3 = new AssociationEndBuilderV3();
					associationEndBuilderV3.build(currEnd, currAssociation,
							schemaNamespace, odataObjectsMap, schema, edmxSet);
				}
			}

			// update function import
			for (EntityContainer currEntityContainer : currSchemaV3
					.getEntityContainers()) {
				// set Classifier to service when entity container is not empty
				schema.getClassifiers().add(SchemaClassifier.SERVICE);

				// update the function import
				for (FunctionImport currFunctionImport : currEntityContainer
						.getFunctionImports()) {
					FunctionImportV3 currFunctionImportV3 = (FunctionImportV3) currFunctionImport;
					FunctionImportBuilderV3 functionImportBuilderV3 = new FunctionImportBuilderV3();
					functionImportBuilderV3.build(currFunctionImportV3,
							schemaNamespace, odataObjectsMap, schema, edmxSet);

				}

				// update entity set
				for (EntitySet currEntitySet : currEntityContainer
						.getEntitySets()) {
					EntitySetBuilderV3 entitySetBuilderV3 = new EntitySetBuilderV3();
					entitySetBuilderV3.build(currEntitySet, schemaNamespace,
							odataObjectsMap, schema, edmxSet);
				}

			}

		}

	}

}
