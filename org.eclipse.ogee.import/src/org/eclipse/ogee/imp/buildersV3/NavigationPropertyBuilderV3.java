/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.NavigationProperty;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.Association;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.osgi.util.NLS;

public class NavigationPropertyBuilderV3 {
	public void build(NavigationProperty xmlNavigationProperty,
			Map<String, Map<Object, Object>> odataObjectsMap, Schema schema,
			EDMXSet edmxSet) throws BuilderException, ModelAPIException {
		// Get Relationship from XML object
		String relationship = xmlNavigationProperty.getRelationship();
		// Get NavigationProperty EdmxSet object
		org.eclipse.ogee.model.odata.NavigationProperty navigationProperty = (org.eclipse.ogee.model.odata.NavigationProperty) odataObjectsMap
				.get(schema.getNamespace()).get(xmlNavigationProperty);
		// XML Relationship is not null but NavigationProperty EdmxSet
		// Relationship object is null - sign to reference type
		if (relationship != null
				&& navigationProperty.getRelationship() == null) {
			Object objectType = null;
			BuilderV3Util builderV3Utility = new BuilderV3Util();
			// Extract type and namespace from the XML relationship string
			String objTypeName = builderV3Utility
					.extractTypeShortName(relationship);
			String objIdentifier = builderV3Utility
					.extractTypeNameSpace(relationship);
			if (objIdentifier != null && !objIdentifier.equals("Edm")) //$NON-NLS-1$
			{

				// Get the target schema namespace
				String targetSchemaNamespace = builderV3Utility
						.getSchemaNamespace(schema, objIdentifier, edmxSet);
				// target schema not found
				if (targetSchemaNamespace == null
						|| targetSchemaNamespace.isEmpty()) {
					throw new BuilderException(NLS.bind(
							BuilderV3Messages.addReference02, objIdentifier));
				}
				// Get the respective relationship(Association) EdmxSet object
				// from the target schema
				objectType = builderV3Utility.getObjectTypeFromTargetSchema(
						targetSchemaNamespace, edmxSet, schema, objTypeName);

				if (objectType == null) {
					throw new BuilderException(NLS.bind(
							BuilderV3Messages.TypeError_2, relationship));
				}

				if (objectType instanceof Association) {
					navigationProperty
							.setRelationship((Association) objectType);
				} else {
					throw new BuilderException(NLS.bind(
							BuilderV3Messages.TypeError_2, relationship));
				}
			}
		}
	}
}
