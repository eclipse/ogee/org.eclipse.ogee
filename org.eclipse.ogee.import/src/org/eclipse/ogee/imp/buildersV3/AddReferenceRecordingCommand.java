/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import javax.management.RuntimeErrorException;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.builders.ODataModelEDMXSetBuilder;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.service.validation.Result;

/**
 * AddReferenceRecordingCommand - add the reference in the context of a command
 */

public class AddReferenceRecordingCommand extends RecordingCommand {
	private EDMXSet currEdmxSet;
	private Result referencedEdmxResult;

	public AddReferenceRecordingCommand(
			final TransactionalEditingDomain domain,
			final Result referencedEdmxResult, final EDMXSet currEdmxSet) {
		super(domain);
		this.currEdmxSet = currEdmxSet;
		this.referencedEdmxResult = referencedEdmxResult;
	}

	@Override
	protected void doExecute() {
		String serviceUrl = this.referencedEdmxResult.getServiceUrl();
		if (serviceUrl.isEmpty()) {
			serviceUrl = this.referencedEdmxResult.getServiceMetadataUri();
		}

		Edmx edmx = this.referencedEdmxResult.getEdmx1();

		try {
			AddReferenceUtil addReferenceUtil = new AddReferenceUtil();
			addReferenceUtil.checkRefNamespaceDuplication(edmx, currEdmxSet);
			ODataModelEDMXSetBuilder.updateEdmxSet(this.currEdmxSet, edmx,
					null, serviceUrl);
		} catch (BuilderException e) {
			throw new RuntimeErrorException(null, e.getMessage());

		}

	}

}
