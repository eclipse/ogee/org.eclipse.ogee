/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import javax.management.RuntimeErrorException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.ogee.imp.Activator;
import org.eclipse.ogee.imp.util.addreferencehandler.api.IODataModelAddReferenceHandler;
import org.eclipse.ogee.model.api.IModelContext;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.api.TransactionException;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.utils.service.validation.Result;

public class ODataModelAddReferenceHandler implements
		IODataModelAddReferenceHandler {

	@Override
	public IStatus addReference(final Result referencedEdmxResult,
			final EDMXSet currEdmxSet) {
		// call the add reference via command
		try {
			TransactionalEditingDomain domain = IModelContext.INSTANCE
					.getTransaction(currEdmxSet);

			AddReferenceRecordingCommand addReferenceRecordingCommand = new AddReferenceRecordingCommand(
					domain, referencedEdmxResult, currEdmxSet);

			domain.getCommandStack().execute(addReferenceRecordingCommand);

		} catch (TransactionException e) {
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					IStatus.ERROR, e.getMessage(), e);
		} catch (ModelAPIException e) {
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					IStatus.ERROR, e.getMessage(), e);
		} catch (RuntimeErrorException e) {
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					IStatus.ERROR, e.getMessage(), e);
		}

		return Status.OK_STATUS;

	}

}
