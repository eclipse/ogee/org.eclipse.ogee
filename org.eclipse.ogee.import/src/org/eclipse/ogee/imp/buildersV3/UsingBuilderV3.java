/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Using;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.osgi.util.NLS;

public class UsingBuilderV3 {
	public void build(Using xmlUsingEntry,
			Map<String, Map<Object, Object>> odataObjectsMap,
			Schema currentSchema, EDMXSet edmxSet) throws BuilderException {
		// get the using form the lockup table
		org.eclipse.ogee.model.odata.Using usingObject = (org.eclipse.ogee.model.odata.Using) odataObjectsMap
				.get(currentSchema.getNamespace()).get(xmlUsingEntry);
		// get target namespace
		BuilderV3Util builderV3Util = new BuilderV3Util();
		Schema targetSchema;
		try {
			String xmlNamespace = xmlUsingEntry.getNamespace();
			targetSchema = builderV3Util.getTargetSchemaFromNamespace(
					xmlNamespace, edmxSet, currentSchema);
			if (targetSchema == null) {
				throw new BuilderException(NLS.bind(
						BuilderV3Messages.addAnnotationToObjectError03,
						xmlNamespace));
			}
			// set used namespace schema to the using object
			usingObject.setUsedNamespace(targetSchema);
		} catch (ModelAPIException e) {
			throw new BuilderException(e.getMessage());
		}
	}
}
