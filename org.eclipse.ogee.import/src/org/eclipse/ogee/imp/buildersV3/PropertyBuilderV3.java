/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.util.Map;

import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.Property;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.utils.logger.Logger;
import org.eclipse.osgi.util.NLS;

public class PropertyBuilderV3 {

	private Logger logger = Logger.getLogger(PropertyBuilderV3.class.getName());

	public void build(
			org.eclipse.ogee.client.model.edmx.Property edmxProperty,
			org.eclipse.ogee.client.model.edmx.ComplexType edmxComplexType,
			org.eclipse.ogee.client.model.edmx.EntityType edmxEntityType,
			Map<String, Map<Object, Object>> odataObjectsMap, Schema schema,
			EDMXSet edmxSet) throws ModelAPIException, BuilderException {
		Property property = null;

		// get the propety form the lockup table
		Map<Object, Object> nsObject = odataObjectsMap.get(schema
				.getNamespace());
		if (edmxEntityType == null) // Complex type
		{
			property = (Property) nsObject.get(edmxComplexType.toString()
					+ edmxProperty);
		} else
		// Entitytype
		{
			property = (Property) nsObject.get(edmxEntityType.toString()
					+ edmxProperty);
		}

		// get edmxProperty type
		String propertyTypeLong = edmxProperty.getType();

		if (property != null) {
			IPropertyTypeUsage propertyTypeUsage = property.getType();
			if (propertyTypeLong != null && propertyTypeUsage == null) {
				// Get the property type
				BuilderV3Util builderV3Util = new BuilderV3Util();

				String propertyType = builderV3Util
						.extractTypeShortName(propertyTypeLong);
				String objIdentifier = builderV3Util
						.extractTypeNameSpace(propertyTypeLong);
				boolean isCollection = builderV3Util
						.isCollection(propertyTypeLong);

				if (objIdentifier == null || objIdentifier.equals("Edm"))//$NON-NLS-1$
				{
					objIdentifier = schema.getNamespace();
				}

				String targetSchema = builderV3Util.getSchemaNamespace(schema,
						objIdentifier, edmxSet);

				if (targetSchema == null || targetSchema.isEmpty()) {
					throw new BuilderException(NLS.bind(
							BuilderV3Messages.addReference02, objIdentifier));
				}

				Object objectType = builderV3Util
						.getObjectTypeFromTargetSchema(targetSchema, edmxSet,
								schema, propertyType);

				if (objectType == null) {
					logger.logWarning(NLS.bind(BuilderV3Messages.TypeError_0,
							propertyTypeLong, "Property"));//$NON-NLS-1$
				}

				Object typeUsage = builderV3Util.createTypeUsage(objectType);

				if (typeUsage != null
						&& typeUsage instanceof IPropertyTypeUsage) {
					((IPropertyTypeUsage) typeUsage)
							.setCollection(isCollection);
					property.setType((IPropertyTypeUsage) typeUsage);
				}

			}
		}

	}

}
