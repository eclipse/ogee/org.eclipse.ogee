/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ogee.imp.Activator;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.builders.ODataModelEDMXSetBuilderUtil;
import org.eclipse.ogee.model.api.vocabularies.ICapabilityVocabulary;
import org.eclipse.ogee.model.api.vocabularies.ICoreVocabulary;
import org.eclipse.ogee.model.api.vocabularies.IMeasuresVocabulary;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXSet;

public class ODataModelAddReferenceHandlerInternal {

	public IStatus addReference(String uri, EDMXSet edmxSet,
			EDMX parentOdataEdmx) {

		// Default vocabularies need not be built from url
		if (uri.contains(ICoreVocabulary.VC_NAMESPACE)
				|| uri.contains(ICapabilityVocabulary.VC_NAMESPACE)
				|| uri.contains(IMeasuresVocabulary.VC_NAMESPACE)) {

			return Status.OK_STATUS;
		}
		// get EDMXSet from the url
		try {
			ODataModelEDMXSetBuilderUtil.buildFromUri(edmxSet, uri,
					parentOdataEdmx);
		} catch (BuilderException e) {

			return new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					IStatus.ERROR, e.getMessage(), e);
		}

		return Status.OK_STATUS;
	}

}
