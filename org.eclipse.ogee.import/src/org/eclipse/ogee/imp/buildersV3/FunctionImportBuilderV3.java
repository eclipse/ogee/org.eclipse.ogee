/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.Parameter;
import org.eclipse.ogee.client.model.edmx.v3.ParameterV3;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.nls.messages.FrameworkImportMessages;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.Binding;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.EntityType;
import org.eclipse.ogee.model.odata.EntityTypeUsage;
import org.eclipse.ogee.model.odata.FunctionImport;
import org.eclipse.ogee.model.odata.IFunctionReturnTypeUsage;
import org.eclipse.ogee.model.odata.IParameterTypeUsage;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.ReturnEntityTypeUsage;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.osgi.util.NLS;

public class FunctionImportBuilderV3 {

	public void build(
			org.eclipse.ogee.client.model.edmx.v3.FunctionImportV3 edmxFunctionImport,
			String namespace, Map<String, Map<Object, Object>> objects,
			Schema schema, EDMXSet edmxSet) throws BuilderException,
			ModelAPIException {

		validateInputParameters(edmxFunctionImport, namespace, objects);

		// get emf model Function Import
		Map<Object, Object> nsObject = objects.get(namespace);

		FunctionImport functionImport = (FunctionImport) nsObject
				.get(edmxFunctionImport);
		String functionImportName = functionImport.getName();

		// set isSideEffecting for FunctionImport
		boolean isSideEffecting = edmxFunctionImport.isIsSideEffecting();
		functionImport.setSideEffecting(isSideEffecting);

		// add option of Return type from enum type usage.
		if ((edmxFunctionImport.getReturnType()) != null
				&& (functionImport.getReturnType() == null)) {
			IFunctionReturnTypeUsage returnObjTypeUsage = this.buildReturnType(
					edmxFunctionImport, namespace, objects, schema, edmxSet);
			if (returnObjTypeUsage != null) {
				functionImport.setReturnType(returnObjTypeUsage);
			}
		}

		// update function import parameters
		for (Parameter currFIParameter : edmxFunctionImport.getParameters()) {
			FunctionImportParameterBuilderV3 fiParameterV3 = new FunctionImportParameterBuilderV3();
			ParameterV3 edmxFIParameter = (ParameterV3) currFIParameter;
			fiParameterV3.build(edmxFIParameter, namespace, objects, schema,
					edmxSet, functionImportName);
		}

		// set BindTo for Function Import
		// check whether isBindable=true and set the type of first parameter
		// into Bind
		boolean isBindable = edmxFunctionImport.isBindable();
		if (isBindable) {
			if (functionImport.getParameters() != null
					&& !functionImport.getParameters().isEmpty()) {
				org.eclipse.ogee.model.odata.Parameter firstParameter = functionImport
						.getParameters().get(0);
				IParameterTypeUsage firstParameterTypeUsage = firstParameter
						.getType();
				if (firstParameterTypeUsage != null
						&& firstParameterTypeUsage instanceof EntityTypeUsage) {

					Binding binding = OdataFactory.eINSTANCE.createBinding();
					EntityType firstParameterType = ((EntityTypeUsage) firstParameterTypeUsage)
							.getEntityType();
					binding.setType(firstParameterType);
					functionImport.setBinding(binding);
					if (firstParameterTypeUsage.isCollection()) {
						binding.setCollection(true);
					}
				}
			}
		}

	}

	public IFunctionReturnTypeUsage buildReturnType(
			org.eclipse.ogee.client.model.edmx.FunctionImport edmxFunctionImport,
			String namespace, Map<String, Map<Object, Object>> objects,
			Schema schema, EDMXSet edmxSet) throws BuilderException,
			ModelAPIException {

		Object objectType = null;

		String edmxFunctionImportReturnType = edmxFunctionImport
				.getReturnType();

		BuilderV3Util builderV3Utility = new BuilderV3Util();

		String objTypeShortName = builderV3Utility
				.extractTypeShortName(edmxFunctionImportReturnType);
		String objIdentifier = builderV3Utility
				.extractTypeNameSpace(edmxFunctionImportReturnType);
		boolean isCollection = builderV3Utility
				.isCollection(edmxFunctionImportReturnType);

		if (objIdentifier == null || objIdentifier.equals("Edm"))//$NON-NLS-1$
		{
			objIdentifier = namespace;
		}
		String targetSchema = builderV3Utility.getSchemaNamespace(schema,
				objIdentifier, edmxSet);
		// target schema not found
		if (targetSchema == null || targetSchema.isEmpty()) {
			throw new BuilderException(NLS.bind(
					BuilderV3Messages.addReference02, objIdentifier));
		}

		objectType = builderV3Utility.getObjectTypeFromTargetSchema(
				targetSchema, edmxSet, schema, objTypeShortName);

		if (objectType == null) {
			return null;
		}

		if (objectType instanceof EntityType) {
			ReturnEntityTypeUsage entityTypeUsage = OdataFactory.eINSTANCE
					.createReturnEntityTypeUsage();
			entityTypeUsage.setEntityType((EntityType) objectType);
			entityTypeUsage.setCollection(isCollection);
			return entityTypeUsage;
		}

		Object typeUsage = builderV3Utility.createTypeUsage(objectType);

		if (typeUsage != null && typeUsage instanceof IPropertyTypeUsage) {
			((IPropertyTypeUsage) typeUsage).setCollection(isCollection);
			return (IFunctionReturnTypeUsage) typeUsage;
		}

		return null;

	}

	private static void validateInputParameters(
			org.eclipse.ogee.client.model.edmx.FunctionImport edmxFunctionImport,
			String namespace, Map<String, Map<Object, Object>> objects)
			throws BuilderException {
		if (edmxFunctionImport == null) {
			throw new BuilderException(
					FrameworkImportMessages.FunctionBuilder_1);
		}

		if (objects == null) {
			throw new BuilderException(FrameworkImportMessages.EDMXBuilder_3);
		}

		if (namespace == null) {
			throw new BuilderException(FrameworkImportMessages.SchemaBuilder_5);
		}

	}

}
