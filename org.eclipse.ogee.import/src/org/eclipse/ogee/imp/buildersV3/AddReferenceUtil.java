/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ogee.client.model.edmx.Edmx;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.odata.EDMX;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.osgi.util.NLS;

public class AddReferenceUtil {

	public String checkNamespaceDuplication(EDMX refEdmx, EDMXSet currEdmxSet) {
		if (refEdmx != null && currEdmxSet != null) {
			EList<Schema> refSchemata = refEdmx.getDataService().getSchemata();
			EList<Schema> currSchemata = currEdmxSet.getSchemata();
			for (Schema currSchema : currSchemata) {
				String currNamspace = currSchema.getNamespace();
				if (currNamspace != null) {
					for (Schema refSchema : refSchemata) {
						if (refSchema.getNamespace().equals(currNamspace)) {
							return currNamspace;
						}
					}
				}
			}
		}
		return null;
	}

	public String checkMainNamespaceDuplication(Edmx edmx, EDMXSet currEdmxSet) {

		if (edmx != null && currEdmxSet != null
				&& currEdmxSet.getMainEDMX() != null) {
			org.eclipse.ogee.client.model.edmx.Schema[] refSchemata = edmx
					.getEdmxDataServices().getSchemas();
			EList<Schema> currSchemata = currEdmxSet.getMainEDMX()
					.getDataService().getSchemata();
			for (Schema currSchema : currSchemata) {
				String currNamspace = currSchema.getNamespace();
				if (currNamspace != null) {

					for (org.eclipse.ogee.client.model.edmx.Schema refSchema : refSchemata) {
						if (refSchema.getNamespace().equals(currNamspace)) {
							return currNamspace;
						}
					}
				}
			}
		}
		return null;

	}

	public boolean skipRefNamespaceDuplication(Edmx edmx, EDMXSet currEdmxSet) {

		if (edmx != null && currEdmxSet != null) {
			org.eclipse.ogee.client.model.edmx.Schema[] refSchemata = edmx
					.getEdmxDataServices().getSchemas();
			EList<Schema> currSchemata = currEdmxSet.getSchemata();
			for (Schema currSchema : currSchemata) {
				String currNamspace = currSchema.getNamespace();
				if (currNamspace != null) {

					for (org.eclipse.ogee.client.model.edmx.Schema refSchema : refSchemata) {
						if (refSchema.getNamespace().equals(currNamspace)) {
							return true;
						}
					}
				}
			}
		}
		return false;

	}

	public void checkRefNamespaceDuplication(Edmx edmx, EDMXSet currEdmxSet)
			throws BuilderException {

		if (edmx != null && currEdmxSet != null) {
			org.eclipse.ogee.client.model.edmx.Schema[] refSchemata = edmx
					.getEdmxDataServices().getSchemas();
			EList<Schema> currSchemata = currEdmxSet.getSchemata();
			for (Schema currSchema : currSchemata) {
				String currNamspace = currSchema.getNamespace();
				if (currNamspace != null) {

					for (org.eclipse.ogee.client.model.edmx.Schema refSchema : refSchemata) {
						if (refSchema.getNamespace().equals(currNamspace)) {
							String message = NLS.bind(
									BuilderV3Messages.addReference03,
									currNamspace);
							throw new BuilderException(message);

						}
					}
				}
			}
		}

	}
}
