/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.util.Map;

import org.eclipse.ogee.client.model.edmx.ComplexType;
import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.osgi.util.NLS;

public class ComplexTypeBuilderV3 {
	public void build(ComplexType xmlComplexType,
			Map<String, Map<Object, Object>> odataObjectsMap, Schema schema,
			EDMXSet edmxSet) throws BuilderException, ModelAPIException {
		// Get base type from XML object
		String baseType = xmlComplexType.getBaseType();
		// Get ComplexType EdmxSet object
		org.eclipse.ogee.model.odata.ComplexType complexType = (org.eclipse.ogee.model.odata.ComplexType) odataObjectsMap
				.get(schema.getNamespace()).get(xmlComplexType);
		// XML base type is not null but ComplexType EdmxSet base type object is
		// null - sign to reference type
		if (baseType != null && complexType.getBaseType() == null) {
			Object objectType = null;
			BuilderV3Util builderV3Utility = new BuilderV3Util();
			// Extract type and namespace from the XML base type string
			String objTypeName = builderV3Utility
					.extractTypeShortName(baseType);
			String objIdentifier = builderV3Utility
					.extractTypeNameSpace(baseType);
			if (objIdentifier != null && !objIdentifier.equals("Edm")) //$NON-NLS-1$
			{

				// Get the target schema namespace
				String targetSchemaNamespace = builderV3Utility
						.getSchemaNamespace(schema, objIdentifier, edmxSet);
				if (targetSchemaNamespace == null
						|| targetSchemaNamespace.isEmpty()) {
					throw new BuilderException(NLS.bind(
							BuilderV3Messages.addReference02, objIdentifier));
				}
				// Get the respective base type EdmxSet object from the target
				// schema
				objectType = builderV3Utility.getObjectTypeFromTargetSchema(
						targetSchemaNamespace, edmxSet, schema, objTypeName);

				if (objectType == null) {
					throw new BuilderException(NLS.bind(
							BuilderV3Messages.TypeError_0, baseType,
							"ComplexType")); //$NON-NLS-1$
				}

				if (objectType instanceof org.eclipse.ogee.model.odata.ComplexType) {
					complexType
							.setBaseType((org.eclipse.ogee.model.odata.ComplexType) objectType);
				} else {
					throw new BuilderException(NLS.bind(
							BuilderV3Messages.TypeError_0, baseType,
							"ComplexType")); //$NON-NLS-1$
				}
			}
		}
	}
}
