/*******************************************************************************
 *  Copyright (c) 2012-2014 SAP SE.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *  SAP SE - initial API and implementation and/or initial documentation
 *
 *******************************************************************************/
package org.eclipse.ogee.imp.buildersV3;

import java.util.Map;

import org.eclipse.ogee.imp.builders.BuilderException;
import org.eclipse.ogee.imp.util.builderV3.nls.messages.BuilderV3Messages;
import org.eclipse.ogee.model.api.ModelAPIException;
import org.eclipse.ogee.model.odata.EDMTypes;
import org.eclipse.ogee.model.odata.EDMXSet;
import org.eclipse.ogee.model.odata.IPropertyTypeUsage;
import org.eclipse.ogee.model.odata.OdataFactory;
import org.eclipse.ogee.model.odata.Schema;
import org.eclipse.ogee.model.odata.SchemaClassifier;
import org.eclipse.ogee.model.odata.SimpleType;
import org.eclipse.ogee.model.odata.SimpleTypeUsage;
import org.eclipse.ogee.model.odata.ValueTerm;
import org.eclipse.osgi.util.NLS;

public class ValueTermBuilderV3 {
	public void build(
			org.eclipse.ogee.client.model.edmx.v3.ValueTerm edmxValueTerm,
			Map<String, Map<Object, Object>> odataObjectsMap, Schema schema,
			EDMXSet edmxSet) throws BuilderException, ModelAPIException {

		// get valueTerm form the lockup table
		Map<Object, Object> nsObject = odataObjectsMap.get(schema
				.getNamespace());
		ValueTerm valueTerm = (ValueTerm) nsObject.get(edmxValueTerm);

		if (valueTerm == null) {
			String valueTermName = edmxValueTerm.getName();
			if (valueTermName == null) {
				throw new BuilderException(BuilderV3Messages.ValueTermV3_1);
			}
			// create valueTerm
			valueTerm = OdataFactory.eINSTANCE.createValueTerm();
			// save valueTerm
			nsObject.put(edmxValueTerm, valueTerm);

			// set valueTerm name
			valueTerm.setName(valueTermName);

			// add the Value term to the Schema
			schema.getValueTerms().add(valueTerm);
			schema.getClassifiers().add(SchemaClassifier.VOCABULARY);

			// Get value term type
			BuilderV3Util builderV3Util = new BuilderV3Util();
			String valueTermTypeLong = edmxValueTerm.getType();
			if (valueTermTypeLong == null) {
				throw new BuilderException(BuilderV3Messages.ValueTermV3_2);
			}
			String valueTermType = builderV3Util
					.extractTypeShortName(valueTermTypeLong);
			String objIdentifier = builderV3Util
					.extractTypeNameSpace(valueTermTypeLong);
			boolean isCollection = builderV3Util
					.isCollection(valueTermTypeLong);

			// type without prefix or starts with Edm, set it's namespace as
			// local
			if (objIdentifier == null || objIdentifier.equals("Edm"))//$NON-NLS-1$
			{
				objIdentifier = schema.getNamespace();
			}

			String targetSchema = builderV3Util.getSchemaNamespace(schema,
					objIdentifier, edmxSet);

			// target schema not found
			if (targetSchema == null || targetSchema.isEmpty()) {
				throw new BuilderException(NLS.bind(
						BuilderV3Messages.addReference02, objIdentifier));
			}

			Object objectType = builderV3Util.getObjectTypeFromTargetSchema(
					targetSchema, edmxSet, schema, valueTermType);

			if (objectType != null) // type can be enumtype or complex type
			{
				Object typeUsage = builderV3Util.createTypeUsage(objectType);
				if (typeUsage != null
						&& typeUsage instanceof IPropertyTypeUsage) {
					((IPropertyTypeUsage) typeUsage)
							.setCollection(isCollection);
					valueTerm.setType((IPropertyTypeUsage) typeUsage);
					return;
				}

			} else {
				// check that it is simple type

				EDMTypes edmType = EDMTypes.getByName(valueTermType);
				if (edmType != null) {
					SimpleType simpleType = OdataFactory.eINSTANCE
							.createSimpleType();
					simpleType.setType(edmType);
					SimpleTypeUsage stuValueTerm = OdataFactory.eINSTANCE
							.createSimpleTypeUsage();
					stuValueTerm.setSimpleType(simpleType);
					stuValueTerm.setCollection(isCollection);
					valueTerm.setType(stuValueTerm);
					return;
				}
			}

			// not valid type
			if (valueTerm.getType() == null) {
				throw new BuilderException(NLS.bind(
						BuilderV3Messages.ValueTermV3_3, valueTermTypeLong));
			}

		}
	}

}
